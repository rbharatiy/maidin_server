<%@page import="java.util.Properties" %>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%-- <%@page import="org.springframework.beans.factory.annotation.Autowired"%> --%>
<%@page import="java.io.InputStream" %>
<%@page import="java.io.InputStreamReader" %>
<%@page import="java.io.OutputStreamWriter" %>
<%@page import="java.net.HttpURLConnection" %>
<%@page import="java.net.URL" %>
<%@page import="com.amazonaws.util.json.JSONObject" %>
<%@page import="java.io.BufferedReader"%>


<%@page import="java.io.File" %>
<%@page import="com.maidin.common.util.CommonUtils" %> 
<%@page import="com.amazonaws.util.json.JSONException" %>
<%@page import="javax.servlet.*" %> 
<%@page import="javax.servlet.http.*" %>
<%@page import="java.io.IOException" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"                                                                        
    pageEncoding="ISO-8859-1"%>                                                                                                             
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">                                      

                                                                                                                                                                                      
<html>                                                                                                                                      
<head>                                                                                                                                      
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">                                                                    
<title>Email Test</title>
<style>
      html, body {
      position: relative;
      width: 100%;
      height: 100%;
      padding: 0;
      margin: 0;
      }
      .loader {
      border: 5px solid #f3f3f3;
      border-radius: 50%;
      border-top: 5px solid #3498db;

      border-bottom: 5px solid #3498db;
      width: 120px;
      height: 120px;
      -webkit-animation: spin 2s linear infinite;
      animation: spin 2s linear infinite;
      }

      @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
      }

      @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
      }
      .displayed {
      display: block;
      margin-left: auto;
      margin-right: auto }
    </style>

<script>
function redirectToContactPage(toRedirectURL){
	
	 window.setTimeout(function(){

	        // Move to a new location
	        window.location.href = toRedirectURL;

	    }, 5000);
	}
</script>
                                                                      
</head>                                                                                                                                     
<body>         
<%

	JSONObject payload = new JSONObject();
	payload.put("email",request.getParameter("email"));
	payload.put("name",request.getParameter("name"));
	payload.put("mobileNumber",request.getParameter("mobileNumber"));
	payload.put("message",request.getParameter("message"));
	
	
	
    
	
	System.out.println("Sending Mailer Request");
	HttpURLConnection connection = null;
	try {
		String redirectURL = "http://maidin.in/contact.html";
		URL url = new URL("http://maidin.in/service/emailer/contact/us");
	    connection = (HttpURLConnection) url.openConnection();
	    connection.setDoOutput(true);
	    connection.setDoInput(true);
	    connection.setRequestMethod("POST");
	    connection.setRequestProperty("Content-Type", "application/json");
	    connection.setRequestProperty("Accept", "application/json");
	    OutputStreamWriter streamWriter = new OutputStreamWriter(connection.getOutputStream());
	    streamWriter.write(payload.toString());
	    streamWriter.flush();
	    StringBuilder stringBuilder = new StringBuilder();
	    System.out.println("Status : " + connection.getResponseCode());
	    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK){
	    	// New location to be redirected
	    	 System.out.println("Status : 200");
	    	%>
	    	<script>
	    		redirectToContactPage("<%=redirectURL %>"); //redirect to contact page
        	</script>
	    	<% 
	    	 //redirectToAnotherPage(response);
	       
	    } else {
	    	%>
	    	<script>
	    		redirectToContactPage("<%=redirectURL %>"); //redirect to contact page
        	</script>
	    	<% 
	    	//redirectToAnotherPage(response);
	    }
	} catch (Exception exception){
		
	} finally {
	    if (connection != null){
	        connection.disconnect();
	    }
	}
	%>
	


<div style="display: table; height: 100%; width:100%; overflow: hidden;">
      <div style="display: table-cell; vertical-align: middle;">
        <div>
          <div>
            <h4 style="text-align:center;">Enquiry Submission in progress...</h4>
                        <h4 style="text-align:center; color:#C30">Thank you for contacting us. We will be in touch with you very soon.</h4>
            <div class="loader displayed"></div>
          </div>
        </div>
      </div>
    </div>
</body>
</html>