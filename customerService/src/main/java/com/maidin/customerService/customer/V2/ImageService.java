package com.maidin.customerService.customer.V2;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Strings;
import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
//import com.maidin.adminApi.photo.impl.PhotoFiles;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.customerApi.photo.PhotoMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.filestore.StoreFile;
import com.maidin.pojo.photo.GetAllPhotosResponse;

@RestController("ImageServiceV2")
public class ImageService {

	@Autowired
	private PhotoMgmt photoMgmt;
	
	@Autowired
	private CommonApplication commonApplication;

	
	/*@RequestMapping(value = "/retrieve/V2/imageType/{imageType}/itemId/{itemId}", method = RequestMethod.GET)
	public @ResponseBody void retrievePhotoV2(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			HttpServletResponse httpResponse) throws ValidationException {

		long fileStoreId = photoMgmt.getFileStoreId(imageType, itemId,"v2", ServiceUtils.getLocale(language));

		if (fileStoreId != -1) {
			try {
				RetrieveFileResponse response = photoMgmt.retrievePhotoV2(imageType, itemId, fileStoreId,
						ServiceUtils.getLocale(language));
				ByteSink byteSink = new ByteSink() {

					@Override
					public OutputStream openStream() throws IOException {
						return httpResponse.getOutputStream();
					}
				};
				httpResponse.setContentType(response.getContentType());
				httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
						CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
				response.getByteSource().copyTo(byteSink);
			} catch (Exception e) {
				e.printStackTrace();
				httpResponse.setHeader("Exception", ExceptionResourceBundle
						.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
				httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
		}
	}*/
	
	/*@GetMapping(value = "/retrieve/productId/{productId}/productDetailsId/{productDetailsId}")
	public @ResponseBody void getImage(
			@RequestHeader(value = "accept-language", required = false) String language,
		    @PathVariable("productId") long productId,
			@PathVariable("productDetailsId") long productDetailsId, 
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreIdOfProduct(ImageCategory.PRODUCT.getId(), productId,productDetailsId, ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrievePhotoV2(ImageCategory.PRODUCT.getId(), productId, fileStoreId,
					ServiceUtils.getLocale(language));

			ByteSink byteSink = new ByteSink() {

				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}

			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}*/
	
	@GetMapping(value = "customer/v2.0/image/retrieve/photoId/{photoId}")
	public @ResponseBody void retrieveByPhotoId(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("photoId") long photoId,
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreIdByPhotoId(photoId, ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrieveIndividualPhoto(photoId, fileStoreId,
					ServiceUtils.getLocale(language));
			ByteSink byteSink = new ByteSink() {
				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}
			};
			if(!(response == null || Strings.isNullOrEmpty(response.getContentType()))){
			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
			}
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}
	

	@GetMapping(value = "customer/v2.0/image/retrieve/productDetail/{photoId}")
	public @ResponseBody void getProductDetailFile(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("photoId") long photoId,
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreIdByPhotoId(photoId, ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrieveIndividualPhoto(photoId, fileStoreId,
					ServiceUtils.getLocale(language));
			ByteSink byteSink = new ByteSink() {
				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}
			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			ByteSource byteSource = response.getByteSource() ;
			int compressesdFileSize = 0;
			try {
				if ( byteSource != null) {
					String imageCategory = ImageCategory.PRODUCTDETAIL.getName();

					// get byte array from MultipartFile
					byte[] bytes = byteSource.read();

					// get input stream from bytes
					InputStream ins = new ByteArrayInputStream(bytes);

					// get BufferedImage from InputStream
					BufferedImage bImageFromConvert = ImageIO.read(ins);

					// compress BufferedImage
					BufferedImage convertedImage = commonApplication.resizeImageWithType(bImageFromConvert, imageCategory);

					// convert compressed BufferedImage to byte
					// ByteArrayOutputStream
					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					// get input stream back from ByteArrayInputStream
					ImageIO.write(convertedImage, "png", baos);
					InputStream is = new ByteArrayInputStream(baos.toByteArray());
					compressesdFileSize = baos.toByteArray().length;
					baos.flush();
					baos.close();
					byteSource = new ByteSource() {
						@Override
						public InputStream openStream() throws IOException {
							return is;
						}
					};
				}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			response.setByteSource(byteSource);
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}
	

	
	@GetMapping(value = "customer/v2.0/productDescription/{productDetailId}")
	public @ResponseBody GetAllPhotosResponse getImageList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("productDetailId") long productDetailId,
			HttpServletResponse httpResponse) throws ValidationException {
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		try {
			response = photoMgmt.getImageList(authToken, productDetailId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}	
	

}
