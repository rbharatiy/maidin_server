package com.maidin.customerService.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.customerApi.cart.PaymentMgmt;
import com.maidin.customerApi.cart.ShoppingCartMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.CartRequest;
import com.maidin.pojo.cart.CartResponse;
import com.maidin.pojo.cart.CheckoutRequest;
import com.maidin.pojo.cart.CheckoutResponse;
import com.maidin.pojo.cart.EditOrder;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.MyCartResponse;
import com.maidin.pojo.cart.ScheduleDetails;
import com.maidin.pojo.payment.PaymentDates;
import com.maidin.pojo.payment.PaymentHistoryRequest;
import com.maidin.pojo.payment.PaymentHistoryResponse;
import com.maidin.pojo.product.CategoryListResponse;

@RestController
public class PaymentService {
	
	@Autowired
	private PaymentMgmt manager;
	
	/*@RequestMapping(value = "customer/get/payment/history/byCalendar", method = RequestMethod.PUT)
	public @ResponseBody PaymentHistoryResponse getPaymentHistorybyCalendar(
			@RequestBody PaymentHistoryRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		try {
			response = manager.getPaymentHistoryByCalendar(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			 return ApiUtils.setErrorResponseWithOperationId(response, e,
					 OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	*/
	/*@RequestMapping(value = "customer/get/payment/history", method = RequestMethod.GET)
	public @ResponseBody PaymentHistoryResponse getPaymentHistory(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		try {
			response = manager.getPaymentHistory(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			 return ApiUtils.setErrorResponseWithOperationId(response, e,
					 OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}*/
	
	/*@RequestMapping(value = "customer/get/payment/history/by/pagination", method = RequestMethod.PUT)
	public @ResponseBody PaymentHistoryResponse getPaymentHistoryByPagination(
			@RequestBody PaymentDates request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		try {
			response = manager.getPaymentHistoryByPagination(authToken, request , ServiceUtils.getLocale(language));
		} catch (Exception e) {
			 return ApiUtils.setErrorResponseWithOperationId(response, e,
					 OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}*/
	
	@RequestMapping(value = "customer/get/payment/history/{currentPageNumber}", method = RequestMethod.GET)
	public @ResponseBody PaymentHistoryResponse getPaymentHistoryByPagination(
			@PathVariable Integer currentPageNumber ,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		try {
			response = manager.getPaymentHistoryByPagination(authToken, currentPageNumber , ServiceUtils.getLocale(language));
		} catch (Exception e) {
			 return ApiUtils.setErrorResponseWithOperationId(response, e,
					 OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
}
