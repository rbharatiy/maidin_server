package com.maidin.customerService.customer.V2;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.customerApi.V2.OrderMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.customerorders.EditOrder;
import com.maidin.pojo.customerorders.GetOrderSummaryAndItemDetailsResponse;
import com.maidin.pojo.customerorders.GetPreviousOrderResponse;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.customerorders.CancelOrder;
import com.maidin.pojo.customerorders.ScheduleProductRequestV2;
import com.maidin.pojo.transactions.EmailOrderSummaryRequest;
import com.maidin.pojo.customerorders.CheckoutV2Request;
import com.maidin.pojo.customerorders.CheckoutV2Response;
import com.maidin.pojo.customerorders.GetScheduledListResponse;
import com.maidin.pojo.customerorders.GetUpcomingOrderResponse;

@RestController
public class CustomerOrderService {
	
	@Autowired
	private OrderMgmt manager;
	
	@RequestMapping(value = "customer/v2.0/checkout", method = RequestMethod.PUT)
	public @ResponseBody CheckoutV2Response checkout(@RequestBody CheckoutV2Request request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CheckoutV2Response response = new CheckoutV2Response();
		try {
			response = manager.checkout(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	

	@RequestMapping(value = "customer/v2.0/get/upcoming/orders", method = RequestMethod.GET)
	public @ResponseBody GetUpcomingOrderResponse V2getUpcomingOrders(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetUpcomingOrderResponse response = new GetUpcomingOrderResponse();
		try {
			response = manager.getUpcomingOrders(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "customer/v2.0/upsert/schedule", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse scheduleOrder(
			@RequestHeader("authToken") String authToken,
			@RequestBody ScheduleProductRequestV2 request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			System.out.println("Upsert Start Final Response: "+new Timestamp(System.currentTimeMillis()));
			response = manager.scheduleProduct(authToken, request, ServiceUtils.getLocale(language));
			System.out.println("Upsert End Final Response: "+new Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "customer/v2.0/get/scheduled/orders", method = RequestMethod.GET)
	public @ResponseBody GetScheduledListResponse getScheduleList(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetScheduledListResponse response = new GetScheduledListResponse();
		try {
			response = manager.getScheduleList(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//old and new is same
	
	@RequestMapping(value = "customer/v2.0/cancel/upcoming", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse cancelUpcoming(
			@RequestHeader("authToken") String authToken,
			@RequestBody CancelOrder request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.cancelUpcoming(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "customer/v2.0/edit/orders", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse editOrder(
			@RequestBody EditOrder request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.editOrder(authToken,request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@GetMapping(value = "customer/v2.0/get/previous/orders/page/{pageNum}")
	public @ResponseBody GetPreviousOrderResponse getPreviousOrders(
			@PathVariable Integer pageNum,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetPreviousOrderResponse response = new GetPreviousOrderResponse();
		try {
			response = manager.getPreviousOrders(authToken,pageNum, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "customer/v2.0/get/order/summary", method = RequestMethod.PUT)
	public @ResponseBody GetPreviousOrderResponse getOrderSummaryAndItemDetailsResponse(
			@RequestBody EmailOrderSummaryRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetPreviousOrderResponse response = new GetPreviousOrderResponse();
		try {
			response = manager.getOrderSummaryAndItemDetailsResponse(authToken,request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
}
