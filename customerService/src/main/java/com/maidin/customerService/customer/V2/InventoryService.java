package com.maidin.customerService.customer.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.customerApi.V2.InventoryMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.inventory.CategoryListV2Response;
import com.maidin.pojo.inventory.GetProductListV2Request;
import com.maidin.pojo.inventory.GetSubCategoryV2Request;
import com.maidin.pojo.inventory.PaymentOptionResponse;
import com.maidin.pojo.inventory.ProductListV2Response;
import com.maidin.pojo.inventory.ProductStatusRequest;
import com.maidin.pojo.inventory.ProductStatusResponse;
import com.maidin.pojo.inventory.SubCategoryListV2Response;

@RestController
public class InventoryService {

	@Autowired
	private InventoryMgmt manager;
	
	@GetMapping("customer/v2.0/inventory/get/category")
	public @ResponseBody CategoryListV2Response getV2Category(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CategoryListV2Response response = new CategoryListV2Response();
		try {
			response = manager.getCategoryListV2(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@PutMapping("customer/v2.0/inventory/get/subcategory")
	public @ResponseBody SubCategoryListV2Response getV2Brand(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			/*@PathVariable Integer pageNum,
			*/@RequestBody GetSubCategoryV2Request request) {
		SubCategoryListV2Response response = new SubCategoryListV2Response();
		try {
			response = manager.getSubCategoryListV2(authToken,request/*,pageNum*/, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@PutMapping("customer/v2.0/inventory/get/product/{pageNum}")
	public @ResponseBody ProductListV2Response getV2Brand(@RequestHeader("authToken") String authToken,
			@PathVariable Integer pageNum,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetProductListV2Request request) {
		ProductListV2Response response = new ProductListV2Response();
		try {
			response = manager.getProductListV2(authToken,request,pageNum, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "customer/v2.0/dashboard/lists", method = RequestMethod.GET)
	public @ResponseBody PaymentOptionResponse getDashboardList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		
		PaymentOptionResponse response = new PaymentOptionResponse();
		try {
			response = manager.getDashboardList(authToken,ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@PutMapping(value = "customer/v2.0/get/product/status")
	public @ResponseBody ProductStatusResponse getProductStatus(@RequestHeader("authToken") String authToken,
			@RequestBody ProductStatusRequest request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		
		ProductStatusResponse response = new ProductStatusResponse();
		try {
			response = manager.getProductStatus(authToken,request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
		
	
	
}
