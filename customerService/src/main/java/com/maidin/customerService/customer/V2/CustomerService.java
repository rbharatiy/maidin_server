package com.maidin.customerService.customer.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.customerApi.V2.CustomerMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.customerv2.PersonalInfoResponse;

@RestController("CustomerServiceV2")
public class CustomerService {

	@Autowired
	private CustomerMgmt manager;
	
	
	
/*
	@RequestMapping(value = "/update/personal/info", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse setBasicInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody CustomerPersonalInfo request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.setPersonalInfo(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
*/
	@RequestMapping(value = "customer/v2.0/get/personal/info", method = RequestMethod.GET)
	public @ResponseBody PersonalInfoResponse getBasicInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PersonalInfoResponse response = new PersonalInfoResponse();
		try {
			response = manager.getPersonalInfo(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	
}
