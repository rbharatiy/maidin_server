package com.maidin.customerService.customer;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
//import com.maidin.adminApi.photo.impl.PhotoFiles;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.customerApi.photo.PhotoMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.filestore.StoreFile;
import com.maidin.pojo.photo.GetAllPhotosResponse;

@RestController
@RequestMapping("/image")
public class ImageService {

	@Autowired
	private PhotoMgmt photoMgmt;
	
	@Autowired
	private CommonApplication commonApplication;

	@RequestMapping(value = "/retrieve/{imageType}/{itemId}", method = RequestMethod.GET)
	public @ResponseBody void getPersonalInfoFile(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreId(imageType, itemId,"v1", ServiceUtils.getLocale(language));
		if(fileStoreId == -1 && (ImageCategory.BRAND.getId()== imageType)){
			imageType = ImageCategory.SUBCATEGORY.getId();
			fileStoreId = photoMgmt.getFileStoreId(imageType, itemId,"v2", ServiceUtils.getLocale(language));
		}
		if (fileStoreId != -1) {
			//fileStoreId = photoMgmt.getFileStoreId(imageType, itemId,"v2", ServiceUtils.getLocale(language));
			try {
				RetrieveFileResponse response = photoMgmt.retrievePhoto(imageType, itemId, fileStoreId,
						ServiceUtils.getLocale(language));
				ByteSink byteSink = new ByteSink() {
					@Override
					public OutputStream openStream() throws IOException {
						return httpResponse.getOutputStream();
					}
				};
				httpResponse.setContentType(response.getContentType());
				httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
						CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
				response.getByteSource().copyTo(byteSink);
			} catch (Exception e) {
				e.printStackTrace();
				httpResponse.setHeader("Exception", ExceptionResourceBundle
						.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
				httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
		}
	}
	
	

}
