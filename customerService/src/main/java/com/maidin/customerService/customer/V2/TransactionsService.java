package com.maidin.customerService.customer.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.customerApi.V2.TransactionHistoryMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.transactions.CustomerTransactionV2Response;
import com.maidin.pojo.transactions.CustomerTransactionsRequest;
import com.maidin.pojo.transactions.EmailOrderSummaryRequest;

@Controller
public class TransactionsService {

	@Autowired
	private TransactionHistoryMgmt manager;

	@RequestMapping(value = "customer/v2.0/get/transactions/{currentPageNumber}", method = RequestMethod.PUT)
	public @ResponseBody CustomerTransactionV2Response getCustomerTransactions(@PathVariable Integer currentPageNumber,
			@RequestBody CustomerTransactionsRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CustomerTransactionV2Response response = new CustomerTransactionV2Response();
		try {
			response = manager.getCustomerTransactionByPageNumber(authToken, currentPageNumber, request.getTransactionType(), ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "customer/v2.0/email/order/summary", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse sendOrderSummaryEmailToCustomer(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody EmailOrderSummaryRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.sendOrderSummaryEmailToCustomer(authToken, request.getOrderId(), ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
}
