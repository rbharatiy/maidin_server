package com.maidin.customerService.customer;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.customerApi.cart.ShoppingCartMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.CancelOrder;
import com.maidin.pojo.cart.CartRequest;
import com.maidin.pojo.cart.CartResponse;
import com.maidin.pojo.cart.CheckoutRequest;
import com.maidin.pojo.cart.CheckoutResponse;
import com.maidin.pojo.cart.EditOrder;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.GetOrderResponseV1;
import com.maidin.pojo.cart.GetOrderResponseV2;
import com.maidin.pojo.cart.GetScheduledProductResponseV2;
import com.maidin.pojo.cart.MyCartResponse;
import com.maidin.pojo.cart.ScheduleDetails;
import com.maidin.pojo.cart.ScheduleProductRequestV2;

@RestController
@RequestMapping("/cart")
public class ShoppingCartService {
	
	@Autowired
	private ShoppingCartMgmt manager;
	
	// Not in use
	@RequestMapping(value = "add/product", method = RequestMethod.PUT)
	public @ResponseBody CartResponse addProductToCart(@RequestBody CartRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CartResponse response = new CartResponse();
		try {
			response = manager.addProductToCart(request,authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	// Not in use
	@RequestMapping(value = "remove/product", method = RequestMethod.PUT)
	public @ResponseBody CartResponse removeProductFromCart(@RequestBody CartRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CartResponse response = new CartResponse();
		try {
			response = manager.removeProductFromCart(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	// Not in use
	@RequestMapping(value = "get/mycart", method = RequestMethod.GET)
	public @ResponseBody MyCartResponse getMyCart(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		MyCartResponse response = new MyCartResponse();
		try {
			response = manager.getCart(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	
	//changes done for V1 according to V2
	@RequestMapping(value = "/checkout", method = RequestMethod.PUT)
	public @ResponseBody CheckoutResponse checkout(@RequestBody CheckoutRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CheckoutResponse response = new CheckoutResponse();
		try {
			response = manager.checkout(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	
	//changes done from V1 to V2
	@RequestMapping(value = "/get/orders", method = RequestMethod.GET)
	public @ResponseBody GetOrderResponse getOrders(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.getOrders(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "V1/get/orders", method = RequestMethod.GET)
	public @ResponseBody GetOrderResponseV1 getV1Orders(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponseV1 response = new GetOrderResponseV1();
		try {
			response = manager.getV1Orders(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//changes done from V1 to V2
	@RequestMapping(value = "/schedule/orders", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse scheduleOrder(
			@RequestBody ScheduleDetails request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.scheduleOrder(authToken,request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	//changes done for V1 to V2
	@RequestMapping(value = "/edit/orders", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse editOrder(
			@RequestBody EditOrder request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.editOrder(authToken,request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//no need to change clear schedule
	@RequestMapping(value = "/clear/schedule", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse clearOrder(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.clearOrder(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	
	//changes done for V1 according to V2
	@RequestMapping(value = "/V2/upsert/schedule", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse scheduleOrder(
			@RequestHeader("authToken") String authToken,
			@RequestBody ScheduleProductRequestV2 request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			System.out.println("Upsert Start Final Response: "+new Timestamp(System.currentTimeMillis()));
			response = manager.scheduleProductV2(authToken, request, ServiceUtils.getLocale(language));
			System.out.println("Upsert End Final Response: "+new Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "/V2/existing/schedule/{scheduleId}", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse scheduleOrderforExistingSchedule(
			@PathVariable(value="scheduleId") Long scheduleId,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.scheduleOrderV2ForExistingSchedule(scheduleId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//changes done for V1 according to V2
	@RequestMapping(value = "/V2/get/scheduled/orders", method = RequestMethod.GET)
	public @ResponseBody GetScheduledProductResponseV2 getV2ScheduleList(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetScheduledProductResponseV2 response = new GetScheduledProductResponseV2();
		try {
			System.out.println("Schedule List Start Final Response: "+new Timestamp(System.currentTimeMillis()));
			response = manager.getV2ScheduleList(authToken, ServiceUtils.getLocale(language));
			System.out.println("Schedule List End  Final Response: "+new Timestamp(System.currentTimeMillis()));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "V2/get/upcoming/orders1", method = RequestMethod.GET)
	public @ResponseBody GetOrderResponseV2 V2getUpcomingOrders1(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponseV2 response = new GetOrderResponseV2();
		try {
			response = manager.getV2UpcomingOrders(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//changes done for V1 according to V2
	@RequestMapping(value = "V2/get/upcoming/orders", method = RequestMethod.GET)
	public @ResponseBody GetOrderResponseV2 V2getUpcomingOrders(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetOrderResponseV2 response = new GetOrderResponseV2();
		try {
			response = manager.getV2UpcomingOrders(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	//no change required
	@RequestMapping(value = "/V2/cancel/upcoming", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse cancelUpcoming(
			@RequestHeader("authToken") String authToken,
			@RequestBody CancelOrder request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.cancelUpcomingV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
		
}
