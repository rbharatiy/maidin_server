package com.maidin.customerService.customer.V2;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.customerApi.V2.SearchMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.pojo.search.CustomerPopularSearches;
import com.maidin.pojo.search.PopularSearchesCustomerResponse;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;
import com.maidin.pojo.search.SearchSuggestionsResponse;

@Controller
public class SearchService {

	@Autowired
	private SearchMgmt manager;

	@RequestMapping(value = "customer/v2.0/search/products", method = RequestMethod.PUT)
	public @ResponseBody SearchProductListResponse searchProducts(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody SearchProductRequest request) {
		SearchProductListResponse response = new SearchProductListResponse();
		try {
			response = manager.searchProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "customer/v2.0/get/popular/searches", method = RequestMethod.GET)
	public @ResponseBody PopularSearchesCustomerResponse getPopularSearches(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PopularSearchesCustomerResponse response = new PopularSearchesCustomerResponse();
		try {
			List<PopularSearchesDTO> popularSearchesList = manager.getActivePopularSearchKeywords(authToken,
					ServiceUtils.getLocale(language));
			if (popularSearchesList.size() > 0) {
				for (PopularSearchesDTO dto : popularSearchesList) {
					CustomerPopularSearches popularSearches = new CustomerPopularSearches();
					popularSearches.setKeyword(dto.getKeyword());
					popularSearches.setPriority(dto.getPriority());
					popularSearches.setId(dto.getId());

					response.getPopularSearchesList().add(popularSearches);
				}
			} else {
				response.getPopularSearchesList().clear();
			}

			response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "customer/v2.0/get/search/suggestions", method = RequestMethod.PUT)
	public @ResponseBody SearchSuggestionsResponse getSearchSuggestions(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody SearchProductRequest request) {
		SearchSuggestionsResponse response = new SearchSuggestionsResponse();
		try {
			response = manager.getSearchSuggestions(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}
