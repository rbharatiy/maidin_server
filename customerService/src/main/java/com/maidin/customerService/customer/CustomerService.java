package com.maidin.customerService.customer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.util.Base64;
import com.google.common.io.ByteSink;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.customer.CustomerMgmt;
import com.maidin.customerApi.enums.DocumentType;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.customerApi.photo.PhotoMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerService.utils.ServiceUtils;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.customer.SearchProductListResponse;
import com.maidin.pojo.customer.SearchProductRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.cart.WalletInfo;
import com.maidin.pojo.common.StoreFile;
import com.maidin.pojo.customer.CustomerLogoutResponse;
import com.maidin.pojo.customer.CustomerPersonalInfo;
import com.maidin.pojo.customer.CustomerRegistration;
import com.maidin.pojo.customer.CustomerRegistrationResponse;
import com.maidin.pojo.customer.DeviceToken;
import com.maidin.pojo.customer.DeviceTokenResponse;
import com.maidin.pojo.customer.GenerateMobileOtp;
import com.maidin.pojo.customer.GenerateMobileOtpResponse;
import com.maidin.pojo.customer.GetGuestUserInfoResponse;
import com.maidin.pojo.customer.PersonalInfoResponse;
import com.maidin.pojo.customer.RegenerateAuthToken;
import com.maidin.pojo.customer.RegenerateAuthTokenResponse;
import com.maidin.pojo.customer.VerifyMobileOtp;
import com.maidin.pojo.customer.VerifyMobileOtpResponse;
import com.maidin.pojo.product.BrandListResponse;
import com.maidin.pojo.product.CategoryListResponse;
import com.maidin.pojo.product.DownloadPhotoResponse;
import com.maidin.pojo.product.GetBrandRequest;
import com.maidin.pojo.product.GetProductRequest;
import com.maidin.pojo.product.ProductListResponse;

@RestController
@RequestMapping("/customer")
public class CustomerService {

	@Autowired
	private CustomerMgmt manager;
	private PhotoMgmt photoMgmt;
	
	@Autowired
	public CustomerService(PhotoMgmt photoMgmt){
    	this.photoMgmt = photoMgmt;
    }

	@RequestMapping(value = "generate/mobile/otp", method = RequestMethod.PUT)
	public @ResponseBody GenerateMobileOtpResponse initiateGenerateMobileOtp(@RequestBody GenerateMobileOtp request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenerateMobileOtpResponse response = new GenerateMobileOtpResponse();
		try {
			response = manager.initiateGenerateMobileOTP(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {

			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "verify/mobile/number/otp", method = RequestMethod.PUT)
	public @ResponseBody VerifyMobileOtpResponse initiateVerifyMobileOtp(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody VerifyMobileOtp request) {
		VerifyMobileOtpResponse response = new VerifyMobileOtpResponse();
		try {
			response = manager.initiateVerifyMobileOtp(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/guest/info", method = RequestMethod.GET)
	public @ResponseBody GetGuestUserInfoResponse getGuestUserInfo(
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetGuestUserInfoResponse response = new GetGuestUserInfoResponse();
		try {
			response = manager.getGuestUserInfo(ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "registration", method = RequestMethod.PUT)
	public @ResponseBody CustomerRegistrationResponse registration(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody CustomerRegistration request) {
		CustomerRegistrationResponse response = new CustomerRegistrationResponse();
		try {
			response = manager.registration(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	// logout Service
	@RequestMapping(value = "logout", method = RequestMethod.PUT)
	public @ResponseBody CustomerLogoutResponse logout(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestHeader("authToken") String authToken) {
		CustomerLogoutResponse response = new CustomerLogoutResponse();
		try {
			response = manager.logout(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "upsert/token", method = RequestMethod.PUT)
	public @ResponseBody DeviceTokenResponse saveDeviceToken(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody DeviceToken request) {
		DeviceTokenResponse response = new DeviceTokenResponse();
		try {
			response = manager.saveDeviceToken(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "regenerate/authtoken", method = RequestMethod.PUT)
	public @ResponseBody RegenerateAuthTokenResponse regenAuthToken(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody RegenerateAuthToken request) {
		RegenerateAuthTokenResponse response = new RegenerateAuthTokenResponse();
		try {
			response = manager.regenAuthToken(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/category", method = RequestMethod.GET)
	public @ResponseBody CategoryListResponse getCategory(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CategoryListResponse response = new CategoryListResponse();
		try {
			response = manager.getCategory(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "V2/get/category", method = RequestMethod.GET)
	public @ResponseBody CategoryListResponse getV2Category(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CategoryListResponse response = new CategoryListResponse();
		try {
			response = manager.getV2Category(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/brands", method = RequestMethod.PUT)
	public @ResponseBody BrandListResponse getBrands(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetBrandRequest request) {
		BrandListResponse response = new BrandListResponse();
		try {
			response = manager.getBrands(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/products", method = RequestMethod.PUT)
	public @ResponseBody ProductListResponse getProductList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetProductRequest request) {
		ProductListResponse response = new ProductListResponse();
		try {
			response = manager.getProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/update/personal/info", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse setBasicInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody CustomerPersonalInfo request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.setPersonalInfo(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/personal/info", method = RequestMethod.GET)
	public @ResponseBody PersonalInfoResponse getBasicInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PersonalInfoResponse response = new PersonalInfoResponse();
		try {
			response = manager.getPersonalInfo(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/{imageCategory}/image/{imageCategoryId}", method = RequestMethod.GET)
	public @ResponseBody void retrievePhotoFromDB(
			// @RequestHeader("authToken") String authToken,
			@PathVariable(value = "imageCategory") String imageCategory,
			@PathVariable(value = "imageCategoryId") Long imageCategoryId,
			@RequestHeader(value = "accept-language", required = false) String language,
			HttpServletResponse httpResponse) throws Exception {

		int imageType = 0;
		if (imageCategory.equalsIgnoreCase(ImageCategory.CATEGORY.getName())) {
			imageType = 2;
		} else if (imageCategory.equalsIgnoreCase(ImageCategory.BRAND.getName())) {
			imageType = 3;
		} else if (imageCategory.equalsIgnoreCase(ImageCategory.PRODUCT.getName())) {
			imageType = 1;
		}
		/*else if (imageCategory.equalsIgnoreCase(ImageCategory.SUBCATEGORY.getName())) {
			imageType = 7;
		}*/

		long fileStoreId = -1;
		if((ImageCategory.BRAND.getId() == imageType)){
			imageType = ImageCategory.SUBCATEGORY.getId();
			imageCategory = ImageCategory.SUBCATEGORY.getName();
			fileStoreId = photoMgmt.getFileStoreId(imageType, imageCategoryId,"v1", ServiceUtils.getLocale(language));
			if(fileStoreId == -1){
				imageType = ImageCategory.BRAND.getId();
				imageCategory = ImageCategory.BRAND.getName();
				fileStoreId = photoMgmt.getFileStoreId(imageType, imageCategoryId,"v1", ServiceUtils.getLocale(language));
			}
		}
		else if(ImageCategory.PRODUCT.getId() == imageType){
			/*imageType = ImageCategory.PRODUCTDETAIL.getId();
			Long photoId = photoMgmt.getPhotoIdOfproductDetailId(imageCategoryId);
			if(photoId == null || photoId == 0){
				fileStoreId = -1;
			}
			else{
				fileStoreId = photoMgmt.getFileStoreIdByPhotoId(photoId);
			}*/
			Long photoId = photoMgmt.getPhotoIdOfproductDetailId(imageCategoryId);
			if(photoId == null || photoId == 0){
				fileStoreId = -1;
			}
			else{
				PhotoDTO photoDTO  = photoMgmt.getFileStoreIdByPhotoId(photoId);
				if(photoDTO != null){
				String version = photoDTO.getVersion();
				if(version != null && version.equalsIgnoreCase("v2")){
					imageType = ImageCategory.PRODUCTDETAIL.getId();
					fileStoreId = photoDTO.getFileStoreId();
				}
				}
			}
			if(fileStoreId == -1){
				PhotoDTO photoDTO  = photoMgmt.getFileStoreIdByPhotoId(photoId);
				if(photoDTO != null && photoDTO.getItemId() != null){
				fileStoreId = photoMgmt.getFileStoreId(imageType, photoDTO.getItemId(),"v1", ServiceUtils.getLocale(language));
				}
			}
		}
		else{
			fileStoreId = photoMgmt.getFileStoreId(imageType, imageCategoryId,"v1", ServiceUtils.getLocale(language));
		}
		
		//TODO : keep this check until all images are not uploaded to production
		if (fileStoreId != -1) {
			try {
				if(imageType == ImageCategory.PRODUCT.getId()){
					Long photoId = photoMgmt.getPhotoIdOfproductDetailId(imageCategoryId);
					PhotoDTO photoDTO  = photoMgmt.getFileStoreIdByPhotoId(photoId);
					if(photoDTO != null && photoDTO.getItemId() != null){
					imageCategoryId = photoDTO.getItemId();
					}
				}
				RetrieveFileResponse response = photoMgmt.retrievePhoto(imageType, imageCategoryId, fileStoreId,
						ServiceUtils.getLocale(language));

				ByteSink byteSink = new ByteSink() {

					@Override
					public OutputStream openStream() throws IOException {
						return httpResponse.getOutputStream();
					}
				};

				httpResponse.setContentType(response.getContentType());
				httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
						CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
				response.getByteSource().copyTo(byteSink);
			} catch (Exception e) {
				e.printStackTrace();
				httpResponse.setHeader("Exception", ExceptionResourceBundle
						.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
				httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
			}
		} else {
			if(!imageCategory.equalsIgnoreCase(ImageCategory.PRODUCT.getName()) || 
					!imageCategory.equalsIgnoreCase(ImageCategory.SUBCATEGORY.getName())	){
			DownloadPhotoResponse response = new DownloadPhotoResponse();

			try {
				response = manager.getProductImage(imageCategory, imageCategoryId,
						ServiceUtils.getLocale(language));
				// byte[] content = response.getData().getBytes();
				if (response != null && response.getData() != null) {
					byte[] content = Base64.decode(response.getData());
					httpResponse.setContentType(response.getContentType());
					httpResponse.setContentLength(content.length);
					httpResponse.getOutputStream().write(content);
					// httpResponse.
				}
			} catch (FileNotFoundException e) {
				System.out.println("File Not Found.");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("Error Reading The File.");
				e.printStackTrace();
			} catch (Exception e) {
				System.out.println("Authenticaton Failed");
			}
		}
		}

	}

	@RequestMapping(value = "/upload/{imageCategory}/image/{imageCategoryId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponse savePhoto(@RequestHeader("authToken") String authToken,
			@PathVariable(value = "imageCategory") String imageCategory,
			@PathVariable(value = "imageCategoryId") Long imageCategoryId,
			@RequestParam(value = "photo", required = true) MultipartFile photo,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			StoreFile storeFile = toStoreFile(photo, ServiceUtils.getLocale(language));
			File file = multipartToFile(photo);
			response = manager.savePhoto(authToken, file, imageCategory, imageCategoryId, storeFile,
					ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/wallet/info", method = RequestMethod.GET)
	public @ResponseBody GetWalletResponse getWalletInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetWalletResponse response = new GetWalletResponse();
		try {
			response = manager.getWalletInfo(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}

	/*@RequestMapping(value = "/update/wallet/info", method = RequestMethod.PUT)
	public @ResponseBody GetWalletResponse updateWalletInfo(@RequestBody WalletInfo request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetWalletResponse response = new GetWalletResponse();
		try {
			response = manager.upsertWalletInfo(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}*/

	@RequestMapping(value = "search/products", method = RequestMethod.PUT)
	public @ResponseBody SearchProductListResponse searchProducts(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody SearchProductRequest request) {
		SearchProductListResponse response = new SearchProductListResponse();
		try {
			response = manager.searchProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	public StoreFile toStoreFile(MultipartFile file, Locale locale) throws ValidationException {
		if (file != null) {
			// String contentType = file.getContentType();
			// String fileName = file.getOriginalFilename();
			// long fileSize = file.getSize();
			String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			StoreFile storeFile = new StoreFile();
			if (extension.equalsIgnoreCase("gif") || extension.equalsIgnoreCase("png")
					|| extension.equalsIgnoreCase("bmp") || extension.equalsIgnoreCase("jpeg")
					|| extension.equalsIgnoreCase("jpg")) {
				storeFile.setContentType(file.getContentType());
				storeFile.setDocumentType(DocumentType.PHOTO.getName());
				// storeFile.setEncrypted(true);
				storeFile.setFileName(file.getOriginalFilename());
				storeFile.setFileSize(file.getSize());
				storeFile.setFileExtension(extension);
				return storeFile;
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));
			}

		}
		return null;
	}

	public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
		File convFile = new File(multipart.getOriginalFilename());
		multipart.transferTo(convFile);
		return convFile;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
