package com.maidin.customerService.utils;

import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maidin.pojo.common.ProjectObject;
import com.maidin.pojo.product.ProjectListRequest;

@RestController
@RequestMapping(value = "/jsonservice")
public class JSONService {

	@RequestMapping(value = "/getCityProjectTowerList", method = RequestMethod.GET)
	public JSONObject getCityProjectTowerList() {
		JSONObject response = new JSONObject();
		JSONObject responseJSON = new JSONObject();
		try {
			String current = System.getProperty("config.dir");
			String filepath = current + "/json/cityProjectTowerList.json";

			response = readJSONObject(filepath);

			// convert response JSON to class object
			List<ProjectObject> projectObjectList = getProjectClassObject(response);

			// sort class object by projectName
			Collections.sort(projectObjectList, new Comparator<ProjectObject>() {
				@Override
				public int compare(ProjectObject projectObject, ProjectObject projectObject2) {
					return projectObject.getProjectName().compareTo(projectObject2.getProjectName());
				}
			});

			// convert class object back to JSON object
			responseJSON = getJSONObject(projectObjectList, response);
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return responseJSON;
	}

	@RequestMapping(value = "/getProjectTowerFlatList", method = RequestMethod.GET)
	public JSONObject getProjectTowerFlatList() {
		JSONObject response = new JSONObject();
		try {
			String current = System.getProperty("config.dir");
			String filepath = current + "/json/projectTowerFlatList.json";

			response = readJSONObject(filepath);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

	@RequestMapping(value = "/getProjectTowerFlatsList", method = RequestMethod.PUT)
	public JSONObject getProjectTowerFlatList(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody ProjectListRequest request) {
		JSONObject response = new JSONObject();
		try {
			String current = System.getProperty("config.dir");
			String filepath = current + "/json/projectTowerFlatList.json";

			response = readFlatJSONObject(filepath, request.getProjectId(), request.getTowerId());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return response;
	}

	// Read JSON file
	public JSONArray readJSON(String filepath) {
		JSONParser parser = new JSONParser();
		JSONArray json = null;
		try {
			FileReader fileReader = new FileReader(filepath);
			json = (JSONArray) parser.parse(fileReader);

			/*System.out.println("json: " + json);*/

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return json;
	}

	// getting all list in one json
	@RequestMapping(value = "/getAllListsInJson", method = RequestMethod.GET)
	public JSONObject getLocationList() {
		JSONObject response = new JSONObject();
		try {
			String current = System.getProperty("config.dir");
			String filepath = current + "/json/allLists.json";

			response = readJSONObject(filepath);
		} catch (Exception e) {
		}
		return response;
	}

	// Read JSON Objectfile
	private JSONObject readFlatJSONObject(String filepath, String projectId, String towerId) {
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		JSONObject towerJSON = new JSONObject();
		try {
			FileReader fileReader = new FileReader(filepath);
			json = (JSONObject) parser.parse(fileReader);
			JSONObject projectsJSON = (JSONObject) json.get("projects");
			JSONObject projectJSON = (JSONObject) projectsJSON.get(projectId);
			towerJSON.put("flatList", (JSONArray) projectJSON.get(towerId));

			/*System.out.println("json: " + towerJSON);*/

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return towerJSON;
	}

	// Read JSON Objectfile
	private JSONObject readJSONObject(String filepath) {
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		try {
			FileReader fileReader = new FileReader(filepath);
			json = (JSONObject) parser.parse(fileReader);

			/*System.out.println("json: " + json);*/

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return json;
	}
	
	private List<ProjectObject> getProjectClassObject(JSONObject response) {
		Gson gson = new Gson();
		JSONArray projectJSON = (JSONArray) response.get("projects");
		String projectObjects = gson.toJson(projectJSON);

		Type listType = new TypeToken<List<ProjectObject>>() {
		}.getType();
		List<ProjectObject> projectObjectList = gson.fromJson(projectObjects, listType);
		return projectObjectList;
	}

	private JSONObject getJSONObject(List<ProjectObject> projectObjectList,JSONObject response) {
		JSONObject resultJSON = new JSONObject();
		resultJSON.put("cities", (JSONArray) response.get("cities"));
		resultJSON.put("projects", new JSONArray());
		JSONArray projectsArray = (JSONArray) resultJSON.get("projects");
		for (int i = 0; i < projectObjectList.size(); i++) {
			JSONObject projectObjectJSON = new JSONObject();
			projectObjectJSON.put("id", projectObjectList.get(i).getId());
			projectObjectJSON.put("cityCode", projectObjectList.get(i).getCityCode() + "");
			projectObjectJSON.put("active", projectObjectList.get(i).isActive());
			projectObjectJSON.put("projectName", projectObjectList.get(i).getProjectName());

			JSONArray towerArray = new JSONArray();

			for (int j = 0; j < projectObjectList.get(i).getTowers().size(); j++) {
				JSONObject towerObject = new JSONObject();

				towerObject.put("id", projectObjectList.get(i).getTowers().get(j).getId());
				towerObject.put("active", projectObjectList.get(i).getTowers().get(j).isActive());
				towerObject.put("towerName", projectObjectList.get(i).getTowers().get(j).getTowerName());

				towerArray.add(towerObject);
			}

			projectObjectJSON.put("towers", towerArray);
			projectsArray.add(projectObjectJSON);
		}
		return resultJSON;
	}
}
