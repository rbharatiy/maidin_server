package com.maidin.customerService.utils;

import java.util.Locale;
import com.google.common.base.Strings;

public class ServiceUtils {


	public static Locale getLocale(String language) {
		if (Strings.isNullOrEmpty(language)) {
			language = "en";
		}
		return new Locale(language);
	}
	 public String toTitleCase(String input) {
		    StringBuilder titleCase = new StringBuilder();
		    boolean nextTitleCase = true;

		    for (char c : input.toCharArray()) {
		        if (Character.isSpaceChar(c)) {
		            nextTitleCase = true;
		        } else if (nextTitleCase) {
		            c = Character.toTitleCase(c);
		            nextTitleCase = false;
		        }

		        titleCase.append(c);
		    }

		    return titleCase.toString();
		}
}
