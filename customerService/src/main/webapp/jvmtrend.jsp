<!DOCTYPE HTML>
<%@ page  language="java" import="java.io.*,java.text.*,java.util.*,java.net.*" %> 

<%

String user=(String)session.getAttribute("UserID");
		
        InetAddress inet = InetAddress.getLocalHost();
        String strHostAddr = inet.getHostAddress();
     
     String strHostName = inet.getHostName();
        if (strHostName != null) {
            if (strHostName.indexOf('.') > 0) {
                strHostName = strHostName.substring(0, (strHostName.indexOf('.')));
            }
        } else {
            strHostName = strHostAddr; 
        }
        
    
        long maxMem = (Runtime.getRuntime().maxMemory() / 1000) / 1024;
%>  
 
<HTML>
<HEAD>

  <STYLE>
     .header14pt {font-size: 14px;font-family: Arial, helvetica, sans-serif, verdana; font-weight:bold;color:#333333;} 
     .header12pt {font-size: 12px;font-family: Arial, helvetica, sans-serif, verdana; font-weight:normal;color:#333333;}
     .tableborder1px {border-width:3px; border-style:solid; border-color:#0099CC}
     th.tbheader {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold; color: #333333; background-color: #CEE7EA}
     td.tbcell {  font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; color: #333333; background-color: #FFFFFF}
  </STYLE>
  <TITLE><%= strHostName %> (<%= strHostAddr %>) JVM Trend</TITLE>
</HEAD>




<BODY onLoad="InitializeTimer()" >
  <form name="frm1" action="jvmtrend.jsp" method="post">
    <%
        Date dtStartTime = null;        
        String strStartTime = request.getParameter("starttime");       		
       
           dtStartTime = new Date();		   
		   strStartTime = dtStartTime.toString();
         
          
        String strInterval = "10";  
                 
         String strHiddenVar = request.getParameter("totstat");       
        if(strHiddenVar == null)
        {
          	strHiddenVar = "";       
        }   
    
        
        long freeMem = (Runtime.getRuntime().freeMemory() / 1000) / 1024;
        long allocMem = (Runtime.getRuntime().totalMemory() / 1000) / 1024;
        long usedMem = allocMem - freeMem; 
        
   	    double usageRatio = (double)usedMem/maxMem;
   	    
   	    String usagePercent = new DecimalFormat(".##").format(usageRatio * 100);
   	    
   	    strHiddenVar +=  maxMem +"," + allocMem +"," + freeMem +"," + usedMem + " (" + usagePercent + " %),"  + new Date() + "," + strHostName + "|";
	
	   
    %>       
    <input type="hidden" name="totstat" value="<%=strHiddenVar%>"> 
    <input type="hidden" name="interval" value="<%=strInterval%>">
	<input type="hidden" name="starttime" value="<%=strStartTime%>">
  </form>
  
  <table align="center" border="0" width="60%">
   <tr>
     <td>
         <p align="center" class="header14pt"> JVM Trend </p>
     </td>
        </tr> 
        <tr>
     <td>
         <p align="center" class="header14pt"> Server : <%= strHostName %> (<%= strHostAddr %>) </p>
     </td>
   </tr> 
   <tr>
     <td height="5">&nbsp;</td>
    </tr>    
   <tr>
      <td> 
         <p align="center" class="header12pt"> Started: <%=strStartTime%></p>
      </td>
   </tr>
  </table>
  <table align="center" border="0" width="60%">
  <tr>
     <td height="10">
         &nbsp;
     </td>
   </tr> 
   </table>
  
  <table align="center" class="tableborder1px">
  <tr>
   <th class="tbheader"> Max Avl Memory (MB) </th>
   <th class="tbheader"> Current <br>Alloc. Memory (MB) </th>
   <th class="tbheader"> Free Memory (MB) </th>
   <th class="tbheader"> Used Memory (MB) </th>
   <th class="tbheader"> Time Stamp </th>
   <th class="tbheader"> Server </th>
  </tr>
  <tr>   
  <%
    String strNextTokenRec ="";
    String strNextTokenField ="";
    StringTokenizer stRec = null;
    StringTokenizer stField = null; 
    if(!strHiddenVar.equalsIgnoreCase(""))
    {
      stRec = new StringTokenizer(strHiddenVar,"|");
      while(stRec.hasMoreTokens())
       {
        strNextTokenRec = stRec.nextToken();
  %>
     <tr>
       <%       
         stField = new StringTokenizer(strNextTokenRec,",");
         while(stField.hasMoreTokens())
          {
           strNextTokenField = stField.nextToken();
          %>
            <td class="tbcell"><%=strNextTokenField%></td>    
        <%}%>
     </tr>  
  <%  }
   }%>
 </table> 
</BODY>

<script>

var secs;
var timerID = null;
var timerRunning = false;
var delay = 1000;

function InitializeTimer()
{
    // Set the length of the timer, in seconds
    secs = <%=strInterval%>;
    StopTheClock();
    StartTheTimer();
}

function StopTheClock()
{
    if(timerRunning)
        clearTimeout(timerID);
    timerRunning = false;
}

function StartTheTimer()
{
    if (secs==0)
    {
        StopTheClock();
        document.frm1.submit();       
    }
    else
    {
        self.status = 'Time remaining: ' + secs + " seconds";
        secs = secs - 1;
        timerRunning = true;
        timerID = self.setTimeout("StartTheTimer()", delay);
    }
}

</SCRIPT>



</HTML>
