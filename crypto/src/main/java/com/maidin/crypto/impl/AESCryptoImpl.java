package com.maidin.crypto.impl;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.maidin.crypto.Crypto;

public class AESCryptoImpl implements Crypto {

	private final Cipher encrypt;
	private final Cipher decrypt;
	private final String passphrase = "x2K([q!R'^bz)zk$)rx=dY`'~AMj%rYma4yJ6.X.4f-}_p[5[3v'=<j%{ErQ^A<V@r-.N3DLry^{;A]n!vWU]rhMYhGp94'UA:Q%d-):FU,KfB?hM{p'8'@,4*5v:)'3jc;/]XXrsRZJ@(X4,t_'jK8c5LUg)As9BvAq)+_WvB,RSwsH;'mCQ5cyD?U*~Z(T}z&C>{T'L82V#WC(/#{yXRDmBck3Khh>B!AfGj=$paB)ND}cW22gT;xY@4f#&Nu)[fe'P_$9~R7L}kP@Nc'?3R(~`['g?4s<qa#~=RDcSR][)jBJ3=)VvsqUtWQkWy(/}d!*Pj34J5U}2h,[&(-F%<r{Ya'`s~RX;g)J_5$e`T{x8^barRDq@7{=yDRTt>c)]T)GY<U)z6'vU5]F_BR_!trCz)X/t&6rWgX<hAZ(#nr#(F5c42*+@dN9n;M2]{j]?j9>Jx[P_*`AjXgh(zF2b-.v:A!6/A+uL?&SJTDAx{tQKR~_q:Za?Dd;:nz-)fj^D4BpJXH'$JNaR4?_qw-FySdmzsj}gA]3fq).=)cEe!}5])v-E&k)'*q6C>hNEK4;J'<3UUM,FB.NYUmf'zjH]u=uC]64[`p=@W?s7p=dbSbw%9n)VYF!Mg7mFdCH@9{;YWbrH-QcA,^b]6A4)8-<4)/6cyx3jXpR-79@@9BgM+Z#(_-WM#U4b+[K7SZQyWF.'*`SmAxFTKa-6MsX*G7'+RsCWB2+qSH%`;hGFncJr<c`'er/F/}2~:NP@6zm8zWuE4)RqTJvGHj2?rM=j[)8$>fG#YrhUvL(!n&du'tDF5M$9@PP&)^!8*u5#~!'fAu'rX$*&EePh:[UVK?#W<]j>!;x6s3DK(3s'WU.=W&nRyx5J)Br]#3AwG(65k_)Hw`<PM8'!x*X3YX'kxTq-[k[L[{#Qwyzvc*~X.TF;xrzbL@Uz-/-;9BaUW@~_an?7r5S_X)WcF%DTLzXLuT([Ap)X>,+A_u]_{]6xRh>g]UX%,7NQ'}:PF~Ng}=JF${!-'/T-f@QWy!F.9'ybqc{F9*&x<q5B%s6}hRF~sMtp6H%^+Zu*-z*,6H}]K^[W7&tv<-3'~%K$a;.'7Yq9P8!RA@XX.3C,B}6U[;4~TBMA5p<j/A!>VtDsUH*~@(>w(79$*Fmy+M/F:>quz5w.:.=D';_~{d$Jap782)y}n9s[Z+D$?5/xk&9G~#ut:XuV*ra:c*k<jtY@24`2({j~psRct<j]W-^Ar#+/P&GS8kvcBM8-m)Pt:S<e&yCbCp_=}-*_[prh4J$7x${p,Mkx$JFJz(VmmsKDNM2'HgVhba]5U?T`K3VtVu$@c%xsVv``_(_npY~/sbW)d8g$q:5.7{s:{+Rq>q2P*k'jFB!yMM*c^8^`<q2UuuN2Tv/FT?G&P2(T?2AtXaBs%=tRepYstW,d>n>?jsqaj%'<3JxaHk]TK@VFY.KJ_89cVLjk`a;L}9GsDK?y7E;~C+Rzypry)7+KDHqU49r)~nTDa3uj}'ybWNx*Q]@!x<Drew)t+v9>Y7h?FF3*^.m{/WnJTbDe{<6qA&L:=RE@(x%{5FTAD>%!{jmJxL,)+n@9S]kEmC>jpwb8[GX24pUQ4nZcAw:n2VJ+?'B[m*pZD~mj!$*[QDsf(RF=#qJb,~m3z*y+2Z)Ah:;+bvwNaYLd~B6+'*zp6BR[W(@Y6M4b8Y?DXa({C48]sS+9Sh)%{h4~gUAaM!f7Em_#(E^5(5,y[f-M~J(;)yC=`HRE/zKCv~RuYsHQd/uaEC2:bz^pyTjp'_d_Rxc_F^hM$GxyUyJQ36M5Yd[/+u$)ymv-`r#LKxC}tQc}9szt(pVwyg}ccseDF)*L@p=}4u^gUPB?puM2MW9:{A}PnmqSY6'`*K);nFh2d%Sdt-k]cxYC.Mazu$a5?gzJbk!tD;LFw{qfXkgBe?HDG8[9ca[&;cNs)EL8s^SK%(#svx'_bea2@W*&q)vy_YF^gK$q'^-2'y9t!8ErF`a3](ZLpc&p)M$P'3kyKYK%UZr{wsN?W?n.yY,CK.mT8KK2<S)B9B%*Axf";
	
	private final String secretKeyAlgorithm = "AES";
	private final String transformationAlgorithm = "AES/CBC/PKCS5Padding";
	private final String secretKeySpecAlgorithm = "PBKDF2WithHmacSHA256";
	private final IvParameterSpec paramSpec; 
	
	// 16-byte Salt
	private static final byte[] salt = {
			(byte) 0xB2, (byte) 0x12, (byte) 0xD5, (byte) 0xB2,
			(byte) 0x44, (byte) 0x21, (byte) 0xC3, (byte) 0xC3,
			(byte) 0xC4, (byte) 0x15, (byte) 0x25, (byte) 0xB2,
			(byte) 0x34, (byte) 0x51, (byte) 0xA2, (byte) 0xA3
	};

	
	public AESCryptoImpl() {
		try {
			paramSpec = new IvParameterSpec(salt);
			SecretKey secretkey = generateKey(passphrase);

			encrypt = Cipher.getInstance(transformationAlgorithm);
            decrypt = Cipher.getInstance(transformationAlgorithm);

			// initialize the ciphers with the given key
			encrypt.init(Cipher.ENCRYPT_MODE, secretkey, paramSpec);
			decrypt.init(Cipher.DECRYPT_MODE, secretkey, paramSpec);
		} catch (Exception e) {
			//catastrophic error
			e.printStackTrace();
			throw new RuntimeException("Could not initialize encyption and decryption cipher");
		}		
	}
	
	@Override
	public byte[] encrypt(byte[] clearText, String password) throws Exception {
		SecretKey secretkey = generateKey(password);
		Cipher localEncrypt = Cipher.getInstance(transformationAlgorithm);
		localEncrypt.init(Cipher.ENCRYPT_MODE, secretkey, paramSpec);
		return localEncrypt.doFinal(clearText);		
	}
	
	@Override
	public byte[] decrypt(byte[] decryptedText, String password) throws Exception {
		SecretKey secretkey = generateKey(password);
		Cipher localDecrypt = Cipher.getInstance(transformationAlgorithm);
		localDecrypt.init(Cipher.DECRYPT_MODE, secretkey, paramSpec);
		return localDecrypt.doFinal(decryptedText);		
	}
	
	@Override
	public byte[] encrypt(byte[] clearText) throws Exception {
		return encrypt.doFinal(clearText);		
	}

	@Override
	public byte[] decrypt(byte[] decryptedText) throws Exception {
		return decrypt.doFinal(decryptedText);		
	}
	
	private SecretKey generateKey(String password) throws NoSuchAlgorithmException, InvalidKeySpecException, UnsupportedEncodingException {
		SecretKeyFactory factory = SecretKeyFactory.getInstance(secretKeySpecAlgorithm);
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
		SecretKey tmp = factory.generateSecret(spec);
		SecretKey secretKey = new SecretKeySpec(tmp.getEncoded(), secretKeyAlgorithm);
		return secretKey;
	}
	
	public static void main(String args[]) throws Exception {
		String textToCrypt = "My name is anthony gonzalvis";
		AESCryptoImpl crypto = new AESCryptoImpl();
		System.out.println(new String(crypto.decrypt(crypto.encrypt(textToCrypt.getBytes()))));
		System.out.println(new String(crypto.decrypt(crypto.encrypt(textToCrypt.getBytes(), "my secret key"), "my secret key")));
	}
}
