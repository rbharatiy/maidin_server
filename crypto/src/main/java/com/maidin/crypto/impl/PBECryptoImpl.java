package com.maidin.crypto.impl;

import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.maidin.crypto.Crypto;

public class PBECryptoImpl implements Crypto {
	
	private Cipher encrypt;
	private Cipher decrypt;
	private final int iterationCount = 1024;
	private final String passPhrase = "x2K([q!R'^bz)zk$)rx=dY`'~AMj%rYma4yJ6.X.4f-}_p[5[3v'=<j%{ErQ^A<V@r-.N3DLry^{;A]n!vWU]rhMYhGp94'UA:Q%d-):FU,KfB?hM{p'8'@,4*5v:)'3jc;/]XXrsRZJ@(X4,t_'jK8c5LUg)As9BvAq)+_WvB,RSwsH;'mCQ5cyD?U*~Z(T}z&C>{T'L82V#WC(/#{yXRDmBck3Khh>B!AfGj=$paB)ND}cW22gT;xY@4f#&Nu)[fe'P_$9~R7L}kP@Nc'?3R(~`['g?4s<qa#~=RDcSR][)jBJ3=)VvsqUtWQkWy(/}d!*Pj34J5U}2h,[&(-F%<r{Ya'`s~RX;g)J_5$e`T{x8^barRDq@7{=yDRTt>c)]T)GY<U)z6'vU5]F_BR_!trCz)X/t&6rWgX<hAZ(#nr#(F5c42*+@dN9n;M2]{j]?j9>Jx[P_*`AjXgh(zF2b-.v:A!6/A+uL?&SJTDAx{tQKR~_q:Za?Dd;:nz-)fj^D4BpJXH'$JNaR4?_qw-FySdmzsj}gA]3fq).=)cEe!}5])v-E&k)'*q6C>hNEK4;J'<3UUM,FB.NYUmf'zjH]u=uC]64[`p=@W?s7p=dbSbw%9n)VYF!Mg7mFdCH@9{;YWbrH-QcA,^b]6A4)8-<4)/6cyx3jXpR-79@@9BgM+Z#(_-WM#U4b+[K7SZQyWF.'*`SmAxFTKa-6MsX*G7'+RsCWB2+qSH%`;hGFncJr<c`'er/F/}2~:NP@6zm8zWuE4)RqTJvGHj2?rM=j[)8$>fG#YrhUvL(!n&du'tDF5M$9@PP&)^!8*u5#~!'fAu'rX$*&EePh:[UVK?#W<]j>!;x6s3DK(3s'WU.=W&nRyx5J)Br]#3AwG(65k_)Hw`<PM8'!x*X3YX'kxTq-[k[L[{#Qwyzvc*~X.TF;xrzbL@Uz-/-;9BaUW@~_an?7r5S_X)WcF%DTLzXLuT([Ap)X>,+A_u]_{]6xRh>g]UX%,7NQ'}:PF~Ng}=JF${!-'/T-f@QWy!F.9'ybqc{F9*&x<q5B%s6}hRF~sMtp6H%^+Zu*-z*,6H}]K^[W7&tv<-3'~%K$a;.'7Yq9P8!RA@XX.3C,B}6U[;4~TBMA5p<j/A!>VtDsUH*~@(>w(79$*Fmy+M/F:>quz5w.:.=D';_~{d$Jap782)y}n9s[Z+D$?5/xk&9G~#ut:XuV*ra:c*k<jtY@24`2({j~psRct<j]W-^Ar#+/P&GS8kvcBM8-m)Pt:S<e&yCbCp_=}-*_[prh4J$7x${p,Mkx$JFJz(VmmsKDNM2'HgVhba]5U?T`K3VtVu$@c%xsVv``_(_npY~/sbW)d8g$q:5.7{s:{+Rq>q2P*k'jFB!yMM*c^8^`<q2UuuN2Tv/FT?G&P2(T?2AtXaBs%=tRepYstW,d>n>?jsqaj%'<3JxaHk]TK@VFY.KJ_89cVLjk`a;L}9GsDK?y7E;~C+Rzypry)7+KDHqU49r)~nTDa3uj}'ybWNx*Q]@!x<Drew)t+v9>Y7h?FF3*^.m{/WnJTbDe{<6qA&L:=RE@(x%{5FTAD>%!{jmJxL,)+n@9S]kEmC>jpwb8[GX24pUQ4nZcAw:n2VJ+?'B[m*pZD~mj!$*[QDsf(RF=#qJb,~m3z*y+2Z)Ah:;+bvwNaYLd~B6+'*zp6BR[W(@Y6M4b8Y?DXa({C48]sS+9Sh)%{h4~gUAaM!f7Em_#(E^5(5,y[f-M~J(;)yC=`HRE/zKCv~RuYsHQd/uaEC2:bz^pyTjp'_d_Rxc_F^hM$GxyUyJQ36M5Yd[/+u$)ymv-`r#LKxC}tQc}9szt(pVwyg}ccseDF)*L@p=}4u^gUPB?puM2MW9:{A}PnmqSY6'`*K);nFh2d%Sdt-k]cxYC.Mazu$a5?gzJbk!tD;LFw{qfXkgBe?HDG8[9ca[&;cNs)EL8s^SK%(#svx'_bea2@W*&q)vy_YF^gK$q'^-2'y9t!8ErF`a3](ZLpc&p)M$P'3kyKYK%UZr{wsN?W?n.yY,CK.mT8KK2<S)B9B%*Axf";
	private final String algorithm = "PBEWithMD5AndDES";

	// 8-byte Salt
	private static final byte[] salt = { (byte) 0xB2, (byte) 0x12, (byte) 0xD5, (byte) 0xB2, (byte) 0x44, (byte) 0x21,
			(byte) 0xC3, (byte) 0xC3 };

	public PBECryptoImpl() {
		try {
			// create a user-chosen password that can be used with
			// password-based encryption(PBE)
			// provide password, salt, iteration count for generating PBEKey of
			// fixed-key-size PBE ciphers
			KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);

			// create a secret (symmetric) key using PBE with MD5 and DES
			SecretKey key = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);

			// construct a parameter set for password-based encryption as
			// defined in the PKCS #5 standard
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			System.out.println(key.getAlgorithm());
			encrypt = Cipher.getInstance(key.getAlgorithm());
			decrypt = Cipher.getInstance(key.getAlgorithm());

			// initialize the ciphers with the given key
			encrypt.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			decrypt.init(Cipher.DECRYPT_MODE, key, paramSpec);
		} catch (Exception e) {
			// catastrophic error
			e.printStackTrace();
			throw new RuntimeException("Could not initialize encyption and decryption cipher");
		}
	}

	@Override
	public byte[] encrypt(byte[] clearText) throws Exception {
		return encrypt.doFinal(clearText);
	}

	@Override
	public byte[] decrypt(byte[] decryptedText) throws Exception {
		return decrypt.doFinal(decryptedText);
	}

	@Override
	public byte[] encrypt(byte[] clearText, String key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] decrypt(byte[] decryptedText, String key) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String args[]) throws Exception {
		String textToCrypt = "My name is anthony gonzalvis";
		PBECryptoImpl crypto = new PBECryptoImpl();
		System.out.println(new String(crypto.decrypt(crypto.encrypt(textToCrypt.getBytes()))));
	}
}
