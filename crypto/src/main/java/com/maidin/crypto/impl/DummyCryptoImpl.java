package com.maidin.crypto.impl;

import com.maidin.crypto.Crypto;

public class DummyCryptoImpl implements Crypto {

	@Override
	public byte[] encrypt(byte[] clearText) throws Exception {
		return clearText;
	}

	@Override
	public byte[] decrypt(byte[] decryptedText) throws Exception {
		return decryptedText;
	}

	@Override
	public byte[] encrypt(byte[] clearText, String key) throws Exception {
		return clearText;
	}

	@Override
	public byte[] decrypt(byte[] decryptedText, String key) throws Exception {
		return decryptedText;
	}
}
