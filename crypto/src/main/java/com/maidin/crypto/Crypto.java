package com.maidin.crypto;

public interface Crypto {

	byte[] encrypt(byte[] clearText) throws Exception;

	byte[] decrypt(byte[] decryptedText) throws Exception;

	byte[] encrypt(byte[] clearText, String key) throws Exception;

	byte[] decrypt(byte[] decryptedText, String key) throws Exception;

}
