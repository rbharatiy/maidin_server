package com.maidin.customerApi.V2;

import java.util.Locale;

import com.maidin.pojo.customerv2.PersonalInfoResponse;

public interface CustomerMgmt {

	
	public PersonalInfoResponse getPersonalInfo(String authToken, Locale locale) throws Exception;

	/*public GenericResponse setPersonalInfo(CustomerPersonalInfo request, String authToken, Locale locale) throws Exception;*/

	
}
