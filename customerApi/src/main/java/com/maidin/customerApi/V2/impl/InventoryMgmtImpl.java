package com.maidin.customerApi.V2.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.V2.InventoryMgmt;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.customerAuthenticate.exception.AuthenticationException;
//import com.maidin.persistence.V2.dto.CityCategoryInfoV2DTO;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.CustomerAuthenticationDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.FileStoreDAO;
import com.maidin.persistence.dao.ImageDAO;
import com.maidin.persistence.dao.OTPVerificationDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ErrorInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.UpdateInfoDAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.FileStoreDTO;
import com.maidin.persistence.dto.ImageDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;
import com.maidin.persistence.dto.V2.ErrorInfoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.UpdateInfoDTO;
import com.maidin.pojo.inventory.CategoryListV2Response;
import com.maidin.pojo.inventory.ErrorCodeInfo;
import com.maidin.pojo.inventory.GetProductListV2Request;
import com.maidin.pojo.inventory.GetSubCategoryV2Request;
import com.maidin.pojo.inventory.PaymentOption;
import com.maidin.pojo.inventory.PaymentOptionInfo;
import com.maidin.pojo.inventory.PaymentOptionResponse;
import com.maidin.pojo.inventory.ProductCategoryV2;
import com.maidin.pojo.inventory.ProductInfo;
import com.maidin.pojo.inventory.ProductInfoV2;
import com.maidin.pojo.inventory.ProductListV2Response;
import com.maidin.pojo.inventory.ProductStatusInfo;
import com.maidin.pojo.inventory.ProductStatusRequest;
import com.maidin.pojo.inventory.ProductStatusResponse;
import com.maidin.pojo.inventory.QuantityUnitInfo;
import com.maidin.pojo.inventory.SubCategoryListV2Response;
import com.maidin.pojo.inventory.SubCategoryV2;
import com.maidin.pojo.inventory.TimeSlotInfoV2;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.photo.PhotoAllAttrs;

import javassist.bytecode.LineNumberAttribute.Pc;

public class InventoryMgmtImpl implements InventoryMgmt {

	private final ProductCategoryDAO productCategoryDAO;
	private final ProductBrandsDAO productBrandsDAO;
	private final ProductDAO productDAO;
	private final OTPVerificationDAO otpVerificationDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final CustomerAuthenticationDAO customerAuthenticationDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private final CommonApplication commonApplication;
	private final ImageDAO productImageDAO;
	private final WalletDAO walletDAO;
	private final AddressDAO addressDAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;
	private final PhotoDAO photoDAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final ImageDAO imageDAO;
	private final FileStoreDAO fileStoreDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final UpdateInfoDAO updateInfoDAO;
	private final PaymentModeDAO paymentModeDAO;
	private final ErrorInfoDAO errorInfoDAO;

	@Autowired
	public InventoryMgmtImpl(ProductCategoryDAO productCategoryDAO, ProductBrandsDAO productSubCategoryDAO,
			ProductDAO productDAO, OTPVerificationDAO otpVerificationDAO, AuthenticationMgmt authenticationMgmt,
			CustomerDAO customerDAO, CustomerAuthenticationDAO customerAuthenticationDAO,
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO, CommonApplication commonApplication,
			ImageDAO productImageDAO, WalletDAO walletDAO, AddressDAO addressDAO,
			CityCategoryInfoDAO cityCategoryInfoDAO, PhotoDAO photoDAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ProductInfoV2DAO productInfoV2DAO, BrandInfoV2DAO brandInfoV2DAO,
			CategoryInfoV2DAO categoryInfoV2DAO, ImageDAO imageDAO, FileStoreDAO fileStoreDAO, TimeSlotDAO timeSlotDAO,
			UpdateInfoDAO updateInfoDAO, PaymentModeDAO paymentModeDAO, ErrorInfoDAO errorInfoDAO) {

		this.productCategoryDAO = productCategoryDAO;
		this.productBrandsDAO = productSubCategoryDAO;
		this.productDAO = productDAO;
		this.otpVerificationDAO = otpVerificationDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.customerAuthenticationDAO = customerAuthenticationDAO;
		this.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
		this.commonApplication = commonApplication;
		this.productImageDAO = productImageDAO;
		this.walletDAO = walletDAO;
		this.addressDAO = addressDAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
		this.photoDAO = photoDAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.imageDAO = imageDAO;
		this.fileStoreDAO = fileStoreDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.updateInfoDAO = updateInfoDAO;
		this.paymentModeDAO = paymentModeDAO;
		this.errorInfoDAO = errorInfoDAO;

	}

	private static final Logger logger = LoggerFactory.getLogger(InventoryMgmtImpl.class);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListV2Response getCategoryListV2(String authToken, Locale locale) throws Exception {
		CategoryListV2Response response = new CategoryListV2Response();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD, customerDTO.getId());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(addressDTO.getCityCode());
		if (cityCategoryDTO != null) {
			String categoryIds = cityCategoryDTO.getCategoryIds();
			String[] categoryIdStrArr = categoryIds.split(",");
			int[] categoryIdIntArr = Stream.of(categoryIdStrArr).mapToInt(Integer::parseInt).toArray();
			List<Integer> categoryIdList = Arrays.stream(categoryIdIntArr).boxed().collect(Collectors.toList());
			List<CategoryInfoV2DTO> categoryList = categoryInfoV2DAO.getByIds(categoryIdList);
			for (CategoryInfoV2DTO pCategoryDTO : categoryList) {
				ProductCategoryV2 pcCategory = new ProductCategoryV2();
				pcCategory.setCategoryId(pCategoryDTO.getId());
				pcCategory.setProductCategory(pCategoryDTO.getCategory());
				pcCategory.setDescription(pCategoryDTO.getDescription());
				pcCategory.setActive(pCategoryDTO.isActive());
				pcCategory.setStockStatus(pCategoryDTO.isStockStatus());
				pcCategory.setPhotoId(pCategoryDTO.getPhotoId() == null ? 0 : pCategoryDTO.getPhotoId());
				if (pCategoryDTO.getPhotoId() != null) {
					PhotoDTO photoDTO = photoDAO.getById(pCategoryDTO.getPhotoId());
					if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
						pcCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
					}
				}

				///////////////////// normal slots//////////////////////////////
				String timeSlotString = pCategoryDTO.getTimeSlotIds();
				List<Integer> timeSlotIdList = new ArrayList<>();
				if (!Strings.isNullOrEmpty(timeSlotString)) {
					String[] timeSlotStringArr = timeSlotString.split(",");
					int[] timeSlotIntArr = Stream.of(timeSlotStringArr).mapToInt(Integer::parseInt).toArray();
					timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				}
				pcCategory.getTimeSlotIdList().addAll(timeSlotIdList);

				///////////////// todays's slots////////////////////////////
				timeSlotString = pCategoryDTO.getTodaySlotIds();
				timeSlotIdList = new ArrayList<>();
				if (!Strings.isNullOrEmpty(timeSlotString)) {
					String[] timeSlotStringArr = timeSlotString.split(",");
					int[] timeSlotIntArr = Stream.of(timeSlotStringArr).mapToInt(Integer::parseInt).toArray();
					timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				}
				pcCategory.getTodaysSlotIdList().addAll(timeSlotIdList);

				/*
				 * PhotoDTO photoDTO =
				 * photoDAO.getPhotoByPhotoTypeAndPhotoId(ImageCategory.CATEGORY
				 * .getId(), pCategoryDTO.getId()); if (photoDTO != null &&
				 * photoDTO.getUpdatedOn() != null) {
				 * pcCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().
				 * getTime()); } else { ImageDTO imageDTO =
				 * productImageDAO.getByProductId(ImageCategory.CATEGORY.getName
				 * (), pCategoryDTO.getId().longValue()); if (imageDTO != null
				 * && imageDTO.getUpdatedOn() != null) {
				 * pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().
				 * getTime()); } }
				 */
				response.getCategoryList().add(pcCategory);
			}
		}
		UpdateInfoDTO updateInfoDTO = updateInfoDAO.getByText("TimeSlot");
		response.setTimeSlotUpdatedTime(updateInfoDTO.getUpdatedOn().getTime());
		UpdateInfoDTO updateInfoDTO1 = updateInfoDAO.getByText("PaymentMode");
		response.setPaymentModeUpdatedTime(updateInfoDTO1.getUpdatedOn().getTime());
		
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SubCategoryListV2Response getSubCategoryListV2(String authToken, GetSubCategoryV2Request request,
			Locale locale) throws Exception {
		SubCategoryListV2Response response = new SubCategoryListV2Response();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(addressDTO.getCityCode());
		if (cityCategoryDTO != null) {
			String subCategoryIds = cityCategoryDTO.getSubCategoryIds();
			// 1-1,2;2-1,3;
			String[] catSubCatStrSeq = subCategoryIds.split(";");
			for (String catSubCatStr : catSubCatStrSeq) {
				String[] catSubCatStrArr = catSubCatStr.split("-");
				System.out.println(catSubCatStrArr[0]);
				Integer categoryId = Integer.parseInt(catSubCatStrArr[0]);
				if (categoryId.equals(request.getProductCategoryId())) {
					String[] subCategoryIdArr = catSubCatStrArr[1].toString().split(",");
					int[] subCategoryIdIntArr = Stream.of(subCategoryIdArr).mapToInt(Integer::parseInt).toArray();
					List<Integer> subCategoryIdList = Arrays.stream(subCategoryIdIntArr).boxed()
							.collect(Collectors.toList());
					List<SubCategoryInfoV2DTO> pdto = subCategoryInfoV2DAO.getAllSubCategories(subCategoryIdList);
					if (pdto == null) {

					} else {
						for (SubCategoryInfoV2DTO dto : pdto) {
							SubCategoryV2 subCategory = new SubCategoryV2();
							subCategory.setSubCategoryId(dto.getId());
							if (dto.getSubCategory().equals("All " + categoryId)) {
								subCategory.setSubCategory("All");
							} else {
								subCategory.setSubCategory(dto.getSubCategory());
							}
							subCategory.setActive(dto.isActive());
							response.getSubCategoryList().add(subCategory);
						}
					}

					List<SubCategoryInfoV2DTO> subCategoryDTOList = subCategoryInfoV2DAO
							.getAllSubCategories(subCategoryIdList);
					SubCategoryInfoV2DTO subCatDTO = subCategoryDTOList.get(0);
					ProductInfo productInfo = new ProductInfo();
					if (subCatDTO != null) {
						String cityCode = addressDTO.getCityCode();
						/*if (request.getCityCode() == null) {
							cityCode = addressDTO.getCityCode();
						} else {
							cityCode = request.getCityCode();
						}*/
						if (subCatDTO.getSubCategory().equals("All " + categoryId)) {
							productInfo = getCategoryProductList(categoryId, cityCode, 0);
						} else {
							productInfo = getSubCategoryProductList(categoryId, subCatDTO.getId(), cityCode, 0);
						}
					}
					response.getFirstSubCategoryList().addAll(productInfo.getProductList());
					response.setTotalProductCount(productInfo.getTotalProductCount());

				}
			}

		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ProductListV2Response getProductListV2(String authToken, GetProductListV2Request request,
			Integer currentPageNumber, Locale locale) throws Exception {
		ProductListV2Response response = new ProductListV2Response();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId() , ApiUtils.toJsonString(request));
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		ProductInfo productInfo = new ProductInfo();
		// List<ProductInfoV2> productList = new ArrayList<>();
		if (addressDTO != null) {
			String cityCode = addressDTO.getCityCode();
			// TODO: commented since app is release already and Android is sending 100001 for all. Uncomment for later
			/*if (Strings.isNullOrEmpty(request.getCityCode())) {
				cityCode = addressDTO.getCityCode();
			} else {
				cityCode = request.getCityCode();
			}*/
			int elementsPerPage = this.elementsPerPage;
			int firstResult = (currentPageNumber - 1) * elementsPerPage;
			SubCategoryInfoV2DTO subCatDTO = subCategoryInfoV2DAO.getById(request.getSubCategoryId());
			if (subCatDTO != null) {

				if (subCatDTO.getSubCategory().equals("All " + request.getCategoryId())) {
					productInfo = getCategoryProductList(request.getCategoryId(), cityCode, firstResult);
				} else {
					productInfo = getSubCategoryProductList(request.getCategoryId(), subCatDTO.getId(), cityCode,
							firstResult);
				}
			}
		}
		response.getProductList().addAll(productInfo.getProductList());
		response.setTotalProductCount(productInfo.getTotalProductCount());
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,  customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public PaymentOptionResponse getDashboardList(String authToken, Locale locale) throws Exception {
		PaymentOptionResponse response = new PaymentOptionResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		if(customerDTO != null){
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId());
		}
		List<PaymentOptionInfo> projectObjectList = getPaymentModeList();
		response.getPaymentOptionList().addAll(projectObjectList);

		// get TimeSlot List
		List<TimeSlotInfoV2> timeSlotV2List = getTimeSlotList();
		response.getTimeSlotList().addAll(timeSlotV2List);
		
		List<ErrorCodeInfo> errorCodeList = getErrorCodeList();
		response.getErrorDescriptionList().addAll(errorCodeList);
		
		response.setIsPaymentHistoryEnabled(Boolean.parseBoolean(prop.getProperty(ResourceProperties.IS_EMAIL_INVOICE_TO_CUSTOMER_ENABLED)));
		response.setIsPaymentHistoryEnabled(Boolean.parseBoolean(prop.getProperty(ResourceProperties.IS_PAYMENTHISTORY_CUSTOMER_ENABLED)));

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	
	private List<ErrorCodeInfo> getErrorCodeList() {
		List<ErrorCodeInfo> list = new ArrayList<>();
		//TODO :
		List<ErrorInfoDTO> errorInfoDTOList = errorInfoDAO.getAll();
		for(ErrorInfoDTO errorDTO :errorInfoDTOList){
			ErrorCodeInfo errorInfo  = new ErrorCodeInfo();
			errorInfo.setErrorCode(errorDTO.getErrorCode());
			errorInfo.setMessage(errorDTO.getMessage());
			list.add(errorInfo);
		}
		
		return list;
	}

	private List<PaymentOptionInfo> getPaymentModeList() {

		List<PaymentOptionInfo> paymentOptionList = new ArrayList<>();
		List<PaymentModeDTO> paymentModeList = paymentModeDAO.getAllPaymentMode(true, true);
		for (PaymentModeDTO paymentModeDTO : paymentModeList) {
			PaymentOptionInfo paymentOptionInfo = new PaymentOptionInfo();
			paymentOptionInfo.setId(paymentModeDTO.getId());
			paymentOptionInfo.setPaymentMode(paymentModeDTO.getPaymentOption());
			if (paymentModeDTO.getPhotoId() != null) {
				paymentOptionInfo.setPhotoId(paymentModeDTO.getPhotoId());
			}
			paymentOptionList.add(paymentOptionInfo);
		}
		return paymentOptionList;

	}

	private List<TimeSlotInfoV2> getTimeSlotList() {
		List<TimeSlotInfoV2> timeSlotInfoList = new ArrayList<>();
		List<TimeSlotDTO> timeSlotList = timeSlotDAO.getAllAppTimeSlots(true);
		for (TimeSlotDTO timeSlotDTO : timeSlotList) {
			TimeSlotInfoV2 timeSlotInfoV2 = new TimeSlotInfoV2();
			timeSlotInfoV2.setTimeSlotId(timeSlotDTO.getId());
			timeSlotInfoV2.setTimeSlotText(timeSlotDTO.getTimeSlot());
			timeSlotInfoV2.setStartTime(timeSlotDTO.getStartTime());
			timeSlotInfoV2.setEndTime(timeSlotDTO.getEndTime());
			timeSlotInfoV2.setLockingHours(timeSlotDTO.getLockingHours());
			timeSlotInfoList.add(timeSlotInfoV2);
		}
		return timeSlotInfoList;
	}

	private List<PaymentOptionInfo> getPaymentOptionsList(JSONObject jsonObject) {
		List<PaymentOptionInfo> paymentModeList = new ArrayList<>();
		Gson gson = new Gson();
		JSONArray paymentModeJSON = (JSONArray) jsonObject.get("paymentModeList");
		String paymentOption = gson.toJson(paymentModeJSON);
		Type listType = new TypeToken<List<PaymentOption>>() {
		}.getType();
		List<PaymentOption> projectObjectList = gson.fromJson(paymentOption, listType);
		projectObjectList = projectObjectList.stream().filter(e -> e.isDisplayedInApp() == true)
				.sorted(Comparator.comparing(PaymentOption::getPriority)).collect(Collectors.toList());
		for (PaymentOption paymentDetail : projectObjectList) {
			PaymentOptionInfo paymentOptionInfo = new PaymentOptionInfo();
			paymentOptionInfo.setId(paymentDetail.getId());
			paymentOptionInfo.setPaymentMode(paymentDetail.getPaymentOption());
			ImageDTO imageDTO = imageDAO.getByProductId(ImageCategory.PAYMENTOPTION.getName(),
					new Long(paymentDetail.getId()));
			if (imageDTO != null) {
				/*
				 * paymentOptionInfo.setImageId(imageDTO.getId());
				 * paymentOptionInfo.setImageUpdatedOn(imageDTO.getUpdatedOn().
				 * getTime());
				 */
			}
			paymentModeList.add(paymentOptionInfo);
		}
		return paymentModeList;
	}

	private JSONObject readJSONObject(String filepath) throws JSONException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = null;
		FileReader fileReader = new FileReader(filepath);
		json = (JSONObject) parser.parse(fileReader);
		return json;
	}

	private ProductInfo getSubCategoryProductList(int categoryInDB, int subCategoryId, String cityCode,
			int firstResult) {
		ProductInfo response = new ProductInfo();
		// List<ProductInfoV2> productList = new ArrayList<>();
		List<ProductCityInfoV2DTO> productCityInfoDTOList = productCityInfoV2DAO.getByCityCodeAndSubCategoryId(cityCode,
				subCategoryId, true);
		// response.setTotalProductCount(productCityInfoDTOList.size());

		List<Integer> productIdList = productCityInfoDTOList.stream().map(ProductCityInfoV2DTO::getProductId).distinct()
				.collect(Collectors.toList());
		if (productIdList.size() > 0) {
			List<Integer> activeProductIds = productInfoV2DAO.getByIds(productIdList);
			response.setTotalProductCount(activeProductIds.size());
			List<ProductInfoV2DTO> productInfoList = new ArrayList<>();
			if (activeProductIds != null && !activeProductIds.isEmpty() && activeProductIds.size() != 0) {
				productInfoList = productInfoV2DAO.getByProductIds(activeProductIds, firstResult, elementsPerPage);
			}
			List<Integer> brandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
					.collect(Collectors.toList());
			List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
			if (brandIdList != null && !brandIdList.isEmpty() && brandIdList.size() != 0) {
				brandInfoList = brandInfoV2DAO.getByBrandIds(brandIdList);
			}
			for (ProductInfoV2DTO prV2dto : productInfoList) {
				ProductInfoV2 pInfoV2 = new ProductInfoV2();
				pInfoV2.setProductId(prV2dto.getId());
				pInfoV2.setProductName(productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
						.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
				pInfoV2.setFoodType(prV2dto.getFoodType());
				pInfoV2.setBrandId(prV2dto.getBrandId());
				pInfoV2.setBrandName(brandInfoList.stream().filter(e -> e.getId().equals(prV2dto.getBrandId()))
						.map(BrandInfoV2DTO::getBrand).findFirst().orElse(""));
				List<ProductCityInfoV2DTO> productDetailsList = productCityInfoDTOList.stream()
						.filter(e -> e.getProductId().equals(prV2dto.getId())).collect(Collectors.toList());
				for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
					QuantityUnitInfo quInfo = new QuantityUnitInfo();
					quInfo.setProductDetailsId(pCityInfoV2DTO.getId());
					quInfo.setSetByDefault(pCityInfoV2DTO.isSetByDefault());
					quInfo.setMRP(pCityInfoV2DTO.getMRP());
					quInfo.setSalePrice(pCityInfoV2DTO.getSalePrice());
					quInfo.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
					if(pCityInfoV2DTO.getDiscountUnit() == null){
						pCityInfoV2DTO.setDiscountUnit("%");
					}
					quInfo.setDiscountUnit(pCityInfoV2DTO.getDiscountUnit());
					if (pCityInfoV2DTO.getDiscountUnit().equals("%")) {
						int discount = Math
								.round((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
										/ pCityInfoV2DTO.getMRP()));
						if (discount == 0 && ((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0)) {
							quInfo.setDiscountUnit("₹");
							quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
						} else {
							quInfo.setDiscount((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
									/ pCityInfoV2DTO.getMRP()));
						}
					} else if (pCityInfoV2DTO.getDiscountUnit().equals("₹")) {
						quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
					}
					quInfo.setSavedAmount((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0
							? pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice() : 0);
					quInfo.setDeliveryCharge(pCityInfoV2DTO.getDeliveryCharge());
					quInfo.setActive(pCityInfoV2DTO.isActive());
					if (prV2dto.isStockStatus()) {
						quInfo.setStockStatus(pCityInfoV2DTO.isStockStatus());
					} else {
						quInfo.setStockStatus(prV2dto.isStockStatus());
					}
					if (pCityInfoV2DTO.getPhotoId() != null) {
						quInfo.setPhotoId(pCityInfoV2DTO.getPhotoId() == null ? 0 : pCityInfoV2DTO.getPhotoId());
						PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
						if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
							quInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
						}
					}
					pInfoV2.setSubCategoryId(pCityInfoV2DTO.getSubCategoryId());
					pInfoV2.getProductDetailList().add(quInfo);
				}
				response.getProductList().add(pInfoV2);
			}
		}
		return response;
	}

	private ProductInfo getCategoryProductList(int categoryInDB, String cityCode, int firstResult) {
		ProductInfo response = new ProductInfo();
		// List<ProductInfoV2> productList = new ArrayList<>();
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(cityCode);
		if (cityCategoryDTO != null) {
			String subCategoryIds = cityCategoryDTO.getSubCategoryIds();
			// 1-1,2;2-1,3;
			String[] catSubCatStrSeq = subCategoryIds.split(";");
			// 1-1,2;
			for (String catSubCatString : catSubCatStrSeq) {
				String[] catSubCatStrArr = catSubCatString.split("-");
				// System.out.println(catSubCatStrArr[0]);
				Integer categoryId = Integer.parseInt(catSubCatStrArr[0]);
				if (categoryId.equals(categoryInDB)) {
					String[] subCategoryIdArr = catSubCatStrArr[1].toString().split(",");
					int[] subCategoryIdIntArr = Stream.of(subCategoryIdArr).mapToInt(Integer::parseInt).toArray();
					List<Integer> subCategoryIdList = Arrays.stream(subCategoryIdIntArr).boxed()
							.collect(Collectors.toList());
					List<Integer> subCategoryActiveIds = subCategoryInfoV2DAO.getByIds(subCategoryIdList);
					List<ProductCityInfoV2DTO> productCityInfoDTOList = productCityInfoV2DAO
							.getBySubCategoryIdsAndCityCode(subCategoryActiveIds, cityCode, true);

					List<Integer> productIdList = productCityInfoDTOList.stream()
							.map(ProductCityInfoV2DTO::getProductId).distinct().collect(Collectors.toList());
					if (productIdList.size() > 0) {
						List<Integer> activeProductIds = productInfoV2DAO.getByIds(productIdList);
						response.setTotalProductCount(activeProductIds.size());
						List<ProductInfoV2DTO> productInfoList = new ArrayList<>();
						if (activeProductIds != null && !activeProductIds.isEmpty() && activeProductIds.size() != 0) {
							// productIdList =
							// commonApplication.wrapWithQuotes(productIdList);
							productInfoList = productInfoV2DAO.getByProductIds(activeProductIds, firstResult,
									elementsPerPage);
						}
						List<Integer> brandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
								.collect(Collectors.toList());
						List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
						if (brandIdList != null && !brandIdList.isEmpty() && brandIdList.size() != 0) {
							brandInfoList = brandInfoV2DAO.getByBrandIds(brandIdList);
						}
						for (ProductInfoV2DTO prV2dto : productInfoList) {
							ProductInfoV2 pInfoV2 = new ProductInfoV2();
							System.out.println("prV2dto.getId(): " + prV2dto.getId() + " prV2dto.getBrandId(): "
									+ prV2dto.getBrandId());
							pInfoV2.setProductId(prV2dto.getId());
							pInfoV2.setProductName(
									productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
											.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
							pInfoV2.setFoodType(prV2dto.getFoodType());
							pInfoV2.setBrandId(prV2dto.getBrandId());
							pInfoV2.setBrandName(
									brandInfoList.stream().filter(e -> e.getId().equals(prV2dto.getBrandId()))
											.map(BrandInfoV2DTO::getBrand).findFirst().orElse(""));

							/*
							 * PhotoDTO photoDTO
							 * =photoDAO.getPhotoByPhotoTypeAndPhotoId(
							 * ImageCategory .PRODUCT.getId(), prV2dto.getId());
							 * if (photoDTO != null && photoDTO.getUpdatedOn()
							 * != null) { if(Strings.isNullOrEmpty(photoDTO.
							 * getProductDetailIds ()) ){
							 * pInfoV2.setHasSameImage(true); } }
							 */
							/*
							 * else { ImageDTO imageDTO =
							 * productImageDAO.getByProductId(ImageCategory.
							 * PRODUCT. getName(), prV2dto.getId().longValue());
							 * if (imageDTO != null && imageDTO.getUpdatedOn()
							 * != null) {
							 * pInfoV2.setImageLastUpdatedOn(imageDTO.
							 * getUpdatedOn() .getTime()); } }
							 */

							List<ProductCityInfoV2DTO> productDetailsList = productCityInfoDTOList.stream()
									.filter(e -> e.getProductId().equals(prV2dto.getId())).collect(Collectors.toList());
							if (productDetailsList.size() == 0) {
								continue;
							}
							for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
								QuantityUnitInfo quInfo = new QuantityUnitInfo();
								quInfo.setProductDetailsId(pCityInfoV2DTO.getId());
								quInfo.setSetByDefault(pCityInfoV2DTO.isSetByDefault());
								quInfo.setMRP(pCityInfoV2DTO.getMRP());
								quInfo.setSalePrice(pCityInfoV2DTO.getSalePrice());
								quInfo.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
								if(pCityInfoV2DTO.getDiscountUnit() == null){
									pCityInfoV2DTO.setDiscountUnit("%");
								}
								quInfo.setDiscountUnit(pCityInfoV2DTO.getDiscountUnit());
								if (pCityInfoV2DTO.getDiscountUnit().equals("%")) {
									int discount = Math.round(
											(int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
													/ pCityInfoV2DTO.getMRP()));
									if (discount == 0
											&& ((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0)) {
										quInfo.setDiscountUnit("₹");
										quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
									} else {
										quInfo.setDiscount(
												(int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
														/ pCityInfoV2DTO.getMRP()));
									}
								} else if (pCityInfoV2DTO.getDiscountUnit().equals("₹")) {
									quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
								}
								quInfo.setSavedAmount((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0
										? pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice() : 0);
								quInfo.setDeliveryCharge(pCityInfoV2DTO.getDeliveryCharge());
								quInfo.setActive(pCityInfoV2DTO.isActive());
								if (prV2dto.isStockStatus()) {
									quInfo.setStockStatus(pCityInfoV2DTO.isStockStatus());
								} else {
									quInfo.setStockStatus(prV2dto.isStockStatus());
								}
								if (pCityInfoV2DTO.getPhotoId() != null) {
									quInfo.setPhotoId(
											pCityInfoV2DTO.getPhotoId() == null ? 0 : pCityInfoV2DTO.getPhotoId());
									PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
									if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
										quInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
									}
								}
								pInfoV2.setSubCategoryId(pCityInfoV2DTO.getSubCategoryId());
								pInfoV2.getProductDetailList().add(quInfo);
							}
							response.getProductList().add(pInfoV2);
						}
						break;
					}
				}

			}
		}

		return response;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	int elementsPerPage = Integer.parseInt(prop.getProperty(ResourceProperties.ELEMENTS_PER_PAGE));

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ProductStatusResponse getProductStatus(String authToken, ProductStatusRequest request, Locale locale)
			throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		if(customerDTO != null){
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		}
		ProductStatusResponse response = new ProductStatusResponse();
		if (request.getProductDetailIdList().size() > 0) {
			List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO
					.getByProductDetailIds(request.getProductDetailIdList());
			List<Integer> subCategoryIdList = productCityList.stream().map(ProductCityInfoV2DTO::getSubCategoryId).distinct().collect(Collectors.toList());
			List<Integer> categoryIdList = productCityList.stream().map(ProductCityInfoV2DTO::getCategoryId).distinct().collect(Collectors.toList());
			List<Integer> productIdList = productCityList.stream().map(ProductCityInfoV2DTO::getProductId).distinct().collect(Collectors.toList());
			
			List<Integer> subCategoryList = new ArrayList<>();
			List<Integer> categoryList = new ArrayList<>();
			List<Integer> productList = new ArrayList<>();
			subCategoryList = subCategoryInfoV2DAO.getByIds(subCategoryIdList);
			categoryList = categoryInfoV2DAO.getActiveIdsByIds(categoryIdList);
			productList = productInfoV2DAO.getByIds(productIdList);
			
			for (ProductCityInfoV2DTO productCityInfo : productCityList) {
				//ProductStatusInfo pStatusInfo = addProductStatusInfo(productCityInfo);
				ProductStatusInfo pStatusInfo = new ProductStatusInfo();
				pStatusInfo.setProductDetailId(productCityInfo.getId());
				if(subCategoryList != null && subCategoryList.contains(productCityInfo.getSubCategoryId()) && 
						categoryList != null && categoryList.contains(productCityInfo.getCategoryId()) &&
								productList != null && productList.contains(productCityInfo.getProductId()) &&
						productCityInfo.isActive()){
				pStatusInfo.setActive(true);
				}
				else{
					pStatusInfo.setActive(false);
				}
				pStatusInfo.setStockStatus(productCityInfo.isStockStatus());
				pStatusInfo.setStockQuantity(productCityInfo.getStockQuantity());
			
				response.getProductStatusList().add(pStatusInfo);
			}
		} else {
			response.getProductStatusList().clear();
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	private ProductStatusInfo addProductStatusInfo(ProductCityInfoV2DTO productCityInfo) {
		ProductStatusInfo pStatusInfo = new ProductStatusInfo();
		pStatusInfo.setProductDetailId(productCityInfo.getId());
		pStatusInfo.setActive(productCityInfo.isActive());
		pStatusInfo.setStockStatus(productCityInfo.isStockStatus());
		pStatusInfo.setStockQuantity(productCityInfo.getStockQuantity());
		return pStatusInfo;
	}
	
	

}