package com.maidin.customerApi.V2;

import java.util.Locale;

import com.maidin.pojo.customerorders.GetUpcomingOrderResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.customerorders.EditOrder;
import com.maidin.pojo.customerorders.GetOrderSummaryAndItemDetailsResponse;
import com.maidin.pojo.customerorders.GetPreviousOrderResponse;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.GetScheduledProductResponseV2;
import com.maidin.pojo.customerorders.ScheduleProductRequestV2;
import com.maidin.pojo.transactions.EmailOrderSummaryRequest;
import com.maidin.pojo.customerorders.CancelOrder;
import com.maidin.pojo.customerorders.CheckoutV2Request;
import com.maidin.pojo.customerorders.CheckoutV2Response;
import com.maidin.pojo.customerorders.GetScheduledListResponse;

public interface OrderMgmt {

	public CheckoutV2Response checkout(CheckoutV2Request request, String authToken, Locale locale) throws Exception;

	public GetUpcomingOrderResponse getUpcomingOrders(String authToken, Locale locale) throws Exception;

	public GenericResponse scheduleProduct(String authToken, ScheduleProductRequestV2 request, Locale locale) throws Exception;

	public GetScheduledListResponse getScheduleList(String authToken, Locale locale) throws Exception;

	public GenericResponse cancelUpcoming(String authToken, CancelOrder request, Locale locale) throws Exception;

	public GetOrderResponse editOrder(String authToken, EditOrder request, Locale locale) throws Exception;

	public GetPreviousOrderResponse getPreviousOrders(String authToken,Integer pageNum, Locale locale) throws Exception;

	public GetPreviousOrderResponse getOrderSummaryAndItemDetailsResponse(String authToken,
			EmailOrderSummaryRequest request, Locale locale) throws Exception;

	
}
