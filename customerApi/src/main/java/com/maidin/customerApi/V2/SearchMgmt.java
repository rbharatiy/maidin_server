package com.maidin.customerApi.V2;

import java.util.List;
import java.util.Locale;

import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;
import com.maidin.pojo.search.SearchSuggestionsResponse;

public interface SearchMgmt {

	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception;

	SearchProductListResponse searchProductList1(String authToken, SearchProductRequest request, Locale locale)
			throws Exception;

	void saveSearchQuery(String query);

	List<PopularSearchesDTO> getActivePopularSearchKeywords(String authToken, Locale locale) throws Exception;

	public SearchSuggestionsResponse getSearchSuggestions(String authToken, SearchProductRequest request, Locale locale)
			throws Exception;
}
