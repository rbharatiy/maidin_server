package com.maidin.customerApi.V2;

import java.util.Locale;

import com.maidin.common.exception.ValidationException;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.transactions.CustomerTransactionV2Response;
import com.maidin.pojo.transactions.GetCustomerTransactionResponse;


public interface TransactionHistoryMgmt {

	public CustomerTransactionV2Response getCustomerTransactionByPageNumber(String authToken, Integer currentPageNumber,
			int transactionType, Locale locale) throws Exception;

	GenericResponse sendOrderSummaryEmailToCustomer(String authToken, Long orderId, Locale locale)
			throws ValidationException, Exception;

}
