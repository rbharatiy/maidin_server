package com.maidin.customerApi.V2.impl;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.EmailSender;
import com.maidin.customerApi.V2.TransactionHistoryMgmt;
import com.maidin.customerApi.enums.CustomerTransactionsType;
import com.maidin.customerApi.enums.WalletDescription;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.transactions.CustomerTransactionDetails;
import com.maidin.pojo.transactions.CustomerTransactionV2Response;

public class TransactionHistoryMgmtImpl implements TransactionHistoryMgmt {


	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final CustomerTransactionDAO customerTransactionDAO;
	//private final CustomerTransactionDAO customerTransactionDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;

	@Autowired
	public TransactionHistoryMgmtImpl(AuthenticationMgmt authenticationMgmt,
			CustomerDAO customerDAO, CustomerTransactionDAO customerTransactionDAO, OrderInfoDAO orderInfoDAO,
			OrderDetailsInfoDAO orderDetailsInfoDAO) {
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.customerTransactionDAO = customerTransactionDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO =orderDetailsInfoDAO;
		
	}

	private static final Logger logger = LoggerFactory.getLogger(TransactionHistoryMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public CustomerTransactionV2Response getCustomerTransactionByPageNumber(String authToken,
			Integer currentPageNumber, int transactionType, Locale locale) throws Exception {

//		String mobile = "8800264604";
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), "transactionType : " + transactionType+ "currentPageNumber : " + currentPageNumber);

		final CustomerTransactionV2Response response = new CustomerTransactionV2Response();
		int elementsPerPage = Integer
				.parseInt(prop.getProperty(ResourceProperties.ELEMENTS_PER_PAGE_CUSTOMER_TRANSACTIONS));
		int firstResult = (currentPageNumber - 1) * elementsPerPage;
		// get no of transactions
		List<Object[]> transactionList = new ArrayList<>();

		if (transactionType == CustomerTransactionsType.All.getId()) {
		
			long noOfRecords = customerTransactionDAO.getMoneyCountAllTransactions(customerDTO.getId());
			response.setTotalRecordsCount(noOfRecords);
			
			transactionList = customerTransactionDAO.getMoneyAllTransactions(customerDTO.getId(), firstResult, elementsPerPage);
		}
		else if (transactionType == CustomerTransactionsType.MONEY_IN.getId()) {
			long noOfRecord = customerTransactionDAO
			.getTotalCountMoneyInTransactions(customerDTO.getId());
			response.setTotalRecordsCount(noOfRecord);
			
			transactionList = customerTransactionDAO
					.getMoneyInTransactions(customerDTO.getId(), firstResult, elementsPerPage);
		}
		else if (transactionType == CustomerTransactionsType.MONEY_OUT.getId()) {
			long noOfRecord = customerTransactionDAO
			.getTotalCountMoneyOutTransactions(customerDTO.getId());
			response.setTotalRecordsCount(noOfRecord);
			
			transactionList = customerTransactionDAO
					.getMoneyOutTransactions(customerDTO.getId(), firstResult, elementsPerPage);
		}

//		for (CustomerTransactionDTO transaction : listForResponse) {
//			
//			for (Field field : transaction.getClass().getDeclaredFields()) {
//			    field.setAccessible(true);
//			    String name = field.getName();
//			    Object value = field.get(transaction);
//			    System.out.printf("%s: %s%n", name, value);
//			}
//			System.out.println("----------------");
//		}
//		
		
		//Put entries in response
		transactionList.stream().forEach((record) -> {
			CustomerTransactionDetails transaction = new CustomerTransactionDetails();
			if(Float.parseFloat(record[3].toString()) > 0){
				transaction.setOrderIdText(prop.getProperty(ResourceProperties.MAIDIN_MONEY_RECHARGE_TEXT));
				transaction.setTransactionType(CustomerTransactionsType.MONEY_IN.getId());
			} else {
				transaction.setOrderIdText(prop.getProperty(ResourceProperties.MAIDIN_ORDER_TEXT) + record[1].toString());
				transaction.setTransactionType(CustomerTransactionsType.MONEY_OUT.getId());
			}
			transaction.setOrderId(Long.parseLong(record[1].toString()));
			transaction.setMoneyOut(Float.parseFloat(record[2].toString()));
			transaction.setMoneyIn(Float.parseFloat(record[3].toString()));
			transaction.setTransactionDateTimeStamp(Long.parseLong(record[4].toString()));
			transaction.setFullyOrPartiallyPaidText(record[5].toString());
			transaction.setTransactionPaymentMode(record[7].toString());
			
			response.getCustomerTransactions().add(transaction);
		});
		
		CustomerTransactionV2Response response1 = response;
		response1 = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse sendOrderSummaryEmailToCustomer(String authToken, Long orderId, Locale locale) throws ValidationException, Exception {
		return null;/*
		final StringBuilder messageBody = new StringBuilder();
		String contextPath = prop.getProperty(ResourceProperties.CONTEXTPATH);
		GenericResponse response = new GenericResponse();
		
		
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString("orderId : " + orderId));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
		
		if (customerDTO.getEmail() == null || customerDTO.getEmail().isEmpty()) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_NOT_FOUND, locale));
		}
		
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		if(orderInfoDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_ORDER, locale));
		}

		List<OrderDetailsInfoDTO> orderDetailsInfoDTO = orderDetailsInfoDAO.getByOrderId(orderId);
		if(orderDetailsInfoDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_ORDER, locale));
		}
		
		
		//Format date and time
		DateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
		dateTime.setTimeZone(TimeZone.getTimeZone("IST"));

		Timestamp timeStamp = orderInfoDTO.getOrderedTime();
		String orderedDate = dateTime.format(timeStamp);
		String deliveryDate = orderInfoDTO.getDeliveryDate() + " " + orderInfoDTO.getTimeSlot();
		
		double f_cartValue = 0;
		double f_deliveryCharges = 0;
		double f_amountPaid = 0;
		double f_pendingAmount = 0;
		String registerdAddress = prop.getProperty(ResourceProperties.COMPANY_REGISTERED_OFFICE_ADDRESS);
		String maidinLogo = prop.getProperty(ResourceProperties.LOGO_LOCATION);
		String maidinSiteLink = prop.getProperty(ResourceProperties.LOGO_LOCATION);
		String gSTIN = prop.getProperty(ResourceProperties.GSTIN);
		String orders = "";
		
		for (OrderDetailsInfoDTO transaction : orderDetailsInfoDTO) {
			 f_cartValue += transaction.getProductSalePrice()*transaction.getQuantity();
			 f_deliveryCharges += transaction.getDeliveryCharges()*transaction.getQuantity();
			 f_amountPaid += transaction.getAmountPaidByCustomer();
			 f_pendingAmount += transaction.getTotalProductCost() - transaction.getAmountPaidByCustomer();
			 ProductCityDTO productDTO = productInfoDAO.getById(transaction.getProductId());
			 String unit = "?";
			 if(productDTO != null) {
				 unit = productDTO.getQuantityUnit();
			 }
			 //Print ordered Items
			 
			 orders += "<tr style=\"color: #505050; border-bottom: 1px solid;\"> "
					 + " <td style=\"color: #505050;\" data-darkreader-inline-color=\"\"> "
					 + " <div style=\"font-size: 12px; padding-bottom: 10px;\"> "
					 + transaction.getProductName()
					 + " </div> "
					 + " </td> "
					 + " <td style=\"text-align: right;\" width=\"20%;\"> "
					 + " <div style=\"font-size: 12px; padding-bottom: 10px;\"> "
					 + transaction.getQuantity() + " x " + unit
					 + "</div> "
					 + " </td> "
					 + " <td style=\"text-align: right;\" width=\"20%;\"> "
					 + " <div style=\"font-size: 12px; padding-bottom: 10px; padding-right: 10px;\" data-darkreader-inline-color=\"\">&#8377; "
					 + transaction.getProductSalePrice() * transaction.getQuantity()
					 + "</div> "
					 + " </td> "
					 + " </tr> ";
			 
			}
		final String orderList = orders;
		final double cartValue = f_cartValue;
		final double deliveryCharges = f_deliveryCharges;
		final double amountPaid = f_amountPaid;
		final double pendingAmount = f_pendingAmount;
	
		CommonUtils.readResourceLineByLine(ResourceProperties.CUSTOMER_ORDER_SUMMARY_RECIEPT_EMAIL_TEMPLATE,
				new Consumer<String>() {

					@Override
					public void handle(String line) {
						
						if (line.indexOf("#{maidinSiteLink}") != -1) {
							line = line.replace("#{maidinSiteLink}", ""+maidinSiteLink);
						}
						
						if (line.indexOf("#{madinLogo}") != -1) {
							line = line.replace("#{madinLogo}", ""+maidinLogo);
						}
						
						if (line.indexOf("#{orderItemsList}") != -1) {
							line = line.replace("#{orderItemsList}", ""+orderList);
						}
						
						if (line.indexOf("#{customerName}") != -1) {
							line = line.replace("#{customerName}", customerDTO.getFullName());
						}
						if (line.indexOf("#{deliveryAddress}") != -1) {
							line = line.replace("#{deliveryAddress}", orderInfoDTO.getDeliveryLocation());
						}
						if (line.indexOf("#{mobileNumber}") != -1) {
							line = line.replace("#{mobileNumber}", customerDTO.getMobileNumber());
						}
						if (line.indexOf("#{orderNo}") != -1) {
							line = line.replace("#{orderNo}", ""+orderInfoDTO.getId());
						}
						if (line.indexOf("#{dateTime}") != -1) {
							line = line.replace("#{dateTime}", orderedDate);
						}
						if (line.indexOf("#{deliveredOn}") != -1) {
							line = line.replace("#{deliveredOn}", deliveryDate);
						}
						
//						if (line.indexOf("#{gaddiLogo}") != -1) {
//							String logoPath = contextPath + prop.getProperty(ResourceProperties.GADDILOGO);
//							line = line.replace("#{gaddiLogo}", logoPath);
//						}
//						if (line.indexOf("#{logoStyle}") != -1) {
//							line = line.replace("#{logoStyle}", stringsProp.getProperty(ResourceProperties.LOGO_STYLE_PROPERTIES_IN_MAIL)) ;
//						}
						// ----------Calculations Bill starts here
						if (line.indexOf("#{cartValue}") != -1) {
							line = line.replace("#{cartValue}", ""+cartValue);
						}
						if (line.indexOf("#{deliveryCharges}") != -1) {
							line = line.replace("#{deliveryCharges}", ""+deliveryCharges);
						}
						if (line.indexOf("#{amountPaid}") != -1) {
							line = line.replace("#{amountPaid}", "" + amountPaid);
						}
						if (line.indexOf("#{pendingAmount}") != -1) {
							line = line.replace("#{pendingAmount}", "" + amountPaid);
						}
						
						
						if(pendingAmount>0)
						{
							if (line.indexOf("#{pendingAmountStyle}") != -1) {
								line = line.replace("#{pendingAmountStyle}", "style=\"font-size: 14px; margin-bottom: 10px; color: #d85972;\"");
							}
							if (line.indexOf("#{pendingAmount}") != -1) {
								line = line.replace("#{pendingAmount}", ""+ pendingAmount);
							}
						} else {
							if (line.indexOf("#{pendingAmountStyle}") != -1) {
								line = line.replace("#{pendingAmountStyle}", "style=\"display:none; border-collapse:collapse;\"");
							}
							if (line.indexOf("#{pendingAmount}") != -1) {
								line = line.replace("#{pendingAmount}", ""+ 0.00);
							}
						}
						
						// -----------Calculation bill ends here
						
						if (line.indexOf("#{officeAddress}") != -1) {
							line = line.replace("#{officeAddress}", "" + registerdAddress);
						}
						if (line.indexOf("#{gSTIN}") != -1) {
							line = line.replace("#{gSTIN}", "" + gSTIN);
						}

						messageBody.append(line);
					}
				});
		String subject = "Your Maidin Order Summary : #" + orderInfoDTO.getId();
		System.out.println("Email Body : " + messageBody.toString());
		EmailSender.sendMail(customerDTO.getEmail(), subject, messageBody.toString());
		return ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
	*/}
	
}
