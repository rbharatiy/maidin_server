package com.maidin.customerApi.V2;

import java.util.Locale;

import com.maidin.pojo.inventory.PaymentOptionResponse;
import com.maidin.pojo.inventory.CategoryListV2Response;
import com.maidin.pojo.inventory.GetProductListV2Request;
import com.maidin.pojo.inventory.GetSubCategoryV2Request;
import com.maidin.pojo.inventory.ProductListV2Response;
import com.maidin.pojo.inventory.ProductStatusRequest;
import com.maidin.pojo.inventory.ProductStatusResponse;
import com.maidin.pojo.inventory.SubCategoryListV2Response;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.product.BrandListResponse;
import com.maidin.pojo.product.CategoryListResponse;
import com.maidin.pojo.product.GetBrandRequest;

public interface InventoryMgmt {

	public CategoryListV2Response getCategoryListV2(String authToken, Locale locale) throws Exception;

	public SubCategoryListV2Response getSubCategoryListV2(String authToken, GetSubCategoryV2Request request/*,Integer pageNum*/, Locale locale)  throws Exception;

	public ProductListV2Response getProductListV2(String authToken, GetProductListV2Request request,Integer pageNum, Locale locale) throws Exception;

	public PaymentOptionResponse getDashboardList(String authToken, Locale locale) throws Exception;

	public ProductStatusResponse getProductStatus(String authToken, ProductStatusRequest request, Locale locale) throws Exception;

	/*public GetAllPhotosResponse getImageList(String authToken, int imageType, long itemId, Locale locale) throws Exception;*/

	

	
}
