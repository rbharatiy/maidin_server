package com.maidin.customerApi.V2.impl;

import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.SmsSender;
import com.maidin.customerApi.V2.OrderMgmt;
import com.maidin.customerApi.enums.OrderStatus;
import com.maidin.customerApi.enums.PaymentMode;
import com.maidin.customerApi.enums.PaymentStatus;
import com.maidin.customerApi.enums.ScheduleStatus;
import com.maidin.customerApi.enums.WalletDescription;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.BlockedSlotDAO;
import com.maidin.persistence.dao.CartDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.BlockedSlotDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.PreviousOrderDetailsInfoDTO;
import com.maidin.persistence.dto.V2.PreviousOrderInfoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.UpcomingOrderDTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.customerorders.ByDeliveryDate;
import com.maidin.pojo.customerorders.ByTimeSlot;
import com.maidin.pojo.customerorders.CancelOrder;
import com.maidin.pojo.customerorders.CheckoutInfo;
import com.maidin.pojo.customerorders.CheckoutV2Request;
import com.maidin.pojo.customerorders.CheckoutV2Response;
import com.maidin.pojo.customerorders.EditOrder;
import com.maidin.pojo.customerorders.GetOrderSummaryAndItemDetailsResponse;
import com.maidin.pojo.customerorders.GetPreviousOrderResponse;
import com.maidin.pojo.customerorders.GetScheduledListResponse;
import com.maidin.pojo.customerorders.GetUpcomingOrderResponse;
import com.maidin.pojo.customerorders.OrderDetailsInfo;
import com.maidin.pojo.customerorders.PaymentSettledModes;
import com.maidin.pojo.customerorders.PreviousOrderDetailsInfo;
import com.maidin.pojo.customerorders.PreviousOrderInfo;
import com.maidin.pojo.customerorders.ProductDetails;
import com.maidin.pojo.customerorders.ScheduleProductRequestV2;
import com.maidin.pojo.customerorders.ScheduledProductInfo;
import com.maidin.pojo.customerorders.UpdatedOrderDetails;
import com.maidin.pojo.transactions.EmailOrderSummaryRequest;

public class OrderMgmtImpl implements OrderMgmt {

	private final ProductDAO productDAO;
	private final ProductBrandsDAO brandDAO;
	private final ProductCategoryDAO categoryDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final CartDAO cartDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final PaymentModeDAO paymentModeDAO;
	private final ScheduleInfoDAO scheduleDAO;
	private final WalletDAO walletDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;
	private final CommonApplication commonApplication;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final AddressDAO addressDAO;
	private final PhotoDAO photoDAO;
	private final BlockedSlotDAO blockedSlotDAO;

	@Autowired
	public OrderMgmtImpl(ProductDAO productDAO, ProductBrandsDAO brandDAO, ProductCategoryDAO categoryDAO,
			AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO, TimeSlotDAO timeSlotDAO, CartDAO cartDAO,
			OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO, PaymentModeDAO paymentModeDAO,
			ScheduleInfoDAO scheduleDAO, WalletDAO walletDAO, CustomerTransactionDAO transactionDAO,
			ProductCityInfoV2DAO productCityInfoDAO, ProductInfoV2DAO productInfoV2DAO,
			CityCategoryInfoDAO cityCategoryInfoDAO, CommonApplication commonApplication, BrandInfoV2DAO brandInfoV2DAO,
			AddressDAO addressDAO, PhotoDAO photoDAO, BlockedSlotDAO blockedSlotDAO) {

		this.productDAO = productDAO;
		this.brandDAO = brandDAO;
		this.categoryDAO = categoryDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.cartDAO = cartDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.paymentModeDAO = paymentModeDAO;
		this.scheduleDAO = scheduleDAO;
		this.walletDAO = walletDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoDAO = productCityInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
		this.commonApplication = commonApplication;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.addressDAO = addressDAO;
		this.photoDAO = photoDAO;
		this.blockedSlotDAO = blockedSlotDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(OrderMgmtImpl.class);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CheckoutV2Response checkout(CheckoutV2Request request, String authToken, Locale locale) throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));

		CheckoutV2Response response = new CheckoutV2Response();
		for (CheckoutInfo checkoutInfo : request.getCheckoutList()) {
			validateCheckout(checkoutInfo, locale);

			float totalBill = 0f;
			long orderId = addCheckoutOrderInfo(customerDTO.getId(), checkoutInfo, locale);
			for (ProductDetails productDetails : checkoutInfo.getProductList()) {
				totalBill += addOrderDetailsInfo(orderId, productDetails, locale);
			}
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
			orderInfoDTO.setOrderSubTotal(totalBill);
			orderInfoDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());

		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		if (response.getStatus()) {
			for (CheckoutInfo checkoutInfo : request.getCheckoutList()) {
				sendSuccessMessageToCustomer(checkoutInfo, mobile);
			}
		}
		return response;

	}

	private void sendSuccessMessageToCustomer(CheckoutInfo request, String mobile) {
		String successMessage = "Your order is confirmed successfully. Order will be delivered on "
				+ ApiUtils.getDateAsString(request.getDeliveryTimestamp()) + " between "
				+ getTimeSlotInterval(request.getTimeslotId()) + " . Thank you for shopping with us. Team Maidin.";
		SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage);
	}

	private String getTimeSlotInterval(int timeSlotId) {
		String result = "";
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(timeSlotId);
		if (timeSlotDTO != null) {
			result = timeSlotDTO.getTimeSlot();
		}
		return result;
	}

	private void validateCheckout(CheckoutInfo checkoutInfo, Locale locale) throws ValidationException {
		if (prop.getProperty(ResourceProperties.IS_CHECKOUT_BLOCKED).equalsIgnoreCase("ON")) {
			List<BlockedSlotDTO> blockedSlotList = blockedSlotDAO
					.getByBlockedDate(new Date(checkoutInfo.getDeliveryTimestamp()));
			for (BlockedSlotDTO blockedSlotDTO : blockedSlotList) {
				String timeSlotIds = blockedSlotDTO.getTimeSlotIds();
				String[] timeSlotStrArr = timeSlotIds.split(",");
				int[] timeSlotIntArr = Stream.of(timeSlotStrArr).mapToInt(Integer::parseInt).toArray();
				List<Integer> timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				if (timeSlotIdList.contains(checkoutInfo.getTimeslotId())) {
					List<String> timeSlotDTOList = timeSlotDAO.getByIds(timeSlotIdList);
					String timeSlotsCommaSeperated = timeSlotDTOList.stream().map(String::toLowerCase)
							.collect(Collectors.joining(", "));

					String message = timeSlotsCommaSeperated + " slots are not available for "
							+ ApiUtils.dateToFormattedString(blockedSlotDTO.getDateFormatter(),
									blockedSlotDTO.getBlockedDate(), locale)
							+ ".";
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(message,
							ExceptionCode.SLOT_NOT_AVAILABLE, locale));
				}

			}

		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetUpcomingOrderResponse getUpcomingOrders(String authToken, Locale locale) throws Exception {
		GetUpcomingOrderResponse response = new GetUpcomingOrderResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId());
		response = getUpcomingOrders(customerDTO.getId());
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse scheduleProduct(String authToken, ScheduleProductRequestV2 request, Locale locale)
			throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		// logger.info("customerId: " + customerDTO.getId());
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		if (request.getScheduleId() == 0) {
			Long scheduleId = addSchedule(request, customerDTO.getId(), locale);
			if (scheduleId != null) {
				ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleId);
				addOrdersForSchedule("V2", scheduleId, scheduleInfoDTO.getStartDate(), locale);
			}
		} else if (request.getScheduleId() > 0) {
			ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(request.getScheduleId());
			if (scheduleDTO != null) {
				if (scheduleDTO.getCustomerId().equals(customerDTO.getId())) {
					if (request.getScheduleStatus() == ScheduleStatus.REMOVE.getId()
							&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
						System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
						deleteSchedule(scheduleDTO);
						System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
					} else if (request.getScheduleStatus() == ScheduleStatus.PAUSENEXT.getId()
							&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
						System.out
								.println("Going to pause in  Upsert...: " + new Timestamp(System.currentTimeMillis()));
						pauseNextDelivery(scheduleDTO);
						System.out.println("Pausing done in Upsert: " + new Timestamp(System.currentTimeMillis()));
					} else {
						boolean addOrderRequired = false;
						updateSchedule(request, scheduleDTO);
						updateOrders(scheduleDTO, request, addOrderRequired, locale);
					}
				}
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);

		if (request.getScheduleId() == 0) {
			if (response.getStatus()) {
				ProductCityInfoV2DTO prCityInfoV2DTO = productCityInfoDAO.getById(request.getProductDetailsId(), true);
				ProductInfoV2DTO productDTO = productInfoV2DAO.getById(prCityInfoV2DTO.getProductId(), true);
				String successMessage = "Your order of " + productDTO.getProductName() + " "
						+ prCityInfoV2DTO.getQuantityUnit()
						+ " has been scheduled successfully. Thank you for shopping with us. Team Maidin.";
				SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage);
			}
		}
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		if (response.getStatus()) {
			if (request.getScheduleStatus() == ScheduleStatus.REMOVE.getId()) {
				String cancelMessage = "Dear Customer, We have cancelled your order as per your request. "
						+ "Please get in touch with us on 9990166644 in case of any queries. Team Maidin.";
				SmsSender.sendCheckoutOrderSuccessSMS(mobile, cancelMessage);
			}
		}
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetScheduledListResponse getScheduleList(String authToken, Locale locale) throws Exception {
		GetScheduledListResponse response = new GetScheduledListResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId());

		List<ScheduleInfoDTO> scheduleDTOList = scheduleDAO.getByCustomerId(customerDTO.getId());
		for (ScheduleInfoDTO scheduleInfoDTO : scheduleDTOList) {
			ScheduledProductInfo scheduleProductInfo = getScheduleProductInfo(scheduleInfoDTO, locale);
			if (scheduleProductInfo != null) {
				response.getScheduledProductsList().add(scheduleProductInfo);
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse cancelUpcoming(String authToken, CancelOrder request, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		for (Long orderDetailsId : request.getOrderDetailsIdList()) {
			OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsId);
			if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
				int changeInQuantity = orderDetailsInfoDTO.getQuantity();
				checkCustomerTransaction(customerDTO.getId(), orderDetailsInfoDTO.getId(), changeInQuantity,
						orderDetailsInfoDTO.getTotalProductCost(), "CANCELLED");

				if (orderDetailsInfoDTO != null) {
					orderDetailsInfoDTO.setCancelled(true);
					orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
					orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
					orderDetailsInfoDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
					// orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
					orderDetailsInfoDTO.setPayBy(String.valueOf(PaymentMode.NA.getId()));

					List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
							.getByOrderId(orderDetailsInfoDTO.getOrderId());

					if (orderDetailsInfoList != null) {
						OrderInfoDTO orderInfo = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
						if (orderDetailsInfoList.size() == 1) {
							orderInfo.setCancelled(true);
							orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
							orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
							// orderInfo.setPaymentModeId(PaymentMode.NA.getId());
							orderInfo.setPayBy(String.valueOf(PaymentMode.NA.getId()));
							orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
							orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						} else {
							int cntCancelled = 0;
							for (OrderDetailsInfoDTO orderD : orderDetailsInfoList) {
								if (orderD.getOrderStatus() == OrderStatus.CANCELLED.getName()) {
									cntCancelled++;
								}
							}
							if (cntCancelled == orderDetailsInfoList.size()) {
								orderInfo.setCancelled(true);
								orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
								orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
								orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
								orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
							}
						}
						setOrderSubTotal(orderInfo);
					}
				}
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		if (response.getStatus()) {
			String cancelMessage = "Dear Customer, We have cancelled your order as per your request. "
					+ "Please get in touch with us on 9990166644 in case of any queries. Team Maidin.";
			SmsSender.sendCheckoutOrderSuccessSMS(mobile, cancelMessage);
		}
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponse editOrder(String authToken, EditOrder request, Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId());

		GetOrderResponse response = new GetOrderResponse();
		List<UpdatedOrderDetails> orderList = request.getUpdatedOrderList();
		for (UpdatedOrderDetails orderDetails : orderList) {
			OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetails.getOrderDetailsId());
			if (orderDetailsInfoDTO != null
					&& orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
				validateEditOrderRequest(orderDetails, orderDetailsInfoDTO.getOrderId(), locale);
				editOrders(customerDTO.getId(), orderDetailsInfoDTO, orderDetails, locale);
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_ORDER, locale));
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void validateEditOrderRequest(UpdatedOrderDetails request, Long orderId, Locale locale)
			throws ValidationException {
		if (prop.getProperty(ResourceProperties.IS_CHECKOUT_BLOCKED).equalsIgnoreCase("ON")) {
			List<BlockedSlotDTO> blockedSlotDTOList = blockedSlotDAO
					.getByBlockedDate(new Date(request.getDeliveryTimestamp()));
			for (BlockedSlotDTO blockedSlotDTO : blockedSlotDTOList) {
				String timeSlotIds = blockedSlotDTO.getTimeSlotIds();
				String[] timeSlotStrArr = timeSlotIds.split(",");
				int[] timeSlotIntArr = Stream.of(timeSlotStrArr).mapToInt(Integer::parseInt).toArray();
				List<Integer> timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				if (timeSlotIdList.contains(request.getTimeSlotId())) {
					List<String> timeSlotDTOList = timeSlotDAO.getByIds(timeSlotIdList);
					String timeSlotsCommaSeperated = timeSlotDTOList.stream().map(String::toLowerCase)
							.collect(Collectors.joining(", "));

					String message = timeSlotsCommaSeperated + " slots are not available for "
							+ ApiUtils.dateToFormattedString(blockedSlotDTO.getDateFormatter(),
									blockedSlotDTO.getBlockedDate(), locale)
							+ ".";
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(message,
							ExceptionCode.SLOT_NOT_AVAILABLE, locale));
				}
			}

		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetPreviousOrderResponse getPreviousOrders(String authToken, Integer pageNum, Locale locale)
			throws Exception {
		GetPreviousOrderResponse response = new GetPreviousOrderResponse();
		// String mobile = "8800264604";
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), " pageNum" + pageNum);
		int elementsPerPage = ELEMENTS_PER_PAGE;
		int firstResult = (pageNum - 1) * elementsPerPage;
		List<PaymentModeDTO> paymentModeDTOList = paymentModeDAO.getAll();
		// int totalOrderCount = 0;
		// totalOrderCount =
		// orderInfoDAO.getTotalOrderCount(customerDTO.getId(),
		// OrderStatus.DELIVERED.getId(),false).intValue();

		response.setTotalOrderCount(orderInfoDAO
				.getPreviousOrders(customerDTO.getId(), OrderStatus.DELIVERED.getName(), false, 0, -1).size());

		List<PreviousOrderInfoDTO> orderInfoList = orderInfoDAO.getPreviousOrders(customerDTO.getId(),
				OrderStatus.DELIVERED.getName(), false, firstResult, elementsPerPage);
		List<Long> orderIdList = orderInfoList.stream().map(PreviousOrderInfoDTO::getOrderId).distinct()
				.collect(Collectors.toList());
		if (orderIdList != null && !orderIdList.isEmpty() && orderIdList.size() != 0) {
			List<PreviousOrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO.getOrderDetails(orderIdList,
					OrderStatus.DELIVERED.getName(), false);

			for (PreviousOrderInfoDTO orderInfoDTO : orderInfoList) {
				int quantity = 0;
				float deliveryCharge = 0;
				float cartValue = 0;
				PreviousOrderInfo orderInfo = new PreviousOrderInfo();
				orderInfo.setOrderId(orderInfoDTO.getOrderId());
				orderInfo.setDeliveryTimestamp(orderInfoDTO.getDeliveryDate().getTime());
				orderInfo.setTimeSlot(orderInfoDTO.getTimeSlot());
				orderInfo.setTotalCost((float) orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToDouble(PreviousOrderDetailsInfoDTO::getTotalPrice).sum());
				orderInfo.setTotalPaid((float) orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToDouble(PreviousOrderDetailsInfoDTO::getAmountPaidByCustomer).sum());
				// orderInfo.setTotalDeliveryCharge((float)orderDetailsInfoDTOList.stream().filter(e
				// -> e.getOrderId() ==
				// orderInfoDTO.getOrderId()).mapToDouble(PreviousOrderDetailsInfoDTO::getOrderDeliveryCharge).sum()*
				// orderDetailsInfoDTOList.stream().filter(e -> e.getOrderId()
				// ==
				// orderInfoDTO.getOrderId()).mapToInt(PreviousOrderDetailsInfoDTO::getQuantity).sum());
				quantity = orderDetailsInfoDTOList.stream().filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToInt(PreviousOrderDetailsInfoDTO::getQuantity).sum();
				orderInfo.setNoOfItems(quantity);
				// orderInfo.setCartValue((float)orderDetailsInfoDTOList.stream().filter(e
				// -> e.getOrderId() ==
				// orderInfoDTO.getOrderId()).mapToDouble(PreviousOrderDetailsInfoDTO::getProductSalePrice).sum()*quantity);
				orderInfo.setTotalPending(orderInfo.getTotalCost() - orderInfo.getTotalPaid());
				/*
				 * <<<<<<< HEAD
				 * 
				 * //Changed By Pushpinder
				 * orderInfo.setPaymentOptions(orderInfoDTO.getPaymentOption());
				 * // orderInfo.setPaymentOptions(transactionDAO.
				 * getByPaymentOptionTextByOrderId(orderInfoDTO.getOrderId()));
				 * 
				 * ======= //
				 * orderInfo.setPaymentOptions(orderInfoDTO.getPaymentOption());
				 * >>>>>>> refs/heads/PaymentModeAddition
				 */
				AddressDTO addressDTO = addressDAO.getById(orderInfoDTO.getAddressId());
				orderInfo.setDeliveryLocation("Flat no. " + addressDTO.getFlat() + ", " + addressDTO.getTower() + ", "
						+ addressDTO.getProject() + ", " + addressDTO.getCity());
				orderInfo.setIsScheduled(orderInfoDTO.getScheduleId() == null ? false : true);
				orderInfo.setOrderText("Maidin-order-" + orderInfoDTO.getOrderId());
				orderInfo.setOrderedTime(orderInfoDTO.getOrderedTime().getTime());
				orderInfo.setOrderStatus(OrderStatus.fromId(orderInfoDTO.getOrderStatus()).getId());
				List<PreviousOrderDetailsInfoDTO> orderDetailsDTOList = orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId()).distinct()
						.collect(Collectors.toList());

				// List<Object[]> paymentModeList =
				// transactionDAO.getByOrderId(orderInfoDTO.getOrderId());
				// paymentModeList.stream().forEach((record) -> {
				// PaymentSettledModes mode = new PaymentSettledModes();
				// mode.setPaymentModeId(Integer.parseInt(record[0].toString()));
				// mode.setAmount(Float.parseFloat(record[1].toString()));
				// mode.setPaymentModeText(record[2].toString());
				// orderInfo.getPaymentSettledModesList().add(mode);
				// });

				List<Object[]> paymentModeList = transactionDAO.getByOrderId(orderInfoDTO.getOrderId());
				paymentModeList.stream().forEach((record) -> {
					PaymentSettledModes mode = new PaymentSettledModes();
					mode.setPaymentModeId(Integer.parseInt(record[0].toString()));
					mode.setAmount(Float.parseFloat(record[1].toString()));
					mode.setPaymentModeText(record[2].toString());
					orderInfo.getPaymentSettledModesList().add(mode);
				});
				List<Integer> paymentModeIdList = orderInfo.getPaymentSettledModesList().stream()
						.map(PaymentSettledModes::getPaymentModeId).collect(Collectors.toList());
				if (orderInfo.getTotalPending() == orderInfo.getTotalCost()) {
					orderInfo.setPaymentOptions(PaymentMode.fromId(PaymentMode.PENDING.getId()).getName());
				} else if (orderInfo.getTotalPending() > 0 && (orderInfo.getTotalPaid() < orderInfo.getTotalCost())) {
					orderInfo.setPaymentOptions(PaymentMode.fromId(PaymentMode.PARTIAL_PAYMENT.getId()).getName());
				} else {
					orderInfo.setPaymentOptions(
							paymentModeDTOList.stream().filter(e -> paymentModeIdList.contains(e.getId()))
									.map(PaymentModeDTO::getPaymentModeAbbr).collect(Collectors.joining("/")));
				}
				for (PreviousOrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsDTOList) {
					PreviousOrderDetailsInfo orderDetailsInfo = new PreviousOrderDetailsInfo();
					deliveryCharge = deliveryCharge
							+ (orderDetailsInfoDTO.getOrderDeliveryCharge() * orderDetailsInfoDTO.getQuantity());
					orderInfo.setTotalDeliveryCharge(deliveryCharge);
					cartValue = cartValue
							+ (orderDetailsInfoDTO.getOrderSalePrice() * orderDetailsInfoDTO.getQuantity());
					orderInfo.setCartValue(cartValue);

					orderDetailsInfo.setOrderDetailsId(orderDetailsInfoDTO.getOrderDetailsId());
					orderDetailsInfo.setProductId(orderDetailsInfoDTO.getProductId());
					orderDetailsInfo.setProductName(orderDetailsInfoDTO.getProductName());
					orderDetailsInfo.setBrandName(orderDetailsInfoDTO.getBrandName());
					orderDetailsInfo.setProductDetailsId(orderDetailsInfoDTO.getProductDetailsId());
					orderDetailsInfo.setCategoryId(orderDetailsInfoDTO.getCategoryId());
					orderDetailsInfo.setSubCategoryId(orderDetailsInfoDTO.getSubCategoryId());
					orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
					orderDetailsInfo.setQuantityUnit(orderDetailsInfoDTO.getQuantityUnit());
					orderDetailsInfo.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer());
					orderDetailsInfo.setTotalProductCost(orderDetailsInfoDTO.getTotalPrice());
					orderDetailsInfo.setPendingAmount(
							orderDetailsInfoDTO.getTotalPrice() - orderDetailsInfoDTO.getTotalPrice());
					orderDetailsInfo.setOrderMRP(orderDetailsInfoDTO.getOrderMRP());
					orderDetailsInfo.setOrderSalePrice(orderDetailsInfoDTO.getOrderSalePrice());
					orderDetailsInfo.setOrderDiscount(orderDetailsInfoDTO.getOrderDiscount());
					orderDetailsInfo.setOrderDeliveryCharge(orderDetailsInfoDTO.getOrderDeliveryCharge());
					orderDetailsInfo.setMRP(orderDetailsInfoDTO.getProductMRP());
					orderDetailsInfo.setSalePrice(orderDetailsInfoDTO.getProductSalePrice());
					orderDetailsInfo.setDeliveryCharge(orderDetailsInfoDTO.getProductDeliveryCharge());
					if (orderDetailsInfoDTO.getProductDiscountUnit().equals("%")) {

						int discount = Math.round((int) (((orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()) * 100)
								/ orderDetailsInfoDTO.getProductMRP()));
						if (discount == 0 && ((orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()) > 0)) {
							orderDetailsInfo.setDiscountUnit("₹");
							orderDetailsInfo.setDiscount(
									orderDetailsInfoDTO.getProductMRP() - orderDetailsInfoDTO.getProductSalePrice());
						} else {
							orderDetailsInfo.setDiscount((int) (((orderDetailsInfoDTO.getProductMRP()
									- orderDetailsInfoDTO.getProductSalePrice()) * 100)
									/ orderDetailsInfoDTO.getProductMRP()));
						}

					} else if (orderDetailsInfoDTO.getProductDiscountUnit().equals("₹")) {
						orderDetailsInfo.setDiscount((int) (orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()));
					}
					orderDetailsInfo.setDiscountUnit(orderDetailsInfoDTO.getProductDiscountUnit());
					orderDetailsInfo.setFoodType(orderDetailsInfoDTO.getFoodType());
					orderDetailsInfo.setStockQuantity(orderDetailsInfoDTO.getProductStockQuantity());
					orderDetailsInfo.setActive(orderDetailsInfoDTO.isProductActiveStatus());
					orderDetailsInfo.setStockStatus(orderDetailsInfoDTO.isProductStockStatus());
					orderDetailsInfo.setPhotoId(orderDetailsInfoDTO.getPhotoId());
					// PhotoDTO photoDTO = photoDAO.

					PhotoDTO photoDTO = photoDAO.getById(orderDetailsInfoDTO.getPhotoId());
					if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
						orderDetailsInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
					}
					orderInfo.getPreviousOrderDetailsList().add(orderDetailsInfo);
				}
				if (orderDetailsDTOList.size() > 0) {
					response.getPreviousOrderInfoList().add(orderInfo);
				}
			}
			if (orderInfoList.size() == 0) {
				response.getPreviousOrderInfoList().clear();
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		return response;
	}

	// Added By Pushpinder 08/01/2018
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetPreviousOrderResponse getOrderSummaryAndItemDetailsResponse(String authToken,
			EmailOrderSummaryRequest request, Locale locale) throws Exception {
		GetPreviousOrderResponse response = new GetPreviousOrderResponse();
		// String mobile = "8800264604";
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId());

		List<PreviousOrderInfoDTO> orderInfoList = orderInfoDAO.getOrderById(request.getOrderId());
		List<Long> orderIdList = orderInfoList.stream().map(PreviousOrderInfoDTO::getOrderId).distinct()
				.collect(Collectors.toList());

		// Put entries in response

		if (orderIdList != null && !orderIdList.isEmpty() && orderIdList.size() != 0) {
			List<PreviousOrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
					.getOrderDetailsWithoutOrderState(orderIdList, false);

			for (PreviousOrderInfoDTO orderInfoDTO : orderInfoList) {
				int quantity = 0;
				float deliveryCharge = 0;
				float cartValue = 0;
				PreviousOrderInfo orderInfo = new PreviousOrderInfo();
				orderInfo.setOrderId(orderInfoDTO.getOrderId());
				orderInfo.setDeliveryTimestamp(orderInfoDTO.getDeliveryDate().getTime());
				orderInfo.setTimeSlot(orderInfoDTO.getTimeSlot());
				orderInfo.setTotalCost((float) orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToDouble(PreviousOrderDetailsInfoDTO::getTotalPrice).sum());
				orderInfo.setTotalPaid((float) orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToDouble(PreviousOrderDetailsInfoDTO::getAmountPaidByCustomer).sum());

				quantity = orderDetailsInfoDTOList.stream().filter(e -> e.getOrderId() == orderInfoDTO.getOrderId())
						.mapToInt(PreviousOrderDetailsInfoDTO::getQuantity).sum();
				orderInfo.setNoOfItems(quantity);

				orderInfo.setTotalPending(orderInfo.getTotalCost() - orderInfo.getTotalPaid());
				orderInfo.setPaymentOptions(transactionDAO.getByPaymentOptionTextByOrderId(orderInfoDTO.getOrderId()));
				AddressDTO addressDTO = addressDAO.getById(orderInfoDTO.getAddressId());
				orderInfo.setDeliveryLocation("Flat no. " + addressDTO.getFlat() + ", " + addressDTO.getTower() + ", "
						+ addressDTO.getProject() + ", " + addressDTO.getCity());
				orderInfo.setIsScheduled(orderInfoDTO.getScheduleId() == null ? false : true);
				orderInfo.setOrderText("Maidin-order-" + orderInfoDTO.getOrderId());
				orderInfo.setOrderedTime(orderInfoDTO.getOrderedTime().getTime());
				orderInfo.setOrderStatus(OrderStatus.fromId(orderInfoDTO.getOrderStatus()).getId());
				List<PreviousOrderDetailsInfoDTO> orderDetailsDTOList = orderDetailsInfoDTOList.stream()
						.filter(e -> e.getOrderId() == orderInfoDTO.getOrderId()).distinct()
						.collect(Collectors.toList());

				List<Object[]> paymentModeList = transactionDAO.getByOrderId(orderInfoDTO.getOrderId());
				paymentModeList.stream().forEach((record) -> {
					PaymentSettledModes mode = new PaymentSettledModes();
					mode.setPaymentModeId(Integer.parseInt(record[0].toString()));
					mode.setAmount(Float.parseFloat(record[1].toString()));
					mode.setPaymentModeText(record[2].toString());
					orderInfo.getPaymentSettledModesList().add(mode);
				});

				for (PreviousOrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsDTOList) {
					PreviousOrderDetailsInfo orderDetailsInfo = new PreviousOrderDetailsInfo();
					deliveryCharge = deliveryCharge
							+ (orderDetailsInfoDTO.getOrderDeliveryCharge() * orderDetailsInfoDTO.getQuantity());
					orderInfo.setTotalDeliveryCharge(deliveryCharge);
					cartValue = cartValue
							+ (orderDetailsInfoDTO.getOrderSalePrice() * orderDetailsInfoDTO.getQuantity());
					orderInfo.setCartValue(cartValue);

					orderDetailsInfo.setOrderDetailsId(orderDetailsInfoDTO.getOrderDetailsId());
					orderDetailsInfo.setProductId(orderDetailsInfoDTO.getProductId());
					orderDetailsInfo.setProductName(orderDetailsInfoDTO.getProductName());
					orderDetailsInfo.setBrandName(orderDetailsInfoDTO.getBrandName());
					orderDetailsInfo.setProductDetailsId(orderDetailsInfoDTO.getProductDetailsId());
					orderDetailsInfo.setCategoryId(orderDetailsInfoDTO.getCategoryId());
					orderDetailsInfo.setSubCategoryId(orderDetailsInfoDTO.getSubCategoryId());
					orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
					orderDetailsInfo.setQuantityUnit(orderDetailsInfoDTO.getQuantityUnit());
					orderDetailsInfo.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer());
					orderDetailsInfo.setTotalProductCost(orderDetailsInfoDTO.getTotalPrice());
					orderDetailsInfo.setPendingAmount(
							orderDetailsInfoDTO.getTotalPrice() - orderDetailsInfoDTO.getTotalPrice());
					orderDetailsInfo.setOrderMRP(orderDetailsInfoDTO.getOrderMRP());
					orderDetailsInfo.setOrderSalePrice(orderDetailsInfoDTO.getOrderSalePrice());
					orderDetailsInfo.setOrderDiscount(orderDetailsInfoDTO.getOrderDiscount());
					orderDetailsInfo.setOrderDeliveryCharge(orderDetailsInfoDTO.getOrderDeliveryCharge());
					orderDetailsInfo.setMRP(orderDetailsInfoDTO.getProductMRP());
					orderDetailsInfo.setSalePrice(orderDetailsInfoDTO.getProductSalePrice());
					orderDetailsInfo.setDeliveryCharge(orderDetailsInfoDTO.getProductDeliveryCharge());
					if (orderDetailsInfoDTO.getProductDiscountUnit().equals("%")) {

						int discount = Math.round((int) (((orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()) * 100)
								/ orderDetailsInfoDTO.getProductMRP()));
						if (discount == 0 && ((orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()) > 0)) {
							orderDetailsInfo.setDiscountUnit("₹");
							orderDetailsInfo.setDiscount(
									orderDetailsInfoDTO.getProductMRP() - orderDetailsInfoDTO.getProductSalePrice());
						} else {
							orderDetailsInfo.setDiscount((int) (((orderDetailsInfoDTO.getProductMRP()
									- orderDetailsInfoDTO.getProductSalePrice()) * 100)
									/ orderDetailsInfoDTO.getProductMRP()));
						}

					} else if (orderDetailsInfoDTO.getProductDiscountUnit().equals("₹")) {
						orderDetailsInfo.setDiscount((int) (orderDetailsInfoDTO.getProductMRP()
								- orderDetailsInfoDTO.getProductSalePrice()));
					}
					orderDetailsInfo.setDiscountUnit(orderDetailsInfoDTO.getProductDiscountUnit());
					orderDetailsInfo.setFoodType(orderDetailsInfoDTO.getFoodType());
					orderDetailsInfo.setStockQuantity(orderDetailsInfoDTO.getProductStockQuantity());
					orderDetailsInfo.setActive(orderDetailsInfoDTO.isProductActiveStatus());
					orderDetailsInfo.setStockStatus(orderDetailsInfoDTO.isProductStockStatus());
					orderDetailsInfo.setPhotoId(orderDetailsInfoDTO.getPhotoId());
					// PhotoDTO photoDTO = photoDAO.

					PhotoDTO photoDTO = photoDAO.getById(orderDetailsInfoDTO.getPhotoId());
					if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
						orderDetailsInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
					}
					orderInfo.getPreviousOrderDetailsList().add(orderDetailsInfo);
				}
				if (orderDetailsDTOList.size() > 0) {
					response.getPreviousOrderInfoList().add(orderInfo);
				}
			}
			if (orderInfoList.size() == 0) {
				response.getPreviousOrderInfoList().clear();
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		return response;
	}

	private void editOrders(Long customerId, OrderDetailsInfoDTO orderDetailsInfoDTO,
			UpdatedOrderDetails requestedOrderDetails, Locale locale) throws ValidationException {
		OrderInfoDTO orderInfo = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
		int changeInQuantity = 0;
		if (requestedOrderDetails.getQuantity() == 0) {
			changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
			checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity,
					orderDetailsInfoDTO.getTotalProductCost(), "CANCELLED");
			orderDetailsInfoDTO.setCancelled(true);
			orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
			orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			// orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
			orderDetailsInfoDTO.setPayBy(String.valueOf(PaymentMode.NA.getId()));
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderDetailsInfoDTO.getOrderId());
			if (orderDetailsInfoList != null) {
				if (orderDetailsInfoList.size() == 1) {
					orderInfo.setCancelled(true);
					orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
					orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
					orderInfo.setCancellationTime((new Timestamp(System.currentTimeMillis())));
					orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				} else {
					int cntCancelled = 0;
					for (OrderDetailsInfoDTO orderD : orderDetailsInfoList) {
						if (orderD.getOrderStatus().equals(OrderStatus.CANCELLED.getName())) {
							cntCancelled++;
						}
					}
					if (cntCancelled == orderDetailsInfoList.size()) {
						orderInfo.setCancelled(true);
						orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
						orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
						orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
						orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
						// orderInfo.setPaymentModeId(PaymentMode.NA.getId());
						// orderInfo.setPayBy(String.valueOf(PaymentMode.NA.getId()));
					}
				}
			}

		} else {
			// quantity is increased here

			if (requestedOrderDetails.getQuantity() > orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
				String productString = productCityInfoDAO.getProductName(orderDetailsInfoDTO.getProductId());
				ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
						.getById(orderDetailsInfoDTO.getProductId(), true);
				if (productCityInfoV2DTO == null) {
					if (productString == null) {
						productString = "Product";
					}
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(
							productString, ExceptionCode.CANNOT_INCREASE_QUANTITY, locale));
				}

				if ((productCityInfoV2DTO.getSalePrice()
						+ productCityInfoV2DTO.getDeliveryCharge()) != (orderDetailsInfoDTO.getProductSalePrice()
								+ orderDetailsInfoDTO.getDeliveryCharges())) {
					ProductDetails request = new ProductDetails();
					request.setProductDetailsId(orderDetailsInfoDTO.getProductId());
					request.setQuantity(changeInQuantity);
					addOrderDetailsInfo(orderDetailsInfoDTO.getOrderId(), request, locale);
				} else {
					if (orderDetailsInfoDTO.getTotalProductCost() == 0) {
						changeInQuantity = requestedOrderDetails.getQuantity();
					} else {
						changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
					}
					orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
					orderDetailsInfoDTO.setTotalProductCost(orderDetailsInfoDTO.getQuantity()
							* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()));
					orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
				// }
				// }
			} else if (requestedOrderDetails.getQuantity() < orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
				orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
				orderDetailsInfoDTO.setTotalProductCost(requestedOrderDetails.getQuantity()
						* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()));
				orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity, changeInQuantity
						* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()),
						"DECREASE");
			}
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(requestedOrderDetails.getTimeSlotId());
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
			if (orderInfoDTO != null) {
				if (timeSlotDTO != null) {
					orderInfoDTO.setDeliveryDate(new Date(requestedOrderDetails.getDeliveryTimestamp()));
					orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
					orderInfoDTO.setTimeSlotId(timeSlotDTO.getId());
					orderInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
			}
		}

		setOrderSubTotal(orderInfo);

	}

	private void setOrderSubTotal(OrderInfoDTO orderInfoDTO) {
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getByOrderId(orderInfoDTO.getId());
		float subtotal = 0;
		for (OrderDetailsInfoDTO orderDetail : orderDetailsList) {
			subtotal += (orderDetail.getProductSalePrice() + orderDetail.getDeliveryCharges())
					* orderDetail.getQuantity();
		}
		orderInfoDTO.setOrderSubTotal(subtotal);
	}

	private void checkCustomerTransaction(Long customerId, Long orderDetailsId, int quantity, float totalPrice,
			String changeType) {
		OrderDetailsInfoDTO orderDetails = orderDetailsInfoDAO.getById(orderDetailsId);
		CustomerTransactionDTO dto = new CustomerTransactionDTO();
		dto.setCustomerId(customerId);
		CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
		transactionRequest.setCustomerId(customerId);
		transactionRequest.setOrderId(orderDetails.getOrderId());
		transactionRequest.setOrderDetailsId(orderDetailsId);
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);

		// if customer fully cancelled
		if (changeType.equals("CANCELLED")) {
			if (orderDetails.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())
					|| orderDetails.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.getName())) {
				transactionRequest.setMoneyIn(orderDetails.getAmountPaidByCustomer());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setPaymentModeId(0);
				transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
				updateWalletBalance(customerId, orderDetails.getAmountPaidByCustomer(),
						WalletDescription.RECHARGE.getName());
				addCustomerTransaction(transactionRequest, -1);
				orderDetails.setAmountPaidByCustomer(0);
			}
		}
		// if decrease quantity
		else if (changeType.equals("DECREASE")) {
			float amountPaid = orderDetails.getAmountPaidByCustomer();
			float amountToPay = orderDetails.getQuantity() * orderDetails.getProductMRP();
			if (amountPaid >= amountToPay) {
				orderDetails.setAmountPaidByCustomer(amountToPay);
				if (amountPaid > amountToPay) {
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setMoneyIn(amountPaid - amountToPay);
					transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, amountPaid - amountToPay, WalletDescription.RECHARGE.getName());
				}
			}
		}
		// if increase quantity
		else if (changeType.equals("INCREASE")) {
			if (walletDTO != null && walletDTO.getMyWalletBalance() >= 0) {
				if (walletDTO.getMyWalletBalance() >= totalPrice) {
					orderDetails.setAmountPaidByCustomer(orderDetails.getAmountPaidByCustomer() + totalPrice);
					orderDetails.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
					transactionRequest.setMoneyOut(quantity * orderDetails.getProductMRP());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setDueAmount(0);
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, totalPrice, WalletDescription.DEDUCTIONS.getName());
				} else if (walletDTO.getMyWalletBalance() < totalPrice) {
					orderDetails.setAmountPaidByCustomer(
							orderDetails.getAmountPaidByCustomer() + walletDTO.getMyWalletBalance());
					orderDetails.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
					transactionRequest.setMoneyOut(walletDTO.getMyWalletBalance());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
					transactionRequest.setDueAmount(totalPrice - walletDTO.getMyWalletBalance());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, walletDTO.getMyWalletBalance(),
							WalletDescription.DEDUCTIONS.getName());

				}
			}

		}
	}

	private void updateWalletBalance(Long customerId, float totalProductCost, String rechargeType) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		// float maidinBalance = 0;
		if (walletDTO != null) {
			walletDTO.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + totalProductCost);
			}
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			// maidinBalance = walletDTO.getMyWalletBalance();
		} else {
			WalletDTO walletDto = new WalletDTO();
			walletDto.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDto.setMyWalletBalance(totalProductCost);
			}
			walletDto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.addBalance(walletDto);
			// maidinBalance = checkMaidinBalance(customerId) ;
		}

	}

	private void addCustomerTransaction(CustomerTransactionRequest transactionRequest, long adminId) {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(transactionRequest.getCustomerId());
		transactionDTO.setOrderId(transactionRequest.getOrderId());
		transactionDTO.setOrderDetailsId(transactionRequest.getOrderDetailsId());
		transactionDTO.setMoneyIn(transactionRequest.getMoneyIn());
		transactionDTO.setRemarks(0);
		transactionDTO.setMoneyOut(transactionRequest.getMoneyOut());
		transactionDTO.setPaymentModeId(transactionRequest.getPaymentModeId());
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(transactionRequest.getTransactionType());
		transactionDTO.setUpdatedBy(adminId);
		transactionDAO.addTransaction(transactionDTO);

	}

	private ScheduledProductInfo getScheduleProductInfo(ScheduleInfoDTO scheduleInfoDTO, Locale locale)
			throws ValidationException {
		ScheduledProductInfo sInfoV2 = new ScheduledProductInfo();
		sInfoV2.setScheduleId(scheduleInfoDTO.getId());
		ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		ProductInfoV2DTO prInfoV2DTO = productInfoV2DAO.getByIdWithoutActiveState(productCityInfoV2DTO.getProductId());
		if (prInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		sInfoV2.setProductDetailsId(productCityInfoV2DTO.getId());
		sInfoV2.setProductName(prInfoV2DTO.getProductName());
		BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(prInfoV2DTO.getBrandId());
		if (brandInfoV2DTO != null) {
			sInfoV2.setBrandName(brandInfoV2DTO.getBrand());
		}
		sInfoV2.setSalePrice(productCityInfoV2DTO.getSalePrice());
		sInfoV2.setDeliveryCharge(productCityInfoV2DTO.getDeliveryCharge());
		sInfoV2.setQuantityUnit(productCityInfoV2DTO.getQuantityUnit());
		sInfoV2.setActive(productCityInfoV2DTO.isActive());
		sInfoV2.setStockStatus(productCityInfoV2DTO.isStockStatus());
		sInfoV2.setStockQuantity(productCityInfoV2DTO.getStockQuantity());
		// }
		sInfoV2.setQuantity(scheduleInfoDTO.getQuantity());
		sInfoV2.setRecurring(scheduleInfoDTO.isRecurring());
		if (scheduleInfoDTO.getPauseDate() != null) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
			Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
			if (pauseDate != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(pauseDate);
				cal.set(Calendar.HOUR_OF_DAY, timeSlotDTO.getEndTime());
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				if (((System.currentTimeMillis() + 1000) >= scheduleInfoDTO.getPauseDate().getTime())
						&& (System.currentTimeMillis() < cal.getTimeInMillis())) {
					sInfoV2.setScheduleStatus(ScheduleStatus.PAUSE.getId());
					System.out.println("---1---" + scheduleInfoDTO.getId());
				} else {
					sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
					System.out.println("---2---" + scheduleInfoDTO.getId());
				}
			} else {
				sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
				System.out.println("---3---" + scheduleInfoDTO.getId());
			}

		} else {
			sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
			System.out.println("---4---" + scheduleInfoDTO.getId());
		}

		sInfoV2.setStartDate(scheduleInfoDTO.getStartDate().getTime());
		sInfoV2.setTimeSlotId(scheduleInfoDTO.getTimeSlotId());
		if (scheduleInfoDTO.isSu())
			sInfoV2.getOrderDays().add(0);
		if (scheduleInfoDTO.isMo())
			sInfoV2.getOrderDays().add(1);
		if (scheduleInfoDTO.isTu())
			sInfoV2.getOrderDays().add(2);
		if (scheduleInfoDTO.isWe())
			sInfoV2.getOrderDays().add(3);
		if (scheduleInfoDTO.isTh())
			sInfoV2.getOrderDays().add(4);
		if (scheduleInfoDTO.isFr())
			sInfoV2.getOrderDays().add(5);
		if (scheduleInfoDTO.isSa())
			sInfoV2.getOrderDays().add(6);
		return sInfoV2;
	}

	private void updateSchedule(ScheduleProductRequestV2 request, ScheduleInfoDTO scheduleDTO) {
		scheduleDTO.setQuantity(request.getQuantity());
		scheduleDTO.setTimeSlotId(request.getTimeSlotId());
		scheduleDTO.setScheduleStatus(request.getScheduleStatus());
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDAO.updateScheduleInfo(scheduleDTO);
	}

	private void updateOrders(ScheduleInfoDTO scheduleDTO, ScheduleProductRequestV2 request, boolean addOrderRequired,
			Locale locale) throws ValidationException {
		boolean cancelled = false;
		ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleDTO.getId());
		Date scheduledFrom = null;
		if (scheduleInfoDTO.getScheduledUpto() == null) {
			scheduledFrom = scheduleInfoDTO.getStartDate();
		} else {
			scheduledFrom = scheduleDTO.getScheduledUpto();
		}
		addOrdersForSchedule("V2", scheduleDTO.getId(), scheduledFrom, locale);
		if (request.getScheduleStatus() == ScheduleStatus.PAUSE.getId()) {
			cancelled = true;
		}
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleDTO.getId());
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			TimeSlotDTO existingTimeSlotDTO = timeSlotDAO.getById(orderInfoDTO.getTimeSlotId());
			if (commonApplication.getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1
						&& cal.get(Calendar.HOUR_OF_DAY) >= (existingTimeSlotDTO.getStartTime()
								- existingTimeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
				orderDetailsInfo.setQuantity(request.getQuantity());
				if (cancelled) {
					orderDetailsInfo.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.NA.getName());
					orderDetailsInfo.setPayBy(String.valueOf(PaymentMode.NA.getId()));
					// orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				} else {
					orderDetailsInfo.setOrderStatus(OrderStatus.CONFIRMED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.PENDING.getName());
					// orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				}
				orderDetailsInfo.setCancelled(cancelled);
				orderDetailsInfo.setOrderCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
				// orderDetailsInfo.setPaymentModeId(0);
				orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			}
			if (!cancelled) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
			} else {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
			}

			orderInfoDTO.setCancelled(cancelled);
			orderInfoDTO.setCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
			orderInfoDTO.setTimeSlotId(timeSlotDTO.getId());
			orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
			orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
	}

	private void pauseNextDelivery(ScheduleInfoDTO scheduleDTO) {
		scheduleDTO.setPauseDate(new Timestamp(System.currentTimeMillis()));
		Date pauseDate = getPauseDeliveryDate(scheduleDTO);
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		if (pauseDate != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleDTO.getId(), pauseDate);
			if (orderInfoDTO != null) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
				orderInfoDTO.setCancelled(true);
				orderInfoDTO.setCancellationTime(new Timestamp(System.currentTimeMillis()));
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO
						.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
				for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
					orderDetailsDTO.setCancelled(true);
					orderDetailsDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
					orderDetailsDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsDTO.setPaymentStatus(PaymentStatus.NA.getName());
					// orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
					orderDetailsDTO.setPayBy(String.valueOf(PaymentMode.NA.getId()));
					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}
			}
		}
	}

	private Date getPauseDeliveryDate(ScheduleInfoDTO scheduleDTO) {
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };

		long currentTimeStamp = System.currentTimeMillis();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTimeInMillis(currentTimeStamp);
		Calendar pauseTime = Calendar.getInstance();
		if (scheduleDTO.getPauseDate() != null) {
			pauseTime.setTimeInMillis(scheduleDTO.getPauseDate().getTime());
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			List<Long> timeStampList = new ArrayList<>();
			int startDay = 0;
			int endDay = 7;
			if ((pauseTime.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getStartTime()))) {
				startDay = 0;
			} else {
				startDay = 1;
				endDay = 8;
			}
			for (; startDay < endDay; startDay++) {
				timeStampList.add(pauseTime.getTimeInMillis() + (startDay * 24 * 60 * 60 * 1000));
			}
			Calendar cal = Calendar.getInstance();
			for (Long timestamp : timeStampList) {
				cal.setTimeInMillis(timestamp);

				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") & scheduleDTO.isSu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") & scheduleDTO.isMo()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") & scheduleDTO.isTu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") & scheduleDTO.isWe()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") & scheduleDTO.isTh()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") & scheduleDTO.isFr()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") & scheduleDTO.isSa()) {
					return new Date(timestamp);
				}
			}
		}
		return null;
	}

	private void deleteSchedule(ScheduleInfoDTO scheduleDTO) {
		Long scheduleId = scheduleDTO.getId();
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setActive(false);
		scheduleDTO.setScheduleEndDate(new Date(System.currentTimeMillis()));
		scheduleDTO.setScheduleStatus(scheduleDTO.getScheduleStatus());
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDAO.updateScheduleInfo(scheduleDTO);
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleId);
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			if (commonApplication.getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<Long> orderDetailsInfoIdList = orderDetailsInfoDAO.getIdListByOrderId(orderInfoDTO.getId());
			if (orderDetailsInfoIdList != null && !orderDetailsInfoIdList.isEmpty()
					&& orderDetailsInfoIdList.size() != 0) {
				orderDetailsInfoDAO.deleteorderDetailsInfoList(orderDetailsInfoIdList);
			}
			orderInfoDAO.deleteOrderInfo(orderInfoDTO);
		}
	}

	private void addOrdersForSchedule(String version, Long scheduleId, Date scheduledFrom, Locale locale)
			throws ValidationException {
		System.out.println("New Schedule");
		ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(scheduleId);
		if (scheduleDTO != null) {
			Calendar cal = Calendar.getInstance();
			if (scheduleDTO.getScheduledUpto() != null) {
				cal.setTime(scheduledFrom);
			} else {
				cal.setTime(scheduleDTO.getStartDate());
			}
			if (commonApplication.getZeroTimeDate(new Date(cal.getTime().getTime()))
					.before(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))
					|| commonApplication.getZeroTimeDate(new Date(cal.getTime().getTime()))
							.equals(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				cal.setTimeInMillis(System.currentTimeMillis());
				TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
				if (cal.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime())) {
					cal.add(Calendar.DATE, 1);
				}
			}
			Date monthStartDate = new Date(cal.getTimeInMillis());
			System.out.println(monthStartDate);
			long monthEndTime = 0;
			if (newScheduleEndDate == 0 || version == "V1") {
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MONTH, 1);
				calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				monthEndTime = calendar.getTimeInMillis();
			} else {
				monthEndTime = newScheduleEndDate;
			}
			Date monthEndDate = new Date(monthEndTime);
			System.out.println(monthStartDate);
			System.out.println(monthEndDate);
			if ((scheduleDTO.getScheduledUpto() != null
					&& ((commonApplication.getZeroTimeDate(scheduleDTO.getScheduledUpto())
							.after(commonApplication.getZeroTimeDate(monthEndDate)))
							|| (commonApplication.getZeroTimeDate(scheduleDTO.getScheduledUpto())
									.equals(commonApplication.getZeroTimeDate(monthEndDate)))))) {
				return;
			}
			if (commonApplication.getZeroTimeDate(monthStartDate)
					.after(commonApplication.getZeroTimeDate(monthEndDate))) {
				return;
			}
			scheduleDTO.setScheduledUpto(monthEndDate);
			List<Date> dateList = commonApplication.getDaysBetweenDates(monthStartDate, monthEndDate);
			for (Date deliveryDate : dateList) {
				Calendar c = commonApplication.toCalendar(deliveryDate);
				System.out.println(deliveryDate);
				String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
						"Saturday" };
				// System.out.println("Next day is : " +
				// strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
				if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") && scheduleDTO.isSu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") && scheduleDTO.isMo()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") && scheduleDTO.isTu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") && scheduleDTO.isWe()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") && scheduleDTO.isTh()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") && scheduleDTO.isFr()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") && scheduleDTO.isSa()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				}
			}
		}
	}

	private void addOrderToOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate, Locale locale)
			throws ValidationException {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleInfoDTO.getId(), deliveryDate);
		if (orderInfoDTO == null) {
			Long orderId = addScheduledOrderInfo(scheduleInfoDTO, deliveryDate);
			addScheduledOrderDetailsInfo(orderId, scheduleInfoDTO, locale);
		}
	}

	private Long addScheduledOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {
		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(scheduleInfoDTO.getCustomerId());
		orderDTO.setDeliveryDate(deliveryDate);
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		// orderDTO.setPaymentModeId(0);
		orderDTO.setScheduleId(scheduleInfoDTO.getId());
		orderDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDTO.setCancelled(false);
		orderDTO.setVersion("v2");
		CustomerDTO customerDTO = customerDAO.getById(scheduleInfoDTO.getCustomerId());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		// orderDTO.setFlatId(addressDTO.getFlatId());
		orderDTO.setAddressId(addressDTO.getId());
		/*
		 * orderDTO.setDeliveryLocation("Flat no. " + addressDTO.getFlat() +
		 * ", " + addressDTO.getTower() + ", " + addressDTO.getProject() + ", "
		 * + addressDTO.getCity());
		 */
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		orderDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
		// orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		return orderInfoDAO.addOrderInfo(orderDTO);
	}

	private Long addSchedule(ScheduleProductRequestV2 request, Long customerId, Locale locale) throws Exception {
		ScheduleInfoDTO scheduleDTO = new ScheduleInfoDTO();
		scheduleDTO.setCustomerId(customerId);
		scheduleDTO.setRecurring(request.isRecurring());
		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO.getById(request.getProductDetailsId(), true);
		if (productCityInfoDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		} else {
			if (!productCityInfoDTO.isStockStatus()) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
			}
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO.getById(productCityInfoDTO.getProductId(), true);
			if (productInfoV2DTO == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
			} else {
				if (!productInfoV2DTO.isStockStatus()) {
					throw new ValidationException(ExceptionResourceBundle
							.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
				}
			}
			if (productInfoV2DTO != null && (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus())) {
				String productName = "";
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
					if (productCityInfoDTO.getQuantityUnit() != null) {
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}
				}
				throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
						ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
			}
		}
		scheduleDTO.setProductId((int) productCityInfoDTO.getId().intValue());
		scheduleDTO.setQuantity(request.getQuantity());
		scheduleDTO.setStartDate(new Date(request.getStartDate()));
		List<Integer> orderDays = request.getOrderDays();
		scheduleDTO.setSu(orderDays.contains(0) ? true : false);
		scheduleDTO.setMo(orderDays.contains(1) ? true : false);
		scheduleDTO.setTu(orderDays.contains(2) ? true : false);
		scheduleDTO.setWe(orderDays.contains(3) ? true : false);
		scheduleDTO.setTh(orderDays.contains(4) ? true : false);
		scheduleDTO.setFr(orderDays.contains(5) ? true : false);
		scheduleDTO.setSa(orderDays.contains(6) ? true : false);
		scheduleDTO.setTimeSlotId(request.getTimeSlotId());
		scheduleDTO.setScheduleStatus(ScheduleStatus.RESUME.getId());
		scheduleDTO.setVersion("v2");
		scheduleDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDTO.setActive(true);
		return scheduleDAO.addScheduleInfo(scheduleDTO);

	}

	private void addScheduledOrderDetailsInfo(long orderId, ScheduleInfoDTO scheduleInfoDTO, Locale locale)
			throws ValidationException {

		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null) {

				String productName = "";
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
					if (productCityInfoDTO.getQuantityUnit() != null) {
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}
				}
				if ((!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus())) {
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
							ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
				}
				orderDetailsDTO.setProductId(scheduleInfoDTO.getProductId());
				orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
				orderDetailsDTO.setProductName(productName);
				orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
				orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
				orderDetailsDTO.setProductSalePrice(productCityInfoDTO.getSalePrice());
				orderDetailsDTO.setTotalProductCost(
						(productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
								* scheduleInfoDTO.getQuantity());
				orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() - productCityInfoDTO.getSalePrice());
				orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
			}
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDetailsDTO.setCancelled(false);
		orderDetailsDTO.setOrderCancellationTime(null);
		// orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);

	}

	private GetUpcomingOrderResponse getOrderInfo(Long customerId) {
		GetUpcomingOrderResponse response = new GetUpcomingOrderResponse();

		// fetch past Order dates and get Orders
		List<Date> pastOrderDateList = orderInfoDAO.getPastOrderDates(customerId);
		if (pastOrderDateList != null & pastOrderDateList.size() > 0) {
			for (Date orderdate : pastOrderDateList) {
				System.out.println("pastCheckoutDay " + orderdate);
				ByDeliveryDate timestampInfo = new ByDeliveryDate();
				timestampInfo.setTimestamp(orderdate.getTime());
				List<Integer> timeSlotList = orderInfoDAO.getCheckoutTimeSlotByCustomerDeliveryDate(customerId,
						orderdate);
				for (int timeSlotId : timeSlotList) {
					ByTimeSlot timeSlot = new ByTimeSlot();
					timeSlot.setTimeslotId(timeSlotId);
					timeSlot.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
					timeSlot = checkoutMethod(customerId, timeSlotId, orderdate, timeSlot);
					timestampInfo.getTimeslotList().add(timeSlot);
				}
				response.getOrderInfoList().add(timestampInfo);
			}
		}

		// get today's orders
		List<Long> todaysCheckoutList = orderInfoDAO.getCheckoutIdsOfUpcomingCurrentDayTimeSlots(customerId,
				lastLockingPeriod);

		if ((todaysCheckoutList != null & todaysCheckoutList.size() > 0)) {
			ByDeliveryDate orderDetailsV2 = new ByDeliveryDate();
			orderDetailsV2.setTimestamp(new Timestamp(System.currentTimeMillis()).getTime());
			if (todaysCheckoutList != null & todaysCheckoutList.size() > 0) {
				Calendar rightNow = Calendar.getInstance();
				int hour = rightNow.get(Calendar.HOUR_OF_DAY);
				List<Integer> timeSlotList = orderInfoDAO.getCheckoutTimeSlotsByIds(todaysCheckoutList);
				for (Integer timeSlotId : timeSlotList) {
					ByTimeSlot timeSlotV2 = new ByTimeSlot();
					timeSlotV2.setTimeslotId(timeSlotId);
					TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(timeSlotId);
					if (hour >= timeSlotDTO.getStartTime()) {
						timeSlotV2.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
					} else {
						if (hour + 1 == timeSlotDTO.getStartTime()) {
							timeSlotV2.setDeliveryStatus(OrderStatus.READY_FOR_DELIVERY.getId());
						} else {
							timeSlotV2.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
						}
					}
					timeSlotV2 = checkoutMethod(customerId, timeSlotId, new Date(System.currentTimeMillis()),
							timeSlotV2);
					orderDetailsV2.getTimeslotList().add(timeSlotV2);
				}
			}
			response.getOrderInfoList().add(orderDetailsV2);
		}
		boolean checkout = false;
		List<Date> orderDateList = orderInfoDAO.getNextOrderDate(customerId);
		if (orderDateList != null & orderDateList.size() > 0) {
			for (Date orderdate : orderDateList) {
				checkout = true;
				System.out.println("nextCheckoutDay " + orderdate);
				if (checkout) {
					ByDeliveryDate orderDetailsV2 = new ByDeliveryDate();
					orderDetailsV2.setTimestamp(orderdate.getTime());
					System.out.println("Customer has checkout only in coming days");
					List<Integer> timeSlotList = orderInfoDAO.getCheckoutTimeSlotByCustomerDeliveryDate(customerId,
							orderdate);
					for (int timeSlotId : timeSlotList) {
						ByTimeSlot timeSlotV2 = new ByTimeSlot();
						timeSlotV2.setTimeslotId(timeSlotId);
						timeSlotV2.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
						timeSlotV2 = checkoutMethod(customerId, timeSlotId, orderdate, timeSlotV2);
						orderDetailsV2.getTimeslotList().add(timeSlotV2);
					}
					response.getOrderInfoList().add(orderDetailsV2);
				}
			}
		}
		if (response.getOrderInfoList() == null) {
			response.getOrderInfoList().clear();
		}
		return response;
	}

	private ByTimeSlot checkoutMethod(Long customerId, Integer timeSlotId, Date dateValue, ByTimeSlot timeSlotV2) {
		List<OrderInfoDTO> orderInfoDTOList = orderInfoDAO.getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId(customerId,
				dateValue, timeSlotId);
		for (OrderInfoDTO orderInfo : orderInfoDTOList) {
			List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO.getByOrderId(orderInfo.getId());
			for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
				OrderDetailsInfo orderDetailsInfo = new OrderDetailsInfo();
				orderDetailsInfo.setOrderId(orderInfo.getId());
				orderDetailsInfo.setOrderDetailsId(orderDetailsInfoDTO.getId());
				orderDetailsInfo.setProductDetailsId(orderDetailsInfoDTO.getProductId());
				ProductCityInfoV2DTO productCityDTO = productCityInfoDAO
						.getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId());
				if (productCityDTO == null) {
					continue;
				}
				ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
						.getByIdWithoutActiveState(productCityDTO.getProductId());
				if (productInfoV2DTO == null) {
					continue;
				}
				BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoDTO != null) {
					orderDetailsInfo.setBrandName(brandInfoDTO.getBrand());
				}
				orderDetailsInfo.setProductName(productInfoV2DTO.getProductName());
				orderDetailsInfo.setProductUnit(productCityDTO.getQuantityUnit());
				orderDetailsInfo.setCategoryId(productCityDTO.getCategoryId());
				orderDetailsInfo.setSubCategoryId(productCityDTO.getSubCategoryId());
				orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
				orderDetailsInfo.setSalePrice(orderDetailsInfoDTO.getProductSalePrice());
				orderDetailsInfo.setDeliveryCharge(orderDetailsInfoDTO.getDeliveryCharges());
				orderDetailsInfo.setProductCost(orderDetailsInfoDTO.getTotalProductCost());
				orderDetailsInfo.setStockQuantity(productCityDTO.getStockQuantity());
				orderDetailsInfo.setStockStatus(productCityDTO.isStockStatus());
				orderDetailsInfo.setActive(productCityDTO.isActive());
				orderDetailsInfo.setVersion("v2");
				if (productCityDTO.getCategoryId() == 9) {
					timeSlotV2.setCategoryId(9);
				}
				orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
				timeSlotV2.getOrderDetailsList().add(orderDetailsInfo);
			}
		}
		return timeSlotV2;

	}

	private Float addOrderDetailsInfo(long orderId, ProductDetails request, Locale locale) throws ValidationException {
		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		String productName = "";
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId((int) (long) request.getProductDetailsId());
		orderDetailsDTO.setQuantity(request.getQuantity());
		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(request.getProductDetailsId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null) {
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
				}
			}
			if (productInfoV2DTO != null && (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus())) {
				if (productCityInfoDTO.getQuantityUnit() != null) {
					productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
				}
				throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
						ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
			}
			orderDetailsDTO.setProductName(productName);
			orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
			orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
			orderDetailsDTO.setProductSalePrice(productCityInfoDTO.getSalePrice());
			orderDetailsDTO
					.setTotalProductCost((productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
							* request.getQuantity());
			orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() - productCityInfoDTO.getSalePrice());
			orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDetailsDTO.setCancelled(false);
		// orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		// orderDetailsDTO.setPayBy(String.valueOf(b));
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);
		return orderDetailsDTO.getTotalProductCost();

	}

	private Long addCheckoutOrderInfo(Long customerId, CheckoutInfo request, Locale locale) throws ValidationException {
		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(customerId);
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(new Date(request.getDeliveryTimestamp()));
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeslotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		// orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		// orderDTO.setPayBy(String.valueOf(PaymentMode.NA.getId()));
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setCancelled(false);
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		orderDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
		orderDTO.setRequestedPaymentModeId(request.getPaymentCode());
		if (request.getFlatId() == -1 || request.getFlatId() == 0) {
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.ADDRESS_NOT_REGISTERED, locale));
			} else {
				orderDTO.setAddressId(addressDTO.getId());
			}
		} else {
			AddressDTO addressDTO = addressDAO.getByFlatId(request.getFlatId());
			if (addressDTO == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.USER_NOT_REGISTERED_WITH_ADDRESS, locale));
			} else {
				orderDTO.setAddressId(addressDTO.getId());
			}
		}
		orderDTO.setVersion("v2");
		return orderInfoDAO.addOrderInfo(orderDTO);
	}

	private GetUpcomingOrderResponse getUpcomingOrders(Long customerId) {
		GetUpcomingOrderResponse response = new GetUpcomingOrderResponse();
		// List<OrderInfoDTO> orderInfoList =
		// orderInfoDAO.getOrders(customerId);
		List<UpcomingOrderDTO> orderDetailsList = orderInfoDAO.getOrderDetails(customerId);
		List<Date> deliveryDateList = orderDetailsList.stream().map(UpcomingOrderDTO::getDeliveryDate).distinct()
				.collect(Collectors.toList());
		List<TimeSlotDTO> timeSlotDTOList = timeSlotDAO.getAll();
		for (Date deliveryDate : deliveryDateList) {
			ByDeliveryDate timestampInfo = new ByDeliveryDate();
			timestampInfo.setTimestamp(deliveryDate.getTime());
			Calendar rightNow = Calendar.getInstance();
			int hour = rightNow.get(Calendar.HOUR_OF_DAY);
			List<Integer> timeSlotList = orderDetailsList.stream().filter(e -> e.getDeliveryDate().equals(deliveryDate))
					.map(UpcomingOrderDTO::getTimeSlotId).distinct().collect(Collectors.toList());
			for (Integer timeSlotId : timeSlotList) {
				ByTimeSlot timeSlot = new ByTimeSlot();
				timeSlot.setTimeslotId(timeSlotId);
				if (commonApplication.getZeroTimeDate(deliveryDate)
						.before(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
					timeSlot.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
				} else if (commonApplication.getZeroTimeDate(deliveryDate)
						.equals(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
					TimeSlotDTO timeSlotDTO = timeSlotDTOList.stream().filter(e -> e.getId() == timeSlotId)
							.collect(Collectors.toList()).get(0);
					if (hour >= timeSlotDTO.getStartTime()) {
						timeSlot.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
					} else {
						if (hour + 1 == timeSlotDTO.getStartTime()) {
							timeSlot.setDeliveryStatus(OrderStatus.READY_FOR_DELIVERY.getId());
						} else {
							timeSlot.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
						}
					}
				} else {
					timeSlot.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
				}
				timeSlot = checkoutMethodV2(customerId, timeSlotId, deliveryDate, timeSlot, orderDetailsList);
				timestampInfo.getTimeslotList().add(timeSlot);
			}
			response.getOrderInfoList().add(timestampInfo);
		}

		return response;
	}

	private ByTimeSlot checkoutMethodV2(Long customerId, Integer timeSlotId, Date deliveryDate, ByTimeSlot timeSlot,
			List<UpcomingOrderDTO> orderDetailsList) {
		// List<UpcomingOrderDTO> orderDetailsList =
		// orderInfoDAO.getOrderDetails(customerId, deliveryDate, timeSlotId);
		orderDetailsList = orderDetailsList.stream()
				.filter(e -> e.getDeliveryDate().equals(deliveryDate) && e.getTimeSlotId() == timeSlotId).distinct()
				.collect(Collectors.toList());
		for (int i = 0; i < orderDetailsList.size(); i++) {
			OrderDetailsInfo orderDetailsInfo = new OrderDetailsInfo();
			orderDetailsInfo.setOrderId(orderDetailsList.get(i).getOrderId());
			orderDetailsInfo.setOrderDetailsId(orderDetailsList.get(i).getOrderDetailsId());
			orderDetailsInfo.setProductDetailsId(orderDetailsList.get(i).getProductDetailsId());
			orderDetailsInfo.setProductName(orderDetailsList.get(i).getProductName());
			orderDetailsInfo.setBrandName(orderDetailsList.get(i).getBrandName());
			orderDetailsInfo.setProductUnit(orderDetailsList.get(i).getProductUnit());
			orderDetailsInfo.setCategoryId(orderDetailsList.get(i).getCategoryId());
			orderDetailsInfo.setSubCategoryId(orderDetailsList.get(i).getSubCategoryId());
			orderDetailsInfo.setQuantity(orderDetailsList.get(i).getQuantity());
			orderDetailsInfo.setSalePrice(orderDetailsList.get(i).getSalePrice() == 0
					? orderDetailsList.get(i).getProductSalePrice() : orderDetailsList.get(i).getSalePrice());
			orderDetailsInfo.setDeliveryCharge(orderDetailsList.get(i).getDeliveryCharge());
			orderDetailsInfo
					.setProductCost(orderDetailsList.get(i).getProductCost() == 0
							? ((orderDetailsList.get(i).getProductSalePrice()
									+ orderDetailsList.get(i).getProductDeliveryCharge())
									* orderDetailsList.get(i).getQuantity())
							: orderDetailsList.get(i).getProductCost());
			orderDetailsInfo.setStockQuantity(orderDetailsList.get(i).getStockQuantity());
			orderDetailsInfo.setStockStatus(orderDetailsList.get(i).isStockStatus());
			orderDetailsInfo.setActive(orderDetailsList.get(i).isActive());
			orderDetailsInfo.setVersion("v2");
			if (orderDetailsList.get(i).getCategoryId() == 9) {
				timeSlot.setCategoryId(9);
			}
			timeSlot.getOrderDetailsList().add(orderDetailsInfo);
		}
		/*
		 * List<OrderInfoDTO> orderInfoDTOList =
		 * orderInfoDAO.getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId(
		 * customerId, deliveryDate, timeSlotId); for (OrderInfoDTO orderInfo :
		 * orderInfoDTOList) { List<OrderDetailsInfoDTO> orderDetailsInfoDTOList
		 * = orderDetailsInfoDAO.getByOrderId(orderInfo.getId()); for
		 * (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
		 * OrderDetailsInfo orderDetailsInfo = new OrderDetailsInfo();
		 * orderDetailsInfo.setOrderId(orderInfo.getId());
		 * orderDetailsInfo.setOrderDetailsId(orderDetailsInfoDTO.getId());
		 * orderDetailsInfo.setProductDetailsId(orderDetailsInfoDTO.getProductId
		 * ()); ProductCityInfoV2DTO productCityDTO =
		 * productCityInfoDAO.getByIdWithoutActiveStatus(orderDetailsInfoDTO.
		 * getProductId()); if(productCityDTO == null){ continue; }
		 * ProductInfoV2DTO productInfoV2DTO =
		 * productInfoV2DAO.getByIdWithoutActiveState(productCityDTO.
		 * getProductId()); if(productInfoV2DTO == null){ continue; }
		 * BrandInfoV2DTO brandInfoDTO =
		 * brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
		 * if(brandInfoDTO!= null){
		 * orderDetailsInfo.setBrandName(brandInfoDTO.getBrand()); }
		 * orderDetailsInfo.setProductName(productInfoV2DTO.getProductName());
		 * orderDetailsInfo.setProductUnit(productCityDTO.getQuantityUnit());
		 * orderDetailsInfo.setCategoryId(productCityDTO.getCategoryId());
		 * orderDetailsInfo.setSubCategoryId(productCityDTO.getSubCategoryId());
		 * orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
		 * orderDetailsInfo.setSalePrice(orderDetailsInfoDTO.getProductSalePrice
		 * ()); orderDetailsInfo.setDeliveryCharge(orderDetailsInfoDTO.
		 * getDeliveryCharges());
		 * orderDetailsInfo.setProductCost(orderDetailsInfoDTO.
		 * getTotalProductCost());
		 * orderDetailsInfo.setStockQuantity(productCityDTO.getStockQuantity());
		 * orderDetailsInfo.setStockStatus(productCityDTO.isStockStatus());
		 * orderDetailsInfo.setActive(productCityDTO.isActive());
		 * orderDetailsInfo.setVersion("v2"); if (productCityDTO.getCategoryId()
		 * == 9) { timeSlot.setCategoryId(9); }
		 * orderDetailsInfo.setQuantity(orderDetailsInfoDTO.getQuantity());
		 * timeSlot.getOrderDetailsList().add(orderDetailsInfo); } } return
		 * timeSlot;
		 * 
		 */
		return timeSlot;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final int lastLockingPeriod = Integer
			.parseInt(prop.getProperty(ResourceProperties.LASTLOCKINGPERIOD));
	private static final Long newScheduleEndDate = Long
			.parseLong(prop.getProperty(ResourceProperties.NEW_SCHEDULE_END_DATE));
	private static final int ELEMENTS_PER_PAGE = Integer
			.parseInt(prop.getProperty(ResourceProperties.ELEMENTS_PER_PAGE));

}
