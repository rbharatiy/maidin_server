package com.maidin.customerApi.V2.impl;

import java.io.File;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

import org.joda.time.DateTime;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.maidin.common.exception.MandatoryFieldMissingException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.V2.CustomerMgmt;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.CustomerAuthenticationDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.ImageDAO;
import com.maidin.persistence.dao.OTPVerificationDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;
import com.maidin.pojo.customer.CustomerRegistration;
import com.maidin.pojo.customer.DeviceToken;
import com.maidin.pojo.customer.VerifyMobileOtp;
import com.maidin.pojo.customerv2.CustomerPersonalInfo;
import com.maidin.pojo.customerv2.PersonalInfoResponse;

public class CustomerMgmtImpl implements CustomerMgmt {

	private final ProductCategoryDAO productCategoryDAO;
	private final ProductBrandsDAO productBrandsDAO;
	private final ProductDAO productDAO;
	private final OTPVerificationDAO otpVerificationDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final CustomerAuthenticationDAO customerAuthenticationDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private final CommonApplication commonApplication;
	private final ImageDAO productImageDAO;
	private final WalletDAO walletDAO;
	private final AddressDAO addressDAO;
	private final CustomerDAO customerDAO2;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final PhotoDAO photoDAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;
	private org.json.simple.JSONArray cityJSONArray;

	@Autowired
	public CustomerMgmtImpl(ProductCategoryDAO productCategoryDAO, ProductBrandsDAO productSubCategoryDAO,
			ProductDAO productDAO, OTPVerificationDAO otpVerificationDAO, AuthenticationMgmt authenticationMgmt,
			CustomerDAO customerDAO, CustomerAuthenticationDAO customerAuthenticationDAO,
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO, CommonApplication commonApplication,
			ImageDAO productImageDAO, WalletDAO walletDAO, AddressDAO addressDAO, CustomerDAO customerDAO2, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ProductInfoV2DAO productInfoV2DAO, BrandInfoV2DAO brandInfoV2DAO,
			CategoryInfoV2DAO categoryInfoV2DAO, PhotoDAO photoDAO, CityCategoryInfoDAO cityCategoryInfoDAO) {

		this.productCategoryDAO = productCategoryDAO;
		this.productBrandsDAO = productSubCategoryDAO;
		this.productDAO = productDAO;
		this.otpVerificationDAO = otpVerificationDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.customerAuthenticationDAO = customerAuthenticationDAO;
		this.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
		this.commonApplication = commonApplication;
		this.productImageDAO = productImageDAO;
		this.walletDAO = walletDAO;
		this.addressDAO = addressDAO;
		this.customerDAO2 = customerDAO2;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.photoDAO = photoDAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
		this.cityJSONArray = readCityJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");;

	}

	private static final Logger logger = LoggerFactory.getLogger(CustomerMgmtImpl.class);

	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public PersonalInfoResponse getPersonalInfo(String authToken, Locale locale) throws Exception {
		PersonalInfoResponse response = new PersonalInfoResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
			CustomerPersonalInfo personalInfo = setPersonalInfo(customerDTO);
			response.setCustomerPersonalInfo(personalInfo);
			response.setEmailVerified(customerDTO.isEmailVerified());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
/*
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse setPersonalInfo(CustomerPersonalInfo request, String authToken, Locale locale)
			throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
			setPersonalInfo(customerDTO, request, locale);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}*/

	

	private void setPersonalInfo(CustomerDTO customerDTO, CustomerPersonalInfo request, Locale locale)
			throws ValidationException {

		checkEmailValidation(customerDTO.getEmail(), request.getEmail(), locale);
		checkMobileValidation(customerDTO.getMobileNumber(), request.getMobileNumber(), locale);

		if (!Strings.isNullOrEmpty(request.getFullName())) {
			customerDTO.setFullName(request.getFullName());
		}
		if (!Strings.isNullOrEmpty(request.getEmail())) {
			customerDTO.setEmail(request.getEmail());
			customerDTO.setEmailVerified(false);
		}
		if (!Strings.isNullOrEmpty(request.getMobileNumber())) {
			customerDTO.setMobileNumber(request.getMobileNumber());
		}
		AddressDTO addressDTO = addressDAO.getByFlatId(request.getFlatId());
		if (addressDTO == null) {
			addressDTO = addAddressDetails(request);
		}
		customerDTO.setAddressId(addressDTO.getId());
		if (!Strings.isNullOrEmpty(request.getCity())) {
			addressDTO.setCity(request.getCity());
		}
		if (!Strings.isNullOrEmpty(request.getProject())) {
			addressDTO.setProject(request.getProject());
		}
		if (!Strings.isNullOrEmpty(request.getTower())) {
			addressDTO.setTower(request.getTower());
		}
		if (!Strings.isNullOrEmpty(request.getFlat())) {
			addressDTO.setFlat(request.getFlat());
		}
		addressDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}

	private AddressDTO addAddressDetails(CustomerPersonalInfo request) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setFlatId(request.getFlatId());
		addressDTO.setCity(request.getCity());
		addressDTO.setCityCode(getCityCode(cityJSONArray,request.getCity()));
		addressDTO.setProject(request.getProject());
		addressDTO.setTower(request.getTower());
		addressDTO.setFlat(request.getFlat());
		addressDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		addressDAO.addAddressInfo(addressDTO);
		return addressDTO;
	}

	private void checkMobileValidation(String existingMobile, String updatedMobile, Locale locale)
			throws ValidationException {
		if (!Strings.isNullOrEmpty(existingMobile)) {
			if (!Strings.isNullOrEmpty(updatedMobile)) {
				if (existingMobile.equals(updatedMobile)) {
					/*
					 * throw new ValidationException(
					 * ExceptionResourceBundle.getExceptionCodeProperties(
					 * ExceptionCode.NO_UPDATE_IN_MOBILE, locale));
					 */
				} else {
					CustomerDTO dto = customerDAO.getCustomerByMobileNumber(updatedMobile);
					if (dto != null) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.MOBILE_ALREADY_IN_USE, locale));
					}
				}
			}

		} else {
			CustomerDTO dto = customerDAO.getCustomerByMobileNumber(updatedMobile);
			if (dto != null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.MOBILE_ALREADY_IN_USE, locale));
			}
		}

	}

	private void checkEmailValidation(String existingEmail, String updatedEmail, Locale locale)
			throws ValidationException {
		if (!Strings.isNullOrEmpty(existingEmail)) {
			if (!Strings.isNullOrEmpty(updatedEmail)) {
				if (existingEmail.equals(updatedEmail)) {
					/*
					 * throw new ValidationException(
					 * ExceptionResourceBundle.getExceptionCodeProperties(
					 * ExceptionCode.NO_UPDATE_IN_EMAIL, locale));
					 */} else {
					CustomerDTO dto = customerDAO.getCustomerByEmail(updatedEmail);
					if (dto != null) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
					}

				}
			}

		} else {
			CustomerDTO dto = customerDAO.getCustomerByEmail(updatedEmail);
			if (dto != null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
			}
		}

	}

	private CustomerPersonalInfo setPersonalInfo(CustomerDTO customerDTO) {
		CustomerPersonalInfo personalInfo = new CustomerPersonalInfo();
		personalInfo.setCustomerId(String.valueOf(customerDTO.getId()));
		personalInfo.setFullName(customerDTO.getFullName());
		personalInfo.setGender(customerDTO.getGender());
		personalInfo.setEmail(customerDTO.getEmail());
		personalInfo.setMobileNumber(customerDTO.getMobileNumber());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		personalInfo.setCityCode(addressDTO.getCityCode());
		personalInfo.setProject(addressDTO.getProject());
		personalInfo.setTower(addressDTO.getTower());
		personalInfo.setFlat(addressDTO.getFlat());
		personalInfo.setCity(addressDTO.getCity());
		personalInfo.setFlatId(addressDTO.getFlatId());
		/*
		 * personalInfo.setProject(customerDTO.getProject());
		 * personalInfo.setTower(customerDTO.getTower());
		 * personalInfo.setFlat(customerDTO.getFlat());
		 */

		return personalInfo;
	}

	private void updateCustomerDeviceDetails(DeviceToken request, CustomerDeviceDetailsDTO devicedto, Long customerId) {
		devicedto.setCustomerId(customerId);
		devicedto.setDeviceModel(request.getDeviceModel());
		devicedto.setDevicePlatform(request.getDevicePlatform());
		devicedto.setDeviceToken(request.getDeviceToken());
		devicedto.setDevicePlatformVersion(request.getDevicePlatformVersion());
		devicedto.setAppVersion(request.getAppVersion());
		devicedto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		devicedto.setNetworkCarrier(request.getNetworkCarrier());
		devicedto.setNetworkType(request.getNetworkType());
		customerDeviceDetailsDAO.updateDeviceDetails(devicedto);

	}

	private Long addCustomerDeviceDetails(DeviceToken request, Long customerId) {
		CustomerDeviceDetailsDTO dtoken = new CustomerDeviceDetailsDTO();
		dtoken.setCustomerId(customerId);
		dtoken.setDeviceModel(request.getDeviceModel());
		dtoken.setDevicePlatform(request.getDevicePlatform());
		dtoken.setDeviceToken(request.getDeviceToken());
		dtoken.setDevicePlatformVersion(request.getDevicePlatformVersion());
		dtoken.setAppVersion(request.getAppVersion());
		dtoken.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return customerDeviceDetailsDAO.addDeviceDetails(dtoken);
	}

	private void updateCustomerAuthenticationInfo(CustomerDTO customerDTO, CustomerAuthenticationDTO customerAuthDTO,
			String mobileNumber, String authToken) {
		if (customerDTO != null) {
			customerAuthDTO.setCustomerId(customerDTO.getId());
		}
		customerAuthDTO.setMobileNumber(mobileNumber);
		customerAuthDTO.setAuthToken(authToken);
		customerAuthenticationDAO.updateToken(customerAuthDTO);

	}

	private void addCustomerAuthenticationInfo(CustomerDTO customerDTO, String mobileNumber, String authToken) {
		CustomerAuthenticationDTO customerAuth = new CustomerAuthenticationDTO();
		if (customerDTO != null) {
			customerAuth.setCustomerId(customerDTO.getId());
		}
		customerAuth.setMobileNumber(mobileNumber);
		customerAuth.setAuthToken(authToken);
		customerAuthenticationDAO.addToken(customerAuth);

	}

	

	private void checkIfEmailAlreadyRegistered(String email, Locale locale) throws ValidationException {
		CustomerDTO tdto = customerDAO.getCustomerByEmail(email);
		if (tdto != null)
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
	}

	private boolean validateRecoveryCodeTime(DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (currentTime.isAfter(userCodeExpireTime)) {
			return false;
		}
		return true;
	}

	private void checkConstraint(VerifyMobileOtp request, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		// entityCommon.checkOTP(request.getOtpCode(), locale);
		// entityCommon.checkMobileNumber(request.getMobileNumber(), locale);
	}

	private void validateRecoveryCode(String recoveryCode, String codeInDB, DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (codeInDB == null || !codeInDB.equals(recoveryCode) || currentTime.isAfter(userCodeExpireTime)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_CODE, locale));
		}
	}

	private void checkRegistrationConstraint(CustomerRegistration request, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		// entityCommon.checkFirstName(request.getFirstName(), locale);

	}

	
	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		List<ProductDTO> pdto = productDAO.getAllActiveMatchingProducts(request.getProductSubString());
		if (pdto == null) {
		} else {
			for (ProductDTO dto : pdto) {
				SearchedProductDetails searchedProductDetails = new SearchedProductDetails();
				searchedProductDetails.setProductId(dto.getId());
				searchedProductDetails.setProductName(dto.getProductName());
				searchedProductDetails.setQuantityUnit(dto.getQuantityUnit());
				searchedProductDetails.setPrice(dto.getPrice());
				searchedProductDetails.setActiveStatus(dto.isActive());

				// TODO: make these dynamic
				searchedProductDetails.setStockStatus(true);
				searchedProductDetails.setDemandStatus(true);

				if (dto.getBrandId() == 0 || dto.getBrandId() == null) {
					continue;
				}

				ProductBrandsDTO brandsDTO = productBrandsDAO.getById(dto.getBrandId());
				searchedProductDetails.setBrandName(brandsDTO.getBrand());
				searchedProductDetails.setBrandId(brandsDTO.getId());

				ProductCategoryDTO categoryDTO = productCategoryDAO.getById(brandsDTO.getCategoryId());
				searchedProductDetails.setCategoryName(categoryDTO.getCategory());
				searchedProductDetails.setCategoryId(categoryDTO.getId());

				ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.PRODUCT.getName(),
						dto.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					searchedProductDetails.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
				response.getProductList().add(searchedProductDetails);
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}*/
	
	private org.json.simple.JSONArray readCityJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray cityArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			cityArray = (org.json.simple.JSONArray) jsonObject.get("cities");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cityArray;
	}
	
	private String getCityCode(org.json.simple.JSONArray cityArray, String city) {
		String paymentMode = null;
		for (int i = 0; i < cityArray.size(); i++) {
			org.json.simple.JSONObject cityJSON = (org.json.simple.JSONObject) cityArray.get(i);
			if ((String) cityJSON.get("city") == (city)) {
				return (String) cityJSON.get("id");

			}

		}
		return paymentMode;

	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String fcmKeyCustomer = prop.getProperty(ResourceProperties.FCM_KEY_CUSTOMER);
	private static final String testUserMobileNumber = prop.getProperty(ResourceProperties.TEST_USER_MOBILE_NUMBER);

}

final class CodeGenerator {

	public static String generateCode() {
		Random random = new Random();
		int n = 1000 + random.nextInt(9000);
		return Integer.toString(n);
	}

	private CodeGenerator() {
		/**/}
}