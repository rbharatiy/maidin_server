package com.maidin.customerApi.V2.impl;

import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.V2.SearchMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.PopularSearchesDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SearchQueryDAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SearchQueryDTO;
import com.maidin.pojo.search.ProductInfoV2;
import com.maidin.pojo.search.QuantityUnitInfo;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;
import com.maidin.pojo.search.SearchSuggestion;
import com.maidin.pojo.search.SearchSuggestionsResponse;

public class SearchMgmtImpl implements SearchMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	// private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final PhotoDAO photoDAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final SearchQueryDAO searchQueryDAO;
	private final PopularSearchesDAO popularSearchesDAO;

	@Autowired
	public SearchMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			// AdminDAO adminDAO,
			OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO, ProductInfoV2DAO productInfoV2DAO,
			AddressDAO addressDAO, CustomerTransactionDAO transactionDAO, ProductCityInfoV2DAO productCityInfoV2DAO,
			ScheduleInfoDAO scheduleInfoDAO, TimeSlotDAO timeSlotDAO, BrandInfoV2DAO brandInfoV2DAO, PhotoDAO photoDAO,
			CategoryInfoV2DAO categoryInfoV2DAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			SearchQueryDAO searchQueryDAO, PopularSearchesDAO popularSearchesDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		// this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.scheduleInfoDAO = scheduleInfoDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.photoDAO = photoDAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.searchQueryDAO = searchQueryDAO;
		this.popularSearchesDAO = popularSearchesDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(SearchMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String searchSuggestionsResultCount = prop
			.getProperty(ResourceProperties.SEARCH_SUGGESTIONS_RESULT_COUNT);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();

		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		}
		if (request.getCityCode() == null) {
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			request.setCityCode(addressDTO.getCityCode());
		}
		long startTime = System.currentTimeMillis();
		System.out.println("Start Time:  " + new Date(System.currentTimeMillis()));
		// boolean isProductAvailable = true;
		List<ProductInfoV2DTO> productInfoList = new ArrayList<>();
		/*
		 * List<ProductInfoV2DTO> productInfoList1 =
		 * productInfoV2DAO.getAllMatchingProductsWithActivationStatus(request.
		 * getProductSubString(),true); if(productInfoList1.size() >= 0){
		 * productInfoList.addAll(productInfoList1); String[] productString =
		 * request.getProductSubString().split(" "); List<String> productStringList =
		 * Arrays.stream(productString).collect(Collectors.toList()); StringJoiner
		 * joiner_searchString = new StringJoiner("%","%","%"); for(String s:
		 * productStringList) joiner_searchString.add(s); List<ProductInfoV2DTO>
		 * productInfoList2 =
		 * productInfoV2DAO.getAllMatchingProductsByBrandWithActivationStatus(
		 * joiner_searchString.toString(),true); if(productInfoList2.size() == 0 &&
		 * productInfoList1.size() == 0){ response.getProductList().clear();
		 * isProductAvailable = false; } else if(productInfoList2.size() > 0){
		 * productInfoList.addAll(productInfoList2); } }
		 */
		if (!Strings.isNullOrEmpty(request.getProductSubString())) {

			// save query in database
			saveSearchQuery(request.getProductSubString());

			String[] productString = request.getProductSubString().split(" ");
			List<String> productStringList = Arrays.stream(productString).collect(Collectors.toList());
			StringJoiner joiner_searchString = new StringJoiner("%", "%", "%");
			for (String s : productStringList)
				joiner_searchString.add(s);

//			productInfoList = productInfoV2DAO
//					.getAllMatchingProductsByBrandWithActivationStatus(joiner_searchString.toString(), true);

			productInfoList = productInfoV2DAO.getActiveProductsBySubCategoryName(joiner_searchString.toString(), true,
					request.getCityCode());
			Collections.sort(productInfoList, new Comparator<ProductInfoV2DTO>() {
				@Override
				public int compare(ProductInfoV2DTO o1, ProductInfoV2DTO o2) {
					return o1.getProductName().compareTo(o2.getProductName());
				}
			});

			// if(isProductAvailable){
			long endTime = System.currentTimeMillis();
			System.out.println("End Time:  " + new Date(System.currentTimeMillis()));
			System.out.println("Duration :  " + (endTime - startTime) / 1000 + "s");

//			if (productInfoList.isEmpty()) {
//				productInfoList = productInfoV2DAO.getActiveProductsBySubCategoryName(joiner_searchString.toString(),
//						true, request.getCityCode());
//			}

			if (productInfoList.size() == 0) {
			} else {
				List<Integer> productIdList = productInfoList.stream().map(ProductInfoV2DTO::getId).distinct()
						.collect(Collectors.toList());

				List<Integer> brandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
						.collect(Collectors.toList());
				List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
				if (brandIdList != null && !brandIdList.isEmpty() && brandIdList.size() != 0) {
					brandInfoList = brandInfoV2DAO.getByBrandIds(brandIdList);
				}
				List<ProductCityInfoV2DTO> productCityV2List = new ArrayList<>();
				productCityV2List = productCityInfoV2DAO.getByproductIdsAndCityCodeWithActivationStatus(productIdList,
						request.getCityCode(), true);
				for (ProductInfoV2DTO dto : productInfoList) {
					ProductInfoV2 pInfoV2 = new ProductInfoV2();
					pInfoV2.setProductId(dto.getId());
					pInfoV2.setProductName(dto.getProductName());
					pInfoV2.setDescription(dto.getDescription() == null ? "" : dto.getDescription());
					pInfoV2.setFoodType(dto.getFoodType());
					pInfoV2.setBrandId(dto.getBrandId());
					pInfoV2.setBrandName(brandInfoList.stream().filter(e -> e.getId().equals(dto.getBrandId()))
							.map(BrandInfoV2DTO::getBrand).findFirst().orElse(""));
					List<ProductCityInfoV2DTO> productDetailsList = productCityV2List.stream()
							.filter(e -> e.getProductId().equals(dto.getId())).collect(Collectors.toList());
					if (productDetailsList.size() == 0) {
						continue;
					}
					for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
						QuantityUnitInfo quInfo = new QuantityUnitInfo();
						quInfo.setProductDetailsId(pCityInfoV2DTO.getId());
						quInfo.setSetByDefault(pCityInfoV2DTO.isSetByDefault());
						quInfo.setMRP(pCityInfoV2DTO.getMRP());
						quInfo.setSalePrice(pCityInfoV2DTO.getSalePrice());
						quInfo.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
						quInfo.setDiscountUnit(pCityInfoV2DTO.getDiscountUnit());
						if (pCityInfoV2DTO.getDiscountUnit().equals("%")) {
							int discount = Math
									.round((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
											/ pCityInfoV2DTO.getMRP()));
							if (discount == 0 && ((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0)) {
								quInfo.setDiscountUnit("₹");
								quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
							} else {
								quInfo.setDiscount(
										(int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
												/ pCityInfoV2DTO.getMRP()));
							}
						} else if (pCityInfoV2DTO.getDiscountUnit().equals("₹")) {
							quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
						}
						quInfo.setSavedAmount((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0
								? pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()
								: 0);
						quInfo.setDeliveryCharge(pCityInfoV2DTO.getDeliveryCharge());
						quInfo.setActive(pCityInfoV2DTO.isActive());
						quInfo.setStockStatus(pCityInfoV2DTO.isStockStatus());
						if (pCityInfoV2DTO.getPhotoId() != null) {
							quInfo.setPhotoId(pCityInfoV2DTO.getPhotoId() == null ? 0 : pCityInfoV2DTO.getPhotoId());
							PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
							if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
								quInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
							}
						}
						pInfoV2.setSubCategoryId(pCityInfoV2DTO.getSubCategoryId());
						pInfoV2.setCategoryId(pCityInfoV2DTO.getCategoryId());
						pInfoV2.getProductDetailList().add(quInfo);
					}
					response.getProductList().add(pInfoV2);
				}
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList1(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();

		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
			// logger.info("Customer: " + customerDTO.getId(), "authToken :" + authToken);
		}
		if (request.getCityCode() == null) {
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			request.setCityCode(addressDTO.getCityCode());
		}
		List<Integer> categoryIdList = categoryInfoV2DAO
				.getAllMatchingTextWithActivationStatus(request.getProductSubString(), true);
		List<Integer> subCategoryIdList = subCategoryInfoV2DAO
				.getAllMatchingTextWithActivationStatus(request.getProductSubString(), true);
		List<Integer> productIdList = productInfoV2DAO
				.getAllMatchingTextWithActivationStatus(request.getProductSubString(), true);
		List<Integer> brandIdList = brandInfoV2DAO.getAllMatchingTextWithActivationStatus(request.getProductSubString(),
				true);
		if (categoryIdList.size() == 0) {
			categoryIdList.add(0);
		}
		if (subCategoryIdList.size() == 0) {
			subCategoryIdList.add(0);
		}
		if (productIdList.size() == 0) {
			productIdList.add(0);
		}
		if (brandIdList.size() == 0) {
			brandIdList.add(0);
		}
		List<ProductCityInfoV2DTO> productCityV2List = new ArrayList<>();
		productCityV2List = productCityInfoV2DAO.getByMatchingIdsAndCityCode(categoryIdList, subCategoryIdList,
				brandIdList, productIdList, request.getCityCode());

		List<Integer> allMatchedProductIdList = productCityV2List.stream().map(ProductCityInfoV2DTO::getProductId)
				.collect(Collectors.toList());

		List<ProductInfoV2DTO> productInfoList = productInfoV2DAO.getByProductIds(allMatchedProductIdList, 0, 10);

		List<Integer> allMatchedBrandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
				.collect(Collectors.toList());
		List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
		if (allMatchedBrandIdList != null && !allMatchedBrandIdList.isEmpty() && allMatchedBrandIdList.size() != 0) {
			brandInfoList = brandInfoV2DAO.getByBrandIds(allMatchedBrandIdList);
		}

		for (ProductInfoV2DTO dto : productInfoList) {
			ProductInfoV2 pInfoV2 = new ProductInfoV2();
			pInfoV2.setProductId(dto.getId());
			pInfoV2.setProductName(dto.getProductName());
			pInfoV2.setDescription(dto.getDescription() == null ? "" : dto.getDescription());
			pInfoV2.setFoodType(dto.getFoodType());
			pInfoV2.setBrandId(dto.getBrandId());
			pInfoV2.setBrandName(brandInfoList.stream().filter(e -> e.getId().equals(dto.getBrandId()))
					.map(BrandInfoV2DTO::getBrand).findFirst().orElse(""));
			List<ProductCityInfoV2DTO> productDetailsList = productCityV2List.stream()
					.filter(e -> e.getProductId().equals(dto.getId())).collect(Collectors.toList());
			if (productDetailsList.size() == 0) {
				continue;
			}
			for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
				QuantityUnitInfo quInfo = new QuantityUnitInfo();
				quInfo.setProductDetailsId(pCityInfoV2DTO.getId());
				quInfo.setSetByDefault(pCityInfoV2DTO.isSetByDefault());
				quInfo.setMRP(pCityInfoV2DTO.getMRP());
				quInfo.setSalePrice(pCityInfoV2DTO.getSalePrice());
				quInfo.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
				quInfo.setDiscountUnit(pCityInfoV2DTO.getDiscountUnit());
				if (pCityInfoV2DTO.getDiscountUnit().equals("%")) {
					int discount = Math.round((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
							/ pCityInfoV2DTO.getMRP()));
					if (discount == 0 && ((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0)) {
						quInfo.setDiscountUnit("₹");
						quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
					} else {
						quInfo.setDiscount((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
								/ pCityInfoV2DTO.getMRP()));
					}
				} else if (pCityInfoV2DTO.getDiscountUnit().equals("₹")) {
					quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
				}
				quInfo.setSavedAmount((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0
						? pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()
						: 0);
				quInfo.setDeliveryCharge(pCityInfoV2DTO.getDeliveryCharge());
				quInfo.setStockStatus(pCityInfoV2DTO.isStockStatus());
				if (pCityInfoV2DTO.getPhotoId() != null) {
					quInfo.setPhotoId(pCityInfoV2DTO.getPhotoId() == null ? 0 : pCityInfoV2DTO.getPhotoId());
					PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
					if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
						quInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
					}
				}
				pInfoV2.setSubCategoryId(pCityInfoV2DTO.getSubCategoryId());
				pInfoV2.setCategoryId(pCityInfoV2DTO.getCategoryId());
				pInfoV2.getProductDetailList().add(quInfo);
			}
			response.getProductList().add(pInfoV2);
		}

		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	public void saveSearchQuery(String query) {
		SearchQueryDTO fetchedDTO = searchQueryDAO.getByQuery(query);
		if (fetchedDTO == null) {
			SearchQueryDTO searchQueryDTO = new SearchQueryDTO();
			searchQueryDTO.setQuery(query);
			searchQueryDTO.setCount(1L);
			searchQueryDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			searchQueryDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			searchQueryDTO.setActive(true);

			searchQueryDAO.addSearchQuery(searchQueryDTO);
		} else {
			fetchedDTO.setCount(fetchedDTO.getCount() + 1);

			searchQueryDAO.updateQuery(fetchedDTO);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<PopularSearchesDTO> getActivePopularSearchKeywords(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info("customerId: " + customerDTO.getId());
		}
		List<PopularSearchesDTO> popularSearchesList = popularSearchesDAO.getActiveKeywords();
		return popularSearchesList;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchSuggestionsResponse getSearchSuggestions(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		}
		if (request.getCityCode() == null) {
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			request.setCityCode(addressDTO.getCityCode());
		}
		SearchSuggestionsResponse response = new SearchSuggestionsResponse();

		List<ProductInfoV2DTO> productInfoList = new ArrayList<>();

		if (!Strings.isNullOrEmpty(request.getProductSubString())) {

			String[] productString = request.getProductSubString().split(" ");
			List<String> productStringList = Arrays.stream(productString).collect(Collectors.toList());
			StringJoiner joiner_searchString = new StringJoiner("%", "%", "%");
			for (String s : productStringList)
				joiner_searchString.add(s);

			productInfoList = productInfoV2DAO.getActiveProductsForSuggestions(joiner_searchString.toString(), true,
					request.getCityCode(), Integer.parseInt(searchSuggestionsResultCount));

			List<SearchSuggestion> searchSuggestionList = new ArrayList<SearchSuggestion>();

			for (ProductInfoV2DTO infoV2DTO : productInfoList) {
				SearchSuggestion searchSuggestion = new SearchSuggestion();
				searchSuggestion.setBrandName(brandInfoV2DAO.getByBrandId(infoV2DTO.getBrandId()).getBrand());
				searchSuggestion.setProductName(infoV2DTO.getProductName());

				searchSuggestionList.add(searchSuggestion);
			}

			Collections.sort(searchSuggestionList, new Comparator<SearchSuggestion>() {
				@Override
				public int compare(SearchSuggestion o1, SearchSuggestion o2) {
					return o1.getBrandName().compareTo(o2.getBrandName());
				}
			});

			response.getSuggestionsList().addAll(searchSuggestionList);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

}
