package com.maidin.customerApi.customer;

import java.io.File;
import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.customer.SearchProductListResponse;
import com.maidin.pojo.customer.SearchProductRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.cart.WalletInfo;
import com.maidin.pojo.common.StoreFile;
import com.maidin.pojo.customer.CustomerLogoutResponse;
import com.maidin.pojo.customer.CustomerPersonalInfo;
import com.maidin.pojo.customer.CustomerRegistration;
import com.maidin.pojo.customer.CustomerRegistrationResponse;
import com.maidin.pojo.customer.DeviceToken;
import com.maidin.pojo.customer.DeviceTokenResponse;
import com.maidin.pojo.customer.GenerateMobileOtp;
import com.maidin.pojo.customer.GenerateMobileOtpResponse;
import com.maidin.pojo.customer.GetGuestUserInfoResponse;
import com.maidin.pojo.customer.PersonalInfoResponse;
import com.maidin.pojo.customer.RegenerateAuthToken;
import com.maidin.pojo.customer.RegenerateAuthTokenResponse;
import com.maidin.pojo.customer.VerifyMobileOtp;
import com.maidin.pojo.customer.VerifyMobileOtpResponse;
import com.maidin.pojo.product.BrandListResponse;
import com.maidin.pojo.product.CategoryListResponse;
import com.maidin.pojo.product.DownloadPhotoResponse;
import com.maidin.pojo.product.GetBrandRequest;
import com.maidin.pojo.product.GetProductRequest;
import com.maidin.pojo.product.ProductListResponse;

public interface CustomerMgmt {

	public GenerateMobileOtpResponse initiateGenerateMobileOTP(GenerateMobileOtp request, Locale locale) throws Exception;

	public VerifyMobileOtpResponse initiateVerifyMobileOtp(VerifyMobileOtp request, Locale locale) throws Exception;

	public CustomerLogoutResponse logout(String authToken, Locale locale) throws Exception;

	public ProductListResponse getProductList(String authToken, GetProductRequest request, Locale locale) throws Exception;

	public RegenerateAuthTokenResponse regenAuthToken(RegenerateAuthToken request, Locale locale) throws Exception;

	public DeviceTokenResponse saveDeviceToken(DeviceToken request, String authToken, Locale locale) throws Exception;

	public PersonalInfoResponse getPersonalInfo(String authToken, Locale locale) throws Exception;

	public GenericResponse setPersonalInfo(CustomerPersonalInfo request, String authToken, Locale locale) throws Exception;

	public DownloadPhotoResponse getProductImage(String imageCategory, /*String authToken, */Long imageCategoryId, Locale locale) throws Exception;

	public GenericResponse savePhoto(String authToken, File file, String imageCategory, Long imageCategoryId, StoreFile storeFile, Locale locale) throws Exception;

	public CategoryListResponse getCategory(String authToken, Locale locale) throws Exception;

	public BrandListResponse getBrands(String authToken, GetBrandRequest request, Locale locale) throws Exception;

	public GetWalletResponse getWalletInfo(String authToken, Locale locale) throws Exception;

	public GetWalletResponse upsertWalletInfo(String authToken, WalletInfo request, Locale locale)  throws Exception;

	public CustomerRegistrationResponse registration(CustomerRegistration request, String authToken, Locale locale) throws Exception;

	public GetGuestUserInfoResponse getGuestUserInfo(Locale locale) throws Exception;

	public CategoryListResponse getV2Category(String authToken, Locale locale)  throws Exception;

	//public GenericResponse scheduleDelivery(CartRequest request, String authToken, Locale locale) throws Exception;
	
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale) throws Exception;

}
