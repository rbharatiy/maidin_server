package com.maidin.customerApi.customer.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.joda.time.DateTime;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.amazonaws.util.Base64;
import com.amazonaws.util.json.JSONObject;
import com.google.common.base.Strings;
import com.google.common.io.ByteSource;
import com.maidin.common.exception.MandatoryFieldMissingException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.SmsSender;
import com.maidin.customerApi.customer.CustomerMgmt;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.enums.UserCategory;
import com.maidin.customerApi.enums.WalletDescription;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.CustomerAuthenticationDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.ImageDAO;
import com.maidin.persistence.dao.OTPVerificationDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;
import com.maidin.persistence.dto.ImageDTO;
import com.maidin.persistence.dto.OTPVerificationDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.cart.WalletInfo;
import com.maidin.pojo.common.StoreFile;
import com.maidin.pojo.customer.CustomerLogoutResponse;
import com.maidin.pojo.customer.CustomerPersonalInfo;
import com.maidin.pojo.customer.CustomerRegistration;
//import com.maidin.pojo.customer.CustomerRegistration1;
import com.maidin.pojo.customer.CustomerRegistrationResponse;
import com.maidin.pojo.customer.DeviceToken;
import com.maidin.pojo.customer.DeviceTokenResponse;
import com.maidin.pojo.customer.GenerateMobileOtp;
import com.maidin.pojo.customer.GenerateMobileOtpResponse;
import com.maidin.pojo.customer.GetGuestUserInfoResponse;
import com.maidin.pojo.customer.PersonalInfoResponse;
//import com.maidin.pojo.customer.ProductDetails;
import com.maidin.pojo.customer.RegenerateAuthToken;
import com.maidin.pojo.customer.RegenerateAuthTokenResponse;
import com.maidin.pojo.customer.SearchProductListResponse;
import com.maidin.pojo.customer.SearchProductRequest;
import com.maidin.pojo.customer.SearchedProductDetails;
import com.maidin.pojo.customer.VerifyMobileOtp;
import com.maidin.pojo.customer.VerifyMobileOtpResponse;
import com.maidin.pojo.inventory.ProductCategoryV2;
import com.maidin.pojo.inventory.ProductInfo;
import com.maidin.pojo.inventory.ProductInfoV2;
import com.maidin.pojo.inventory.QuantityUnitInfo;
import com.maidin.pojo.inventory.SubCategoryListV2Response;
import com.maidin.pojo.inventory.SubCategoryV2;
import com.maidin.pojo.product.BrandListResponse;
import com.maidin.pojo.product.CategoryListResponse;
import com.maidin.pojo.product.DownloadPhotoResponse;
import com.maidin.pojo.product.GetBrandRequest;
import com.maidin.pojo.product.GetProductRequest;
import com.maidin.pojo.product.ProductBrands;
import com.maidin.pojo.product.ProductCategory;
import com.maidin.pojo.product.ProductDetails;
import com.maidin.pojo.product.ProductListResponse;

public class CustomerMgmtImpl implements CustomerMgmt {

	private final ProductCategoryDAO productCategoryDAO;
	private final ProductBrandsDAO productBrandsDAO;
	private final ProductDAO productDAO;
	private final OTPVerificationDAO otpVerificationDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final CustomerAuthenticationDAO customerAuthenticationDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private final CommonApplication commonApplication;
	private final ImageDAO productImageDAO;
	private final WalletDAO walletDAO;
	private final AddressDAO addressDAO;
	private final CustomerDAO customerDAO2;
	private final PhotoDAO photoDAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;
	private org.json.simple.JSONArray cityJSONArray;


	@Autowired
	public CustomerMgmtImpl(ProductCategoryDAO productCategoryDAO, ProductBrandsDAO productSubCategoryDAO,
			ProductDAO productDAO, OTPVerificationDAO otpVerificationDAO, AuthenticationMgmt authenticationMgmt,
			CustomerDAO customerDAO, CustomerAuthenticationDAO customerAuthenticationDAO,
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO, CommonApplication commonApplication,
			ImageDAO productImageDAO, WalletDAO walletDAO, AddressDAO addressDAO, CustomerDAO customerDAO2,
			PhotoDAO photoDAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ProductInfoV2DAO productInfoV2DAO, BrandInfoV2DAO brandInfoV2DAO,
			CategoryInfoV2DAO categoryInfoV2DAO, CityCategoryInfoDAO cityCategoryInfoDAO) {


		this.productCategoryDAO = productCategoryDAO;
		this.productBrandsDAO = productSubCategoryDAO;
		this.productDAO = productDAO;
		this.otpVerificationDAO = otpVerificationDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.customerAuthenticationDAO = customerAuthenticationDAO;
		this.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
		this.commonApplication = commonApplication;
		this.productImageDAO = productImageDAO;
		this.walletDAO = walletDAO;
		this.addressDAO = addressDAO;
		this.customerDAO2 = customerDAO2;
		this.photoDAO = photoDAO;

		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
		this.cityJSONArray = readCityJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");;


	}

	private static final Logger logger = LoggerFactory.getLogger(CustomerMgmtImpl.class);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenerateMobileOtpResponse initiateGenerateMobileOTP(GenerateMobileOtp request, Locale locale)
			throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, ApiUtils.toJsonString(request));
		GenerateMobileOtpResponse response = new GenerateMobileOtpResponse();
		// entityCommon.checkMobileNumber(request.getMobileNumber(), locale);
		String code = "1234";
		if (!request.getMobileNumber().equals(testUserMobileNumber)) {
			code = CodeGenerator.generateCode();
		}

		String message = "Your unique verification code for MAIDIN is " + code;
		OTPVerificationDTO OTPdto = otpVerificationDAO.getByMobile(request.getMobileNumber(),
				UserCategory.CUSTOMER.getName());

		if (OTPdto == null) {
			OTPVerificationDTO dto = new OTPVerificationDTO();
			dto.setMobileNumber(request.getMobileNumber());
			dto.setOtp(code);
			dto.setGeneratedOn(new Timestamp(System.currentTimeMillis()));
			dto.setUsercategory(UserCategory.CUSTOMER.getName());
			otpVerificationDAO.saveOTPInfo(dto);
			SmsSender.sendVerifySMS(request.getMobileNumber(), message);
			System.out.println("New OTP : " + code);
		} else {
			DateTime codeExpireTime = new DateTime(OTPdto.getGeneratedOn()).plusMinutes(15);
			if (validateRecoveryCodeTime(codeExpireTime, locale)) {
				message = "Your unique verification code for MAIDIN is " + OTPdto.getOtp();
				SmsSender.sendVerifySMS(request.getMobileNumber(), message);
				System.out.println("OTP as earlier : " + OTPdto.getOtp());
			} else {
				if (!request.getMobileNumber().equals("2233445566")) {
					code = CodeGenerator.generateCode();
				}
				// code = CodeGenerator.generateCode();
				message = "Your unique verification code for MAIDIN is " + code;
				OTPdto.setOtp(code);
				OTPdto.setGeneratedOn(new Timestamp(System.currentTimeMillis()));
				OTPdto.setUsercategory(UserCategory.CUSTOMER.getName());
				otpVerificationDAO.updateOTPInfo(OTPdto);
				SmsSender.sendVerifySMS(request.getMobileNumber(), message);
				System.out.println("Updated OTP as time expires: " + code);
			}

		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public VerifyMobileOtpResponse initiateVerifyMobileOtp(VerifyMobileOtp request, Locale locale) throws Exception {

		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, ApiUtils.toJsonString(request));

		VerifyMobileOtpResponse response = new VerifyMobileOtpResponse();
		checkConstraint(request, locale);
		OTPVerificationDTO dto = otpVerificationDAO.getByMobile(request.getMobileNumber(),
				UserCategory.CUSTOMER.getName());
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_MOBILE_NUMBER, locale));
		} else {
			OTPVerificationDTO otpdto = otpVerificationDAO.getByMobileAndOtp(request.getMobileNumber(),
					request.getOtpCode(), UserCategory.CUSTOMER.getName());
			if (otpdto == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_OTP, locale));
			} else {
				String codeInDB = otpdto.getOtp();
				DateTime codeExpireTime = new DateTime(otpdto.getGeneratedOn()).plusMinutes(15);
				validateRecoveryCode(request.getOtpCode(), codeInDB, codeExpireTime, locale);
				String authToken = authenticationMgmt.generateAuthToken(request.getMobileNumber());
				CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(request.getMobileNumber());
				CustomerAuthenticationDTO customerAuthDTO = customerAuthenticationDAO
						.getByMobileNumber(request.getMobileNumber());
				if (customerAuthDTO == null) {
					CustomerAuthenticationDTO customerAuth = new CustomerAuthenticationDTO();
					if (customerDTO != null) {
						if (!customerDTO.isActive()) {
							throw new ValidationException(ExceptionResourceBundle
									.getExceptionCodeProperties(ExceptionCode.CUSTOMER_ID_IS_DEACTIVATED, locale));
						}
						customerAuth.setCustomerId(customerDTO.getId());
					}
					customerAuth.setMobileNumber(request.getMobileNumber());
					customerAuth.setAuthToken(authToken);
					customerAuthenticationDAO.addToken(customerAuth);
				} else {
					authenticationMgmt.removeAuthToken(customerAuthDTO.getAuthToken());
					if (customerDTO != null) {
						customerAuthDTO.setCustomerId(customerDTO.getId());
					}
					customerAuthDTO.setMobileNumber(request.getMobileNumber());
					customerAuthDTO.setAuthToken(authToken);
					customerAuthenticationDAO.updateToken(customerAuthDTO);
				}
				response.setAuthToken(authToken);
				otpVerificationDAO.deleteInfo(otpdto);
				CustomerDTO customerdto = customerDAO.getCustomerByMobileNumber(request.getMobileNumber());
				if (customerdto != null) {
					response.setCustomerId(customerdto.getId());
					response.setIsRegistered(true);
					CustomerDeviceDetailsDTO deviceDTO = customerDeviceDetailsDAO
							.getDeviceDetailsByCustomerId(customerdto.getId());
					if (deviceDTO != null) {
						if (request.getAppToken() != null) {
							if (deviceDTO.getAppToken() != null
									&& !(request.getAppToken().equals(deviceDTO.getAppToken()))) {
								JSONObject payloadJSON = new JSONObject();
								payloadJSON.put("notificationType", prop.getProperty(ResourceProperties.LOGOUT));
								JSONObject payload = commonApplication.getPayloadDataOnly(deviceDTO.getDeviceToken(),
										payloadJSON);
								commonApplication.sendPushNotificationDataToCustomer(payload, fcmKeyCustomer,
										customerdto.getId(), UserCategory.CUSTOMER.getName());
							}
							deviceDTO.setAppToken(request.getAppToken());
							deviceDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
							customerDeviceDetailsDAO.updateDeviceDetails(deviceDTO);
						}
					} else {
						CustomerDeviceDetailsDTO detailsDTO = new CustomerDeviceDetailsDTO();
						detailsDTO.setCustomerId(customerdto.getId());
						if (request.getAppToken() != null) {
							detailsDTO.setAppToken(request.getAppToken());
						}
						detailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						customerDeviceDetailsDAO.addDeviceDetails(detailsDTO);
					}
				} else {
					response.setIsRegistered(false);
				}

			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, ApiUtils.toJsonString(response));
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public GetGuestUserInfoResponse getGuestUserInfo(Locale locale) throws Exception {
		GetGuestUserInfoResponse response = new GetGuestUserInfoResponse();
		String authToken = authenticationMgmt.generateAuthToken("2233445566");
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		response.setAuthToken(authToken);
		response.setCustomerId(customerDTO.getId());
		response.setMobileNumber(mobile);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public
	 * CustomerRegistrationResponse registration(CustomerRegistration request,
	 * String authToken, Locale locale) throws Exception {
	 * logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
	 * String mobile =
	 * authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
	 * CustomerRegistrationResponse response = new
	 * CustomerRegistrationResponse();
	 * 
	 * CustomerDTO dto = customerDAO.getCustomerByMobileNumber(mobile); if (dto
	 * != null) { throw new ValidationException(
	 * ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.
	 * ALREADY_REGISTERED, locale)); } else {
	 * checkRegistrationConstraint(request, locale); Long customerId =
	 * generateCustomerId(request, mobile, locale); CustomerDeviceDetailsDTO
	 * detailsDTO = new CustomerDeviceDetailsDTO();
	 * detailsDTO.setCustomerId(customerId); if(request.getAppToken() != null){
	 * detailsDTO.setAppToken(request.getAppToken()); }
	 * detailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	 * customerDeviceDetailsDAO.addDeviceDetails(detailsDTO);
	 * response.setCustomerId(String.valueOf(customerId)); } response =
	 * ApiUtils.setResponseWithOperationId(response,
	 * OperationId.CUSTOMER_SERVICES); logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response)); return response; }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CustomerRegistrationResponse registration(CustomerRegistration request, String authToken, Locale locale)
			throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobileForNewUser(ApiUtils.createAuth(authToken));
		CustomerRegistrationResponse response = new CustomerRegistrationResponse();

		CustomerDTO dto = customerDAO.getCustomerByMobileNumber(mobile);
		if (dto != null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ALREADY_REGISTERED, locale));
		} else {
			Long customerId = generateCustomerId(request, mobile, locale);

			response.setCustomerId(String.valueOf(customerId));
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	private Long generateCustomerId(CustomerRegistration request, String mobile, Locale locale)
			throws ValidationException {
		AddressDTO addressDTO = null;
		addressDTO = addressDAO.getByFlatId(request.getFlatId());
		if (addressDTO == null) {
			addressDTO = addAddressInfo(request);
		}

		CustomerDTO customerDTO = new CustomerDTO();
		customerDTO.setMobileNumber(mobile);
		customerDTO.setFullName(StringUtils.capitalize(request.getFullName()));
		customerDTO.setAddressId(addressDTO.getId());
		if (!Strings.isNullOrEmpty(request.getEmail())) {
			//checkIfEmailAlreadyRegistered(request.getEmail(), locale);
			customerDTO.setEmail(request.getEmail());
		}
		customerDTO.setEmailVerified(false);
		customerDTO.setGender(request.getGender());
		customerDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		customerDTO.setActive(true);
		customerDAO2.addCustomerInfo(customerDTO);
		Long customerId = customerDTO.getId();
		return customerId;

	}

	private AddressDTO addAddressInfo(CustomerRegistration request) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setFlatId(request.getFlatId());
		addressDTO.setCity(request.getCity());
		addressDTO.setCityCode(getCityCode(cityJSONArray,request.getCity()));
		addressDTO.setProject(request.getProject());
		addressDTO.setTower(request.getTower());
		addressDTO.setFlat(request.getFlat());
		addressDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		addressDAO.addAddressInfo(addressDTO);
		return addressDTO;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CustomerLogoutResponse logout(String authToken, Locale locale) throws Exception {

		CustomerLogoutResponse response = new CustomerLogoutResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD, customerDTO.getId());
		CustomerDeviceDetailsDTO deviceDTO = customerDeviceDetailsDAO.getDeviceDetailsByCustomerId(customerDTO.getId());
		if (deviceDTO != null) {
			deviceDTO.setAppToken(null);
			deviceDTO.setDeviceToken(null);
			deviceDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			customerDeviceDetailsDAO.updateDeviceDetails(deviceDTO);
		}

		// customerDAO.updateTravellerInfo(travellerDTO);
		authenticationMgmt.removeAuthToken(authToken);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductListResponse getProductList(String authToken, GetProductRequest request, Locale locale)
			throws Exception {
		
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		ProductListResponse response = new ProductListResponse();

		if (addressDTO != null) {
			String cityCode = addressDTO.getCityCode();
			//here brandId is is subcategoryId
			List<ProductDetails> productList = new ArrayList<>();
			productList = getSubCategoryProductListV1(request.getProductBrandId(), cityCode);
			response.getProducts().addAll(productList);

		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
 	}


	private List<ProductDetails> getSubCategoryProductListV1(int subCategoryId, String cityCode) {
		List<ProductDetails> productList = new ArrayList<>();
		List<ProductCityInfoV2DTO> productCityInfoDTOList = productCityInfoV2DAO.getByCityCodeAndSubCategoryId(cityCode,
				subCategoryId, true);
		List<Integer> productIdList = productCityInfoDTOList.stream().map(ProductCityInfoV2DTO::getProductId).distinct()
				.collect(Collectors.toList());
		if(productIdList.size() >0){
			List<Integer> activeProductIds = productInfoV2DAO.getByIds(productIdList);
		List<ProductInfoV2DTO> productInfoList = new ArrayList<>();
		if (activeProductIds != null && !activeProductIds.isEmpty() && activeProductIds.size() != 0) {
			productInfoList = productInfoV2DAO.getByProductIds(activeProductIds, 0, -1);
		}
		List<Integer> brandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
				.collect(Collectors.toList());
		List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
		if (brandIdList != null && !brandIdList.isEmpty() && brandIdList.size() != 0) {
			brandInfoList = brandInfoV2DAO.getByBrandIds(brandIdList);
		}
		for (ProductInfoV2DTO prV2dto : productInfoList) {
			List<ProductCityInfoV2DTO> productDetailsList = productCityInfoDTOList.stream()
					.filter(e -> e.getProductId().equals(prV2dto.getId())).collect(Collectors.toList());
			for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
				ProductDetails pInfoV2 = new ProductDetails();
				pInfoV2.setProductId(pCityInfoV2DTO.getId());
				if(pCityInfoV2DTO.isDiscountAvailable()){
				if(pCityInfoV2DTO.getSubCategoryId().equals(60) || pCityInfoV2DTO.getSubCategoryId().equals(61))	{
				pInfoV2.setProductName(new DecimalFormat("0.#").format(pCityInfoV2DTO.getDiscount())+pCityInfoV2DTO.getDiscountUnit() + " off on " + 
						 productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
						.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
				}
				else{
					pInfoV2.setProductName(new DecimalFormat("0.#").format(pCityInfoV2DTO.getDiscount())+pCityInfoV2DTO.getDiscountUnit() + " off on " + brandInfoList.stream().filter(e -> e.getId().equals(prV2dto.getBrandId()))
							.map(BrandInfoV2DTO::getBrand).findFirst().orElse("")+" "+productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
							.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
					
				}
				}
				else{
					if(pCityInfoV2DTO.getSubCategoryId().equals(60) || pCityInfoV2DTO.getSubCategoryId().equals(61))	{
						pInfoV2.setProductName(productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
								.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
					}
					else{
					pInfoV2.setProductName(brandInfoList.stream().filter(e -> e.getId().equals(prV2dto.getBrandId()))
							.map(BrandInfoV2DTO::getBrand).findFirst().orElse("")+" "+productInfoList.stream().filter(e -> e.getId().equals(prV2dto.getId()))
							.map(ProductInfoV2DTO::getProductName).distinct().findFirst().orElse(""));
					}
					
				}
				pInfoV2.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
				pInfoV2.setPrice(pCityInfoV2DTO.getSalePrice()+pCityInfoV2DTO.getDeliveryCharge());
				pInfoV2.setStockStatus(pCityInfoV2DTO.isStockStatus());
				if(!prV2dto.isStockStatus() || !pCityInfoV2DTO.isStockStatus()){
					continue;
				}
				/*PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(ImageCategory.PRODUCT.getId(), pCityInfoV2DTO.getId());
				if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
					pInfoV2.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
				}	*/	
				if(pCityInfoV2DTO.getPhotoId() != null){
				PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
				if(photoDTO != null && photoDTO.getUpdatedOn() != null){
				pInfoV2.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
				}
				}
				productList.add(pInfoV2);
				}
			
		}
		
	}
		return productList;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RegenerateAuthTokenResponse regenAuthToken(RegenerateAuthToken request, Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		Long customerId = Long.parseLong(request.getCustomerId());
		RegenerateAuthTokenResponse response = new RegenerateAuthTokenResponse();
		CustomerDTO tdto = customerDAO.getByIdAndMobileNumber(customerId, request.getMobileNumber());
		if (tdto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		} else {
			String authToken = authenticationMgmt.generateAuthToken(request.getMobileNumber());
			//logger.info("New AuthToken generated for customerId " + customerId + ": ", authToken);
			CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(request.getMobileNumber());
			CustomerAuthenticationDTO customerAuthDTO = customerAuthenticationDAO
					.getByMobileNumber(request.getMobileNumber());
			if (customerAuthDTO == null) {
				addCustomerAuthenticationInfo(customerDTO, request.getMobileNumber(), authToken);
			} else {
				/*logger.info("AuthToken going to remove for customerId " + customerId + " : ",
						customerAuthDTO.getAuthToken());*/
				authenticationMgmt.removeAuthToken(customerAuthDTO.getAuthToken());
				updateCustomerAuthenticationInfo(customerDTO, customerAuthDTO, request.getMobileNumber(), authToken);

			}
			response.setAuthToken(authToken);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, tdto.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public DeviceTokenResponse saveDeviceToken(DeviceToken request, String authToken, Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		DeviceTokenResponse response = new DeviceTokenResponse();
		if (customerDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}
		if (customerDTO.getId() > 0) {
			// CustomerDTO customerDTO =
			// customerDAO.getByMobileNumberWithoutActiveState(mobile);
			logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

			response.setCustomerId(customerDTO.getId());
			CustomerDeviceDetailsDTO devicedto = customerDeviceDetailsDAO
					.getDeviceDetailsByCustomerId(customerDTO.getId());
			if (devicedto == null) {
				Long id = addCustomerDeviceDetails(request, customerDTO.getId());
				response.setId(id);
			} else {
				updateCustomerDeviceDetails(request, devicedto, customerDTO.getId());
				response.setId(devicedto.getId());
			}
			response.setCustomerId(customerDTO.getId());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public PersonalInfoResponse getPersonalInfo(String authToken, Locale locale) throws Exception {
		PersonalInfoResponse response = new PersonalInfoResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
			CustomerPersonalInfo personalInfo = setPersonalInfo(customerDTO);
			response.setCustomerPersonalInfo(personalInfo);
			response.setEmailVerified(customerDTO.isEmailVerified());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse setPersonalInfo(CustomerPersonalInfo request, String authToken, Locale locale)
			throws Exception {
		//logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		if (customerDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
			setPersonalInfo(customerDTO, request, locale);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public DownloadPhotoResponse getProductImage(/* String authToken, */ String imageCategory, Long imageCategoryId,
			Locale locale) {
		// String mobile =
		// authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		// CustomerDTO customerDTO =
		// customerDAO.getCustomerByMobileNumber(mobile);
		if (!Strings.isNullOrEmpty(imageCategory) && imageCategory.equalsIgnoreCase(ImageCategory.PRODUCT.getName())) {
			imageCategory = ImageCategory.PRODUCT.getName().toLowerCase();
		} else if (!Strings.isNullOrEmpty(imageCategory)
				&& imageCategory.equalsIgnoreCase(ImageCategory.CATEGORY.getName())) {
			imageCategory = ImageCategory.CATEGORY.getName().toLowerCase();
		} else if (!Strings.isNullOrEmpty(imageCategory)
				&& imageCategory.equalsIgnoreCase(ImageCategory.BRAND.getName())) {
			imageCategory = ImageCategory.BRAND.getName().toLowerCase();
		}
		ImageDTO dto = productImageDAO.getByProductId(imageCategory, imageCategoryId);
		DownloadPhotoResponse response = new DownloadPhotoResponse();
		if (dto == null || dto.getData() == null) {
			return null;
		}
		String byteArrayStr = new String(Base64.encode(dto.getData()));
		response.setImageId(dto.getId());
		response.setImageCategoryId(dto.getImageCategoryId());
		response.setData(byteArrayStr);
		response.setFileName(dto.getFileName());
		response.setContentType(dto.getContentType());
		// response.setUpdatedOn(dto.getUpdatedOn().getTime());
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		// logger.info(ApiUtils.RESPONSE_PAYLOAD,
		// ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse savePhoto(String authToken, File file, String imageCategory, Long imageCategoryId,
			StoreFile storeFile, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		try {

			BufferedImage resizeImageJpg;
			InputStream is;
			BufferedImage originalImage = ImageIO.read(file);
			// int type = originalImage.getType() == 0 ?
			// BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			resizeImageJpg = commonApplication.resizeImageWithType(originalImage, imageCategory);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			ImageIO.write(resizeImageJpg, storeFile.getFileExtension(), os);
			is = new ByteArrayInputStream(os.toByteArray());
			ByteSource byteSource = new ByteSource() {
				@Override
				public InputStream openStream() throws IOException {
					return is;
				}
			};
			byte[] bytes = byteSource.read();
			if (!Strings.isNullOrEmpty(imageCategory)
					&& imageCategory.equalsIgnoreCase(ImageCategory.PRODUCT.getName())) {
				imageCategory = ImageCategory.PRODUCT.getName().toLowerCase();
			} else if (!Strings.isNullOrEmpty(imageCategory)
					&& imageCategory.equalsIgnoreCase(ImageCategory.CATEGORY.getName())) {
				imageCategory = ImageCategory.CATEGORY.getName().toLowerCase();
			} else if (!Strings.isNullOrEmpty(imageCategory)
					&& imageCategory.equalsIgnoreCase(ImageCategory.BRAND.getName())) {
				imageCategory = ImageCategory.BRAND.getName().toLowerCase();
			}
			ImageDTO dto = productImageDAO.getByProductId(imageCategory, imageCategoryId);
			if (dto == null) {
				ImageDTO uploadFile = new ImageDTO();
				uploadFile.setImageCategory(imageCategory);
				uploadFile.setImageCategoryId(imageCategoryId);
				uploadFile.setFileName(storeFile.getFileName());
				uploadFile.setData(bytes);
				uploadFile.setContentType(storeFile.getContentType());
				uploadFile.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				uploadFile.setUpdatedBy(customerDTO.getId());
				productImageDAO.addFile(uploadFile);
			} else {
				dto.setImageCategory(imageCategory);
				dto.setImageCategoryId(imageCategoryId);
				dto.setFileName(storeFile.getFileName());
				dto.setData(bytes);
				dto.setContentType(storeFile.getContentType());
				dto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				dto.setUpdatedBy(customerDTO.getId());
				productImageDAO.updateFile(dto);
			}
			os.flush();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void setPersonalInfo(CustomerDTO customerDTO, CustomerPersonalInfo request, Locale locale)
			throws ValidationException {

		//checkEmailValidation(customerDTO.getEmail(), request.getEmail(), locale);
		checkMobileValidation(customerDTO.getMobileNumber(), request.getMobileNumber(), locale);

		if (!Strings.isNullOrEmpty(request.getFullName())) {
			customerDTO.setFullName(StringUtils.capitalize(request.getFullName()));
		}
		if (!Strings.isNullOrEmpty(request.getEmail())) {
			customerDTO.setEmail(request.getEmail());
			customerDTO.setEmailVerified(false);
		}
		if (!Strings.isNullOrEmpty(request.getMobileNumber())) {
			customerDTO.setMobileNumber(request.getMobileNumber());
		}
		if (!Strings.isNullOrEmpty(request.getGender())) {
			customerDTO.setGender(request.getGender());
		}
		AddressDTO addressDTO = addressDAO.getByFlatId(request.getFlatId());
		if (addressDTO == null) {
			addressDTO = addAddressDetails(request);
		}
		customerDTO.setAddressId(addressDTO.getId());
		if (!Strings.isNullOrEmpty(request.getCity())) {
			addressDTO.setCity(request.getCity());
		}
		if (!Strings.isNullOrEmpty(request.getProject())) {
			addressDTO.setProject(request.getProject());
		}
		if (!Strings.isNullOrEmpty(request.getTower())) {
			addressDTO.setTower(request.getTower());
		}
		if (!Strings.isNullOrEmpty(request.getFlat())) {
			addressDTO.setFlat(request.getFlat());
		}
		if (request.getFlatId() != 0) {
			addressDTO.setFlatId(request.getFlatId());
		}
		if (!Strings.isNullOrEmpty(request.getCityCode())) {
			addressDTO.setCityCode(request.getCityCode());
		}
		else{
			addressDTO.setCityCode(getCityCode(cityJSONArray,request.getCity()));
		}
		addressDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}

	private AddressDTO addAddressDetails(CustomerPersonalInfo request) {
		AddressDTO addressDTO = new AddressDTO();
		addressDTO.setFlatId(request.getFlatId());
		addressDTO.setCity(request.getCity());
		addressDTO.setCityCode(getCityCode(cityJSONArray,request.getCity()));
		addressDTO.setProject(request.getProject());
		addressDTO.setTower(request.getTower());
		addressDTO.setFlat(request.getFlat());
		addressDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		addressDAO.addAddressInfo(addressDTO);
		return addressDTO;
	}

	private void checkMobileValidation(String existingMobile, String updatedMobile, Locale locale)
			throws ValidationException {
		if (!Strings.isNullOrEmpty(existingMobile)) {
			if (!Strings.isNullOrEmpty(updatedMobile)) {
				if (existingMobile.equals(updatedMobile)) {
					/*
					 * throw new ValidationException(
					 * ExceptionResourceBundle.getExceptionCodeProperties(
					 * ExceptionCode.NO_UPDATE_IN_MOBILE, locale));
					 */
				} else {
					CustomerDTO dto = customerDAO.getCustomerByMobileNumber(updatedMobile);
					if (dto != null) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.MOBILE_ALREADY_IN_USE, locale));
					}
				}
			}

		} else {
			CustomerDTO dto = customerDAO.getCustomerByMobileNumber(updatedMobile);
			if (dto != null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.MOBILE_ALREADY_IN_USE, locale));
			}
		}

	}

	private void checkEmailValidation(String existingEmail, String updatedEmail, Locale locale)
			throws ValidationException {
		if (!Strings.isNullOrEmpty(existingEmail)) {
			if (!Strings.isNullOrEmpty(updatedEmail)) {
				if (existingEmail.equals(updatedEmail)) {
					/*
					 * throw new ValidationException(
					 * ExceptionResourceBundle.getExceptionCodeProperties(
					 * ExceptionCode.NO_UPDATE_IN_EMAIL, locale));
					 */} else {
					CustomerDTO dto = customerDAO.getCustomerByEmail(updatedEmail);
					if (dto != null) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
					}

				}
			}

		} else {
			CustomerDTO dto = customerDAO.getCustomerByEmail(updatedEmail);
			if (dto != null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
			}
		}

	}

	private CustomerPersonalInfo setPersonalInfo(CustomerDTO customerDTO) {
		CustomerPersonalInfo personalInfo = new CustomerPersonalInfo();
		personalInfo.setCustomerId(String.valueOf(customerDTO.getId()));
		personalInfo.setFullName(customerDTO.getFullName());
		personalInfo.setGender(customerDTO.getGender());
		personalInfo.setEmail(customerDTO.getEmail());
		personalInfo.setMobileNumber(customerDTO.getMobileNumber());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		personalInfo.setProject(addressDTO.getProject());
		personalInfo.setTower(addressDTO.getTower());
		personalInfo.setFlat(addressDTO.getFlat());
		personalInfo.setCity(addressDTO.getCity());
		personalInfo.setCityCode(addressDTO.getCityCode());
		personalInfo.setFlatId(addressDTO.getFlatId());
		/*
		 * personalInfo.setProject(customerDTO.getProject());
		 * personalInfo.setTower(customerDTO.getTower());
		 * personalInfo.setFlat(customerDTO.getFlat());
		 */

		return personalInfo;
	}

	private void updateCustomerDeviceDetails(DeviceToken request, CustomerDeviceDetailsDTO devicedto, Long customerId) {
		devicedto.setCustomerId(customerId);
		devicedto.setDeviceModel(request.getDeviceModel());
		devicedto.setDevicePlatform(request.getDevicePlatform());
		devicedto.setDeviceToken(request.getDeviceToken());
		devicedto.setDevicePlatformVersion(request.getDevicePlatformVersion());
		devicedto.setAppVersion(request.getAppVersion());
		devicedto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		devicedto.setNetworkCarrier(request.getNetworkCarrier());
		devicedto.setNetworkType(request.getNetworkType());
		customerDeviceDetailsDAO.updateDeviceDetails(devicedto);

	}

	private Long addCustomerDeviceDetails(DeviceToken request, Long customerId) {
		CustomerDeviceDetailsDTO dtoken = new CustomerDeviceDetailsDTO();
		dtoken.setCustomerId(customerId);
		dtoken.setDeviceModel(request.getDeviceModel());
		dtoken.setDevicePlatform(request.getDevicePlatform());
		dtoken.setDeviceToken(request.getDeviceToken());
		dtoken.setDevicePlatformVersion(request.getDevicePlatformVersion());
		dtoken.setAppVersion(request.getAppVersion());
		dtoken.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return customerDeviceDetailsDAO.addDeviceDetails(dtoken);
	}

	private void updateCustomerAuthenticationInfo(CustomerDTO customerDTO, CustomerAuthenticationDTO customerAuthDTO,
			String mobileNumber, String authToken) {
		if (customerDTO != null) {
			customerAuthDTO.setCustomerId(customerDTO.getId());
		}
		customerAuthDTO.setMobileNumber(mobileNumber);
		customerAuthDTO.setAuthToken(authToken);
		customerAuthenticationDAO.updateToken(customerAuthDTO);

	}

	private void addCustomerAuthenticationInfo(CustomerDTO customerDTO, String mobileNumber, String authToken) {
		CustomerAuthenticationDTO customerAuth = new CustomerAuthenticationDTO();
		if (customerDTO != null) {
			customerAuth.setCustomerId(customerDTO.getId());
		}
		customerAuth.setMobileNumber(mobileNumber);
		customerAuth.setAuthToken(authToken);
		customerAuthenticationDAO.addToken(customerAuth);

	}

	/*
	 * private Long generateCustomerId(CustomerRegistration request, String
	 * mobile, Locale locale) throws ValidationException {
	 * 
	 * CustomerDTO customerDTO = new CustomerDTO();
	 * customerDTO.setMobileNumber(mobile);
	 * customerDTO.setFullName(request.getFullName());
	 * customerDTO.setCity(request.getCity());
	 * customerDTO.setProject(request.getProject());
	 * customerDTO.setTower(request.getTower());
	 * customerDTO.setFlat(request.getFlat());
	 * if(!Strings.isNullOrEmpty(request.getEmail())){
	 * checkIfEmailAlreadyRegistered(request.getEmail(), locale);
	 * customerDTO.setEmail(request.getEmail()); }
	 * customerDTO.setEmailVerified(false);
	 * customerDTO.setGender(request.getGender()); customerDTO.setCreatedOn(new
	 * Timestamp(System.currentTimeMillis())); customerDTO.setActive(true);
	 * customerDAO.addCustomerInfo(customerDTO); Long customerId =
	 * customerDTO.getId(); return customerId;
	 * 
	 * }
	 */

	private void checkIfEmailAlreadyRegistered(String email, Locale locale) throws ValidationException {
		CustomerDTO tdto = customerDAO.getCustomerByEmail(email);
		if (tdto != null)
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_ALREADY_IN_USE, locale));
	}

	private boolean validateRecoveryCodeTime(DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (currentTime.isAfter(userCodeExpireTime)) {
			return false;
		}
		return true;
	}

	private void checkConstraint(VerifyMobileOtp request, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		// entityCommon.checkOTP(request.getOtpCode(), locale);
		// entityCommon.checkMobileNumber(request.getMobileNumber(), locale);
	}

	private void validateRecoveryCode(String recoveryCode, String codeInDB, DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (codeInDB == null || !codeInDB.equals(recoveryCode) || currentTime.isAfter(userCodeExpireTime)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_CODE, locale));
		}
	}

	private void checkRegistrationConstraint(CustomerRegistration request, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		// entityCommon.checkFirstName(request.getFirstName(), locale);

	}

	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListResponse getCategory(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
		CategoryListResponse response = new CategoryListResponse();
		List<ProductCategoryDTO> categoryList = productCategoryDAO.getAllProductCategories(true);
		for (ProductCategoryDTO pCategoryDTO : categoryList) {
			if (pCategoryDTO.getId() == 9) {
				continue;
			}
			ProductCategory pcCategory = new ProductCategory();
			pcCategory.setCategoryId(pCategoryDTO.getId());
			pcCategory.setProductCategory(pCategoryDTO.getCategory());
			pcCategory.setDescription(pCategoryDTO.getDescription());
			response.getCategory().add(pcCategory);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}*/
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListResponse getCategory(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
		CategoryListResponse response = new CategoryListResponse();
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(addressDTO.getCityCode());
		if (cityCategoryDTO != null) {
			String categoryIds = cityCategoryDTO.getCategoryIds();
			String[] categoryIdStrArr = categoryIds.split(",");
			int[] categoryIdIntArr = Stream.of(categoryIdStrArr).mapToInt(Integer::parseInt).toArray();
			List<Integer> categoryIdList = Arrays.stream(categoryIdIntArr).boxed().collect(Collectors.toList());
			List<CategoryInfoV2DTO> categoryList = categoryInfoV2DAO.getByIds(categoryIdList);
			for (CategoryInfoV2DTO pCategoryDTO : categoryList) {
				if (pCategoryDTO.getId() == 9) {
					continue;
				}
				ProductCategory pcCategory = new ProductCategory();
				pcCategory.setCategoryId(pCategoryDTO.getId());
				pcCategory.setProductCategory(pCategoryDTO.getCategory());
				pcCategory.setDescription(pCategoryDTO.getDescription());
				
				PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(ImageCategory.CATEGORY.getId(),
						pCategoryDTO.getId());
				if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
					pcCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
				} else {
					ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.CATEGORY.getName(),
							pCategoryDTO.getId().longValue());
					if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
						pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
					}
				}
				response.getCategory().add(pcCategory);
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListResponse getV2CategoryNotInUse(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId : " + customerDTO.getId(), " , authToken : " + authToken);

		CategoryListResponse response = new CategoryListResponse();
		List<ProductCategoryDTO> categoryList = productCategoryDAO.getAllProductCategories(true);
		for (ProductCategoryDTO pCategoryDTO : categoryList) {
			ProductCategory pcCategory = new ProductCategory();
			pcCategory.setCategoryId(pCategoryDTO.getId());
			pcCategory.setProductCategory(pCategoryDTO.getCategory());
			pcCategory.setDescription(pCategoryDTO.getDescription());

			PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(ImageCategory.CATEGORY.getId(),
					pCategoryDTO.getId());
			if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
				pcCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
			} else {
				ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.CATEGORY.getName(),
						pCategoryDTO.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
			}
			response.getCategory().add(pcCategory);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListResponse getV2Category(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
		CategoryListResponse response = new CategoryListResponse();
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(addressDTO.getCityCode());
		if (cityCategoryDTO != null) {
			String categoryIds = cityCategoryDTO.getCategoryIds();
			String[] categoryIdStrArr = categoryIds.split(",");
			int[] categoryIdIntArr = Stream.of(categoryIdStrArr).mapToInt(Integer::parseInt).toArray();
			List<Integer> categoryIdList = Arrays.stream(categoryIdIntArr).boxed().collect(Collectors.toList());
			List<CategoryInfoV2DTO> categoryList = categoryInfoV2DAO.getByIds(categoryIdList);
			for (CategoryInfoV2DTO pCategoryDTO : categoryList) {
				ProductCategory pcCategory = new ProductCategory();
				pcCategory.setCategoryId(pCategoryDTO.getId());
				pcCategory.setProductCategory(pCategoryDTO.getCategory());
				pcCategory.setDescription(pCategoryDTO.getDescription());
				
				PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(ImageCategory.CATEGORY.getId(),
						pCategoryDTO.getId(), "v1");
						/*ImageCategory.CATEGORY.getId(),
						pCategoryDTO.getId(),);*/
				if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
					pcCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
				} else {
					ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.CATEGORY.getName(),
							pCategoryDTO.getId().longValue());
					if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
						pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
					}
				}
				response.getCategory().add(pcCategory);
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	
	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public BrandListResponse getBrands(String authToken, GetBrandRequest request, Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		// check app version access
		authenticationMgmt.checkAppVersionAccess(authToken);

		BrandListResponse response = new BrandListResponse();
		List<ProductBrandsDTO> brandList = productBrandsDAO.getAllBrandsByCategoryID(request.getProductCategoryId(),
				true);
		for (ProductBrandsDTO pBrandsDTO : brandList) {
			ProductBrands pBrands = new ProductBrands();
			pBrands.setBrandId(pBrandsDTO.getId());
			pBrands.setProductBrands(pBrandsDTO.getBrand());

			PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(ImageCategory.BRAND.getId(), pBrandsDTO.getId());
			if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
				pBrands.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
			} else {
				ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.BRAND.getName(),
						pBrandsDTO.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					pBrands.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
			}

			response.getBrands().add(pBrands);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
*/	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public BrandListResponse getBrands(String authToken, GetBrandRequest request, Locale locale) throws Exception {
		BrandListResponse response = new BrandListResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		CityCategoryInfoDTO cityCategoryDTO = cityCategoryInfoDAO.getByCityCode(addressDTO.getCityCode());
		if (cityCategoryDTO != null) {
			String subCategoryIds = cityCategoryDTO.getSubCategoryIds();
			// 1-1,2;2-1,3;
			String[] catSubCatStrSeq = subCategoryIds.split(";");
			for (String catSubCatStr : catSubCatStrSeq) {
				String[] catSubCatStrArr = catSubCatStr.split("-");
				System.out.println(catSubCatStrArr[0]);
				Integer categoryId = Integer.parseInt(catSubCatStrArr[0]);
				if (categoryId.equals(request.getProductCategoryId())) {
					String[] subCategoryIdArr = catSubCatStrArr[1].toString().split(",");
					int[] subCategoryIdIntArr = Stream.of(subCategoryIdArr).mapToInt(Integer::parseInt).toArray();
					List<Integer> subCategoryIdList = Arrays.stream(subCategoryIdIntArr).boxed()
							.collect(Collectors.toList());
					List<SubCategoryInfoV2DTO> pdto = subCategoryInfoV2DAO.getAllSubCategories(subCategoryIdList);
					if (pdto == null) {

					} else {
						for (SubCategoryInfoV2DTO dto : pdto) {
							ProductBrands subCategory = new ProductBrands();
							subCategory.setBrandId(dto.getId());
							if (dto.getSubCategory().equals("All " + categoryId)) {
								continue;
							} else {
								subCategory.setProductBrands(dto.getSubCategory());
							}
							/*ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.BRAND.getName(),
									pBrandsDTO.getId().longValue());
							if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
								pBrands.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
							}*/
							if(dto != null && dto.getPhotoId() != null){
							PhotoDTO photoDTO = photoDAO.getById(dto.getPhotoId());
							if(photoDTO != null && photoDTO.getUpdatedOn() != null){
								subCategory.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
							}
							}
							
							response.getBrands().add(subCategory);
						}
					}
			}		}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetWalletResponse getWalletInfo(String authToken, Locale locale) throws Exception {
		GetWalletResponse response = new GetWalletResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		float balance = 0;
		if (customerDTO != null) {

			WalletDTO dto = walletDAO.getByCustomerId(customerDTO.getId());

			if (dto != null) {
				balance = dto.getMyWalletBalance();
			}
		}
		response.setWalletBalance(balance);

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetWalletResponse upsertWalletInfo(String authToken, WalletInfo request, Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		GetWalletResponse response = new GetWalletResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerDTO.getId());
		if (walletDTO == null) {
			addWalletInfo(customerDTO.getId(), request);
		} else {
			updateWalletInfo(walletDTO, request, locale);
		}
		WalletDTO waDto = walletDAO.getByCustomerId(customerDTO.getId());
		float balance = 0;
		if (waDto != null) {
			balance = waDto.getMyWalletBalance();
		}
		response.setWalletBalance(balance);
		// response.setWalletBalance(waDto.getId());
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void updateWalletInfo(WalletDTO walletDTO, WalletInfo request, Locale locale) throws ValidationException {
		if (request.getTransactionDesciption().equals(WalletDescription.RECHARGE.getName())) {
			walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + request.getAmount());
		} else if (request.getTransactionDesciption().equals(WalletDescription.DEDUCTIONS.getName()) ||
				request.getTransactionDesciption().equals(WalletDescription.DISCHARGE.getName())) {
			if (walletDTO.getMyWalletBalance() >= request.getAmount()) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - request.getAmount());
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.LOW_WALLET_BALANCE, locale));
			}
		}

	}

	private void addWalletInfo(Long customerId, WalletInfo request) {
		WalletDTO walletDTO = new WalletDTO();
		walletDTO.setCustomerId(customerId);
		walletDTO.setMyWalletBalance(request.getAmount());
		walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		walletDAO.addBalance(walletDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		List<ProductDTO> pdto = productDAO.getAllActiveMatchingProducts(request.getProductSubString());
		if (pdto == null) {
		} else {
			for (ProductDTO dto : pdto) {
				SearchedProductDetails searchedProductDetails = new SearchedProductDetails();
				searchedProductDetails.setProductId(dto.getId());
				searchedProductDetails.setProductName(dto.getProductName());
				searchedProductDetails.setQuantityUnit(dto.getQuantityUnit());
				searchedProductDetails.setPrice(dto.getPrice());
				searchedProductDetails.setActiveStatus(dto.isActive());

				// TODO: make these dynamic
				searchedProductDetails.setStockStatus(true);
				searchedProductDetails.setDemandStatus(true);

				if (dto.getBrandId() == 0 || dto.getBrandId() == null) {
					continue;
				}

				ProductBrandsDTO brandsDTO = productBrandsDAO.getById(dto.getBrandId());
				searchedProductDetails.setBrandName(brandsDTO.getBrand());
				searchedProductDetails.setBrandId(brandsDTO.getId());

				ProductCategoryDTO categoryDTO = productCategoryDAO.getById(brandsDTO.getCategoryId());
				searchedProductDetails.setCategoryName(categoryDTO.getCategory());
				searchedProductDetails.setCategoryId(categoryDTO.getId());

				ImageDTO imageDTO = productImageDAO.getByProductId(ImageCategory.PRODUCT.getName(),
						dto.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					searchedProductDetails.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
				response.getProductList().add(searchedProductDetails);
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
	
	private org.json.simple.JSONArray readCityJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray cityArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			cityArray = (org.json.simple.JSONArray) jsonObject.get("cities");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cityArray;
	}
	
	private String getCityCode(org.json.simple.JSONArray cityArray, String city) {
		String paymentMode = null;
		for (int i = 0; i < cityArray.size(); i++) {
			org.json.simple.JSONObject cityJSON = (org.json.simple.JSONObject) cityArray.get(i);
			if (((String) cityJSON.get("city")).equals(city)) {
				return (String.valueOf((Long) cityJSON.get("id")));

			}

		}
		return paymentMode;

	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String fcmKeyCustomer = prop.getProperty(ResourceProperties.FCM_KEY_CUSTOMER);
	private static final String testUserMobileNumber = prop.getProperty(ResourceProperties.TEST_USER_MOBILE_NUMBER);

}

final class CodeGenerator {

	public static String generateCode() {
		Random random = new Random();
		int n = 1000 + random.nextInt(9000);
		return Integer.toString(n);
	}

	private CodeGenerator() {
		/**/}
}