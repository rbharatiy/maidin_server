package com.maidin.customerApi.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public class OprationIdResourceBundle {

	public static String getOperationId(String propertyName, Locale locale) {

		ResourceBundle operationIdBundle = ResourceBundle.getBundle("customerService-operation-id", locale);
		String operationId = operationIdBundle.getString(propertyName);
		return operationId;	
	}
	
	public static String getOperationId(String propertyName) {

		ResourceBundle operationIdBundle = ResourceBundle.getBundle("customerService-operation-id");
		String operationId = operationIdBundle.getString(propertyName);
		return operationId;	
	}
	
}