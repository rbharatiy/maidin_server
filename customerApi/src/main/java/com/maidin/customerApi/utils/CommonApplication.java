package com.maidin.customerApi.utils;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpStatus;
import org.codehaus.jackson.map.ObjectMapper;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.common.collect.Lists;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.enums.ImageCategory;

/*import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpStatus;
import org.codehaus.jackson.map.ObjectMapper;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.common.collect.Lists;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.enums.ImageCategory;*/


public class CommonApplication {
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(CommonApplication.class);
	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Unable to load the properties file: " + fileName);
		}
	}
	 
	
	
	public void sendPushNotificationDataToCustomer(JSONObject payload, String key, Long Id, String userType)throws IOException, JSONException {
		int responseCode = -1;
		JSONObject responseBody = null;
		try {
			System.out.println("Sending FCM request");

			//String payload = getPayloadRideDetails(tdo.getDeviceToken(),title, pushNotificationMessage, response);
			URL url = new URL(prop.getProperty(ResourceProperties.FCM_URL));
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
//			httpURLConnection.setRequestProperty("Content-Length",
//			Integer.toString(payload.length()));
			httpURLConnection.setRequestProperty("Authorization",
					"key="+key);
			OutputStream out = httpURLConnection.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out, "utf-8");
			
			System.out.println("Payload : "+payload);
			outputStreamWriter.write(payload.toString());
			outputStreamWriter.flush();
			out.close();
			responseCode = httpURLConnection.getResponseCode();
			if (responseCode == HttpStatus.SC_OK) {
				//responseBody = convertStreamToString(httpURLConnection.getInputStream());
				responseBody = convertStreamToJSON(httpURLConnection.getInputStream());
				System.out.println("FCM message sent : " + responseBody);
				//recordPushNotificationTraveller(payload, responseBody, Id, userType);
			} else {
				//responseBody = convertStreamToString(httpURLConnection.getErrorStream());
				responseBody = convertStreamToJSON(httpURLConnection.getErrorStream());
				System.out.println("FCM message Error Message : " + responseBody);
				//recordPushNotificationTraveller(payload, responseBody, Id, userType);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

	public static JSONObject convertStreamToJSON(InputStream inStream) throws Exception {
		
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(inStream, Map.class);
		return new JSONObject(jsonMap);
	}

	public JSONObject getPayloadDataOnly(String token, JSONObject response) {
		JSONObject jsonObject = new JSONObject();
		try {
			JSONObject data = new JSONObject();
			jsonObject.put("data", response);
			jsonObject.put("to", token);
			jsonObject.put("priority" , "high");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}
	
	public BufferedImage resizeImageWithType(BufferedImage originalImage, String imageType) {
		if (!((imageType.equalsIgnoreCase(ImageCategory.CATEGORY.getName()))
				|| (imageType.equalsIgnoreCase(ImageCategory.PAYMENTOPTION.getName())))) {
			int imageWidth = originalImage.getWidth(null);
			int imageHeight = originalImage.getHeight(null);
			int larger = checkLargerOfTwoNumbers(imageWidth, imageHeight);
			Dimension dimension = setImageRatio(larger, imageWidth, imageHeight, imageType);

			originalImage = Scalr.resize(originalImage, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, dimension.width,
					dimension.height);
		}
		return originalImage;
	}

	public Dimension setImageRatio(int larger, int imageWidth, int imageHeight, String imageType) {
		Dimension dimension = new Dimension(imageWidth, imageHeight);
		if(imageType.equals(ImageCategory.CATEGORY.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer.parseInt(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_5001)));
			}
			
		}
		else if(imageType.equals(ImageCategory.BRAND.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_5001)));
			}
			
		}
		else if(imageType.equals(ImageCategory.PRODUCT.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_5001)));
			}
			
		}
		
		else if (imageType.equals(ImageCategory.PAYMENTOPTION.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_5001)));
			}

		}

		else if (imageType.equals(ImageCategory.PRODUCTDESCRIPTION.getName())) {

			if (larger > 0 && larger < 401) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_0_250)));
			} else if (larger > 400 && larger < 801) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_251_500)));
			} else if (larger > 800 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_5001)));
			}

		}
		
		else if (imageType.equals(ImageCategory.PRODUCTDETAIL.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_5001)));
			}

		}
		
		else {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001)));
			}
			
		}
		
		System.out.println(larger + "----" + imageWidth + " --- " + imageHeight);
		return dimension;

	}

	/*public Dimension setImageDimension(int larger, int imageWidth, int imageHeight, float i) {
		imageHeight = (int) (imageHeight / i);
		imageWidth = (int) (imageWidth / i);
		Dimension dimension = new Dimension(imageWidth, imageHeight);
		return dimension;

	}*/
	
	public Dimension setImageDimension(float larger, float imageWidth, float imageHeight, float i) {
		int imageHeight1 = (int) (imageHeight / i);
		int imageWidth1 =  (int) (imageWidth / i);
		Dimension dimension = new Dimension(imageWidth1, imageHeight1);
		return dimension;

	}

	public int checkLargerOfTwoNumbers(int imageWidth, int imageHeight) {
		int larger = imageWidth;
		if (imageWidth > imageHeight) {
			larger = imageWidth;
		} else {
			larger = imageHeight;
		}
		return larger;
	}
	
	public List<Integer> wrapWithQuotes(List<Integer> inputList) {
		List<Integer> inputListWithQuote = Lists.newArrayList();
		for (Integer input : inputList) {
			if (input != null) {
				inputListWithQuote.add(wrapWithQuotes(input));
			}
		}
		return inputListWithQuote;
	}

	private Integer wrapWithQuotes(Integer input) {
		return input;
	}


	public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (getZeroTimeDate(new Date(calendar.getTime().getTime())).before(getZeroTimeDate(enddate))
				|| getZeroTimeDate(new Date(calendar.getTime().getTime())).equals(getZeroTimeDate(enddate))) {
			Date result = new Date(calendar.getTime().getTime());
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}
	
	public Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}
	
	public Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	/*private final int PIXEL_RANGE_0_250 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250));
	private final int PIXEL_RANGE_251_500 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500));
	private final int PIXEL_RANGE_501_1000 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000));
	private final int PIXEL_RANGE_1001_2500 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500));
	private final int PIXEL_RANGE_2501_3500 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500));
	private final int PIXEL_RANGE_3501_5000 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000));
	private final int PIXEL_RANGE_5001 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001));
	*/
	
	
	
	
	}