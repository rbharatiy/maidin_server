package com.maidin.customerApi.utils;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;

import org.codehaus.jackson.map.ObjectMapper;
//import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.maidin.customerAuthenticate.Auth;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OprationIdResourceBundle;
import com.maidin.common.exception.UserException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.EmailSender;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.customer.GenerateMobileOtpResponse;

public class ApiUtils {

	private static final Logger LOG = LoggerFactory.getLogger(ApiUtils.class);
	private static final Properties prop = CommonUtils.loadProperties();
	
	public static final String REQUEST_PAYLOAD = "request payload: {}";
    public static final String RESPONSE_PAYLOAD = "response payload: {}";
    public static final String REQUEST_PAYLOAD_PARAMETER ="customerId: {} request payload: {}";
    public static final String RESPONSE_PAYLOAD_PARAMETER ="customerId: {} response payload: {}";
    
  
	public static final String dateFormat = "yyyy-MM-dd";
	public static final String dateformatter = "dd/MM/yyyy";
	public static final String timeFormat = "hh:mm"; 
	public static final String dateTimeFormat = "yyyy-MM-dd hh:mm";

	public static final long INVALID_ID = -1;
    
    private ApiUtils() {
        
    }
    /* Sets status and operation.*/
     
    public static <T extends GenericResponse> T setResponse(T genericResponse){
        genericResponse.setStatus(true);
        return genericResponse;
    }
    
    public static <T extends GenericResponse> T setResponseWithOperationId(T genericResponse, String operationId){
        genericResponse.setStatus(true);
        genericResponse.setOperationId(OprationIdResourceBundle.getOperationId(operationId));
        genericResponse.setMinSupportedVer(getMinSupportedVersion(OprationIdResourceBundle.getOperationId(operationId)));
        genericResponse.setMinSupportedVerAndroid(getMinSupportedVersion("Android"));
        genericResponse.setMinSupportedVerIOS(getMinSupportedVersion("IOS"));
        return genericResponse;
    }
    
  /*  *//**
     * Sets status, operation, error message.
     *//*
*/    public static <T extends GenericResponse> T setErrorResponse(T genericResponse, String errorMessage){
        genericResponse.setStatus(false);
        genericResponse.setErrorMessage(errorMessage);
        LOG.error("Error {}", errorMessage);
        return genericResponse;
    }
    
   /* *//**
     * Sets status, operation, error message and error code.
     *//*
*/    public static <T extends GenericResponse> T setErrorResponse(T genericResponse, String errorCode, String errorMessage){
        genericResponse.setStatus(false);
        genericResponse.setErrorCode(errorCode);
        genericResponse.setErrorMessage(errorMessage);
        LOG.error("Error {}", errorMessage);
        return genericResponse;
    }
    
    public static <T extends GenericResponse> T setErrorResponseWithOperationId(T genericResponse, 
    		String errorCode, String errorMessage, String operationId){
        genericResponse.setStatus(false);
        genericResponse.setMinSupportedVer(getMinSupportedVersion(operationId));
        genericResponse.setMinSupportedVerAndroid(getMinSupportedVersion("Android"));
        genericResponse.setMinSupportedVerIOS(getMinSupportedVersion("IOS"));
        genericResponse.setErrorCode(errorCode);
        genericResponse.setErrorMessage(errorMessage);
        genericResponse.setOperationId(operationId);
        LOG.error("Error {}", errorMessage);
        return genericResponse;
    }
    
  /*  *//**
     * Sets status, operation, error message and error code.
     *//*
*/    public static <T extends GenericResponse> T setErrorResponse(T genericResponse, Exception ex){
        String errorCode = null;
        if (ex instanceof UserException) {
        	errorCode = ((UserException) ex).getCode();
            setErrorResponse(genericResponse, ((UserException)ex).getCode(), ((UserException)ex).getMessage());
            //TODO: This logger statement should not make it to the production
            LOG.error(errorCode, ex);
        } else {
        	//This will be the segment of exceptions which are not handled by the developer correctly.
        	//Each time we receive an unhandled issue, we should revisit our code and see how
        	//we can correct it
            errorCode = "UNHANDLED-ISSUE";
            //in the case of unhandled issue, generate a unique error code and inform the developer
            
            setErrorResponse(genericResponse, errorCode, ex.getMessage());
            LOG.error(errorCode, ex);
        }
        return genericResponse;
    }
    
    
    public static <T extends GenericResponse> T setErrorResponseWithOperationId(T genericResponse, Exception ex, String operationId){
        String errorCode = null;
        if (ex instanceof UserException) {
        	errorCode = ((UserException) ex).getCode();
        	setErrorResponseWithOperationId(genericResponse, ((UserException)ex).getCode(),
            		((UserException)ex).getMessage(), OprationIdResourceBundle.getOperationId(operationId));
            //TODO: This logger statement should not make it to the production
            LOG.error(errorCode, ex);
        } else {
        	//This will be the segment of exceptions which are not handled by the developer correctly.
        	//Each time we receive an unhandled issue, we should revisit our code and see how
        	//we can correct it
            errorCode = "UNHANDLED-ISSUE";
            //in the case of unhandled issue, generate a unique error code and inform the developer
            
            //setErrorResponse(genericResponse, errorCode, ex.getMessage());
            setErrorResponseWithOperationId(genericResponse, errorCode,
            		ex.getMessage(), OprationIdResourceBundle.getOperationId(operationId));
            //convert StackTrace Message to string
            StringWriter sw = new StringWriter();
            ex.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            EmailSender.sendMail("ranjana@shrigroup.net",
            		OprationIdResourceBundle.getOperationId(operationId).toString(),exceptionAsString );
           /* EmailSender.sendMail("pushpinder.singh@matrology.com",
            		OprationIdResourceBundle.getOperationId(operationId).toString(),exceptionAsString );*/
            LOG.error(errorCode, ex);
        }
        return genericResponse;
    }
    
    
    public static <T> String toJsonString(T input) {
    	try {
    		return new ObjectMapper().writeValueAsString(input);
    	} catch (Exception e) {
    		//Ideally, this exception should never happen if JSON
    		//Will occur only if JSON is not well formed
    		LOG.error("Could not transform JSON to String", e);
    		return "";
    	}
    }
    
    public static <T> String toJsonString(T input,Long Id) {
    	try {
    		String val = new ObjectMapper().writeValueAsString(input);
    		return val.concat(" customerId: "+Id);
    	} catch (Exception e) {
    		//Ideally, this exception should never happen if JSON
    		//Will occur only if JSON is not well formed
    		LOG.error("Could not transform JSON to String", e);
    		return "";
    	}
    }
    
    public static Auth createAuth(String authToken) {
        Auth auth = new Auth();
        auth.setAuthToken(authToken);
        return auth;
    }
    
	public static <T extends GenericResponse, V> T setErrorResponse(T genericResponse, Exception ex, String exceptionPropertyName, Locale locale, V request) {
		if (ex instanceof UserException) {
			String exceptionMessage = ExceptionResourceBundle.getExceptionMessage(exceptionPropertyName, locale);
        	setErrorResponse(genericResponse, ((UserException)ex).getCode(), ((UserException)ex).getMessage());
            //TODO: This logger statement should not make it to the production
            LOG.error(exceptionMessage, ex);
        } else {
        	setErrorResponse(ex, exceptionPropertyName, locale, genericResponse, toJsonString(request));
        }
        return genericResponse;
	}
	
	
	public static <T extends GenericResponse, V> T setErrorResponse(Exception ex, String exceptionPropertyName, Locale locale, T genericResponse, String... param) {
		String exceptionMessage = ExceptionResourceBundle.getExceptionMessage(exceptionPropertyName, locale);
		if (ex instanceof UserException) {
        	setErrorResponse(genericResponse, ((UserException)ex).getCode(), ((UserException)ex).getMessage());
            //TODO: This logger statement should not make it to the production
            LOG.error(exceptionMessage, ex);
        } else {
        	//This will be the segment of exceptions which are not handled by the developer correctly.
        	//Each time we receive an unhandled issue, we should revisit our code and see how
        	//we can correct it
            String errorCode = "UNHANDLED-ISSUE";
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = new Date();
            errorCode = errorCode + "-" + dateFormat.format(date) + "-" + new Random().nextInt(999999);
            
            //in the case of unhandled issue, generate a unique error code and inform the developer
            setErrorResponse(genericResponse, errorCode, exceptionMessage);
            
            StringBuilder builder = new StringBuilder();
            for (String arg : param) {
                builder.append(arg);
            }
            String appendedString = builder.toString();
            
            String errorString = "";
            if (Strings.isNullOrEmpty(appendedString)) {
            	errorString = String.format("ErrorCode : %s generated for Request", errorCode);
            } else {
            	errorString = String.format("ErrorCode : %s generated for Request: %s", errorCode, appendedString);
            }
            LOG.error(errorString, ex);
        }
        return genericResponse;
	}
	
	public static Date toDate(String dateString,  String timeString, Locale locale) throws ValidationException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.parse(dateString);
			
			sdf = new SimpleDateFormat(timeFormat);
			sdf.parse(timeString);
			
			sdf = new SimpleDateFormat(dateTimeFormat);
			return sdf.parse(dateString + " " + timeString);
		} catch (ParseException pe) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_DATE_TIME_FORMAT, locale));
		}
	}
	
	public static Date toDate(String dateString, Locale locale) throws ValidationException {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			return sdf.parse(dateString);
		} catch (ParseException pe) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_DATE_FORMAT, locale));
		}
	}
	
	public static String dateToString(Date dateOfBirth, Locale locale) throws ValidationException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformatter);
		return sdf.format(dateOfBirth);
	}
	
	public static String dateToFormattedString(String dateformatter,Date dateOfBirth, Locale locale) throws ValidationException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformatter);
		return sdf.format(dateOfBirth);
	}
	
	/*public static void checkIfOrgIdProvided(String orgId, Locale locale) throws ValidationException {
		if (Strings.isNullOrEmpty(orgId)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.NO_ORG_ID_PROVIDED, locale));
		}
		
	}*/

	public static String getMinSupportedVersion(String platform) {
		String minSupportedVer = "";
		if(platform.equals("Android")){
			minSupportedVer = prop.getProperty(ResourceProperties.MIN_SUPPORTED_VER_ANDROID);
			}
		else if(platform.equals("IOS")){
			minSupportedVer = prop.getProperty(ResourceProperties.MIN_SUPPORTED_VER_IOS);
			}
		else{
			if(platform.equals("01")){
				minSupportedVer = prop.getProperty(ResourceProperties.MIN_SUPPORTED_VER_CUSTOMER);
			}
			else if(platform.equals("02")){
				minSupportedVer = prop.getProperty(ResourceProperties.MIN_SUPPORTED_VER_ADMIN);
			}
		}
		return minSupportedVer;
	}
	
	public static Object toJsonString(GenerateMobileOtpResponse response) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static String getDateAsString(long timeStamp) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timeStamp);
		return dateFormat.format(calendar.getTimeInMillis());
	}

	}
