package com.maidin.customerApi.photo;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Locale;

import com.maidin.common.exception.ValidationException;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.pojo.photo.AddPhoto;
import com.maidin.pojo.photo.AddPhotoResponse;
import com.maidin.pojo.photo.ChangePrimaryPhoto;
import com.maidin.pojo.photo.ChangePrimaryPhotoResponse;
import com.maidin.pojo.photo.DeletePhotoResponse;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.photo.GetPhotoResponse;
import com.maidin.pojo.photo.GetPrimaryPhotoResponse;
import com.maidin.pojo.photo.UpdatePhotoResponse;


public interface PhotoMgmt {
	
	/*public AddPhotoResponse addPhoto(String authToken, int imageType , Long imageId, PhotoFiles photoFiles, Locale locale)
			throws Exception;
	
	public BufferedImage resizeImageWithType(BufferedImage originalImage, String imageType);*/
	
	public RetrieveFileResponse retrievePhoto(int imageType, long itemId, long fileStoreId, Locale locale) throws Exception;
	
	public long getFileStoreId(int imageType, long itemId, String version, Locale locale) throws ValidationException ;

	public long getFileStoreIdOfProduct(int imageType, long itemId, long productDetailsId, Locale locale) throws Exception;

	public GetAllPhotosResponse getImageList(String authToken, long productId, Locale locale) throws Exception;

	public long getFileStoreIdByPhotoId(long photoId, Locale locale) throws Exception;

	public RetrieveFileResponse retrieveIndividualPhoto(long photoId, long fileStoreId, Locale locale) throws Exception;

	public RetrieveFileResponse retrievePhotoV2(int imageType, long itemId, long fileStoreId, Locale locale) throws Exception;

	long getFileStoreIdV1(int imageType, long itemId, Locale locale) throws Exception;

	public Long getPhotoIdOfproductDetailId(Long imageCategoryId);

	public PhotoDTO getFileStoreIdByPhotoId(Long photoId);

	public String getVersionOfPhoto();
	
	/*public UpdatePhotoResponse updatePhoto(String authToken, PhotoFiles photoFiles, Locale locale) throws Exception;

	public GetPhotoResponse getPhoto(String authToken, long forUser, Locale locale) throws Exception;
	
	public DeletePhotoResponse deleteAllPhoto(String authToken, long forUser, Locale locale)
			throws Exception;
	
	public DeletePhotoResponse deletePhoto(String authToken, List<Long> photoIds, Locale locale) 
			throws Exception;

	public GetPrimaryPhotoResponse getPrimaryPhoto(String authToken, long parseLong, Locale locale) throws Exception;

	public ChangePrimaryPhotoResponse changePrimaryPhoto(String authToken, long parseLong, ChangePrimaryPhoto request,
			Locale locale) throws Exception;
	
	public GetAllPhotosResponse getAllPhotos(String authToken, long parseLong, Locale locale) throws Exception;
	
	public RetrieveFileResponse retrievePrimaryPhoto(
			//String authToken, 
			long parseLong, Locale locale) throws Exception;

	public RetrieveFileResponse getDemoPhoto(long parseLong, Locale locale) throws Exception;*/
}
