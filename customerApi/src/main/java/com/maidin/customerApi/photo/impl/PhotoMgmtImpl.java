package com.maidin.customerApi.photo.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.google.common.base.Strings;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.enums.ImageCategory;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.filestore.FileStoreMgmt;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.customerApi.photo.PhotoMgmt;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.FileStoreDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.FileStoreDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.photo.PhotoAllAttrs;

public class PhotoMgmtImpl implements PhotoMgmt {

	private final PhotoDAO photoDAO;
	private final FileStoreDAO fileStoreDAO;
	private final FileStoreMgmt fileStoreMgmt;
	private final AuthenticationMgmt authenticationMgmt;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;

	@Autowired
	public PhotoMgmtImpl(PhotoDAO photoDAO, FileStoreDAO fileStoreDAO, FileStoreMgmt fileStoreMgmt,
			AuthenticationMgmt authenticationMgmt, ProductInfoV2DAO productInfoV2DAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO) {
		this.photoDAO = photoDAO;
		this.fileStoreDAO = fileStoreDAO;
		this.fileStoreMgmt = fileStoreMgmt;
		this.authenticationMgmt = authenticationMgmt;
		this.productInfoV2DAO = productInfoV2DAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrievePhoto(int imageType, long itemId, long fileStoreId, Locale locale)
			throws Exception {
		if(imageType == ImageCategory.BRAND.getId()){
		SubCategoryInfoV2DTO subCategoryInfoV2DTO = subCategoryInfoV2DAO.getById((int)itemId);
		itemId = subCategoryInfoV2DTO.getBrandIdV1();
		}
		if(imageType == ImageCategory.PRODUCT.getId()){
			PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(imageType, itemId, "v1");
			itemId = photoDTO.getItemIdV1();
		}
		if(imageType == ImageCategory.PRODUCTDETAIL.getId()){
			/*PhotoDTO photoDTO = photoDAO.getByFileStoreId(fileStoreId);
			itemId = photoDTO.getItemId();
			*/return fileStoreMgmt.retrieveFileV2FromBucket2(imageType, fileStoreId, true, locale);
		}
		
		return fileStoreMgmt.retrieveFile(imageType, itemId, fileStoreId, true, locale);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreIdV1(int imageType, long itemId, Locale locale) throws ValidationException {
		long fileStoreId = -1l;
		PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(imageType, itemId);
		if (photoDTO == null) {

		} else {
			fileStoreId = photoDTO.getFileStoreId();

		}

		return fileStoreId;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreId(int imageType, long itemId, String version, Locale locale) throws ValidationException {
		long fileStoreId = -1l;
		/*PhotoDTO photoDTO = photoDAO.getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto(imageType, itemId, version, true);
		if (photoDTO == null) {
			List<PhotoDTO> photoDTOList = photoDAO.getListByPhotoTypePhotoIdAndVersion(imageType, itemId, version);
			if (photoDTOList.size() > 0) {
				fileStoreId = photoDTOList.get(0).getFileStoreId();
			}
			// throw new ValidationException(
			// ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE,
			// locale));
		} else {
			fileStoreId = photoDTO.getFileStoreId();

		}
*/
		PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(imageType, itemId, version);
		if (photoDTO != null) {
			fileStoreId = photoDTO.getFileStoreId();
		}
		return fileStoreId;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreIdOfProduct(int imageType, long itemId, long productDetailsId, Locale locale)
			throws Exception {
				return productDetailsId;/*
		long fileStoreId = -1l;
		List<PhotoDTO> photoDTOList = photoDAO.getListByPhotoTypePhotoIdAndVersion(imageType, itemId, "v2");
		if (photoDTOList == null || photoDTOList.size() == 0) {
			
			 * throw new ValidationException(
			 * ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.
			 * PHOTO_NOT_PRESENT, locale));
			 } else {
			if (photoDTOList.size() == 1) {
				return photoDTOList.get(0).getFileStoreId();
			} else {
				for (PhotoDTO photoDTO : photoDTOList) {
					String productDetailsIds = photoDTO.getProductDetailIds();
					if (!Strings.isNullOrEmpty(productDetailsIds)) {
						String[] productDetailStringArray = productDetailsIds.split(",");
						for (String productDetailIdInString : productDetailStringArray) {
							if (Long.parseLong(productDetailIdInString) == productDetailsId) {
								return photoDTO.getFileStoreId();
							}
						}
					}
				}
				// return primary Photo if multiple images exist
				PhotoDTO photoDTO = photoDAO.getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto(imageType, itemId, "v2",
						true);
				if (photoDTO != null) {
					fileStoreId = photoDTO.getFileStoreId();
				}
			}
		}
		return fileStoreId;
	*/}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetAllPhotosResponse getImageList(String authToken, long productDetailId, Locale locale) throws Exception {
		authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		ProductCityInfoV2DTO pcCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		String photoGroup = pcCityInfoV2DTO.getPhotoGroup();
		List<Long> photoIds = new ArrayList<>();
		if (!Strings.isNullOrEmpty(photoGroup)) {
			String[] timeSlotStringArr = photoGroup.split(",");
			long[] timeSlotIntArr = Stream.of(timeSlotStringArr).mapToLong(Long::parseLong).toArray();
			photoIds = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
		}
		String productDescription = "";
		if (photoIds.size() > 0) {
			List<PhotoDTO> photoDTOs = photoDAO.getByIds(photoIds);
			for (PhotoDTO dto : photoDTOs) {
				PhotoAllAttrs attrs = new PhotoAllAttrs();
				FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(dto.getFileStoreId());
				attrs.setPhotoId(dto.getId());
				attrs.setPrimaryPhoto(dto.isPrimaryPhoto());
				// attrs.setImageType(ImageCategory.fromId(dto.getImageType()).getId());
				attrs.setFileName(filstoreDTO.getFileName());
				attrs.setImageUpdatedOn(dto.getUpdatedOn().getTime());
				response.getPhotoAllAttrs().add(attrs);
			}
			
			
		} else {
			response.getPhotoAllAttrs().clear();
		}
		ProductCityInfoV2DTO pCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		if(pCityInfoV2DTO != null){
		ProductInfoV2DTO pInfoV2DTO = productInfoV2DAO
				.getByIdWithoutActiveState(pCityInfoV2DTO.getProductId());
		if (pInfoV2DTO != null &&  pInfoV2DTO.getDescription() != null) {
			productDescription = pInfoV2DTO.getDescription();
		}
		}
		response.setProductDescription(productDescription);

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);

		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreIdByPhotoId(long photoId, Locale locale) throws Exception {
		PhotoDTO photoDTO = photoDAO.getById(photoId);
		if (photoDTO != null) {
			return photoDTO.getFileStoreId();
		}
		return -1;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrieveIndividualPhoto(long photoId, long fileStoreId, Locale locale) throws Exception {
		RetrieveFileResponse response = null;
		PhotoDTO photoDTO = photoDAO.getById(photoId);
		if (photoDTO == null) {
			/*throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));*/
		}
		else if(photoDTO.getVersion().equals("v1")){
			response =  fileStoreMgmt.retrieveFile(photoDTO.getImageType(), photoDTO.getItemIdV1(), fileStoreId, true, locale);
		}
		else{
			try{
			response =  fileStoreMgmt.retrieveFileV2FromBucket2(photoDTO.getImageType(), fileStoreId, true, locale);
			}
			catch(AmazonS3Exception e){
				//if(response == null){
				response= 	fileStoreMgmt.retrieveFileV2FromBucket1(fileStoreId, true, locale);
				//}
			}
			
		}
		return response;
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrievePhotoV2(int imageType, long itemId, long fileStoreId, Locale locale)
			throws Exception {
		return null;
		//return fileStoreMgmt.retrieveFileV2(photoDTO.getId(), fileStoreId, true, locale);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public Long getPhotoIdOfproductDetailId(Long productDetailId) {
		ProductCityInfoV2DTO pcInfo =  productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		/*if(pcInfo == null){
			return null;
		}*/
		/*PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(ImageCategory.PRODUCTDETAIL.getId(), pcInfo.getId(), "v2");
		if(photoDTO != null){
			return photoDTO.getId();
		}*/
		if(pcInfo != null && pcInfo.getPhotoId() !=  null){
			return pcInfo.getPhotoId();
		}
		else 
			return null;
		
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public PhotoDTO getFileStoreIdByPhotoId(Long photoId) {
		PhotoDTO photDTO = photoDAO.getById(photoId);
		if(photDTO != null){
			return photDTO;
		}
		return null;
	}

	@Override
	public String getVersionOfPhoto() {
		// TODO Auto-generated method stub
		return null;
	}

}
