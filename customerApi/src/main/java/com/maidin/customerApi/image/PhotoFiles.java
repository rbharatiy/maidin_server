package com.maidin.customerApi.image;

import com.google.common.io.ByteSource;
import com.maidin.pojo.common.StoreFile;

public class PhotoFiles {

	private final StoreFile photo1StoreFile;
	private final ByteSource photo1ByteSource;

	public PhotoFiles(StoreFile photo1StoreFile, ByteSource photo1ByteSource) {
		this.photo1StoreFile = photo1StoreFile;
		this.photo1ByteSource = photo1ByteSource;
	}

	public StoreFile getPhoto1StoreFile() {
		return photo1StoreFile;
	}

	public ByteSource getPhoto1ByteSource() {
		return photo1ByteSource;
	}

}
