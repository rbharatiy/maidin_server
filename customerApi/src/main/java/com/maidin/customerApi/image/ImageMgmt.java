package com.maidin.customerApi.image;

import java.util.Locale;

import com.maidin.pojo.cart.CartRequest;
import com.maidin.pojo.cart.CartResponse;
import com.maidin.pojo.cart.CheckoutRequest;
import com.maidin.pojo.cart.CheckoutResponse;
import com.maidin.pojo.cart.ConfirmOrderRequest;
import com.maidin.pojo.cart.ConfirmOrderResponse;
import com.maidin.pojo.cart.EditOrder;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.MyCartResponse;
import com.maidin.pojo.cart.ScheduleDetails;
import com.maidin.pojo.images.AddPhotoResponse;
import com.maidin.pojo.payment.PaymentDates;
import com.maidin.pojo.payment.PaymentHistoryRequest;
import com.maidin.pojo.payment.PaymentHistoryResponse;
import com.maidin.pojo.product.CategoryListResponse;

public interface ImageMgmt {

	AddPhotoResponse addPhoto(String authToken, PhotoFiles photoFiles, Locale locale);



}
