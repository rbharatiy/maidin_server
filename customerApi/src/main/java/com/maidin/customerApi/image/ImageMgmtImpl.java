package com.maidin.customerApi.image;

import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.io.ByteSource;
import com.maidin.customerApi.enums.DocumentType;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.pojo.common.StoreFile;
import com.maidin.pojo.images.AddPhotoResponse;

public class ImageMgmtImpl implements ImageMgmt {

	private final ProductDAO productDAO;
	private final ProductBrandsDAO brandDAO;
	private final ProductCategoryDAO categoryDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	//private final CommonMgmt commonMgmt;
	

	@Autowired
	public ImageMgmtImpl(ProductDAO productDAO, ProductBrandsDAO brandDAO, ProductCategoryDAO categoryDAO,
			AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO/*, CommonMgmt commonMgmt*/) {

		this.productDAO = productDAO;
		this.brandDAO = brandDAO;
		this.categoryDAO = categoryDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		//this.commonMgmt = commonMgmt;
		
	}

	private static final Logger logger = LoggerFactory.getLogger(ImageMgmtImpl.class);


	@Override
	public AddPhotoResponse addPhoto(String authToken, PhotoFiles photoFiles, Locale locale) {
		return null;/*
		
		AddPhotoResponse response = new AddPhotoResponse();

		long photo1FileStoreId = uploadFile(photoFiles.getPhoto1StoreFile(), photoFiles.getPhoto1ByteSource(),
				locale);
		long photo2FileStoreId = uploadFile(photoFiles.getPhoto2StoreFile(), photoFiles.getPhoto2ByteSource(),
				locale);
		long photo3FileStoreId = uploadFile(photoFiles.getPhoto3StoreFile(), photoFiles.getPhoto3ByteSource(),
				locale);
		long photo4FileStoreId = uploadFile(photoFiles.getPhoto4StoreFile(), photoFiles.getPhoto4ByteSource(),
				locale);
		long photo5FileStoreId = uploadFile(photoFiles.getPhoto5StoreFile(), photoFiles.getPhoto5ByteSource(),
				locale);

	
		List<Long> ids = commonMgmt.addUserDocument(DocumentType.PHOTO.getName(), null, photo1FileStoreId,
				photo2FileStoreId, photo3FileStoreId, photo4FileStoreId, photo5FileStoreId);


		
		
		Map<Long, StoreFile> map = new HashMap<Long, StoreFile>();
		map.put(photo1FileStoreId, photoFiles.getPhoto1StoreFile());
		map.put(photo2FileStoreId, photoFiles.getPhoto2StoreFile());
		map.put(photo3FileStoreId, photoFiles.getPhoto3StoreFile());
		map.put(photo4FileStoreId, photoFiles.getPhoto4StoreFile());
		map.put(photo5FileStoreId, photoFiles.getPhoto5StoreFile());
		
		checkConstraint(userId, primaryPhoto, locale);
		
		List<PhotoDTO> listPhotos = photoDAO.getAllPhotosByUserId(userId);	
		if (!Strings.isNullOrEmpty(primaryPhoto)){
		if ((listPhotos.size() > 0) ){
			for(PhotoDTO dto : listPhotos){
				dto.setPrimaryPhoto(false);
				photoDAO.updatePhoto(dto);
			}
		}
		}
			 Iterator<Entry<Long, StoreFile>> it = map.entrySet().iterator();
			 	
			    while (it.hasNext()) {
			    	boolean isprimaryPhoto =false;
			        Map.Entry pair = (Map.Entry)it.next();
			        if((Long)pair.getKey()== -1)
			        	continue;
			        String  fileName = ((StoreFile)pair.getValue()).getFileName();
			        if (!Strings.isNullOrEmpty(primaryPhoto)){//if filename is present in header
			        	
			        	
			        	if(primaryPhoto.equals(fileName)){
			        		response.setPrimaryPhotoFileName(fileName);
			        		isprimaryPhoto = true;
			        		
			        	}
			        }
			        else                                     //if filename is not  present in header
					{
			        	if(listPhotos.size() >= 0 ){
							PhotoDTO dto = photoDAO.getPhotoByUserIdAndPrimaryKey(userId);
							if(dto == null){
							    isprimaryPhoto = true;
							}
							else{
								isprimaryPhoto = false;	
							}
						}
					}
			        PhotoAllAttrs attrs = new PhotoAllAttrs();
			        List<PhotoAllAttrs> attrList = response.getPhotoAllAttrs();
			        long photoId = addPhoto(userId,(Long)pair.getKey(),isprimaryPhoto);
			        PhotoDTO dto = photoDAO.getPhotoByPhotoId(photoId);
			        FileStoreDTO filstoreDTO = fileStoreDAO.getFile(userId, dto.getProfilePhotoUserFileId());
			        attrs.setPhotoId(dto.getId());
			        attrs.setPrimaryPhoto(dto.isPrimaryPhoto());
			        attrs.setProfilePhotoUserFileId(dto.getProfilePhotoUserFileId());
			        attrs.setUserId(dto.getUserId());
			        attrs.setFileName(filstoreDTO.getFileName());
			        attrList.add(attrs);
			       
			       
			    }

		response = ApiUtils.setResponse(response);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	*/}

	private long uploadFile(StoreFile storeFile, ByteSource source, Locale locale) throws Exception {
		/*if (storeFile != null && source != null) {
			return fileStoreMgmt.uploadFile(storeFile, source, locale);
		}*/
		return ApiUtils.INVALID_ID;
	}


}
