package com.maidin.customerApi.enums;

public enum DocumentType {
	
	
	PHOTO(1, "PHOTO");
	
	
	
	private final int id;
	private final String name;
	
	private DocumentType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static DocumentType fromId(int id) throws Exception {
		for (DocumentType t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Document Type: " + id);
	}
}
