package com.maidin.customerApi.enums;

public enum RemarkStatus {
	
	UNDELIVERED(1, "UNDELIVERED"),
	
	DELIVERED(2, "DELIVERED")
	;
	
	private final int id;
	private final String name;
	
	private RemarkStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static RemarkStatus fromId(int id) throws Exception {
		for (RemarkStatus t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Order Status: " + id);
	}
}
