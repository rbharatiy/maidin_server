package com.maidin.customerApi.enums;

public enum UserCategory {

    CUSTOMER(1, "CUSTOMER"),
	
	ADMIN(2, "ADMIN");
	
	private final int id;
	private final String name;
	
	private UserCategory(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	//Manish: Please give proper names to the variables 
	//Ranjana: Done
	public static UserCategory fromId(int id) throws Exception {
		for (UserCategory userCategory : values()) {
			if (userCategory.id == id) {
				return userCategory;
			}
		}
		throw new Exception("Invalid UserCategory: " + id);
	}
}
