package com.maidin.customerApi.enums;

public enum CustomerTransactionsType {
	
	All(1, "ALL"),
	
	MONEY_IN(2, "MONEY_IN"),
	
	MONEY_OUT(3, "MONEY_OUT");
	
	
	private final int id;
	private final String name;
	
	private CustomerTransactionsType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static CustomerTransactionsType fromId(int id) throws Exception {
		for (CustomerTransactionsType t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid transaction type: " + id);
	}
}
