package com.maidin.customerApi.cart;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.CancelOrder;
import com.maidin.pojo.cart.CartRequest;
import com.maidin.pojo.cart.CartResponse;
import com.maidin.pojo.cart.CheckoutRequest;
import com.maidin.pojo.cart.CheckoutResponse;
import com.maidin.pojo.cart.EditOrder;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.GetOrderResponseV1;
import com.maidin.pojo.cart.GetOrderResponseV2;
import com.maidin.pojo.cart.GetScheduledProductResponseV2;
import com.maidin.pojo.cart.MyCartResponse;
import com.maidin.pojo.cart.ScheduleDetails;
import com.maidin.pojo.cart.ScheduleProductRequestV2;

public interface ShoppingCartMgmt {

	public CartResponse addProductToCart(CartRequest request, String authToken, Locale locale) throws Exception;

	public CartResponse removeProductFromCart(CartRequest request, String authToken, Locale locale)  throws Exception;

	public MyCartResponse getCart(String authToken, Locale locale) throws Exception;

//	public ConfirmOrderResponse confirmOrder(ConfirmOrderRequest request, String authToken, Locale locale) throws Exception;

	public CheckoutResponse checkout(CheckoutRequest request, String authToken, Locale locale) throws Exception;

	public GetOrderResponse getOrders(String authToken, Locale locale) throws Exception;

	public GetOrderResponse scheduleOrder(String authToken, ScheduleDetails request, Locale locale) throws Exception;

	public GetOrderResponse editOrder(String authToken, EditOrder request, Locale locale) throws Exception;

	public GenericResponse clearOrder(String authToken, Locale locale)  throws Exception;

	public GenericResponse scheduleProductV2(String authToken, ScheduleProductRequestV2 request, Locale locale) throws Exception;

	public GetScheduledProductResponseV2 getV2ScheduleList(String authToken, Locale locale) throws Exception;

	public GetOrderResponseV1 getV1Orders(String authToken, Locale locale) throws Exception;

	public GetOrderResponseV2 getV2UpcomingOrders(String authToken, Locale locale) throws Exception;

	public GenericResponse cancelUpcomingV2(String authToken, CancelOrder request, Locale locale)  throws Exception;

	public GenericResponse scheduleOrderV2ForExistingSchedule(Long scheduleId, Locale locale) throws Exception;

	

	

}
