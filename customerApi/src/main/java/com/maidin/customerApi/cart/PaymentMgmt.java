package com.maidin.customerApi.cart;

import java.util.Locale;

import com.maidin.pojo.payment.PaymentHistoryResponse;

public interface PaymentMgmt {

	public PaymentHistoryResponse getPaymentHistory(String authToken, Locale locale) throws Exception;

	/*public PaymentHistoryResponse getPaymentHistoryByCalendar(String authToken, PaymentHistoryRequest request,
			Locale locale) throws Exception;
*/
	public PaymentHistoryResponse getPaymentHistoryByPagination(String authToken, Integer currentPageNumber,
			Locale locale) throws Exception;

	/*public PaymentHistoryResponse getPaymentHistoryByPagination(String authToken, PaymentDates request, Locale locale) throws Exception;*/

	

	

}
