package com.maidin.customerApi.cart.impl;

import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.maidin.customerApi.enums.RemarkStatus;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.SmsSender;
import com.maidin.customerApi.cart.ShoppingCartMgmt;
import com.maidin.customerApi.enums.OrderStatus;
import com.maidin.customerApi.enums.PaymentMode;
import com.maidin.customerApi.enums.PaymentStatus;
import com.maidin.customerApi.enums.ScheduleStatus;
import com.maidin.customerApi.enums.WalletDescription;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.CommonApplication;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;

import com.maidin.persistence.dao.BlockedSlotDAO;

import com.maidin.persistence.dao.AddressDAO;

import com.maidin.persistence.dao.CartDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dto.BlockedSlotDTO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;

import com.maidin.persistence.dto.CartDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.UpcomingOrderDTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.cart.CancelOrder;
import com.maidin.pojo.cart.CartDetails;
import com.maidin.pojo.cart.CartRequest;
import com.maidin.pojo.cart.CartResponse;
import com.maidin.pojo.cart.CheckoutRequest;
import com.maidin.pojo.cart.CheckoutResponse;
//import com.maidin.pojo.cart.ConfirmOrder;
import com.maidin.pojo.cart.ConfirmOrderDetails;
import com.maidin.pojo.cart.EditOrder;
import com.maidin.pojo.cart.GetOrderResponse;
import com.maidin.pojo.cart.GetOrderResponseV1;
import com.maidin.pojo.cart.GetOrderResponseV2;
import com.maidin.pojo.cart.GetScheduledProductResponseV2;
import com.maidin.pojo.cart.MyCartResponse;
import com.maidin.pojo.cart.OrderDetails;
import com.maidin.pojo.cart.OrderDetailsV1;
import com.maidin.pojo.cart.OrderDetailsV2;
import com.maidin.pojo.cart.OrderInfo;
import com.maidin.pojo.cart.OrderInfoV1;
import com.maidin.pojo.cart.OrderInfoV2;
import com.maidin.pojo.cart.ScheduleDetails;
import com.maidin.pojo.cart.ScheduleProductRequestV2;
import com.maidin.pojo.cart.ScheduledProductInfoV2;
import com.maidin.pojo.cart.TimeSlotWise;
import com.maidin.pojo.cart.TimeSlotWiseV1;
import com.maidin.pojo.cart.TimeSlotWiseV2;
import com.maidin.pojo.cart.UpdatedOrderDetails;
import com.maidin.pojo.customerorders.ByDeliveryDate;
import com.maidin.pojo.customerorders.ByTimeSlot;
import com.maidin.pojo.customerorders.GetUpcomingOrderResponse;
import com.maidin.pojo.customerorders.OrderDetailsInfo;

import antlr.StringUtils;

public class ShoppingCartMgmtImpl implements ShoppingCartMgmt {

	private final ProductDAO productDAO;
	private final ProductBrandsDAO brandDAO;
	private final ProductCategoryDAO categoryDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final CartDAO cartDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final PaymentModeDAO paymentModeDAO;
	private final JSONArray timeSlotJsonArray;
	private final ScheduleInfoDAO scheduleDAO;
	private final WalletDAO walletDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final BlockedSlotDAO blockedSlotDAO;
	private final ProductCityInfoV2DAO productCityInfoDAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CommonApplication commonApplication;

	Joiner joiner = Joiner.on(",").skipNulls();

	@Autowired
	public ShoppingCartMgmtImpl(ProductDAO productDAO, ProductBrandsDAO brandDAO, ProductCategoryDAO categoryDAO,
			AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO, TimeSlotDAO timeSlotDAO, CartDAO cartDAO,
			OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO, PaymentModeDAO paymentModeDAO,
			ScheduleInfoDAO scheduleDAO, WalletDAO walletDAO, CustomerTransactionDAO transactionDAO,
			BlockedSlotDAO blockedSlotDAO, ProductCityInfoV2DAO productCityInfoDAO, BrandInfoV2DAO brandInfoV2DAO,
			ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CommonApplication commonApplication) {

		this.productDAO = productDAO;
		this.brandDAO = brandDAO;
		this.categoryDAO = categoryDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.cartDAO = cartDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.paymentModeDAO = paymentModeDAO;
		this.scheduleDAO = scheduleDAO;
		this.walletDAO = walletDAO;
		this.transactionDAO = transactionDAO;
		this.blockedSlotDAO = blockedSlotDAO;
		this.productCityInfoDAO = productCityInfoDAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.commonApplication = commonApplication;

		this.timeSlotJsonArray = readTimeSlotJSON(System.getProperty("config.dir") + "/json/allLists.json");
	}

	private static final Logger logger = LoggerFactory.getLogger(ShoppingCartMgmtImpl.class);

	@Transactional(rollbackFor = Exception.class)
	public CartResponse addProductToCart(CartRequest request, String authToken, Locale locale) throws Exception {
		//logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		CartResponse response = new CartResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO dto = customerDAO.getCustomerByMobileNumber(mobile);
		addProductToCart(dto.getId(), request);
		int cartSize = cartDAO.getByCustomerId(dto.getId()).size();
		response.setOrderCount(cartSize);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		//logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CartResponse removeProductFromCart(CartRequest request, String authToken, Locale locale) throws Exception {
		//logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		CartResponse response = new CartResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO dto = customerDAO.getCustomerByMobileNumber(mobile);
		removeProductFromCart(dto.getId(), request.getProductId());
		int cartSize = cartDAO.getByCustomerId(dto.getId()).size();
		response.setOrderCount(cartSize);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		//logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public MyCartResponse getCart(String authToken, Locale locale) throws Exception {
		MyCartResponse response = new MyCartResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO cartDto = customerDAO.getCustomerByMobileNumber(mobile);
		List<CartDTO> cartList = cartDAO.getByCustomerId(cartDto.getId());
		Iterator<CartDTO> it = cartList.iterator();
		CartDetails cartDetails = null;
		while (it.hasNext()) {
			cartDetails = setCartResponse(it.next());
			response.getCartDetails().add(cartDetails);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		//logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponse editOrder(String authToken, EditOrder request, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
		
		//logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		GetOrderResponse response = new GetOrderResponse();
		List<UpdatedOrderDetails> orderList = request.getUpdatedOrderList();
		for (UpdatedOrderDetails orderDetails : orderList) {
			OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetails.getOrderId());
			if (orderDetailsInfoDTO != null
					&& orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
				validateEditOrderRequest(orderDetails, orderDetailsInfoDTO.getOrderId(), locale);
				editOrders(customerDTO.getId(), orderDetailsInfoDTO, orderDetails, locale);
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_ORDER, locale));
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(),ApiUtils.toJsonString(response));
		return response;
	}

	private void validateEditOrderRequest(UpdatedOrderDetails request, Long orderId, Locale locale)
			throws ValidationException {
		if (prop.getProperty(ResourceProperties.IS_CHECKOUT_BLOCKED).equalsIgnoreCase("ON")) {
			List<BlockedSlotDTO> blockedSlotDTOList = blockedSlotDAO
					.getByBlockedDate(new Date(request.getDeliveryTimestamp()));
			for (BlockedSlotDTO blockedSlotDTO : blockedSlotDTOList) {
				String timeSlotIds = blockedSlotDTO.getTimeSlotIds();
				String[] timeSlotStrArr = timeSlotIds.split(",");
				int[] timeSlotIntArr = Stream.of(timeSlotStrArr).mapToInt(Integer::parseInt).toArray();
				List<Integer> timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				if (timeSlotIdList.contains(request.getTimeSlotId())) {
					List<String> timeSlotDTOList = timeSlotDAO.getByIds(timeSlotIdList);
					String timeSlotsCommaSeperated = timeSlotDTOList.stream().map(String::toLowerCase)
							.collect(Collectors.joining(", "));

					String message = timeSlotsCommaSeperated + " slots are not available for "
							+ ApiUtils.dateToFormattedString(blockedSlotDTO.getDateFormatter(),
									blockedSlotDTO.getBlockedDate(), locale)
							+ ".";
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(message,
							ExceptionCode.SLOT_NOT_AVAILABLE, locale));
				}
			}

		}

	}

	private void editOrders1(Long customerId, OrderDetailsInfoDTO orderDetailsInfoDTO,
			UpdatedOrderDetails orderDetails) {
	}

	private void editOrders(Long customerId, OrderDetailsInfoDTO orderDetailsInfoDTO,
			UpdatedOrderDetails requestedOrderDetails, Locale locale) throws ValidationException {
		OrderInfoDTO orderInfo = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
		int changeInQuantity = 0;
		if (requestedOrderDetails.getQuantity() == 0) {
			changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
			checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity,
					orderDetailsInfoDTO.getTotalProductCost(), "CANCELLED");
			orderDetailsInfoDTO.setCancelled(true);
			orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
			orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
			orderDetailsInfoDTO.setPayBy(null);
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			//orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderDetailsInfoDTO.getOrderId());
			if (orderDetailsInfoList != null) {
				if (orderDetailsInfoList.size() == 1) {
					orderInfo.setCancelled(true);
					orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
					orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
					orderInfo.setPayBy(null);
					orderInfo.setOrderSubTotal(0);
					orderInfo.setCancellationTime((new Timestamp(System.currentTimeMillis())));
					orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				} else {
					int cntCancelled = 0;
					//float totalBill = 0;
					for (OrderDetailsInfoDTO orderD : orderDetailsInfoList) {
						if (orderD.getOrderStatus().equals(OrderStatus.CANCELLED.getName())) {
							cntCancelled++;
						}
						/*else{
							totalBill += (orderD.getProductSalePrice()
									+ orderD.getDeliveryCharges())*orderD.getQuantity();
						}*/
					}
					//orderInfo.setOrderSubTotal(totalBill);
					if (cntCancelled == orderDetailsInfoList.size()) {
						orderInfo.setCancelled(true);
						orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
						orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
						orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
						orderInfo.setPayBy(null);
						orderInfo.setOrderSubTotal(0);
						orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
						//orderInfo.setPaymentModeId(0);
					}
				}
			}

		} else {
			// quantity is increased here

			if (requestedOrderDetails.getQuantity() > orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
				String productString = productCityInfoDAO.getProductName(orderDetailsInfoDTO.getProductId());
				ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
						.getById(orderDetailsInfoDTO.getProductId(),true);
				if(productCityInfoV2DTO == null){
					if(productString == null){
						productString = "Product";
					}
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productString, ExceptionCode.CANNOT_INCREASE_QUANTITY, locale));
				}
				if ((productCityInfoV2DTO.getSalePrice()
						+ productCityInfoV2DTO.getDeliveryCharge()) != (orderDetailsInfoDTO.getProductSalePrice()
								+ orderDetailsInfoDTO.getDeliveryCharges())) {
					ConfirmOrderDetails request = new ConfirmOrderDetails();
					request.setProductId(orderDetailsInfoDTO.getProductId());
					request.setQuantity(changeInQuantity);
					Long orderDetailsId = addOrderDetailsForCheckout(orderDetailsInfoDTO.getOrderId(), request, locale);
				} else {
					if (orderDetailsInfoDTO.getTotalProductCost() == 0) {
						changeInQuantity = requestedOrderDetails.getQuantity();
					} else {
						changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
					}
					orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
					orderDetailsInfoDTO.setTotalProductCost(orderDetailsInfoDTO.getQuantity()
							* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()));
					orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
				//}
				//}
			} else if (requestedOrderDetails.getQuantity() < orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
				orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
				orderDetailsInfoDTO.setTotalProductCost(requestedOrderDetails.getQuantity()
						* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()));
				orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity, changeInQuantity
						* (orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges()),
						"DECREASE");
			}
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(requestedOrderDetails.getTimeSlotId());
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
			if (orderInfoDTO != null) {
				if (timeSlotDTO != null) {
					orderInfoDTO.setDeliveryDate(new Date(requestedOrderDetails.getDeliveryTimestamp()));
					orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
					orderInfoDTO.setTimeSlotId(requestedOrderDetails.getTimeSlotId());
					orderInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
			}
		}

		setOrderSubTotal(orderInfo);

	}

	private void editOrdersV1(Long customerId, OrderDetailsInfoDTO orderDetailsInfoDTO,
			UpdatedOrderDetails requestedOrderDetails, Locale locale) throws ValidationException {
		OrderInfoDTO orderInfo = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
		int changeInQuantity = 0;
		if (requestedOrderDetails.getQuantity() == 0) {

			changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
			checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity,
					orderDetailsInfoDTO.getTotalProductCost(), "CANCELLED");
			orderDetailsInfoDTO.setCancelled(true);
			orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
			orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			//orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderDetailsInfoDTO.getOrderId());
			if (orderDetailsInfoList != null) {
				if (orderDetailsInfoList.size() == 1) {
					orderInfo.setCancelled(true);
					orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
					orderInfo.setCancellationTime((new Timestamp(System.currentTimeMillis())));
					orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				} else {
					int cntCancelled = 0;
					for (OrderDetailsInfoDTO orderD : orderDetailsInfoList) {
						if (orderD.getOrderStatus().equals(OrderStatus.CANCELLED.getName())) {
							cntCancelled++;
						}
					}
					if (cntCancelled == orderDetailsInfoList.size()) {
						orderInfo.setCancelled(true);
						orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
						orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
						orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
						orderInfo.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
						orderInfo.setPaymentModeId(0);
					}
				}
			}

		} else {
			// quantity is increased here

			if (requestedOrderDetails.getQuantity() > orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
				ProductDTO productDTO = productDAO.getByProductId(orderDetailsInfoDTO.getProductId());
				if (productDTO.getPrice() != orderDetailsInfoDTO.getProductMRP()) {
					ConfirmOrderDetails request = new ConfirmOrderDetails();
					request.setProductId(orderDetailsInfoDTO.getProductId());
					request.setQuantity(changeInQuantity);
					Long orderDetailsId = addOrderDetailsForCheckout(orderDetailsInfoDTO.getOrderId(), request, locale);
				} else {
					if (orderDetailsInfoDTO.getTotalProductCost() == 0) {
						changeInQuantity = requestedOrderDetails.getQuantity();
					} else {
						changeInQuantity = requestedOrderDetails.getQuantity() - orderDetailsInfoDTO.getQuantity();
					}
					orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
					orderDetailsInfoDTO.setTotalProductCost(
							orderDetailsInfoDTO.getQuantity() * orderDetailsInfoDTO.getProductMRP());
					orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
			} else if (requestedOrderDetails.getQuantity() < orderDetailsInfoDTO.getQuantity()) {
				changeInQuantity = orderDetailsInfoDTO.getQuantity() - requestedOrderDetails.getQuantity();
				orderDetailsInfoDTO.setQuantity(requestedOrderDetails.getQuantity());
				orderDetailsInfoDTO
						.setTotalProductCost(requestedOrderDetails.getQuantity() * orderDetailsInfoDTO.getProductMRP());
				orderDetailsInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				checkCustomerTransaction(customerId, orderDetailsInfoDTO.getId(), changeInQuantity,
						changeInQuantity * orderDetailsInfoDTO.getProductMRP(), "DECREASE");
			}
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(requestedOrderDetails.getTimeSlotId());
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
			if (orderInfoDTO != null) {
				if (timeSlotDTO != null) {
					orderInfoDTO.setDeliveryDate(new Date(requestedOrderDetails.getDeliveryTimestamp()));
					orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
					orderInfoDTO.setTimeSlotId(timeSlotDTO.getId());
					orderInfoDTO.setUpdatedOn((new Timestamp(System.currentTimeMillis())));
				}
			}
		}

		setOrderSubTotal(orderInfo);

	}

	private void reshuffleSameOrderPayment(Long orderId, Long customerId, float maidinBalance) {

		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledOrdersByOrderId(orderId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				// OrderInfoDTO orderInfoDTO =
				// orderInfoDAO.getById(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				float amountToPay = orderDetailsDTO.getTotalProductCost() - orderDetailsDTO.getAmountPaidByCustomer();
				if (maidinBalance > 0 && amountToPay > 0) {
					if (maidinBalance >= amountToPay) {
						transactionRequest.setMoneyOut(amountToPay);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + amountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, amountToPay, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = maidinBalance - amountToPay;
					} else {
						transactionRequest.setMoneyOut(maidinBalance);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + maidinBalance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, maidinBalance, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = 0;
					}
				}
				setPaymentId(orderDetailsDTO.getOrderId());
			}
			transactionRequest = null;
		}
	}

	// By Pushpinder
	private void setOrderSubTotal(OrderInfoDTO orderInfoDTO) {
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getByOrderId(orderInfoDTO.getId());
		float subtotal = 0;
		for (OrderDetailsInfoDTO orderDetail : orderDetailsList) {
			subtotal += (orderDetail.getProductSalePrice() + orderDetail.getDeliveryCharges())
					* orderDetail.getQuantity();
		}
		orderInfoDTO.setOrderSubTotal(subtotal);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CheckoutResponse checkout(CheckoutRequest request, String authToken, Locale locale) throws Exception {
		CheckoutResponse response = new CheckoutResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
		validateCheckout(request, locale);
		Iterator<ConfirmOrderDetails> confirmListIterator = request.getConfirmOrderList().iterator();
		if (request.getConfirmOrderList() != null && request.getConfirmOrderList().size() > 0) {
			Long orderId = addOrderInfo(customerDTO.getId(), request);
			float totalBill = 0;
			while (confirmListIterator.hasNext()) {
				ConfirmOrderDetails confirmOrder = confirmListIterator.next();
				confirmOrder.setDeliveryTimestamp(request.getDeliveryTimestamp());
				confirmOrder.setTimeSlotId(request.getTimeslotId());
				Long orderDetailsId = addOrderDetailsForCheckout(orderId, confirmOrder, locale);
				System.out.println("orderDetailsId: " + orderDetailsId);
				totalBill += (orderDetailsInfoDAO.getById(orderDetailsId).getProductSalePrice()
						+ orderDetailsInfoDAO.getById(orderDetailsId).getDeliveryCharges())*orderDetailsInfoDAO.getById(orderDetailsId).getQuantity();
			}
			OrderInfoDTO orderDTO = orderInfoDAO.getById(orderId);
			if (orderDTO != null) {
				orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
				orderDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
				orderDTO.setOrderSubTotal(totalBill);
				response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
				logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, ApiUtils.toJsonString(response));
				if (response.getStatus()) {
					String successMessage = "Your order is confirmed successfully. Order will be delivered on "
							+ ApiUtils.getDateAsString(request.getDeliveryTimestamp()) + " between "
							+ getTimeSlotInterval(request.getTimeslotId())
							+ " . Thank you for shopping with us. Team Maidin.";
					SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage);
				}
			}
		}
		return response;
	}

	private void validateCheckout(CheckoutRequest request, Locale locale) throws ValidationException {
		if (prop.getProperty(ResourceProperties.IS_CHECKOUT_BLOCKED).equalsIgnoreCase("ON")) {
			List<BlockedSlotDTO> blockedSlotList = blockedSlotDAO
					.getByBlockedDate(new Date(request.getDeliveryTimestamp()));
			for (BlockedSlotDTO blockedSlotDTO : blockedSlotList) {
				String timeSlotIds = blockedSlotDTO.getTimeSlotIds();
				String[] timeSlotStrArr = timeSlotIds.split(",");
				int[] timeSlotIntArr = Stream.of(timeSlotStrArr).mapToInt(Integer::parseInt).toArray();
				List<Integer> timeSlotIdList = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
				if (timeSlotIdList.contains(request.getTimeslotId())) {
					List<String> timeSlotDTOList = timeSlotDAO.getByIds(timeSlotIdList);
					String timeSlotsCommaSeperated = timeSlotDTOList.stream().map(String::toLowerCase)
							.collect(Collectors.joining(", "));

					String message = timeSlotsCommaSeperated + " slots are not available for "
							+ ApiUtils.dateToFormattedString(blockedSlotDTO.getDateFormatter(),
									blockedSlotDTO.getBlockedDate(), locale)
							+ ".";
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(message,
							ExceptionCode.SLOT_NOT_AVAILABLE, locale));
				}

			}

		}

	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public CheckoutResponse
	 * checkout_Bakup(CheckoutRequest request, String authToken, Locale locale)
	 * throws Exception { logger.info(ApiUtils.REQUEST_PAYLOAD,
	 * ApiUtils.toJsonString(request)); CheckoutResponse response = new
	 * CheckoutResponse(); //
	 * checkSameDateValidation(request.getDeliveryTimestamp(), locale); String
	 * mobile =
	 * authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
	 * CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
	 * logger.info("customerId: " + customerDTO.getId(), "authToken :" +
	 * authToken);
	 * 
	 * Iterator<ConfirmOrderDetails> confirmListIterator =
	 * request.getConfirmOrderList().iterator(); if
	 * (request.getConfirmOrderList() != null &&
	 * request.getConfirmOrderList().size() > 0) { Long orderId =
	 * addOrderInfo(customerDTO.getId(), request); float totalBill = 0; while
	 * (confirmListIterator.hasNext()) { ConfirmOrderDetails confirmOrder =
	 * confirmListIterator.next();
	 * confirmOrder.setDeliveryTimestamp(request.getDeliveryTimestamp());
	 * confirmOrder.setTimeSlotId(request.getTimeslotId()); Long orderDetailsId
	 * = addOrderDetailsForCheckout(orderId, confirmOrder, locale);
	 * System.out.println("orderDetailsId: " + orderDetailsId); totalBill +=
	 * orderDetailsInfoDAO.getById(orderDetailsId).getProductMRP(); }
	 * OrderInfoDTO orderDTO = orderInfoDAO.getById(orderId);
	 * orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
	 * orderDTO.setOrderSubTotal(totalBill);
	 * 
	 * response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.CUSTOMER_SERVICES); logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response));
	 * 
	 * if (response.getStatus()) { String successMessage =
	 * "Your order is confirmed successfully. Order will be delivered on " +
	 * getDateAsString(request.getDeliveryTimestamp()) + " between " +
	 * getTimeSlotInterval(request.getTimeslotId()) +
	 * " . Thank you for shopping with us. Team Maidin.";
	 * SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage); } } return
	 * response; }
	 */
	// changed void function to return double
	// By pushpinder
	private void addOrderDetailsInfo(long orderId, ConfirmOrderDetails request, Locale locale)
			throws ValidationException {
		OrderDetailsInfoDTO orderDTO = new OrderDetailsInfoDTO();
		orderDTO.setOrderId(orderId);
		orderDTO.setProductId(request.getProductId());
		orderDTO.setQuantity(request.getQuantity());
		/*
		 * ProductDTO productDTO =
		 * productDAO.getByProductId(request.getProductId()); if (productDTO !=
		 * null) { orderDTO.setProductMRP(productDTO.getPrice()); }
		 */
		ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(request.getProductId());
		if (productCityInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		ProductInfoV2DTO prInfoV2DTO = productInfoV2DAO.getByIdWithoutActiveState(productCityInfoV2DTO.getProductId());
		if (prInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		orderDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setCancelled(false);
		orderDTO.setOrderCancellationTime(null);
		//orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDTO);
		// return productCityInfoV2DTO.getPrice();
		// return null;
	}

	private void deductBalance(Long orderDetailsId, Long customerId) {

		float hasMaidinBalance = checkMaidinBalance(customerId);
		CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
		transactionRequest.setCustomerId(customerId);
		// transactionRequest.setOrderId(value);
		transactionRequest.setOrderDetailsId(orderDetailsId);
		transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsId);
		OrderInfoDTO orderInfo = orderInfoDAO.getById(detailsInfoDTO.getOrderId());
		if (hasMaidinBalance > 0) {
			if (hasMaidinBalance >= detailsInfoDTO.getTotalProductCost()) {
				transactionRequest.setMoneyOut(detailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				addCustomerTransaction(transactionRequest, -1);
				updateWalletBalance(orderInfo.getCustomerId(), detailsInfoDTO.getTotalProductCost(),
						WalletDescription.DEDUCTIONS.getName());
				orderInfo.setPaymentModeId(PaymentMode.ByWallet.getId());
				detailsInfoDTO.setAmountPaidByCustomer(detailsInfoDTO.getTotalProductCost());
				detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
			} else if (hasMaidinBalance < detailsInfoDTO.getTotalProductCost()) {
				transactionRequest.setMoneyOut(hasMaidinBalance);
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				addCustomerTransaction(transactionRequest, -1);
				updateWalletBalance(orderInfo.getCustomerId(), hasMaidinBalance,
						WalletDescription.DEDUCTIONS.getName());
				orderInfo.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
				detailsInfoDTO.setAmountPaidByCustomer(hasMaidinBalance);
				detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
			}
		}

	}

	private void addCustomerTransaction(CustomerTransactionRequest transactionRequest, long adminId) {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(transactionRequest.getCustomerId());
		transactionDTO.setOrderId(transactionRequest.getOrderId());
		transactionDTO.setOrderDetailsId(transactionRequest.getOrderDetailsId());
		transactionDTO.setMoneyIn(transactionRequest.getMoneyIn());
		transactionDTO.setMoneyOut(transactionRequest.getMoneyOut());
		transactionDTO.setPaymentModeId(transactionRequest.getPaymentModeId());
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(transactionRequest.getTransactionType());
		transactionDTO.setRemarks(transactionRequest.getRemarkStatus());
		transactionDTO.setUpdatedBy(adminId);
		transactionDAO.addTransaction(transactionDTO);

	}

	private float checkMaidinBalance(Long customerId) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			return walletDTO.getMyWalletBalance();
		}
		return 0;
	}

	private void updateWalletBalance(Long customerId, float totalProductCost, String rechargeType) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		// float maidinBalance = 0;
		if (walletDTO != null) {
			walletDTO.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + totalProductCost);
			}
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			// maidinBalance = walletDTO.getMyWalletBalance();
		} else {
			WalletDTO walletDto = new WalletDTO();
			walletDto.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDto.setMyWalletBalance(totalProductCost);
			}
			walletDto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.addBalance(walletDto);
			// maidinBalance = checkMaidinBalance(customerId) ;
		}

	}

	private void reshufflePayment(Long customerId, float maidinBalance) {

		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledPaymentOrders(customerId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				// OrderInfoDTO orderInfoDTO =
				// orderInfoDAO.getById(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				float amountToPay = orderDetailsDTO.getTotalProductCost() - orderDetailsDTO.getAmountPaidByCustomer();
				if (maidinBalance > 0 && amountToPay > 0) {
					if (maidinBalance >= amountToPay) {
						transactionRequest.setMoneyOut(amountToPay);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + amountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, amountToPay, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = maidinBalance - amountToPay;
						/*
						 * if(orderDetailsInfoDAO.getUnSettledPaymentOrders(
						 * customerId).size() == 0){
						 * orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.
						 * getId()); }
						 */
						// orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					} else {
						transactionRequest.setMoneyOut(maidinBalance);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + maidinBalance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, maidinBalance, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = 0;
						// orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());

					}
				}
				setPaymentId(orderDetailsDTO.getOrderId());
			}
			transactionRequest = null;
		}
	}

	private void setPaymentId(Long orderId) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getByOrderId(orderId);
		if (orderDetailsList != null && orderDetailsList.size() > 0) {
			boolean paymentStatus = samePaymentStatus(orderDetailsList);
			if (!paymentStatus) {
				orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			} else {
				if (orderDetailsList.get(0).getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
					orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				} else if (orderDetailsList.get(0).getPaymentStatus().equals(PaymentStatus.PENDING.getName())) {
					orderInfoDTO.setPaymentModeId(PaymentMode.ByCash.getId());
					orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}

			}
		}
	}

	private boolean samePaymentStatus(List<OrderDetailsInfoDTO> orderDetailsList) {
		for (int i = 0; i < orderDetailsList.size(); i++) {
			if (orderDetailsList.get(0).getPaymentStatus().equals(orderDetailsList.get(i).getPaymentStatus())) {
				return false;
			}
		}
		return true;
	}

	private Long addOrderDetailsForCheckout(long orderId, ConfirmOrderDetails request, Locale locale)
			throws ValidationException {
		String productName = "";

		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId(request.getProductId());
		orderDetailsDTO.setQuantity(request.getQuantity());

		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO.getByIdWithoutActiveStatus(request.getProductId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null) {
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
				}
				if (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus() || !productCityInfoDTO.isActive()
						|| !productCityInfoDTO.isStockStatus()) {
					if (productCityInfoDTO.getQuantityUnit() != null) {
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}

					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
							ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));

				}
			}

			orderDetailsDTO.setProductName(productInfoV2DTO.getProductName());
			orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
			orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
			orderDetailsDTO.setProductSalePrice(productCityInfoDTO.getSalePrice());
			orderDetailsDTO
					.setTotalProductCost((productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
							* request.getQuantity());
			orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() - productCityInfoDTO.getSalePrice());
			orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDetailsDTO.setCancelled(false);
		//orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);

	}

	private String getTimeSlotInterval(int timeSlotId) {
		String result = "";
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(timeSlotId);
		if (timeSlotDTO != null) {
			result = timeSlotDTO.getTimeSlot();
		}
		return result;
	}

	private Long addOrderInfo(Long customerId, CheckoutRequest request) {

		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(customerId);
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(new Date(request.getDeliveryTimestamp()));
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeslotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		//orderDTO.setPaymentModeId(request.getPaymentCode());
		orderDTO.setRequestedPaymentModeId(request.getPaymentCode());
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setCancelled(false);
		CustomerDTO customerDTO = customerDAO.getById(customerId);
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		orderDTO.setAddressId(addressDTO.getId());
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponse getOrders(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		GetOrderResponse response = new GetOrderResponse();
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		response = getAllOrders(customerDTO.getId(), locale);
		// response.
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponseV1 getV1Orders(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		GetOrderResponseV1 response = new GetOrderResponseV1();
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		response = getV1Orders(customerDTO.getId());
		// response.
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponseV2 getV2UpcomingOrders(String authToken, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		GetOrderResponseV2 response = new GetOrderResponseV2();
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);

		response = getV2Orders(customerDTO.getId());
		// response.
		// if(response.)
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*
	 * private GetOrderResponseV2 getV2Orders(Long customerId) {
	 * GetOrderResponseV2 response = new GetOrderResponseV2();
	 * 
	 * List<Date> pastOrderDateList =
	 * orderInfoDAO.getPastOrderDates(customerId); if (pastOrderDateList != null
	 * & pastOrderDateList.size() > 0) { for (Date orderdate :
	 * pastOrderDateList) { System.out.println("pastCheckoutDay " + orderdate);
	 * 
	 * OrderDetailsV2 orderDetailsV2 = new OrderDetailsV2();
	 * orderDetailsV2.setTimestamp(orderdate.getTime()); List<Integer>
	 * timeSlotList =
	 * orderInfoDAO.getCheckoutTimeSlotByCustomerDeliveryDate(customerId,
	 * orderdate);
	 * 
	 * for (int timeSlotId : timeSlotList) { TimeSlotWiseV2 timeSlotV2 = new
	 * TimeSlotWiseV2(); timeSlotV2.setTimeslotId(timeSlotId);
	 * timeSlotV2.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
	 * timeSlotV2 = checkoutMethod(customerId, timeSlotId, orderdate,
	 * timeSlotV2); if(timeSlotV2 == null || timeSlotV2.getOrderList().size() ==
	 * 0 ){ continue; } orderDetailsV2.getTimeslotList().add(timeSlotV2); }
	 * 
	 * response.getOrderDetailsV2().add(orderDetailsV2);
	 * 
	 * } }
	 * 
	 * 
	 * List<Long> todaysCheckoutList =
	 * orderInfoDAO.getCheckoutIdsOfUpcomingCurrentDayTimeSlots(customerId,
	 * lastLockingPeriod);
	 * 
	 * if ((todaysCheckoutList != null & todaysCheckoutList.size() > 0)) {
	 * OrderDetailsV2 orderDetailsV2 = new OrderDetailsV2();
	 * orderDetailsV2.setTimestamp(new
	 * Timestamp(System.currentTimeMillis()).getTime()); if (todaysCheckoutList
	 * != null && !todaysCheckoutList.isEmpty() && todaysCheckoutList.size() !=
	 * 0) { todaysCheckoutList = wrapWithQuotes(todaysCheckoutList); } if
	 * (todaysCheckoutList != null & todaysCheckoutList.size() > 0) { Calendar
	 * rightNow = Calendar.getInstance(); int hour =
	 * rightNow.get(Calendar.HOUR_OF_DAY); List<Integer> timeSlotList =
	 * orderInfoDAO.getCheckoutTimeSlotsByIds(todaysCheckoutList); for (Integer
	 * timeSlotId : timeSlotList) { TimeSlotWiseV2 timeSlotV2 = new
	 * TimeSlotWiseV2(); timeSlotV2.setTimeslotId(timeSlotId); TimeSlotDTO
	 * timeSlotDTO = timeSlotDAO.getById(timeSlotId); if (hour >= timeSlotDTO
	 * .getStartTime() ) {
	 * timeSlotV2.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId()); }
	 * else { if (hour + 1 == timeSlotDTO.getStartTime()) {
	 * timeSlotV2.setDeliveryStatus(OrderStatus.READY_FOR_DELIVERY.getId()); }
	 * else { timeSlotV2.setDeliveryStatus(OrderStatus.CONFIRMED.getId()); } }
	 * timeSlotV2 = checkoutMethod(customerId, timeSlotId, new
	 * Date(System.currentTimeMillis()), timeSlotV2); if(timeSlotV2 == null ||
	 * timeSlotV2.getOrderList().size() == 0 ){ continue; }
	 * orderDetailsV2.getTimeslotList().add(timeSlotV2); } }
	 * response.getOrderDetailsV2().add(orderDetailsV2); } boolean checkout =
	 * false; List<Date> orderDateList =
	 * orderInfoDAO.getNextOrderDate(customerId); if (orderDateList != null &
	 * orderDateList.size() > 0) { for (Date orderdate : orderDateList) {
	 * checkout = true; System.out.println("nextCheckoutDay " + orderdate); if
	 * (checkout) { OrderDetailsV2 orderDetailsV2 = new OrderDetailsV2();
	 * orderDetailsV2.setTimestamp(orderdate.getTime()); System.out.println(
	 * "Customer has checkout only in coming days"); List<Integer> timeSlotList
	 * = orderInfoDAO.getCheckoutTimeSlotByCustomerDeliveryDate(customerId,
	 * orderdate);
	 * 
	 * for (int timeSlotId : timeSlotList) { TimeSlotWiseV2 timeSlotV2 = new
	 * TimeSlotWiseV2(); timeSlotV2.setTimeslotId(timeSlotId);
	 * timeSlotV2.setDeliveryStatus(OrderStatus.CONFIRMED.getId()); timeSlotV2 =
	 * checkoutMethod(customerId, timeSlotId, orderdate, timeSlotV2);
	 * if(timeSlotV2 == null || timeSlotV2.getOrderList().size() == 0 ){
	 * continue; } orderDetailsV2.getTimeslotList().add(timeSlotV2); }
	 * 
	 * response.getOrderDetailsV2().add(orderDetailsV2); } } }
	 * 
	 * if (response.getOrderDetailsV2() == null) {
	 * response.getOrderDetailsV2().clear();
	 * 
	 * } return response; }
	 */

	private GetOrderResponseV2 getV2Orders(Long customerId) {
		GetOrderResponseV2 response = new GetOrderResponseV2();
		// List<OrderInfoDTO> orderInfoList =
		// orderInfoDAO.getOrders(customerId);
		List<UpcomingOrderDTO> orderDetailsList = orderInfoDAO.getOrderDetails(customerId);
		List<Date> deliveryDateList = orderDetailsList.stream().map(UpcomingOrderDTO::getDeliveryDate).distinct()
				.collect(Collectors.toList());
		List<TimeSlotDTO> timeSlotDTOList = timeSlotDAO.getAll();
		for (Date deliveryDate : deliveryDateList) {
			OrderDetailsV2 orderDetailsV2 = new OrderDetailsV2();
			orderDetailsV2.setTimestamp(deliveryDate.getTime());
			Calendar rightNow = Calendar.getInstance();
			int hour = rightNow.get(Calendar.HOUR_OF_DAY);
			List<Integer> timeSlotList = orderDetailsList.stream().filter(e -> e.getDeliveryDate().equals(deliveryDate))
					.map(UpcomingOrderDTO::getTimeSlotId).distinct().collect(Collectors.toList());
			for (Integer timeSlotId : timeSlotList) {
				TimeSlotWiseV2 timeSlot = new TimeSlotWiseV2();
				timeSlot.setTimeslotId(timeSlotId);
				if (commonApplication.getZeroTimeDate(deliveryDate)
						.before(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
					timeSlot.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
				} else if (commonApplication.getZeroTimeDate(deliveryDate)
						.equals(commonApplication.getZeroTimeDate(new Date(System.currentTimeMillis())))) {
					TimeSlotDTO timeSlotDTO = timeSlotDTOList.stream().filter(e -> e.getId() == timeSlotId)
							.collect(Collectors.toList()).get(0);
					if (hour >= timeSlotDTO.getStartTime()) {
						timeSlot.setDeliveryStatus(OrderStatus.OUT_FOR_DELIVERY.getId());
					} else {
						if (hour + 1 == timeSlotDTO.getStartTime()) {
							timeSlot.setDeliveryStatus(OrderStatus.READY_FOR_DELIVERY.getId());
						} else {
							timeSlot.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
						}
					}
				} else {
					timeSlot.setDeliveryStatus(OrderStatus.CONFIRMED.getId());
				}
				timeSlot = checkoutMethodV2(customerId, timeSlotId, deliveryDate, timeSlot, orderDetailsList);
				orderDetailsV2.getTimeslotList().add(timeSlot);
			}
			response.getOrderDetailsV2().add(orderDetailsV2);
		}

		return response;
	}

	private TimeSlotWiseV2 checkoutMethodV2(Long customerId, Integer timeSlotId, Date deliveryDate,
			TimeSlotWiseV2 timeSlot, List<UpcomingOrderDTO> orderDetailsList) {
		orderDetailsList = orderDetailsList.stream()
				.filter(e -> e.getDeliveryDate().equals(deliveryDate) && e.getTimeSlotId() == timeSlotId).distinct()
				.collect(Collectors.toList());
		for (int i = 0; i < orderDetailsList.size(); i++) {
			OrderInfoV2 orderDetailsInfo = new OrderInfoV2();
			orderDetailsInfo.setOrderId(orderDetailsList.get(i).getOrderId());
			orderDetailsInfo.setOrderDetailsId(orderDetailsList.get(i).getOrderDetailsId());
			orderDetailsInfo.setProductId((int) orderDetailsList.get(i).getProductDetailsId());
			orderDetailsInfo.setProductName(
					orderDetailsList.get(i).getBrandName() + " " + orderDetailsList.get(i).getProductName());
			orderDetailsInfo.setProductUnit(orderDetailsList.get(i).getProductUnit());
			// orderDetailsInfo.setBrandId(orderDetailsList.get(i).getSubCategoryId());
			orderDetailsInfo.setQuantity(orderDetailsList.get(i).getQuantity());
			orderDetailsInfo.setProductCost(orderDetailsList.get(i).getSalePrice() > 0
					? (orderDetailsList.get(i).getSalePrice() + orderDetailsList.get(i).getDeliveryCharge())
					: (orderDetailsList.get(i).getProductSalePrice()
							+ orderDetailsList.get(i).getProductDeliveryCharge()));
			if (orderDetailsList.get(i).getCategoryId() == 9) {
				timeSlot.setCategoryId(9);
			}
			timeSlot.getOrderList().add(orderDetailsInfo);
		}

		return timeSlot;
	}

	private TimeSlotWiseV2 checkoutMethod(Long customerId, Integer timeSlotId, Date dateValue,
			TimeSlotWiseV2 timeSlotV2) {
		List<OrderInfoDTO> orderInfoDTOList = orderInfoDAO.getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId(customerId,
				dateValue, timeSlotId);
		for (OrderInfoDTO orderInfo : orderInfoDTOList) {
			List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO.getByOrderId(orderInfo.getId());
			for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
				OrderInfoV2 orderInfoV2 = new OrderInfoV2();
				orderInfoV2.setOrderId(orderDetailsInfoDTO.getOrderId());
				orderInfoV2.setOrderDetailsId(orderDetailsInfoDTO.getId());
				orderInfoV2.setProductId(orderDetailsInfoDTO.getProductId());
				ProductCityInfoV2DTO productCityDTO = productCityInfoDAO
						.getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId());
				if (productCityDTO == null) {
					continue;
				}
				ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
						.getByIdWithoutActiveState(productCityDTO.getProductId());
				if (productInfoV2DTO == null) {
					continue;
				}
				BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoDTO != null) {
					orderInfoV2.setProductName(brandInfoDTO.getBrand() + " " + productInfoV2DTO.getProductName());
				} else {
					orderInfoV2.setProductName(productInfoV2DTO.getProductName());
				}
				orderInfoV2.setProductUnit(productCityDTO.getQuantityUnit());

				orderInfoV2.setBrandId(productInfoV2DTO.getBrandId());
				orderInfoV2.setQuantity(orderDetailsInfoDTO.getQuantity());
				orderInfoV2.setProductCost(
						orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges());
				if (productCityDTO.getCategoryId() == 9) {
					timeSlotV2.setCategoryId(9);
				}
				//////////////////////////////////////////////////////////////////////////
				timeSlotV2.getOrderList().add(orderInfoV2);

			}

		}
		return timeSlotV2;

	}

	private Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}

	private GetOrderResponseV1 getV1Orders(Long customerId) {

		GetOrderResponseV1 response = new GetOrderResponseV1();
		List<UpcomingOrderDTO> orderDetailsList = orderInfoDAO.getAllOrderDetails(customerId);
		List<Date> deliveryDateList = orderDetailsList.stream().map(UpcomingOrderDTO::getDeliveryDate).distinct()
				.collect(Collectors.toList());
		for (Date expectedDeliveryDate : deliveryDateList) {

			OrderDetailsV1 orderDetails = new OrderDetailsV1();
			orderDetails.setTimestamp(expectedDeliveryDate.getTime());
			List<Long> nonVegetableOrderList = orderDetailsList.stream()
					.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() != 9)
					.map(UpcomingOrderDTO::getOrderId).distinct().collect(Collectors.toList());

			for (Long orderId : nonVegetableOrderList) {
				TimeSlotWiseV1 timeSloWise = new TimeSlotWiseV1();
				List<UpcomingOrderDTO> orderDetailsInfoList = orderDetailsList.stream()
						.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() != 9
								&& e.getOrderId() == orderId)
						.distinct().collect(Collectors.toList());

				for (UpcomingOrderDTO orderDetailsInfoDTO : orderDetailsInfoList) {
					OrderInfoV1 details = new OrderInfoV1();

					if (orderDetailsInfoDTO != null) {
						details.setOrderId(orderDetailsInfoDTO.getOrderDetailsId());
						details.setProductId((int) orderDetailsInfoDTO.getProductDetailsId());
						details.setQuantity(orderDetailsInfoDTO.getQuantity());
						details.setProductName(
								orderDetailsInfoDTO.getBrandName() + " " + orderDetailsInfoDTO.getProductName());
						details.setProductUnit(orderDetailsInfoDTO.getProductUnit());
						details.setProductCost(orderDetailsInfoDTO.getSalePrice() > 0
								? (orderDetailsInfoDTO.getSalePrice() + orderDetailsInfoDTO.getDeliveryCharge())
								: (orderDetailsInfoDTO.getProductSalePrice()
										+ orderDetailsInfoDTO.getProductDeliveryCharge()));
						timeSloWise.getOrderList().add(details);
						timeSloWise.setTimeslotId(orderDetailsInfoDTO.getTimeSlotId());
					}
				}
				orderDetails.getTimeslotList().add(timeSloWise);

			}

			//////////////////////////////////////////////////////////////////////////////////
			List<Long> vegetableOrderList = orderDetailsList.stream()
					.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() == 9)
					.map(UpcomingOrderDTO::getOrderId).distinct().collect(Collectors.toList());

			for (Long orderId : vegetableOrderList) {
				TimeSlotWiseV1 timeSloWise = new TimeSlotWiseV1();
				List<UpcomingOrderDTO> orderDetailsInfoList = orderDetailsList.stream()
						.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() == 9
								&& e.getOrderId() == orderId)
						.distinct().collect(Collectors.toList());

				for (UpcomingOrderDTO orderDetailsInfoDTO : orderDetailsInfoList) {
					OrderInfoV1 details = new OrderInfoV1();
					details.setOrderId(orderDetailsInfoDTO.getOrderId());
					details.setProductId((int) orderDetailsInfoDTO.getProductDetailsId());
					details.setQuantity(orderDetailsInfoDTO.getQuantity());
					details.setProductName(
							orderDetailsInfoDTO.getBrandName() + " " + orderDetailsInfoDTO.getProductName());
					details.setProductUnit(orderDetailsInfoDTO.getProductUnit());
					details.setProductCost(orderDetailsInfoDTO.getSalePrice() > 0
							? (orderDetailsInfoDTO.getSalePrice() + orderDetailsInfoDTO.getDeliveryCharge())
							: (orderDetailsInfoDTO.getProductSalePrice()
									+ orderDetailsInfoDTO.getProductDeliveryCharge()));
					timeSloWise.getOrderList().add(details);
					timeSloWise.setTimeslotId(orderDetailsInfoDTO.getTimeSlotId());
				}
				if (timeSloWise.getOrderList().size() > 0) {
					orderDetails.getTimeslotList().add(timeSloWise);
				}

			}

			if (orderDetails.getTimeslotList().size() > 0) {
				response.getOrderDetailsList().add(orderDetails);
			}

		}
		return response;

	}

	/*
	 * private GetOrderResponseV1 getV1Orders(Long customerId) {
	 * 
	 * GetOrderResponseV1 response = new GetOrderResponseV1();
	 * 
	 * List<Date> orderDateList =
	 * orderInfoDAO.getDifferentDeliveryDateByCustomerId(customerId); Calendar
	 * todayCalendar = Calendar.getInstance(); for (Date expectedDeliveryDate :
	 * orderDateList) {
	 * 
	 * Calendar deliveryDateCalendar = Calendar.getInstance(); Calendar
	 * tomorrowCalendar = Calendar.getInstance();
	 * tomorrowCalendar.add(Calendar.DAY_OF_YEAR, 1);
	 * deliveryDateCalendar.setTime(expectedDeliveryDate);
	 * todayCalendar.setTime(new
	 * Date((Calendar.getInstance().getTime()).getTime())); boolean sameDay =
	 * deliveryDateCalendar.get(Calendar.YEAR) ==
	 * todayCalendar.get(Calendar.YEAR) &&
	 * deliveryDateCalendar.get(Calendar.DAY_OF_YEAR) ==
	 * todayCalendar.get(Calendar.DAY_OF_YEAR);
	 * 
	 * OrderDetailsV1 orderDetails = new OrderDetailsV1();
	 * orderDetails.setTimestamp(expectedDeliveryDate.getTime());
	 * 
	 * List<OrderInfoDTO> orderDTONoVegetableList = orderInfoDAO
	 * .getByCustomerIdDeliveryDateWithOtherCategory(customerId,
	 * expectedDeliveryDate, 9);
	 * 
	 * for (OrderInfoDTO orderInfoDTO : orderDTONoVegetableList) {
	 * 
	 * TimeSlotDTO timeSlotDTO =
	 * timeSlotDAO.getById(orderInfoDTO.getTimeSlotId()); if (sameDay &&
	 * (todayCalendar.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getEndTime())))
	 * { continue; } TimeSlotWiseV1 timeSloWise = new TimeSlotWiseV1();
	 * List<OrderDetailsInfoDTO> orderDetailsInfoList =
	 * orderDetailsInfoDAO.getByOrderId(orderInfoDTO.getId()); if
	 * (orderDetailsInfoList.size() == 0) { continue; } for (OrderDetailsInfoDTO
	 * orderDetailsInfoDTO : orderDetailsInfoList) {
	 * System.out.println(orderDetailsInfoDTO.getProductId()); OrderInfoV1
	 * details = new OrderInfoV1();
	 * 
	 * if (orderDetailsInfoDTO != null) {
	 * details.setOrderId(orderDetailsInfoDTO.getId());
	 * details.setProductId(orderDetailsInfoDTO.getProductId());
	 * details.setQuantity(orderDetailsInfoDTO.getQuantity());
	 * ProductCityInfoV2DTO productCityDTO =
	 * productCityInfoDAO.getByIdWithoutActiveStatus(orderDetailsInfoDTO.
	 * getProductId()); if(productCityDTO == null){ continue; } ProductInfoV2DTO
	 * productInfoV2DTO =
	 * productInfoV2DAO.getByIdWithoutActiveState(productCityDTO.getProductId())
	 * ; if(productInfoV2DTO == null){ continue; } BrandInfoV2DTO brandInfoDTO =
	 * brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
	 * if(brandInfoDTO!= null){ details.setProductName(brandInfoDTO.getBrand()+
	 * " "+productInfoV2DTO.getProductName()); } else{
	 * details.setProductName(productInfoV2DTO.getProductName()); }
	 * details.setProductUnit(productCityDTO.getQuantityUnit());
	 * details.setProductCost(orderDetailsInfoDTO.getProductSalePrice() > 0 ?
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * orderDetailsInfoDTO.getDeliveryCharges()):
	 * (productCityDTO.getSalePrice()+productCityDTO.getDeliveryCharge()));
	 * timeSloWise.getOrderList().add(details);
	 * 
	 * }
	 * 
	 * } timeSloWise.setTimeslotId(timeSlotDTO.getId());
	 * 
	 * orderDetails.getTimeslotList().add(timeSloWise);
	 * 
	 * }
	 * 
	 * /////////////////////////////////////////////////////////////////////////
	 * /////////
	 * 
	 * List<OrderInfoDTO> orderDTOList =
	 * orderInfoDAO.getByCustomerIdDeliveryDateWithVegetableCategory(customerId,
	 * expectedDeliveryDate, 9); for (OrderInfoDTO orderInfoDTO : orderDTOList)
	 * { TimeSlotDTO timeslotDTO =
	 * timeSlotDAO.getById(orderInfoDTO.getTimeSlotId()); if (sameDay &&
	 * (todayCalendar.get(Calendar.HOUR_OF_DAY) >= (timeslotDTO.getEndTime())))
	 * { continue; } TimeSlotWiseV1 timeSloWise = new TimeSlotWiseV1();
	 * 
	 * List<OrderDetailsInfoDTO> orderDetailsInfoList =
	 * orderDetailsInfoDAO.getByOrderId(orderInfoDTO.getId()); if
	 * (orderDetailsInfoList.size() == 0) { continue; } for (OrderDetailsInfoDTO
	 * orderDetailsInfoDTO : orderDetailsInfoList) {
	 * System.out.println(orderDetailsInfoDTO.getProductId()); OrderInfoV1
	 * details = new OrderInfoV1();
	 * 
	 * if (orderDetailsInfoDTO != null) {
	 * details.setOrderId(orderDetailsInfoDTO.getId());
	 * details.setProductId(orderDetailsInfoDTO.getProductId());
	 * details.setQuantity(orderDetailsInfoDTO.getQuantity());
	 * ProductCityInfoV2DTO productCityDTO =
	 * productCityInfoDAO.getByIdWithoutActiveStatus(orderDetailsInfoDTO.
	 * getProductId()); if(productCityDTO == null){ continue; } ProductInfoV2DTO
	 * productInfoV2DTO =
	 * productInfoV2DAO.getByIdWithoutActiveState(productCityDTO.getProductId())
	 * ; if(productInfoV2DTO == null){ continue; } BrandInfoV2DTO brandInfoDTO =
	 * brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
	 * if(brandInfoDTO!= null){ details.setProductName(brandInfoDTO.getBrand()+
	 * " "+productInfoV2DTO.getProductName()); } else{
	 * details.setProductName(productInfoV2DTO.getProductName()); }
	 * details.setProductName(productInfoV2DTO.getProductName());
	 * details.setProductCost(orderDetailsInfoDTO.getProductSalePrice() > 0 ?
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * orderDetailsInfoDTO.getDeliveryCharges()):
	 * (productCityDTO.getSalePrice()+productCityDTO.getDeliveryCharge()));
	 * timeSloWise.getOrderList().add(details);
	 * 
	 * }
	 * 
	 * } timeSloWise.setTimeslotId(orderInfoDTO.getTimeSlotId());
	 * timeSloWise.setCategoryId(9);
	 * orderDetails.getTimeslotList().add(timeSloWise);
	 * 
	 * } if (orderDetails.getTimeslotList().size() > 0) {
	 * response.getOrderDetailsList().add(orderDetails); }
	 * 
	 * } return response;
	 * 
	 * }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponse scheduleOrder(String authToken, ScheduleDetails request, Locale locale) throws Exception {
		GetOrderResponse response = new GetOrderResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		
		confirmFutureOrders(customerDTO.getId(), request, locale);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);

		if (response.getStatus()) {
			ProductCityInfoV2DTO productCityDTO = productCityInfoDAO.getByIdWithoutActiveStatus(request.getProductId());
			ProductInfoV2DTO productDTO = productInfoV2DAO.getByIdWithoutActiveState(productCityDTO.getProductId());
			String successMessage = "Your order of " + productDTO.getProductName() + " "
					+ productCityDTO.getQuantityUnit()
					+ " has been scheduled successfully. Thank you for shopping with us. Team Maidin.";
			SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage);
		}

		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse clearOrder(String authToken, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		//logger.info("customerId: " + customerDTO.getId(), "authToken :" + authToken);
		// clearAllBalanceIfDeducted(customerDTO.getId());
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId());
		float amountToRecharge = clearOrder(customerDTO.getId());
		if (amountToRecharge > 0) {
			float myBalance = checkMaidinBalance(customerDTO.getId());
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerDTO.getId());
			transactionRequest.setMoneyIn(amountToRecharge);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(myBalance < 0 ? Math.abs(myBalance) : 0);
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setTransactionType(WalletDescription.REFUND_ON_CANCELLATION.getId());
			transactionRequest.setRemarkStatus(0);
			// transactionRequest.setRemarks(request.getRemarks());
			addCustomerTransaction(transactionRequest, -1);
			updateWalletBalance(customerDTO.getId(), amountToRecharge, WalletDescription.RECHARGE.getName());
			float walletBalanceAfterClearingOrder = checkMaidinBalance(customerDTO.getId());
			if(walletBalanceAfterClearingOrder > 0){
				reshufflePayment(customerDTO.getId(), walletBalanceAfterClearingOrder, PaymentMode.ByWallet.getId());
			}
			

		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	private void reshufflePayment(Long customerId, float balance, int paymentMode) {
		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());
				float pendingAmountToPay = orderDetailsDTO.getTotalProductCost()
						- orderDetailsDTO.getAmountPaidByCustomer();
				if (balance > 0 && pendingAmountToPay > 0) {
					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					if (balance >= pendingAmountToPay) {
						transactionRequest.setMoneyOut(pendingAmountToPay);
						orderDetailsDTO.setAmountPaidByCustomer(
								orderDetailsDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, pendingAmountToPay, WalletDescription.RECHARGE.getName());
						balance = balance - pendingAmountToPay;
					} else {
						transactionRequest.setMoneyOut(balance);
						orderDetailsDTO.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + balance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(pendingAmountToPay - balance);
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, balance, WalletDescription.RECHARGE.getName());

						balance = 0;
						orderDetailsDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());

					}
				}
				// TODO:change SetPaymentModeid
				setPaymentModeId(orderDetailsDTO.getOrderId());
			}
			transactionRequest = null;
		}
		
	}
	
	private void setPaymentModeId(Long orderId) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		List<Integer> paymentModeIdList = orderDetailsInfoDAO.getByOrderIdAndPaymentModeId(orderId);
		if (paymentModeIdList != null && paymentModeIdList.size() > 0) {
			if (Collections.frequency(paymentModeIdList, paymentModeIdList.get(0)) == paymentModeIdList.size()) {
				orderInfoDTO.setPaymentModeId(paymentModeIdList.get(0));
			} else if (!Collections.disjoint(Arrays.asList(5, 6, 7, 8, 9), paymentModeIdList)) {
				orderInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
			} else if (!Collections.disjoint(Arrays.asList(1, 2, 3), paymentModeIdList)) {
				orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
			}

		}
		orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse scheduleProductV2(String authToken, ScheduleProductRequestV2 request, Locale locale)
			throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(request));
		if (request.getScheduleId() == 0) {
			Long scheduleId = addSchedule(request, customerDTO.getId(), locale);
			if (scheduleId != null) {
				ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleId);
				addOrdersForSchedule("V2", scheduleId, scheduleInfoDTO.getStartDate(), locale);
			}
		} else if (request.getScheduleId() > 0) {
			ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(request.getScheduleId());
			if (scheduleDTO != null) {
				if (scheduleDTO.getCustomerId().equals(customerDTO.getId())) {
					if (request.getScheduleStatus() == ScheduleStatus.REMOVE.getId()
							&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
						System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
						deleteSchedule(scheduleDTO);
						System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
					} else if (request.getScheduleStatus() == ScheduleStatus.PAUSENEXT.getId()
							&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
						System.out
								.println("Going to pause in  Upsert...: " + new Timestamp(System.currentTimeMillis()));
						pauseNextDelivery(scheduleDTO);
						System.out.println("Pausing done in Upsert: " + new Timestamp(System.currentTimeMillis()));
					} else {
						boolean addOrderRequired = false;
						/*
						 * if(request.getScheduleStatus() !=
						 * scheduleDTO.getScheduleStatus()){
						 * if(request.getScheduleStatus() ==
						 * ScheduleStatus.RESUME.getId()){ addOrderRequired =
						 * true; }
						 * 
						 * } else{
						 * 
						 * }
						 */
						updateSchedule(request, scheduleDTO);
						updateOrders(scheduleDTO, request, addOrderRequired, locale);

					}
				}
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);

		if (request.getScheduleId() == 0) {
			if (response.getStatus()) {
				ProductCityInfoV2DTO productCityDTO = productCityInfoDAO
						.getByIdWithoutActiveStatus(request.getProductId());
				ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
						.getByIdWithoutActiveState(productCityDTO.getProductId());
				String successMessage = "Your order of " + productInfoV2DTO.getProductName() + " "
						+ productCityDTO.getQuantityUnit()
						+ " has been scheduled successfully. Thank you for shopping with us. Team Maidin.";
				SmsSender.sendCheckoutOrderSuccessSMS(mobile, successMessage);
			}
		}
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, customerDTO.getId(), ApiUtils.toJsonString(response));
		if (response.getStatus()) {
			if (request.getScheduleStatus() == ScheduleStatus.REMOVE.getId()) {
				String cancelMessage = "Dear Customer, We have cancelled your order as per your request. "
						+ "Please get in touch with us on 9990166644 in case of any queries. Team Maidin.";
				SmsSender.sendCheckoutOrderSuccessSMS(mobile, cancelMessage);
			}
		}
		return response;
	}

	private void addOrdersForSchedule(String version, Long scheduleId, Date scheduledFrom, Locale locale)
			throws ValidationException {

		System.out.println("New Schedule");
		ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(scheduleId);
		if (scheduleDTO != null) {
			Calendar cal = Calendar.getInstance();
			if (scheduleDTO.getScheduledUpto() != null) {
				cal.setTime(scheduledFrom);
			} else {
				cal.setTime(scheduleDTO.getStartDate());
			}
			if (getZeroTimeDate(new Date(cal.getTime().getTime()))
					.before(getZeroTimeDate(new Date(System.currentTimeMillis())))
					|| getZeroTimeDate(new Date(cal.getTime().getTime()))
							.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				cal.setTimeInMillis(System.currentTimeMillis());
				TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
				if (cal.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime())) {
					cal.add(Calendar.DATE, 1);
				}

			}

			Date monthStartDate = new Date(cal.getTimeInMillis());
			System.out.println(monthStartDate);
			long monthEndTime = 0;

			if (newScheduleEndDate == 0 || version == "V1") {
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MONTH, 1);
				calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				monthEndTime = calendar.getTimeInMillis();
			} else {
				monthEndTime = newScheduleEndDate;
			}
			Date monthEndDate = new Date(monthEndTime);
			System.out.println(monthStartDate);
			System.out.println(monthEndDate);
			if ((scheduleDTO.getScheduledUpto() != null && ((getZeroTimeDate(scheduleDTO.getScheduledUpto())
					.after(getZeroTimeDate(monthEndDate)))
					|| (getZeroTimeDate(scheduleDTO.getScheduledUpto()).equals(getZeroTimeDate(monthEndDate)))))) {
				return;
			}
			if (getZeroTimeDate(monthStartDate).after(getZeroTimeDate(monthEndDate))) {
				return;
			}
			scheduleDTO.setScheduledUpto(monthEndDate);
			List<Date> dateList = getDaysBetweenDates(monthStartDate, monthEndDate);
			for (Date deliveryDate : dateList) {

				Calendar c = toCalendar(deliveryDate);
				System.out.println(deliveryDate);
				String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
						"Saturday" };
				System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
				if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") && scheduleDTO.isSu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") && scheduleDTO.isMo()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") && scheduleDTO.isTu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") && scheduleDTO.isWe()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") && scheduleDTO.isTh()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") && scheduleDTO.isFr()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") && scheduleDTO.isSa()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				}
			}

		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse scheduleOrderV2ForExistingSchedule(Long scheduleId, Locale locale) throws Exception {

		GenericResponse response = new GenericResponse();
		System.out.println("Existing Schedule");
		// Calendar c = Calendar.getInstance();
		ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(scheduleId);
		if (scheduleDTO != null) {
			Calendar startDate = Calendar.getInstance();
			System.out.println("The original Date is : " + startDate.getTime());
			////// month start Date///////
			startDate.setTime(scheduleDTO.getScheduledUpto());
			startDate.add(Calendar.DATE, 1);
			Date monthStartDate = new Date(startDate.getTimeInMillis());
			System.out.println(monthStartDate);
			////// month start Date///////
			////// month end Date///////
			startDate.add(Calendar.MONTH, 1);
			startDate.set(Calendar.DATE, startDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date monthEndDate = new Date(startDate.getTimeInMillis());
			System.out.println(monthEndDate);
			////// month end Date///////
			boolean scheduleRequired = false;
			if ((scheduleDTO.getScheduledUpto() != null
					&& getZeroTimeDate(scheduleDTO.getScheduledUpto()).before(getZeroTimeDate(monthEndDate)))) {
				scheduleDTO.setScheduledUpto(monthEndDate);
				scheduleRequired = true;
			}
			if (scheduleRequired) {
				List<Date> dateList = getDaysBetweenDates(monthStartDate, monthEndDate);
				for (Date deliveryDate : dateList) {

					Calendar c = toCalendar(deliveryDate);
					System.out.println(deliveryDate);
					String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
							"Saturday" };
					// Day_OF_WEEK starts from 1 while array index starts from 0
					System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
					if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") && scheduleDTO.isSu()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") && scheduleDTO.isMo()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") && scheduleDTO.isTu()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") && scheduleDTO.isWe()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") && scheduleDTO.isTh()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") && scheduleDTO.isFr()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") && scheduleDTO.isSa()) {
						addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
					}
				}

			}
		}
		return response;
	}

	public static Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	private void addOrderToOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate, Locale locale)
			throws ValidationException {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleInfoDTO.getId(), deliveryDate);
		if (orderInfoDTO == null) {
			Long orderId = addScheduledOrderInfo(scheduleInfoDTO, deliveryDate);
			addScheduledOrderDetailsInfo(orderId, scheduleInfoDTO, locale);

		}

	}

	private void addScheduledOrderDetailsInfo(long orderId, ScheduleInfoDTO scheduleInfoDTO, Locale locale)
			throws ValidationException {
		String productName = "";
		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId(scheduleInfoDTO.getProductId());
		orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null) {

				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
				}
				if (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus() || !productCityInfoDTO.isActive()
						|| !productCityInfoDTO.isStockStatus()) {
					if (productCityInfoDTO.getQuantityUnit() != null) {
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}
					// }
					throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
							ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
				}
			}

			orderDetailsDTO.setProductName(productInfoV2DTO.getProductName());
			orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
			/*
			 * orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
			 * orderDetailsDTO.setProductSalePrice(productCityInfoDTO.
			 * getSalePrice()); orderDetailsDTO
			 * .setTotalProductCost((productCityInfoDTO.getSalePrice() +
			 * productCityInfoDTO.getDeliveryCharge())
			 * scheduleInfoDTO.getQuantity());
			 */
			/*
			 * orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() -
			 * productCityInfoDTO.getSalePrice());
			 */
			orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}

		////////////////////////////////////////////////////////////////////

		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDetailsDTO.setCancelled(false);
		//orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);

	}

	private Long addScheduledOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {

		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(scheduleInfoDTO.getCustomerId());
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(deliveryDate);
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		//orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDTO.setScheduleId(scheduleInfoDTO.getId());
		orderDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDTO.setCancelled(false);
		CustomerDTO customerDTO = customerDAO.getById(scheduleInfoDTO.getCustomerId());
		orderDTO.setAddressId(customerDTO.getAddressId());
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (getZeroTimeDate(new Date(calendar.getTime().getTime())).before(getZeroTimeDate(enddate))
				|| getZeroTimeDate(new Date(calendar.getTime().getTime())).equals(getZeroTimeDate(enddate))) {
			Date result = new Date(calendar.getTime().getTime());
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	private void updateOrders(ScheduleInfoDTO scheduleDTO, ScheduleProductRequestV2 request, boolean addOrderRequired,
			Locale locale) throws ValidationException {
		boolean cancelled = false;
		// if (addOrderRequired) {
		// cancelled = false;
		ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleDTO.getId());
		Date scheduledFrom = null;
		if (scheduleInfoDTO.getScheduledUpto() == null) {
			scheduledFrom = scheduleInfoDTO.getStartDate();
		} else {
			scheduledFrom = scheduleDTO.getScheduledUpto();
		}
		addOrdersForSchedule("V2", scheduleDTO.getId(), scheduledFrom, locale);
		// }
		if (request.getScheduleStatus() == ScheduleStatus.PAUSE.getId()) {
			cancelled = true;
		}
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleDTO.getId());
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			TimeSlotDTO existingTimeSlotDTO = timeSlotDAO.getById(orderInfoDTO.getTimeSlotId());
			if (getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1
						&& cal.get(Calendar.HOUR_OF_DAY) >= (existingTimeSlotDTO.getStartTime()
								- existingTimeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
				orderDetailsInfo.setQuantity(request.getQuantity());
				if (cancelled) {
					orderDetailsInfo.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.NA.getName());
					//orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				} else {
					orderDetailsInfo.setOrderStatus(OrderStatus.CONFIRMED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.PENDING.getName());
					//orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				}
				orderDetailsInfo.setCancelled(cancelled);
				orderDetailsInfo.setOrderCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
				// orderDetailsInfo.setPaymentModeId(0);
				orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			}
			if (!cancelled) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
			} else {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
			}
			orderInfoDTO.setCancelled(cancelled);
			orderInfoDTO.setCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
			orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
			orderInfoDTO.setTimeSlotId(timeSlotDTO.getId());
			orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
	}

	private void pauseNextDelivery(ScheduleInfoDTO scheduleDTO) {
		scheduleDTO.setPauseDate(new Timestamp(System.currentTimeMillis()));
		Date pauseDate = getPauseDeliveryDate(scheduleDTO);

		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		if (pauseDate != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleDTO.getId(), pauseDate);
			if (orderInfoDTO != null) {
				orderInfoDTO.setCancelled(true);
				orderInfoDTO.setCancellationTime(new Timestamp(System.currentTimeMillis()));
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO
						.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
				for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
					orderDetailsDTO.setCancelled(true);
					orderDetailsDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
					orderDetailsDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsDTO.setPaymentStatus(PaymentStatus.NA.getName());
					//orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}
			}
		}
	}

	private Date getPauseDeliveryDate(ScheduleInfoDTO scheduleDTO) {
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };

		long currentTimeStamp = System.currentTimeMillis();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTimeInMillis(currentTimeStamp);
		Calendar pauseTime = Calendar.getInstance();
		if (scheduleDTO.getPauseDate() != null) {
			pauseTime.setTimeInMillis(scheduleDTO.getPauseDate().getTime());
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			List<Long> timeStampList = new ArrayList<>();

			int startDay = 0;
			int endDay = 7;

			if ((pauseTime.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getStartTime()))) {
				startDay = 0;
				if (getZeroTimeDate(scheduleDTO.getStartDate())
						.after(getZeroTimeDate(new Date(scheduleDTO.getPauseDate().getTime())))) {
					startDay = 1;
					endDay = 8;
				}
			} else {
				startDay = 1;
				endDay = 8;
			}

			for (; startDay < endDay; startDay++) {

				timeStampList.add(pauseTime.getTimeInMillis() + (startDay * 24 * 60 * 60 * 1000));

			}

			Calendar cal = Calendar.getInstance();
			for (Long timestamp : timeStampList) {
				cal.setTimeInMillis(timestamp);

				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") & scheduleDTO.isSu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") & scheduleDTO.isMo()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") & scheduleDTO.isTu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") & scheduleDTO.isWe()) {
					return new Date(timestamp);

				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") & scheduleDTO.isTh()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") & scheduleDTO.isFr()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") & scheduleDTO.isSa()) {
					return new Date(timestamp);
				}

			}
		}

		return null;
	}

	boolean checkTimeSlotValidation(ScheduleInfoDTO scheduleDTO, boolean sameDay, Calendar todayCal,
			TimeSlotDTO timeSlotDTO, long timestamp) {

		if ((sameDay) && ((scheduleDTO.getTimeSlotId() == 1))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getEndTime()))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) > (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
			return false;
		}

		return true;

	}

	private void deleteSchedule(ScheduleInfoDTO scheduleDTO) {
		Long scheduleId = scheduleDTO.getId();
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setActive(false);
		scheduleDTO.setScheduleStatus(scheduleDTO.getScheduleStatus());
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDAO.updateScheduleInfo(scheduleDTO);
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleId);
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			if (getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<Long> orderDetailsInfoIdList = orderDetailsInfoDAO.getIdListByOrderId(orderInfoDTO.getId());
			if (orderDetailsInfoIdList != null && !orderDetailsInfoIdList.isEmpty()
					&& orderDetailsInfoIdList.size() != 0) {
				orderDetailsInfoIdList = wrapWithQuotes(orderDetailsInfoIdList);
				orderDetailsInfoDAO.deleteorderDetailsInfoList(orderDetailsInfoIdList);
			}

			orderInfoDAO.deleteOrderInfo(orderInfoDTO);
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetScheduledProductResponseV2 getV2ScheduleList(String authToken, Locale locale) throws Exception {
		GetScheduledProductResponseV2 response = new GetScheduledProductResponseV2();
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));

		List<ScheduleInfoDTO> scheduleDTOList = scheduleDAO.getByCustomerId(customerDTO.getId());
		for (ScheduleInfoDTO scheduleInfoDTO : scheduleDTOList) {
			ScheduledProductInfoV2 scheduleProductInfo = getScheduleProductInfoV2(scheduleInfoDTO, locale);
			if (scheduleProductInfo != null) {
				response.getScheduledProductsListV2().add(scheduleProductInfo);
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	// @Override
	// @Transactional(rollbackFor = Exception.class)
	// public GenericResponse cancelUpcomingV2(String authToken, CancelOrder
	// request, Locale locale) throws Exception {
	// GenericResponse response = new GenericResponse();
	// logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
	// String mobile =
	// authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
	// for (Long orderDetailsId : request.getOrderDetailsIdList()) {
	// OrderDetailsInfoDTO orderDetailsInfoDTO =
	// orderDetailsInfoDAO.getById(orderDetailsId);
	// if (orderDetailsInfoDTO != null) {
	// List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
	// .getByOrderId(orderDetailsInfoDTO.getOrderId());
	// if (orderDetailsInfoList != null) {
	// if (orderDetailsInfoList.size() == 1) {
	// OrderInfoDTO orderInfo =
	// orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
	// orderDetailsInfoDAO.deleteOrderDetails(orderDetailsInfoDTO);
	// orderInfoDAO.deleteOrderInfo(orderInfo);
	// } else if (orderDetailsInfoList.size() > 1) {
	// orderDetailsInfoDAO.deleteOrderDetails(orderDetailsInfoDTO);
	// }
	// }
	// }
	// }
	//
	// response = ApiUtils.setResponseWithOperationId(response,
	// OperationId.CUSTOMER_SERVICES);
	// logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
	// return response;
	// }

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse cancelUpcomingV2(String authToken, CancelOrder request, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		//logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(request));
		for (Long orderDetailsId : request.getOrderDetailsIdList()) {
			OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsId);
			// Ranjana Code 14/062018
			if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
				int changeInQuantity = orderDetailsInfoDTO.getQuantity();
				checkCustomerTransaction(customerDTO.getId(), orderDetailsInfoDTO.getId(), changeInQuantity,
						orderDetailsInfoDTO.getTotalProductCost(), "CANCELLED");

				if (orderDetailsInfoDTO != null) {
					orderDetailsInfoDTO.setCancelled(true);
					orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
					orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
					orderDetailsInfoDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
					//orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());

					List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
							.getByOrderId(orderDetailsInfoDTO.getOrderId());

					if (orderDetailsInfoList != null) {
						OrderInfoDTO orderInfo = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
						if (orderDetailsInfoList.size() == 1) {
							orderInfo.setCancelled(true);
							orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
							orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
							//orderInfo.setPaymentModeId(PaymentMode.NA.getId());
							orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
							orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

							
						} else {
							int cntCancelled = 0;
							for (OrderDetailsInfoDTO orderD : orderDetailsInfoList) {
								if (orderD.getOrderStatus() == OrderStatus.CANCELLED.getName()) {
									cntCancelled++;
								}
							}
							if (cntCancelled == orderDetailsInfoList.size()) {
								orderInfo.setCancelled(true);
								orderInfo.setCancellationTime(new Timestamp(System.currentTimeMillis()));
								orderInfo.setOverallPaymentStatus(PaymentStatus.NA.getId());
								orderInfo.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
								orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
							}
							

						}
						setOrderSubTotal(orderInfo);
					}
				}
			}
		}
		
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		if (response.getStatus()) {
			String cancelMessage = "Dear Customer, We have cancelled your order as per your request. "
					+ "Please get in touch with us on 9990166644 in case of any queries. Team Maidin.";
			SmsSender.sendCheckoutOrderSuccessSMS(mobile, cancelMessage);
		}
		return response;
	}

	/**
	 * @param scheduleInfoDTO
	 * @return
	 * @throws ValidationException
	 */
	private ScheduledProductInfoV2 getScheduleProductInfoV2(ScheduleInfoDTO scheduleInfoDTO, Locale locale)
			throws ValidationException {
		ScheduledProductInfoV2 sInfoV2 = new ScheduledProductInfoV2();
		sInfoV2.setScheduleId(scheduleInfoDTO.getId());
		sInfoV2.setProductId(scheduleInfoDTO.getProductId());
		ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		ProductInfoV2DTO prInfoV2DTO = productInfoV2DAO.getByIdWithoutActiveState(productCityInfoV2DTO.getProductId());
		if (prInfoV2DTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		sInfoV2.setProductId(productCityInfoV2DTO.getId().intValue());
		BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(prInfoV2DTO.getBrandId());
		if (brandInfoDTO != null) {
			sInfoV2.setProductName(brandInfoDTO.getBrand() + " " + prInfoV2DTO.getProductName());
		} else {
			sInfoV2.setProductName(prInfoV2DTO.getProductName());
		}
		sInfoV2.setPrice(productCityInfoV2DTO.getSalePrice() + productCityInfoV2DTO.getDeliveryCharge());
		sInfoV2.setQuantityUnit(productCityInfoV2DTO.getQuantityUnit());
		sInfoV2.setProductStatus(productCityInfoV2DTO.isActive() ? 1 : 0);
		sInfoV2.setQuantity(scheduleInfoDTO.getQuantity());
		sInfoV2.setRecurring(scheduleInfoDTO.isRecurring());
		if (scheduleInfoDTO.getPauseDate() != null) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
			Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
			if (pauseDate != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(pauseDate);
				cal.set(Calendar.HOUR_OF_DAY, timeSlotDTO.getEndTime());
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);

				System.out.println("System Time" + new Timestamp(System.currentTimeMillis()));
				System.out.println("Pause Start Time" + new Timestamp(scheduleInfoDTO.getPauseDate().getTime()));
				System.out.println("Pause End Time" + new Timestamp(cal.getTimeInMillis()));
				System.out.println(
						"1st condition" + (System.currentTimeMillis() > scheduleInfoDTO.getPauseDate().getTime()));
				System.out.println("2nd condition" + (System.currentTimeMillis() < cal.getTimeInMillis()));
				if (((System.currentTimeMillis() + 1000) >= scheduleInfoDTO.getPauseDate().getTime())
						&& (System.currentTimeMillis() < cal.getTimeInMillis())) {
					sInfoV2.setScheduleStatus(ScheduleStatus.PAUSE.getId());
					System.out.println("---1---" + scheduleInfoDTO.getId());
				} else {
					sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
					System.out.println("---2---" + scheduleInfoDTO.getId());
				}
			} else {
				sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
				System.out.println("---3---" + scheduleInfoDTO.getId());
			}

		} else {
			sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
			System.out.println("---4---" + scheduleInfoDTO.getId());
		}

		sInfoV2.setStartDate(scheduleInfoDTO.getStartDate().getTime());
		sInfoV2.setTimeSlotId(scheduleInfoDTO.getTimeSlotId());
		if (scheduleInfoDTO.isSu())
			sInfoV2.getOrderDays().add(0);
		if (scheduleInfoDTO.isMo())
			sInfoV2.getOrderDays().add(1);
		if (scheduleInfoDTO.isTu())
			sInfoV2.getOrderDays().add(2);
		if (scheduleInfoDTO.isWe())
			sInfoV2.getOrderDays().add(3);
		if (scheduleInfoDTO.isTh())
			sInfoV2.getOrderDays().add(4);
		if (scheduleInfoDTO.isFr())
			sInfoV2.getOrderDays().add(5);
		if (scheduleInfoDTO.isSa())
			sInfoV2.getOrderDays().add(6);
		return sInfoV2;
	}

	private void updateSchedule(ScheduleProductRequestV2 request, ScheduleInfoDTO scheduleDTO) {
		/*
		 * scheduleDTO.setRecurring(request.isRecurring());
		 * scheduleDTO.setProductId(request.getProductId());
		 */
		scheduleDTO.setQuantity(request.getQuantity());
		/* scheduleDTO.setStartDate(new Date(request.getStartDate())); */
		/*
		 * List<Integer> orderDays = request.getOrderDays();
		 * scheduleDTO.setSu(orderDays.contains(0) ? true : false);
		 * scheduleDTO.setMo(orderDays.contains(1) ? true : false);
		 * scheduleDTO.setTu(orderDays.contains(2) ? true : false);
		 * scheduleDTO.setWe(orderDays.contains(3) ? true : false);
		 * scheduleDTO.setTh(orderDays.contains(4) ? true : false);
		 * scheduleDTO.setFr(orderDays.contains(5) ? true : false);
		 * scheduleDTO.setSa(orderDays.contains(6) ? true : false);
		 */
		scheduleDTO.setTimeSlotId(request.getTimeSlotId());
		scheduleDTO.setScheduleStatus(request.getScheduleStatus());
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDAO.updateScheduleInfo(scheduleDTO);

	}

	private Long addSchedule(ScheduleProductRequestV2 request, Long customerId, Locale locale) throws Exception {
		ScheduleInfoDTO scheduleDTO = new ScheduleInfoDTO();
		scheduleDTO.setCustomerId(customerId);
		scheduleDTO.setRecurring(request.isRecurring());

		/*
		 * if (request.getProductId() > 0) { ProductDTO productDTO =
		 * productDAO.getByProductId(request.getProductId()); if (productDTO !=
		 * null) { scheduleDTO.setProductId(request.getProductId()); } else {
		 * throw new ValidationException(ExceptionResourceBundle
		 * .getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE,
		 * locale)); } }
		 */
		/////////////////////////////////////////////////////////
		/*
		 * ProductCityInfoV2DTO productCityInfoDTO =
		 * productCityInfoDAO.getById(request.getProductId(), true); if
		 * (productCityInfoDTO == null) { throw new ValidationException(
		 * ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.
		 * PRODUCT_NOT_AVAILABLE, locale)); } ProductInfoV2DTO productInfoV2DTO
		 * = productInfoV2DAO.getById(productCityInfoDTO.getProductId(), true);
		 * if (productInfoV2DTO == null) { throw new ValidationException(
		 * ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.
		 * PRODUCT_NOT_AVAILABLE, locale)); }
		 */

		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO.getByIdWithoutActiveStatus(request.getProductId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null && (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus()
					|| !productCityInfoDTO.isActive() || !productCityInfoDTO.isStockStatus())) {
				String productName = "";
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if (brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null) {
					productName = brandInfoV2DTO.getBrand();
				}
				if (productInfoV2DTO.getProductName() != null) {
					if (brandInfoV2DTO != null) {
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
					if (productCityInfoDTO.getQuantityUnit() != null) {
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}
				}
				throw new ValidationException(ExceptionResourceBundle.getDynamicExceptionCodeProperties(productName,
						ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
			}

			scheduleDTO.setProductId((int) productCityInfoDTO.getId().intValue());
		}
		/////////////////////////////////////////////////////////
		scheduleDTO.setQuantity(request.getQuantity());
		scheduleDTO.setStartDate(new Date(request.getStartDate()));
		List<Integer> orderDays = request.getOrderDays();
		scheduleDTO.setSu(orderDays.contains(0) ? true : false);
		scheduleDTO.setMo(orderDays.contains(1) ? true : false);
		scheduleDTO.setTu(orderDays.contains(2) ? true : false);
		scheduleDTO.setWe(orderDays.contains(3) ? true : false);
		scheduleDTO.setTh(orderDays.contains(4) ? true : false);
		scheduleDTO.setFr(orderDays.contains(5) ? true : false);
		scheduleDTO.setSa(orderDays.contains(6) ? true : false);
		scheduleDTO.setTimeSlotId(request.getTimeSlotId());
		scheduleDTO.setScheduleStatus(ScheduleStatus.RESUME.getId());
		scheduleDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDTO.setActive(true);
		return scheduleDAO.addScheduleInfo(scheduleDTO);

	}

	private float clearOrder(Long customerId) {
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getAllScheduledOrders(customerId);
		List<TimeSlotDTO> timeSlotDTOList = timeSlotDAO.getAll();
		Calendar calenDar = Calendar.getInstance();
		Calendar databaseCalendar = Calendar.getInstance();
		Calendar tomorrowCalendar = Calendar.getInstance();
		tomorrowCalendar.add(Calendar.DAY_OF_YEAR, 1);
		calenDar.setTime(new Date((Calendar.getInstance().getTime()).getTime()));
		List<Long> scheduleIds = new ArrayList<Long>();
		float amountToRecharge = 0f;
		for (OrderInfoDTO orderInfo : orderInfoList) {
			// TimeSlotDTO timeSlotDTO =
			// timeSlotDAO.getById(orderInfo.getTimeSlotId());
			TimeSlotDTO timeSlotDTO = timeSlotDTOList.stream().filter(e -> e.getId().equals(orderInfo.getTimeSlotId()))
					.findFirst().get();
			if (timeSlotDTO == null) {
				continue;
			}
			databaseCalendar.setTime(new Date(orderInfo.getDeliveryDate().getTime()));
			boolean sameDayDelivery = databaseCalendar.get(Calendar.YEAR) == calenDar.get(Calendar.YEAR)
					&& databaseCalendar.get(Calendar.DAY_OF_YEAR) == calenDar.get(Calendar.DAY_OF_YEAR);
			boolean tomorrowDelivery = databaseCalendar.get(Calendar.YEAR) == tomorrowCalendar.get(Calendar.YEAR)
					&& databaseCalendar.get(Calendar.DAY_OF_YEAR) == tomorrowCalendar.get(Calendar.DAY_OF_YEAR);
			if (sameDayDelivery && calenDar
					.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours())) {
				continue;
			} else if (timeSlotDTO.getId() == 1 && tomorrowDelivery
					&& (calenDar.get(Calendar.HOUR_OF_DAY) > lastLockingPeriod)) {
				continue;
			} else {
				if (orderInfo.getScheduleId() != null) {
					scheduleIds.add(orderInfo.getScheduleId());
				}
				List<Long> orderDetailsInfoIdList = orderDetailsInfoDAO.getIdListByOrderId(orderInfo.getId());
				if (orderDetailsInfoIdList != null && !orderDetailsInfoIdList.isEmpty()
						&& orderDetailsInfoIdList.size() != 0) {
					orderDetailsInfoIdList = wrapWithQuotes(orderDetailsInfoIdList);
					amountToRecharge = amountToRecharge
							+ orderDetailsInfoDAO.getTotalPaidByCustomer(orderDetailsInfoIdList);
					deleteorderDetailsInfoList(orderDetailsInfoIdList);
				}
				deleteOrderInfo(orderInfo);

			}
		}
		if (scheduleIds != null && !scheduleIds.isEmpty() && scheduleIds.size() != 0) {
			scheduleIds = wrapWithQuotes(scheduleIds);
		}
		Set<Long> scheduleIdUniqueSet = new LinkedHashSet<>(scheduleIds);
		List<Long> scheduleIdUniqueList = new ArrayList<>();
		scheduleIdUniqueList.addAll(scheduleIdUniqueSet);
		deleteScheduleId(scheduleIdUniqueList);
		return amountToRecharge;
	}

	private void deleteScheduleId(List<Long> scheduleIdUniqueList) {
		/*
		 * for (Long scheduleId : scheduleIdUniqueList) { ScheduleInfoDTO
		 * scheduleDTO = scheduleDAO.getById(scheduleId); if (scheduleDTO !=
		 * null) { scheduleDTO.setActive(false); } }
		 */
		List<ScheduleInfoDTO> scheduleInfoDTOList = scheduleDAO.getByIds(scheduleIdUniqueList);
		scheduleInfoDTOList.stream().forEach(elt -> {
			elt.setActive(false);
			elt.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		});

	}

	private void deleteOrderInfo(OrderInfoDTO orderInfoDTO) {

		orderInfoDAO.deleteOrderInfo(orderInfoDTO);

	}

	private List<Long> wrapWithQuotes(List<Long> inputList) {
		List<Long> inputListWithQuote = Lists.newArrayList();
		for (Long input : inputList) {
			if (input != null) {
				inputListWithQuote.add(wrapWithQuotes(input));
			}
		}
		return inputListWithQuote;
	}

	private Long wrapWithQuotes(Long input) {
		return input;
	}

	private void deleteorderDetailsInfoList(List<Long> orderDetailsInfoIdList) {
		orderDetailsInfoDAO.deleteorderDetailsInfoList(orderDetailsInfoIdList);

	}

	private CartDetails setCartResponse(CartDTO cartDto) {
		CartDetails cartDetails = new CartDetails();
		cartDetails.setProductId(cartDto.getProductId());
		ProductDTO productDTO = productDAO.getByProductId(cartDto.getProductId());
		cartDetails.setProductCost(productDTO.getPrice());
		cartDetails.setQuantity(cartDto.getQuantity());
		cartDetails.setTotalCost(cartDto.getQuantity() * productDTO.getPrice());
		cartDetails.setTimeslot(cartDto.getTimeSlotId());
		return cartDetails;

	}

	private void addProductToCart(Long customerId, CartRequest request) {
		CartDTO cartDTO = new CartDTO();
		cartDTO.setCustomerId(customerId);
		cartDTO.setProductId(request.getProductId());
		cartDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		cartDTO.setQuantity(1);
		cartDAO.addCart(cartDTO);

	}

	private void confirmFutureOrders(Long customerId, ScheduleDetails request, Locale locale) throws Exception {
		checkOrderedFromTimeValidation(request.getOrderedFrom(), locale);
		// checkRecurrenceTypeValiddation();
		if (request.getRecurrenceTypeId() == 1) {
			request.setDeliveryTimestamp(request.getOrderedFrom());
			upsertOrder(customerId, request, locale);
		} else if (request.getRecurrenceTypeId() == 2) {
			checkOrderedToTimeValidation(request.getOrderedFrom(), request.getOrderedTo(), locale);

			ScheduleProductRequestV2 scheduleRequest = createScheduleRequest(request);

			Long scheduleId = addSchedule(scheduleRequest, customerId, locale);
			if (scheduleId != null) {
				ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleId);
				scheduleInfoDTO.setScheduleEndDate(new Date(request.getOrderedTo()));
				addOrdersForSchedule("V1", scheduleId, scheduleInfoDTO.getStartDate(), locale);

			}

		}

	}

	private ScheduleProductRequestV2 createScheduleRequest(ScheduleDetails scheduleDetails) {
		ScheduleProductRequestV2 request = new ScheduleProductRequestV2();
		request.setProductId(scheduleDetails.getProductId());
		request.setQuantity(scheduleDetails.getQuantity());
		request.setRecurring(true);
		request.setStartDate(scheduleDetails.getOrderedFrom());
		request.setTimeSlotId(scheduleDetails.getTimeSlotId());
		request.getOrderDays().addAll(Arrays.asList(0, 1, 2, 3, 4, 5, 6));
		return request;
	}

	private void checkOrderedToTimeValidation(long orderedFrom, long orderedTo, Locale locale)
			throws ValidationException {
		if (new Timestamp(orderedTo).before(new Timestamp(orderedFrom))) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.ORDERED_TO_DATE_MUST_BE_AFTER_ORDERED_FROM_DATE, locale));
		}

	}

	private void checkOrderedFromTimeValidation(long orderedFrom, Locale locale) throws ValidationException {
		if (new Timestamp(orderedFrom).before(new Timestamp(System.currentTimeMillis()))) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CANNOT_SET_ORDER_OF_PAST, locale));
		}

	}

	private void removeProductFromCart(Long customerId, long productId) {
		CartDTO cartDTO = cartDAO.getByCustomerIdAndProductId(customerId, productId);
		if (cartDTO != null) {
			cartDAO.removeCart(cartDTO);
		}

	}

	private void upsertOrder(long customerId, ConfirmOrderDetails request, Locale locale) throws Exception {
		// TimeSlotDTO timeSlotDTO =
		// timeSlotDAO.getById(request.getTimeSlotId());
		Long orderId = addOrderInfo(customerId, request);
		addOrderDetailsInfo(orderId, request, locale);

	}

	private Long addOrderInfo(long customerId, ConfirmOrderDetails request) {
		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(customerId);
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(new Date(request.getDeliveryTimestamp()));
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		orderDTO.setRequestedPaymentModeId(request.getPaymentModeId());
		//orderDTO.setPaymentModeId(request.getPaymentModeId());
		orderDTO.setCancelled(false);
		CustomerDTO customerDTO = customerDAO.getById(customerId);
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		orderDTO.setAddressId(addressDTO.getId());
		// orderDTO.setFlatId(addressDTO.getFlatId());
		/*
		 * orderDTO.setDeliveryLocation("Flat no. " + addressDTO.getFlat() +
		 * ", " + addressDTO.getTower() + ", " + addressDTO.getProject() + ", "
		 * + addressDTO.getCity());
		 */
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		// By Pushpinder
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());

		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	// By Pushpinder
	private void addOrderDetailsInfoAfterConfirm(long orderId, ConfirmOrderDetails request) {
		OrderDetailsInfoDTO orderDTO = new OrderDetailsInfoDTO();
		orderDTO.setOrderId(orderId);
		orderDTO.setProductId(request.getProductId());
		orderDTO.setQuantity(request.getQuantity());
		ProductDTO productDTO = productDAO.getByProductId(request.getProductId());
		if (productDTO != null) {
			orderDTO.setProductMRP(productDTO.getPrice());
		}
		orderDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setCancelled(false);
		orderDTO.setOrderCancellationTime(null);
		//orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDTO);

	}

	private GetOrderResponse getAllOrders(Long customerId, Locale locale) throws ValidationException {
		GetOrderResponse response = new GetOrderResponse();
		List<UpcomingOrderDTO> orderDetailsList = orderInfoDAO.getAllOrderDetails(customerId);
		List<Date> deliveryDateList = orderDetailsList.stream().map(UpcomingOrderDTO::getDeliveryDate).distinct()
				.collect(Collectors.toList());
		for (Date expectedDeliveryDate : deliveryDateList) {

			OrderDetails orderDetails = new OrderDetails();
			orderDetails.setTimestamp(expectedDeliveryDate.getTime());
			List<Long> nonVegetableOrderList = orderDetailsList.stream()
					.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() != 9)
					.map(UpcomingOrderDTO::getOrderId).distinct().collect(Collectors.toList());

			for (Long orderId : nonVegetableOrderList) {
				TimeSlotWise timeSloWise = new TimeSlotWise();
				List<UpcomingOrderDTO> orderDetailsInfoList = orderDetailsList.stream()
						.filter(e -> e.getDeliveryDate().equals(expectedDeliveryDate) && e.getCategoryId() != 9
								&& e.getOrderId() == orderId)
						.distinct().collect(Collectors.toList());

				for (UpcomingOrderDTO orderDetailsInfoDTO : orderDetailsInfoList) {
					OrderInfo details = new OrderInfo();

					if (orderDetailsInfoDTO != null) {
						details.setOrderId(orderDetailsInfoDTO.getOrderDetailsId());
						details.setProductId((int) orderDetailsInfoDTO.getProductDetailsId());
						details.setQuantity(orderDetailsInfoDTO.getQuantity());
						details.setProductName(
								orderDetailsInfoDTO.getBrandName() + " " + orderDetailsInfoDTO.getProductName());
						details.setProductUnit(orderDetailsInfoDTO.getProductUnit());
						details.setProductCost(orderDetailsInfoDTO.getSalePrice() > 0
								? (orderDetailsInfoDTO.getSalePrice() + orderDetailsInfoDTO.getDeliveryCharge())
								: (orderDetailsInfoDTO.getProductSalePrice()
										+ orderDetailsInfoDTO.getProductDeliveryCharge()));
						timeSloWise.getOrderList().add(details);
						timeSloWise.setTimeslotId(orderDetailsInfoDTO.getTimeSlotId());
					}
				}
				orderDetails.getTimeslotList().add(timeSloWise);

			}

			//////////////////////////////////////////////////////////////////////////////////
			/*
			 * List<Long> vegetableOrderList =
			 * orderDetailsList.stream().filter(e->e.getDeliveryDate().equals(
			 * expectedDeliveryDate) &&
			 * e.getCategoryId()==9).map(UpcomingOrderDTO::getOrderId).distinct(
			 * ).collect(Collectors.toList());
			 * 
			 * for (Long orderId : vegetableOrderList) { TimeSlotWiseV1
			 * timeSloWise = new TimeSlotWiseV1(); List<UpcomingOrderDTO>
			 * orderDetailsInfoList =
			 * orderDetailsList.stream().filter(e->e.getDeliveryDate().equals(
			 * expectedDeliveryDate) && e.getCategoryId()==9 && e.getOrderId()
			 * == orderId).distinct().collect(Collectors.toList());
			 * 
			 * for (UpcomingOrderDTO orderDetailsInfoDTO : orderDetailsInfoList)
			 * { OrderInfoV1 details = new OrderInfoV1();
			 * details.setOrderId(orderDetailsInfoDTO.getOrderId());
			 * details.setProductId((int)orderDetailsInfoDTO.getProductDetailsId
			 * ()); details.setQuantity(orderDetailsInfoDTO.getQuantity());
			 * details.setProductName(orderDetailsInfoDTO.getBrandName()+ " "
			 * +orderDetailsInfoDTO.getProductName());
			 * details.setProductUnit(orderDetailsInfoDTO.getProductUnit());
			 * details.setProductCost(orderDetailsInfoDTO.getSalePrice() > 0 ?
			 * (orderDetailsInfoDTO.getSalePrice() +
			 * orderDetailsInfoDTO.getDeliveryCharge()):
			 * (orderDetailsInfoDTO.getProductSalePrice()+orderDetailsInfoDTO.
			 * getProductDeliveryCharge()));
			 * timeSloWise.getOrderList().add(details);
			 * timeSloWise.setTimeslotId(orderDetailsInfoDTO.getTimeSlotId()); }
			 * if(timeSloWise.getOrderList().size() > 0){
			 * orderDetails.getTimeslotList().add(timeSloWise); }
			 * 
			 * }
			 */

			if (orderDetails.getTimeslotList().size() > 0) {
				response.getOrderDetails().add(orderDetails);
			}

		}
		return response;

	}

	/*
	 * private GetOrderResponse getAllOrders(Long customerId, Locale locale)
	 * throws ValidationException {
	 * 
	 * GetOrderResponse response = new GetOrderResponse();
	 * 
	 * List<Date> orderDateList =
	 * orderInfoDAO.getDifferentDeliveryDateByCustomerId(customerId); Calendar
	 * todayCalendar = Calendar.getInstance(); for (Date expectedDeliveryDate :
	 * orderDateList) { Calendar deliveryDateCalendar = Calendar.getInstance();
	 * Calendar tomorrowCalendar = Calendar.getInstance();
	 * tomorrowCalendar.add(Calendar.DAY_OF_YEAR, 1);
	 * deliveryDateCalendar.setTime(expectedDeliveryDate);
	 * todayCalendar.setTime(new
	 * Date((Calendar.getInstance().getTime()).getTime())); boolean sameDay =
	 * deliveryDateCalendar.get(Calendar.YEAR) ==
	 * todayCalendar.get(Calendar.YEAR) &&
	 * deliveryDateCalendar.get(Calendar.DAY_OF_YEAR) ==
	 * todayCalendar.get(Calendar.DAY_OF_YEAR);
	 * 
	 * OrderDetails orderDetails = new OrderDetails();
	 * 
	 * List<OrderInfoDTO> orderDTOList =
	 * orderInfoDAO.getByCustomerIdAndDeliveryDate(customerId,
	 * expectedDeliveryDate); for (OrderInfoDTO orderInfoDTO : orderDTOList) {
	 * TimeSlotDTO timeSlotDTO =
	 * timeSlotDAO.getById(orderInfoDTO.getTimeSlotId()); if (sameDay &&
	 * (todayCalendar.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getEndTime())))
	 * { continue; }
	 * 
	 * TimeSlotWise timeSloWise = new TimeSlotWise(); List<OrderDetailsInfoDTO>
	 * orderDetailsInfoList =
	 * orderDetailsInfoDAO.getByOrderId(orderInfoDTO.getId()); if
	 * (orderDetailsInfoList.size() == 0) { continue; } for (OrderDetailsInfoDTO
	 * orderDetailsInfoDTO : orderDetailsInfoList) { OrderInfo details = new
	 * OrderInfo(); if (orderDetailsInfoDTO != null) {
	 * details.setOrderId(orderDetailsInfoDTO.getId());
	 * details.setProductId(orderDetailsInfoDTO.getProductId());
	 * details.setQuantity(orderDetailsInfoDTO.getQuantity());
	 * ProductCityInfoV2DTO productCityInfoV2DTO =
	 * productCityInfoDAO.getById(orderDetailsInfoDTO.getProductId(), true);
	 * if(productCityInfoV2DTO == null){ throw new
	 * ValidationException(ExceptionResourceBundle
	 * .getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE,
	 * locale)); } ProductInfoV2DTO productInfoV2DTO =
	 * productInfoV2DAO.getById(productCityInfoV2DTO.getProductId(), true);
	 * if(productInfoV2DTO == null){ throw new
	 * ValidationException(ExceptionResourceBundle
	 * .getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE,
	 * locale)); } if (productInfoV2DTO != null) {
	 * details.setProductCost(orderDetailsInfoDTO.getProductSalePrice() > 0 ?
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * orderDetailsInfoDTO.getDeliveryCharges()):
	 * (productCityInfoV2DTO.getSalePrice()+productCityInfoV2DTO.
	 * getDeliveryCharge()));
	 * details.setProductName(productInfoV2DTO.getProductName());
	 * details.setProductUnit(productCityInfoV2DTO.getQuantityUnit());
	 * timeSloWise.getOrderList().add(details); } }
	 * 
	 * } timeSloWise.setTimeslotId(timeSlotDTO.getId());
	 * orderDetails.getTimeslotList().add(timeSloWise); }
	 * 
	 * if (orderDetails.getTimeslotList().size() > 0) {
	 * orderDetails.setTimestamp(expectedDeliveryDate.getTime());
	 * response.getOrderDetails().add(orderDetails); }
	 * 
	 * } return response;
	 * 
	 * }
	 */

	private void checkCustomerTransaction(Long customerId, Long orderDetailsId, int quantity, float totalPrice,
			String changeType) {
		OrderDetailsInfoDTO orderDetails = orderDetailsInfoDAO.getById(orderDetailsId);
		CustomerTransactionDTO dto = new CustomerTransactionDTO();
		dto.setCustomerId(customerId);
		CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
		transactionRequest.setCustomerId(customerId);
		transactionRequest.setOrderId(orderDetails.getOrderId());
		transactionRequest.setOrderDetailsId(orderDetailsId);
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);

		// if customer fully cancelled
		if (changeType.equals("CANCELLED")) {
			if (orderDetails.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())
					|| orderDetails.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.getName())) {
				transactionRequest.setMoneyIn(orderDetails.getAmountPaidByCustomer());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				// transactionRequest.setPaymentModeId(0);
				transactionRequest.setTransactionType(WalletDescription.REFUND_ON_CANCELLATION.getId());
				updateWalletBalance(customerId, orderDetails.getAmountPaidByCustomer(),
						WalletDescription.RECHARGE.getName());
				addCustomerTransaction(transactionRequest, -1);
				orderDetails.setAmountPaidByCustomer(0);
			}
		}
		// if decrease quantity
		else if (changeType.equals("DECREASE")) {
			float amountPaid = orderDetails.getAmountPaidByCustomer();
			float amountToPay = orderDetails.getQuantity() * orderDetails.getProductMRP();
			if (amountPaid >= amountToPay) {
				orderDetails.setAmountPaidByCustomer(amountToPay);
				if (amountPaid > amountToPay) {
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setMoneyIn(amountPaid - amountToPay);
					transactionRequest.setRemarkStatus(0);
					transactionRequest.setTransactionType(WalletDescription.REFUND_ON_QUANTITY_DECREASE.getId());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, amountPaid - amountToPay, WalletDescription.RECHARGE.getName());
				}
			}
		}
		// if increase quantity
		else if (changeType.equals("INCREASE")) {
			if (walletDTO != null && walletDTO.getMyWalletBalance() >= 0) {
				if (walletDTO.getMyWalletBalance() >= totalPrice) {
					orderDetails.setAmountPaidByCustomer(orderDetails.getAmountPaidByCustomer() + totalPrice);
					orderDetails.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
					transactionRequest.setMoneyOut(quantity * orderDetails.getProductMRP());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setDueAmount(0);
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS_ON_QUANTITY_INCREASE.getId());
					transactionRequest.setRemarkStatus(0);
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, totalPrice, WalletDescription.DEDUCTIONS.getName());
				} else if (walletDTO.getMyWalletBalance() < totalPrice) {
					orderDetails.setAmountPaidByCustomer(
							orderDetails.getAmountPaidByCustomer() + walletDTO.getMyWalletBalance());
					orderDetails.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
					transactionRequest.setMoneyOut(walletDTO.getMyWalletBalance());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS_ON_QUANTITY_INCREASE.getId());
					transactionRequest.setDueAmount(totalPrice - walletDTO.getMyWalletBalance());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, walletDTO.getMyWalletBalance(),
							WalletDescription.DEDUCTIONS.getName());
				}
			}
		}
	}

	private void checkCustomerTransaction1(Long customerId, Long orderDetailsId, int quantity, float totalPrice,
			String changeType) {
		OrderDetailsInfoDTO orderDetails = orderDetailsInfoDAO.getById(orderDetailsId);
		List<CustomerTransactionDTO> customerTransactionList = transactionDAO
				.getByOrderDetailsIdAndPaymentMode(orderDetailsId, PaymentMode.ByWallet.getId());
		float netBalance = 0;
		float totalMoneyDeducted = 0;
		float totalMoneyAdded = 0;
		if (customerTransactionList != null && customerTransactionList.size() > 0) {
			for (CustomerTransactionDTO transactionDTO : customerTransactionList) {
				totalMoneyDeducted = totalMoneyDeducted + transactionDTO.getMoneyOut();
				totalMoneyAdded = totalMoneyAdded + transactionDTO.getMoneyIn();
			}
		}
		netBalance = totalMoneyAdded - totalMoneyDeducted;
		// -netBalance minus means Company ---> Customer
		// +netBalance plus means Customer ---> Company
		CustomerTransactionDTO dto = new CustomerTransactionDTO();
		dto.setCustomerId(customerId);
		// if(netBalance < 0 ){
		CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
		transactionRequest.setCustomerId(customerId);
		transactionRequest.setOrderId(orderDetails.getOrderId());
		transactionRequest.setOrderDetailsId(orderDetailsId);
		// transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);

		// if customer fully cancelled
		if (changeType.equals("CANCELLED")) {
			if (netBalance < 0 && java.lang.Math.abs(netBalance) <= totalPrice) {
				transactionRequest.setMoneyIn(java.lang.Math.abs(netBalance));
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
				orderDetails.setAmountPaidByCustomer(0);
				updateWalletBalance(customerId, java.lang.Math.abs(netBalance), WalletDescription.RECHARGE.getName());
				addCustomerTransaction(transactionRequest, -1);

			}
		}
		// if decrease quantity
		else if (changeType.equals("DECREASE")) {
			float amountPaid = orderDetails.getAmountPaidByCustomer();
			if (amountPaid >= (orderDetails.getQuantity() * orderDetails.getProductMRP())) {
				orderDetails.setAmountPaidByCustomer(orderDetails.getQuantity() * orderDetails.getProductMRP());
				if (amountPaid > orderDetails.getQuantity() * orderDetails.getProductMRP()) {
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest
							.setMoneyIn(amountPaid - (orderDetails.getQuantity() * orderDetails.getProductMRP()));
					transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
					orderDetails.setAmountPaidByCustomer(quantity * orderDetails.getProductMRP());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, quantity * orderDetails.getProductMRP(),
							WalletDescription.RECHARGE.getName());
				}
			}

			/*
			 * if(netBalance < 0 && java.lang.Math.abs(netBalance) >= (quantity*
			 * orderDetails.getProductMRP()) ){
			 * orderDetails.setAmountPaidByCustomer(orderDetails.
			 * getAmountPaidByCustomer() - (quantity *
			 * orderDetails.getProductMRP()) );
			 * transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId())
			 * ; transactionRequest.setMoneyIn(quantity*
			 * orderDetails.getProductMRP());
			 * transactionRequest.setTransactionType(WalletDescription.RECHARGE.
			 * getName()); addCustomerTransaction(transactionRequest, -1);
			 * updateWalletBalance(customerId, quantity*
			 * orderDetails.getProductMRP(),
			 * WalletDescription.RECHARGE.getName()); }
			 */}
		// if increase quantity
		else if (changeType.equals("INCREASE")) {

			if (walletDTO != null && walletDTO.getMyWalletBalance() > 0) {
				if (walletDTO.getMyWalletBalance() >= totalPrice) {
					orderDetails.setAmountPaidByCustomer(orderDetails.getAmountPaidByCustomer() + totalPrice);
					orderDetails.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
					transactionRequest.setMoneyOut(quantity * orderDetails.getProductMRP());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, totalPrice, WalletDescription.DEDUCTIONS.getName());
				} else if (walletDTO.getMyWalletBalance() < totalPrice) {
					orderDetails.setAmountPaidByCustomer(
							orderDetails.getAmountPaidByCustomer() + walletDTO.getMyWalletBalance());
					orderDetails.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
					transactionRequest.setMoneyOut(walletDTO.getMyWalletBalance());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
					addCustomerTransaction(transactionRequest, -1);
					updateWalletBalance(customerId, walletDTO.getMyWalletBalance(),
							WalletDescription.DEDUCTIONS.getName());
					/*
					 * transactionRequest.setMoneyOut(totalPrice -
					 * walletDTO.getMyWalletBalance() );
					 * transactionRequest.setPaymentModeId(PaymentMode.ByCash.
					 * getId());
					 * transactionRequest.setTransactionType(WalletDescription.
					 * DEDUCTIONS.getName());
					 * addCustomerTransaction(transactionRequest, -1);
					 */
				}
			}
			/*
			 * else{ orderDetails.setAmountPaidByCustomer(orderDetails.
			 * getAmountPaidByCustomer() + totalPrice);
			 * orderDetails.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.
			 * getName()); transactionRequest.setMoneyOut(quantity *
			 * orderDetails.getProductMRP() );
			 * transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
			 * transactionRequest.setTransactionType(WalletDescription.
			 * DEDUCTIONS.getName()); addCustomerTransaction(transactionRequest,
			 * -1); }
			 */
		}
	}

	public static JSONArray readTimeSlotJSON(String filepath) {
		JSONParser parser = new JSONParser();
		JSONArray timeSlotArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			JSONObject jsonObject = (JSONObject) obj;
			timeSlotArray = (JSONArray) jsonObject.get("timeSlotList");
			/* System.out.println("json: " + timeSlotArray); */

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return timeSlotArray;
	}

	private JSONObject readTimeSlotArray(JSONArray timeSlotArray, String timeSlot) {
		JSONObject timeslotRequest = new JSONObject();
		for (int i = 0; i < timeSlotArray.size(); i++) {
			JSONObject timeSlotJSON = (JSONObject) timeSlotArray.get(i);
			if (timeSlotJSON.get("timeSlot").equals(timeSlot)) {
				timeslotRequest.put("Id", (Long) timeSlotJSON.get("id"));
				timeslotRequest.put("timeSlot", (String) timeSlotJSON.get("timeSlot"));
				timeslotRequest.put("lockingPeriod", (Long) timeSlotJSON.get("lockingPeriod"));
				timeslotRequest.put("slotStartTime", (Long) timeSlotJSON.get("slotStartTime"));
				timeslotRequest.put("slotEndTime", (Long) timeSlotJSON.get("slotEndTime"));
				break;
			}

		}
		return timeslotRequest;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final int lastLockingPeriod = Integer
			.parseInt(prop.getProperty(ResourceProperties.LASTLOCKINGPERIOD));
	private static final Long newScheduleEndDate = Long
			.parseLong(prop.getProperty(ResourceProperties.NEW_SCHEDULE_END_DATE));

}
