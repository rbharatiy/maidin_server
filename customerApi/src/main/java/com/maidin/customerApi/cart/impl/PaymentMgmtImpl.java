package com.maidin.customerApi.cart.impl;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.customerApi.cart.PaymentMgmt;
import com.maidin.customerApi.enums.PaymentStatus;
import com.maidin.customerApi.utils.ApiUtils;
import com.maidin.customerApi.utils.OperationId;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.OrderPaymentDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.CustomerDTO;
//import com.maidin.persistence.dto.OrderDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.pojo.payment.OrderDetails;
import com.maidin.pojo.payment.PaymentHistory;
import com.maidin.pojo.payment.PaymentHistoryResponse;

public class PaymentMgmtImpl implements PaymentMgmt {

	private final ProductDAO productDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final OrderPaymentDAO orderPaymentDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	
	@Autowired
	public PaymentMgmtImpl(ProductDAO productDAO, AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO,
		OrderPaymentDAO orderPaymentDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
		ProductCityInfoV2DAO productCityInfoV2DAO,ProductInfoV2DAO productInfoV2DAO,BrandInfoV2DAO brandInfoV2DAO) {
		this.productDAO = productDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.orderPaymentDAO = orderPaymentDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productInfoV2DAO =productInfoV2DAO;
		this.brandInfoV2DAO =brandInfoV2DAO;
	}
	private static final Logger logger = LoggerFactory.getLogger(PaymentMgmtImpl.class);

	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public PaymentHistoryResponse getPaymentHistory(String authToken , Locale locale)
			throws Exception {
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		List<Date> dateList = orderInfoDAO.getByCustomerIdAndLatestPastOrder(customerDTO.getId(),10,
				PaymentStatus.PAYMENTSETTLED.getName());
		response.setTotalOrderDays(dateList.size());
		int countTotalPastOrder=0;
		for(Date paymentDate : dateList){
			PaymentHistory paymentHistory = new PaymentHistory();
			paymentHistory.setTimestamp(paymentDate.getTime());
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO.getByCustomerIdDeliveryDateAndPaymentStatus(customerDTO.getId(), paymentDate, PaymentStatus.PAYMENTSETTLED.getName());
			int  quantity = 0;
			float totalCost = 0.0f;
			for(OrderDetailsInfoDTO orderDetailsInfoDTO :orderDetailsInfoList){
				OrderDetails order = new OrderDetails();
					order.setOrderId(orderDetailsInfoDTO.getId());
					order.setProductId(orderDetailsInfoDTO.getProductId());
					ProductDTO productDTO = productDAO.getByProductId(orderDetailsInfoDTO.getProductId());
					order.setProductName(productDTO.getProductName());
					order.setProductUnit(productDTO.getQuantityUnit());
					order.setQuantity(orderDetailsInfoDTO.getQuantity());
					order.setProductCost(orderDetailsInfoDTO.getProductMRP());
					paymentHistory.getOrderList().add(order);
					quantity = quantity + orderDetailsInfoDTO.getQuantity();
					totalCost = totalCost + orderDetailsInfoDTO.getTotalProductCost();
					
					
			   }
			paymentHistory.setTotalCost(totalCost);
			paymentHistory.setTotalQuantity(quantity);
			response.getPaymentHistoryList().add(paymentHistory);
			countTotalPastOrder = countTotalPastOrder+1;	
			//}
			
		}
		
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public PaymentHistoryResponse getPaymentHistoryByCalendar(String authToken, PaymentHistoryRequest request,
			Locale locale) throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		//List<Date> orderList = orderDAO.getDifferentTimestampsOfCustomerId(customerId);
		List<Date> dateList = orderDAO.getByCustomerIdAndCalendar(customerDTO.getId(), 
		new Date(request.getFromTimestamp()),new Date(request.getToTimestamp()),PaymentStatus.PAYMENTSETTLED.getName());
		for(Date paymentDate : dateList){
			PaymentHistory paymentHistory = new PaymentHistory();
			paymentHistory.setTimestamp(paymentDate.getTime());
			List<OrderDTO> orderList = orderDAO.getByCustomerIdDeliveryDate(customerDTO.getId(), paymentDate);
			int  quantity = 0;
			float totalCost = 0.0f;
			for(OrderDTO orderDTO :orderList){
				OrderDetails order = new OrderDetails();
					order.setOrderId(orderDTO.getId());
					order.setProductId(orderDTO.getProductId());
					order.setQuantity(orderDTO.getQuantity());
					order.setProductCost(orderDTO.getTotalOrderLineCost());
					quantity = quantity + orderDTO.getQuantity();
					totalCost = totalCost + orderDTO.getTotalOrderLineCost();
					paymentHistory.setTotalCost(totalCost);
					paymentHistory.setTotalQuantity(quantity);
					paymentHistory.getOrderList().add(order);
					response.getPaymentHistory().add(paymentHistory);
				
			   }
			paymentHistory.setTotalCost(totalCost);
			paymentHistory.setTotalQuantity(quantity);
			response.getPaymentHistoryList().add(paymentHistory);
		}
		
		//List<PaymentHistory> list = response.getPaymentHistory();
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	
*/
	

	@Override
	@Transactional(rollbackFor = Exception.class)
	public PaymentHistoryResponse getPaymentHistoryByPagination(String authToken, Integer currentPageNumber,
			Locale locale) throws Exception {
		
        String mobile = authenticationMgmt.getCustomerMobile(ApiUtils.createAuth(authToken));
        //CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobile)
        int elementsPerPage = this.elementsPerPage;
		int firstResult = (currentPageNumber - 1) * elementsPerPage;
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobile);
		logger.info("CustomerId : "+customerDTO.getId(),"authToken :"+authToken );
		PaymentHistoryResponse response = new PaymentHistoryResponse();
		List<Date> dateList = orderInfoDAO.getPastDaysDeliveryByPagination(customerDTO.getId(), PaymentStatus.PAYMENTSETTLED.getName(),
				firstResult, elementsPerPage);
		List<Date> totalDateCount = orderInfoDAO.getByCustomerIdAndLatestPastOrder(customerDTO.getId(),-1,
				PaymentStatus.PAYMENTSETTLED.getName());
		response.setTotalOrderDays(totalDateCount.size());
		int countTotalPastOrder=0;
		for(Date paymentDate : dateList){
			PaymentHistory paymentHistory = new PaymentHistory();
			paymentHistory.setTimestamp(paymentDate.getTime());
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO.getByCustomerIdDeliveryDateAndPaymentStatus(customerDTO.getId(), paymentDate, PaymentStatus.PAYMENTSETTLED.getName());
			int  quantity = 0;
			float totalCost = 0.0f;
			for(OrderDetailsInfoDTO orderDetailsInfoDTO :orderDetailsInfoList){
				OrderDetails order = new OrderDetails();
					order.setOrderId(orderDetailsInfoDTO.getId());
					order.setProductId(orderDetailsInfoDTO.getProductId());
					/*ProductDTO productDTO = productDAO.getById(orderDetailsInfoDTO.getProductId());
					if(productDTO == null){
						continue;
					}*/
					ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId());
					if(productCityInfoV2DTO == null){
						continue;
					}
					ProductInfoV2DTO productInfoV2DTO =productInfoV2DAO.getByIdWithoutActiveState(productCityInfoV2DTO.getProductId());
					if(productInfoV2DTO == null){
						continue;
					}
					BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
					if(brandInfoV2DTO != null){
						order.setProductName(brandInfoV2DTO.getBrand()+ " " +productInfoV2DTO.getProductName());
					}
					else{
					order.setProductName(productInfoV2DTO.getProductName());
					}
					order.setProductUnit(productCityInfoV2DTO.getQuantityUnit());
					order.setQuantity(orderDetailsInfoDTO.getQuantity());
					/*order.setProductCost(orderDetailsInfoDTO.getProductMRP());*/
					order.setProductCost(orderDetailsInfoDTO.getProductSalePrice()+orderDetailsInfoDTO.getDeliveryCharges());
					paymentHistory.getOrderList().add(order);
					quantity = quantity + orderDetailsInfoDTO.getQuantity();
					totalCost = totalCost + orderDetailsInfoDTO.getTotalProductCost();
					
					
			   }
			paymentHistory.setTotalCost(totalCost);
			paymentHistory.setTotalQuantity(quantity);
			response.getPaymentHistoryList().add(paymentHistory);
			countTotalPastOrder = countTotalPastOrder+1;	
			
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,customerDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	int elementsPerPage = Integer.parseInt(prop.getProperty(ResourceProperties.ELEMENTS_PER_PAGE));


	/*private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(
					"Unable to load the properties file: " + fileName);
		}
	}
	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	*///private static final int defaultPaymentDaysCount = Integer.parseInt(prop.getProperty(ResourceProperties.DEFAULT_PAYMENT_DAYS_COUNT));
	

}
