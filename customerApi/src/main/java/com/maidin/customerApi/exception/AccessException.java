package com.maidin.customerApi.exception;

import com.maidin.common.exception.ExceptionDetail;
import com.maidin.common.exception.UserException;

public class AccessException extends UserException {

    private static final long serialVersionUID = 6702339420964932612L;

    public AccessException(ExceptionDetail exceptionDetail) {
        super(exceptionDetail.getCode(), exceptionDetail.getMessage());
    }
}
