package com.maidin.customerApi.exception;

public class ExceptionCode {

	public static final String NO_UPDATE_IN_MOBILE = "NO_UPDATE_IN_MOBILE";
	public static final String MOBILE_ALREADY_IN_USE = "MOBILE_ALREADY_IN_USE";
	public static final String INVALID_MOBILE_NUMBER = "INVALID_MOBILE_NUMBER";
	public static final String CUSTOMER_DOES_NOT_EXIST = "CUSTOMER_DOES_NOT_EXIST";
	public static final String ALREADY_REGISTERED = "ALREADY_REGISTERED";
	public static final String CUSTOMER_ID_IS_DEACTIVATED = "CUSTOMER_ID_IS_DEACTIVATED";
	public static final String NO_UPDATE_IN_EMAIL = "NO_UPDATE_IN_EMAIL";
	public static final String EMAIL_ALREADY_IN_USE = "EMAIL_ALREADY_IN_USE";
	public static final String INVALID_CODE = "INVALID_CODE";
	public static final String INVALID_OTP = "INVALID_OTP";
	public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
	public static final String INVALID_DATE_TIME_FORMAT = "INVALID_DATE_TIME_FORMAT";
	public static final String INVALID_IMAGE = "INVALID_IMAGE";
	public static final String CANNOT_SET_ORDER_OF_PAST = "CANNOT_SET_ORDER_OF_PAST";
	public static final String ORDERED_TO_DATE_MUST_BE_AFTER_ORDERED_FROM_DATE = "ORDERED_TO_DATE_MUST_BE_AFTER_ORDERED_FROM_DATE";
	public static final String CANNOT_CHECKOUT_PAST_OR_FUTURE_DAYS = "CANNOT_CHECKOUT_PAST_OR_FUTURE_DAYS";
	public static final String LOW_WALLET_BALANCE = "LOW_WALLET_BALANCE";
	public static final String INVALID_ORDER = "INVALID_ORDER";
	public static final String PERMISSION_DENIED = "PERMISSION_DENIED";
	public static final String EMAIL_MISSING = "EMAIL_MISSING";
	public static final String PRODUCT_NOT_AVAILABLE = "PRODUCT_NOT_AVAILABLE";
	
	public static final String FILE_DOES_NOT_EXIST = "FILE_DOES_NOT_EXIST";
	public static final String MD5_DOES_NOT_MATCH = "MD5_DOES_NOT_MATCH";
	public static final String INVALID_FILE_NAME = "INVALID_FILE_NAME";
	public static final String INVALID_VALUE_OF_PRIMARYPHOTO = "INVALID_VALUE_OF_PRIMARYPHOTO";
	public static final String FILE_DOWNLOAD_FAILED = "FILE_DOWNLOAD_FAILED";
	public static final String SLOT_NOT_AVAILABLE = "SLOT_NOT_AVAILABLE";
	public static final String PHOTO_NOT_PRESENT = "PHOTO_NOT_PRESENT";
	public static final String ADDRESS_NOT_REGISTERED = "ADDRESS_NOT_REGISTERED";
	public static final String USER_NOT_REGISTERED_WITH_ADDRESS = "USER_NOT_REGISTERED_WITH_ADDRESS";
	public static final String PRODUCTDETAILS_NOT_AVAILABLE = "PRODUCTDETAILS_NOT_AVAILABLE";
	public static final String EMAIL_NOT_FOUND = "EMAIL_NOT_FOUND";
	public static final String CANNOT_INCREASE_QUANTITY = "CANNOT_INCREASE_QUANTITY";

	
}