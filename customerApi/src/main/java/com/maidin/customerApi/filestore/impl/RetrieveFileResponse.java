package com.maidin.customerApi.filestore.impl;

import java.io.ByteArrayOutputStream;

import com.google.common.io.ByteSource;

public class RetrieveFileResponse {

	private String contentType;
	private String fileName;
	private ByteSource byteSource;
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	private int size;
	public ByteArrayOutputStream getByteSream() {
		return byteSream;
	}

	public void setByteSream(ByteArrayOutputStream byteSream) {
		this.byteSream = byteSream;
	}

	private ByteArrayOutputStream byteSream;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String value) {
        this.contentType = value;
    }

	public ByteSource getByteSource() {
		return byteSource;
	}

	public void setByteSource(ByteSource byteSource) {
		this.byteSource = byteSource;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
