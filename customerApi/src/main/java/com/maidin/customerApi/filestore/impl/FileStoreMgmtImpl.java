package com.maidin.customerApi.filestore.impl;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.hash.Hashing;
import com.google.common.io.ByteSource;
import com.maidin.common.exception.ValidationException;
import com.maidin.crypto.Crypto;
import com.maidin.customerApi.exception.ExceptionCode;
import com.maidin.customerApi.exception.ExceptionResourceBundle;
import com.maidin.customerApi.filestore.FileStoreMgmt;
import com.maidin.filestore.FileId;
import com.maidin.filestore.FileIdV2;
import com.maidin.filestore.FileStore;
import com.maidin.filestore.FileStoreInstance;
import com.maidin.persistence.dao.FileStoreDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dto.FileStoreDTO;

public class FileStoreMgmtImpl implements FileStoreMgmt {

	private static final Logger LOG = LoggerFactory.getLogger(FileStoreMgmtImpl.class);

	private final FileStore fileStore;
	private final FileStoreDAO fileStoreDAO;
	private final Crypto crypto;
	private final PhotoDAO photoDAO;

	@Autowired
	public FileStoreMgmtImpl(FileStoreDAO fileStoreDAO, Crypto crypto, FileStoreInstance fileStoreInstance,
			PhotoDAO photoDAO) {
		this.fileStoreDAO = fileStoreDAO;
		this.crypto = crypto;
		this.fileStore = fileStoreInstance.getInstance();
		this.photoDAO = photoDAO;
	}

	@Override
	public RetrieveFileResponse retrieveFile(int imageType, long itemId, long fileStoreId, boolean encrypted,
			Locale locale) throws Exception {
		FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
		}

		String folderPath = imageType + FileId.separator + itemId + FileId.separator + fileStoreId;
		String fileName = dto.getFileName();
		FileId fileId = FileId.forUser(imageType, itemId, folderPath, fileName);
		ByteSource byteSource = fileStore.retrieveFile(fileId);

		byte[] bytes = byteSource.read();
		String md5 = Hashing.md5().hashBytes(bytes).toString();
		if (!dto.getMd5().equals(md5)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.MD5_DOES_NOT_MATCH, locale));
		}

		if (encrypted) {
			bytes = crypto.decrypt(bytes);
			byteSource = ByteSource.wrap(bytes);
		} else {
			byteSource = ByteSource.wrap(bytes);
		}

		RetrieveFileResponse response = new RetrieveFileResponse();
		response.setContentType(dto.getContentType());
		response.setByteSource(byteSource);
		response.setFileName(fileName);
		return response;
	}

	@Override
	public RetrieveFileResponse retrieveFileV2FromBucket2(int imageType, long fileStoreId, boolean encrypted, Locale locale) throws Exception {
		FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
		}

		//String folderPath = String.valueOf(fileStoreId);
		String folderPath = imageType + FileId.separator + fileStoreId;
		String fileName = dto.getFileName();
		FileIdV2 fileId = FileIdV2.forUser(imageType, folderPath, fileName);
		ByteSource byteSource = fileStore.retrieveFileV2FromBucket2(fileId);

		byte[] bytes = byteSource.read();
		String md5 = Hashing.md5().hashBytes(bytes).toString();
		if (!dto.getMd5().equals(md5)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.MD5_DOES_NOT_MATCH, locale));
		}

		if (encrypted) {
			bytes = crypto.decrypt(bytes);
			byteSource = ByteSource.wrap(bytes);
		} else {
			byteSource = ByteSource.wrap(bytes);
		}

		RetrieveFileResponse response = new RetrieveFileResponse();
		response.setContentType(dto.getContentType());
		response.setByteSource(byteSource);
		response.setFileName(fileName);
		return response;
	}

	@Override
	public RetrieveFileResponse retrieveFileV2FromBucket1(long fileStoreId, boolean encrypted, Locale locale) throws Exception {
		FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
		}

		String folderPath = String.valueOf(fileStoreId);
		String fileName = dto.getFileName();
		FileIdV2 fileId = FileIdV2.forUser(folderPath, fileName);
		ByteSource byteSource = fileStore.retrieveFileV2FromBucket1(fileId);

		byte[] bytes = byteSource.read();
		String md5 = Hashing.md5().hashBytes(bytes).toString();
		if (!dto.getMd5().equals(md5)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.MD5_DOES_NOT_MATCH, locale));
		}

		if (encrypted) {
			bytes = crypto.decrypt(bytes);
			byteSource = ByteSource.wrap(bytes);
		} else {
			byteSource = ByteSource.wrap(bytes);
		}

		RetrieveFileResponse response = new RetrieveFileResponse();
		response.setContentType(dto.getContentType());
		response.setByteSource(byteSource);
		response.setFileName(fileName);
		return response;
	}
}
