package com.maidin.customerApi.filestore;

import java.util.Locale;

import com.google.common.io.ByteSource;
import com.maidin.common.exception.ValidationException;
import com.maidin.customerApi.filestore.impl.RetrieveFileResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.filestore.RetrieveFile;
import com.maidin.pojo.filestore.StoreFile;

public interface FileStoreMgmt {

	/*
	 * public GenericResponse addFile(long userId, StoreFile storeFile,
	 * ByteSource byteSource, Locale locale) throws Exception;
	 * 
	 * public GenericResponse updateFile(long userId, StoreFile storeFile,
	 * ByteSource byteSource, Locale locale) throws Exception;
	 * 
	 * 
	 * 
	 * public long uploadFile(long userId, StoreFile storeFile, ByteSource
	 * byteSource, Locale locale) throws Exception;
	 * 
	 * 
	 * 
	 * public void removeFile(long userId, long[] fileStoreIds, Locale locale)
	 * throws Exception;
	 */

	public RetrieveFileResponse retrieveFile(int imageType, long itemId, long fileStoreId, boolean encrypted,
			Locale locale) throws Exception;

	public RetrieveFileResponse retrieveFileV2FromBucket2(int imageType, long fileStoreId, boolean b, Locale locale) throws Exception;

	public RetrieveFileResponse retrieveFileV2FromBucket1(long fileStoreId, boolean b, Locale locale) throws Exception;

	/*public long uploadFile(int imageType, long imageId, StoreFile storeFile, ByteSource source, Locale locale)
			throws Exception;*/

}
