package com.maidin.common.exception;

public class ValidationException extends UserException {

    private static final long serialVersionUID = 3462307144108952977L;
    
    public ValidationException(ExceptionDetail exceptionDetail) {
        super(exceptionDetail.getCode(), exceptionDetail.getMessage());
    }

}