package com.maidin.common.exception;

public class MandatoryFieldMissingException extends UserException {

    private static final long serialVersionUID = -6900477408270586961L;
    
    public MandatoryFieldMissingException(ExceptionDetail exceptionDetail) {
        super(exceptionDetail.getCode(), exceptionDetail.getMessage());
    }

}
