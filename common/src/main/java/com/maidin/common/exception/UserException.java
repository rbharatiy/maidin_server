package com.maidin.common.exception;

public class UserException extends Exception {

    private static final long serialVersionUID = -171616030698003561L;
    private final String code;
    private final String message;
    
    public UserException(String code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public String getCode() {
        return code;
    }
    
    @Override
    public String getMessage() {
        return message;
    }
    
}
