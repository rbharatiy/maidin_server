package com.maidin.common.properties;

import java.io.File;

/**
 * Class containing the key name in the resource files
 * 
 */
public class ResourceProperties {

	public static final String SMTP_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "smtp.properties";
	public static final String CONTACT_SMTP_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "contact.support.smtp.properties";

	public static final String MIN_SUPPORTED_VER = "minSupportedVer";
	public static final String MIN_SUPPORTED_VER_ANDROID = "minSupportedVerAndroid";
	public static final String MIN_SUPPORTED_VER_IOS = "minSupportedVerIOS";
	public static final String MIN_SUPPORTED_VER_ADMIN = "minSupportedVerAdmin";
	public static final String MIN_SUPPORTED_VER_CUSTOMER = "minSupportedVerCustomer";
	public static final String SMTP_HOST = "mail.smtp.host";
	public static final String SMTP_PORT = "mail.smtp.port";
	public static final String SMTP_AUTH = "mail.smtp.auth";
	public static final String SMTP_STARTTLS = "mail.smtp.starttls.enable";
	public static final String SMTP_FROM = "mail.smtp.from";
	public static final String SMTP_USERNAME = "mail.smtp.username";
	public static final String SMTP_PASSWORD = "mail.smtp.password";
	public static final String SMTP_TO = "mail.smtp.to";
	public static final String SMTP_TIMEOUT = "mail.smtp.timeout";
	public static final String SMTP_CONNECTIONTIMEOUT = "mail.smtp.connectiontimeout";

	public static final String SMS_FILE_LOCATION = System.getProperty("config.dir") + File.separator + "sms.properties";

	public static final String SMS_URL = "url";
	public static final String SMS_USER = "user";
	public static final String SMS_SENDER_ID = "senderID";
	public static final String SMS_STATE = "state";

	public static final String CCAVENUE_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "ccavenue.properties";

	public static final String CCAVENUE_ACCESSCODESECURE = "accessCodeSecure";
	public static final String CCAVENUE_WORKINGKEYSECURE = "workingKeySecure";
	public static final String CCAVENUE_ACCESSCODETEST = "accessCodeTest";
	public static final String CCAVENUE_WORKINGKEYTEST = "workingKeyTest";
	public static final String CCAVENUE_MERCHANTID = "merchantId";
	public static final String CONNECTION_PROPERTIES_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "urlConnection.properties";

	public static final String CONNECTION_TIMEOUT = "connectionTimeout";
	public static final String READ_TIMEOUT = "readTimeout";

	public static final String COMMON_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "common.properties";
	public static final String ERROR_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "error.properties";

	public static final String AWS_PROPERTY_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "s3.properties";
	public static final String AWS_S3_ACCESS_KEY = "accessKey";
	public static final String AWS_S3_SECRET_KEY = "secretKey";
	public static final String AWS_S3_FILESTORE_BUCKET = "bucketName";
	public static final String AWS_S3_BASE_URL = "baseUrl";

	public static final String FCM_URL = "fcm.api.url";
	public static final String FCM_KEY_ADMIN = "fcm.api.admin.key";
	public static final String FCM_KEY_CUSTOMER = "fcm.api.customer.key";

	public static final String GOOGLE_DISTANCE_MATRIX_API_URL = "google.distance.matrix.api.url";
	public static final String GOOGLE_GEOCODE_API_URL = "google.geocode.api.url";
	public static final String GOOGLE_API_KEY = "google.api.key";

	// public static final String CURRENT_CGSTQUERY = null;
	// public static final String CURRENT_SGSTQUERY = null;

	public static final String CGST = "CGST";
	public static final String SGST = "SGST";
	public static final String UTGST = "UTGST";

	public static final String LOGOUT = "logout";

	// paths

	public static final String CONTEXTPATH = "context.Path";
	public static final String CONTEXTWEBPATH = "context.web.Path";

	public static final String VERIFY_EMAIL_TEMPLATE = "verify-email.template";
	public static final String VERIFY_EMAIL_SUBJECT = "GADDI4U - Verify your email-id";

	public static final String PIXEL_RANGE_0_250 = "PIXEL_RANGE_0_250";
	public static final String PIXEL_RANGE_251_500 = "PIXEL_RANGE_251_500";
	public static final String PIXEL_RANGE_501_1000 = "PIXEL_RANGE_501_1000";
	public static final String PIXEL_RANGE_1001_2500 = "PIXEL_RANGE_1001_2500";
	public static final String PIXEL_RANGE_2501_3500 = "PIXEL_RANGE_2501_3500";
	public static final String PIXEL_RANGE_3501_5000 = "PIXEL_RANGE_3501_5000";
	public static final String PIXEL_RANGE_5001 = "PIXEL_RANGE_5001";

	public static final String PRODUCT_PIXEL_RANGE_0_250 = "PRODUCT_PIXEL_RANGE_0_250";
	public static final String PRODUCT_PIXEL_RANGE_251_500 = "PRODUCT_PIXEL_RANGE_251_500";
	public static final String PRODUCT_PIXEL_RANGE_501_1000 = "PRODUCT_PIXEL_RANGE_501_1000";
	public static final String PRODUCT_PIXEL_RANGE_1001_2500 = "PRODUCT_PIXEL_RANGE_1001_2500";
	public static final String PRODUCT_PIXEL_RANGE_2501_3500 = "PRODUCT_PIXEL_RANGE_2501_3500";
	public static final String PRODUCT_PIXEL_RANGE_3501_5000 = "PRODUCT_PIXEL_RANGE_3501_5000";
	public static final String PRODUCT_PIXEL_RANGE_5001 = "PRODUCT_PIXEL_RANGE_5001";

	public static final String BRAND_PIXEL_RANGE_0_250 = "BRAND_PIXEL_RANGE_0_250";
	public static final String BRAND_PIXEL_RANGE_251_500 = "BRAND_PIXEL_RANGE_251_500";
	public static final String BRAND_PIXEL_RANGE_501_1000 = "BRAND_PIXEL_RANGE_501_1000";
	public static final String BRAND_PIXEL_RANGE_1001_2500 = "BRAND_PIXEL_RANGE_1001_2500";
	public static final String BRAND_PIXEL_RANGE_2501_3500 = "BRAND_PIXEL_RANGE_2501_3500";
	public static final String BRAND_PIXEL_RANGE_3501_5000 = "BRAND_PIXEL_RANGE_3501_5000";
	public static final String BRAND_PIXEL_RANGE_5001 = "BRAND_PIXEL_RANGE_5001";

	public static final String CATEGORY_PIXEL_RANGE_0_250 = "CATEGORY_PIXEL_RANGE_0_250";
	public static final String CATEGORY_PIXEL_RANGE_251_500 = "CATEGORY_PIXEL_RANGE_251_500";
	public static final String CATEGORY_PIXEL_RANGE_501_1000 = "CATEGORY_PIXEL_RANGE_501_1000";
	public static final String CATEGORY_PIXEL_RANGE_1001_2500 = "CATEGORY_PIXEL_RANGE_1001_2500";
	public static final String CATEGORY_PIXEL_RANGE_3501_5000 = "CATEGORY_PIXEL_RANGE_3501_5000";
	public static final String CATEGORY_PIXEL_RANGE_2501_3500 = "CATEGORY_PIXEL_RANGE_2501_3500";
	public static final String CATEGORY_PIXEL_RANGE_5001 = "CATEGORY_PIXEL_RANGE_5001";
	
	public static final String PAYMENTOPTION_PIXEL_RANGE_0_250 = "PAYMENTOPTION_PIXEL_RANGE_0_250";
	public static final String PAYMENTOPTION_PIXEL_RANGE_251_500 = "PAYMENTOPTION_PIXEL_RANGE_251_500";
	public static final String PAYMENTOPTION_PIXEL_RANGE_501_1000 = "PAYMENTOPTION_PIXEL_RANGE_501_1000";
	public static final String PAYMENTOPTION_PIXEL_RANGE_1001_2500 = "PAYMENTOPTION_PIXEL_RANGE_1001_2500";
	public static final String PAYMENTOPTION_PIXEL_RANGE_2501_3500 = "PAYMENTOPTION_PIXEL_RANGE_2501_3500";
	public static final String PAYMENTOPTION_PIXEL_RANGE_3501_5000 = "PAYMENTOPTION_PIXEL_RANGE_3501_5000";
	public static final String PAYMENTOPTION_PIXEL_RANGE_5001 = "PAYMENTOPTION_PIXEL_RANGE_5001";
	
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_0_250 = "PRODUCTDESCRIPTION_PIXEL_RANGE_0_400";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_251_500 = "PRODUCTDESCRIPTION_PIXEL_RANGE_401_800";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_501_1000 = "PRODUCTDESCRIPTION_PIXEL_RANGE_801_1000";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_1001_2500 = "PRODUCTDESCRIPTION_PIXEL_RANGE_1001_2500";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_2501_3500 = "PRODUCTDESCRIPTION_PIXEL_RANGE_2501_3500";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_3501_5000 = "PRODUCTDESCRIPTION_PIXEL_RANGE_3501_5000";
	public static final String PRODUCTDESCRIPTION_PIXEL_RANGE_5001 = "PRODUCTDESCRIPTION_PIXEL_RANGE_5001";
	
	public static final String PRODUCTDETAIL_PIXEL_RANGE_0_250 = "PRODUCTDETAIL_PIXEL_RANGE_0_250";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_251_500 = "PRODUCTDETAIL_PIXEL_RANGE_251_500";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_501_1000 = "PRODUCTDETAIL_PIXEL_RANGE_501_1000";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_1001_2500 = "PRODUCTDETAIL_PIXEL_RANGE_1001_2500";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_2501_3500 = "PRODUCTDETAIL_PIXEL_RANGE_2501_3500";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_3501_5000 = "PRODUCTDETAIL_PIXEL_RANGE_3501_5000";
	public static final String PRODUCTDETAIL_PIXEL_RANGE_5001 = "PRODUCTDETAIL_PIXEL_RANGE_5001";


	public static final String NOTIFICATION_TYPE_RECHARGE_WALLET = "NOTIFICATION_TYPE_RECHARGE_WALLET";
	public static final String NOTIFICATION_TYPE_DISCHARGE_WALLET = "NOTIFICATION_TYPE_DISCHARGE_WALLET";
	public static final String NOTIFICATION_TYPE_ORDER_DELIVERY_MARKED = "NOTIFICATION_TYPE_ORDER_DELIVERY_MARKED";
	public static final String NOTIFICATION_TYPE_ORDER_OUT_FOR_DELIVERY = "NOTIFICATION_TYPE_ORDER_OUT_FOR_DELIVERY";
	public static final String NOTIFICATION_TYPE_PROMOTIONAL_OFFERS = "NOTIFICATION_TYPE_PROMOTIONAL_OFFERS";
	public static final String NOTIFICATION_TYPE_ONGOING_SALE = "NOTIFICATION_TYPE_ONGOING_SALE";
	public static final String NOTIFICATION_TYPE_DISCOUNT_ON_PRODUCTS = "NOTIFICATION_TYPE_DISCOUNT_ON_PRODUCTS";
	public static final String NOTIFICATION_TYPE_ORDER_SUCCESS = "NOTIFICATION_TYPE_ORDER_SUCCESS";

	public static final String NOTIFICATION_TYPE_NEW_USER_REGISTRATION = "NOTIFICATION_TYPE_NEW_USER_REGISTRATION";
	public static final String NOTIFICATION_TYPE_DELIVERY_CANCELLED_BY_CUSTOMER = "NOTIFICATION_TYPE_DELIVERY_CANCELLED_BY_CUSTOMER";

	public static final String RECHARGE_SUCCESS_NOTIFICATION_TITLE = "Recharge successful!";
	public static final String RECHARGE_SUCCESS_NOTIFICATION_BODY = "Your Maidin Money is successfully recharged with ₹ ";

	public static final String DEVICE_PLATFORM_ANDROID = "android";
	public static final String DEVICE_PLATFORM_IOS = "ios";

	public static final String DEFAULT_PAYMENT_DAYS_COUNT = "DEFAULT_PAYMENT_DAYS_COUNT";

	// Strings File ------------------------------------------------
	public static final String STRINGS_FILE_LOCATION = System.getProperty("config.dir") + File.separator
			+ "strings.properties";
	public static final String ADMIN_CONTEXTPATH = "context.path.admin";
	public static final String LASTLOCKINGPERIOD = "LASTLOCKINGPERIOD";
	public static final String BATCH_START_DATE = "batchStartDate";
	public static final String BATCH_END_DATE = "batchEndDate";
	public static final String NEW_SCHEDULE_START_DATE = "newScheduleStartDate";
	public static final String NEW_SCHEDULE_END_DATE = "newScheduleEndDate";

	public static final String TEST_ACCOUNT_ADDRESS_ID = "testAccountAddressId";

	public static final String ACCOUNT_LOCKING_HOUR = "accountLockingHour";

	public static final String ADMIN_TRANSACTIONS_RECORDS_COUNT = "adminTransactionsRecordsCount";
	public static String DEDUCTION_LIMIT = "deductionLimit";

	public static final String TEST_USER_MOBILE_NUMBER = "testUserMobileNumber";
	public static final String TEST_USER_ACCOUNT_ID = "testUserAccountId";

	public static final String IS_CHECKOUT_BLOCKED = "isCheckoutBlocked";
	public static final String NEW_CUSTOMER_REGISTRATIONS_NO_OF_DAYS_BEFORE_CURRENT = "newCustomersRegistrationNoOfDaysBeforeCurrent";


	public static final String ELEMENTS_PER_PAGE = "elementsPerPage";
	public static final String BATCH_MONTH_DURATION = "batchMonthDuration";
	
	public static final String ELEMENTS_PER_PAGE_CUSTOMER_TRANSACTIONS = "elementsPerPageCustomerTransactions";
	public static final String MAIDIN_ORDER_TEXT = "maidinOrderText";
	public static final String MAIDIN_MONEY_RECHARGE_TEXT = "maidinMoneyRechargeText";
	public static final String MAIDIN_DISCHARGE_ORDER_TEXT = "maidinDischargeOrderText";
	
	public static final String CUSTOMER_ORDER_SUMMARY_RECIEPT_EMAIL_TEMPLATE = "customer-order-summary-reciept.template";
	public static final String COMPANY_REGISTERED_OFFICE_ADDRESS = "companyRegisteredOfficeAddress";
	public static final String GSTIN = "gSTIN";
	public static final String LOGO_LOCATION = "logoLocation";
	public static final String MAIDIN_SITE_LINK = "maidinSiteLink";
	public static final String CUSTOMER_SUMMARY_RECIEPT_EMAIL_SUBJECT = "customer_summary_reciept_email_subject";
	public static final String IS_EMAIL_INVOICE_TO_CUSTOMER_ENABLED = "isEmailInvoiceToCustomerEnabled";
	public static final String IS_PAYMENTHISTORY_CUSTOMER_ENABLED = "isPaymentHistoryCustomerEnabled";
	public static final String CRON_EXPRESSION1 = "cronExpression1";
	public static final String PRODUCTDETAIL_30000 = "PRODUCTDETAIL_30000";
	public static final String PRODUCTDETAIL_60000 = "PRODUCTDETAIL_60000";
	public static final String PRODUCTDETAIL_100000 = "PRODUCTDETAIL_100000";
	public static final String PRODUCTDETAIL_130000 = "PRODUCTDETAIL_130000";
	public static final String PRODUCTDETAIL_160000 = "PRODUCTDETAIL_160000";
	public static final String PRODUCTDETAIL_210000 = "PRODUCTDETAIL_210000";
	public static final String PRODUCTDETAIL_250000 = "PRODUCTDETAIL_250000";
	
	public static final String SEARCH_SUGGESTIONS_RESULT_COUNT = "searchSuggestionsResultCount";
	
	// public static final String CURRENT_CGSTQUERY =
	// "TaxiFareDetails.getLastCGST";

}