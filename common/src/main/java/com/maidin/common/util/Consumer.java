package com.maidin.common.util;

public interface Consumer<T> {

	public void handle(T object);
}
