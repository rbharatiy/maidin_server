package com.maidin.common.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpStatus;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maidin.common.properties.ResourceProperties;

public class NotificationUtils {
	private static final Logger logger = LoggerFactory.getLogger(NotificationUtils.class);
	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	public static void sendPushNotificationToCustomerOnAndroid(JSONObject payload, String key)
			throws IOException, JSONException {
		int responseCode = -1;
		JSONObject responseBody = null;
		try {
			System.out.println("Sending FCM request");
			URL url = new URL(prop.getProperty(ResourceProperties.FCM_URL));
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");

			httpURLConnection.setRequestProperty("Authorization", "key=" + key);
			OutputStream out = httpURLConnection.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out, "utf-8");

			System.out.println("Payload : " + payload);
			outputStreamWriter.write(payload.toString());
			outputStreamWriter.flush();
			out.close();
			responseCode = httpURLConnection.getResponseCode();
			if (responseCode == HttpStatus.SC_OK) {
				responseBody = convertStreamToJSON(httpURLConnection.getInputStream());
				System.out.println("FCM message sent : " + responseBody);
			} else {
				responseBody = convertStreamToJSON(httpURLConnection.getErrorStream());
				System.out.println("FCM message Error Message : " + responseBody);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject convertStreamToJSON(InputStream inStream) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(inStream, Map.class);
		return new JSONObject(jsonMap);
	}
}
