package com.maidin.common.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import com.maidin.common.properties.ResourceProperties;
import com.google.common.base.Charsets;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;

public class CommonUtils {

	public static final String CONTENT_DISPOSITION = "Content-Disposition";
	public static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment;filename=";
	public static final String CONTENT_FILENAME = "Content-File-Name";
	
    public static Properties loadPropertyFile(String filename) throws IOException {
		Properties properties = new Properties();
		try (InputStream inputStream = CommonUtils.class.getClassLoader().getResourceAsStream(filename)) {
			properties.load(inputStream);
		} 
		return properties;
	}
    
    public static Properties loadProperties() {
		try {
			return CommonUtils.loadPropertyFile(new File(ResourceProperties.COMMON_FILE_LOCATION));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + ResourceProperties.COMMON_FILE_LOCATION);
		}
	}
    
    public static Properties loadPropertyFile(File file) throws IOException {
		Properties properties = new Properties();
		try (InputStream inputStream = new FileInputStream(file)) {
			properties.load(inputStream);
		} 
		return properties;
	}
    
    public static void readResourceLineByLine(String resourceName, Consumer<String> lineConsumer) throws IOException {
		URL resource = Resources.getResource(resourceName);
		CharSource charSource = Resources.asCharSource(resource, Charsets.UTF_8);
		try (BufferedReader br = charSource.openBufferedStream()) {
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				lineConsumer.handle(line);
			}
		}
	}
}
