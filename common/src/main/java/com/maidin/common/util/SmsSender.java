package com.maidin.common.util;

import java.io.File;
import java.net.URI;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maidin.common.properties.ResourceProperties;

public class SmsSender {
	private static Logger LOG = LoggerFactory.getLogger(SmsSender.class);

	private static final int DEFAULT_THREADPOOL_SIZE = 5;

	private static final Properties smsProperties = loadProperties();

	private static final String url = smsProperties.getProperty(ResourceProperties.SMS_URL);
	private static final String user = smsProperties.getProperty(ResourceProperties.SMS_USER);
	private static final String senderID = smsProperties.getProperty(ResourceProperties.SMS_SENDER_ID);
	private static final String state = smsProperties.getProperty(ResourceProperties.SMS_STATE);

	private static Properties loadProperties() {
		try {
			return CommonUtils.loadPropertyFile(new File(ResourceProperties.SMS_FILE_LOCATION));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + ResourceProperties.SMS_FILE_LOCATION);
		}
	}

	private static int getThreadPoolSize() {
		return DEFAULT_THREADPOOL_SIZE;
	}

	private static final ExecutorService executor = Executors.newFixedThreadPool(getThreadPoolSize());

	public static void sendVerifySMS(String mobileNumber, String message) {
		executor.submit(new Runnable() {

			@Override
			public void run() {
				try {
					URIBuilder uribuilder = new URIBuilder(url);
					uribuilder.addParameter("user", user);
					uribuilder.addParameter("senderID", senderID);
					uribuilder.addParameter("receipientno", mobileNumber);
					uribuilder.addParameter("msgtxt", message);
					uribuilder.addParameter("state", state);

					URI uri = uribuilder.build();
					LOG.info("Final Outboud API url " + uri);

					HttpGet httpget = new HttpGet(uri);
					HttpClient client = HttpClientBuilder.create().build();
					client.execute(httpget);
				} catch (Exception e) {
					LOG.error("Exception encountered while sending SMS", e);
					// TODO:cleanup
					throw new RuntimeException("Unknown issue while sending SMS");
				}

			}
		});
	}
	
	public static void sendCheckoutOrderSuccessSMS(String mobileNumber, String message) {
		executor.submit(new Runnable() {

			@Override
			public void run() {
				try {
					URIBuilder uribuilder = new URIBuilder(url);
					uribuilder.addParameter("user", user);
					uribuilder.addParameter("senderID", senderID);
					uribuilder.addParameter("receipientno", mobileNumber);
					uribuilder.addParameter("msgtxt", message);
					uribuilder.addParameter("state", state);

					URI uri = uribuilder.build();
					LOG.info("Final Outboud API url " + uri);

					HttpGet httpget = new HttpGet(uri);
					HttpClient client = HttpClientBuilder.create().build();
					client.execute(httpget);
				} catch (Exception e) {
					LOG.error("Exception encountered while sending SMS", e);
					// TODO:cleanup
					throw new RuntimeException("Unknown issue while sending SMS");
				}

			}
		});
	}
	
	public static void sendSchedulrOrderSuccessSMS(String mobileNumber, String message) {
		executor.submit(new Runnable() {

			@Override
			public void run() {
				try {
					URIBuilder uribuilder = new URIBuilder(url);
					uribuilder.addParameter("user", user);
					uribuilder.addParameter("senderID", senderID);
					uribuilder.addParameter("receipientno", mobileNumber);
					uribuilder.addParameter("msgtxt", message);
					uribuilder.addParameter("state", state);

					URI uri = uribuilder.build();
					LOG.info("Final Outboud API url " + uri);

					HttpGet httpget = new HttpGet(uri);
					HttpClient client = HttpClientBuilder.create().build();
					client.execute(httpget);
				} catch (Exception e) {
					LOG.error("Exception encountered while sending SMS", e);
					// TODO:cleanup
					throw new RuntimeException("Unknown issue while sending SMS");
				}

			}
		});
	}
}
