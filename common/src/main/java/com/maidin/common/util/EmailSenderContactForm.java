package com.maidin.common.util;


import java.io.File;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.maidin.common.properties.ResourceProperties;
import com.google.common.base.Charsets;
import com.google.common.io.BaseEncoding;

public class EmailSenderContactForm {
	private static Logger LOG = LoggerFactory.getLogger(EmailSenderContactForm.class);
	
	private static final int DEFAULT_THREADPOOL_SIZE = 5;
	private static final Charset EMAIL_PASSWORD_ENCODING = Charsets.UTF_8;

	private static final Properties smtpProperties = loadProperties();
	
	private static Properties loadProperties() {
		try {
			return CommonUtils.loadPropertyFile(new File(ResourceProperties.CONTACT_SMTP_FILE_LOCATION));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException ("Unable to load the properties file: " + ResourceProperties.CONTACT_SMTP_FILE_LOCATION);
		}
	}
	
	private static int getThreadPoolSize() {
		return DEFAULT_THREADPOOL_SIZE;
	}

	private static final ExecutorService executor = Executors.newFixedThreadPool(getThreadPoolSize());
	
	public static void sendMail(final String fromEmailId, final String subject, final String messageBody){
		executor.submit(new Runnable() {
			
			@Override
			public void run() {

				Properties mailProps = new Properties();

				// Setup mail server
				mailProps.put("mail.smtp.host", smtpProperties.getProperty(ResourceProperties.SMTP_HOST));
				mailProps.put("mail.smtp.port", smtpProperties.getProperty(ResourceProperties.SMTP_PORT));
				mailProps.put("mail.smtp.auth",	smtpProperties.getProperty(ResourceProperties.SMTP_AUTH, "true"));
				mailProps.put("mail.smtp.starttls.enable", smtpProperties.getProperty(ResourceProperties.SMTP_STARTTLS, "true"));
				System.out.println("Hurray I have sent the mail");
				//final String fromEmailAddress = smtpProperties.getProperty(ResourceProperties.SMTP_FROM);
				final String toEmailAddress = smtpProperties.getProperty(ResourceProperties.SMTP_TO);
				final String userName = smtpProperties.getProperty(ResourceProperties.SMTP_USERNAME);
				final String base64Password = smtpProperties.getProperty(ResourceProperties.SMTP_PASSWORD);
				final String password = new String(BaseEncoding.base64().decode(base64Password), EMAIL_PASSWORD_ENCODING);

				// Get the default Session object.
				Session session = Session.getInstance(mailProps,
						new javax.mail.Authenticator() {
							@Override
							protected PasswordAuthentication getPasswordAuthentication() {
								return new PasswordAuthentication(userName, password);
							}
						});

				Message message = new MimeMessage(session);
				try {
					message.setFrom(new InternetAddress(fromEmailId));
					message.setRecipients(Message.RecipientType.TO,
							InternetAddress.parse(toEmailAddress));
					message.setSubject(subject);
					message.setContent(messageBody, "text/html");
					Transport.send(message);
				} catch (Exception e) {
					//report the exception, nothing much to do here
					LOG.error("Exception sending the mail", e);
				}				
			}
		});		
	}
}
