package com.maidin.customerAuthenticate.exception;

import com.maidin.common.exception.UserException;

public class AuthenticationException extends UserException {
	private static final long serialVersionUID = 6702339420964932612L;

    public AuthenticationException(String code, String message) {
        super(code, message);
    }
}
