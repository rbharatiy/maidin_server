package com.maidin.customerAuthenticate;

import com.maidin.customerAuthenticate.exception.AuthenticationException;

public interface AuthenticationMgmt {

	String getCustomerMobile(Auth auth) throws AuthenticationException;

	public void destroy();

	public void removeAuthToken(String string);

	public String generateAuthToken(String mobileNumber);

	public String checkAccess(Auth auth) throws AuthenticationException;

	String getCustomerMobileForNewUser(Auth createAuth) throws AuthenticationException;

	// public String getAdminMobile(Auth createAuth) throws
	// AuthenticationException;

	void checkAppVersionAccess(String authToken) throws AuthenticationException;
}
