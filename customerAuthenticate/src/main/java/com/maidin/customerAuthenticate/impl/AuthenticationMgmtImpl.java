package com.maidin.customerAuthenticate.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.CacheRuntimeConfiguration;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;
import org.ehcache.xml.exceptions.XmlConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;

import com.maidin.customerAuthenticate.Auth;
import com.maidin.customerAuthenticate.AuthenticationMgmt;
import com.maidin.customerAuthenticate.exception.AuthenticationException;
import com.maidin.customerAuthenticate.properties.ResourceProperties;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;

/*import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;*/

public class AuthenticationMgmtImpl implements AuthenticationMgmt {

	/*private final CacheManager manager;
	private final Cache authTokenCache;*/
	private final Configuration xmlConfig;
	private final CacheManager manager;
	private final Cache<String, String> authTokenCache;
	private final CustomerDAO customerDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDeailsDao;

	@Autowired
	public AuthenticationMgmtImpl(CustomerDAO customerDAO, CustomerDeviceDetailsDAO customerDeviceDeailsDao) throws XmlConfigurationException, MalformedURLException {
		super();
		/*manager = CacheManager.newInstance(ResourceProperties.EHCACHE_XML_FILE_LOCATION);
		// managerAdmin =
		// CacheManager.newInstance(ResourceProperties.EHCACHE_ADMIN_XML_FILE_LOCATION);
		authTokenCache = manager.getCache("AuthToken");*/
		xmlConfig = new XmlConfiguration(Paths.get(ResourceProperties.EHCACHE_XML_FILE_LOCATION).toUri().toURL());
		manager =  CacheManagerBuilder.newCacheManager(xmlConfig);
		manager.init();
		authTokenCache = manager.getCache("authToken", String.class, String.class);
		this.customerDAO = customerDAO;
		this.customerDeviceDeailsDao = customerDeviceDeailsDao;

	}

	private String getDateAsString(long timeStamp) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm:ss , dd MMM yyyy, EEE", Locale.US);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(timeStamp);
		return dateFormat.format(calendar.getTimeInMillis());
	}

	@Override
	public String getCustomerMobile(Auth auth) throws AuthenticationException {
		/*Element cachedResult = authTokenCache.get(auth.getAuthToken());*/
		Object cachedResult = authTokenCache.get(auth.getAuthToken());
		List<Cache.Entry<String, String>> cacheList = StreamSupport.stream(authTokenCache.spliterator(), false)
				.collect(Collectors.toList());
		System.out.println("Entries list" + cacheList.size());
		/*for(Cache.Entry<String, String> entry : cacheList){
			System.out.println("AuthToken()  "+entry.getKey() +" || MobileNumber  "+entry.getValue());
		}
		CacheRuntimeConfiguration<String, String> cacheconfi  = authTokenCache.getRuntimeConfiguration();
		System.out.println("cacheconfi.getExpiry();"+cacheconfi.getExpiry());*/
		//System.out.println("cacheconfi.getResourcePools()" +cacheconfi.getResourcePools());
	
		
		/*
		 * try { System.out.println( auth.getAuthToken() + "\n" +
		 * "authTokenCache.getKeys().size():  " +
		 * authTokenCache.getKeys().size() + "\n" +
		 * "authTokenCache.getDiskStoreSize():  " +
		 * authTokenCache.getDiskStoreSize() + "\n" +
		 * "authTokenCache.calculateInMemorySize():  " +
		 * authTokenCache.calculateInMemorySize() + "\n" +
		 * "authTokenCache.calculateOffHeapSize():  " +
		 * authTokenCache.calculateOffHeapSize() + "\n" +
		 * "authTokenCache.calculateOnDiskSize():  " +
		 * authTokenCache.calculateOnDiskSize() + "\n" +
		 * "authTokenCache.getAverageGetTime():  " +
		 * authTokenCache.getAverageGetTime() + "\n" +
		 * "authTokenCache.getMemoryStoreSize():  " +
		 * authTokenCache.getMemoryStoreSize() + "\n" +
		 * "authTokenCache.getSize():  " + authTokenCache.getSize() + "\n" +
		 * "authTokenCache.hasAbortedSizeOf():  " +
		 * authTokenCache.hasAbortedSizeOf() + "\n" +
		 * "authTokenCache.isElementInMemory(auth.getAuthToken()):  " +
		 * authTokenCache.isElementInMemory(auth.getAuthToken()) + "\n" +
		 * "authTokenCache.isElementOffHeap(auth.getAuthToken()):  " +
		 * authTokenCache.isElementOffHeap(auth.getAuthToken()) + "\n" +
		 * "authTokenCache.isElementOnDisk(auth.getAuthToken()):  " +
		 * authTokenCache.isElementOnDisk(auth.getAuthToken()) + "\n" +
		 * "authTokenCache.get(auth.getAuthToken()).getCreationTime():  " +
		 * getDateAsString(authTokenCache.get(auth.getAuthToken()).
		 * getCreationTime()) + "\n"
		 * 
		 * 
		 * "authTokenCache.isReadLockedByCurrentThread(auth.getAuthToken()):  "
		 * +authTokenCache.isReadLockedByCurrentThread(auth.getAuthToken())+
		 * "\n"
		 * 
		 * ); } catch (Exception e) { e.printStackTrace(); }
		 */
		if (cachedResult == null) {
			// System.out.println("cachedResult == null " + (cachedResult == null) + "earlier authToken of customer is "+ auth.getAuthToken());
			 throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");
		} else {
			/*String mobileNumber = ((String) cachedResult.getObjectValue()).toString();*/
			String mobileNumber = (String) cachedResult.toString();
			storeAuthToken(auth.getAuthToken(), mobileNumber);
			// response.setMobileNumber(mobileNumber);
			CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobileNumber);

			if (customerDTO != null) {
				if (customerDTO.isActive()) {
					// response.setDriverId(driverDTO.getId());
				} else {
					throw new AuthenticationException("CUSTOMER_ID_IS_DEACTIVATED",
							"Customer Account is deactivated. Please contact support to continue using Maidin services");
				}

			} else if (customerDTO == null) {
				throw new AuthenticationException("CUSTOMER_DOES_NOT_EXIST", "Customer does not exist.");
			}
			return mobileNumber;
		}
	}

	/*
	 * @Override public String getCustomerMobile(Auth auth) throws
	 * AuthenticationException { Element cachedResult =
	 * authTokenCache.get(auth.getAuthToken());
	 * 
	 * if(cachedResult == null){ System.out.println(auth.getAuthToken()+
	 * "    ===="+(cachedResult == null)); throw new
	 * AuthenticationException("INVALID_AUTH_TOKEN",
	 * "Invalid authentication token."); } else{ String mobileNumber =
	 * ((String)cachedResult.getObjectValue()).toString();
	 * storeAuthToken(auth.getAuthToken(), mobileNumber);
	 * //response.setMobileNumber(mobileNumber); CustomerDTO customerDTO =
	 * customerDAO.getByMobileNumberWithoutActiveState(mobileNumber);
	 * if(customerDTO != null){ if(customerDTO.isActive()){
	 * //response.setDriverId(driverDTO.getId()); } else { throw new
	 * AuthenticationException("CUSTOMER_ID_IS_DEACTIVATED",
	 * "Customer Account is deactivated. Please contact support to continue using Maidin services"
	 * ); }
	 * 
	 * } else if(customerDTO == null){ throw new
	 * AuthenticationException("CUSTOMER_DOES_NOT_EXIST",
	 * "Customer does not exist."); } return mobileNumber; } }
	 */

	@Override
	public void removeAuthToken(String authToken) {
		checkNotNull(authToken);
		//System.out.println("authToken that is going to remove "+authToken+ "MobileNumber "+ authTokenCache.get(authToken));
		authTokenCache.remove(authToken);
		/*if(authTokenCache.containsKey(authToken)){
			System.out.println("OOPS, authToken does not removed "+ authToken);
		}
		else{
			System.out.println("YESSS, authToken  removed "+ authToken+ "MobileNumber "+ authTokenCache.get(authToken));
		}*/
	}

	public void storeAuthToken(String authToken, String mobileNumber) {
		authTokenCache.put(authToken, mobileNumber);
		//System.out.println("Going to store Key in Cache");
		/*if(authTokenCache.containsKey(authToken)){
			System.out.println("I am stored in cache "+authToken + "Mobile Number"+mobileNumber);
		}
		else{
			System.out.println("Failed to store in cache "+authToken + "Mobile Number"+mobileNumber);
			List<Cache.Entry<String, String>> cacheList = StreamSupport.stream(authTokenCache.spliterator(), false)
					.collect(Collectors.toList());
			System.out.println("Entries list" + cacheList.size());
			for(Cache.Entry<String, String> entry : cacheList){
				System.out.println("AuthToken()  "+entry.getKey() +" || MobileNumber  "+entry.getValue());
			}
			//authTokenCache.
		}*/
		//authTokenCache.containsKey(authToken);
		/*authTokenCache.put(new Element(authToken, mobileNumber));*/
		
	}

	@Override
	public void destroy() {
		System.out.println("Cache is destroyed");
		authTokenCache.clear();
		manager.close();
		/*authTokenCache.dispose();
		manager.shutdown();*/
	}

	@Override
	public String generateAuthToken(String mobileNumber) {
		String authToken = UUID.randomUUID().toString();
		storeAuthToken(authToken, mobileNumber);
		return authToken;

	}

	@Override
	public String checkAccess(Auth auth) throws AuthenticationException {
		// checkNotNull(auth);
		// checkNotNull(auth.getAuthToken());

		/*Element cachedResult = authTokenCache.get(auth.getAuthToken());*/
		Object cachedResult = authTokenCache.get(auth.getAuthToken());
		if (cachedResult == null) {
			// System.out.println("cachedResult == null " + (cachedResult == null) + "earlier authToken of customer is "+ auth.getAuthToken());
			throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");
		} else {
			/*String mobileNumber = ((String) cachedResult.getObjectValue()).toString();*/
			String mobileNumber = (String) cachedResult.toString();
			storeAuthToken(auth.getAuthToken(), mobileNumber);
			CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobileNumber);
			if (customerDTO != null) {
				if (customerDTO.isActive()) {
					// response.setDriverId(driverDTO.getId());
				} else {
					throw new AuthenticationException("CUSTOMER_ID_IS_DEACTIVATED",
							"Customer Account is deactivated. Please contact support to continue using Maidin services");
				}

			} else if (customerDTO == null) {
				throw new AuthenticationException("CUSTOMER_DOES_NOT_EXIST", "Customer does not exist.");
			}
			return mobileNumber;
		}
	}

	@Override
	public String getCustomerMobileForNewUser(Auth auth) throws AuthenticationException {
		/*Element cachedResult = authTokenCache.get(auth.getAuthToken());*/
		Object cachedResult = authTokenCache.get(auth.getAuthToken());
		if (cachedResult == null) {
			 //System.out.println("cachedResult == null " + (cachedResult == null) + "earlier authToken of customer for getCustomerMobileForNewUser "+ auth.getAuthToken());
			throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");
		} else {
			/*String mobileNumber = ((String) cachedResult.getObjectValue()).toString();*/
			String mobileNumber = (String) cachedResult.toString();
			storeAuthToken(auth.getAuthToken(), mobileNumber);
			// response.setMobileNumber(mobileNumber);
			CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobileNumber);
			if (customerDTO != null) {
				if (customerDTO.isActive()) {
					// response.setDriverId(driverDTO.getId());
				} else {
					throw new AuthenticationException("CUSTOMER_ID_IS_DEACTIVATED",
							"Customer Account is deactivated. Please contact support to continue using Maidin services");
				}

			}
			/*
			 * else if(customerDTO == null){ throw new
			 * AuthenticationException("CUSTOMER_DOES_NOT_EXIST",
			 * "Customer does not exist."); }
			 */
			return mobileNumber;
		}
	}

	private boolean checkAndroidAccess(String appVersion) {
		boolean isAllowed = true;
		List<String> appVersionList = new ArrayList<>();
		appVersionList.add("1.2.18.17");
		appVersionList.add("1.2.18.21");
		appVersionList.add("1.2.18.22");
		appVersionList.add("1.2.18.25");
		appVersionList.add("1.2.18.27");
		appVersionList.add("1.2.18.31");
		appVersionList.add("1.2.18.32");
		appVersionList.add("1.2.19.35");
		appVersionList.add("1.2.19.36");
		appVersionList.add("1.2.19.38");
		appVersionList.add("1.2.22.44");
		appVersionList.add("1.2.22.45");
		appVersionList.add("1.2.22.46");
		appVersionList.add("2.2.2.22");

		if (appVersionList.contains(appVersion)) {
			isAllowed = false;
		}
		return isAllowed;
	}

	@Override
	public void checkAppVersionAccess(String authToken) throws AuthenticationException {
		List<String> anroidAppVersionList = new ArrayList<>();
		anroidAppVersionList.add("1.2.18.17");
		anroidAppVersionList.add("1.2.18.21");
		anroidAppVersionList.add("1.2.18.22");
		anroidAppVersionList.add("1.2.18.25");
		anroidAppVersionList.add("1.2.18.27");
		anroidAppVersionList.add("1.2.18.31");
		anroidAppVersionList.add("1.2.18.32");
		anroidAppVersionList.add("1.2.19.35");
		anroidAppVersionList.add("1.2.19.36");
		anroidAppVersionList.add("1.2.19.38");
		anroidAppVersionList.add("1.2.22.44");
		anroidAppVersionList.add("1.2.22.45");
		anroidAppVersionList.add("1.2.22.46");
		anroidAppVersionList.add("2.2.2.22");

		List<String> iOSAppVersionList = new ArrayList();
		iOSAppVersionList.add("1.1.13");
		iOSAppVersionList.add("1.1.16");
		iOSAppVersionList.add("1.1.18");
		iOSAppVersionList.add("1.1.22");
		iOSAppVersionList.add("1.1.24");
		iOSAppVersionList.add("1.1.25");
		iOSAppVersionList.add("1.1.27");

		/*Element cachedResult = authTokenCache.get(authToken);
		String mobileNumber = ((String) cachedResult.getObjectValue()).toString();
		Object cachedResult = authTokenCache.get(authToken);
		String mobileNumber = (String) cachedResult.toString();
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(mobileNumber);
		CustomerDeviceDetailsDTO customerDeviceDetailsDTO = customerDeviceDeailsDao
				.getDeviceDetailsByCustomerId(customerDTO.getId());
		String appVersion = customerDeviceDetailsDTO.getAppVersion();
		String appPlatform = customerDeviceDetailsDTO.getDevicePlatform();
		if (appPlatform.equalsIgnoreCase("android") && anroidAppVersionList.contains(appVersion)) {
			throw new AuthenticationException("APP_VERSION_EXPIRED",
					"This version of app is not supported anymore. Please update your app from playstore.");
		} else if (appPlatform.equalsIgnoreCase("iOS") && iOSAppVersionList.contains(appVersion)) {
			throw new AuthenticationException("APP_VERSION_EXPIRED",
					"This version of app is not supported anymore. Please update your app from playstore.");
		} else {
			// do nothing
		}*/
		return;
	}
}
