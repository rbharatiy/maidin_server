-- drop database maidin; 
-- create database maidin;
-- use maidin;
 create schema maidin;use maidin;
 


DROP TABLE IF EXISTS Customer;
CREATE TABLE Customer (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  MobileNumber varchar(20) NOT NULL UNIQUE,
  Email varchar(50) ,
  emailVerified boolean default FALSE,
  FullName varchar(50),
  Gender varchar (10),
  AddressId bigint,
  CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  Active bit
);

DROP TABLE IF EXISTS Address;
CREATE TABLE Address (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  FlatId bigint,
  City varchar(50),
  CityCode varchar(15),
  Project varchar(150),
  Tower varchar(20),
  Flat varchar(10),
  CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UpdatedBy bigint
  
);

DROP TABLE IF EXISTS Admin;
CREATE TABLE Admin (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  MobileNumber varchar(20) NOT NULL UNIQUE,
  Email varchar(50) UNIQUE,
  EmailVerified boolean default FALSE,
  FullName varchar(50),
  Gender varchar (10),
  City varchar (10),
  Project varchar (10),
  CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  Active bit default FALSE
);

DROP TABLE IF EXISTS Admin_Role;
CREATE TABLE Admin_Role (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  AdminId bigint,
  RoleId int 
  
);

DROP TABLE IF EXISTS Role;
CREATE TABLE Role (
  Id int AUTO_INCREMENT PRIMARY KEY,
  Role varchar(100) 
);

DROP TABLE IF EXISTS RolePermission;
CREATE TABLE RolePermission (
  Id int AUTO_INCREMENT PRIMARY KEY,
  RoleId varchar(100) 
);

DROP TABLE IF EXISTS Permissions;
CREATE TABLE Permissions (
  Id int AUTO_INCREMENT PRIMARY KEY,
  Permission varchar(100) 
);


DROP TABLE IF EXISTS AdminDeviceDetails;
CREATE TABLE AdminDeviceDetails (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  AdminId varchar(10) UNIQUE,
  AppToken varchar(200),
  DeviceToken varchar(500) ,
  DeviceModel varchar(200),
  DevicePlatform varchar (50),
  DevicePlatformVersion varchar(10),
  AppVersion varchar (20),
  UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);



DROP TABLE IF EXISTS AdminAuthentication;
CREATE TABLE AdminAuthentication (
Id bigint AUTO_INCREMENT PRIMARY KEY,
AdminId varchar(10),
MobileNumber varchar(10),
AuthToken varchar(100)
);

DROP TABLE IF EXISTS OTPVerification;
CREATE TABLE OTPVerification (
  MobileNumber varchar(20),
  Otp varchar(6),
  GeneratedOn timestamp,
  UserCategory varchar(20) ,
  primary key (MobileNumber, UserCategory)
);


 DROP TABLE IF EXISTS CustomerDeviceDetails;
CREATE TABLE CustomerDeviceDetails (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  CustomerId varchar(10) UNIQUE,
  appToken varchar(200),
  DeviceToken varchar(500) ,
  DeviceModel varchar(200),
  DevicePlatform varchar (50),
  DevicePlatformVersion varchar(10),
  appVersion varchar (20),
  UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);



DROP TABLE IF EXISTS CustomerAuthentication;
CREATE TABLE CustomerAuthentication (
Id bigint AUTO_INCREMENT PRIMARY KEY,
CustomerId varchar(10),
MobileNumber varchar(10),
AuthToken varchar(100)
);


DROP TABLE IF EXISTS ProductCategory;
CREATE TABLE IF NOT EXISTS ProductCategory (
         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         Category  		varchar(50)		NOT NULL UNIQUE,
         Priority  		int,
         Description  		varchar(200),
         Active 		bit default TRUE,
         stockStatus boolean default TRUE,
         updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         updatedBy bigint
);

DROP TABLE IF EXISTS ProductBrands;
CREATE TABLE IF NOT EXISTS ProductBrands (
         Id    				int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         brand		varchar(50)		NOT NULL ,
         categoryId int	NOT NULL,
         active bit default TRUE ,
         stockStatus boolean default TRUE,
         updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         updatedBy bigint
--         FOREIGN KEY (categoryId)
--			REFERENCES ProductCategory(Id)
--			ON UPDATE CASCADE ON DELETE RESTRICT
);

DROP TABLE IF EXISTS Product;
CREATE TABLE IF NOT EXISTS Product (
         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         productCode  	varchar(10)		DEFAULT NULL UNIQUE,
         productName	varchar(50)		NOT NULL DEFAULT '' ,
         quantityUnit	varchar(10)		NOT NULL,	
         price   DECIMAL(7,2)  	NOT NULL DEFAULT 0.00,
         brandId	int			NOT NULL,
         active bit default TRUE,
         stockStatus boolean default TRUE,
         updatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         updatedBy bigint

--         FOREIGN KEY (brandId)
--			REFERENCES ProductBrands(Id)
--			ON UPDATE CASCADE ON DELETE RESTRICT,
--		 FOREIGN KEY (categoryId)
--			REFERENCES ProductCategory(Id)
--			ON UPDATE CASCADE ON DELETE RESTRICT	
--			
);

DROP TABLE IF EXISTS Images;
CREATE TABLE Images (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  ImageCategory varchar(100),
  ImageCategoryId bigint,
  FileName varchar(128) DEFAULT NULL,
  Data longblob,
  ContentType varchar(20),
  FileSize bigint,
  UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UpdatedBy bigint
);

DROP TABLE IF EXISTS Cart;
CREATE TABLE Cart (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  CustomerId bigint,
  ProductId varchar(100),
  Quantity int,
  TimeSlotId bigint,
  orderedTime timestamp,
  DeliveryTime timestamp
 
  
);

DROP TABLE IF EXISTS OrderInfo;
CREATE TABLE OrderInfo (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  CustomerId bigint,
  TimeSlot varchar(100),
  PaymentModeId int,
  InvoiceNo varchar(50),
  OrderedTime timestamp,
  OrderSubTotal float(7,2) DEFAULT 0.00,
  DeliveryDate date,
  SpecialInstructions varchar(100),
  ShippingId bigint,
  Cancelled boolean default FALSE,
  CancellationTime timestamp NULL DEFAULT NULL, -- ON UPDATE CURRENT_TIMESTAMP,
  OverallOrderStatus varchar(100),
  OverallPaymentStatus varchar(100),
  ScheduleId bigint,
  UpdatedOn timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
  
 );
 
 
DROP TABLE IF EXISTS OrderDetailsInfo;
 CREATE TABLE OrderDetailsInfo (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  OrderId bigint,
  ProductId int,
  Quantity int,
  OrderedTime timestamp,
  Cancelled boolean default FALSE,
  OrderCancellationTime timestamp,
  productMRP float(7,2) DEFAULT 0.00,
  TaxAmount float(7,2) DEFAULT 0.00,
  deliveryCharges float(7,2) DEFAULT 0.00,
  DiscountAmount float(7,2) DEFAULT 0.00,
  -- CompanyBenefitAmount float(7,2) DEFAULT 0.00,
  totalProductCost float(7,2) DEFAULT 0.00,
  AmountPaidByCustomer float(7,2) DEFAULT 0.00 ,
  PaymentStatus varchar(100),
  OrderStatus varchar(100),
  UpdatedOn timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
 
  
);
 

DROP TABLE IF EXISTS Wallet;
CREATE TABLE IF NOT EXISTS Wallet (
        Id    	bigint AUTO_INCREMENT PRIMARY KEY,
        CustomerId  varchar(100),
        MyWalletBalance float(7,2) DEFAULT 0.00,
        UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
        
);




DROP TABLE IF EXISTS CustomerTransaction;
CREATE TABLE IF NOT EXISTS CustomerTransaction (
        Id    	bigint AUTO_INCREMENT PRIMARY KEY,
        CustomerId  varchar(100),
        orderDetailsId bigint,
        PaymentModeId int,
        MoneyIn float(7,2) DEFAULT 0.00,
        MoneyOut float(7,2) DEFAULT 0.00,
        TransactionDescription varchar(200),-- Wallet Recharge| Goods Purchase| any debit| any Credit
        TransactionDate timestamp,
        UpdatedBy bigint
        
);


DROP TABLE IF EXISTS OrderPayment;
CREATE TABLE IF NOT EXISTS OrderPayment (
        Id    	bigint AUTO_INCREMENT PRIMARY KEY,
        CustomerId  varchar(100),
        OrderId bigint ,
        PaymentModeId int ,
        ProductCost float(7,2) DEFAULT 0.00,
        TotalPaid float(7,2)  DEFAULT 0.00,
        PaymentStatus varchar(50),
        TransactionDescription varchar(200), -- Wallet Recharge| Goods Purchase| any debit| any Credit
        TransactionDate timestamp
        
);


DROP TABLE IF EXISTS PaymentMode;
CREATE TABLE IF NOT EXISTS PaymentMode (
        Id    	int AUTO_INCREMENT PRIMARY KEY,
        PaymentMode  varchar(100),
        PaymentOption  varchar(100),
        Priority  int,
        DisplayedInApp boolean,
        CreatedOn timestamp,
        CreatedBy bigint,
        UpdatedOn timestamp,
        UpdatedBy bigint
        
        
);

DROP TABLE IF EXISTS TimeSlot;
CREATE TABLE IF NOT EXISTS TimeSlot (
        Id    	int AUTO_INCREMENT PRIMARY KEY,
        TimeSlot  varchar(100),
        StartTime int,
        EndTime int,
        LockingHours int,
        DisplayedInApp boolean,
        Priority int
);

DROP TABLE IF EXISTS ScheduleInfo;
CREATE TABLE IF NOT EXISTS ScheduleInfo (
        Id    	bigint AUTO_INCREMENT PRIMARY KEY,
        CustomerId  bigint,
        TimeSlotId int,
        ProductId int,
        Quantity int,
        StartDate date,
        PauseDate timestamp,
        Su boolean,
        Mo boolean,
        Tu boolean,
        We boolean,
        Th boolean,
        Fr boolean,
        Sa boolean,
        ScheduleStatus int,
        CreatedOn timestamp,
        UpdatedOn timestamp,
        Recurring boolean,
        ScheduledUpto date,
        Active bit default FALSE,
        scheduleEndDate date,
        Version varchar(10)
);



-- added by Mudit
DROP TABLE IF EXISTS Photo;
CREATE TABLE Photo (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  itemId bigint NOT NULL,
  imageType int NOT NULL,
  fileStoreId bigint NULL,
  primaryPhoto boolean,
  CreatedOn timestamp,
  CreatedBy bigint,
  UpdatedOn timestamp NULL,
  UpdatedBy bigint,
  Active BIT,
  Deleted BIT
);

-- added by Mudit
DROP TABLE IF EXISTS FileStore;
CREATE TABLE FileStore (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  FileName varchar(200) NOT NULL,
  Encrypted bit NOT NULL,
  ContentType varchar(200) NOT NULL,
  FileSize bigint NOT NULL,
  Md5 varchar(200) NOT NULL,
  CreatedOn timestamp,
  CreatedBy bigint,
  UpdatedOn timestamp NULL,
  UpdatedBy bigint,
  Active BIT,
  Deleted BIT
);

-- added by Mudit
DROP TABLE IF EXISTS PhotoType;
CREATE TABLE `PhotoType` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `photoType` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`)
);
INSERT INTO `PhotoType` VALUES (1, 'PRODUCT'), (2, 'CATEGORY'), (3, 'BRAND');

-- Table data modifications,  BY MUDIT
-- changes in orderdetailsinfo, product, admin for getting transaction history in admin
INSERT INTO `maidin`.`OrderDetailsInfo` (`Id`, `OrderId`, `ProductId`, `Quantity`, `Cancelled`, `productMRP`, `TaxAmount`, `deliveryCharges`, `DiscountAmount`, `totalProductCost`, `AmountPaidByCustomer`, `UpdatedOn`) VALUES ('-1', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2018-07-19 12:19:47');
INSERT INTO `maidin`.`OrderDetailsInfo` (`Id`, `OrderId`, `ProductId`, `Quantity`, `Cancelled`, `productMRP`, `TaxAmount`, `deliveryCharges`, `DiscountAmount`, `totalProductCost`, `AmountPaidByCustomer`, `UpdatedOn`) VALUES ('0', '0', '0', '0', '0', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '2018-07-19 12:19:47');
INSERT INTO `maidin`.`Admin` (`Id`, `MobileNumber`, `Email`, `EmailVerified`, `FullName`, `Gender`, `CreatedOn`, `Active`) VALUES ('-1', '1234567890', 'SYSTEM', '0', 'SYSTEM', 'Male', '2018-03-31 16:00:02', '1');
INSERT INTO `maidin`.`Product` (`Id`, `productName`, `quantityUnit`, `price`, `active`, `stockStatus`) VALUES ('0', ' ', ' ', '0.00', '1', '1');
update maidin.CustomerTransaction set orderDetailsId = -1 where id < 50000 and orderDetailsId is null;

-- Add columns networkCarrier and networkType in customerdevicedetails
ALTER TABLE maidin.CustomerDeviceDetails ADD COLUMN networkCarrier varchar(50), ADD COLUMN networkType varchar(50);

-- Add PreBalance,DueAmount to CustomerTransaction
ALTER TABLE `maidin`.`CustomerTransaction` 
ADD COLUMN `PreBalance` FLOAT(7,2) NULL DEFAULT 0 AFTER `PaymentModeId`,
ADD COLUMN `DueAmount` FLOAT(7,2) NULL DEFAULT 0 AFTER `MoneyOut`;

-- Add PaymentModeId to OrderDetailsInfo
ALTER TABLE `maidin`.`OrderDetailsInfo` 
ADD COLUMN `PaymentModeId` INT(11) NULL DEFAULT 0 AFTER `OrderStatus`;

<<<<<<< HEAD
-- added by Mudit
-- set table default character set to utf-8
DROP TABLE IF EXISTS Notification;
CREATE TABLE Notification (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  title varchar(2000),
  message varchar(2000),
  successCount bigint,
  failureCount bigint,
  adminId bigint,
  notificationType int NOT NULL,
  CreatedOn timestamp,
  UpdatedOn timestamp
);

DROP TABLE IF EXISTS BlockedSlotInfo;
CREATE TABLE BlockedSlotInfo (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  BlockedDate date,
  TimeSlotIds varchar(200),
  DateFormatter varchar(50),
  Active bit,
  CreatedOn timestamp,
  UpdatedOn timestamp,
  CreatedBy bigint(20),
  UpdatedBy bigint(20)
);
=======
------------------------------------------ V2 addition in DB---------------------------------------------------------------

--DROP TABLE IF EXISTS Product;
--CREATE TABLE IF NOT EXISTS Product (
--         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         ProductCode  		varchar(50)		NOT NULL UNIQUE,
--         ProductName  		varchar(50),
--         Description  		varchar(50),
--         Active 		bit default TRUE,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);
--
--DROP TABLE IF EXISTS CategoryCity;
--CREATE TABLE IF NOT EXISTS CategoryCity (
--         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         CategoryId  		varchar(50)		NOT NULL UNIQUE,
--         CityId  		int,
--         Active 		bit default TRUE,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);
--
--DROP TABLE IF EXISTS BrandCity;
--CREATE TABLE IF NOT EXISTS BrandCity (
--         Id    				int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         BrandId		varchar(50)		NOT NULL ,
--         CityId int	NOT NULL,
--         Active bit default TRUE ,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);
--
--DROP TABLE IF EXISTS ProductCity;
--CREATE TABLE IF NOT EXISTS ProductCity (
--         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         ProductId  	varchar(10)		DEFAULT NULL UNIQUE,
--         CityId	varchar(50)		NOT NULL DEFAULT '' ,
--         QuantityUnit	varchar(10)		NOT NULL,	
--         MRP   DECIMAL(7,2)  	NOT NULL DEFAULT 0.00,
--         SalesPrice	int			NOT NULL,
--         DiscountUnit bit default TRUE,
--         Discount boolean default TRUE,
--         DeliveryCharge boolean default TRUE,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);

DROP TABLE IF EXISTS CustomerAddress;
CREATE TABLE IF NOT EXISTS CustomerAddress (
         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         CustomerId  	varchar(10)		DEFAULT NULL UNIQUE,
         AddressKey	varchar(50)		NOT NULL DEFAULT '' ,
         AddressId	varchar(10)		NOT NULL
        
);

 
DROP TABLE IF EXISTS OrderDetailsInfo;
 CREATE TABLE OrderDetailsInfo (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  OrderId bigint,
  ProductId int,
  Quantity int,
  OrderedTime timestamp,
  Cancelled boolean default FALSE,
  OrderCancellationTime timestamp,
  ProductName varchar(200),-- Added
  productMRP float(7,2) DEFAULT 0.00,
  BarCode varchar(50),
  SalePrice float(7,2) DEFAULT 0.00, -- Added
  TaxAmount float(7,2) DEFAULT 0.00,
  DeliveryCharges float(7,2) DEFAULT 0.00,
  DiscountAmount float(7,2) DEFAULT 0.00,
  TotalProductCost float(7,2) DEFAULT 0.00,
  AmountPaidByCustomer float(7,2) DEFAULT 0.00 ,
  PaymentStatus varchar(100),
  OrderStatus varchar(100),
  PaymentModeId  INT(11) NULL DEFAULT 0,
  UpdatedOn timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
 
  
);

----------------------------------------V2 New Tables--------------------------------------------------------

DROP TABLE IF EXISTS CategoryInfoV2;
CREATE TABLE IF NOT EXISTS CategoryInfoV2 (
         Id	int  NOT NULL AUTO_INCREMENT PRIMARY KEY,
         Category	varchar(50)		NOT NULL ,
         Description varchar(200),
         Active bit default TRUE ,
         Priority int default 1, 
         StockStatus bit default TRUE ,
         timeSlotIds varchar(100),
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);


DROP TABLE IF EXISTS SubCategoryInfoV2;
CREATE TABLE IF NOT EXISTS SubCategoryInfoV2 (
         Id	int  NOT NULL AUTO_INCREMENT PRIMARY KEY,
         SubCategory	varchar(50)		NOT NULL ,
         Active bit default TRUE ,
         StockStatus bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);

DROP TABLE IF EXISTS CityCategoryInfo;
CREATE TABLE IF NOT EXISTS CityCategoryInfo (
         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         CityCode	varchar(10)		DEFAULT NULL UNIQUE,
         GeoLocId   varchar(6)	    DEFAULT NULL,
         CityName	varchar(50)		NOT NULL DEFAULT '' ,
         StateName varchar(50),
         Country   varchar(50),
         CategoryIds	varchar(200)	,
         SubCategoryIds varchar(200),
         Active bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
         
        
);



DROP TABLE IF EXISTS BrandInfoV2;
CREATE TABLE IF NOT EXISTS BrandInfoV2 (
         Id	int  NOT NULL AUTO_INCREMENT PRIMARY KEY,
         Brand	varchar(50)		NOT NULL ,
         Active bit default TRUE ,
         stockStatus bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);


DROP TABLE IF EXISTS ProductInfoV2;
CREATE TABLE IF NOT EXISTS ProductInfoV2 (
         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         ProductCode  	varchar(50)	 UNIQUE,
         ProductName  	varchar(50),
         Description  	varchar(200),
         BrandId int,
         Priority int,
         FoodType int,
         Active bit default TRUE ,
         StockStatus bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);

--DROP TABLE IF EXISTS ParentProductV2;
--CREATE TABLE IF NOT EXISTS ProductInfoV2 (
--         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         ProductCode varchar(50),
--         ProductName  	varchar(50),
--         Description  	varchar(200),
--         BrandId int,
--         Priority int,
--         FoodType int,
--         Active bit default TRUE ,
--         StockStatus bit default TRUE ,
--         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
--         CreatedBy bigint,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);
--DROP TABLE IF EXISTS ProductsV2;
--CREATE TABLE IF NOT EXISTS ProductsV2 (
--         Id    			int  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         BarCode  	varchar(50)	 UNIQUE,
--         ParentProductId int,
--         QuantityUnit  	varchar(200),
--         Priority int,
--         Active bit default TRUE ,
--         StockStatus bit default TRUE ,
--         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
--         CreatedBy bigint,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);
--
--DROP TABLE IF EXISTS ProductCityInfoV2;
--CREATE TABLE IF NOT EXISTS ProductCityInfoV2 (
--         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
--         ProductId  	int		DEFAULT NULL ,
--         SubCategoryId  int,
--         CategoryId int,
--         CityCode	varchar(50)		NOT NULL DEFAULT '' ,
--         -- QuantityUnit	varchar(50)	 NOT NULL,
--         Priority int,
--         MRP  DECIMAL(7,2)  NOT NULL DEFAULT 0.00,
--         SalePrice	DECIMAL(7,2) NOT NULL DEFAULT 0.00,
--         SavedAmount	DECIMAL(7,2) NOT NULL DEFAULT 0.00,
--         DiscountUnit varchar(50) default NULL,
--         Discount DECIMAL(7,2) NOT NULL DEFAULT 0.00,
--         DeliveryCharge DECIMAL(7,2) NOT NULL DEFAULT 0.00,
--         StockQuantity int,
--         SetByDefault bit default TRUE ,
--         Active bit default TRUE ,
--         StockStatus bit default TRUE ,
--         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
--         CreatedBy bigint,
--         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
--         UpdatedBy bigint
--);

DROP TABLE IF EXISTS ProductCityInfoV2;
CREATE TABLE IF NOT EXISTS ProductCityInfoV2 (
         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         ProductId  	int		DEFAULT NULL ,
         ProductIdV1  	int	,
         SubCategoryId  int,
         CategoryId int,
         CityCode	varchar(50)		NOT NULL DEFAULT '' ,5
         QuantityUnit	varchar(50)	 NOT NULL,
         Priority int,
         MRP  DECIMAL(7,2)  NOT NULL DEFAULT 0.00,
         SalePrice	DECIMAL(7,2) NOT NULL DEFAULT 0.00,
         SavedAmount	DECIMAL(7,2) NOT NULL DEFAULT 0.00,10
         DiscountUnit varchar(50) default NULL,
         Discount DECIMAL(7,2) NOT NULL DEFAULT 0.00,
         DeliveryCharge DECIMAL(7,2) NOT NULL DEFAULT 0.00,
         SetByDefault bit default TRUE ,
         Active bit default TRUE ,
         StockStatus bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint,
         StockQuantity int,
         BarCode varchar(50),
         ProductDetailGroupId int,
         PhotoGroup varchar(45),
         PhotoId bigint
);

DROP TABLE IF EXISTS ProductImagesV2;
CREATE TABLE IF NOT EXISTS ProductImagesV2 (
         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         ProductId  	int		DEFAULT NULL ,
         productDetailIds  varchar(50) DEFAULT NULL ,
         PrimaryPhoto	varchar(50)	 NOT NULL,
         FileStoreId int,
         Active  DECIMAL(7,2)  NOT NULL DEFAULT 0.00,
         Deleted	DECIMAL(7,2) NOT NULL DEFAULT 0.00,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);

DROP TABLE IF EXISTS ProductDetailGroup;
CREATE TABLE IF NOT EXISTS ProductDetailGroup (
         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         productDetailIds  	int		DEFAULT NULL ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);


ALTER TABLE `maidin`.`Address` 
ADD COLUMN `CityCode` VARCHAR(15) NULL DEFAULT NULL AFTER `Flat`;

UPDATE maidin.Address p SET p.cityCode = 100003  WHERE city= "Noida"
UPDATE maidin.Address p SET p.cityCode = 100002  WHERE city= "Crossing Republic"
UPDATE maidin.Address p SET p.cityCode = 100001  WHERE city= "Greater Noida (W)"


ALTER TABLE `maidin`.`OrderDetailsInfo` 
ADD COLUMN `ProductName` VARCHAR(150) NULL AFTER `OrderCancellationTime`,
ADD COLUMN `ProductSalePrice` FLOAT(7,2) NULL DEFAULT 0 AFTER `productMRP`,
ADD COLUMN `BarCode` VARCHAR(45) NULL AFTER `UpdatedOn`;

ALTER TABLE `maidin`.`OrderInfo` 
ADD COLUMN `Version` VARCHAR(10) NULL AFTER `ScheduleId`;

ALTER TABLE `maidin`.`ScheduleInfo` 
ADD COLUMN `Version` VARCHAR(10) NULL AFTER `scheduleEndDate`;

ALTER TABLE `maidin`.`ProductCityInfoV2` 
ADD COLUMN `CategoryId` INT(5) NULL DEFAULT 0 AFTER `StockQuantity`;

ALTER TABLE `maidin`.`TimeSlot` 
ADD COLUMN `DisplayedInApp` TINYINT(1) NULL DEFAULT 0 AFTER `BatchUpdateTime`,
ADD COLUMN `Priority` INT(3) NULL DEFAULT 1 AFTER `DisplayedInApp`;


DROP TABLE IF EXISTS UpdateInfo;
CREATE TABLE IF NOT EXISTS UpdateInfo (
         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         ParameterText  varchar(50),
         UpdatedOn  	TIMESTAMP	
);


DROP TABLE IF EXISTS ErrorInfo;
CREATE TABLE IF NOT EXISTS ErrorInfo (
         Id    			bigint  		NOT NULL AUTO_INCREMENT PRIMARY KEY,
         ErrorCode  	int ,
         Message  	varchar(100) ,
         Active bit default TRUE ,
         CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
         CreatedBy bigint,
         UpdatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
         UpdatedBy bigint
);
-- UpdateInfo table Start ---
-- Import Prod data of UpdateInfo from Prod to DEV
INSERT INTO `maidin`.`UpdateInfo` (`Id`, `ParameterText`, `UpdatedOn`) VALUES 
('1', 'TimeSlot', '2018-09-19 13:44:16')
('2', 'PaymentMode', '2018-09-19 13:44:16');
-- UpdateInfo table End ---


-- OrderInfo table Start ---
ALTER TABLE `maidin`.`OrderInfo` 
ADD COLUMN `DeliveryLocation` VARCHAR(150) NULL AFTER `UpdatedOn`;

ALTER TABLE `maidin`.`orderinfo` 
ADD COLUMN `TimeSlotId` INT(4) NULL AFTER `DeliveryLocation`;

UPDATE OrderInfo o, TimeSlot t set o.TimeSlotId = t.Id where t.timeSlot = o.timeSlot;

ALTER TABLE `maidin`.`OrderInfo` 
ADD COLUMN `AddressId` BIGINT(20) NULL AFTER `TimeSlotId`;

UPDATE OrderInfo o, Address a, Customer c  set o.FlatId = a.FlatId where o.customerId = c.Id and  a.Id = c.addressId  

UPDATE OrderInfo o  set o.DeliveryLocation = 
( Select  concat('Flat no.',a.Flat,', ', a.tower,', ',a.Project,', ',a.City)   from Address a, Customer c where a.Id = c.addressId and 
o.customerId = c.Id)
-- OrderInfo table End ---


ALTER TABLE `maidin`.`Photo` 
CHANGE COLUMN `itemId` `itemId` BIGINT(20) NULL,
CHANGE COLUMN `imageType` `imageType` INT(11) NULL ;

-- OrderDetailsInfo table Start ---
 UPDATE OrderDetailsInfo o, ProductCityInfoV2 pc  set o.ProductId = pc.Id where o.ProductId = pc.ProductIdV1 
 update OrderDetailsInfo od set od.productSalePrice = od.ProductMRP 
UPDATE ScheduleInfo s, ProductCityInfoV2 pc  set s.ProductId= pc.Id where s.ProductId = pc.ProductIdV1
-- OrderDetailsInfo table End ---


-- CustomerTransaction table Start ---
ALTER TABLE `maidin`.`CustomerTransaction` 
ADD COLUMN `OrderId` BIGINT(20) NULL AFTER `UpdatedBy`;

 Update CustomerTransaction c, OrderDetailsInfo o set c.orderId = o.orderId where c.orderDetailsId = o.Id
 -- CustomerTransaction table End ---


-- CategoryInfoV2 table Start ---
ALTER TABLE `maidin`.`CategoryInfoV2` 
ADD COLUMN `PhotoId` BIGINT(20) NULL AFTER `TodaySlotIds`;

ALTER TABLE `maidin`.`categoryinfov2` 
ADD COLUMN `TodaySlotIds` VARCHAR(100) NULL AFTER `TimeSlotIds`;
-- CategoryInfoV2 table End ---

-- PaymentMode table Start ---

ALTER TABLE `maidin`.`PaymentMode` 
ADD COLUMN `PaymentOption` VARCHAR(100) NULL AFTER `PaymentMode`,
ADD COLUMN `Priority` INT(5) NULL AFTER `PaymentOption`,
ADD COLUMN `DisplayedInApp` BIT(1) NULL DEFAULT 0b0 AFTER `Priority`,
ADD COLUMN `Active` BIT(1) NULL DEFAULT 0b1 AFTER `DisplayedInApp`;

ALTER TABLE `maidin`.`PaymentMode` 
ADD COLUMN `PhotoId` BIGINT(20) NULL AFTER `Active`;

ALTER TABLE `maidin`.`PaymentMode` 
ADD COLUMN `CreatedOn` TIMESTAMP NULL AFTER `PhotoId`,
ADD COLUMN `CreatedBy` BIGINT(20) NULL AFTER `CreatedOn`,
ADD COLUMN `UpdatedOn` TIMESTAMP NULL AFTER `CreatedBy`,
ADD COLUMN `UpdatedBy` BIGINT(20) NULL AFTER `UpdatedOn`;
-- PaymentMode table End ---



-- ProductCityInfoV2 table Start ---
ALTER TABLE `maidin`.`ProductCityInfoV2` 
ADD COLUMN `ProductDetailGroupId` INT(7) NULL AFTER `BarCode`,
ADD COLUMN `PhotoGroup` VARCHAR(45) NULL AFTER `ProductDetailGroupId`;

ALTER TABLE `maidin`.`ProductCityInfoV2` 
ADD COLUMN `PhotoId` BIGINT(20) NULL AFTER `PhotoGroup`;
-- ProductCityInfoV2 table End ---







ALTER TABLE `maidin`.`Photo` 
CHANGE COLUMN `ProductDetailIds` `ProductDetailId` BIGINT(20) NULL DEFAULT NULL ;
Update  maidin.photo set version = "v1" where version is null;
Update maidin.Photo set ProductDetailId = itemId;
Update  maidin.Photo p , ProductCityInfoV2 pc set p.itemId = pc.Id where  imageType =1 and p.itemId = pc.ProductIdV1 ;
Update  maidin.ProductCityInfoV2 pc, Photo p set pc.PhotoId = p.Id where p.imageType =1 and p.ItemId = pc.Id  ;
Update photo p,subcategoryinfov2 s set p.itemId = s.Id where s.brandIdV1= p.itemId and imageType = 3 and version="v1";

-- ProductInfoV2 table Start ---
ALTER TABLE `maidin`.`ProductInfoV2` 
CHANGE COLUMN `ProductName` `ProductName` VARCHAR(200) NULL DEFAULT NULL ;
-- ProductInfoV2 table End ---




ALTER TABLE `maidin`.`SubCategoryInfoV2` 
ADD COLUMN `BrandIdV1` INT(5) NULL AFTER `UpdatedBy`;

Update subcategoryinfov2 s, productbrands b set s.brandIdV1 = b.Id where s.SubCategory =b.brand;



Update maidin.ProductCityInfoV2 pc 
set pc.PhotoId = (pc.PhotoId + (1666*2)) 
where pc.cityCode in (100003) 
and pc.id > 1

ALTER TABLE `maidin`.`productdetailgroup` 
CHANGE COLUMN `productDetailIds` `productDetailIds` VARCHAR(100) NULL DEFAULT NULL ;

DROP TABLE IF EXISTS MaidinMoneyLedger;
CREATE TABLE MaidinMoneyLedger (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  PositiveBalance DOUBLE,
  NegativeBalance DOUBLE,
  Date date,
  CreatedOn TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `maidin`.`OrderInfo` 
ADD COLUMN `AmountReceivedAtDelivery` FLOAT(7,2) NULL DEFAULT 0 AFTER `Remarks`;

ALTER TABLE `maidin`.`productcityinfov2` 
ADD COLUMN `discountAvailable` BIT(1) NULL DEFAULT 0 AFTER `PhotoId`;


-- added by Mudit
-- active column is added to remove query from popular searches (in case if it is required)
DROP TABLE IF EXISTS SearchQuery;
CREATE TABLE `SearchQuery` (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  Query varchar(500) NOT NULL,
  Count bigint ,
  CreatedOn timestamp,
  UpdatedOn timestamp,
  Active BIT
);

-- added by Mudit
-- active column is added to remove query from popular searches (in case if it is required)
DROP TABLE IF EXISTS PopularSearches;
CREATE TABLE `PopularSearches` (
  Id bigint AUTO_INCREMENT PRIMARY KEY,
  Keyword varchar(500) NOT NULL,
  Priority int,
  CreatedOn timestamp,
  UpdatedOn timestamp,
  CreatedBy varchar(100),
  UpdatedBy varchar(100),
  Active BIT
);

DROP TABLE IF EXISTS OrderStatus;
CREATE TABLE IF NOT EXISTS OrderStatus (
        Id    	int AUTO_INCREMENT PRIMARY KEY,
        OrderStatus  varchar(50),
        CreatedOn timestamp,
        CreatedBy bigint,
        UpdatedOn timestamp,
        UpdatedBy bigint
);

INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('1', 'CONFIRMED');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('2', 'INPROGESS');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('3', 'DELIVERED');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('4', 'CANCELLED');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('5', 'DELIVERYSTATUSPENDING');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('6', 'UNDELIVERED');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('7', 'SETTLED');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('8', 'OUT_FOR_DELIVERY');
INSERT INTO `maidin`.`orderstatus` (`Id`, `OrderStatus`) VALUES ('9', 'PARTIAL_DELIVERED');

Update maidin.orderstatus set UpdatedOn = current_timestamp();


