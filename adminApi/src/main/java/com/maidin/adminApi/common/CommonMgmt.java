package com.maidin.adminApi.common;

import java.util.List;

public interface CommonMgmt {

	List<Long> addUserDocument(long userId, String documentType, String documentUid, long... fileStoreIds);

	void deleteUserDocument(long userId, long... fileStoreIds);

	List<Long> updateUserDocument(long userId, String documentType, String documentUid, long... fileStoreIds);

}
