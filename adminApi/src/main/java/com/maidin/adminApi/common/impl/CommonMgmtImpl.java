package com.maidin.adminApi.common.impl;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.maidin.adminApi.common.CommonMgmt;
import com.maidin.adminApi.filestore.DocumentType;

/*import com.ostopd.persistence.dto.UserFileDTO;
import com.ostopd.persistence.dto.UserFileMetadataDTO;
import com.ostopd.persistence.user.profile.dao.UserFileDAO;
import com.ostopd.persistence.user.profile.dao.UserFileMetadataDAO;*/

//This class is planned not to have any transactionality, lets see how far can it go.
public class CommonMgmtImpl implements CommonMgmt {

		/*private static final Logger LOG = LoggerFactory.getLogger(PersonalInfoMgmtImpl.class);
		private final UserFileMetadataDAO userFileMetadataDAO;
		private final UserFileDAO userFileDAO;
		private final DocumentRelationsDAO documentRelationsDAO;*/

	@Autowired
	public CommonMgmtImpl(/*UserFileMetadataDAO userFileMetadataDAO, UserFileDAO userFileDAO,
			DocumentRelationsDAO documentRelationsDAO*/) {
		/*this.userFileMetadataDAO = userFileMetadataDAO;
		this.userFileDAO = userFileDAO;
		this.documentRelationsDAO = documentRelationsDAO;*/
	}

	/*
	 * @Override public List<Long> addUserDocument(long userId, String
	 * documentType, String refTableID, long... fileStoreIds) { List<Long> ids =
	 * Lists.newArrayList(); UserFileMetadataDTO dto = new
	 * UserFileMetadataDTO(); dto.setUserId(userId);
	 * dto.setDocumentType(documentType); dto.setDocumentUid(null);
	 * 
	 * if(documentType.equals(DocumentType.PHOTO.getName())){
	 * dto.setCertificationStatus(CertificationStatus.NOT_APPLICABLE.getName());
	 * } else { // if (documentType.equals(DocumentType.PAN.getName()) // ||
	 * documentType.equals(DocumentType.UAN.getName()) // ||
	 * documentType.equals(DocumentType.ADHAAR.getName()) // ||
	 * documentType.equals(DocumentType.PASSPORT.getName()) // ||
	 * documentType.equals(DocumentType.VOTER.getName())) {
	 * 
	 * boolean isAnyFilePresent = false; for (long fileStoreId: fileStoreIds) {
	 * if (fileStoreId != -1) { isAnyFilePresent = true; break; } }
	 * 
	 * if (isAnyFilePresent) {
	 * dto.setCertificationStatus(CertificationStatus.RECEIVED.getName()); }
	 * else {
	 * dto.setCertificationStatus(CertificationStatus.NOT_RECEIVED.getName()); }
	 * } // else if(documentType.equals(DocumentType.PHOTO.getName())){ //
	 * dto.setCertificationStatus(CertificationStatus.NOT_APPLICABLE.getName());
	 * // } else{ // // }
	 * 
	 * dto.setActive(true); dto.setDeleted(false); dto.setCreatedOn(new
	 * Timestamp(System.currentTimeMillis())); dto.setCreatedBy(userId);
	 * 
	 * long userFileMetadataId = userFileMetadataDAO.addUserFileMetadata(dto);
	 * ids.add(userFileMetadataId);
	 * 
	 * for (long fileStoreId: fileStoreIds) { if(fileStoreId!= -1){ UserFileDTO
	 * ufdto = new UserFileDTO();
	 * ufdto.setUserFileMetadataId(userFileMetadataId);
	 * ufdto.setFileStoreId(fileStoreId);
	 * 
	 * ufdto.setActive(true); ufdto.setDeleted(false); ufdto.setCreatedOn(new
	 * Timestamp(System.currentTimeMillis())); ufdto.setCreatedBy(userId); long
	 * userFileId = userFileDAO.addUserFile(ufdto); ids.add(userFileId); } }
	 * 
	 * if(documentType.equals(DocumentType.EDUCATION.getName()) ||
	 * documentType.equals(DocumentType.OCCUPATION.getName()) ||
	 * documentType.equals(DocumentType.DEGREE.getName()) ||
	 * documentType.equals(DocumentType.MARKSHEET.getName()) ||
	 * documentType.equals(DocumentType.COLLEGEID.getName()) ||
	 * documentType.equals(DocumentType.OFFERLETTER.getName()) ||
	 * documentType.equals(DocumentType.SALARYSLIP.getName()) ||
	 * documentType.equals(DocumentType.FORM16.getName()) ||
	 * documentType.equals(DocumentType.COMPANYTAG.getName())){
	 * DocumentRelationsDTO documentRelationsDTO = new DocumentRelationsDTO();
	 * documentRelationsDTO.setUserId(userId);
	 * documentRelationsDTO.setDocumentId(userFileMetadataId);
	 * documentRelationsDTO.setReferenceTableId(Long.parseLong(refTableID));
	 * documentRelationsDTO.setReferenceTable(documentType);
	 * 
	 * documentRelationsDTO.setActive(true);
	 * documentRelationsDTO.setDeleted(false);
	 * documentRelationsDTO.setCreatedOn(new
	 * Timestamp(System.currentTimeMillis()));
	 * documentRelationsDTO.setCreatedBy(userId); long relationId =
	 * documentRelationsDAO.addDocumentRelation(documentRelationsDTO); } return
	 * ids; }
	 * 
	 */
	@Override
	public List<Long> addUserDocument(long userId, String documentType, String refTableID, long... fileStoreIds) {
		return null;
		/*List<Long> ids = Lists.newArrayList();
		boolean isAnyFilePresent = false;
		for (long fileStoreId : fileStoreIds) {
			if (fileStoreId != -1) {
				isAnyFilePresent = true;
				// break;
				// }

				UserFileMetadataDTO dto = new UserFileMetadataDTO();
				dto.setUserId(userId);
				dto.setDocumentType(documentType);
				dto.setDocumentUid(null);

				if (documentType.equals(DocumentType.PHOTO.getName())) {
					dto.setCertificationStatus(CertificationStatus.NOT_APPLICABLE.getName());
				} else {
					// if (documentType.equals(DocumentType.PAN.getName())
					// || documentType.equals(DocumentType.UAN.getName())
					// || documentType.equals(DocumentType.ADHAAR.getName())
					// || documentType.equals(DocumentType.PASSPORT.getName())
					// || documentType.equals(DocumentType.VOTER.getName())) {

					if (isAnyFilePresent) {
						dto.setCertificationStatus(CertificationStatus.RECEIVED.getName());
					} else {
						dto.setCertificationStatus(CertificationStatus.NOT_RECEIVED.getName());
					}
				}
				// else if(documentType.equals(DocumentType.PHOTO.getName())){
				// dto.setCertificationStatus(CertificationStatus.NOT_APPLICABLE.getName());
				// } else{
				//
				// }

				dto.setActive(true);
				dto.setDeleted(false);
				dto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
				dto.setCreatedBy(userId);

				long userFileMetadataId = userFileMetadataDAO.addUserFileMetadata(dto);
				// ids.add(userFileMetadataId);

				// for (long fileStoreId: fileStoreIds) {
				// if(fileStoreId!= -1){
				UserFileDTO ufdto = new UserFileDTO();
				ufdto.setUserFileMetadataId(userFileMetadataId);
				ufdto.setFileStoreId(fileStoreId);

				ufdto.setActive(true);
				ufdto.setDeleted(false);
				ufdto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
				ufdto.setCreatedBy(userId);
				long userFileId = userFileDAO.addUserFile(ufdto);
				ids.add(userFileId);
				// }
				// }

				if (documentType.equals(DocumentType.EDUCATION.getName())
						|| documentType.equals(DocumentType.OCCUPATION.getName())
						|| documentType.equals(DocumentType.DEGREE.getName())
						|| documentType.equals(DocumentType.MARKSHEET.getName())
						|| documentType.equals(DocumentType.COLLEGEID.getName())
						|| documentType.equals(DocumentType.OFFERLETTER.getName())
						|| documentType.equals(DocumentType.SALARYSLIP.getName())
						|| documentType.equals(DocumentType.FORM16.getName())
						|| documentType.equals(DocumentType.COMPANYTAG.getName())) {
					DocumentRelationsDTO documentRelationsDTO = new DocumentRelationsDTO();
					documentRelationsDTO.setUserId(userId);
					documentRelationsDTO.setDocumentId(userFileMetadataId);
					documentRelationsDTO.setReferenceTableId(Long.parseLong(refTableID));
					documentRelationsDTO.setReferenceTable(documentType);

					documentRelationsDTO.setActive(true);
					documentRelationsDTO.setDeleted(false);
					documentRelationsDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
					documentRelationsDTO.setCreatedBy(userId);
					long relationId = documentRelationsDAO.addDocumentRelation(documentRelationsDTO);
				}
			}
		}

		return ids;*/
	}

	@Override
	public List<Long> updateUserDocument(long userId, String documentType, String documentUid, long... fileStoreIds) {
		return null;
		/*UserFileMetadataDTO ufmdto = userFileMetadataDAO.getUserFileMetadataByUserIdAndDocumentType(userId,
				documentType);
		if (ufmdto == null) {
			return addUserDocument(userId, documentType, documentUid, fileStoreIds);
		}
		if (documentUid != null) {
			ufmdto.setDocumentUid(documentUid);
		}

		List<Long> ids = Lists.newArrayList();
		ids.add(ufmdto.getId());
		boolean fileReceived = false;
		for (long fileStoreId : fileStoreIds) {
			if (fileStoreId != -1) {
				UserFileDTO ufdto = new UserFileDTO();
				ufdto.setUserFileMetadataId(ufmdto.getId());
				ufdto.setFileStoreId(fileStoreId);

				ufdto.setActive(true);
				ufdto.setDeleted(false);
				ufdto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
				ufdto.setCreatedBy(userId);
				long userFileId = userFileDAO.addUserFile(ufdto);
				ids.add(userFileId);
				fileReceived = true;
			}
		}

		if (fileReceived) {
			ufmdto.setCertificationStatus(CertificationStatus.RECEIVED.getName());
		} 
			 * else {
			 * ufmdto.setCertificationStatus(CertificationStatus.NOT_RECEIVED.
			 * getName()); }
			 
		userFileMetadataDAO.updateUserFileMetadata(ufmdto);
		return ids;*/
	}

	@Override
	public void deleteUserDocument(long userId, long... fileStoreIds) {

		/*for (long fileStoreId : fileStoreIds) {
			UserFileDTO ufdto = userFileDAO.getUserFileByFileStoreId(fileStoreId);
			boolean fileDeleted = false;
			if (ufdto != null) {
				UserFileMetadataDTO ufmdto = userFileMetadataDAO.getUserFileMetadataById(ufdto.getUserFileMetadataId());
				if (ufmdto != null && ufmdto.getUserId() == userId) {
					userFileDAO.deleteUserFile(ufdto);
					fileDeleted = true;
					DocumentRelationsDTO drdto = documentRelationsDAO.getDocumentRelationByDocumentId(ufmdto.getId());
					if (drdto != null) {
						documentRelationsDAO.deleteDocumentRelation(drdto);
					}
					userFileMetadataDAO.deleteUserFileMetadata(ufmdto);
				}
			}
			if (!fileDeleted) {
				LOG.error("Unable to delete file with filestore id: " + fileStoreId);
			}
		}*/
	}
}
