package com.maidin.adminApi.filestore;

public enum DocumentCategory {
	
	IDENTIFICATION(1, "IDENTIFICATION"),
	
	EDUCATION(2, "EDUCATION"),
	
	PROFESSION(3, "PROFESSION");
	
	private final int id;
	private final String name;
	
	private DocumentCategory(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static DocumentCategory fromId(int id) throws Exception {
		for (DocumentCategory t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Document Category: " + id);
	}
}
