package com.maidin.adminApi.filestore;

import java.util.Locale;

import com.google.common.io.ByteSource;
import com.maidin.adminApi.filestore.impl.RetrieveFileResponse;
import com.maidin.common.exception.ValidationException;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.filestore.RetrieveFile;
import com.maidin.pojo.filestore.StoreFile;

public interface FileStoreMgmt {

	/*
	 * public GenericResponse addFile(long userId, StoreFile storeFile,
	 * ByteSource byteSource, Locale locale) throws Exception;
	 * 
	 * public GenericResponse updateFile(long userId, StoreFile storeFile,
	 * ByteSource byteSource, Locale locale) throws Exception;
	 *
	 * public long uploadFile(long userId, StoreFile storeFile, ByteSource
	 * byteSource, Locale locale) throws Exception;
	 * 
	 */
	public void removeFile(int imageType, long itemId, long[] fileStoreIds, Locale locale) throws Exception;

	public RetrieveFileResponse retrieveFile(int imageType, long itemId, long fileStoreId, boolean encrypted,
			Locale locale) throws Exception;

	public long uploadFileV2(long adminId, int imageType, StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception;

	public RetrieveFileResponse retrieveFileV2(int imageType, long itemId, long fileStoreId, boolean b, Locale locale)  throws Exception;

	public void removeFileV2(int imageType, long[] fileStoreIds, Locale locale) throws Exception;

	public long uploadFile(long adminId, int imageType, Long imageId, StoreFile storeFile, ByteSource source,
			Locale locale) throws ValidationException, Exception;


	

}
