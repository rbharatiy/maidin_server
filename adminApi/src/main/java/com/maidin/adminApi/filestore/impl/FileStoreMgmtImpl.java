package com.maidin.adminApi.filestore.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;
import com.google.common.hash.Hashing;
import com.google.common.io.ByteSource;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.filestore.DocumentType;
import com.maidin.adminApi.filestore.FileStoreMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.crypto.Crypto;
import com.maidin.filestore.FileId;
import com.maidin.filestore.FileIdV2;
import com.maidin.filestore.FileStore;
import com.maidin.filestore.FileStoreInstance;
import com.maidin.persistence.dao.FileStoreDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dto.FileStoreDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.pojo.filestore.StoreFile;

public class FileStoreMgmtImpl implements FileStoreMgmt {

	private static final Logger LOG = LoggerFactory.getLogger(FileStoreMgmtImpl.class);

	private final FileStore fileStore;
	private final FileStoreDAO fileStoreDAO;
	private final Crypto crypto;
	private final PhotoDAO photoDAO;

	@Autowired
	public FileStoreMgmtImpl(FileStoreDAO fileStoreDAO, Crypto crypto, FileStoreInstance fileStoreInstance,
			PhotoDAO photoDAO) {
		this.fileStoreDAO = fileStoreDAO;
		this.crypto = crypto;
		this.fileStore = fileStoreInstance.getInstance();
		this.photoDAO = photoDAO;
	}

	private void checkFileName(String fileName, Locale locale) throws ValidationException {
		// picked up from
		// http://stackoverflow.com/questions/6730009/validate-a-file-name-on-windows
		Pattern pattern = Pattern.compile(
				"# Match a valid Windows filename (unspecified file system).          \n"
						+ "^                                # Anchor to start of string.        \n"
						+ "(?!                              # Assert filename is not: CON, PRN, \n"
						+ "  (?:                            # AUX, NUL, COM1, COM2, COM3, COM4, \n"
						+ "    CON|PRN|AUX|NUL|             # COM5, COM6, COM7, COM8, COM9,     \n"
						+ "    COM[1-9]|LPT[1-9]            # LPT1, LPT2, LPT3, LPT4, LPT5,     \n"
						+ "  )                              # LPT6, LPT7, LPT8, and LPT9...     \n"
						+ "  (?:\\.[^.]*)?                  # followed by optional extension    \n"
						+ "  $                              # and end of string                 \n"
						+ ")                                # End negative lookahead assertion. \n"
						+ "[^<>:\"/\\\\|?*\\x00-\\x1F]*     # Zero or more valid filename chars.\n"
						+ "[^<>:\"/\\\\|?*\\x00-\\x1F\\ .]  # Last char is not a space or dot.  \n"
						+ "$                                # Anchor to end of string.            ",
				Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE | Pattern.COMMENTS);
		Matcher matcher = pattern.matcher(fileName);
		boolean isMatch = matcher.matches();
		if (!isMatch) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_FILE_NAME, locale));
		}
	}

	@Override
	public long uploadFileV2(long adminId, int imageType,  StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception {
		String fileName = checkNotNull(storeFile.getFileName());
		checkFileName(fileName, locale);
		DocumentType documentType = DocumentType.valueOf(storeFile.getDocumentType());

		// Confirm if the file does not already exist in the given path
		/*PhotoDTO storedPhotoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(imageType, imageId);
		if (storedPhotoDTO != null) {
			FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(storedPhotoDTO.getFileStoreId());
			if (filstoreDTO != null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_ALREADY_EXIST, locale));
			}
		}*/
		/*List<PhotoDTO> photoList = photoDAO.getByPhotoTypeAndPhotoId(imageType, imageId);
		for(PhotoDTO photoDTO : photoList){*/
			/*FileStoreDTO filstoreDTO = fileStoreDAO.getFileByFileName(storeFile.getFileName());
			if(filstoreDTO != null){
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_ALREADY_EXIST, locale));
			}*/
		
		/*}*/

		FileStoreDTO dto = new FileStoreDTO();
		return storeFileV2(adminId, imageType, storeFile, source, dto, false);
	}

	private long storeFileV2(long adminId, int imageType, StoreFile storeFile, ByteSource byteSource,
			FileStoreDTO dto, boolean update) throws Exception {
		String baseName = FilenameUtils.getBaseName(storeFile.getFileName())+1;
		String fileName  = baseName +"."+FilenameUtils.getExtension(storeFile.getFileName());
		boolean encrypt = storeFile.isEncrypted();
		String contentType = storeFile.getContentType();
		long fileSize = storeFile.getFileSize();

		String md5 = "";
		if (encrypt) {
			byte[] bytes = crypto.encrypt(byteSource.read());
			md5 = Hashing.md5().hashBytes(bytes).toString();
			fileSize = bytes.length;
			byteSource = ByteSource.wrap(bytes);
		} else {
			byte[] bytes = byteSource.read();
			md5 = Hashing.md5().hashBytes(bytes).toString();
			fileSize = bytes.length;
			byteSource = ByteSource.wrap(bytes);
		}

		dto.setFileName(fileName);
		dto.setEncrypted(encrypt);
		dto.setFileSize(fileSize);
		dto.setContentType(contentType);
		dto.setMd5(md5);
		dto.setActive(true);
		dto.setDeleted(false);
		dto.setCreatedBy(adminId);
		dto.setUpdatedBy(adminId);

		dto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		dto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		long fileStoreId = 0L;
		if (!update) {
			fileStoreId = fileStoreDAO.addFileStore(dto);
		} else {
			fileStoreId = dto.getId();
			fileStoreDAO.updateFileStore(dto);
		}

		String folderPath =  imageType + FileId.separator + fileStoreId; //String.valueOf(fileStoreId);
		FileIdV2 fileId = FileIdV2.forUser(imageType, folderPath, fileName, fileSize);
		fileStore.storeFileV2(fileId, byteSource);
		return fileStoreId;
	}

	@Override
	public RetrieveFileResponse retrieveFile(int imageType, long itemId, long fileStoreId, boolean encrypted,
			Locale locale) throws Exception {
		FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
		}

		String folderPath = imageType + FileId.separator + itemId + FileId.separator + fileStoreId;
		String fileName = dto.getFileName();
		FileId fileId = FileId.forUser(imageType, itemId, folderPath, fileName);
		ByteSource byteSource = fileStore.retrieveFile(fileId);

		byte[] bytes = byteSource.read();
		String md5 = Hashing.md5().hashBytes(bytes).toString();
		if (!dto.getMd5().equals(md5)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.MD5_DOES_NOT_MATCH, locale));
		}

		if (encrypted) {
			bytes = crypto.decrypt(bytes);
			byteSource = ByteSource.wrap(bytes);
		} else {
			byteSource = ByteSource.wrap(bytes);
		}

		RetrieveFileResponse response = new RetrieveFileResponse();
		response.setContentType(dto.getContentType());
		response.setByteSource(byteSource);
		response.setFileName(fileName);
		return response;
	}

	@Override
	public void removeFile(int imageType, long itemId, long[] fileStoreIds, Locale locale) throws Exception {
		List<FileId> fileIds = Lists.newArrayList();
		for (long fileStoreId : fileStoreIds) {
			FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
			if (dto == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
			}

			String folderPath = imageType + FileId.separator + itemId + FileId.separator + fileStoreId;
			String fileName = dto.getFileName();
			FileId fileId = FileId.forUser(imageType, itemId, folderPath, fileName);
			fileIds.add(fileId);
			fileStoreDAO.deleteFileStore(dto);
		}
		fileStore.deleteFile(fileIds);

	}

	@Override
	public RetrieveFileResponse retrieveFileV2(int imageType, long itemId, long fileStoreId, boolean encrypted, Locale locale)
			throws Exception {
		FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
		}

		//String folderPath = String.valueOf(fileStoreId);
		String folderPath = imageType + FileId.separator + fileStoreId;
		String fileName = dto.getFileName();
		FileIdV2 fileId = FileIdV2.forUser(imageType, folderPath, fileName);
		ByteSource byteSource = fileStore.retrieveFileV2FromBucket2(fileId);

		byte[] bytes = byteSource.read();
		String md5 = Hashing.md5().hashBytes(bytes).toString();
		if (!dto.getMd5().equals(md5)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.MD5_DOES_NOT_MATCH, locale));
		}

		if (encrypted) {
			bytes = crypto.decrypt(bytes);
			byteSource = ByteSource.wrap(bytes);
		} else {
			byteSource = ByteSource.wrap(bytes);
		}

		RetrieveFileResponse response = new RetrieveFileResponse();
		response.setContentType(dto.getContentType());
		response.setByteSource(byteSource);
		response.setFileName(fileName);
		return response;
	}

	@Override
	public void removeFileV2(int imageType, long[] fileStoreIds, Locale locale) throws ValidationException {
		List<FileIdV2> fileIds = Lists.newArrayList();
		for (long fileStoreId : fileStoreIds) {
			FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
			if (dto == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
			}

			String folderPath = imageType + FileId.separator + fileStoreId;
			String fileName = dto.getFileName();
			FileIdV2 fileId = FileIdV2.forUser(imageType, folderPath, fileName, dto.getFileSize());
			fileIds.add(fileId);
			fileStoreDAO.deleteFileStore(dto);
		}
		fileStore.deleteFileV2(fileIds);
		
		
		List<FileIdV2> fileIds1 = Lists.newArrayList();
		for (long fileStoreId : fileStoreIds) {
			FileStoreDTO dto = fileStoreDAO.getFileById(fileStoreId);
			if (dto == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_DOES_NOT_EXIST, locale));
			}

			String folderPath = String.valueOf(fileStoreId);
			String fileName = dto.getFileName();
			FileIdV2 fileIdV2 = FileIdV2.forUser(folderPath, fileName, dto.getFileSize());
			fileIds1.add(fileIdV2);
			fileStoreDAO.deleteFileStore(dto);
		}
		fileStore.deleteFileV2ForBucket1(fileIds1);
		
	}
/*
	@Override
	public long uploadFile(long adminId, int imageType, long imageId, StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception {
		String fileName = checkNotNull(storeFile.getFileName());
		checkFileName(fileName, locale);
		DocumentType documentType = DocumentType.valueOf(storeFile.getDocumentType());

		// Confirm if the file does not already exist in the given path
		PhotoDTO storedPhotoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(imageType, imageId);
		if (storedPhotoDTO != null) {
			FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(storedPhotoDTO.getFileStoreId());
			if (filstoreDTO != null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_ALREADY_EXIST, locale));
			}
		}

		FileStoreDTO dto = new FileStoreDTO();
		//return storeFile1(adminId, imageType, imageId, storeFile, source, dto, false);
	}*/

	@Override
	public long uploadFile(long adminId, int imageType, Long imageId, StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception {String fileName = checkNotNull(storeFile.getFileName());
			checkFileName(fileName, locale);
			DocumentType documentType = DocumentType.valueOf(storeFile.getDocumentType());

			// Confirm if the file does not already exist in the given path
			PhotoDTO storedPhotoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(imageType, imageId);
			if (storedPhotoDTO != null) {
				FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(storedPhotoDTO.getFileStoreId());
				if (filstoreDTO != null) {
					throw new ValidationException(
							ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.FILE_ALREADY_EXIST, locale));
				}
			}

			FileStoreDTO dto = new FileStoreDTO();
			return storeFile1(adminId, imageType, imageId, storeFile, source, dto, false);}
	
	private long storeFile1(long adminId, int imageType, long imageId, StoreFile storeFile, ByteSource byteSource,
			FileStoreDTO dto, boolean update) throws Exception {
		String fileName = storeFile.getFileName();
		boolean encrypt = storeFile.isEncrypted();
		String contentType = storeFile.getContentType();
		long fileSize = storeFile.getFileSize();

		String md5 = "";
		if (encrypt) {
			byte[] bytes = crypto.encrypt(byteSource.read());
			md5 = Hashing.md5().hashBytes(bytes).toString();
			fileSize = bytes.length;
			byteSource = ByteSource.wrap(bytes);
		} else {
			byte[] bytes = byteSource.read();
			md5 = Hashing.md5().hashBytes(bytes).toString();
			byteSource = ByteSource.wrap(bytes);
		}

		dto.setFileName(fileName);
		dto.setEncrypted(encrypt);
		dto.setFileSize(fileSize);
		dto.setContentType(contentType);
		dto.setMd5(md5);
		dto.setActive(true);
		dto.setDeleted(false);
		dto.setCreatedBy(adminId);
		dto.setUpdatedBy(adminId);

		dto.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		dto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		long fileStoreId = 0L;
		if (!update) {
			fileStoreId = fileStoreDAO.addFileStore(dto);
		} else {
			fileStoreId = dto.getId();
			fileStoreDAO.updateFileStore(dto);
		}

		String folderPath = imageType + FileId.separator + imageId + FileId.separator + fileStoreId;
		FileId fileId = FileId.forUser(imageType, imageId, folderPath, fileName, fileSize);
		fileStore.storeFile(fileId, byteSource);
		return fileStoreId;
	}

}
