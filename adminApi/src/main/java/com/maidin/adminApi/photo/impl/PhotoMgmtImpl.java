package com.maidin.adminApi.photo.impl;

import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.IndexColorModel;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.imageio.ImageIO;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.google.common.io.ByteSource;
import com.maidin.adminApi.enums.ImageCategory;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.filestore.FileStoreMgmt;
import com.maidin.adminApi.filestore.impl.RetrieveFileResponse;
import com.maidin.adminApi.photo.PhotoMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.FileStoreDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductDetailGroupDAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.UpdateInfoDAO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.FileStoreDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductDetailGroupDTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.UpdateInfoDTO;
import com.maidin.pojo.filestore.StoreFile;
import com.maidin.pojo.photo.AddPhotoResponse;
import com.maidin.pojo.photo.ChangePrimaryPhoto;
import com.maidin.pojo.photo.ChangePrimaryPhotoResponse;
import com.maidin.pojo.photo.DeletePhotoResponse;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.photo.PhotoAllAttrs;
import com.mortennobel.imagescaling.MultiStepRescaleOp;
import com.mortennobel.imagescaling.ProgressListener;
import com.mortennobel.imagescaling.ResampleFilters;
import com.mortennobel.imagescaling.ResampleOp;

import net.coobird.thumbnailator.makers.FixedSizeThumbnailMaker;
import net.coobird.thumbnailator.resizers.DefaultResizerFactory;
import net.coobird.thumbnailator.resizers.Resizer;

public class PhotoMgmtImpl implements PhotoMgmt {

	private static final Logger LOG = LoggerFactory.getLogger(PhotoMgmtImpl.class);
	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);

	private final PhotoDAO photoDAO;
	private final FileStoreDAO fileStoreDAO;
	private final FileStoreMgmt fileStoreMgmt;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final UpdateInfoDAO updateInfoDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductDetailGroupDAO productDetailGroupDAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final PaymentModeDAO paymentModeDAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;

	@Autowired
	public PhotoMgmtImpl(PhotoDAO photoDAO, FileStoreDAO fileStoreDAO, FileStoreMgmt fileStoreMgmt,
			AuthenticationMgmt authenticationMgmt, AdminDAO adminDAO, ProductInfoV2DAO productInfoV2DAO,
			UpdateInfoDAO updateInfoDAO, ProductCityInfoV2DAO productCityInfoV2DAO,
			ProductDetailGroupDAO productDetailGroupDAO, CategoryInfoV2DAO categoryInfoV2DAO, PaymentModeDAO paymentModeDAO,
			SubCategoryInfoV2DAO subCategoryInfoV2DAO) {
		this.photoDAO = photoDAO;
		this.fileStoreDAO = fileStoreDAO;
		this.fileStoreMgmt = fileStoreMgmt;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.updateInfoDAO = updateInfoDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productDetailGroupDAO = productDetailGroupDAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.paymentModeDAO = paymentModeDAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
	}

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AddPhotoResponse addPhoto(String authToken, int imageType, Long itemId,
			PhotoFiles photoFiles, Locale locale) throws Exception {
		// check authentication
		String mobileNumber = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		//String mobileNumber = "9811611743";
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobileNumber);
		long adminId = adminDTO.getId();

		AddPhotoResponse response = new AddPhotoResponse();
		long photo1FileStoreId = uploadFileV2(adminId, imageType, photoFiles.getPhoto1StoreFile(),
				photoFiles.getPhoto1ByteSource(), locale);

		FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(photo1FileStoreId);

		//checkConstraint(imageType, itemId, productDetailIdList, locale);

		PhotoDTO photoDTO = new PhotoDTO();
		photoDTO.setActive(true);
		photoDTO.setItemId(itemId);
		photoDTO.setImageType(imageType);
		photoDTO.setFileStoreId(filstoreDTO.getId());
		if (imageType == ImageCategory.PAYMENTOPTION.getId()) {
			UpdateInfoDTO updateInfoDTO = updateInfoDAO.getByText("PaymentMode");
			updateInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			
		}
		
		photoDTO.setVersion("v2");
		photoDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		photoDTO.setDeleted(false);
		photoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		photoDTO.setCreatedBy(adminId);
		photoDTO.setUpdatedBy(adminId);
		photoDTO.setPrimaryPhoto(true);
		
		Long photoId = photoDAO.addPhoto(photoDTO);
		if(imageType == ImageCategory.CATEGORY.getId()){
			CategoryInfoV2DTO catInfoV2DTO = categoryInfoV2DAO.getById(itemId.intValue());
			catInfoV2DTO.setPhotoId(photoId);
		}
		else if(imageType == ImageCategory.PAYMENTOPTION.getId()){
			PaymentModeDTO paymentModeDTO = paymentModeDAO.getById(itemId.intValue());
			paymentModeDTO.setPhotoId(photoId);
		}
		else if(imageType == ImageCategory.PRODUCTDETAIL.getId()){
			ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(itemId);
			List<ProductCityInfoV2DTO> productCityDTOList = productCityInfoV2DAO.getByProductIdQuantityUnit(productCityInfoV2DTO.getProductId(),productCityInfoV2DTO.getQuantityUnit());
			productCityDTOList.stream().forEach(elt -> {
				elt.setPhotoId(photoId);
			});
			
		}
		
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminId, ApiUtils.toJsonString(response));
		return response;
	}

	

	private void checkConstraint(int imageType, long imageId, List<Long> productDetailsIdList, Locale locale)
			throws ValidationException {
		List<PhotoDTO> photoList = photoDAO.getListByPhotoTypePhotoIdAndVersion(imageType, imageId, "v2");
		
		if (photoList != null) {
			if (imageType != ImageCategory.PRODUCTDESCRIPTION.getId()) {
				if (photoList.size() > 1) {
					if (imageType != ImageCategory.PRODUCT.getId()) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.MULTIPLE_IMAGES_EXIST, locale));
					} else if (imageType != ImageCategory.PRODUCT.getId()) {
						if(productDetailsIdList.size() == 0 ){
						List<PhotoDTO> photoList1 = photoDAO.getListByPhotoTypePhotoIdVersionAndProductDetailIds(imageType, imageId, "v2");
						if(photoList1.size() > 1){
							throw new ValidationException(ExceptionResourceBundle
									.getExceptionCodeProperties(ExceptionCode.MULTIPLE_IMAGES_EXIST, locale));
						}
					    }
						else if(productDetailsIdList.size() > 1 ){
						for (PhotoDTO photoDTO : photoList) {/*
							String productDetailsIds = photoDTO.getProductDetailIds();
							if (!Strings.isNullOrEmpty(productDetailsIds)) {
								String[] productDetailStringArray = productDetailsIds.split(",");
								for (String productDetailIdInString : productDetailStringArray) {
									if (productDetailsIdList.contains(Long.parseLong(productDetailIdInString))) {
										throw new ValidationException(
												ExceptionResourceBundle.getExceptionCodeProperties(
														ExceptionCode.MULTIPLE_IMAGES_EXIST_OF_PRODUCTDETAILSID,
														locale));
									}
								}
							}
						*/}
					}
					}
				}
			}
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));
		}
	}

	private long uploadFileV2(long adminId,int imageType, StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception {
		if (storeFile != null && source != null) {
			return fileStoreMgmt.uploadFileV2(adminId, imageType, storeFile, source, locale);
		}
		return ApiUtils.INVALID_ID;
	}
	
	public static BufferedImage getScaledImage(BufferedImage image, int width, int height) throws IOException {
	    int imageWidth  = image.getWidth();
	    int imageHeight = image.getHeight();
	    int type = image.getTransparency() == Transparency.OPAQUE ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
	    double scaleX = (double)width/imageWidth;
	    double scaleY = (double)height/imageHeight;
	    AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
	    AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

	    return bilinearScaleOp.filter(
	        image,
	        new BufferedImage(width, height, type));
	}

	public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

	    int original_width = imgSize.width;
	    int original_height = imgSize.height;
	    int bound_width = boundary.width;
	    int bound_height = boundary.height;
	    int new_width = original_width;
	    int new_height = original_height;

	    // first check if we need to scale width
	    if (original_width > bound_width) {
	        //scale width to fit
	        new_width = bound_width;
	        //scale height to maintain aspect ratio
	        new_height = (new_width * original_height) / original_width;
	    }

	    // then check if we need to scale even with the new height
	    if (new_height > bound_height) {
	        //scale height to fit instead
	        new_height = bound_height;
	        //scale width to maintain aspect ratio
	        new_width = (new_height * original_width) / original_height;
	    }

	    return new Dimension(new_width, new_height);
	}
	
	@Override
	public BufferedImage resizeImageWithType(BufferedImage originalImage, String imageType, long fileSize) throws IOException {
		BufferedImage scaledImage = null;
		if (!((imageType.equalsIgnoreCase(ImageCategory.CATEGORY.getName()))
				|| (imageType.equalsIgnoreCase(ImageCategory.PAYMENTOPTION.getName()))) ) {
			int imageWidth = originalImage.getWidth(null);
			int imageHeight = originalImage.getHeight(null);
			Dimension imageSize = new Dimension(imageWidth, imageHeight);
			int larger = checkLargerOfTwoNumbers(imageWidth, imageHeight);
			Dimension dimension = setImageRatio(larger, imageWidth, imageHeight, imageType,fileSize );
			
			
			
			//scaledImage = progressiveScaling(originalImage, larger);
			/*scaledImage = getScaledInstance(
		            originalImage, dimension.width, dimension.height, RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
*/
			//A word on JDK Scaling Methods

			/*int type = (originalImage.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		    
			scaledImage = new BufferedImage(dimension.width, dimension.height,type );
			Graphics2D graphics2D = scaledImage.createGraphics();
			//graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
			graphics2D.drawImage(originalImage, 0, 0, dimension.width, dimension.height, null);
			graphics2D.dispose();*/
			//
			//originalImage = new MultiStepRescaleOp(dimension.width, dimension.height, RenderingHints.VALUE_INTERPOLATION_BILINEAR).filter(originalImage, null);
			
			//scaledImage = new MultiStepRescaleOp(dimension.width, dimension.height, RenderingHints.VALUE_INTERPOLATION_BICUBIC).filter(originalImage, null);
			scaledImage = Scalr.resize(originalImage, Scalr.Method.SPEED, Scalr.Mode.AUTOMATIC, dimension.width, dimension.height);
			//originalImage = scalefromThumbnail(originalImage, dimension.width,dimension.height, imageSize.width,imageSize.height);
			//scaledImage = scalefromLancoz(originalImage, dimension.width,dimension.height);
			
			//originalImage = new BufferedImage(dimension.width, dimension.height, IndexColorModel.TRANSLUCENT);
			//originalImage = getScaledImage(originalImage, dimension.width, dimension.height);
			//Dimension dimension1 = getScaledDimension(imageSize, dimension);
			//originalImage = Scalr.resize(originalImage,dimension.width,dimension.height);
			//BufferedImage thumbnail = Scalr.resize(originalImage,Scalr.Method.ULTRA_QUALITY,Scalr.Mode.AUTOMATIC,dimension.width,dimension.height);
			/*originalImage = Scalr.resize(originalImage, dimension1.width,dimension1.height,BufferedImageOp.)*/
			//originalImage = new BufferedImage(dimension1.width,dimension1.height, BufferedImage.TYPE_BYTE_GRAY);
			
			/*originalImage = Scalr.resize(originalImage, Scalr.Method.SPEED, Scalr.Mode.AUTOMATIC, dimension.width,
					dimension.height);*/
			/*try {
				  BufferedImage thumbnail = Scalr.resize(originalImage,Scalr.Method.ULTRA_QUALITY,Scalr.Mode.AUTOMATIC,
				                                         dimension.width,
				                                         dimension.height);
				  if(thumbnail.getWidth() > destinationSize.width) {
				    thumbnail = Scalr.crop(thumbnail,
				                           (thumbnail.getWidth() - destinationSize.width) / 2,
				                           0,
				                           destinationSize.width,
				                           destinationSize.height);
				  }
				  else if(thumbnail.getHeight() > destinationSize.height) {
				    thumbnail = Scalr.crop(thumbnail,
				                           0,
				                           (thumbnail.getHeight() - destinationSize.height) / 2,
				                           destinationSize.width,
				                           destinationSize.height);
				  }
				}
				catch(IllegalArgumentException | ImagingOpException e) {
				  System.out.println("imgscalr threw an exception: " + e.getMessage());
				}
			BufferedImage resizedImage = Scalr.resize(originalImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.AUTOMATIC, dimension.width, dimension.height);
			if(resizedImage.getWidth() > dimension.width){
				resizedImage = Scalr.crop(resizedImage, width, height, ops);
			}*/
			
		//	originalImage = scale1(originalImage, dimension.width,dimension.height);
			//originalImage = scale(originalImage,dimension.width,dimension.height);
			
			/*int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
			        : originalImage.getType();
			*/
			//int type = (originalImage.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
		    
			 // originalImage = resizeImage1(originalImage, dimension, type);*/
			  //ImageIO.write(resizeImageBmp, "png", new File("a.png"));

			//originalImage = resizeImageWithHint(originalImage, dimension, type);
			 //  ImageIO.write(resizeImageBmp, "png", new File("b.png"));
		}
		else{
			scaledImage= originalImage;
		}
		
		return scaledImage;
	}
	
	private  BufferedImage progressiveScaling(BufferedImage before, Integer longestSideLength, float givenRatio) {
	    if (before != null) {
	        Integer w = before.getWidth();
	        Integer h = before.getHeight();

	        Double ratio = h > w ? longestSideLength.doubleValue() / h : longestSideLength.doubleValue() / w;

	        //Multi Step Rescale operation
	        //This technique is describen in Chris Campbell’s blog The Perils of Image.getScaledInstance(). As Chris mentions, when downscaling to something less than factor 0.5, you get the best result by doing multiple downscaling with a minimum factor of 0.5 (in other words: each scaling operation should scale to maximum half the size).
	        while (ratio < 0.5) {
	            BufferedImage tmp = scale(before, 0.5);
	            before = tmp;
	            w = before.getWidth();
	            h = before.getHeight();
	            ratio = h > w ? longestSideLength.doubleValue() / h : longestSideLength.doubleValue() / w;
	        }
	        BufferedImage after = scale(before, ratio);
	        return after;
	    }
	    return null;
	}

	private  BufferedImage scale(BufferedImage imageToScale, Double ratio) {
	    Integer dWidth = ((Double) (imageToScale.getWidth() * ratio)).intValue();
	    Integer dHeight = ((Double) (imageToScale.getHeight() * ratio)).intValue();
	    BufferedImage scaledImage = new BufferedImage(dWidth, dHeight, BufferedImage.TYPE_INT_RGB);
	    Graphics2D graphics2D = scaledImage.createGraphics();
	    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
	    graphics2D.dispose();
	    return scaledImage;
	}
	
	public  BufferedImage getScaledInstance(
	        BufferedImage img, int targetWidth,
	        int targetHeight, Object hint, 
	        boolean higherQuality)
	    {
	        int type =
	            (img.getTransparency() == Transparency.OPAQUE)
	            ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
	        BufferedImage ret = (BufferedImage) img;
	        int w, h;
	        if (higherQuality)
	        {
	            // Use multi-step technique: start with original size, then
	            // scale down in multiple passes with drawImage()
	            // until the target size is reached
	            w = img.getWidth();
	            h = img.getHeight();
	        }
	        else
	        {
	            // Use one-step technique: scale directly from original
	            // size to target size with a single drawImage() call
	            w = targetWidth;
	            h = targetHeight;
	        }

	        do
	        {
	            if (higherQuality && w > targetWidth)
	            {
	                w /= 2;
	                if (w < targetWidth)
	                {
	                    w = targetWidth;
	                }
	            }

	            if (higherQuality && h > targetHeight)
	            {
	                h /= 2;
	                if (h < targetHeight)
	                {
	                    h = targetHeight;
	                }
	            }

	            BufferedImage tmp = new BufferedImage(w, h, type);
	            Graphics2D g2 = tmp.createGraphics();
	            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
	            g2.drawImage(ret, 0, 0, w, h, null);
	            g2.dispose();

	            ret = tmp;
	        } while (w != targetWidth || h != targetHeight);

	        return ret;
	    }

	
	/*public static BufferedImage resizeImage(final Image image, int width, int height) {
        final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        //below three lines are for RenderingHints for better image quality at cost of higher processing time
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
       // graphics2D.drawImage(image, 0, 0, width, height, null);
        graphics2D.dispose();
        return bufferedImage;
    }*/

	/*private BufferedImage scalefromLancoz(BufferedImage originalImage, int newwidth, int newheight) {
	 * Progressive Scaling

		ResampleOp resizeOp = new ResampleOp(newwidth, newheight);
    resizeOp.addProgressListener(new ProgressListener() {
        public void notifyProgress(float fraction) {
            System.out.printf("Resizing %f%n",fraction);
        }
    });
    return resizeOp.filter(originalImage, null);
    }*/
	private BufferedImage scalefromLancoz(BufferedImage originalImage, int newwidth, int newheight) {
		//Lanczos Resampling

		ResampleOp resizeOp = new ResampleOp(newwidth, newheight);
		resizeOp.setFilter(ResampleFilters.getLanczos3Filter());
    System.out.println("newwidth: "+newwidth+" newheight: "+newheight);
    return resizeOp.filter(originalImage, null);
    }

	
	/*private BufferedImage scalefromThumbnail(BufferedImage originalImage, int newwidth, int newheight, int width, int height) {
		//Thumbnailator

		Resizer resizer = DefaultResizerFactory.getInstance().getResizer(
	 new Dimension(width, height),   new Dimension(newwidth, newheight));
		return new FixedSizeThumbnailMaker(  newwidth, newheight, false, true).resizer(resizer).make(originalImage);
     
    }
*/
	
	/*private BufferedImage getScaledInstance(BufferedImage originalImage, int i, int j,
			Object valueInterpolationBilinear, boolean b) {
		// TODO Auto-generated method stub
		return null;
	}
*/
	private BufferedImage scale1(BufferedImage originalImage, int width, int height) {
		BufferedImage thumbnail = Scalr.resize(originalImage,Scalr.Method.ULTRA_QUALITY,Scalr.Mode.AUTOMATIC,width,height);
		if(thumbnail.getWidth() > width) {
		thumbnail = Scalr.crop(thumbnail,(thumbnail.getWidth() - width) / 2,0,width,height);
		}
		else if(thumbnail.getHeight() > height) {
		thumbnail = Scalr.crop(thumbnail,0,(thumbnail.getHeight() - height) / 2,width,height);
		}
		return thumbnail;
		}
	

	private static BufferedImage resizeImage1(BufferedImage originalImage, Dimension dimension, int type) {
	    int IMG_WIDTH =dimension.width;
	    int IMG_CLAHEIGHT =dimension.height;
	    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_CLAHEIGHT,
	        type);
	    Graphics2D g = resizedImage.createGraphics();
	    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_CLAHEIGHT, null);
	    g.dispose();
	    return resizedImage;
	  }

	  private static BufferedImage resizeImageWithHint(BufferedImage originalImage,Dimension dimension,
	      int type) {
		  int IMG_WIDTH = dimension.width;
		    int IMG_CLAHEIGHT = dimension.height;
	    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_CLAHEIGHT,
	        type);
	    Graphics2D g = resizedImage.createGraphics();
	    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_CLAHEIGHT, null);
	    g.dispose();
	    g.setComposite(AlphaComposite.Src);

	    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g.setRenderingHint(RenderingHints.KEY_RENDERING,
	        RenderingHints.VALUE_RENDER_QUALITY);
	    g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	        RenderingHints.VALUE_ANTIALIAS_ON);

	    return resizedImage;
	  }

	private BufferedImage scale(BufferedImage img, int targetWidth, int targetHeight) {
		int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
	    BufferedImage ret = img;
	    BufferedImage scratchImage = null;
	    Graphics2D g2 = null;

	    int w = img.getWidth();
	    int h = img.getHeight();

	    int prevW = w;
	    int prevH = h;

	    do {
	    	if (w > targetWidth) {
	            w /= 2;
	            w = (w < targetWidth) ? targetWidth : w;
	        }

	        if (h > targetHeight) {
	            h /= 2;
	            h = (h < targetHeight) ? targetHeight : h;
	        }
	        if (scratchImage == null) {
	            scratchImage = new BufferedImage(w, h, type);
	            g2 = scratchImage.createGraphics();
	        }

	        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	        g2.drawImage(ret, 0, 0, w, h, 0, 0, prevW, prevH, null);

	        prevW = w;
	        prevH = h;
	        ret = scratchImage;
	    } while (w != targetWidth || h != targetHeight);

	    if (g2 != null) {
	        g2.dispose();
	    }

	    if (targetWidth != ret.getWidth() || targetHeight != ret.getHeight()) {
	        scratchImage = new BufferedImage(targetWidth, targetHeight, type);
	        g2 = scratchImage.createGraphics();
	        g2.drawImage(ret, 0, 0, null);
	        g2.dispose();
	        ret = scratchImage;
	    }

	    return ret;


	}

	public Dimension setImageRatio(int larger, int imageWidth, int imageHeight, String imageType, long fileSize) {
		Dimension dimension = new Dimension(imageWidth, imageHeight);

		if (imageType.equals(ImageCategory.CATEGORY.getName())) {
			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_5001)));
			}

		} else if (imageType.equals(ImageCategory.BRAND.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_5001)));
			}

		} else if (imageType.equals(ImageCategory.PRODUCT.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_5001)));
			}

		}

		else if (imageType.equals(ImageCategory.PAYMENTOPTION.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PAYMENTOPTION_PIXEL_RANGE_5001)));
			}

		}

		else if (imageType.equals(ImageCategory.PRODUCTDESCRIPTION.getName())) {

			if (larger > 0 && larger < 401) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_0_250)));
			} else if (larger > 400 && larger < 651) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_251_500)));
			} else if (larger > 650 && larger < 900) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_501_1000)));
			} else if (larger > 900 && larger < 1501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_5001)));
			}

		}
		
		else if (imageType.equals(ImageCategory.PRODUCTDETAIL.getName())) {

			/*if (larger > 0 && larger < 200) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_0_250)));
			} else if (larger > 200 && larger < 401) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_251_500)));
			} else if (larger > 401 && larger < 551) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_501_1000)));
			} else if (larger > 550 && larger < 651) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_1001_2500)));
			} else if (larger > 650 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_2501_3500)));
			} else if (larger > 1000 && larger < 2001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float
						.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_5001)));
			}*/
			if(larger >= 500 && larger <= 800){
			if(fileSize <= 30000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_30000)));
			}
			else if(fileSize >  30000 && fileSize <= 60000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_60000)));
			}
			else if(fileSize >  60000 && fileSize <= 100000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_100000)));
			}
			else if(fileSize >  100000 && fileSize <= 130000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_130000)));
			}
			else if(fileSize >  130000 && fileSize <= 160000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_160000)));
			}
			else if(fileSize >  160000 && fileSize <= 210000){
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_210000)));
			}
			else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_250000)));
			}
			}
			else{
				if (larger > 0 && larger < 200) {
					dimension = setImageDimension(larger, imageWidth, imageHeight,
							Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250)));
				} else if (larger > 200 && larger < 401) {
					dimension = setImageDimension(larger, imageWidth, imageHeight,
							Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500)));
				} else if (larger > 401 && larger < 551) {
					dimension = setImageDimension(larger, imageWidth, imageHeight,
							Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000)));
				} else if (larger > 550 && larger < 651) {
					dimension = setImageDimension(larger, imageWidth, imageHeight, Float
							.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500)));
				} else if (larger > 650 && larger < 1001) {
					dimension = setImageDimension(larger, imageWidth, imageHeight, Float
							.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500)));
				} else if (larger > 1000 && larger < 2001) {
					dimension = setImageDimension(larger, imageWidth, imageHeight, Float
							.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000)));
				} else {
					dimension = setImageDimension(larger, imageWidth, imageHeight,
							Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001)));
				}
			}
			
		}

		else {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001)));
			}

		}

		System.out.println(larger + "----" + imageWidth + " --- " + imageHeight);
		System.out.println(larger + "----" + dimension.width + " --- " + dimension.height);
		return dimension;

	}

	public Dimension setImageDimension(float larger, float imageWidth, float imageHeight, float i) {
		int imageHeight1 = (int) (imageHeight / i);
		int imageWidth1 =  (int) (imageWidth / i);
		Dimension dimension = new Dimension(imageWidth1, imageHeight1);
		return dimension;

	}

	public int checkLargerOfTwoNumbers(int imageWidth, int imageHeight) {
		int larger = imageWidth;
		if (imageWidth > imageHeight) {
			larger = imageWidth;
		} else {
			larger = imageHeight;
		}
		return larger;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrievePhoto(int imageType, long itemId, long fileStoreId, Locale locale)
			throws Exception {

		return fileStoreMgmt.retrieveFile(imageType, itemId, fileStoreId, true, locale);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrievePhotoV2(int imageType, long itemId, long fileStoreId, Locale locale)
			throws Exception {

		return fileStoreMgmt.retrieveFileV2(ImageCategory.PRODUCT.getId(), itemId, fileStoreId, true, locale);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreId(int imageType, long itemId, String version, Locale locale) throws ValidationException {
		PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(imageType, itemId, version);
		if (photoDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));
		}
		return photoDTO.getFileStoreId();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public DeletePhotoResponse deletePhoto(String authToken, int imageType, long itemId, Locale locale)
			throws Exception {
		// check authentication
		String mobileNumber = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobileNumber);

		DeletePhotoResponse response = new DeletePhotoResponse();

		PhotoDTO photoDTO = photoDAO.getPhotoByPhotoTypeAndPhotoId(imageType, itemId);
		if (photoDTO != null) {
			// mark photo as deleted
			photoDTO.setDeleted(true);
			photoDTO.setUpdatedBy(adminDTO.getId());
			photoDAO.updatePhoto(photoDTO);

			// mark fileStore as deleted
			FileStoreDTO fileStoreDTO = fileStoreDAO.getFileById(photoDTO.getFileStoreId());
			fileStoreDTO.setDeleted(true);
			fileStoreDAO.updateFileStore(fileStoreDTO);

			// delete file from S3
			long[] fileStoreIds = new long[1];
			fileStoreIds[0] = fileStoreDTO.getId();
			fileStoreMgmt.removeFile(imageType, itemId, fileStoreIds, locale);
		}

		response = ApiUtils.setResponse(response);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public DeletePhotoResponse deletePhotoByphotoId(String authToken, long photoId, Locale locale) throws Exception {
		// check authentication
		String mobileNumber = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobileNumber);

		DeletePhotoResponse response = new DeletePhotoResponse();

		PhotoDTO photoDTO = photoDAO.getById(photoId);
		if (photoDTO != null) {
			// mark photo as deleted
			photoDTO.setDeleted(true);
			photoDTO.setUpdatedBy(adminDTO.getId());
			photoDAO.updatePhoto(photoDTO);

			// mark fileStore as deleted
			FileStoreDTO fileStoreDTO = fileStoreDAO.getFileById(photoDTO.getFileStoreId());
			fileStoreDTO.setDeleted(true);
			fileStoreDAO.updateFileStore(fileStoreDTO);

			// delete file from S3
			long[] fileStoreIds = new long[1];
			fileStoreIds[0] = fileStoreDTO.getId();
			if(photoDTO.getVersion().equals("v2")){
			    fileStoreMgmt.removeFileV2(photoDTO.getImageType(), fileStoreIds, locale);
			}
			else{
				if(photoDTO.getImageType() == ImageCategory.BRAND.getId() || photoDTO.getImageType() == ImageCategory.PRODUCT.getId()){
				fileStoreMgmt.removeFile(photoDTO.getImageType(), photoDTO.getItemIdV1(), fileStoreIds, locale);
				}
				else if(photoDTO.getImageType() == ImageCategory.CATEGORY.getId()){
					fileStoreMgmt.removeFile(photoDTO.getImageType(), photoDTO.getItemId(), fileStoreIds, locale);	
				}
			}
			
			if(photoDTO.getImageType() == ImageCategory.CATEGORY.getId()){
				CategoryInfoV2DTO categoryInfoV2DTO = categoryInfoV2DAO.getById(photoDTO.getItemId().intValue());
				categoryInfoV2DTO.setPhotoId(null);
			}
			else if(photoDTO.getImageType() == ImageCategory.PAYMENTOPTION.getId()){
				PaymentModeDTO paymentModeDTO = paymentModeDAO.getById(photoDTO.getItemId().intValue());
				paymentModeDTO.setPhotoId(null);
			}
			else if(photoDTO.getImageType() == ImageCategory.PRODUCTDESCRIPTION.getId()){
				ProductCityInfoV2DTO productCityInfo = productCityInfoV2DAO.getByIdWithoutActiveStatus(photoDTO.getItemId());
				List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO.getByProductDetailGroupId(productCityInfo.getProductDetailGroupId());
				for(ProductCityInfoV2DTO productCityInfoDTO : productCityList){
					String photoGroup = productCityInfoDTO.getPhotoGroup();
					String[] photoGroupStringArr = photoGroup.split(",");
					long[] photoGroupIntArr = Stream.of(photoGroupStringArr).mapToLong(Long::parseLong).toArray();
					List<Long> photoIds = Arrays.stream(photoGroupIntArr).boxed()
							.collect(Collectors.toList());
					if(photoIds.contains(photoDTO.getId())){
						photoIds.remove(photoDTO.getId());
						 productCityInfoDTO.setPhotoGroup(StringUtils.join(photoIds, ','));
					}
				}
			}
			else if(photoDTO.getImageType() == ImageCategory.PRODUCTDETAIL.getId()){
				List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO.getByPhotoId(photoDTO.getId());
				for(ProductCityInfoV2DTO productCityInfoDTO :productCityList){
					productCityInfoDTO.setPhotoId(null);
				}
			}
			
		}

		response = ApiUtils.setResponse(response);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AddPhotoResponse addMultiplePhotos(String authToken,Long productDetailId,
			List<PhotoFiles> photoFiles, String primaryPhoto, Locale locale) throws Exception {
		// check authentication
		String mobileNumber = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		//String mobileNumber = "9811611743";

		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobileNumber);
		long adminId = adminDTO.getId();

		AddPhotoResponse response = new AddPhotoResponse();
		//Map<PhotoDTO, String> map = new HashMap<>();
		List<Long> photoIds = null;
		ProductCityInfoV2DTO pcInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		if(Strings.isNullOrEmpty(pcInfoV2DTO.getPhotoGroup())){
			/////////////////////////////////
			//addPhotoGroupId();
			///////////////////////////////
			photoIds = new ArrayList<>();
		}
		else{
			String photoGroups = pcInfoV2DTO.getPhotoGroup();
			String[] timeSlotStringArr = photoGroups.split(",");
			long[] timeSlotIntArr = Stream.of(timeSlotStringArr).mapToLong(Long::parseLong).toArray();
			photoIds = Arrays.stream(timeSlotIntArr).boxed()
					.collect(Collectors.toList());
		}
		for (PhotoFiles photoFile : photoFiles) {
			long photoFileStoreId = uploadFileV2(adminId,ImageCategory.PRODUCTDESCRIPTION.getId(),  photoFile.getPhoto1StoreFile(),
					photoFile.getPhoto1ByteSource(), locale);

			FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(photoFileStoreId);
			if (filstoreDTO != null) {
				PhotoDTO photoDTO = new PhotoDTO();
				photoDTO.setActive(true);
				photoDTO.setFileStoreId(filstoreDTO.getId());
				photoDTO.setImageType(ImageCategory.PRODUCTDESCRIPTION.getId());
				photoDTO.setItemId(pcInfoV2DTO.getId());
				photoDTO.setVersion("v2");
				photoDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
				photoDTO.setDeleted(false);
				photoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				photoDTO.setCreatedBy(adminId);
				photoDTO.setUpdatedBy(adminId);
				photoIds.add(photoDAO.addPhoto(photoDTO));
			}
		}

		//Long productDetailGroupId = null;
		String photoGroups = StringUtils.join(photoIds, ',');
		Long photoId = photoIds.get(0);
		ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO.getByProductIdQuantityUnit(productCityInfoV2DTO.getProductId(), productCityInfoV2DTO.getQuantityUnit());
		
		Long  productDetailGroupId = productCityList.stream().map(ProductCityInfoV2DTO::getProductDetailGroupId).filter(Objects::nonNull).findAny().orElse(0L);
		ProductDetailGroupDTO productDetailGroupDTO = productDetailGroupDAO
				.getById(productDetailGroupId);
		List<Long> productDetailIds = productCityList.stream().map(ProductCityInfoV2DTO::getId).distinct().collect(Collectors.toList());
		
		if (productDetailGroupDTO == null) {
			productDetailGroupId = addProductDetailGroupInfo(productDetailIds, adminDTO.getId());
		} else {
			updateProductDetailGroupInfo(productDetailGroupDTO, productDetailIds, adminDTO.getId());
			productDetailGroupId = productDetailGroupDTO.getId();
		}
		Long pdGroupId = productDetailGroupId;
		productCityList.stream().forEach(elt -> {
			elt.setProductDetailGroupId(pdGroupId);
			elt.setPhotoGroup(photoGroups);
		});
		
		//if(pcInfoV2DTO.getProductDetailGroupId() == null ||pcInfoV2DTO.getProductDetailGroupId() == 0){
			/*ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
			List<ProductCityInfoV2DTO> productCityDTOList = productCityInfoV2DAO.getByProductIdQuantityUnit(productCityInfoV2DTO.getProductId(),productCityInfoV2DTO.getQuantityUnit());
			List<Long> productDetailIds = productCityDTOList.stream().map(ProductCityInfoV2DTO::getId).distinct().collect(Collectors.toList());
			Long productDetailGroupId = addProductDetailGroupInfo(productDetailIds, adminDTO.getId());
			productCityDTOList.stream().forEach(elt -> {
				elt.setProductDetailGroupId(productDetailGroupId);
			});*/
		//}
		/*List<ProductCityInfoV2DTO> productCityInfoV2DTOs =  productCityInfoV2DAO.getByProductDetailGroupId(pcInfoV2DTO.getProductDetailGroupId());
		productCityInfoV2DTOs.stream().forEach(elt -> {
			elt.setPhotoGroup(photoGroups) ;
		    //elt.setPhotoId(photoId);
		});*/
		/*if(!Strings.isNullOrEmpty(primaryPhoto)){
			PhotoDTO photoDTO = p
		}
		*/
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));

		return response;
	}

	private Long addProductDetailGroupInfo(List<Long> productDetailIds, long adminId) {
		ProductDetailGroupDTO productDetailGroupDTO = new ProductDetailGroupDTO();
		productDetailGroupDTO.setProductDetailIds(StringUtils.join(productDetailIds, ','));
		productDetailGroupDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		productDetailGroupDTO.setCreatedBy(adminId);
		productDetailGroupDTO.setUpdatedBy(adminId);
		productDetailGroupDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return productDetailGroupDAO.addProductDetailGroupInfo(productDetailGroupDTO);

	}
	
	private void updateProductDetailGroupInfo(ProductDetailGroupDTO pDetailGroupDTO, List<Long> productDetailIds,
			long adminId) {
		String productDetailIdsInString = pDetailGroupDTO.getProductDetailIds();
		String[] productDetailIdsStrArr = productDetailIdsInString.split(",");
		long[] productDetailIdsLongArr = Stream.of(productDetailIdsStrArr).mapToLong(Long::parseLong).toArray();
		List<Long> productDetailIdListInSet = Arrays.stream(productDetailIdsLongArr).boxed().distinct().collect(Collectors.toList());
		List<Long> combinedProductDetailIds = ListUtils.union(productDetailIdListInSet, productDetailIds);
		combinedProductDetailIds = combinedProductDetailIds.stream().distinct().collect(Collectors.toList());
		pDetailGroupDTO.setProductDetailIds(StringUtils.join(combinedProductDetailIds, ","));
		pDetailGroupDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		pDetailGroupDTO.setUpdatedBy(adminId);
		productDetailGroupDAO.updateProductDetailGroupInfo(pDetailGroupDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreIdOfProduct(int imageType, long itemId, long productDetailsId, Locale locale)
			throws ValidationException {
		long fileStoreId = -1l;
		List<PhotoDTO> photoDTOList = photoDAO.getListByPhotoTypePhotoIdAndVersion(imageType, itemId, "v2");
		if (photoDTOList == null || photoDTOList.size() == 0) {
			} else {
			if (photoDTOList.size() == 1) {
				return photoDTOList.get(0).getFileStoreId();
			} else {
				for (PhotoDTO photoDTO : photoDTOList) {/*
					String productDetailsIds = photoDTO.getProductDetailIds();
					if (!Strings.isNullOrEmpty(productDetailsIds)) {
						String[] productDetailStringArray = productDetailsIds.split(",");
						for (String productDetailIdInString : productDetailStringArray) {
							if (Long.parseLong(productDetailIdInString) == productDetailsId) {
								return photoDTO.getFileStoreId();
							}
						}
					}
				*/}
				// return primary Photo if multiple images exist
				PhotoDTO photoDTO = photoDAO.getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto(imageType, itemId, "v2",
						true);
				if (photoDTO != null) {
					fileStoreId = photoDTO.getFileStoreId();
				}
			}
		}
		return fileStoreId;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetAllPhotosResponse getImageList(String authToken, long productDetailId, Locale locale) throws Exception {
		authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		ProductCityInfoV2DTO pcCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		String photoGroup = pcCityInfoV2DTO.getPhotoGroup();
		List<Long> photoIds = new ArrayList<>();
		if (!Strings.isNullOrEmpty(photoGroup)) {
			String[] timeSlotStringArr = photoGroup.split(",");
			long[] timeSlotIntArr = Stream.of(timeSlotStringArr).mapToLong(Long::parseLong).toArray();
			photoIds = Arrays.stream(timeSlotIntArr).boxed().collect(Collectors.toList());
		}
		String productDescription = "";
		if (photoIds.size() > 0) {
			List<PhotoDTO> photoDTOs = photoDAO.getByIds(photoIds);
			for (PhotoDTO dto : photoDTOs) {
				PhotoAllAttrs attrs = new PhotoAllAttrs();
				FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(dto.getFileStoreId());
				attrs.setPhotoId(dto.getId());
				attrs.setPrimaryPhoto(dto.isPrimaryPhoto());
				// attrs.setImageType(ImageCategory.fromId(dto.getImageType()).getId());
				attrs.setFileName(filstoreDTO.getFileName());
				attrs.setImageUpdatedOn(dto.getUpdatedOn().getTime());
				response.getPhotoAllAttrs().add(attrs);
			}
			
			
		} else {
			response.getPhotoAllAttrs().clear();
		}
		ProductCityInfoV2DTO pCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		if(pCityInfoV2DTO != null){
		ProductInfoV2DTO pInfoV2DTO = productInfoV2DAO
				.getByIdWithoutActiveState(pCityInfoV2DTO.getProductId());
		if (pInfoV2DTO != null &&  pInfoV2DTO.getDescription() != null) {
			productDescription = pInfoV2DTO.getDescription();
		}
		}
		response.setProductDescription(productDescription);

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);

		return response;
	}

	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public GetAllPhotosResponse getImageList(String authToken, int imageType, long itemId, Locale locale)
			throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		List<PhotoDTO> listPhotos = photoDAO.getListByPhotoTypePhotoIdAndVersion(imageType, itemId, "v2");
		for (PhotoDTO dto : listPhotos) {
			PhotoAllAttrs attrs = new PhotoAllAttrs();
			FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(dto.getFileStoreId());
			attrs.setPhotoId(dto.getId());
			attrs.setPrimaryPhoto(dto.isPrimaryPhoto());
			attrs.setImageType(ImageCategory.fromId(dto.getImageType()).getId());
			//attrs.setImageId(dto.getItemId());
			attrs.setFileName(filstoreDTO.getFileName());
			attrs.setImageUpdatedOn(dto.getUpdatedOn().getTime());
			response.getPhotoAllAttrs().add(attrs);
		}
		if (ImageCategory.fromId(listPhotos.get(0).getImageType()).getName()
				.equals(ImageCategory.PRODUCTDESCRIPTION.getName())) {
			ProductInfoV2DTO pInfoV2DTO = productInfoV2DAO
					.getByIdWithoutActiveState(listPhotos.get(0).getItemId().intValue());
			response.setProductDescription(pInfoV2DTO.getDescription()== null?"":pInfoV2DTO.getDescription());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		return response;
	}*/

	@Override
	@Transactional(rollbackFor = Exception.class)
	public long getFileStoreIdByPhotoId(int photoId, Locale locale) throws Exception {
		PhotoDTO photoDTO = photoDAO.getById(photoId);
		if (photoDTO != null) {
			return photoDTO.getFileStoreId();
		}
		return -1;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RetrieveFileResponse retrieveIndividualPhoto(int photoId, long fileStoreId, Locale locale) throws Exception {
		PhotoDTO photoDTO = photoDAO.getById(photoId);
		if (photoDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));
		}
		return fileStoreMgmt.retrieveFileV2(photoDTO.getImageType(), photoDTO.getItemId(), fileStoreId, true, locale);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ChangePrimaryPhotoResponse changePrimaryPhoto(String authToken,
			ChangePrimaryPhoto request, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(mobile);
		ChangePrimaryPhotoResponse response = new ChangePrimaryPhotoResponse();
		List<PhotoDTO> listPhotos = photoDAO.getListByPhotoTypePhotoIdAndVersion(ImageCategory.PRODUCTDESCRIPTION.getId(), request.getProductDetailId(), "v2");

		if (listPhotos.size() == 0)
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PHOTO_NOT_PRESENT, locale));
		PhotoDTO dto = photoDAO.getById(request.getPhotoId());

		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PHOTO_NOT_PRESENT, locale));
		} else {
			for (PhotoDTO pdto : listPhotos) {
				pdto.setPrimaryPhoto(false);
				photoDAO.updatePhoto(pdto);
			}
			dto.setPrimaryPhoto(true);
			photoDAO.updatePhoto(dto);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public Long getPhotoIdOfproductDetailId(Long productDetailId) {
		ProductCityInfoV2DTO pcInfo =  productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		if(pcInfo == null){
			return null;
		}
		PhotoDTO photoDTO = photoDAO.getByPhotoTypePhotoIdAndVersion(ImageCategory.PRODUCTDETAIL.getId(), pcInfo.getId(), "v2");
		if(photoDTO != null){
			return photoDTO.getId();
		}
		return null;
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AddPhotoResponse uploadPhoto(String authToken, int imageType, Long imageId, PhotoFiles photoFiles,
			Locale locale) throws Exception {
		// check authentication
		String mobileNumber = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));

		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobileNumber);
		long adminId = adminDTO.getId();

		AddPhotoResponse response = new AddPhotoResponse();
		long photo1FileStoreId = uploadFile(adminId, imageType, imageId, photoFiles.getPhoto1StoreFile(),
				photoFiles.getPhoto1ByteSource(), locale);

		FileStoreDTO filstoreDTO = fileStoreDAO.getFileById(photo1FileStoreId);

		// TODO: change it
		// checkConstraint(imageType, imageId, filstoreDTO.getFileName(),
		// locale);

		PhotoDTO photoDTO = new PhotoDTO();
		photoDTO.setActive(true);
		photoDTO.setItemId(imageId);
		photoDTO.setImageType(imageType);
		photoDTO.setFileStoreId(filstoreDTO.getId());
		photoDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		photoDTO.setDeleted(false);
		photoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		photoDTO.setCreatedBy(adminId);
		photoDTO.setUpdatedBy(adminId);
		photoDTO.setVersion("v1");
		// photoDTO.setPrimaryPhoto(false);

		long photoId = photoDAO.addPhoto(photoDTO);
		if(imageType == ImageCategory.SUBCATEGORY.getId()){
			SubCategoryInfoV2DTO subCategoryInfoV2DTO = subCategoryInfoV2DAO.getByIdWithoutActiveStatus(imageId.intValue());
			subCategoryInfoV2DTO.setPhotoId(photoId);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		LOG.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;}
	
	private long uploadFile(long adminId, int imageType, Long imageId, StoreFile storeFile, ByteSource source,
			Locale locale) throws Exception {
		if (storeFile != null && source != null) {
			return fileStoreMgmt.uploadFile(adminId, imageType, imageId, storeFile, source, locale);
		}
		return ApiUtils.INVALID_ID;
	}

	@Override
	public float setCompression(String imageCategory) {
		// TODO Auto-generated method stub
		return 0;
	}

	
}
