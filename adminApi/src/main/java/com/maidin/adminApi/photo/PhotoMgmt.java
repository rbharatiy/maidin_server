package com.maidin.adminApi.photo;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.maidin.adminApi.filestore.impl.RetrieveFileResponse;
import com.maidin.adminApi.photo.impl.PhotoFiles;
import com.maidin.common.exception.ValidationException;
import com.maidin.pojo.photo.AddPhotoResponse;
import com.maidin.pojo.photo.ChangePrimaryPhoto;
import com.maidin.pojo.photo.ChangePrimaryPhotoResponse;
import com.maidin.pojo.photo.DeletePhotoResponse;
import com.maidin.pojo.photo.GetAllPhotosResponse;


public interface PhotoMgmt {
	
	public AddPhotoResponse addPhoto(String authToken, int imageType , Long imageId , PhotoFiles photoFiles, Locale locale)
			throws Exception;
	
	public BufferedImage resizeImageWithType(BufferedImage originalImage, String imageType, long fileSize) throws IOException;
	
	public RetrieveFileResponse retrievePhoto(int imageType, long itemId, long fileStoreId, Locale locale) throws Exception;
	
	public long getFileStoreId(int imageType, long itemId,String version,  Locale locale) throws ValidationException ;

	public DeletePhotoResponse deletePhoto(String authToken, int imageType, long itemId, Locale locale)
			throws Exception;

	/*public AddPhotoResponse addMultiplePhotos(String authToken, int imageType, Long imageId,
			List<PhotoFiles> photoFileList, Locale locale) throws Exception;
*/
	AddPhotoResponse addMultiplePhotos(String authToken, Long productDetailId, List<PhotoFiles> photoFiles,
			String primaryPhoto, Locale locale) throws Exception;

	public long getFileStoreIdOfProduct(int imageType, long itemId, long productDetailsId, Locale locale) throws ValidationException;

	//public GetAllPhotosResponse getImageList(String authToken, int imageType, long itemId, Locale locale) throws Exception;
	
	

	public long getFileStoreIdByPhotoId(int photoId, Locale locale) throws Exception;

	public RetrieveFileResponse retrieveIndividualPhoto(int photoId, long fileStoreId, Locale locale) throws Exception;

	public ChangePrimaryPhotoResponse changePrimaryPhoto(String authToken, ChangePrimaryPhoto request, Locale locale) throws Exception;

	RetrieveFileResponse retrievePhotoV2(int imageType, long itemId, long fileStoreId, Locale locale) throws Exception;

	public DeletePhotoResponse deletePhotoByphotoId(String authToken, long photoId, Locale locale) throws Exception;

	public Long getPhotoIdOfproductDetailId(Long productDetailId);

	public GetAllPhotosResponse getImageList(String authToken, long productDetailId, Locale locale) throws Exception;

	public AddPhotoResponse uploadPhoto(String authToken, int imageType, Long imageId, PhotoFiles photoFiles,
			Locale locale) throws Exception;

	public float setCompression(String imageCategory);
	
//	public DeletePhotoResponse deletePhoto(String authToken, List<Long> photoIds, Locale locale) throws Exception;
	
	/*public UpdatePhotoResponse updatePhoto(String authToken, PhotoFiles photoFiles, Locale locale) throws Exception;

	public GetPhotoResponse getPhoto(String authToken, long forUser, Locale locale) throws Exception;

	public GetPrimaryPhotoResponse getPrimaryPhoto(String authToken, long parseLong, Locale locale) throws Exception;

	public ChangePrimaryPhotoResponse changePrimaryPhoto(String authToken, long parseLong, ChangePrimaryPhoto request,
			Locale locale) throws Exception;
	
	public GetAllPhotosResponse getAllPhotos(String authToken, long parseLong, Locale locale) throws Exception;
	
	public RetrieveFileResponse retrievePrimaryPhoto(
			//String authToken, 
			long parseLong, Locale locale) throws Exception;

	public RetrieveFileResponse getDemoPhoto(long parseLong, Locale locale) throws Exception;*/
}
