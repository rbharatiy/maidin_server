package com.maidin.adminApi.photo.impl;

import com.google.common.io.ByteSource;
import com.maidin.pojo.filestore.StoreFile;


public class PhotoFiles {

	private final StoreFile photo1StoreFile;
	private final ByteSource photo1ByteSource;

/*	private final StoreFile photo2StoreFile;
	private final ByteSource photo2ByteSource;

	private final StoreFile photo3StoreFile;
	private final ByteSource photo3ByteSource;

	private final StoreFile photo4StoreFile;
	private final ByteSource photo4ByteSource;

	private final StoreFile photo5StoreFile;
	private final ByteSource photo5ByteSource;*/

	
	public PhotoFiles(StoreFile photo1StoreFile, ByteSource photo1ByteSource/*, StoreFile photo2StoreFile,
			ByteSource photo2ByteSource, StoreFile photo3StoreFile, ByteSource photo3ByteSource,
			StoreFile photo4StoreFile, ByteSource photo4ByteSource, StoreFile photo5StoreFile,
			ByteSource photo5ByteSource*/) {

		this.photo1StoreFile = photo1StoreFile;
		this.photo1ByteSource = photo1ByteSource;

		/*this.photo2StoreFile = photo2StoreFile;
		this.photo2ByteSource = photo2ByteSource;

		this.photo3StoreFile = photo3StoreFile;
		this.photo3ByteSource = photo3ByteSource;

		this.photo4StoreFile = photo4StoreFile;
		this.photo4ByteSource = photo4ByteSource;

		this.photo5StoreFile = photo5StoreFile;
		this.photo5ByteSource = photo5ByteSource;*/
	}

	public StoreFile getPhoto1StoreFile() {
		return photo1StoreFile;
	}

	public ByteSource getPhoto1ByteSource() {
		return photo1ByteSource;
	}

	/*public StoreFile getPhoto2StoreFile() {
		return photo2StoreFile;
	}

	public ByteSource getPhoto2ByteSource() {
		return photo2ByteSource;
	}

	public StoreFile getPhoto3StoreFile() {
		return photo3StoreFile;
	}

	public ByteSource getPhoto3ByteSource() {
		return photo3ByteSource;
	}

	public StoreFile getPhoto4StoreFile() {
		return photo4StoreFile;
	}

	public ByteSource getPhoto4ByteSource() {
		return photo4ByteSource;
	}

	public StoreFile getPhoto5StoreFile() {
		return photo5StoreFile;
	}

	public ByteSource getPhoto5ByteSource() {
		return photo5ByteSource;
	}*/

}
