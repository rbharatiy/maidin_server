package com.maidin.adminApi.exception;

import java.util.Locale;
import java.util.ResourceBundle;

import com.maidin.common.exception.ExceptionDetail;

public class ExceptionResourceBundle {
	
	public static ExceptionDetail getExceptionCodeProperties(String propertyName, Locale locale) {
		//TODO: This should be optimized, the file being read everytime is not the right thing to go.
		//maybe atleast for english locale, we should do a different handling.
		ResourceBundle codeBundle = ResourceBundle.getBundle("api-exception-code");
		String code = codeBundle.getString(propertyName);
		ResourceBundle messageBundle = ResourceBundle.getBundle("api-exception-message", locale);
		String message = messageBundle.getString("ERROR_MESSAGE_" + code);
		ExceptionDetail exceptionDetail = new ExceptionDetail(code, message);
		return exceptionDetail;
	}
	
	public static String getExceptionMessage(String propertyName, Locale locale) {
		//TODO: This should be optimized, the file being read everytime is not the right thing to go.
		//maybe atleast for english locale, we should do a different handling.
		ResourceBundle codeBundle = ResourceBundle.getBundle("api-exception-code");
		String code = codeBundle.getString(propertyName);
		ResourceBundle messageBundle = ResourceBundle.getBundle("api-exception-message", locale);
		String message = messageBundle.getString("ERROR_MESSAGE_" + code);
		return message;
	}

	public static ExceptionDetail getDynamicExceptionCodeProperties(String value,
			String propertyName, Locale locale) {
		//TODO: This should be optimized, the file being read everytime is not the right thing to go.
		//maybe atleast for english locale, we should do a different handling.
		ResourceBundle codeBundle = ResourceBundle.getBundle("api-exception-code");
		String code = codeBundle.getString(propertyName);
		ResourceBundle messageBundle = ResourceBundle.getBundle("api-exception-message", locale);
		String message = messageBundle.getString("ERROR_MESSAGE_" + code);
		ExceptionDetail exceptionDetail = new ExceptionDetail(code, value.concat(" ").concat(message));
		return exceptionDetail;
	}
}