package com.maidin.adminApi.exception;

public class ExceptionCode {

	public static final String NO_UPDATE_IN_MOBILE = "NO_UPDATE_IN_MOBILE";
	public static final String MOBILE_ALREADY_IN_USE = "MOBILE_ALREADY_IN_USE";
	public static final String INVALID_MOBILE_NUMBER = "INVALID_MOBILE_NUMBER";
	public static final String CUSTOMER_DOES_NOT_EXIST = "CUSTOMER_DOES_NOT_EXIST";
	public static final String ALREADY_REGISTERED = "ALREADY_REGISTERED";
	public static final String CUSTOMER_ID_IS_DEACTIVATED = "CUSTOMER_ID_IS_DEACTIVATED";
	public static final String NO_UPDATE_IN_EMAIL = "NO_UPDATE_IN_EMAIL";
	public static final String EMAIL_ALREADY_IN_USE = "EMAIL_ALREADY_IN_USE";
	public static final String INVALID_CODE = "INVALID_CODE";
	public static final String INVALID_OTP = "INVALID_OTP";
	public static final String INVALID_DATE_FORMAT = "INVALID_DATE_FORMAT";
	public static final String INVALID_DATE_TIME_FORMAT = "INVALID_DATE_TIME_FORMAT";
	public static final String INVALID_IMAGE = "INVALID_IMAGE";
	public static final String CANNOT_SET_ORDER_OF_PAST = "CANNOT_SET_ORDER_OF_PAST";
	public static final String ORDERED_TO_DATE_MUST_BE_AFTER_ORDERED_FROM_DATE = "ORDERED_TO_DATE_MUST_BE_AFTER_ORDERED_FROM_DATE";
	public static final String CANNOT_CHECKOUT_PAST_OR_FUTURE_DAYS = "CANNOT_CHECKOUT_PAST_OR_FUTURE_DAYS";
	public static final String LOW_WALLET_BALANCE = "LOW_WALLET_BALANCE";
	public static final String INVALID_ORDER = "INVALID_ORDER";
	public static final String PERMISSION_DENIED = "PERMISSION_DENIED";
	public static final String EMAIL_MISSING = "EMAIL_MISSING";
	public static final String ADMIN_ID_IS_DEACTIVATED = "ADMIN_ID_IS_DEACTIVATED";
	public static final String ADMIN_DOES_NOT_EXIST = "ADMIN_DOES_NOT_EXIST";
	public static final String USER_NOT_REGISTERED_WITH_MOBILE = "USER_NOT_REGISTERED_WITH_MOBILE";
	public static final String USER_NOT_REGISTERED_WITH_ADDRESS = "USER_NOT_REGISTERED_WITH_ADDRESS";
	public static final String NO_BRAND_EXISTS = "NO_BRAND_EXISTS";
	public static final String CANNOT_DEDUCT = "CANNOT_DEDUCT";
	public static final String CITYPROJECT_JSON_ERROR = "CITYPROJECT_JSON_ERROR";
	public static final String TOWER_JSON_ERROR = "TOWER_JSON_ERROR";
	
	public static final String FILE_ALREADY_EXIST = "FILE_ALREADY_EXIST";
	public static final String FILE_DOES_NOT_EXIST = "FILE_DOES_NOT_EXIST";
	public static final String MD5_DOES_NOT_MATCH = "MD5_DOES_NOT_MATCH";
	public static final String INVALID_FILE_NAME = "INVALID_FILE_NAME";
	public static final String INVALID_VALUE_OF_PRIMARYPHOTO = "INVALID_VALUE_OF_PRIMARYPHOTO";
	public static final String FILE_DOWNLOAD_FAILED = "FILE_DOWNLOAD_FAILED";
	//public static final String MANDATORY_FIELD_MISSING = "MANDATORY_FIELD_MISSING";
	public static final String INVALID_AMOUNT = "INVALID_AMOUNT";
	public static final String FCM_HTTP_HIT_FAILED = "FCM_HTTP_HIT_FAILED";
	public static final String PHOTO_NOT_PRESENT = "PHOTO_NOT_PRESENT";
	public static final String MULTIPLE_IMAGES_EXIST = "MULTIPLE_IMAGES_EXIST";
	public static final String MULTIPLE_IMAGES_EXIST_OF_PRODUCTDETAILSID = "MULTIPLE_IMAGES_EXIST_OF_PRODUCTDETAILSID";
	public static final String PRODUCTDETAIL_GROUP_NOT_SET = "PRODUCTDETAIL_GROUP_NOT_SET";
	public static final String BRAND_ALREADY_EXIST = "BRAND_ALREADY_EXIST";
	public static final String PRODUCT_ALREADY_EXIST = "PRODUCT_ALREADY_EXIST";
	public static final String PRODUCTDETAILID_NOT_EXIST = "PRODUCTDETAILID_NOT_EXIST";
	public static final String PRODUCTID_NOT_EXIST = "PRODUCTID_NOT_EXIST";
	public static final String BRANDID_DOES_NOT_EXIST = "BRANDID_DOES_NOT_EXIST";
	public static final String BRANDID_AND_BRANDNAME_MISMATCH = "BRANDID_AND_BRANDNAME_MISMATCH";
	public static final String PRODUCT_NOT_AVAILABLE = "PRODUCT_NOT_AVAILABLE";
	public static final String PRODUCTDETAILS_NOT_AVAILABLE = "PRODUCTDETAILS_NOT_AVAILABLE";
	public static final String CATEGORY_ID_MISSING = "CATEGORY_ID_MISSING";
	public static final String SUBCATEGORY_ID_MISSING = "SUBCATEGORY_ID_MISSING";
	public static final String QUANTITYUNIT_MISSING = "QUANTITYUNIT_MISSING";
	public static final String CITYCODE_MISSING = "CITYCODE_MISSING";
	//public static final String MRP_SHOULD_NOT_BE_GREATERTHAN_SALEPRICE = null;
	public static final String SALEPRICE_SHOULD_NOT_BE_GREATERTHAN_MRP = "SALEPRICE_SHOULD_NOT_BE_GREATERTHAN_MRP";
	public static final String PAYMENTMODE_CANNOT_BE_WALLET = "PAYMENTMODE_CANNOT_BE_WALLET";
	
}