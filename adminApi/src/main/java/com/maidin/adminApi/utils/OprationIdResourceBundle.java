package com.maidin.adminApi.utils;

import java.util.Locale;
import java.util.ResourceBundle;

public class OprationIdResourceBundle {

	public static String getOperationId(String propertyName, Locale locale) {

		ResourceBundle operationIdBundle = ResourceBundle.getBundle("adminService-operation-id", locale);
		String operationId = operationIdBundle.getString(propertyName);
		return operationId;	
	}
	
	public static String getOperationId(String propertyName) {

		ResourceBundle operationIdBundle = ResourceBundle.getBundle("adminService-operation-id");
		String operationId = operationIdBundle.getString(propertyName);
		return operationId;	
	}
	
}