package com.maidin.adminApi.utils;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.Map;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpStatus;
import org.codehaus.jackson.map.ObjectMapper;
import org.imgscalr.Scalr;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.maidin.adminApi.enums.ImageCategory;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;


public class CommonApplication {

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	public void sendPushNotificationDataToCustomer(JSONObject payload, String key, Long Id, String userType)
			throws IOException, JSONException {
		int responseCode = -1;
		JSONObject responseBody = null;
		try {
			System.out.println("Sending FCM request");

			// String payload =
			// getPayloadRideDetails(tdo.getDeviceToken(),title,
			// pushNotificationMessage, response);
			URL url = new URL(prop.getProperty(ResourceProperties.FCM_URL));
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");
			// httpURLConnection.setRequestProperty("Content-Length",
			// Integer.toString(payload.length()));
			httpURLConnection.setRequestProperty("Authorization", "key=" + key);
			OutputStream out = httpURLConnection.getOutputStream();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(out, "utf-8");

			System.out.println("Payload : " + payload);
			outputStreamWriter.write(payload.toString());
			outputStreamWriter.flush();
			out.close();
			responseCode = httpURLConnection.getResponseCode();
			if (responseCode == HttpStatus.SC_OK) {
				// responseBody =
				// convertStreamToString(httpURLConnection.getInputStream());
				responseBody = convertStreamToJSON(httpURLConnection.getInputStream());
				System.out.println("FCM message sent : " + responseBody);
				// recordPushNotificationTraveller(payload, responseBody, Id,
				// userType);
			} else {
				// responseBody =
				// convertStreamToString(httpURLConnection.getErrorStream());
				responseBody = convertStreamToJSON(httpURLConnection.getErrorStream());
				System.out.println("FCM message Error Message : " + responseBody);
				// recordPushNotificationTraveller(payload, responseBody, Id,
				// userType);
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JSONObject convertStreamToJSON(InputStream inStream) throws Exception {

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> jsonMap = mapper.readValue(inStream, Map.class);
		return new JSONObject(jsonMap);
	}

	public JSONObject getPayloadDataOnly(String token, JSONObject response) {
		JSONObject jsonObject = new JSONObject();
		try {
			JSONObject data = new JSONObject();
			jsonObject.put("data", response);
			jsonObject.put("to", token);
			jsonObject.put("priority", "high");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

	public JSONObject getPayloadNotificationOnly(String token, String title, String body) {
		JSONObject jsonObject = new JSONObject();
		try {
			JSONObject notification = new JSONObject();
			notification.put("title", title);
			notification.put("body", body);
			jsonObject.put("notification", notification);
			jsonObject.put("to", token);
			jsonObject.put("priority", "high");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

	public JSONObject getNotificationAndDataPayload(String token, String title, String body, String notificationType) {
		JSONObject jsonObject = new JSONObject();
		try {
			JSONObject notification = new JSONObject();
			notification.put("title", title);
			notification.put("body", body);

			JSONObject data = new JSONObject();
			data.put("notificationType", notificationType);

			jsonObject.put("data", data);
			jsonObject.put("notification", notification);
			jsonObject.put("to", token);
			jsonObject.put("priority", "high");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jsonObject;
	}

	public BufferedImage resizeImageWithType(BufferedImage originalImage, String imageType) {
		if(!imageType.equalsIgnoreCase(ImageCategory.CATEGORY.getName())){
		int imageWidth = originalImage.getWidth(null);
		int imageHeight = originalImage.getHeight(null);
		int larger = checkLargerOfTwoNumbers(imageWidth, imageHeight);
		Dimension dimension = setImageRatio(larger, imageWidth, imageHeight, imageType);
	
		
		originalImage = Scalr.resize(originalImage, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, dimension.width,
				dimension.height);
		}
		return originalImage;
	}

	public Dimension setImageRatio(int larger, int imageWidth, int imageHeight, String imageType) {
		Dimension dimension = new Dimension(imageWidth, imageHeight);
		if(imageType.equals(ImageCategory.CATEGORY.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.CATEGORY_PIXEL_RANGE_5001)));
			}
			
		}
		else if(imageType.equals(ImageCategory.BRAND.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.BRAND_PIXEL_RANGE_5001)));
			}
			
		}
		else if(imageType.equals(ImageCategory.PRODUCT.getName())){

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCT_PIXEL_RANGE_5001)));
			}
			
		}
		
		else if (imageType.equals(ImageCategory.PRODUCTDESCRIPTION.getName())) {

			if (larger > 0 && larger < 401) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_0_250)));
			} else if (larger > 400 && larger < 801) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_251_500)));
			} else if (larger > 800 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDESCRIPTION_PIXEL_RANGE_5001)));
			}

		}
		
		else if (imageType.equals(ImageCategory.PRODUCTDETAIL.getName())) {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Integer
						.parseInt(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight,
						Float.parseFloat(prop.getProperty(ResourceProperties.PRODUCTDETAIL_PIXEL_RANGE_5001)));
			}

		}
		
		else {

			if (larger > 0 && larger < 251) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250)));
			} else if (larger > 250 && larger < 501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500)));
			} else if (larger > 500 && larger < 1001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000)));
			} else if (larger > 1000 && larger < 2501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500)));
			} else if (larger > 2500 && larger < 3501) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500)));
			} else if (larger > 3500 && larger < 5001) {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000)));
			} else {
				dimension = setImageDimension(larger, imageWidth, imageHeight, Float.parseFloat(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001)));
			}
			
		}
		
		System.out.println(larger + "----" + imageWidth + " --- " + imageHeight);
		return dimension;

	}

	/*public Dimension setImageDimension(int larger, int imageWidth, int imageHeight, int i) {
		imageHeight = imageHeight / i;
		imageWidth = imageWidth / i;
		Dimension dimension = new Dimension(imageWidth, imageHeight);
		return dimension;

	}*/
	
	public Dimension setImageDimension(float larger, float imageWidth, float imageHeight, float i) {
		int imageHeight1 = (int) (imageHeight / i);
		int imageWidth1 =  (int) (imageWidth / i);
		Dimension dimension = new Dimension(imageWidth1, imageHeight1);
		return dimension;

	}

	public int checkLargerOfTwoNumbers(int imageWidth, int imageHeight) {
		int larger = imageWidth;
		if (imageWidth > imageHeight) {
			larger = imageWidth;
		} else {
			larger = imageHeight;
		}
		return larger;
	}
	
	public String toTitleCase(String input) {
	    StringBuilder titleCase = new StringBuilder();
	    boolean nextTitleCase = true;

	    for (char c : input.toCharArray()) {
	        if (Character.isSpaceChar(c)) {
	            nextTitleCase = true;
	        } else if (nextTitleCase) {
	            c = Character.toTitleCase(c);
	            nextTitleCase = false;
	        }

	        titleCase.append(c);
	    }

	    return titleCase.toString();
	}



/*	private final int PIXEL_RANGE_0_250 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_0_250));
	private final int PIXEL_RANGE_251_500 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_251_500));
	private final int PIXEL_RANGE_501_1000 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_501_1000));
	private final int PIXEL_RANGE_1001_2500 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_1001_2500));
	private final int PIXEL_RANGE_2501_3500 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_2501_3500));
	private final int PIXEL_RANGE_3501_5000 = Integer
			.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_3501_5000));
	private final int PIXEL_RANGE_5001 = Integer.parseInt(prop.getProperty(ResourceProperties.PIXEL_RANGE_5001));*/

}