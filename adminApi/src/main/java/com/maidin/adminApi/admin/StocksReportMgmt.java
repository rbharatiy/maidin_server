package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.reports.PurchaseOrderInfoRequest;

public interface StocksReportMgmt {

	public OrderReportResponse getPurchaseOrderReport(PurchaseOrderInfoRequest request, String authToken, Locale locale)
			throws Exception;

	GenericResponse batchUpdateWalletLedger() throws Exception;	

}
