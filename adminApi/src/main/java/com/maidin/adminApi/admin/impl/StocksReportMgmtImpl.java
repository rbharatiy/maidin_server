package com.maidin.adminApi.admin.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Locale.Category;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.google.common.base.Strings;
import com.maidin.adminApi.admin.StocksReportMgmt;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
//import com.maidin.adminApi.utils.XlsxBuilder.StyleAttribute;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.EmailSenderWithAttachment;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.MaidinMoneyLedgerDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.impl.MaidinMoneyLedgerDAOImpl;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.MaidinMoneyLedgerDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.reports.PurchaseOrder;
import com.maidin.pojo.reports.PurchaseOrderInfoRequest;
import com.maidin.pojo.reports.PurchaseOrderInfoResponse;

public class StocksReportMgmtImpl implements StocksReportMgmt {


	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final WalletDAO walletDAO;
	private final MaidinMoneyLedgerDAO maidinMoneyLedgerDAO;
	private final ProductCategoryDAO categoryDAO;
	private final org.json.simple.JSONArray timeSlotJsonArray;

	@Autowired
	public StocksReportMgmtImpl(AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO,
			TimeSlotDAO timeSlotDAO, WalletDAO walletDAO, 
			MaidinMoneyLedgerDAO maidinMoneyLedgerDAO, ProductCategoryDAO categoryDAO) {

		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.walletDAO = walletDAO;
		this.maidinMoneyLedgerDAO = maidinMoneyLedgerDAO;
		this.categoryDAO = categoryDAO;
		this.timeSlotJsonArray = readTimeSlotJSON(System.getProperty("config.dir") + "/json/allLists.json");
	}

	private static final Logger logger = LoggerFactory.getLogger(StocksReportMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public OrderReportResponse getPurchaseOrderReport(PurchaseOrderInfoRequest request, String authToken, Locale locale) throws Exception {
		
		OrderReportResponse response = new OrderReportResponse();
		// String mobile = "9811611743";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(request));
		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		
		List<String> timeSlotIdList = timeSlotDAO.getByIds(request.getTimeSlotIdList());
		new Thread(() -> {
			try {
				callAsynchronouslyStocksReportV2(request, locale, adminDTO, timeSlotIdList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Async
	private void callAsynchronouslyStocksReportV2(PurchaseOrderInfoRequest request, Locale locale, AdminDTO adminDTO, List<String> timeSlotIdList)
			throws InterruptedException, ExecutionException {
//		List<String> timeSlotIdList = timeSlotDAO.getByIds(request.getTimeSlotIdList());
		try {
			PurchaseOrderInfoResponse reportResponse = new PurchaseOrderInfoResponse();
			reportResponse = generateMultipleDayOrderReportV2(request, timeSlotIdList, locale);
			convertMultipleDayOrderReportToExcel(request, reportResponse, timeSlotIdList, adminDTO);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	private void setExcelCellBorder(CellStyle cellStyle) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
	}
	
	private void setExcelCellBorderWithColor(CellStyle cellStyle, short indexedColor) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(indexedColor);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(indexedColor);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(indexedColor);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(indexedColor);
	}

	private int createOrderReportHeader(SXSSFSheet sheet, int rowCount, PurchaseOrderInfoRequest request, List<String> timeSlotIdList, String adminName, String displayDate) {
		logger.info("HEADER creation started");
		
		
		
		///
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		
		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorderWithColor(cellStyle, IndexedColors.GREY_80_PERCENT.getIndex());
		///
		
		
		Row dateTimeSlotRow = sheet.createRow(rowCount);

		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue(displayDate);
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));

		rowCount++;
		Row row = sheet.createRow(rowCount);

		int cellSequenc = 0;
		Cell serialNumber = row.createCell(cellSequenc);
		serialNumber.setCellValue("S.No");
		serialNumber.setCellStyle(cellStyle);
		
		cellSequenc += 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Required Qty");
		quantityHeader.setCellStyle(cellStyle);
		
		cellSequenc += 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Buying Price/Unit");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Selling Price/Unit");
		projectHeader.setCellStyle(cellStyle);


		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("New Selling Price");
		flatHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
		logger.info("HEADER creation end");
		
		return rowCount;
	}
	
	public static org.json.simple.JSONArray readTimeSlotJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray timeSlotArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			timeSlotArray = (org.json.simple.JSONArray) jsonObject.get("timeSlotList");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return timeSlotArray;
	}

	@Transactional(rollbackFor = Exception.class)
	private PurchaseOrderInfoResponse generateMultipleDayOrderReportV2(PurchaseOrderInfoRequest request, List<String> timeSlotIdList, Locale locale)
			throws InterruptedException, ExecutionException {
		
		PurchaseOrderInfoResponse response = new PurchaseOrderInfoResponse();
		
		StringJoiner joiner_TimeSlots = new StringJoiner("','","'", "'");
        for(String s: timeSlotIdList)
        	joiner_TimeSlots.add(s);
        StringJoiner joiner_CategoryIds = new StringJoiner(",");
        for(Integer s: request.getCategoryIdList())
        	joiner_CategoryIds.add(s.toString());
        
        
		List<Object[]> list = new ArrayList<>();
		list = orderInfoDAO.getPurchaseReport(new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), joiner_TimeSlots.toString(), joiner_CategoryIds.toString());
	
//		double categoryId = Double.parseDouble(list.get(0)[5].toString());
		for(Object[] record: list)
		{
//		list.stream().forEach((record) -> {
			PurchaseOrder purchaseOrder = new PurchaseOrder();
//			String namedQuery = "SELECT  pr.Id - 0, pr.productName - 1, pr.quantityUnit - 2, sum(od.Quantity) as quantity - 3  " 
			purchaseOrder.setProduct(String.valueOf(record[1].toString()));
			purchaseOrder.setUnit(String.valueOf(record[2].toString()));
			purchaseOrder.setQuantity(Integer.parseInt(record[3].toString()));
			purchaseOrder.setSellingPrice(Double.parseDouble(record[4].toString()));
			purchaseOrder.setCategory(String.valueOf(record[5].toString()));
//			if(Double.parseDouble(record[5].toString()) != categoryId){
//				categoryId
//			}
			
			response.getPurchaseOrderList().add(purchaseOrder);
			purchaseOrder = null;
		}
//		});
		System.out.println("----------------------" + list.size());
		list = null;
		return response;
	}

	@Transactional
	private void convertMultipleDayOrderReportToExcel(PurchaseOrderInfoRequest request, PurchaseOrderInfoResponse response, List<String> timeSlotIdList,
			AdminDTO adminDTO) throws MessagingException, JSONException, FileNotFoundException, IOException {

		logger.info("Excel creation started with formatting");

		// XSSFWorkbook workbook = new XSSFWorkbook();
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet = workbook.createSheet("PurchaseOrderReport");
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = (XSSFCellStyle) workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = (XSSFCellStyle) workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = (XSSFCellStyle) workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		Font font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
//		AtomicInteger rowCount = new AtomicInteger();
		
		// Create Header Title of Sheet 
		
		String categoryName = "";
		if(request.getCategoryIdList().size() > 1)
			categoryName = "All Categories ";
		else 
			categoryName = categoryDAO.getById(request.getCategoryIdList().get(0)).getCategory();
		
		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));
		String displayDate = fromDate.equalsIgnoreCase(toDate) ? fromDate : fromDate + " - " + toDate ;
		String headerTitle = ("Purchase Report | "+ categoryName + " | " + "Date: " + displayDate + " | " 
				+ (timeSlotJsonArray.size()-1 == timeSlotIdList.size() ? "All timeslots" : timeSlotIdList)
				+ ".  Downloaded by - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())));
		
		int rowCount = 0;
		rowCount = createOrderReportHeader(sheet, rowCount, request, timeSlotIdList, adminDTO.getFullName(), headerTitle);
		short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
		
		int serialNo=1 ;
//		response.getPurchaseOrderList().stream().forEach((rec) -> {
			for(PurchaseOrder rec: response.getPurchaseOrderList()) {

			Row row2 = sheet.createRow(++rowCount);

			cellStyle.setDataFormat(dateFormat);
			
			int cellSequenc = 0;
			
			Cell serialNumber = row2.createCell(cellSequenc);
			serialNumber.setCellValue(serialNo++);
			serialNumber.setCellStyle(integerValueStyle);
			
			cellSequenc += 1;
			Cell productHeader = row2.createCell(cellSequenc);
			productHeader.setCellValue(rec.getProduct());
			productHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell unitHeader = row2.createCell(cellSequenc);
			unitHeader.setCellValue(rec.getUnit());
			unitHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell quantityHeader = row2.createCell(cellSequenc);
			quantityHeader.setCellValue(rec.getQuantity());
			quantityHeader.setCellStyle(integerValueStyle);
			
			cellSequenc += 1;
			Cell towerHeader = row2.createCell(cellSequenc);
			towerHeader.setCellStyle(integerValueStyle);

			cellSequenc += 1;
			Cell projectHeader = row2.createCell(cellSequenc);
			projectHeader.setCellValue((float)rec.getSellingPrice());
			projectHeader.setCellStyle(integerValueStyle);


			cellSequenc = cellSequenc + 1;
			Cell flatHeader = row2.createCell(cellSequenc);
			flatHeader.setCellStyle(integerValueStyle);
			}
//		});

			// limit product name width to 25 characters
			sheet.setColumnWidth(0, 256 * 6);
		
				// limit product name width to 25 characters
				sheet.setColumnWidth(1, 256 * 25);

				// limit unit width to 10 characters
				sheet.setColumnWidth(2, 256 * 16);

				// limit quantity width to 5 characters
				sheet.setColumnWidth(3, 256 * 12);

				// limit buying price/Unit width to 8 characters
				sheet.setColumnWidth(4, 256 * 12);

				// limit Selling Price/Unit width to 16 characters
				sheet.setColumnWidth(5, 256 * 12);

				// limit New Todays Selling Price/Unit width to 16 characters
				sheet.setColumnWidth(6, 256 * 12);


		logger.info("Excel creation END with formatting");
		
		File file = new File(categoryName + "PurchaseOrderReport-" + displayDate + "-" + (timeSlotJsonArray.size()-1 == timeSlotIdList.size()
				? "All timeslots" : timeSlotIdList) + ".xlsx");
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
			outputStream.flush();
			workbook.dispose();
			workbook.close();
			// workbook = new SXSSFWorkbook(new FileInputStream(file));
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Purchase Report | "+categoryName+ " | " + "Date : " + displayDate + " | Timeslot : "
				+ timeSlotIdList;
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);
		logger.info("Order report email SENT");

	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse batchUpdateWalletLedger() throws Exception {
		GenericResponse response = new GenericResponse();
		//System.out.println("Batch Update Wallet Ledger Service starting at: " + java.time.LocalDateTime.now());
		logger.info(ApiUtils.REQUEST_PAYLOAD, "Batch Update Wallet Ledger Service starting at: "+java.time.LocalDateTime.now());
		/*System.out.println("Batch Started for price deduction for schedule/checkout at "
				+ new Date(System.currentTimeMillis()));*/
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		
		double negativeBalance = walletDAO.getNegativeMadinMoney();
		double positiveBalance = walletDAO.getPositiveMadinMoney();
		
		MaidinMoneyLedgerDTO mDTO = maidinMoneyLedgerDAO.getByDate(new Date(System.currentTimeMillis()));
		if(mDTO != null){
			mDTO.setNegativeBalance(negativeBalance);
			mDTO.setPositiveBalance(positiveBalance);
			mDTO.setDate(new Date(System.currentTimeMillis()));
			mDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			maidinMoneyLedgerDAO.updateBalance(mDTO);
		} else{
			MaidinMoneyLedgerDTO maidinMoneyDTO = new MaidinMoneyLedgerDTO();
			maidinMoneyDTO.setNegativeBalance(negativeBalance);
			maidinMoneyDTO.setPositiveBalance(positiveBalance);
			maidinMoneyDTO.setDate(new Date(System.currentTimeMillis()));
			maidinMoneyDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
			maidinMoneyLedgerDAO.addEntryInLedger(maidinMoneyDTO);
		}
		
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

}
