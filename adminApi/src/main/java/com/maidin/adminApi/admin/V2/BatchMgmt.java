package com.maidin.adminApi.admin.V2;

import java.util.List;

//import com.maidin.adminService.admin.V2.List;
import com.maidin.pojo.GenericResponse;

public interface BatchMgmt {
	
	public void addMonthlyOrderInSchedule() throws Exception;
	
	public GenericResponse batchPriceDeductionAtSlotStart(List<Integer> timeSlotId);

	public List<Integer> getTimeSlotIdListByStartTime(int hour);

	//public List<Integer> getTimeSlotIdListByStartTime(int hour);
}
