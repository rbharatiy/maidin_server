package com.maidin.adminApi.admin.V2;

import java.util.Locale;

import com.maidin.pojo.inventory.PaymentOptionResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.adminv2.DashboardEssentialsResponse;
import com.maidin.pojo.adminv2.GetCategoryV2Request;
import com.maidin.pojo.adminv2.UpdateProductRequest;
import com.maidin.pojo.adminv2.UpdateProductResponse;
import com.maidin.pojo.inventory.BrandListV2Response;
import com.maidin.pojo.inventory.CategoryListV2Response;
import com.maidin.pojo.inventory.GetProductListV2Request;
import com.maidin.pojo.inventory.GetSubCategoryV2Request;
import com.maidin.pojo.inventory.ProductListV2Response;
import com.maidin.pojo.inventory.SubCategoryListV2Response;
import com.maidin.pojo.inventory.UpsertBrandRequest;
import com.maidin.pojo.photo.GetAllPhotosResponse;
import com.maidin.pojo.product.BrandListResponse;
import com.maidin.pojo.product.CategoryListResponse;
import com.maidin.pojo.product.GetBrandRequest;

public interface InventoryMgmt {

	public CategoryListV2Response getCategoryListV2(GetCategoryV2Request request, String authToken, Locale locale) throws Exception;

	public SubCategoryListV2Response getSubCategoryListV2(String authToken, GetSubCategoryV2Request request/*,Integer pageNum*/, Locale locale)  throws Exception;

	public ProductListV2Response getProductListV2(String authToken, GetProductListV2Request request,Integer pageNum, Locale locale) throws Exception;

	public DashboardEssentialsResponse getDashboardList(String authToken, Locale locale) throws Exception;

	public BrandListV2Response getBrandListV2(String authToken, Locale locale) throws Exception;

	public GenericResponse upsertBrand(String authToken, UpsertBrandRequest request, Locale locale) throws Exception; 

	

	
}
