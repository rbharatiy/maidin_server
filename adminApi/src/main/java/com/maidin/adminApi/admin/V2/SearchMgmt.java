package com.maidin.adminApi.admin.V2;

import java.util.List;
import java.util.Locale;

import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;

public interface SearchMgmt {

	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception;

	List<PopularSearchesDTO> getAllPopularSearchKeywords(String authToken, Locale locale) throws Exception;
}
