package com.maidin.adminApi.admin.V2;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.adminv2.ChangeStatusRequest;
import com.maidin.pojo.adminv2.UpdateProductRequest;
import com.maidin.pojo.adminv2.UpdateProductResponse;

public interface ProductUpdateMgmt {
	
	
	public UpdateProductResponse upsertProduct(String authToken, UpdateProductRequest request, Locale locale) throws Exception;

	public GenericResponse changeStatus(String authToken, ChangeStatusRequest request, Locale locale) throws Exception;
}
