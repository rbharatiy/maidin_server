package com.maidin.adminApi.admin.V2;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;

public interface DashboardMgmt {
	
	public CustomerCountInfoResponse getDashboardInfo(String authToken, Locale locale) throws Exception;
}
