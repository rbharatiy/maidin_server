package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.OrderUpdate;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.adminv2.UpsertWalletRequest;
//import com.maidin.pojo.adminv2.UpsertWalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.ChangeDeliveredOrderStateRequest;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.UpdateProductRequest;

public interface AdminMgmt {

	public GenerateMobileOtpResponse initiateGenerateMobileOTP(GenerateMobileOtp request, Locale locale) throws Exception;

	public VerifyMobileOtpResponse initiateVerifyMobileOtp(VerifyMobileOtp request, Locale locale) throws Exception;

	/*public CustomerRegistrationResponse registration(CustomerRegistration request, String authToken, Locale locale);

	public GetWalletResponse getWalletInfo(String authToken, Locale locale);*/

	public GetWalletResponse upsertWalletInfo(String authToken, UpsertWalletRequest request, Locale locale) throws Exception;

	//public GenericResponse getOrderReport(OrderRequest request, String authToken, Locale locale);

	public OrderReportResponse getOrderReport(OrderRequest request, String authToken, Locale locale) throws Exception;

	public RegenerateAuthTokenResponse regenAuthToken(RegenerateAuthToken request, Locale locale)  throws Exception;

	public DeviceTokenResponse saveDeviceToken(DeviceToken request, String authToken, Locale locale)  throws Exception;

	public AddressInfoResponse getCustomerAddressInfo(String authToken, AddressInfoRequest request, Locale locale)  throws Exception;

	public CustomerCountInfoResponse getCustomerCountInfo(String authToken, Locale locale)  throws Exception;

	public CustomerCountInfoResponse getCustomerCountByParameters(String authToken, CustomerCountRequest request,
			Locale locale);

	public GenericResponse changeFlag(String authToken, ItemRequest request, Locale locale) throws Exception;

	public CategoryListResponse getCategory(String authToken, Locale locale)  throws Exception;

	public BrandListResponse getBrands(String authToken, GetBrandRequest request, Locale locale) throws Exception;

	public ProductListResponse getProductList(String authToken, GetProductRequest request, Locale locale) throws Exception;

	public GenericResponse getCustomerRecords(String authToken, Locale locale) throws Exception;

	/*public GenericResponse markOrderDelivered(String authToken, OrderUpdate request, Locale locale) throws Exception;*/
	public GenericResponse markOrderDelivered(String authToken, MarkOrderRequest request, Locale locale) throws Exception;

//	public GetOrderResponse getOrderList(String authToken, GetOrderRequest request, Locale locale)  throws Exception;

	public ProductResponse updateProduct(String authToken, UpdateProductRequest request, Locale locale)  throws Exception;

	public void addOrderInSchedule() throws Exception;

	public void addParticularDayInSchedule(Long timeStamp);

	//public GenerateMobileOtpResponse initiateGenerateMobileOTP(GenerateMobileOtp request, Locale locale);
	
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale) throws Exception;

	//public GenericResponse batchPriceDeductionForScheduledOrders(int timeSlotId);

	public OrderReportResponse getOrderReportV2(OrderRequest request, String authToken, Locale locale)  throws Exception;

	public GenericResponse markDeliveryApiV2(String authToken, MarkOrder request, Locale locale) throws Exception;

	public GenericResponse markOrderDeliveredWithQtyUpdateV2(String authToken, MarkOrderWithQtyUpdateRequest request, Locale locale) throws Exception;

	public GenericResponse changeDeliveredOrderStateV2(String authToken, ChangeDeliveredOrderStateRequest request, Locale locale) throws Exception;	

	

}
