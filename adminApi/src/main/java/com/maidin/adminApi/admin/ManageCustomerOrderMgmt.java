package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.common.exception.ValidationException;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.ScheduleProductRequestV2;
import com.maidin.pojo.manageorders.GetScheduleOrdersRequest;
import com.maidin.pojo.manageorders.GetScheduledProductsResponse;

public interface ManageCustomerOrderMgmt {

	GetScheduledProductsResponse getScheduledOrders(GetScheduleOrdersRequest getScheduleOrdersRequest, String authToken,
			Locale locale) throws AuthenticationException, ValidationException;

	GenericResponse updateScheduledOrder(String authToken, ScheduleProductRequestV2 request, Locale locale)
			throws Exception;
}
