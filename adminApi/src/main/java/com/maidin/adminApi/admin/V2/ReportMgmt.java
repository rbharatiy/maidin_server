package com.maidin.adminApi.admin.V2;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.adminv2.OrderRequest;
import com.maidin.pojo.reports.BalanceReportRequest;
import com.maidin.pojo.reports.LedgerReportRequest;

public interface ReportMgmt {

	public OrderReportResponse getOrderReport(OrderRequest request, String authToken, Locale locale)  throws Exception;

	public OrderReportResponse getBalanceReport(BalanceReportRequest request, String authToken, Locale locale) throws Exception;

	public GenericResponse getLedgerReport(LedgerReportRequest request, String authToken, Locale locale)  throws Exception;
	

	

	

}
