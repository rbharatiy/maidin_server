package com.maidin.adminApi.admin.V2.impl;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.maidin.adminApi.admin.V2.SearchMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
//import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.PopularSearchesDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.pojo.search.ProductInfoV2;
import com.maidin.pojo.search.QuantityUnitInfo;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;

public class SearchMgmtImpl implements SearchMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final PhotoDAO photoDAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final CategoryInfoV2DAO caInfoV2DAO;
	private final PopularSearchesDAO popularSearchesDAO;

	@Autowired
	public SearchMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CustomerTransactionDAO transactionDAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ScheduleInfoDAO scheduleInfoDAO, TimeSlotDAO timeSlotDAO,
			BrandInfoV2DAO brandInfoV2DAO, PhotoDAO photoDAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			CategoryInfoV2DAO caInfoV2DAO, PopularSearchesDAO popularSearchesDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.scheduleInfoDAO = scheduleInfoDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.photoDAO = photoDAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.caInfoV2DAO = caInfoV2DAO;
		this.popularSearchesDAO = popularSearchesDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(SearchMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();

		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		long startTime = System.currentTimeMillis();
		System.out.println("Start Time:  " + new Date(System.currentTimeMillis()));
		/*
		 * boolean isProductAvailable = true; List<ProductInfoV2DTO> productInfoList =
		 * new ArrayList<>(); List<ProductInfoV2DTO> productInfoList1 =
		 * productInfoV2DAO.getAllMatchingProducts(request.getProductSubString());
		 * if(productInfoList1.size() >= 0){ productInfoList.addAll(productInfoList1);
		 * List<ProductInfoV2DTO> productInfoList2 =
		 * productInfoV2DAO.getAllMatchingProductsByBrand(request.getProductSubString())
		 * ; if(productInfoList2.size() == 0 && productInfoList1.size() == 0){
		 * response.getProductList().clear(); isProductAvailable = false; } else
		 * if(productInfoList2.size() > 0){ productInfoList.addAll(productInfoList2); }
		 * }
		 */
		if (!Strings.isNullOrEmpty(request.getProductSubString())) {
			String[] productString = request.getProductSubString().split(" ");
			List<String> productStringList = Arrays.stream(productString).collect(Collectors.toList());
			StringJoiner joiner_searchString = new StringJoiner("%", "%", "%");
			for (String s : productStringList)
				joiner_searchString.add(s);

			List<ProductInfoV2DTO> productInfoList = productInfoV2DAO
					.getAllMatchingProductsByBrand(joiner_searchString.toString());
			List<SubCategoryInfoV2DTO> subCategoryInfoV2DTOs = subCategoryInfoV2DAO
					.getAllSubCategoriesWithoutActiveState();
			List<CategoryInfoV2DTO> caInfoV2DTOs = caInfoV2DAO.getAll();

			// if(isProductAvailable){
			long endTime = System.currentTimeMillis();
			System.out.println("End Time:  " + new Date(System.currentTimeMillis()));
			System.out.println("Duration :  " + (endTime - startTime) / 1000 + "s");

			if (productInfoList.size() == 0) {
			} else {
				List<Integer> productIdList = productInfoList.stream().map(ProductInfoV2DTO::getId).distinct()
						.collect(Collectors.toList());

				List<Integer> brandIdList = productInfoList.stream().map(ProductInfoV2DTO::getBrandId).distinct()
						.collect(Collectors.toList());
				List<BrandInfoV2DTO> brandInfoList = new ArrayList<>();
				if (brandIdList != null && !brandIdList.isEmpty() && brandIdList.size() != 0) {
					brandInfoList = brandInfoV2DAO.getByBrandIds(brandIdList);
				}
				List<ProductCityInfoV2DTO> productCityV2List = new ArrayList<>();
				if (!request.getCityCode().equals("-1")) {
					productCityV2List = productCityInfoV2DAO.getByproductIdsAndCityCode(productIdList,
							request.getCityCode());
				} else {
					productCityV2List = productCityInfoV2DAO.getByproductIds(productIdList);
				}
				for (ProductInfoV2DTO dto : productInfoList) {
					ProductInfoV2 pInfoV2 = new ProductInfoV2();
					pInfoV2.setProductId(dto.getId());
					pInfoV2.setProductName(dto.getProductName());
					pInfoV2.setDescription(dto.getDescription() == null ? "" : dto.getDescription());
					pInfoV2.setFoodType(dto.getFoodType());
					pInfoV2.setBrandId(dto.getBrandId());
					pInfoV2.setBrandName(brandInfoList.stream().filter(e -> e.getId().equals(dto.getBrandId()))
							.map(BrandInfoV2DTO::getBrand).findFirst().orElse(""));
					List<ProductCityInfoV2DTO> productDetailsList = new ArrayList<>();
					if (request.getCityCode().equals("-1")) {
						productDetailsList = productCityInfoV2DAO.getByProductIdGroupByCity(dto.getId());
					} else if (request.getCityCode() != null) {
						productDetailsList = productCityV2List.stream()
								.filter(e -> e.getProductId().equals(dto.getId())).collect(Collectors.toList());
					}
					if (productDetailsList.size() == 0) {
						continue;
					}
					for (ProductCityInfoV2DTO pCityInfoV2DTO : productDetailsList) {
						QuantityUnitInfo quInfo = new QuantityUnitInfo();
						quInfo.setProductDetailsId(pCityInfoV2DTO.getId());
						if (request.getCityCode().equals("-1")) {
							quInfo.getCityCodeList().addAll(productDetailsList.stream()
									.filter(e -> e.getProductId().equals(dto.getId()))
									.map(ProductCityInfoV2DTO::getCityCode).distinct().collect(Collectors.toList()));
						} else {
							quInfo.getCityCodeList().add(pCityInfoV2DTO.getCityCode());
						}
						quInfo.setSetByDefault(pCityInfoV2DTO.isSetByDefault());
						quInfo.setMRP(pCityInfoV2DTO.getMRP());
						quInfo.setSalePrice(pCityInfoV2DTO.getSalePrice());
						quInfo.setQuantityUnit(pCityInfoV2DTO.getQuantityUnit());
						quInfo.setDiscountUnit(pCityInfoV2DTO.getDiscountUnit());
						if (pCityInfoV2DTO.getDiscountUnit().equals("%")) {
							int discount = Math
									.round((int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
											/ pCityInfoV2DTO.getMRP()));
							if (discount == 0 && ((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0)) {
								quInfo.setDiscountUnit("₹");
								quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
							} else {
								quInfo.setDiscount(
										(int) (((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) * 100)
												/ pCityInfoV2DTO.getMRP()));
							}
						} else if (pCityInfoV2DTO.getDiscountUnit().equals("₹")) {
							quInfo.setDiscount(pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice());
						}
						quInfo.setSavedAmount((pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()) > 0
								? pCityInfoV2DTO.getMRP() - pCityInfoV2DTO.getSalePrice()
								: 0);
						quInfo.setDeliveryCharge(pCityInfoV2DTO.getDeliveryCharge());
						quInfo.setActive(pCityInfoV2DTO.isActive());
						quInfo.setStockStatus(pCityInfoV2DTO.isStockStatus());
						if (pCityInfoV2DTO.getPhotoId() != null) {
							quInfo.setPhotoId(pCityInfoV2DTO.getPhotoId() == null ? 0 : pCityInfoV2DTO.getPhotoId());
							PhotoDTO photoDTO = photoDAO.getById(pCityInfoV2DTO.getPhotoId());
							if (photoDTO != null && photoDTO.getUpdatedOn() != null) {
								quInfo.setImageLastUpdatedOn(photoDTO.getUpdatedOn().getTime());
							}
						}
						pInfoV2.setSubCategoryId(pCityInfoV2DTO.getSubCategoryId());
						pInfoV2.setCategoryId(pCityInfoV2DTO.getCategoryId());
						pInfoV2.setCategoryName(
								caInfoV2DTOs.stream().filter(e -> e.getId().equals(pCityInfoV2DTO.getCategoryId()))
										.map(CategoryInfoV2DTO::getCategory).findFirst().orElse(""));
						pInfoV2.setSubCategoryName(subCategoryInfoV2DTOs.stream()
								.filter(e -> e.getId().equals(pCityInfoV2DTO.getSubCategoryId()))
								.map(SubCategoryInfoV2DTO::getSubCategory).findFirst().orElse(""));
						pInfoV2.getProductDetailList().add(quInfo);
					}
					response.getProductList().add(pInfoV2);
				}
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<PopularSearchesDTO> getAllPopularSearchKeywords(String authToken, Locale locale) throws Exception {
		List<PopularSearchesDTO> popularSearchesList = popularSearchesDAO.getAllKeywords();
		return popularSearchesList;
	}

}
