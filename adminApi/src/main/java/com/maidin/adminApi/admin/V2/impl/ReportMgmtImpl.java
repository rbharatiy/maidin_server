package com.maidin.adminApi.admin.V2.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.google.common.base.Strings;
import com.maidin.adminApi.admin.V2.ReportMgmt;
//import com.maidin.adminApi.admin.V2.ReportV2Mgmt;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
//import com.maidin.adminApi.utils.XlsxBuilder.StyleAttribute;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.EmailSenderWithAttachment;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.MaidinMoneyLedgerDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.MaidinMoneyLedgerDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.PreviousOrderDetailsInfoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerOrderInfo;
import com.maidin.pojo.admin.CustomerOrderReport;
import com.maidin.pojo.admin.OrderFormat;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.TimeSlotInfo;
import com.maidin.pojo.adminv2.OrderRequest;
import com.maidin.pojo.reports.BalanceReportRequest;
import com.maidin.pojo.reports.BalanceReportResponse;
import com.maidin.pojo.reports.CustomerBalanceInfo;
import com.maidin.pojo.reports.CustomerOrder;
import com.maidin.pojo.reports.CustomerOrderDetailsInfo;
import com.maidin.pojo.reports.LedgerData;
import com.maidin.pojo.reports.LedgerReportRequest;
import com.maidin.pojo.reports.LedgerReportResponse;
import com.maidin.pojo.reports.PurchaseOrder;
import com.maidin.pojo.reports.PurchaseOrderInfoRequest;
import com.maidin.pojo.reports.PurchaseOrderInfoResponse;

public class ReportMgmtImpl implements ReportMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ProductDAO productDAO;
	private final ProductCategoryDAO categoryDAO;
	private final ProductBrandsDAO brandDAO;
	private final PaymentModeDAO paymentModeDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;
	private final org.json.simple.JSONArray timeSlotJsonArray;
	private org.json.simple.JSONArray paymentModeJSONArray;
	private final MaidinMoneyLedgerDAO maidinMoneyLedgerDAO;

	@Autowired
	public ReportMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductDAO productDAO, ProductCategoryDAO categoryDAO, ProductBrandsDAO brandDAO,
			PaymentModeDAO paymentModeDAO, TimeSlotDAO timeSlotDAO, AddressDAO addressDAO,
			CustomerTransactionDAO transactionDAO, ProductCityInfoV2DAO productCityInfoV2DAO,
			ProductInfoV2DAO productInfoV2DAO, CategoryInfoV2DAO categoryInfoV2DAO, BrandInfoV2DAO brandInfoV2DAO,
			CityCategoryInfoDAO cityCategoryInfoDAO, MaidinMoneyLedgerDAO maidinMoneyLedgerDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productDAO = productDAO;
		this.categoryDAO = categoryDAO;
		this.brandDAO = brandDAO;
		this.paymentModeDAO = paymentModeDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
		this.timeSlotJsonArray = readTimeSlotJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.paymentModeJSONArray = readPaymentModeJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.maidinMoneyLedgerDAO = maidinMoneyLedgerDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(ReportMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final int accountLockingHour = Integer
			.parseInt(prop.getProperty(ResourceProperties.ACCOUNT_LOCKING_HOUR));

	@Override
	@Transactional(rollbackFor = Exception.class)
	public OrderReportResponse getOrderReport(OrderRequest request, String authToken, Locale locale) throws Exception {

		OrderReportResponse response = new OrderReportResponse();
		// String mobile = "7982340415";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		new Thread(() -> {
			try {
				callAsynchronouslyReportV2(request, locale, adminDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Async
	private void callAsynchronouslyReportV2(OrderRequest request, Locale locale, AdminDTO adminDTO)
			throws InterruptedException, ExecutionException {

		TimeSlotDTO timeSlotDTO = timeSlotDAO.getByTimeSlot(request.getTimeSlot());
		try {
			if (request.getFromTimestamp() == request.getToTimestamp()) {
				if (!request.getTimeSlot().equals("All")) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(System.currentTimeMillis());
					CustomerOrderReport reportResponse = new CustomerOrderReport();
					if (getZeroTimeDate(new Date(request.getFromTimestamp()))
							.before(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
						reportResponse = generateOrderReportV2(request, locale);
						convertSingleSlotOrderReportToExcel(request, reportResponse, adminDTO);
					} else if (getZeroTimeDate(new Date(request.getFromTimestamp()))
							.after(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
						reportResponse = generateOrderReportWithOutAccountInfo(request, locale);
						convertSingleSlotOrderReportWithoutAccountInfoToExcel(request, reportResponse, adminDTO);
					} else {
						if (calendar.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - accountLockingHour)) {
							reportResponse = generateOrderReportV2(request, locale);
							convertSingleSlotOrderReportToExcel(request, reportResponse, adminDTO);
						} else {
							reportResponse = generateOrderReportWithOutAccountInfo(request, locale);
							convertSingleSlotOrderReportWithoutAccountInfoToExcel(request, reportResponse, adminDTO);
						}
					}
				} else {
					CustomerOrderReport reportResponse = new CustomerOrderReport();
					reportResponse = generateOrderReportV2(request, locale);
					convertOrderReportToExcel(request, reportResponse, adminDTO.getEmail());

				}
			} else {
				CustomerOrderDetailsInfo reportResponse = new CustomerOrderDetailsInfo();
				reportResponse = generateMultipleDayOrderReportV2(request, locale);
				convertMultipleDayOrderReportToExcel(request, reportResponse, adminDTO.getEmail());

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void convertSingleSlotOrderReportToExcel(OrderRequest request, CustomerOrderReport response,
			AdminDTO adminDTO) throws FileNotFoundException, IOException, MessagingException, JSONException {
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = (XSSFCellStyle) workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = (XSSFCellStyle) workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = (XSSFCellStyle) workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createSingleSlotOrderReportHeader(sheet, rowCount, request, adminDTO);
		rowCount = 1; // two headers added
		int rf = 2;
		int rt = 0;

		int sNo = 1;

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
					cellStyle.setDataFormat(dateFormat);

					int cellSequence = 0;
					Cell sNoVal = row2.createCell(cellSequence);
					sNoVal.setCellValue(sNo);
					sNoVal.setCellStyle(textStyle);

					sNo++;

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell categoryVal = row2.createCell(cellSequence);
					categoryVal.setCellValue(rec.getCategory());
					categoryVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paymentModeVal = row2.createCell(cellSequence);
					paymentModeVal.setCellValue(rec.getPaymentMode());
					paymentModeVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell maidinMoneyVal = row2.createCell(cellSequence);
					maidinMoneyVal.setCellValue(rec.getMaidinMoney());
					maidinMoneyVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paidByMaidinMoneyVal = row2.createCell(cellSequence);
					paidByMaidinMoneyVal.setCellValue(timeSlotInfo.getPaidyMaidinMoney());
					paidByMaidinMoneyVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell cashCollectVal = row2.createCell(cellSequence);
					cashCollectVal.setCellValue(timeSlotInfo.getCashCollect());
					cashCollectVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 182, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(55, 178, 73)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (rt != rf && rf < rt) {
					// merge Customer name
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

					// merge project
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

					// merge tower
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

					// merge flat
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

					// merge mobile number
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

					// merge Maidin Money
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 13, 13));

					// merge Paid by Maidin Money
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 14, 14));

					// merge Cash need to Collect
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 15, 15));

					// merge Ordered Time
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));
					// }

					rf = rt + 1;
					rt = rt + timeSlotInfo.getOrderFormatList().size();
				} else {
					rf++;
				}
			}

			// set serial No width to autoFit
			/* sheet.autoSizeColumn(0); */
			sheet.setColumnWidth(0, 256 * 15);
			// limit Customer name width to 15 characters
			sheet.setColumnWidth(1, 256 * 15);

			// limit Project name width to 20 characters
			sheet.setColumnWidth(2, 256 * 20);

			// limit Tower name width to 10 characters
			sheet.setColumnWidth(3, 256 * 10);

			// limit Flat name width to 10 characters
			sheet.setColumnWidth(4, 256 * 10);

			// limit Mobile number width to 13 characters
			sheet.setColumnWidth(5, 256 * 13);

			// limit category width to 15 characters
			sheet.setColumnWidth(6, 256 * 15);

			// limit brand width to 15 characters
			sheet.setColumnWidth(7, 256 * 15);

			// limit product name width to 20 characters
			sheet.setColumnWidth(8, 256 * 20);

			// limit unit width to 10 characters
			sheet.setColumnWidth(9, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(10, 256 * 5);

			// limit bill amount width to 9 characters
			sheet.setColumnWidth(11, 256 * 9);

			// set payment mode to auto fit
			/* sheet.autoSizeColumn(12); */
			sheet.setColumnWidth(12, 256 * 9);

			// limit Maidin money width to 9 characters
			sheet.setColumnWidth(13, 256 * 9);

			// limit paid by maidin money width to 9 characters
			sheet.setColumnWidth(14, 256 * 9);

			// limit Cash need to Collect width to 9 characters
			sheet.setColumnWidth(15, 256 * 9);

			// limit Ordered Time width to 14 characters
			sheet.setColumnWidth(16, 256 * 14);

			// set Order Status to auto fit
			sheet.setColumnWidth(17, 256 * 17);

		}

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String finalDateString = fromDate;
		File file = new File("OrderReport " + " - "
				+ (request.getTimeSlot().equalsIgnoreCase("All") ? "All timeslots" : request.getTimeSlot()) + "    "
				+ finalDateString + " - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : " + request.getTimeSlot();
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);
		logger.info("Order report email SENT");

	}

	/*
	 * private void convertOrderReportToExcel(OrderRequest request,
	 * CustomerOrderReport response, String senderEmail) throws
	 * FileNotFoundException, IOException, MessagingException, JSONException {
	 * logger.info("Excel creation started with formatting"); boolean isOneDayReport
	 * = false; if (request.getFromTimestamp() == request.getToTimestamp()) {
	 * isOneDayReport = true; } else { isOneDayReport = false; } XSSFWorkbook
	 * workbook = new XSSFWorkbook(); XSSFSheet sheet =
	 * workbook.createSheet("OrderReport"); XSSFCellStyle cellStyle =
	 * workbook.createCellStyle();
	 * cellStyle.setAlignment(HorizontalAlignment.CENTER);
	 * cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	 * cellStyle.setWrapText(true);
	 * 
	 * XSSFCellStyle integerValueStyle = workbook.createCellStyle();
	 * integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
	 * integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	 * 
	 * XSSFCellStyle orderStatusStyle = workbook.createCellStyle();
	 * orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
	 * orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	 * 
	 * XSSFCellStyle textStyle = workbook.createCellStyle();
	 * textStyle.setAlignment(HorizontalAlignment.LEFT);
	 * textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	 * textStyle.setWrapText(true);
	 * 
	 * setExcelCellBorder(cellStyle); setExcelCellBorder(integerValueStyle);
	 * setExcelCellBorder(orderStatusStyle); setExcelCellBorder(textStyle);
	 * 
	 * XSSFFont font = sheet.getWorkbook().createFont();
	 * 
	 * CreationHelper createHelper = workbook.getCreationHelper(); int rowCount = 0;
	 * createOrderReportheader(sheet, rowCount, request); int rf = 1; int rt = 0;
	 * 
	 * for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
	 * for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) { rt = rf
	 * + (timeSlotInfo.getOrderFormatList().size() - 1); for (int i = 0; i <
	 * timeSlotInfo.getOrderFormatList().size(); ++i) { OrderFormat rec =
	 * timeSlotInfo.getOrderFormatList().get(i); Row row2 =
	 * sheet.createRow(++rowCount); short dateFormat =
	 * createHelper.createDataFormat().getFormat("yyyy-dd-MM");
	 * cellStyle.setDataFormat(dateFormat); int cellSequence = 0; Cell del_DateVal =
	 * row2.createCell(cellSequence);
	 * del_DateVal.setCellValue(rec.getDeliveryDate());
	 * del_DateVal.setCellStyle(cellStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell del_SlotVal =
	 * row2.createCell(cellSequence);
	 * del_SlotVal.setCellValue(rec.getDeliverySlot());
	 * del_SlotVal.setCellStyle(cellStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell nameVal =
	 * row2.createCell(cellSequence); nameVal.setCellValue(rec.getName());
	 * nameVal.setCellStyle(textStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell projVal =
	 * row2.createCell(cellSequence); projVal.setCellValue(rec.getProject());
	 * projVal.setCellStyle(textStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell towerVal =
	 * row2.createCell(cellSequence); towerVal.setCellValue(rec.getTower());
	 * towerVal.setCellStyle(cellStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell flatVal =
	 * row2.createCell(cellSequence); flatVal.setCellValue(rec.getFlat());
	 * flatVal.setCellStyle(cellStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell mobVal = row2.createCell(cellSequence);
	 * mobVal.setCellValue(rec.getMobile()); mobVal.setCellStyle(cellStyle);
	 * 
	 * // if (!isOneDayReport) { cellSequence = cellSequence + 1; Cell catVal =
	 * row2.createCell(cellSequence); catVal.setCellValue(rec.getCategory());
	 * catVal.setCellStyle(textStyle); // }
	 * 
	 * cellSequence = cellSequence + 1; Cell brandVal =
	 * row2.createCell(cellSequence); brandVal.setCellValue(rec.getBrand());
	 * brandVal.setCellStyle(textStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell productVal =
	 * row2.createCell(cellSequence); productVal.setCellValue(rec.getProduct());
	 * productVal.setCellStyle(textStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell unitVal =
	 * row2.createCell(cellSequence); unitVal.setCellValue(rec.getUnit());
	 * unitVal.setCellStyle(textStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell quantityVal =
	 * row2.createCell(cellSequence); quantityVal.setCellValue(rec.getQuantity());
	 * quantityVal.setCellStyle(integerValueStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell billAmountVal =
	 * row2.createCell(cellSequence);
	 * billAmountVal.setCellValue(rec.getBillAmount());
	 * billAmountVal.setCellStyle(integerValueStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell paymentModeVal =
	 * row2.createCell(cellSequence);
	 * paymentModeVal.setCellValue(rec.getPaymentMode());
	 * paymentModeVal.setCellStyle(cellStyle);
	 * 
	 * if (isOneDayReport) { cellSequence = cellSequence + 1; Cell maidinMoneyVal =
	 * row2.createCell(cellSequence);
	 * maidinMoneyVal.setCellValue(rec.getMaidinMoney());
	 * maidinMoneyVal.setCellStyle(integerValueStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell paidByMaidinMoneyVal =
	 * row2.createCell(cellSequence);
	 * paidByMaidinMoneyVal.setCellValue(timeSlotInfo.getPaidyMaidinMoney());
	 * paidByMaidinMoneyVal.setCellStyle(integerValueStyle);
	 * 
	 * cellSequence = cellSequence + 1; Cell cashCollectVal =
	 * row2.createCell(cellSequence);
	 * cashCollectVal.setCellValue(timeSlotInfo.getCashCollect());
	 * cashCollectVal.setCellStyle(integerValueStyle); }
	 * 
	 * cellSequence = cellSequence + 1; Cell orderedTimeVal =
	 * row2.createCell(cellSequence);
	 * orderedTimeVal.setCellValue(rec.getOrderedTime());
	 * orderedTimeVal.setCellStyle(cellStyle);
	 * 
	 * XSSFRichTextString richTextString = new
	 * XSSFRichTextString(rec.getOrderStatus());
	 * 
	 * if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
	 * font.setColor(null); font.setColor(new XSSFColor(new java.awt.Color(255, 182,
	 * 0))); richTextString.applyFont(0, rec.getOrderStatus().length(), font); }
	 * else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
	 * font.setColor(null); font.setColor(new XSSFColor(new java.awt.Color(55, 178,
	 * 73))); richTextString.applyFont(0, rec.getOrderStatus().length(), font); }
	 * else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
	 * font.setColor(null); font.setColor(new XSSFColor(new java.awt.Color(255, 0,
	 * 0))); richTextString.applyFont(0, rec.getOrderStatus().length(), font); }
	 * else { font.setColor(null); font.setColor(new XSSFColor(new java.awt.Color(0,
	 * 0, 0))); richTextString.applyFont(0, rec.getOrderStatus().length(), font); }
	 * 
	 * cellSequence = cellSequence + 1; Cell orderStatusVal =
	 * row2.createCell(cellSequence);
	 * orderStatusVal.setCellValue(rec.getOrderStatus());
	 * orderStatusVal.setCellStyle(orderStatusStyle);
	 * orderStatusVal.setCellValue(richTextString); } if (isOneDayReport) {
	 * System.out.println("rt : " + rt + "  ||   rf : " + rf); if (rt != rf) { //
	 * merge Delivery Date sheet.addMergedRegion(new CellRangeAddress(rf, rt, 0,
	 * 0));
	 * 
	 * // merge Delivery slot sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1,
	 * 1));
	 * 
	 * // merge Customer name sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2,
	 * 2));
	 * 
	 * // merge project sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));
	 * 
	 * // merge tower sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));
	 * 
	 * // merge flat sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));
	 * 
	 * // merge mobile number sheet.addMergedRegion(new CellRangeAddress(rf, rt, 6,
	 * 6));
	 * 
	 * // merge Maidin Money sheet.addMergedRegion(new CellRangeAddress(rf, rt, 14,
	 * 14));
	 * 
	 * // merge Paid by Maidin Money sheet.addMergedRegion(new CellRangeAddress(rf,
	 * rt, 15, 15));
	 * 
	 * // merge Cash need to Collect sheet.addMergedRegion(new CellRangeAddress(rf,
	 * rt, 16, 16));
	 * 
	 * // merge Ordered Time sheet.addMergedRegion(new CellRangeAddress(rf, rt, 17,
	 * 17));
	 * 
	 * rf = rt + 1; rt = rt + timeSlotInfo.getOrderFormatList().size(); } else {
	 * rf++; } } }
	 * 
	 * }
	 * 
	 * // limit delivery date width to 12 characters sheet.setColumnWidth(0, 256 *
	 * 12);
	 * 
	 * for (int i = 1; i < 7; i++) { sheet.autoSizeColumn(i); } for (int i = 7; i <
	 * 10; i++) { sheet.autoSizeColumn(i); } // limit Customer name width to 15
	 * characters sheet.setColumnWidth(2, 256 * 15);
	 * 
	 * // limit Project name width to 20 characters sheet.setColumnWidth(3, 256 *
	 * 20);
	 * 
	 * // limit Tower name width to 10 characters sheet.setColumnWidth(4, 256 * 10);
	 * 
	 * // limit Flat name width to 10 characters sheet.setColumnWidth(5, 256 * 10);
	 * 
	 * // limit Mobile number width to 13 characters sheet.setColumnWidth(6, 256 *
	 * 13);
	 * 
	 * sheet.autoSizeColumn(13); sheet.autoSizeColumn(16);
	 * 
	 * if (!isOneDayReport) { // limit category width to 18 characters
	 * sheet.setColumnWidth(7, 256 * 18);
	 * 
	 * // limit brand width to 15 characters sheet.setColumnWidth(8, 256 * 15);
	 * 
	 * // limit product name width to 25 characters sheet.setColumnWidth(9, 256 *
	 * 25);
	 * 
	 * // limit unit width to 10 characters sheet.setColumnWidth(10, 256 * 10);
	 * 
	 * // limit quantity width to 5 characters sheet.setColumnWidth(11, 256 * 5);
	 * 
	 * // limit bill amount width to 8 characters sheet.setColumnWidth(12, 256 * 8);
	 * 
	 * // limit payment mode width to 16 characters sheet.setColumnWidth(13, 256 *
	 * 16);
	 * 
	 * sheet.autoSizeColumn(14); sheet.autoSizeColumn(15); } else { // limit
	 * category width to 18 characters sheet.setColumnWidth(7, 256 * 18);
	 * 
	 * // limit brand width to 15 characters sheet.setColumnWidth(8, 256 * 15);
	 * 
	 * // limit product name width to 25 characters sheet.setColumnWidth(9, 256 *
	 * 25);
	 * 
	 * // limit unit width to 10 characters sheet.setColumnWidth(10, 256 * 10);
	 * 
	 * // limit quantity width to 5 characters sheet.setColumnWidth(11, 256 * 5);
	 * 
	 * // limit bill amount width to 8 characters sheet.setColumnWidth(12, 256 * 8);
	 * 
	 * // limit Payment mode width to 16 characters sheet.setColumnWidth(13, 256 *
	 * 16);
	 * 
	 * // limit Cash need to collect width to 8 characters sheet.setColumnWidth(16,
	 * 256 * 8);
	 * 
	 * // limit Ordered time width to 13 characters sheet.setColumnWidth(17, 256 *
	 * 13);
	 * 
	 * // limit Order status width to auto fit sheet.autoSizeColumn(18);
	 * 
	 * } logger.info("Excel creation END with formatting");
	 * org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray,
	 * request.getTimeSlot()); String fromDate =
	 * ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
	 * String toDate = ApiUtils.dateWithMonthNameFormat(new
	 * Timestamp(request.getToTimestamp())); String finalDateString = isOneDayReport
	 * ? fromDate : fromDate + "  TO  " + toDate; File file = new
	 * File("OrderReport " + " - " +
	 * (jsonObject.get("timeSlot").toString().equalsIgnoreCase("All") ?
	 * "All timeslots" : jsonObject.get("timeSlot")) + "    " + finalDateString +
	 * ".xlsx");
	 * 
	 * try (FileOutputStream outputStream = new FileOutputStream(file)) {
	 * workbook.write(outputStream); outputStream.close(); } MimeBodyPart
	 * attachmentBodypart = new MimeBodyPart(); attachmentBodypart.attachFile(file);
	 * Multipart multipart = new MimeMultipart();
	 * 
	 * multipart.addBodyPart(attachmentBodypart);
	 * 
	 * String subject = "Order Summary Report | Date :  " + finalDateString +
	 * " | Timeslot : " + jsonObject.get("timeSlot");
	 * EmailSenderWithAttachment.sendMail(senderEmail, subject, "Attachment",
	 * multipart); logger.info("Order report email SENT"); }
	 */
	private void setExcelCellBorder(CellStyle cellStyle) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
	}

	private void createOrderReportheader(SXSSFSheet sheet, int rowCount, OrderRequest request) {
		logger.info("HEADER creation started");
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

		Font font = sheet.getWorkbook().createFont();
		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		setExcelCellBorder(cellStyle);

		Row row = sheet.createRow(rowCount);

		int cellSequenc = 0;
		Cell del_DateHeader = row.createCell(cellSequenc);
		del_DateHeader.setCellValue("Delivery Date");
		del_DateHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell del_SlotHeader = row.createCell(cellSequenc);
		del_SlotHeader.setCellValue("Delivery Slot");
		del_SlotHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		if (request.getFromTimestamp() != request.getToTimestamp()) {
			if (request.getFromTimestamp() == request.getToTimestamp()) {
				cellSequenc = cellSequenc + 1;
				Cell emailHeader = row.createCell(cellSequenc);
				emailHeader.setCellValue("Email");
				emailHeader.setCellStyle(cellStyle);

			}
		}

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paymentModeHeader = row.createCell(cellSequenc);
		paymentModeHeader.setCellValue("Payment Mode");
		paymentModeHeader.setCellStyle(cellStyle);

		if (request.getFromTimestamp() == request.getToTimestamp()) {
			cellSequenc = cellSequenc + 1;
			Cell maidinMoneyHeader = row.createCell(cellSequenc);
			maidinMoneyHeader.setCellValue("Maidin Money");
			maidinMoneyHeader.setCellStyle(cellStyle);

			cellSequenc = cellSequenc + 1;
			Cell paidByMaidinMoneyHeader = row.createCell(cellSequenc);
			paidByMaidinMoneyHeader.setCellValue("Paid by Maidin Money");
			paidByMaidinMoneyHeader.setCellStyle(cellStyle);

			cellSequenc = cellSequenc + 1;
			Cell cashCollectHeader = row.createCell(cellSequenc);
			cashCollectHeader.setCellValue("Cash need to Collect ");
			cashCollectHeader.setCellStyle(cellStyle);
		}

		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 1, 0, 1);
		logger.info("HEADER createion end");
	}

	public static org.json.simple.JSONArray readTimeSlotJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray timeSlotArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			timeSlotArray = (org.json.simple.JSONArray) jsonObject.get("timeSlotList");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return timeSlotArray;
	}

	private void createSingleSlotOrderReportHeader(SXSSFSheet sheet, int rowCount, OrderRequest request,
			AdminDTO adminDTO) throws JSONException {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));


		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue("Delivery report of " + fromDate + " | " + request.getTimeSlot()
				+ ".  Downloaded by - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())));
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 17));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		int cellSequenc = 0;
		Cell sNoHeader = row.createCell(cellSequenc);
		sNoHeader.setCellValue("S No.");
		sNoHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paymentModeHeader = row.createCell(cellSequenc);
		paymentModeHeader.setCellValue("Payment Mode");
		paymentModeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell maidinMoneyHeader = row.createCell(cellSequenc);
		maidinMoneyHeader.setCellValue("Maidin Money");
		maidinMoneyHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paidByMaidinMoneyHeader = row.createCell(cellSequenc);
		paidByMaidinMoneyHeader.setCellValue("Paid by Maidin Money");
		paidByMaidinMoneyHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell cashCollectHeader = row.createCell(cellSequenc);
		cashCollectHeader.setCellValue("Cash need to Collect ");
		cashCollectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
	}

	/*
	 * private CustomerOrderReport generateOrderReportV2(OrderRequest request,
	 * Locale locale) { CustomerOrderReport response = new CustomerOrderReport();
	 * List<Long> customerList = new ArrayList<>(); List<Object[]> list = new
	 * ArrayList<>(); final List<CustomerOrder> customerOrdersList = new
	 * ArrayList<>();
	 * 
	 * // get wallet balance of all customers List<WalletDTO> walletDTOList =
	 * walletDAO.getAll(); // get orderList by from date and two date and timeSlot
	 * if (request.getTimeSlot() != 0) { list =
	 * orderInfoDAO.getCustomerOrdersByDateAndSlot(new
	 * Date(request.getFromTimestamp()), new Date(request.getToTimestamp()),
	 * request.getTimeSlot()); } else { list =
	 * orderInfoDAO.getCustomerOrdersByDate(new Date(request.getFromTimestamp()),
	 * new Date(request.getToTimestamp())); }
	 * 
	 * // fetch all data from db and add to list list.stream().forEach((record) -> {
	 * CustomerOrder customerOrder = addCustomerOrderInfo(record, locale);
	 * customerOrdersList.add(customerOrder); });
	 * 
	 * // get unique customers from list and iterate by customerId customerList =
	 * customerOrdersList.stream().map(CustomerOrder::getCustomerId).distinct()
	 * .collect(Collectors.toList()); for (Long customerId : customerList) { float
	 * maidinMoney = 0; if (walletDTOList != null) { // get wallet money of unique
	 * customers maidinMoney = walletDTOList.stream().filter(e -> e.getCustomerId()
	 * == customerId) .map(WalletDTO::getMyWalletBalance).findFirst().orElse(0f); }
	 * 
	 * CustomerOrderInfo customerOrder = new CustomerOrderInfo();
	 * customerOrder.setCustomerId(customerId); List<OrderInfoDTO> orderInfoDTOList
	 * = null; List<Integer> timeSlotList = null; // get sublist from
	 * customerOrdersList to get specific customer Data // into customerOrdersList1
	 * List<CustomerOrder> customerOrdersList1 = customerOrdersList.stream()
	 * .filter((u) -> u.getCustomerId() == customerId).collect(Collectors.toList());
	 * 
	 * // get date list from customerOrdersList1 and iterate by date
	 * List<java.util.Date> dateList =
	 * customerOrdersList1.stream().map(CustomerOrder::getDeliveryDate).distinct ()
	 * .collect(Collectors.toList()); for (java.util.Date deliveryDate : dateList) {
	 * 
	 * // get sublist of customerOrdersList1 by specifying particular // date
	 * customerOrdersList1 = customerOrdersList1.stream() .filter((u) ->
	 * u.getDeliveryDate().equals(deliveryDate)).collect(Collectors.toList());
	 * 
	 * // get timeSlot list of customers of particular delivery date by //
	 * specifying timeSlot is all or specific timeslot if (request.getTimeSlot() ==
	 * 0) { timeSlotList =
	 * customerOrdersList1.stream().map(CustomerOrder::getTimeSlotId).distinct()
	 * .collect(Collectors.toList()); } else if (request.getTimeSlot() > 0) {
	 * timeSlotList = customerOrdersList1.stream() .filter((u) -> u.getTimeSlotId()
	 * == request.getTimeSlot()).map(CustomerOrder::getTimeSlotId)
	 * .distinct().collect(Collectors.toList()); }
	 * 
	 * // iterate orderDetails by customer,date and timeslotId for (int timeSlotId :
	 * timeSlotList) { float cc = 0; float mm = 0; TimeSlotInfo timeSlot = new
	 * TimeSlotInfo(); timeSlot.setTimeSlotId(timeSlotId); int
	 * orderCountPerCustomerPerTimeSlot = 0; customerOrdersList1 =
	 * customerOrdersList1.stream().filter((u) -> u.getTimeSlotId() == timeSlotId)
	 * .collect(Collectors.toList()); orderCountPerCustomerPerTimeSlot =
	 * orderCountPerCustomerPerTimeSlot + customerOrdersList1.size(); for
	 * (CustomerOrder customerOrder2 : customerOrdersList1) {
	 * 
	 * // set order details to object OrderFormat from list // customerOrdersList1
	 * OrderFormat orderRow = new OrderFormat(); float cashPerOrder = 0;// total
	 * cash of each // orderDetails float mmPerOrder = 0;// total Maidin Money of
	 * each // orderDetails orderRow.setName(customerOrder2.getName());
	 * orderRow.setProject(customerOrder2.getProject());
	 * orderRow.setTower(customerOrder2.getTower());
	 * orderRow.setFlat(customerOrder2.getFlat());
	 * orderRow.setMobile(customerOrder2.getMobile());
	 * orderRow.setEmail(customerOrder2.getEmail());
	 * orderRow.setDeliverySlot(customerOrder2.getDeliverySlot());
	 * orderRow.setProduct(customerOrder2.getProduct());
	 * orderRow.setUnit(customerOrder2.getUnit());
	 * orderRow.setBrand(customerOrder2.getBrand());
	 * orderRow.setCategory(customerOrder2.getCategory());
	 * orderRow.setQuantity(customerOrder2.getQuantity());
	 * orderRow.setBillAmount(customerOrder2.getBillAmount()); // set paymentMode,
	 * CashCollect, PaidByMaidinMoney for // OrderStatus - 'CONFIRMED' if
	 * (customerOrder2.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
	 * 
	 * // in case of Payment Status 'PAYMENTSETTLED' if
	 * (customerOrder2.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.
	 * getName())) { orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
	 * orderRow.setCashCollect(0);
	 * orderRow.setPaidByMaidinMoney(customerOrder2.getBillAmount()); }
	 * 
	 * // in case of Payment Status 'PARTIALPAYMENT' else if
	 * (customerOrder2.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.
	 * getName())) { orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
	 * float amountToPay = customerOrder2.getTotalProductCost() -
	 * customerOrder2.getAmountPaidByCustomer(); if (maidinMoney >= (amountToPay)) {
	 * orderRow.setPaidByMaidinMoney( customerOrder2.getAmountPaidByCustomer() +
	 * amountToPay); maidinMoney = maidinMoney - amountToPay; } else {
	 * orderRow.setPaidByMaidinMoney( customerOrder2.getAmountPaidByCustomer() +
	 * maidinMoney); maidinMoney = 0; }
	 * orderRow.setCashCollect(customerOrder2.getTotalProductCost() -
	 * customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaidByMaidinMoney(customerOrder2.getAmountPaidByCustomer()); }
	 * 
	 * // in case of Payment Status 'PENDING' else if
	 * (customerOrder2.getPaymentStatus().equals(PaymentStatus.PENDING.getName() ))
	 * { if (maidinMoney >= customerOrder2.getBillAmount()) { maidinMoney =
	 * maidinMoney - customerOrder2.getBillAmount();
	 * orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
	 * orderRow.setCashCollect(0);
	 * orderRow.setPaidByMaidinMoney(customerOrder2.getBillAmount()); } else if
	 * (maidinMoney > 0 && customerOrder2.getBillAmount() > maidinMoney) {
	 * orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
	 * orderRow.setCashCollect(customerOrder2.getBillAmount() - maidinMoney);
	 * orderRow.setPaidByMaidinMoney(maidinMoney); maidinMoney = 0; } else {
	 * orderRow.setPaymentMode(PaymentMode.ByCash.getName()); if (maidinMoney < 0) {
	 * orderRow.setCashCollect(customerOrder2.getBillAmount() - maidinMoney);
	 * maidinMoney = 0; } else if (maidinMoney == 0) {
	 * orderRow.setCashCollect(customerOrder2.getBillAmount()); }
	 * orderRow.setPaidByMaidinMoney(0); } } } // set paymentMode, CashCollect,
	 * PaidByMaidinMoney for // OrderStatus - 'UNDELIVERED' else if
	 * (customerOrder2.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName() ))
	 * { orderRow.setPaymentMode("NA"); orderRow.setCashCollect(0);
	 * orderRow.setPaidByMaidinMoney(0); } // set paymentMode, CashCollect,
	 * PaidByMaidinMoney for // OrderStatus - 'DELIVERED' else if
	 * (customerOrder2.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
	 * if (customerOrder2.getPaymentModeId() != 0) { // handling paymentMode from //
	 * OrderDetailsInfo if (customerOrder2.getPaymentModeId() ==
	 * PaymentMode.ByCash.getId() || customerOrder2 .getPaymentModeId() ==
	 * PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId()) {
	 * orderRow.setCashCollect(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode( paymentMode(paymentModeJSONArray,
	 * customerOrder2.getPaymentModeId())); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByWallet.getId() ||
	 * customerOrder2 .getPaymentModeId() ==
	 * PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING .getId()) {
	 * orderRow.setPaidByMaidinMoney(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode( paymentMode(paymentModeJSONArray,
	 * customerOrder2.getPaymentModeId())); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
	 * orderRow.setCashCollect(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode(PaymentMode.ByPaytm.getName()); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByCashByWallet.getId()) {
	 * orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
	 * List<CustomerTransactionDTO> transactionDTOList = transactionDAO
	 * .getByOrderDetailsId(customerOrder2.getOrderDetailsId()); for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByCash.getName()); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
	 * mmPerOrder = mmPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByWallet.getName()); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByPaytm.getName()); } }
	 * orderRow.setCashCollect(cashPerOrder);
	 * orderRow.setPaidByMaidinMoney(mmPerOrder);
	 * 
	 * } else { orderRow.setPaymentMode(PaymentMode.PENDING.getName()); }
	 * 
	 * } else { // handling paymentMode from OrderInfo List<CustomerTransactionDTO>
	 * transactionDTOList = transactionDAO
	 * .getByOrderDetailsId(customerOrder2.getOrderDetailsId()); if
	 * (transactionDTOList == null || transactionDTOList.isEmpty()) { if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * orderRow.setCashCollect(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode(PaymentMode.ByCash.getName()); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
	 * orderRow.setPaidByMaidinMoney(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode(PaymentMode.ByWallet.getName()); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
	 * orderRow.setCashCollect(customerOrder2.getAmountPaidByCustomer());
	 * orderRow.setPaymentMode(PaymentMode.ByPaytm.getName()); } else if
	 * (customerOrder2.getPaymentModeId() == PaymentMode.ByCashByWallet .getId()) {
	 * orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName()); } else {
	 * PaymentModeDTO modeDTO = paymentModeDAO
	 * .getById(customerOrder2.getPaymentModeId()); if (modeDTO == null) {
	 * orderRow.setPaymentMode("Payment Not linked To Order"); } } }
	 * 
	 * else { boolean transaction = sameTransaction(transactionDTOList); if
	 * (!transaction) {
	 * orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName()); for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn()); } else if (transactionDTO.getPaymentModeId() ==
	 * PaymentMode.ByWallet .getId()) { mmPerOrder = mmPerOrder +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn()); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm .getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn()); } } orderRow.setCashCollect(cashPerOrder);
	 * orderRow.setPaidByMaidinMoney(mmPerOrder); } else { for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByCash.getName()); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet .getId()) {
	 * mmPerOrder = mmPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByWallet.getName()); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm .getId()) {
	 * cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut() -
	 * transactionDTO.getMoneyIn());
	 * orderRow.setPaymentMode(PaymentMode.ByPaytm.getName()); } }
	 * orderRow.setCashCollect(cashPerOrder);
	 * orderRow.setPaidByMaidinMoney(mmPerOrder); } } } } // set wallet money if not
	 * null set 0 orderRow.setMaidinMoney( walletDTOList.stream().filter(e ->
	 * e.getCustomerId() == Long.valueOf(customerId))
	 * .map(WalletDTO::getMyWalletBalance).findFirst().orElse(0f));
	 * orderRow.setOrderedTime((customerOrder2.getOrderedTime()));
	 * orderRow.setDeliveryDate(customerOrder2.getDeliveryDate());
	 * orderRow.setOrderStatus(customerOrder2.getOrderStatus());
	 * timeSlot.getOrderFormatList().add(orderRow); cc = cc +
	 * orderRow.getCashCollect(); mm = mm + orderRow.getPaidByMaidinMoney();
	 * timeSlot.setCashCollect(cc); timeSlot.setPaidyMaidinMoney(mm); }
	 * customerOrder.getTimeSlotId().add(timeSlot); } }
	 * response.getCustomerOrderInfo().add(customerOrder); } return response;
	 * 
	 * }
	 */
	private CustomerOrder addCustomerOrderInfo(Object[] record, Locale locale) {
		CustomerOrder customerOrder = new CustomerOrder();
		try {
			customerOrder.setDeliveryDate(ApiUtils.toDate(record[0].toString(), locale));
		} catch (Exception e) {
			e.printStackTrace();
		}
		customerOrder.setDeliverySlot(String.valueOf(record[1].toString()));
		customerOrder.setCustomerId(Long.valueOf(record[2].toString()));
		customerOrder.setName(String.valueOf(record[3].toString()));
		customerOrder.setProject(String.valueOf(record[4].toString()));
		customerOrder.setTower(String.valueOf(record[5].toString()));
		customerOrder.setFlat(String.valueOf(record[6].toString()));
		customerOrder.setMobile(String.valueOf(record[7].toString()));
		customerOrder.setCategory(String.valueOf(record[8].toString()));
		customerOrder.setBrand(String.valueOf(record[9].toString()));
		customerOrder.setProduct(String.valueOf(record[10].toString()));
		customerOrder.setUnit(String.valueOf(record[11].toString()));
		customerOrder.setQuantity(Integer.parseInt(record[12].toString()));
		if (Float.parseFloat(record[17].toString()) == 0) {
			customerOrder
					.setBillAmount(Float.parseFloat(record[18].toString()) * Integer.parseInt(record[12].toString()));
		} else {
			customerOrder.setBillAmount(Float.parseFloat(record[13].toString()));
		}
		customerOrder.setTimeSlotId(Integer.parseInt(record[20].toString()));
		customerOrder.setOrderStatus(String.valueOf(record[16].toString()));
		customerOrder.setAmountPaidByCustomer(Float.parseFloat(record[21].toString()));
		customerOrder.setOrderMRP(Float.parseFloat(record[17].toString()));
		customerOrder.setProductPrice(Float.parseFloat(record[18].toString()));
		customerOrder.setOrderDetailsId(Long.parseLong(record[19].toString()));
		customerOrder.setTotalProductCost(Float.parseFloat(record[13].toString()));
		customerOrder.setPaymentStatus(String.valueOf(record[22].toString()));
		// customerOrder.setPaymentMode(String.valueOf(record[26].toString()));
		if (record[15] != null) {
			customerOrder.setOrderedTime(String.valueOf(record[15].toString()));
		}
		return customerOrder;
	}

	private void convertOrderReportToExcel(OrderRequest request, CustomerOrderReport response, String senderEmail)
			throws FileNotFoundException, IOException, MessagingException, JSONException {
		logger.info("Excel creation started with formatting");
		boolean isOneDayReport = false;
		if (request.getFromTimestamp() == request.getToTimestamp()) {
			isOneDayReport = true;
		} else {
			isOneDayReport = false;
		}
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet = workbook.createSheet("OrderReport");
		CellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		CellStyle integerValueStyle = workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		CellStyle orderStatusStyle = workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		CellStyle textStyle = workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		Font font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		int rf = 1;
		int rt = 0;

		createOrderReportheader(sheet, rowCount, request);
		short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					cellStyle.setDataFormat(dateFormat);
					int cellSequence = 0;
					Cell del_DateVal = row2.createCell(cellSequence);
					del_DateVal.setCellValue(rec.getDeliveryDate());
					del_DateVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell del_SlotVal = row2.createCell(cellSequence);
					del_SlotVal.setCellValue(rec.getDeliverySlot());
					del_SlotVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					// if (!isOneDayReport) {
					cellSequence = cellSequence + 1;
					Cell catVal = row2.createCell(cellSequence);
					catVal.setCellValue(rec.getCategory());
					catVal.setCellStyle(textStyle);
					// }

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paymentModeVal = row2.createCell(cellSequence);
					paymentModeVal.setCellValue(rec.getPaymentMode());
					paymentModeVal.setCellStyle(cellStyle);

					if (isOneDayReport) {
						cellSequence = cellSequence + 1;
						Cell maidinMoneyVal = row2.createCell(cellSequence);
						maidinMoneyVal.setCellValue(rec.getMaidinMoney());
						maidinMoneyVal.setCellStyle(integerValueStyle);

						cellSequence = cellSequence + 1;
						Cell paidByMaidinMoneyVal = row2.createCell(cellSequence);
						paidByMaidinMoneyVal.setCellValue(timeSlotInfo.getPaidyMaidinMoney());
						paidByMaidinMoneyVal.setCellStyle(integerValueStyle);

						cellSequence = cellSequence + 1;
						Cell cashCollectVal = row2.createCell(cellSequence);
						cashCollectVal.setCellValue(timeSlotInfo.getCashCollect());
						cashCollectVal.setCellStyle(integerValueStyle);
					}

					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						// font.setColor(null);
						// font.setColor(new XSSFColor(new java.awt.Color(255,
						// 182, 0)));
						font.setColor(IndexedColors.ORANGE.getIndex());
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						// font.setColor(null);
						// font.setColor(new XSSFColor(new java.awt.Color(55,
						// 178, 73)));
						font.setColor(IndexedColors.GREEN.getIndex());
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						// font.setColor(null);
						// font.setColor(new XSSFColor(new java.awt.Color(255,
						// 0, 0)));
						font.setColor(IndexedColors.RED.getIndex());
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						// font.setColor(null);
						// font.setColor(new XSSFColor(new java.awt.Color(0, 0,
						// 0)));
						font.setColor(IndexedColors.ORANGE.getIndex());
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (isOneDayReport) {
					if (rt != rf && rf < rt) {
						// merge Delivery Date
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 0, 0));

						// merge Delivery slot
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

						// merge Customer name
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

						// merge project
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

						// merge tower
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

						// merge flat
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

						// merge mobile number
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 6, 6));

						// merge Payment Mode
						// sheet.addMergedRegion(new CellRangeAddress(rf, rt,
						// 12, 12));

						// merge Maidin Money
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 14, 14));

						// merge Paid by Maidin Money
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 15, 15));

						// merge Cash need to Collect
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));

						// merge Ordered Time
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 17, 17));

						rf = rt + 1;
						rt = rt + timeSlotInfo.getOrderFormatList().size();
					} else {
						rf++;
					}
				}
			}

		}

		// limit delivery date width to 12 characters
		sheet.setColumnWidth(0, 256 * 15);

		// limit delivery slot width to 15 characters
		sheet.setColumnWidth(1, 256 * 15);

		// limit Customer name width to 15 characters
		sheet.setColumnWidth(2, 256 * 15);

		// limit Project name width to 20 characters
		sheet.setColumnWidth(3, 256 * 20);

		// limit Tower name width to 10 characters
		sheet.setColumnWidth(4, 256 * 10);

		// limit Flat name width to 10 characters
		sheet.setColumnWidth(5, 256 * 10);

		// limit Mobile number width to 13 characters
		sheet.setColumnWidth(6, 256 * 13);

		/*
		 * sheet.autoSizeColumn(13); sheet.autoSizeColumn(16);
		 */

		if (!isOneDayReport) {
			// limit category width to 18 characters
			sheet.setColumnWidth(7, 256 * 18);

			// limit brand width to 15 characters
			sheet.setColumnWidth(8, 256 * 15);

			// limit product name width to 25 characters
			sheet.setColumnWidth(9, 256 * 25);

			// limit unit width to 10 characters
			sheet.setColumnWidth(10, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(11, 256 * 5);

			// limit bill amount width to 8 characters
			sheet.setColumnWidth(12, 256 * 8);

			// limit payment mode width to 16 characters
			sheet.setColumnWidth(13, 256 * 16);

			/*
			 * sheet.autoSizeColumn(14); sheet.autoSizeColumn(15);
			 */
		} else {
			// limit category width to 18 characters
			sheet.setColumnWidth(7, 256 * 18);

			// limit brand width to 15 characters
			sheet.setColumnWidth(8, 256 * 15);

			// limit product name width to 25 characters
			sheet.setColumnWidth(9, 256 * 25);

			// limit unit width to 10 characters
			sheet.setColumnWidth(10, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(11, 256 * 5);

			// limit bill amount width to 8 characters
			sheet.setColumnWidth(12, 256 * 8);

			// limit Payment mode width to 16 characters
			sheet.setColumnWidth(13, 256 * 16);

			// limit Cash need to collect width to 8 characters
			sheet.setColumnWidth(16, 256 * 8);

			// limit Ordered time width to 13 characters
			sheet.setColumnWidth(17, 256 * 13);

			// limit Order status width to auto fit
			sheet.setColumnWidth(18, 256 * 15);

		}
		logger.info("Excel creation END with formatting");

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));
		String finalDateString = isOneDayReport ? fromDate : fromDate + "  TO  " + toDate;
		File file = new File("OrderReport " + " - "
				+ (request.getTimeSlot().equalsIgnoreCase("All") ? "All timeslots" : request.getTimeSlot()) + "    "
				+ finalDateString + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : " + request.getTimeSlot();
		EmailSenderWithAttachment.sendMail(senderEmail, subject, "Attachment", multipart);
		logger.info("Order report email SENT");
	}

	private CustomerOrderReport generateOrderReportV2(OrderRequest request, Locale locale) throws Exception {

		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		List<OrderInfoDTO> orderInfoListWithCity = new ArrayList<>();
		List<OrderInfoDTO> orderInfoListWithOutCity = new ArrayList<>();
		List<PaymentModeDTO> paymentModeDTOList = paymentModeDAO.getAll();

		List<AddressDTO> addressDTOList = new ArrayList<>();
		List<OrderDetailsInfoDTO> orderDetailsList = new ArrayList<>();
		List<CustomerTransactionDTO> transactionList = transactionDAO.getAllOrderTransaction();

		if (!request.getCityCode().equals("-1")) {
			orderInfoListWithCity = orderInfoDAO.getOrdersByDateAndCityCode(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()), request.getCityCode());
			if (orderInfoListWithCity.size() > 0) {
				addressDTOList = addressDAO.getByIds(orderInfoListWithCity.stream().map(OrderInfoDTO::getAddressId)
						.distinct().collect(Collectors.toList()));
				orderDetailsList = orderDetailsInfoDAO.getSettledOrderByOrderIdList(orderInfoListWithCity.stream()
						.map(OrderInfoDTO::getId).distinct().collect(Collectors.toList()));


			}
		} else {

			orderInfoListWithOutCity = orderInfoDAO.getOrdersByDate(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
			if (orderInfoListWithOutCity.size() > 0) {
				addressDTOList = addressDAO.getByIds(orderInfoListWithOutCity.stream().map(OrderInfoDTO::getAddressId)
						.distinct().collect(Collectors.toList()));
				orderDetailsList = orderDetailsInfoDAO.getSettledOrderByOrderIdList(orderInfoListWithOutCity.stream()
						.map(OrderInfoDTO::getId).distinct().collect(Collectors.toList()));
			}
		}

		if (request.getTimeSlot().equals("All")) {
			if (!request.getCityCode().equals("-1")) {
				customerList = orderInfoListWithCity.stream().map(OrderInfoDTO::getCustomerId).distinct()
						.collect(Collectors.toList());
			} else {
				customerList = orderInfoListWithOutCity.stream().map(OrderInfoDTO::getCustomerId).distinct()
						.collect(Collectors.toList());
			}
		} else {

			if (!request.getCityCode().equals("-1")) {
				customerList = orderInfoListWithCity.stream().filter(e -> e.getTimeSlot().equals(request.getTimeSlot()))
						.map(OrderInfoDTO::getCustomerId).distinct().collect(Collectors.toList());
			} else {
				customerList = orderInfoListWithOutCity.stream()
						.filter(e -> e.getTimeSlot().equals(request.getTimeSlot())).map(OrderInfoDTO::getCustomerId)
						.distinct().collect(Collectors.toList());
			}
		}
		for (Long customerId : customerList) {
			WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
			float maidinMoney = 0;
			if (walletDTO != null) {
				maidinMoney = walletDTO.getMyWalletBalance();
			}
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}

			List<OrderInfoDTO> orderInfoDTOList = null;
			List<String> timeSlotList = null;
			List<Date> dateList = orderInfoDAO.getByCustomerDifferentDeliveryDates(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (Date deliveryDate : dateList) {
				if (request.getTimeSlot().equals("All")) {
					if (!request.getCityCode().equals("-1")) {
						timeSlotList = orderInfoListWithCity.stream()
								.filter(e -> e.getCustomerId().equals(customerId)
										&& e.getDeliveryDate().equals(deliveryDate))
								.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
					} else {
						timeSlotList = orderInfoListWithOutCity.stream()
								.filter(e -> e.getCustomerId().equals(customerId)
										&& e.getDeliveryDate().equals(deliveryDate))
								.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
					}

				} else {
					if (!request.getCityCode().equals("-1")) {
						timeSlotList = orderInfoListWithCity.stream()
								.filter(e -> e.getCustomerId().equals(customerId)
										&& e.getDeliveryDate().equals(deliveryDate)
										&& e.getTimeSlot().equals(request.getTimeSlot()))
								.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
					} else {
						timeSlotList = orderInfoListWithOutCity.stream()
								.filter(e -> e.getCustomerId().equals(customerId)
										&& e.getDeliveryDate().equals(deliveryDate)
										&& e.getTimeSlot().equals(request.getTimeSlot()))
								.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
					}
				}
				for (String timeSlot : timeSlotList) {
					float totalCashOfASlot = 0;
					float totalMMOfSlot = 0;
					TimeSlotInfo timeSlotInfo = new TimeSlotInfo();
					// timeSlotInfo.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					if (!request.getCityCode().equals("-1")) {
						orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotAndCityCode(customerId,
								deliveryDate, timeSlot, request.getCityCode());
					} else {
						orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlot(customerId, deliveryDate,
								timeSlot);
					}
					for (OrderInfoDTO orderInfo : orderInfoDTOList) {
						AddressDTO addressDTO = addressDTOList.stream()
								.filter(e -> e.getId().equals(orderInfo.getAddressId())).findFirst().orElse(null);
						if (addressDTO == null) {
							continue;
						}
						List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsList.stream()
								.filter(e -> e.getOrderId().equals(orderInfo.getId())).distinct()
								.collect(Collectors.toList());
						orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot
								+ orderDetailsInfoList.size();
						for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
							OrderFormat orderRow = new OrderFormat();
							float cashPerOrder = 0;
							float mmPerOrder = 0;
							if (customerDTO != null) {
								orderRow.setName(customerDTO.getFullName());

								orderRow.setProject(addressDTO.getProject());
								orderRow.setTower(addressDTO.getTower());
								orderRow.setFlat(addressDTO.getFlat());
								orderRow.setMobile(customerDTO.getMobileNumber());
								orderRow.setEmail(customerDTO.getEmail());
								orderRow.setDeliverySlot(orderInfo.getTimeSlot());
								ProductCityInfoV2DTO pcCityInfoV2DTO = productCityInfoV2DAO
										.getByIdWithoutActiveStatus(orderDetailsInfo.getProductId());
								if (pcCityInfoV2DTO == null) {
									continue;
								}
								ProductInfoV2DTO pcInfoV2DTO = productInfoV2DAO
										.getByIdWithoutActiveState(pcCityInfoV2DTO.getProductId());
								if (pcInfoV2DTO == null) {
									continue;
								}
								orderRow.setProduct(pcInfoV2DTO.getProductName());
								orderRow.setUnit(pcCityInfoV2DTO.getQuantityUnit());
								BrandInfoV2DTO brandDTO = brandInfoV2DAO.getByBrandId(pcInfoV2DTO.getBrandId());
								orderRow.setBrand(brandDTO.getBrand());
								CategoryInfoV2DTO categoryDTO = categoryInfoV2DAO
										.getById(pcCityInfoV2DTO.getCategoryId());
								if (categoryDTO == null)
									continue;
								orderRow.setCategory(categoryDTO.getCategory());
								orderRow.setQuantity(orderDetailsInfo.getQuantity());
								float billAmount = 0;
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									billAmount = orderDetailsInfo.getTotalProductCost();
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									billAmount = (orderDetailsInfo.getProductSalePrice() > 0
											? (orderDetailsInfo.getProductSalePrice()
													+ orderDetailsInfo.getDeliveryCharges())
											: (pcCityInfoV2DTO.getSalePrice() + pcCityInfoV2DTO.getDeliveryCharge()))
											* orderDetailsInfo.getQuantity();
								}
								orderRow.setBillAmount(billAmount);
								System.out.println(
										"customerId: " + orderInfo.getCustomerId() + "  orderId:" + orderInfo.getId());
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PAYMENTSETTLED.getName())) {

										List<CustomerTransactionDTO> transList = transactionList.stream()
												.filter(e -> ((e.getCustomerId() != null && e.getOrderDetailsId() != null)
														&& (e.getCustomerId() == orderInfo.getCustomerId().longValue())
														&& (e.getOrderDetailsId() == orderDetailsInfo.getId().longValue())))
												.collect(Collectors.toList());
										orderRow.setPaidByMaidinMoney(
												(float) transList.stream().filter(e -> e.getPaymentModeId() == 2)
														.mapToDouble(CustomerTransactionDTO::getMoneyOut).sum());
										if (!Strings.isNullOrEmpty(orderDetailsInfo.getPayBy())) {
											List<Integer> paymentModeList = Stream
													.of(orderDetailsInfo.getPayBy().startsWith(",")
															? orderDetailsInfo.getPayBy().substring(1).split(",")
															: orderDetailsInfo.getPayBy().split(","))
													.map(Integer::parseInt).collect(Collectors.toList());
											orderRow.setPaymentMode(paymentModeDTOList.stream()
													.filter(e -> paymentModeList.contains(e.getId()))
													.map(PaymentModeDTO::getPaymentModeAbbr)
													.collect(Collectors.joining(",")));
										}

									} else if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PARTIALPAYMENT.getName())) {
										if (orderInfo.getRequestedPaymentModeId() == null) {
											orderInfo.setRequestedPaymentModeId(1);
										}
										orderRow.setPaymentMode(paymentModeDTOList.stream()
												.filter(e -> e.getId() == orderInfo.getRequestedPaymentModeId())
												.map(PaymentModeDTO::getPaymentModeAbbr).findFirst().orElse("NA"));
										float amountToPay = orderDetailsInfo.getTotalProductCost()
												- orderDetailsInfo.getAmountPaidByCustomer();
										if (maidinMoney >= (amountToPay)) {
											orderRow.setPaidByMaidinMoney(
													orderDetailsInfo.getAmountPaidByCustomer() + amountToPay);
											orderRow.setCashCollect(orderDetailsInfo.getTotalProductCost()
													- (orderDetailsInfo.getAmountPaidByCustomer() + amountToPay));
											maidinMoney = maidinMoney - amountToPay;
										} else {
											orderRow.setPaidByMaidinMoney(
													orderDetailsInfo.getAmountPaidByCustomer() + maidinMoney);
											orderRow.setCashCollect(orderDetailsInfo.getTotalProductCost()
													- (orderDetailsInfo.getAmountPaidByCustomer() + maidinMoney));
											maidinMoney = 0;
										}
										// orderRow.setPaidByMaidinMoney(orderDetailsInfo.getAmountPaidByCustomer());
									} else if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PENDING.getName())) {
										if (maidinMoney >= billAmount) {
											maidinMoney = maidinMoney - billAmount;
											orderRow.setPaymentMode(
													PaymentMode.fromId(PaymentMode.ByWallet.getId()).getName());
											orderRow.setCashCollect(0);
											orderRow.setPaidByMaidinMoney(billAmount);
										} else if (maidinMoney > 0 && billAmount > maidinMoney) {
											orderRow.setPaymentMode(
													PaymentMode.fromId(orderInfo.getRequestedPaymentModeId() == null ? 1
															: orderInfo.getRequestedPaymentModeId()).getName());
											orderRow.setCashCollect(billAmount - maidinMoney);
											orderRow.setPaidByMaidinMoney(maidinMoney);
											maidinMoney = 0;
										} else {
											orderRow.setPaymentMode(
													PaymentMode.fromId(orderInfo.getRequestedPaymentModeId() == null ? 1
															: orderInfo.getRequestedPaymentModeId()).getName());
											if (maidinMoney < 0) {
												orderRow.setCashCollect(billAmount - maidinMoney);
												maidinMoney = 0;
											} else if (maidinMoney == 0) {
												orderRow.setCashCollect(billAmount);
											}
											orderRow.setPaidByMaidinMoney(0);
										}
									}
								} else if (orderDetailsInfo.getOrderStatus()
										.equals(OrderStatus.UNDELIVERED.getName())) {
									orderRow.setPaymentMode("NA");
									orderRow.setCashCollect(0);
									orderRow.setPaidByMaidinMoney(0);
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									if (orderDetailsInfo.getPaymentStatus().equals(PaymentStatus.PENDING.getName())) {
										orderRow.setPaymentMode(
												PaymentMode.fromId(PaymentMode.PENDING.getId()).getName());
										orderRow.setCashCollect(0);
										orderRow.setPaidByMaidinMoney(0);

									} else {
										List<CustomerTransactionDTO> transList = transactionList.stream()
												.filter(e -> (e.getCustomerId() != null && e.getOrderDetailsId() != null)
														&& (e.getCustomerId() == orderInfo.getCustomerId().longValue())
														&& (e.getOrderDetailsId() == orderDetailsInfo.getId().longValue()))
												.collect(Collectors.toList());
										/*transList = transList.stream()
												.filter(e -> e.getOrderDetailsId().equals(orderDetailsInfo.getId()))
												.collect(Collectors.toList());
										*/if (orderDetailsInfo.getPaymentStatus()
												.equals(PaymentStatus.PARTIALPAYMENT.getName())) {
											orderRow.setPaymentMode(
													PaymentMode.fromId(PaymentMode.PARTIAL_PAYMENT.getId()).getName());
										} else {
											List<Integer> paymentModeList = transList.stream()
													.map(CustomerTransactionDTO::getPaymentModeId).distinct()
													.collect(Collectors.toList());
											orderRow.setPaymentMode(paymentModeDTOList.stream()
													.filter(e -> paymentModeList.contains(e.getId()))
													.map(PaymentModeDTO::getPaymentModeAbbr)
													.collect(Collectors.joining(",")));
										}
										for (CustomerTransactionDTO transactionDTO : transList) {
											if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
												mmPerOrder = mmPerOrder
														+ (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn());
											} else {
												cashPerOrder = cashPerOrder
														+ (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn());
											}
										}
										orderRow.setCashCollect(cashPerOrder);
										orderRow.setPaidByMaidinMoney(mmPerOrder);
										transList = null;
									}
								}

								if (walletDTO != null) {
									orderRow.setMaidinMoney(walletDTO.getMyWalletBalance());
								}
								orderRow.setOrderedTime(
										ApiUtils.dateTimeWithMonthNameFormat(orderInfo.getOrderedTime()));
								orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
								orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
								timeSlotInfo.getOrderFormatList().add(orderRow);
								totalCashOfASlot = totalCashOfASlot + orderRow.getCashCollect();
								totalMMOfSlot = totalMMOfSlot + orderRow.getPaidByMaidinMoney();
							}
						}
					}
					timeSlotInfo.setCashCollect(totalCashOfASlot);
					timeSlotInfo.setPaidyMaidinMoney(totalMMOfSlot);
					customerOrder.getTimeSlotId().add(timeSlotInfo);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;

	}

	private void convertSingleSlotOrderReportWithoutAccountInfoToExcel(OrderRequest request,
			CustomerOrderReport response, AdminDTO adminDTO)
			throws JSONException, MessagingException, FileNotFoundException, IOException {
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = (XSSFCellStyle) workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = (XSSFCellStyle) workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = (XSSFCellStyle) workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createSingleSlotOrderReportWithoutAccountInfoHeader(sheet, rowCount, request, adminDTO);
		rowCount = 1; // two headers added
		int rf = 2;
		int rt = 0;

		int sNo = 1;

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
					cellStyle.setDataFormat(dateFormat);

					int cellSequence = 0;
					Cell sNoVal = row2.createCell(cellSequence);
					sNoVal.setCellValue(sNo);
					sNoVal.setCellStyle(textStyle);

					sNo++;

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell categoryVal = row2.createCell(cellSequence);
					categoryVal.setCellValue(rec.getCategory());
					categoryVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);
					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 182, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(55, 178, 73)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (rt != rf && rf < rt) {
					// merge Customer name
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

					// merge project
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

					// merge tower
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

					// merge flat
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

					// merge mobile number
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

					// merge Ordered Time
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));

					rf = rt + 1;
					rt = rt + timeSlotInfo.getOrderFormatList().size();
				} else {
					rf++;
				}
			}

			// set serial No width to autoFit
			/* sheet.autoSizeColumn(0); */
			sheet.setColumnWidth(0, 256 * 15);
			// limit Customer name width to 15 characters
			sheet.setColumnWidth(1, 256 * 15);

			// limit Project name width to 20 characters
			sheet.setColumnWidth(2, 256 * 20);

			// limit Tower name width to 10 characters
			sheet.setColumnWidth(3, 256 * 10);

			// limit Flat name width to 10 characters
			sheet.setColumnWidth(4, 256 * 10);

			// limit Mobile number width to 13 characters
			sheet.setColumnWidth(5, 256 * 13);

			// limit category width to 15 characters
			sheet.setColumnWidth(6, 256 * 15);

			// limit brand width to 15 characters
			sheet.setColumnWidth(7, 256 * 15);

			// limit product name width to 20 characters
			sheet.setColumnWidth(8, 256 * 20);

			// limit unit width to 10 characters
			sheet.setColumnWidth(9, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(10, 256 * 5);

			// limit bill amount width to 9 characters
			sheet.setColumnWidth(11, 256 * 9);

			// limit Ordered Time width to 14 characters
			sheet.setColumnWidth(12, 256 * 25);

			// limit Order Status width to 14 characters
			sheet.setColumnWidth(13, 256 * 15);
			// set payment mode to auto fit
			/* sheet.autoSizeColumn(12); */

			// limit Ordered Time width to 14 characters
			/* sheet.setColumnWidth(16, 256 * 14); */

			// set Order Status to auto fit
			/* sheet.autoSizeColumn(17); */

		}

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String finalDateString = fromDate;
		File file = new File("OrderReport " + " - "
				+ (request.getTimeSlot().equalsIgnoreCase("All") ? "All timeslots" : request.getTimeSlot()) + "    "
				+ finalDateString + " - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : " + request.getTimeSlot();
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);
		logger.info("Order report email SENT");

	}

	private void createSingleSlotOrderReportWithoutAccountInfoHeader(SXSSFSheet sheet, int rowCount,
			OrderRequest request, AdminDTO adminDTO) throws JSONException {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));


		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue("Delivery report of " + fromDate + " | " + request.getTimeSlot()
				+ ".  Downloaded by - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())));
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 17));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		int cellSequenc = 0;
		Cell sNoHeader = row.createCell(cellSequenc);
		sNoHeader.setCellValue("S No.");
		sNoHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);
		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
	}

	private CustomerOrderReport generateOrderReportWithOutAccountInfo(OrderRequest request, Locale locale) {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = new ArrayList<>();
		List<Object[]> list = new ArrayList<>();
		final List<CustomerOrder> customerOrdersList = new ArrayList<>();

		// get wallet balance of all customers
		// List<WalletDTO> walletDTOList = walletDAO.getAll();
		// get orderList by from date and two date and timeSlot
		if (!request.getTimeSlot().equals("All")) {
			if (request.getCityCode().equals("-1")) {
				list = orderInfoDAO.getCustomerOrdersByDateAndSlot(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()), request.getTimeSlot());
				System.out.println();
			} else {
				list = orderInfoDAO.getCustomerOrdersByDateSlotAndCityCode(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()), request.getTimeSlot(), request.getCityCode());

			}
		} else {
			if (request.getCityCode().equals("-1")) {
				list = orderInfoDAO.getCustomerOrdersByDate(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()));
			} else {
				list = orderInfoDAO.getCustomerOrdersByDateAndCityCode(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()), request.getCityCode());
			}
		}

		// fetch all data from db and add to list
		list.stream().forEach((record) -> {
			CustomerOrder customerOrder = addCustomerOrderInfo(record, locale);
			customerOrdersList.add(customerOrder);
		});

		// get unique customers from list and iterate by customerId
		customerList = customerOrdersList.stream().map(CustomerOrder::getCustomerId).distinct()
				.collect(Collectors.toList());
		for (Long customerId : customerList) {

			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			// List<OrderInfoDTO> orderInfoDTOList = null;
			List<String> timeSlotList = null;
			// get sublist from customerOrdersList to get specific customer Data
			// into customerOrdersList1
			List<CustomerOrder> customerOrdersList1 = customerOrdersList.stream()
					.filter((u) -> u.getCustomerId() == customerId).collect(Collectors.toList());

			// get date list from customerOrdersList1 and iterate by date
			List<java.util.Date> dateList = customerOrdersList1.stream().map(CustomerOrder::getDeliveryDate).distinct()
					.collect(Collectors.toList());
			for (java.util.Date deliveryDate : dateList) {

				// get sublist of customerOrdersList1 by specifying particular
				// date
				customerOrdersList1 = customerOrdersList1.stream()
						.filter((u) -> u.getDeliveryDate().equals(deliveryDate)).collect(Collectors.toList());

				// get timeSlot list of customers of particular delivery date by
				// specifying timeSlot is all or specific timeslot
				if (request.getTimeSlot().equals("All")) {
					timeSlotList = customerOrdersList1.stream().map(CustomerOrder::getDeliverySlot).distinct()
							.collect(Collectors.toList());
				} else {
					timeSlotList = customerOrdersList1.stream()
							.filter((u) -> u.getDeliverySlot().equals(request.getTimeSlot()))
							.map(CustomerOrder::getDeliverySlot).distinct().collect(Collectors.toList());
				}

				// iterate orderDetails by customer,date and timeslotId
				for (String timeSlot : timeSlotList) {
					TimeSlotInfo timeSlotInfo = new TimeSlotInfo();
					// timeSlot.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					customerOrdersList1 = customerOrdersList1.stream()
							.filter((u) -> u.getDeliverySlot().equals(timeSlot)).collect(Collectors.toList());
					orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot + customerOrdersList1.size();
					for (CustomerOrder customerOrder2 : customerOrdersList1) {

						OrderFormat orderRow = new OrderFormat();
						orderRow.setName(customerOrder2.getName());
						orderRow.setProject(customerOrder2.getProject());
						orderRow.setTower(customerOrder2.getTower());
						orderRow.setFlat(customerOrder2.getFlat());
						orderRow.setMobile(customerOrder2.getMobile());
						orderRow.setEmail(customerOrder2.getEmail());
						orderRow.setDeliverySlot(customerOrder2.getDeliverySlot());
						orderRow.setProduct(customerOrder2.getProduct());
						orderRow.setUnit(customerOrder2.getUnit());
						orderRow.setBrand(customerOrder2.getBrand());
						orderRow.setCategory(customerOrder2.getCategory());
						orderRow.setQuantity(customerOrder2.getQuantity());
						orderRow.setBillAmount(customerOrder2.getBillAmount());
						orderRow.setOrderedTime(ApiUtils
								.dateTimeWithMonthNameFormat(Timestamp.valueOf(customerOrder2.getOrderedTime())));
						orderRow.setDeliveryDate(customerOrder2.getDeliveryDate());
						orderRow.setOrderStatus(customerOrder2.getOrderStatus());
						timeSlotInfo.getOrderFormatList().add(orderRow);
					}
					customerOrder.getTimeSlotId().add(timeSlotInfo);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;

	}

	private CustomerOrderReport generateOrderReportWithOutAccountInfo1(OrderRequest request, Locale locale) {

		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		// TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot().equals("All")) {
			customerList = orderInfoDAO.getDifferentCustomersByTimestamp(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlot(
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), request.getTimeSlot());
		}
		for (Long customerId : customerList) {

			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				continue;
			}
			List<OrderInfoDTO> orderInfoDTOList = null;
			List<Integer> timeSlotList = null;
			List<Date> dateList = orderInfoDAO.getByCustomerDifferentDeliveryDates(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (Date deliveryDate : dateList) {
				if (request.getTimeSlot().equals("All")) {
					timeSlotList = orderInfoDAO.getTimeSlotByCustomerDeliveryDate(customerId, deliveryDate);
				} else {
					timeSlotList = orderInfoDAO.getTimeSlotsByCustomerDeliveryDateAndTimeSlot(customerId, deliveryDate,
							request.getTimeSlot());
				}
				for (Integer timeSlotId : timeSlotList) {
					TimeSlotInfo timeSlot = new TimeSlotInfo();
					timeSlot.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotId(customerId, deliveryDate,
							timeSlotId);
					for (OrderInfoDTO orderInfo : orderInfoDTOList) {

						List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
								.getSettledOrderDetailsByOrderId(orderInfo.getId());
						orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot
								+ orderDetailsInfoList.size();
						for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
							OrderFormat orderRow = new OrderFormat();
							if (customerDTO != null) {
								orderRow.setName(customerDTO.getFullName());
								orderRow.setProject(addressDTO.getProject());
								orderRow.setTower(addressDTO.getTower());
								orderRow.setFlat(addressDTO.getFlat());
								orderRow.setMobile(customerDTO.getMobileNumber());
								orderRow.setEmail(customerDTO.getEmail());
								orderRow.setDeliverySlot(orderInfo.getTimeSlot());
								ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
								if (productDTO == null)
									continue;
								orderRow.setProduct(productDTO.getProductName());
								orderRow.setUnit(productDTO.getQuantityUnit());
								ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
								orderRow.setBrand(brandDTO.getBrand());
								ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
								if (categoryDTO == null)
									continue;
								orderRow.setCategory(categoryDTO.getCategory());
								orderRow.setQuantity(orderDetailsInfo.getQuantity());
								float billAmount = 0;
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									billAmount = (orderDetailsInfo.getProductMRP() > 0
											? orderDetailsInfo.getProductMRP()
											: productDTO.getPrice()) * orderDetailsInfo.getQuantity();
								}
								orderRow.setBillAmount(billAmount);
								orderRow.setOrderedTime(
										ApiUtils.dateTimeWithMonthNameFormat(orderInfo.getOrderedTime()));
								orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
								orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
								timeSlot.getOrderFormatList().add(orderRow);

							}
						}
					}
					customerOrder.getTimeSlotId().add(timeSlot);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;

	}

	private Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}

	boolean sameTransaction(List<CustomerTransactionDTO> transactionDTOList) {
		for (int i = 0; i < transactionDTOList.size(); i++) {
			if (transactionDTOList.get(0).getPaymentModeId() != transactionDTOList.get(i).getPaymentModeId()) {
				return false;
			}
		}
		return true;
	}

	private org.json.simple.JSONObject readTimeSlotArray(org.json.simple.JSONArray timeSlotArray, int timeSlotId)
			throws JSONException {
		org.json.simple.JSONObject timeslotRequest = new org.json.simple.JSONObject();
		for (int i = 0; i < timeSlotArray.size(); i++) {
			org.json.simple.JSONObject timeSlotJSON = (org.json.simple.JSONObject) timeSlotArray.get(i);
			if ((Long) timeSlotJSON.get("id") == (timeSlotId)) {
				timeslotRequest.put("Id", (Long) timeSlotJSON.get("id"));
				timeslotRequest.put("timeSlot", (String) timeSlotJSON.get("timeSlot"));
				timeslotRequest.put("lockingPeriod", (Long) timeSlotJSON.get("lockingPeriod"));
				timeslotRequest.put("slotStartTime", (Long) timeSlotJSON.get("slotStartTime"));
				timeslotRequest.put("slotEndTime", (Long) timeSlotJSON.get("slotEndTime"));
				break;
			}

		}
		return timeslotRequest;
	}

	private CustomerOrderDetailsInfo generateMultipleDayOrderReportV2(OrderRequest request, Locale locale)
			throws InterruptedException, ExecutionException {
		CustomerOrderDetailsInfo response = new CustomerOrderDetailsInfo();
		List<Object[]> list = new ArrayList<>();
		if (!request.getTimeSlot().equals("All")) {
			if (request.getCityCode().equals("-1")) {
				list = orderInfoDAO.getCustomerOrdersByDateAndSlot(new Date(request.getFromTimestamp()),

						new Date(request.getToTimestamp()), request.getTimeSlot());

			} else {
				list = orderInfoDAO.getCustomerOrdersByDateSlotAndCityCode(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()), request.getTimeSlot(), request.getCityCode());

			}
		} else {
			if (request.getCityCode().equals("-1")) {
				list = orderInfoDAO.getCustomerOrdersByDate(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()));
			} else {
				list = orderInfoDAO.getCustomerOrdersByDateAndCityCode(new Date(request.getFromTimestamp()),
						new Date(request.getToTimestamp()), request.getCityCode());
			}
		}
		list.stream().forEach((record) -> {
			CustomerOrder customerOrder = new CustomerOrder();
			try {
				customerOrder.setDeliveryDate(ApiUtils.toDate(record[0].toString(), locale));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
			customerOrder.setDeliverySlot(String.valueOf(record[1].toString()));
			// customerOrder.setCustomerId(String.valueOf(record[2].toString()));
			customerOrder.setName(String.valueOf(record[3].toString()));
			customerOrder.setProject(String.valueOf(record[4].toString()));
			customerOrder.setTower(String.valueOf(record[5].toString()));
			customerOrder.setFlat(String.valueOf(record[6].toString()));
			customerOrder.setMobile(String.valueOf(record[7].toString()));
			customerOrder.setCategory(String.valueOf(record[8].toString()));
			customerOrder.setBrand(String.valueOf(record[9].toString()));
			customerOrder.setProduct(String.valueOf(record[10].toString()));
			customerOrder.setUnit(String.valueOf(record[11].toString()));
			customerOrder.setQuantity(Integer.parseInt(record[12].toString()));
			if (Float.parseFloat(record[17].toString()) == 0) {
				customerOrder.setBillAmount(
						Float.parseFloat(record[18].toString()) * Integer.parseInt(record[12].toString()));
			} else {
				customerOrder.setBillAmount(Float.parseFloat(record[13].toString()));
			}
			if (String.valueOf(record[16].toString()).equals(OrderStatus.UNDELIVERED.getName())) {
				customerOrder.setPaymentMode("NA");
				customerOrder.setBillAmount(0);
			} else {
				if (String.valueOf(record[22].toString()).equals(PaymentStatus.PENDING.getName())
						|| String.valueOf(record[22].toString()).equals(PaymentStatus.PARTIALPAYMENT.getName())) {
					if (String.valueOf(record[16].toString()).equals(OrderStatus.CONFIRMED.getName())) {
						try {
							customerOrder.setPaymentMode(PaymentMode
									.fromId(Integer.valueOf(
											(record[25] == null ? PaymentMode.NA.getId() : record[25]).toString()))
									.getName());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();

						}
					} else if (String.valueOf(record[16].toString()).equals(OrderStatus.DELIVERED.getName())) {
						try {
							if (String.valueOf(record[22].toString()) == PaymentStatus.PARTIALPAYMENT.getName()) {
								customerOrder.setPaymentMode(
										PaymentMode.fromId(PaymentMode.PARTIAL_PAYMENT.getId()).getName());
							} else if (String.valueOf(record[22].toString()) == PaymentStatus.PARTIALPAYMENT
									.getName()) {
								customerOrder.setPaymentMode(
										PaymentMode.fromId(PaymentMode.PARTIAL_PAYMENT.getId()).getName());
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
					}
				} else {
					// need to take from db
					customerOrder.setPaymentMode(String.valueOf(record[26] == null ? "NA" : record[26].toString()));
				}
			}
			if (record[15] != null) {
				customerOrder.setOrderedTime(ApiUtils.dateTimeFormat(Timestamp.valueOf(record[15].toString())));
			}
			System.out.println("orderdetails: " + customerOrder.getOrderDetailsId() + "paymentStatus: "
					+ customerOrder.getPaymentMode());
			customerOrder.setOrderStatus(String.valueOf(record[16].toString()));
			response.getCustomerOrderList().add(customerOrder);
			customerOrder = null;
		});
		list = null;
		return response;

	}

	private void convertMultipleDayOrderReportToExcel(OrderRequest request, CustomerOrderDetailsInfo response,
			String email) throws MessagingException, JSONException, FileNotFoundException, IOException {

		logger.info("Excel creation started with formatting");

		// XSSFWorkbook workbook = new XSSFWorkbook();
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = (XSSFCellStyle) workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = (XSSFCellStyle) workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = (XSSFCellStyle) workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		Font font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		AtomicInteger rowCount = new AtomicInteger();
		createOrderReportheader(sheet, rowCount.get(), request);
		short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");

		response.getCustomerOrderList().stream().forEach((rec) -> {
			Row row2 = sheet.createRow(rowCount.incrementAndGet());

			cellStyle.setDataFormat(dateFormat);
			int cellSequence = 0;
			Cell del_DateVal = row2.createCell(cellSequence);
			del_DateVal.setCellValue(rec.getDeliveryDate());
			del_DateVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell del_SlotVal = row2.createCell(cellSequence);
			del_SlotVal.setCellValue(rec.getDeliverySlot() == null ? "" : rec.getDeliverySlot());
			del_SlotVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell nameVal = row2.createCell(cellSequence);
			nameVal.setCellValue(rec.getName() == null ? "" : rec.getName());
			nameVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell projVal = row2.createCell(cellSequence);
			projVal.setCellValue(rec.getProject() == null ? "" : rec.getProject());
			projVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell towerVal = row2.createCell(cellSequence);
			towerVal.setCellValue(rec.getTower() == null ? "" : rec.getTower());
			towerVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell flatVal = row2.createCell(cellSequence);
			flatVal.setCellValue(rec.getFlat() == null ? "" : rec.getFlat());
			flatVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell mobVal = row2.createCell(cellSequence);
			mobVal.setCellValue(rec.getMobile() == null ? "" : rec.getMobile());
			mobVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell catVal = row2.createCell(cellSequence);
			catVal.setCellValue(rec.getCategory() == null ? "" : rec.getCategory());
			catVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell brandVal = row2.createCell(cellSequence);
			brandVal.setCellValue(rec.getBrand() == null ? "" : rec.getBrand());
			brandVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell productVal = row2.createCell(cellSequence);
			productVal.setCellValue(rec.getProduct() == null ? "" : rec.getProduct());
			productVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell unitVal = row2.createCell(cellSequence);
			unitVal.setCellValue(rec.getUnit());
			unitVal.setCellStyle(textStyle);

			cellSequence = cellSequence + 1;
			Cell quantityVal = row2.createCell(cellSequence);
			quantityVal.setCellValue(rec.getQuantity());
			quantityVal.setCellStyle(integerValueStyle);

			cellSequence = cellSequence + 1;
			Cell billAmountVal = row2.createCell(cellSequence);
			billAmountVal.setCellValue(rec.getBillAmount());
			billAmountVal.setCellStyle(integerValueStyle);

			cellSequence = cellSequence + 1;
			Cell paymentModeVal = row2.createCell(cellSequence);
			paymentModeVal.setCellValue(rec.getPaymentMode() == null ? "NA" : rec.getPaymentMode());
			paymentModeVal.setCellStyle(cellStyle);

			cellSequence = cellSequence + 1;
			Cell orderedTimeVal = row2.createCell(cellSequence);
			orderedTimeVal.setCellValue(rec.getOrderedTime() == null ? "" : rec.getOrderedTime());
			orderedTimeVal.setCellStyle(cellStyle);

			XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

			if (rec.getOrderStatus().equalsIgnoreCase(OrderStatus.CONFIRMED.getName())) {
				// font.setColor(null);
				// font.setColor(new Color(new java.awt.Color(255, 182, 0)));
				font.setColor(IndexedColors.ORANGE.getIndex());
				richTextString.applyFont(0, rec.getOrderStatus().length(), font);
			} else if (rec.getOrderStatus().equalsIgnoreCase(OrderStatus.DELIVERED.getName())) {
				// font.setColor(null);
				// font.setColor(new XSSFColor(new java.awt.Color(55, 178,
				// 73)));
				font.setColor(IndexedColors.GREEN.getIndex());
				richTextString.applyFont(0, rec.getOrderStatus().length(), font);
			} else if (rec.getOrderStatus().equalsIgnoreCase(OrderStatus.UNDELIVERED.getName())) {
				// font.setColor(null);
				// font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
				font.setColor(IndexedColors.RED.getIndex());
				richTextString.applyFont(0, rec.getOrderStatus().length(), font);
			} else {
				// font.setColor(null);
				// font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
				font.setColor(IndexedColors.ORANGE.getIndex());
				richTextString.applyFont(0, rec.getOrderStatus().length(), font);
			}

			cellSequence = cellSequence + 1;
			Cell orderStatusVal = row2.createCell(cellSequence);
			orderStatusVal.setCellValue(rec.getOrderStatus() == null ? "" : rec.getOrderStatus());
			orderStatusVal.setCellStyle(orderStatusStyle);
			orderStatusVal.setCellValue(richTextString);
			richTextString = null;
		});

		// limit delivery date width to 12 characters
		sheet.setColumnWidth(0, 256 * 12);

		/*
<<<<<<< HEAD
		 * for (int i = 1; i < 7; i++) { // sheet.autoSizeColumn(i); } for (int i = 7; i
		 * < 10; i++) { // sheet.autoSizeColumn(i); }
=======
		 * for (int i = 1; i < 7; i++) { // sheet.autoSizeColumn(i); } for (int
		 * i = 7; i < 10; i++) { // sheet.autoSizeColumn(i); }
>>>>>>> refs/heads/PaymentModeAddition
		 */
		sheet.setColumnWidth(0, 256 * 14);
		// limit delivery slot width to 15 characters
		sheet.setColumnWidth(1, 256 * 14);

		// limit Customer name width to 15 characters
		sheet.setColumnWidth(2, 256 * 15);

		// limit Project name width to 20 characters
		sheet.setColumnWidth(3, 256 * 20);

		// limit Tower name width to 10 characters
		sheet.setColumnWidth(4, 256 * 10);

		// limit Flat name width to 10 characters
		sheet.setColumnWidth(5, 256 * 10);

		// limit Mobile number width to 13 characters
		sheet.setColumnWidth(6, 256 * 13);

		// sheet.autoSizeColumn(13);
		// sheet.autoSizeColumn(16);

		// limit category width to 18 characters
		sheet.setColumnWidth(7, 256 * 18);

		// limit brand width to 15 characters
		sheet.setColumnWidth(8, 256 * 15);

		// limit product name width to 25 characters
		sheet.setColumnWidth(9, 256 * 25);

		// limit unit width to 10 characters
		sheet.setColumnWidth(10, 256 * 10);

		// limit quantity width to 5 characters
		sheet.setColumnWidth(11, 256 * 5);

		// limit bill amount width to 8 characters
		sheet.setColumnWidth(12, 256 * 8);

		// limit Payment mode width to 16 characters
		sheet.setColumnWidth(13, 256 * 16);

		// limit ordered time width to 16 characters
		sheet.setColumnWidth(14, 256 * 16);

		// limit order status width to 16 characters
		sheet.setColumnWidth(15, 256 * 16);

		// limit Cash need to collect width to 8 characters
		sheet.setColumnWidth(16, 256 * 8);

		// limit Ordered time width to 13 characters
		sheet.setColumnWidth(17, 256 * 13);

		// limit Order status width to auto fit
		// sheet.autoSizeColumn(18);

		logger.info("Excel creation END with formatting");

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));
		String finalDateString = fromDate + "  TO  " + toDate;
		File file = new File("OrderReport " + " - "
				+ (request.getTimeSlot().equalsIgnoreCase("All") ? "All timeslots" : request.getTimeSlot()) + "    "
				+ finalDateString + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
			outputStream.flush();
			workbook.dispose();
			workbook.close();
			// workbook = new SXSSFWorkbook(new FileInputStream(file));
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : " + request.getTimeSlot();
		EmailSenderWithAttachment.sendMail(email, subject, "Attachment", multipart);
		logger.info("Order report email SENT");

	}

	private String paymentMode(org.json.simple.JSONArray paymentModeArray, int paymentModeId) {
		String paymentMode = null;
		for (int i = 0; i < paymentModeArray.size(); i++) {
			org.json.simple.JSONObject paymentModeJSON = (org.json.simple.JSONObject) paymentModeArray.get(i);
			if ((Long) paymentModeJSON.get("id") == (paymentModeId)) {
				return (String) paymentModeJSON.get("paymentMode");

			}

		}
		return paymentMode;

	}

	private org.json.simple.JSONArray readPaymentModeJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray paymentModeArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			paymentModeArray = (org.json.simple.JSONArray) jsonObject.get("paymentModeList");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return paymentModeArray;

	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public OrderReportResponse getBalanceReport(BalanceReportRequest request, String authToken, Locale locale)
			throws Exception {

		OrderReportResponse response = new OrderReportResponse();
		// String mobile = "9811611743";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD, adminDTO.getId());
		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		new Thread(() -> {
			try {
				callAsynchronouslyBalanceReport(request, adminDTO, locale);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void callAsynchronouslyBalanceReport(BalanceReportRequest request, AdminDTO adminDTO, Locale locale)
			throws FileNotFoundException, MessagingException, JSONException {
		try {
			BalanceReportResponse reportResponse = new BalanceReportResponse();
			reportResponse = generateBalanceReport(request, locale);
			convertBalanceReportToExcel(reportResponse, adminDTO);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void convertBalanceReportToExcel(BalanceReportResponse response, AdminDTO adminDTO)
			throws IOException, MessagingException {

		logger.info("Excel creation started with formatting");

		// XSSFWorkbook workbook = new XSSFWorkbook();
		SXSSFWorkbook workbook = new SXSSFWorkbook(100);
		SXSSFSheet sheet1 = workbook.createSheet("+ve Balance Report");
		SXSSFSheet sheet2 = workbook.createSheet("-ve Balance Report");
		XSSFCellStyle cellStyle = (XSSFCellStyle) workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = (XSSFCellStyle) workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = (XSSFCellStyle) workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = (XSSFCellStyle) workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		Font font = sheet1.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();


		// Create Header Title of Sheet

		String curDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(System.currentTimeMillis()));
		String headerTitle = ("Maidin Money Report | " + "Date: " + curDate) + ".  Downloaded by - "
				+ adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis()));

		int rowCount1 = 0;
		rowCount1 = createBalanceReportHeader(sheet1, rowCount1, adminDTO.getFullName(), headerTitle);
		int rowCount2 = 0;
		rowCount2 = createBalanceReportHeader(sheet2, rowCount2, adminDTO.getFullName(), headerTitle);
		short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");

		int sheet1serialNo = 1;
		for (CustomerBalanceInfo rec : response.getPositiveBalanceInfoList()) {

			Row row2 = sheet1.createRow(++rowCount1);

			cellStyle.setDataFormat(dateFormat);

			int cellSequenc = 0;

			Cell serialNumber = row2.createCell(cellSequenc);
			serialNumber.setCellValue(sheet1serialNo++);
			serialNumber.setCellStyle(integerValueStyle);

			cellSequenc += 1;
			Cell productHeader = row2.createCell(cellSequenc);
			productHeader.setCellValue(rec.getCustomerName() == null ? "" : rec.getCustomerName());
			productHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell unitHeader = row2.createCell(cellSequenc);
			unitHeader.setCellValue(rec.getMobileNumber() == null ? "" : rec.getMobileNumber());
			unitHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell projectHeader = row2.createCell(cellSequenc);
			projectHeader.setCellValue(rec.getProject() == null ? "" : rec.getProject());
			projectHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell towerHeader = row2.createCell(cellSequenc);
			towerHeader.setCellValue(rec.getTower() == null ? "" : rec.getTower());
			towerHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell flatHeader = row2.createCell(cellSequenc);
			flatHeader.setCellValue(rec.getFlat() == null ? "" : rec.getFlat());
			flatHeader.setCellStyle(textStyle);


			cellSequenc += 1;
			Cell balanceHeader = row2.createCell(cellSequenc);
			balanceHeader.setCellValue((float) rec.getBalance() == 0 ? 0 : (float) rec.getBalance());
			balanceHeader.setCellStyle(integerValueStyle);


		}
		int sheet2serialNo = 1;
		for (CustomerBalanceInfo rec : response.getNegativeBalanceInfoList()) {
			Row row2 = sheet2.createRow(++rowCount2);

			cellStyle.setDataFormat(dateFormat);

			int cellSequenc = 0;

			Cell serialNumber = row2.createCell(cellSequenc);
			serialNumber.setCellValue(sheet2serialNo++);
			serialNumber.setCellStyle(integerValueStyle);

			cellSequenc += 1;
			Cell productHeader = row2.createCell(cellSequenc);
			productHeader.setCellValue(rec.getCustomerName() == null ? "" : rec.getCustomerName());
			productHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell unitHeader = row2.createCell(cellSequenc);
			unitHeader.setCellValue(rec.getMobileNumber() == null ? "" : rec.getMobileNumber());
			unitHeader.setCellStyle(textStyle);


			cellSequenc += 1;
			Cell projectHeader = row2.createCell(cellSequenc);
			projectHeader.setCellValue(rec.getProject() == null ? "" : rec.getProject());
			projectHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell towerHeader = row2.createCell(cellSequenc);
			towerHeader.setCellValue(rec.getTower() == null ? "" : rec.getTower());
			towerHeader.setCellStyle(textStyle);

			cellSequenc += 1;
			Cell flatHeader = row2.createCell(cellSequenc);
			flatHeader.setCellValue(rec.getFlat() == null ? "" : rec.getFlat());
			flatHeader.setCellStyle(textStyle);


			cellSequenc += 1;
			Cell balanceHeader = row2.createCell(cellSequenc);
			balanceHeader.setCellValue((float) rec.getBalance() == 0 ? 0 : (float) rec.getBalance());
			balanceHeader.setCellStyle(integerValueStyle);



		}


		sheet1.autoSizeColumn(0);
		sheet1.autoSizeColumn(1);
		sheet1.autoSizeColumn(2);
		sheet1.autoSizeColumn(3);
		sheet1.autoSizeColumn(4);
		sheet1.autoSizeColumn(5);
		sheet1.autoSizeColumn(6);
		sheet1.autoSizeColumn(7);

		sheet2.autoSizeColumn(0);
		sheet2.autoSizeColumn(1);
		sheet2.autoSizeColumn(2);
		sheet2.autoSizeColumn(3);
		sheet2.autoSizeColumn(4);
		sheet2.autoSizeColumn(5);
		sheet2.autoSizeColumn(6);
		sheet2.autoSizeColumn(7);

		// limit s.No name width to 25 characters
		/*
		 * sheet1.setColumnWidth(0, 256 * 6);
		 * 
<<<<<<< HEAD
		 * // limit mobile name width to 25 characters sheet1.setColumnWidth(1, 256 *
		 * 25);
		 * 
		 * // limit balance width to 10 characters sheet1.setColumnWidth(2, 256 * 16);
		 * 
		 * // limit city width to 5 characters sheet1.setColumnWidth(3, 256 * 12);
		 * 
		 * // limit project width to 8 characters sheet1.setColumnWidth(4, 256 * 18);
		 * 
		 * // limit tower width to 16 characters sheet1.setColumnWidth(5, 256 * 18);
		 * 
		 * // limit flat width to 16 characters sheet1.setColumnWidth(6, 256 * 12);
		 * 
		 * sheet1.setColumnWidth(6, 256 * 12);
		 * 
		 * 
		 * 
		 * // limit flat width to 25 characters sheet2.setColumnWidth(0, 256 * 6);
		 * 
		 * 
		 * 
		 * // limit product name width to 25 characters sheet2.setColumnWidth(1, 256 *
		 * 25);
		 * 
		 * // limit unit width to 10 characters sheet2.setColumnWidth(2, 256 * 16);
		 * 
		 * // limit quantity width to 5 characters sheet2.setColumnWidth(3, 256 * 12);
		 * 
		 * // limit buying price/Unit width to 8 characters sheet2.setColumnWidth(4, 256
		 * * 18);
		 * 
		 * // limit Selling Price/Unit width to 16 characters sheet2.setColumnWidth(5,
		 * 256 * 18);
		 * 
		 * // limit New Todays Selling Price/Unit width to 16 characters
		 * sheet2.setColumnWidth(6, 256 * 12); sheet2.setColumnWidth(6, 256 * 12);
=======
		 * // limit mobile name width to 25 characters sheet1.setColumnWidth(1,
		 * 256 * 25);
		 * 
		 * // limit balance width to 10 characters sheet1.setColumnWidth(2, 256
		 * * 16);
		 * 
		 * // limit city width to 5 characters sheet1.setColumnWidth(3, 256 *
		 * 12);
		 * 
		 * // limit project width to 8 characters sheet1.setColumnWidth(4, 256 *
		 * 18);
		 * 
		 * // limit tower width to 16 characters sheet1.setColumnWidth(5, 256 *
		 * 18);
		 * 
		 * // limit flat width to 16 characters sheet1.setColumnWidth(6, 256 *
		 * 12);
		 * 
		 * sheet1.setColumnWidth(6, 256 * 12);
		 * 
		 * 
		 * 
		 * // limit flat width to 25 characters sheet2.setColumnWidth(0, 256 *
		 * 6);
		 * 
		 * 
		 * 
		 * // limit product name width to 25 characters sheet2.setColumnWidth(1,
		 * 256 * 25);
		 * 
		 * // limit unit width to 10 characters sheet2.setColumnWidth(2, 256 *
		 * 16);
		 * 
		 * // limit quantity width to 5 characters sheet2.setColumnWidth(3, 256
		 * * 12);
		 * 
		 * // limit buying price/Unit width to 8 characters
		 * sheet2.setColumnWidth(4, 256 * 18);
		 * 
		 * // limit Selling Price/Unit width to 16 characters
		 * sheet2.setColumnWidth(5, 256 * 18);
		 * 
		 * // limit New Todays Selling Price/Unit width to 16 characters
		 * sheet2.setColumnWidth(6, 256 * 12); sheet2.setColumnWidth(6, 256 *
		 * 12);
>>>>>>> refs/heads/PaymentModeAddition
		 */

		logger.info("Excel creation END with formatting");

		File file = new File("Maidin Money Report-"
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
			outputStream.flush();
			workbook.dispose();
			workbook.close();

		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Maidin Money Report | Date : "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis()));
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);
		logger.info("Maidin Money Report email SENT");

	}

	private int createBalanceReportHeader(SXSSFSheet sheet, int rowCount, String fullName, String headerTitle) {
		logger.info("HEADER creation started");

		///
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

		XSSFFont font = (XSSFFont) sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorderWithColor(cellStyle, IndexedColors.GREY_80_PERCENT.getIndex());
		///

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue(headerTitle);
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		sheet.trackAllColumnsForAutoSizing();
		int cellSequenc = 0;
		Cell serialNumber = row.createCell(cellSequenc);
		serialNumber.setCellValue("S.No");
		serialNumber.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell numberHeader = row.createCell(cellSequenc);
		numberHeader.setCellValue("Mobile");
		numberHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc += 1;
		Cell balanceHeader = row.createCell(cellSequenc);
		balanceHeader.setCellValue("Balance");
		balanceHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
		logger.info("HEADER creation end");

		return rowCount;
	}

	private void setExcelCellBorderWithColor(CellStyle cellStyle, short indexedColor) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(indexedColor);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(indexedColor);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(indexedColor);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(indexedColor);
	}

	private BalanceReportResponse generateBalanceReport(BalanceReportRequest request, Locale locale) {

		BalanceReportResponse response = new BalanceReportResponse();
		List<String> cityCodeList = cityCategoryInfoDAO.getAllCitiesWithoutActiveState();
		if (request != null && request.getCityCodeList() != null && request.getCityCodeList().size() > 0) {
			cityCodeList = request.getCityCodeList();
		}
		StringJoiner joiner_CityCodes = new StringJoiner("','", "'", "'");
		for (String s : cityCodeList)
			joiner_CityCodes.add(s);

		List<Object[]> list = new ArrayList<>();
		list = orderInfoDAO.getBalanceReport(joiner_CityCodes.toString());
		for (Object[] record : list) {
			CustomerBalanceInfo cBalanceInfo = new CustomerBalanceInfo();
			cBalanceInfo.setCustomerName(String.valueOf(record[0].toString()));
			cBalanceInfo.setMobileNumber(String.valueOf(record[1].toString()));
			cBalanceInfo.setBalance(record[2].toString() == null ? 0 : Float.valueOf(record[2].toString()));
			cBalanceInfo.setCity(String.valueOf(record[3].toString()));
			cBalanceInfo.setProject(String.valueOf(record[4].toString()));
			cBalanceInfo.setTower(String.valueOf(record[5].toString()));
			cBalanceInfo.setFlat(String.valueOf(record[6].toString()));
			response.getConsolidatedBalanceInfoList().add(cBalanceInfo);
			cBalanceInfo = null;
		}

		List<CustomerBalanceInfo> positiveBalance = response.getConsolidatedBalanceInfoList().stream()
				.filter(e -> e.getBalance() >= 0).collect(Collectors.toList());
		List<CustomerBalanceInfo> negativeBalance = response.getConsolidatedBalanceInfoList().stream()
				.filter(e -> e.getBalance() < 0).sorted(Comparator.comparing(CustomerBalanceInfo::getBalance))
				.collect(Collectors.toList());
		response.getPositiveBalanceInfoList().addAll(positiveBalance);
		response.getNegativeBalanceInfoList().addAll(negativeBalance);

		System.out.println("----------------------" + list.size());
		list = null;
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse getLedgerReport(LedgerReportRequest request, String authToken, Locale locale)
			throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		new Thread(() -> {
			try {
				getLedgerReportDataAsynchronously(request, locale, adminDTO);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();

		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Async
	private void getLedgerReportDataAsynchronously(LedgerReportRequest request, Locale locale, AdminDTO adminDTO)
			throws InterruptedException, ExecutionException, JSONException, FileNotFoundException, IOException,
			MessagingException {
		LedgerReportResponse reportResponse = new LedgerReportResponse();
		Calendar cal = Calendar.getInstance();

		reportResponse = fetchLedgerReportData(request, locale);
		exportLedgerDataToExcel(request, reportResponse, adminDTO);
	}

	private LedgerReportResponse fetchLedgerReportData(LedgerReportRequest request, Locale locale)
			throws JSONException {
		LedgerReportResponse ledgerReportResponse = new LedgerReportResponse();
		List<MaidinMoneyLedgerDTO> ledgerDTOList = maidinMoneyLedgerDAO
				.getBetweenDates(new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
		List<LedgerData> ledgerDataList = new ArrayList<LedgerData>();
		for (MaidinMoneyLedgerDTO ledgerDTO : ledgerDTOList) {
			LedgerData ledgerData = new LedgerData();
			ledgerData.setDate(ledgerDTO.getDate());
			ledgerData.setNegativeBalance(ledgerDTO.getNegativeBalance());
			ledgerData.setPositiveBalance(ledgerDTO.getPositiveBalance());

			ledgerDataList.add(ledgerData);
		}
		ledgerReportResponse.getLedgerDataList().addAll(ledgerDataList);
		return ledgerReportResponse;
	}

	private void exportLedgerDataToExcel(LedgerReportRequest request, LedgerReportResponse response, AdminDTO adminDTO)
			throws FileNotFoundException, IOException, MessagingException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = sheet.getWorkbook().createFont();
		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createLedgerReportHeader(sheet, rowCount, request, adminDTO);
		rowCount = 1; // two headers added

		int sNo = 1;

		for (LedgerData data : response.getLedgerDataList()) {
			Row row2 = sheet.createRow(++rowCount);

			short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
			cellStyle.setDataFormat(dateFormat);

			int cellSequence = 0;

			Cell sNOCell = row2.createCell(cellSequence++);
			sNOCell.setCellValue(sNo++);
			sNOCell.setCellStyle(integerValueStyle);

			Cell dateCell = row2.createCell(cellSequence++);
			dateCell.setCellValue(data.getDate());
			dateCell.setCellStyle(cellStyle);

			Cell positiveBalanceCell = row2.createCell(cellSequence++);
			positiveBalanceCell.setCellValue(data.getPositiveBalance());
			positiveBalanceCell.setCellStyle(integerValueStyle);

			Cell negativeBalanceCell = row2.createCell(cellSequence++);
			negativeBalanceCell.setCellValue(data.getNegativeBalance());
			negativeBalanceCell.setCellStyle(integerValueStyle);
		}

		for (int i = 0; i < 4; i++) {
			sheet.autoSizeColumn(i);
		}

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));

		File file = new File("Ledger report " + fromDate + " to " + toDate + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Ledger report " + fromDate + " to " + toDate + " | " + " Downloaded by - "
				+ adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis()));
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);
		logger.info("Ledger report email SENT to : " + adminDTO.getEmail());
	}

	private void createLedgerReportHeader(XSSFSheet sheet, int rowCount, LedgerReportRequest request,
			AdminDTO adminDTO) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));

		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue("Ledger report " + fromDate + " to " + toDate);
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		int cellSequenc = 0;
		Cell sNoHeader = row.createCell(cellSequenc);
		sNoHeader.setCellValue("Sr No.");
		sNoHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Date");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Positive Balance");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Negative Balance");
		towerHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
	}
}
