package com.maidin.adminApi.admin.impl;

import java.io.File;
import java.sql.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.adminApi.admin.TransactionHistoryMgmt;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.OrderPaymentDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.AdminTransactionsDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.pojo.transactions.CustomerDetails;
import com.maidin.pojo.transactions.GetCustomerTransactionResponse;
import com.maidin.pojo.transactions.TransactionDetails;

public class TransactionHistoryMgmtImpl implements TransactionHistoryMgmt {

	private final ProductDAO productDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final OrderPaymentDAO orderPaymentDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final CustomerTransactionDAO customerTransactionDAO;
	private final AddressDAO addressDAO;
	private final AdminDAO adminDAO;

	@Autowired
	public TransactionHistoryMgmtImpl(ProductDAO productDAO, AuthenticationMgmt authenticationMgmt,
			CustomerDAO customerDAO, OrderPaymentDAO orderPaymentDAO, OrderInfoDAO orderInfoDAO,
			OrderDetailsInfoDAO orderDetailsInfoDAO, CustomerTransactionDAO customerTransactionDAO,
			AddressDAO addressDAO, AdminDAO adminDAO) {
		this.productDAO = productDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.orderPaymentDAO = orderPaymentDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.customerTransactionDAO = customerTransactionDAO;
		this.addressDAO = addressDAO;
		this.adminDAO = adminDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(TransactionHistoryMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String adminTransactionsRecordsCount = prop
			.getProperty(ResourceProperties.ADMIN_TRANSACTIONS_RECORDS_COUNT);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetCustomerTransactionResponse getCustomerTransactionByPageNumber(String authToken, String mobileNumber,
			Integer currentPageNumber, Locale locale) throws Exception {
		
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ADMIN_DOES_NOT_EXIST, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), "mobile number : " + mobileNumber);

		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(mobileNumber);
		if (customerDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}

		GetCustomerTransactionResponse response = new GetCustomerTransactionResponse();

		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setFullName(customerDTO.getFullName());

		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		if (addressDTO == null) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.USER_NOT_REGISTERED_WITH_ADDRESS, locale));
		}
		customerDetails.setFullName(customerDTO.getFullName());
		customerDetails.setCity(addressDTO.getCity());
		customerDetails.setProject(addressDTO.getProject());
		customerDetails.setTower(addressDTO.getTower());
		customerDetails.setFlat(addressDTO.getFlat());

		response.setCustomerDetails(customerDetails);

		long totalRecordsCount = customerTransactionDAO.getCustomerTransactionCount(customerDTO.getId());
		response.setTotalRecordsCount(totalRecordsCount);

		int elementsPerPage = Integer.parseInt(adminTransactionsRecordsCount);
		int firstResult = (currentPageNumber - 1) * elementsPerPage;

		List<AdminTransactionsDTO> adminTransactionsDTOList = customerTransactionDAO
				.getCustomerTransactions(customerDTO.getId(), firstResult, elementsPerPage);

		for (int i = 0; i < adminTransactionsDTOList.size(); i++) {
			TransactionDetails transactionDetails = new TransactionDetails();
			transactionDetails.setAdminName(adminTransactionsDTOList.get(i).getAdminName());
			transactionDetails.setCustomerTransactionId(adminTransactionsDTOList.get(i).getCustomerTransactionId());
			transactionDetails.setMoneyIn(adminTransactionsDTOList.get(i).getMoneyIn());
			transactionDetails.setMoneyOut(adminTransactionsDTOList.get(i).getMoneyOut());
			transactionDetails.setDueAmount(adminTransactionsDTOList.get(i).getDueAmount());
			transactionDetails.setOrderStatus(adminTransactionsDTOList.get(i).getOrderStatus());
			transactionDetails.setPaymentMode(adminTransactionsDTOList.get(i).getPaymentMode());
			transactionDetails.setPrice(adminTransactionsDTOList.get(i).getPrice());
			transactionDetails.setProductName(adminTransactionsDTOList.get(i).getProductName());
			transactionDetails.setQuantity(adminTransactionsDTOList.get(i).getQuantity());
			transactionDetails.setQuantityUnit(adminTransactionsDTOList.get(i).getQuantityUnit());
			transactionDetails.setTotalCost(adminTransactionsDTOList.get(i).getTotalCost());
			transactionDetails.setTransactionDate(adminTransactionsDTOList.get(i).getTransactionDate());
			transactionDetails.setTransactionDescription(WalletDescription.fromId(adminTransactionsDTOList.get(i).getTransactionDescription()).getName());
			transactionDetails.setTransactionDescriptionId(adminTransactionsDTOList.get(i).getTransactionDescription());
			transactionDetails.setOrderedTime(adminTransactionsDTOList.get(i).getOrderedTime());
			transactionDetails.setTimeSlotId(adminTransactionsDTOList.get(i).getTimeSlotId());
			response.getTransactionDetailsList().add(transactionDetails);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

}
