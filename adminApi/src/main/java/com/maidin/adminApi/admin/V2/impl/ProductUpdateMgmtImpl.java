package com.maidin.adminApi.admin.V2.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.maidin.adminApi.admin.V2.BatchMgmt;
import com.maidin.adminApi.admin.V2.DashboardMgmt;
import com.maidin.adminApi.admin.V2.ProductUpdateMgmt;
import com.maidin.adminApi.admin.V2.SearchMgmt;
import com.maidin.adminApi.admin.impl.AdminMgmtImpl;
import com.maidin.adminApi.enums.ImageCategory;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.ScheduleStatus;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PhotoDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductDetailGroupDAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.ImageDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductDetailGroupDTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.admin.DeliveriesCountByDay;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;
import com.maidin.pojo.admin.SearchedProductDetails;
import com.maidin.pojo.adminv2.ChangeStatusRequest;
import com.maidin.pojo.adminv2.UpdateProductRequest;
import com.maidin.pojo.adminv2.UpdateProductResponse;
import com.maidin.pojo.search.ProductInfoV2;
import com.maidin.pojo.search.QuantityUnitInfo;

public class ProductUpdateMgmtImpl implements ProductUpdateMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final PhotoDAO photoDAO;
	private final ProductDetailGroupDAO productDetailGroupDAO;
	private final CategoryInfoV2DAO categoryInfoV2DAO;
	private final SubCategoryInfoV2DAO subCategoryInfoV2DAO;
	private final CityCategoryInfoDAO cityCategoryInfoDAO;

	@Autowired
	public ProductUpdateMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CustomerTransactionDAO transactionDAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ScheduleInfoDAO scheduleInfoDAO, TimeSlotDAO timeSlotDAO,
			BrandInfoV2DAO brandInfoV2DAO, PhotoDAO photoDAO, ProductDetailGroupDAO productDetailGroupDAO,
			CategoryInfoV2DAO categoryInfoV2DAO, SubCategoryInfoV2DAO subCategoryInfoV2DAO,
			CityCategoryInfoDAO cityCategoryInfoDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.scheduleInfoDAO = scheduleInfoDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.photoDAO = photoDAO;
		this.productDetailGroupDAO = productDetailGroupDAO;
		this.categoryInfoV2DAO = categoryInfoV2DAO;
		this.subCategoryInfoV2DAO = subCategoryInfoV2DAO;
		this.cityCategoryInfoDAO = cityCategoryInfoDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(ProductUpdateMgmtImpl.class);

	/*private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}*/

	//private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public UpdateProductResponse upsertProduct(String authToken, UpdateProductRequest request, Locale locale)
			throws Exception {

		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		UpdateProductResponse response = new UpdateProductResponse();
		Integer productId = 0;
		if (request.getProductId() < 1) {
			// add productInfo
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO.getByProductNameAndBrandId(request.getProductName(),
					request.getBrandId());
			if (productInfoV2DTO == null) {
				productId = addProduct(request, adminDTO.getId(), locale);
			} else {
				productId = productInfoV2DTO.getId();
			}
			if (productId != null) {
				// add different productCityInfo based on cities
				request.setProductId(productId);
				addProductDetails(request, adminDTO.getId(), locale);
			}
		} else {
			// productId > 0 so update ProductInfoV2
			ProductInfoV2DTO productDTO = productInfoV2DAO.getByIdWithoutActiveState(request.getProductId());
			if (productDTO == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCTID_NOT_EXIST, locale));
			}
			updateProduct(productDTO, adminDTO.getId(), request, adminDTO.getId(), locale);
			List<Long> productDetailIds = new ArrayList<>();
			ProductCityInfoV2DTO pcInfoV2DTO = productCityInfoV2DAO
					.getByIdWithoutActiveStatus(request.getProductDetailId());

			Long productDetailId = null;
			if (pcInfoV2DTO == null) {
				if (request.getProductDetailId() == 0 || request.getProductDetailId() == -1) {
					for (String cityCode : request.getCityCodeList()) {
						productDetailId = addProductCityInfo(request, cityCode, adminDTO.getId(), locale);
						productDetailIds.add(productDetailId);
						changeSetByDefault(productDetailId, request.getProductId(), cityCode, request.isSetByDefault());
					}
				} else {
					throw new ValidationException(ExceptionResourceBundle
							.getExceptionCodeProperties(ExceptionCode.PRODUCTDETAILID_NOT_EXIST, locale));
				}
			} else {
				if (request.getCityCodeList() == null) {
					request.getCityCodeList().add(pcInfoV2DTO.getCityCode());
				}
				int org_productId = pcInfoV2DTO.getProductId();
				String org_quantityUnit = pcInfoV2DTO.getQuantityUnit();
				int org_categoryId = pcInfoV2DTO.getCategoryId();
				int org_subCategoryId = pcInfoV2DTO.getSubCategoryId();

				for (String cityCode : request.getCityCodeList()) {
					ProductCityInfoV2DTO vCityInfoV2DTO = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(org_productId,
					org_quantityUnit, org_categoryId, org_subCategoryId, cityCode);
					if (vCityInfoV2DTO != null && (vCityInfoV2DTO.getId() != 0 || vCityInfoV2DTO.getId() != -1)) {
						updateProductCityInfo(vCityInfoV2DTO, request, adminDTO.getId(), locale);
						productDetailId = vCityInfoV2DTO.getId();
						productDetailIds.add(productDetailId);
						changeSetByDefault(productDetailId, request.getProductId(), cityCode,
								request.isSetByDefault());
					}
					 else {
						productDetailId = addProductCityInfo(request, cityCode, adminDTO.getId(), locale);
						productDetailIds.add(productDetailId);
						changeSetByDefault(productDetailId, request.getProductId(), cityCode, request.isSetByDefault());
					}
				}
			}

			Long productDetailGroupId = null;
			List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO
					.getByProductIdQuantityUnit(request.getProductId(), request.getQuantityUnit());
			productDetailGroupId = productCityList.stream().map(ProductCityInfoV2DTO::getProductDetailGroupId)
					.filter(Objects::nonNull).findAny().orElse(0L);
			ProductDetailGroupDTO productDetailGroupDTO = productDetailGroupDAO.getById(productDetailGroupId);
			productDetailIds = productDetailIds.stream().distinct().collect(Collectors.toList());
			if (productDetailGroupDTO == null) {
				productDetailGroupId = addProductDetailGroupInfo(productDetailIds, adminDTO.getId());
			} else {
				updateProductDetailGroupInfo(productDetailGroupDTO, productDetailIds, adminDTO.getId());
				productDetailGroupId = productDetailGroupDTO.getId();
			}
			if (productDetailIds.size() > 0) {
				productCityInfoV2DAO.updateProductDetailGroup(productDetailIds, productDetailGroupId);
				}

		}
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	public void updateProductCityInfo(ProductCityInfoV2DTO pcCityInfoV2DTO, UpdateProductRequest request, Long adminId,
			Locale locale) throws ValidationException {
		pcCityInfoV2DTO.setProductId(request.getProductId());
		// pcCityInfoV2DTO.setProductIdV1(request.getProductIdV1());
		if (request.getCategoryId() > 0) {
			pcCityInfoV2DTO.setCategoryId(request.getCategoryId());
		}
		if (request.getSubCategoryId() > 0) {
			pcCityInfoV2DTO.setSubCategoryId(request.getSubCategoryId());
		}
		if (request.getSalePrice() > request.getMrp()) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.SALEPRICE_SHOULD_NOT_BE_GREATERTHAN_MRP, locale));
		}
		pcCityInfoV2DTO.setMRP(request.getMrp());
		pcCityInfoV2DTO.setSalePrice(request.getSalePrice());
		pcCityInfoV2DTO.setDeliveryCharge(request.getDeliveryCharge());
		if (!Strings.isNullOrEmpty(request.getQuantityUnit())) {
			pcCityInfoV2DTO.setQuantityUnit(request.getQuantityUnit());
		}
		pcCityInfoV2DTO.setDiscount(0);
		if (!Strings.isNullOrEmpty(request.getDiscountUnit())) {
			pcCityInfoV2DTO.setDiscountUnit(request.getDiscountUnit());
		}
		/*
		 * if (!Strings.isNullOrEmpty(cityCode)) {
		 * pcCityInfoV2DTO.setCityCode(cityCode); }
		 */
		if (!Strings.isNullOrEmpty(request.getBarCode())) {
			pcCityInfoV2DTO.setBarCode(request.getBarCode());
		}
		pcCityInfoV2DTO.setSetByDefault(request.isSetByDefault());
		pcCityInfoV2DTO.setActive(request.isActive());
		pcCityInfoV2DTO.setStockStatus(true);
		pcCityInfoV2DTO.setStockQuantity(request.getStockQuantity());
		pcCityInfoV2DTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		pcCityInfoV2DTO.setUpdatedBy(adminId);
		productCityInfoV2DAO.updateProductInfo(pcCityInfoV2DTO);
		/*
		 * ProductCityInfoV2DTO pcCityInfoV2DTO2 = productCityInfoV2DAO
		 * .getByIdWithoutActiveStatus(pcCityInfoV2DTO.getId()); return
		 * pcCityInfoV2DTO2;
		 */
	}

	

	private ProductCityInfoV2DTO updateProductCityInfo(ProductCityInfoV2DTO pcCityInfoV2DTO,
			UpdateProductRequest request, Integer productId, String cityCode, long adminId, Locale locale)
			throws ValidationException {
		pcCityInfoV2DTO.setProductId(productId);
		// pcCityInfoV2DTO.setProductIdV1(request.getProductIdV1());
		if (request.getCategoryId() > 0) {
			pcCityInfoV2DTO.setCategoryId(request.getCategoryId());
		}
		if (request.getSubCategoryId() > 0) {
			pcCityInfoV2DTO.setSubCategoryId(request.getSubCategoryId());
		}
		if (request.getSalePrice() > request.getMrp()) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.SALEPRICE_SHOULD_NOT_BE_GREATERTHAN_MRP, locale));
		}
		pcCityInfoV2DTO.setMRP(request.getMrp());
		pcCityInfoV2DTO.setSalePrice(request.getSalePrice());
		pcCityInfoV2DTO.setDeliveryCharge(request.getDeliveryCharge());
		if (!Strings.isNullOrEmpty(request.getQuantityUnit())) {
			pcCityInfoV2DTO.setQuantityUnit(request.getQuantityUnit());
		}
		pcCityInfoV2DTO.setDiscount(0);
		if (!Strings.isNullOrEmpty(request.getDiscountUnit())) {
			pcCityInfoV2DTO.setDiscountUnit(request.getDiscountUnit());
		}
		if (!Strings.isNullOrEmpty(cityCode)) {
			pcCityInfoV2DTO.setCityCode(cityCode);
		}
		if (!Strings.isNullOrEmpty(request.getBarCode())) {
			pcCityInfoV2DTO.setBarCode(request.getBarCode());
		}
		pcCityInfoV2DTO.setActive(request.isActive());
		pcCityInfoV2DTO.setStockStatus(true);
		pcCityInfoV2DTO.setStockQuantity(request.getStockQuantity());
		pcCityInfoV2DTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		pcCityInfoV2DTO.setUpdatedBy(adminId);
		productCityInfoV2DAO.updateProductInfo(pcCityInfoV2DTO);
		ProductCityInfoV2DTO pcCityInfoV2DTO2 = productCityInfoV2DAO
				.getByIdWithoutActiveStatus(pcCityInfoV2DTO.getId());
		return pcCityInfoV2DTO2;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse changeStatus(String authToken, ChangeStatusRequest request, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}

		if (request.getCategoryId() > 0 && request.getSubCategoryId() <= 0) {
			updateCategory(request);
		} else if (request.getCategoryId() <= 0 && request.getSubCategoryId() > 0) {
			updateSubCategory(request);
		} else if (request.getProductDetailId() > 0 && request.getCategoryId() <= 0
				&& request.getSubCategoryId() <= 0) {
			updateProduct(request);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void updateProduct(ChangeStatusRequest request) {
		ProductCityInfoV2DTO pcCityInfoV2DTO = productCityInfoV2DAO
				.getByIdWithoutActiveStatus(request.getProductDetailId());
		List<String> cities = new ArrayList<>();
		if (request.isIsAll()) {
			cities = cityCategoryInfoDAO.getByAllCities(true);
		} else {
			cities = request.getCities();
		}
		if (pcCityInfoV2DTO != null && cities.size() > 0) {
			List<Long> productDetailIdList = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdAndCities(
					pcCityInfoV2DTO.getProductId(), pcCityInfoV2DTO.getQuantityUnit(), pcCityInfoV2DTO.getCategoryId(),pcCityInfoV2DTO.getSubCategoryId(),  cities);
			if (productDetailIdList.size() > 0) {
				productCityInfoV2DAO.updateStatus(request.isActive(), request.isStockStatus(), productDetailIdList);
				
			}
		}

	}

	private void updateCategory(ChangeStatusRequest request) {
		List<String> cities = new ArrayList<>();
		if (request.isIsAll()) {
			CategoryInfoV2DTO categoryDTO = categoryInfoV2DAO.getById(request.getCategoryId());
			if (categoryDTO != null) {
				categoryDTO.setActive(request.isActive());
				categoryInfoV2DAO.updateCategoryInfo(categoryDTO);
			}
			cities = cityCategoryInfoDAO.getByAllCities(true);
		} else {
			cities = request.getCities();

		}

		if (!request.isActive()) {

			List<Long> productDetailIdList = productCityInfoV2DAO.getIdsByCategoryIdAndCities(request.getCategoryId(),
					cities, true);
			if (productDetailIdList.size() > 0) {
				productCityInfoV2DAO.updateStatus(request.isActive(), request.isStockStatus(), productDetailIdList);
			}
		}

	}

	private void updateSubCategory(ChangeStatusRequest request) {
		List<String> cities = new ArrayList<>();
		if (request.isIsAll()) {
			SubCategoryInfoV2DTO subCategoryInfoV2DTO = subCategoryInfoV2DAO
					.getByIdWithoutActiveStatus(request.getSubCategoryId());
			if (subCategoryInfoV2DTO != null) {
				subCategoryInfoV2DTO.setActive(request.isActive());
				subCategoryInfoV2DTO.setStockStatus(request.isStockStatus());
				subCategoryInfoV2DAO.updateSubCategoryInfo(subCategoryInfoV2DTO);
			}
			cities = cityCategoryInfoDAO.getByAllCities(true);
		} else {
			cities = request.getCities();
		}
		if (!request.isActive()) {
			List<Long> productDetailIdList = productCityInfoV2DAO
					.getIdsBySubCategoryIdAndCities(request.getSubCategoryId(), cities, true);
			if (productDetailIdList.size() > 0) {
				productCityInfoV2DAO.updateStatus(request.isActive(), request.isStockStatus(), productDetailIdList);
			}
		}

	}

	private void updateProduct(ProductInfoV2DTO productDTO, Long id, UpdateProductRequest request, Long adminId,
			Locale locale) throws ValidationException {

		productDTO.setProductName(request.getProductName());
		// create brandId if needed
		if (request.getBrandId() == -1) {
			BrandInfoV2DTO bInfoV2DTO = brandInfoV2DAO.getByBrandName(request.getBrandName());
			if (bInfoV2DTO != null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.BRAND_ALREADY_EXIST, locale));
			} else {
				productDTO.setBrandId(addBrand(request.getBrandName(), adminId));
			}
		}
		// update BrandId if needed
		else if (request.getBrandId() > 0) {
			BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(request.getBrandId());
			// if given brandId not exists in db
			if (brandInfoV2DTO == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.BRANDID_DOES_NOT_EXIST, locale));
			} else {
				// if given brandId exists in db
				if (!brandInfoV2DTO.getBrand().equals(request.getBrandName())) {
					BrandInfoV2DTO bInfoV2DTO = brandInfoV2DAO.getByBrandName(request.getBrandName());
					if (bInfoV2DTO != null) {
						throw new ValidationException(ExceptionResourceBundle
								.getExceptionCodeProperties(ExceptionCode.BRANDID_AND_BRANDNAME_MISMATCH, locale));
					} else {
						updateBrand(brandInfoV2DTO, request.getBrandName(), adminId);
					}

				} else {
					if (!productDTO.getBrandId().equals(request.getBrandId())) {
						productDTO.setBrandId(request.getBrandId());
					}
				}

			}
		}
		if (request.getDescription() != null) {
			productDTO.setDescription(request.getDescription());
		}
		productDTO.setActive(true);
		productDTO.setStockStatus(request.isStockStatus());
		productDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		productDTO.setUpdatedBy(adminId);
		productInfoV2DAO.updateProductInfo(productDTO);

	}

	private void updateBrand(BrandInfoV2DTO brandInfoV2DTO, String brandName, long adminId) {
		brandInfoV2DTO.setBrand(brandName);
		brandInfoV2DTO.setUpdatedBy(adminId);
		brandInfoV2DTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		brandInfoV2DAO.updateBrandInfo(brandInfoV2DTO);

	}

	private void addProductDetails(UpdateProductRequest request, Long adminId, Locale locale)
			throws ValidationException {
		List<Long> productDetailIds = new ArrayList<>();
		for (String cityCode : request.getCityCodeList()) {
			long productDetailId = addProductCityInfo(request, cityCode, adminId, locale);
			productDetailIds.add(productDetailId);
			changeSetByDefault(productDetailId, request.getProductId(), cityCode, request.isSetByDefault());
			}
		if (request.getProductDetailGroupId() == -1 || request.getProductDetailGroupId() == 0) {
			Long productDetailGroupId = addProductDetailGroupInfo(productDetailIds, adminId);
			productCityInfoV2DAO.updateProductDetailGroup(productDetailIds, productDetailGroupId);
			
		} else {
			ProductDetailGroupDTO pDetailGroupDTO = productDetailGroupDAO.getById(request.getProductDetailGroupId());
			if (pDetailGroupDTO != null) {
				updateProductDetailGroupInfo(pDetailGroupDTO, productDetailIds, adminId);
			}
		}

		// return null;
	}

	private void changeSetByDefault(long productDetailId, int productId, String cityCode, boolean setByDefault) {
		List<Long> productDetailIdList = productCityInfoV2DAO.getByProductIdAndCityCode(productDetailId, productId,
				cityCode);
		ProductCityInfoV2DTO cityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailId);
		if (productDetailIdList.size() == 1) {
			cityInfoV2DTO.setSetByDefault(true);
		} else if (productDetailIdList.size() > 1) {
			if (setByDefault) {
				productDetailIdList.remove(cityInfoV2DTO.getId());
				productCityInfoV2DAO.updateSetByDefault(productDetailIdList, !setByDefault);
				cityInfoV2DTO.setSetByDefault(setByDefault);

			}
		}
	}

	private void updateProductDetailGroupInfo(ProductDetailGroupDTO pDetailGroupDTO, List<Long> productDetailIds,
			long adminId) {
		String productDetailIdsInString = pDetailGroupDTO.getProductDetailIds();
		String[] productDetailIdsStrArr = productDetailIdsInString.split(",");
		long[] productDetailIdsLongArr = Stream.of(productDetailIdsStrArr).mapToLong(Long::parseLong).toArray();
		List<Long> productDetailIdListInSet = Arrays.stream(productDetailIdsLongArr).boxed().distinct()
				.collect(Collectors.toList());
		List<Long> combinedProductDetailIds = ListUtils.union(productDetailIdListInSet, productDetailIds);
		combinedProductDetailIds = combinedProductDetailIds.stream().distinct().collect(Collectors.toList());
		pDetailGroupDTO.setProductDetailIds(StringUtils.join(combinedProductDetailIds, ","));
		pDetailGroupDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		pDetailGroupDTO.setUpdatedBy(adminId);
		productDetailGroupDAO.updateProductDetailGroupInfo(pDetailGroupDTO);

	}

	
	private Long addProductDetailGroupInfo(List<Long> productDetailIds, long adminId) {
		ProductDetailGroupDTO productDetailGroupDTO = new ProductDetailGroupDTO();
		productDetailGroupDTO.setProductDetailIds(StringUtils.join(productDetailIds, ','));
		productDetailGroupDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		productDetailGroupDTO.setCreatedBy(adminId);
		productDetailGroupDTO.setUpdatedBy(adminId);
		productDetailGroupDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return productDetailGroupDAO.addProductDetailGroupInfo(productDetailGroupDTO);

	}

	private Long addProductCityInfo(UpdateProductRequest request, String cityId, Long adminId, Locale locale)
			throws ValidationException {
		ProductCityInfoV2DTO productCityDTO = new ProductCityInfoV2DTO();
		productCityDTO.setProductId(request.getProductId());
		// productDTO.setProductIdV1(request.getProductIdV1());
		if (request.getCategoryId() > 0) {
			productCityDTO.setCategoryId(request.getCategoryId());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CATEGORY_ID_MISSING, locale));

		}
		if (request.getSubCategoryId() > 0) {
			productCityDTO.setSubCategoryId(request.getSubCategoryId());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.SUBCATEGORY_ID_MISSING, locale));

		}
		if (request.getSalePrice() > request.getMrp()) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.SALEPRICE_SHOULD_NOT_BE_GREATERTHAN_MRP, locale));
		}
		productCityDTO.setMRP(request.getMrp());
		productCityDTO.setSalePrice(request.getSalePrice());
		productCityDTO.setDeliveryCharge(request.getDeliveryCharge());
		if (!Strings.isNullOrEmpty(request.getQuantityUnit())) {
			productCityDTO.setQuantityUnit(request.getQuantityUnit());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.QUANTITYUNIT_MISSING, locale));

		}
		productCityDTO.setDiscount(0);
		if (!Strings.isNullOrEmpty(request.getDiscountUnit())) {
			productCityDTO.setDiscountUnit(
					!Strings.isNullOrEmpty(request.getDiscountUnit()) ? request.getDiscountUnit() : "%");
		}
		if (request.getProductDetailGroupId() > 0) {
			productCityDTO.setProductDetailGroupId(request.getProductDetailGroupId());
		}
		productCityDTO.setSetByDefault(request.isSetByDefault());
		if (!Strings.isNullOrEmpty(cityId)) {
			productCityDTO.setCityCode(cityId);
		} else {

			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CITYCODE_MISSING, locale));

		}
		if (!Strings.isNullOrEmpty(request.getBarCode())) {
			productCityDTO.setBarCode(request.getBarCode());
		}
		productCityDTO.setActive(request.isActive());
		productCityDTO.setStockStatus(true);
		productCityDTO.setStockQuantity(request.getStockQuantity());
		productCityDTO.setCreatedBy(adminId);
		productCityDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		productCityDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		productCityDTO.setUpdatedBy(adminId);
		return productCityInfoV2DAO.addProductInfo(productCityDTO);

	}


	private Integer addProduct(UpdateProductRequest request, Long adminId, Locale locale) {
		ProductInfoV2DTO productDTO = new ProductInfoV2DTO();
		productDTO.setProductName(request.getProductName());
		// if(request.getBrandId() < 1){
		BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(request.getBrandId());
		if (brandInfoV2DTO == null) {
			productDTO.setBrandId(addBrand(request.getBrandName(), adminId));
		} else {
			productDTO.setBrandId(request.getBrandId());
		}
		productDTO.setPriority(1);
		productDTO.setActive(true);
		if (request.getDescription() != null) {
			productDTO.setDescription(request.getDescription());
		}
		productDTO.setStockStatus(true);
		productDTO.setFoodType(request.getFoodType());
		productDTO.setCreatedBy(adminId);
		productDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		productDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		productDTO.setUpdatedBy(adminId);
		return productInfoV2DAO.addProductInfo(productDTO);

	}

	private Integer addBrand(String brandName, Long adminId) {
		BrandInfoV2DTO brandInfoV2DTO = new BrandInfoV2DTO();
		brandInfoV2DTO.setBrand(brandName);
		brandInfoV2DTO.setActive(true);
		brandInfoV2DTO.setStockStatus(true);
		brandInfoV2DTO.setCreatedBy(adminId);
		brandInfoV2DTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		brandInfoV2DTO.setUpdatedBy(adminId);
		brandInfoV2DTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return brandInfoV2DAO.addBrandInfo(brandInfoV2DTO);

	}

	
	private List<Long> upsertProductCity(long productDetailsId, String cityCode, UpdateProductRequest request,
			List<Long> productDetailIds, long adminId, Locale locale) throws ValidationException {
		ProductCityInfoV2DTO productCityInfoV2DTO = null;
		if (productDetailsId > 0) {
			productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailsId);
			if (!productCityInfoV2DTO.getCityCode().equals(cityCode)) {
				productCityInfoV2DTO = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(
						productCityInfoV2DTO.getProductId(), productCityInfoV2DTO.getQuantityUnit(),
						productCityInfoV2DTO.getCategoryId(), productCityInfoV2DTO.getSubCategoryId(), cityCode);
			}
		} else {
			productCityInfoV2DTO = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(
					request.getProductId(), request.getQuantityUnit(), request.getCategoryId(),
					request.getSubCategoryId(), cityCode);
		}
		if (productCityInfoV2DTO != null) {
			productCityInfoV2DTO = updateProductCityInfo(productCityInfoV2DTO, request, request.getProductId(),
					cityCode, adminId, locale);
		} else {
			productDetailsId = addProductCityInfo(request, cityCode, adminId, locale);
			productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailsId);
		}
		if (request.isSetByDefault()) {
			changeSetByDefault(productDetailsId, request.getProductId(), cityCode, !request.isSetByDefault());
			productCityInfoV2DTO.setSetByDefault(request.isSetByDefault());
		}
		productDetailIds.add(productDetailsId);

		return productDetailIds;
	}

	private List<Long> updateForAllCity(long productDetailsId, String cityCode, UpdateProductRequest request,
			List<Long> productDetailIds, long adminId, Locale locale) throws ValidationException {
		ProductCityInfoV2DTO productCityInfoV2DTO = null;
		if (productDetailsId > 0) {
			productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailsId);
			if (!productCityInfoV2DTO.getCityCode().equals(cityCode)) {
				productCityInfoV2DTO = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(
						productCityInfoV2DTO.getProductId(), productCityInfoV2DTO.getQuantityUnit(),
						productCityInfoV2DTO.getCategoryId(), productCityInfoV2DTO.getSubCategoryId(), cityCode);
			}
		} else {
			productCityInfoV2DTO = productCityInfoV2DAO.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(
					request.getProductId(), request.getQuantityUnit(), request.getCategoryId(),
					request.getSubCategoryId(), cityCode);
		}
		if (productCityInfoV2DTO != null) {
			productCityInfoV2DTO = updateProductCityInfo(productCityInfoV2DTO, request, request.getProductId(),
					cityCode, adminId, locale);
		} else {
			productDetailsId = addProductCityInfo(request, cityCode, adminId, locale);
			productCityInfoV2DTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(productDetailsId);
		}
		if (request.isSetByDefault()) {
			changeSetByDefault(productDetailsId, request.getProductId(), cityCode, !request.isSetByDefault());
			productCityInfoV2DTO.setSetByDefault(request.isSetByDefault());
		}
		productDetailIds.add(productDetailsId);

		return productDetailIds;
	}
}
