package com.maidin.adminApi.admin.impl;

import java.io.File;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.maidin.adminApi.admin.ManageCustomerOrderMgmt;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.ScheduleStatus;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.SmsSender;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CartDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.cart.ScheduleProductRequestV2;
import com.maidin.pojo.manageorders.CustomerDetails;
import com.maidin.pojo.manageorders.GetScheduleOrdersRequest;
import com.maidin.pojo.manageorders.GetScheduledProductsResponse;
import com.maidin.pojo.manageorders.ScheduledProduct;

public class ManageCustomerOrderMgmtImpl implements ManageCustomerOrderMgmt {

	private final ProductDAO productDAO;
	private final ProductBrandsDAO brandDAO;
	private final ProductCategoryDAO categoryDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final PaymentModeDAO paymentModeDAO;
	private final ScheduleInfoDAO scheduleDAO;
	private final WalletDAO walletDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final AddressDAO addressDAO;
	private final AdminDAO adminDAO;
	private final ProductCityInfoV2DAO productCityInfoDAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	
	@Autowired
	public ManageCustomerOrderMgmtImpl(ProductDAO productDAO, ProductBrandsDAO brandDAO, ProductCategoryDAO categoryDAO,
			AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO, TimeSlotDAO timeSlotDAO,
			OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO, PaymentModeDAO paymentModeDAO,
			ScheduleInfoDAO scheduleDAO, WalletDAO walletDAO, CustomerTransactionDAO transactionDAO,
			AddressDAO addressDAO, AdminDAO adminDAO, ProductCityInfoV2DAO productCityInfoDAO,
			BrandInfoV2DAO brandInfoV2DAO, ProductInfoV2DAO productInfoV2DAO) {

		this.productDAO = productDAO;
		this.brandDAO = brandDAO;
		this.categoryDAO = categoryDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.paymentModeDAO = paymentModeDAO;
		this.scheduleDAO = scheduleDAO;
		this.walletDAO = walletDAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.adminDAO = adminDAO;
		this.productCityInfoDAO = productCityInfoDAO;
		this.brandInfoV2DAO =brandInfoV2DAO;
		this.productInfoV2DAO =productInfoV2DAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(ManageCustomerOrderMgmtImpl.class);

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final Long newScheduleEndDate = Long
			.parseLong(prop.getProperty(ResourceProperties.NEW_SCHEDULE_END_DATE));

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetScheduledProductsResponse getScheduledOrders(GetScheduleOrdersRequest getScheduleOrdersRequest,
			String authToken, Locale locale) throws AuthenticationException, ValidationException {
		
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ADMIN_DOES_NOT_EXIST, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(getScheduleOrdersRequest));
		
		GetScheduledProductsResponse response = new GetScheduledProductsResponse();
		CustomerDTO customerDTO = customerDAO.getCustomerByMobileNumber(getScheduleOrdersRequest.getMobile());
		if (customerDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}
		CustomerDetails customerDetails = new CustomerDetails();
		customerDetails.setFullName(customerDTO.getFullName());

		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		if (addressDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}
		customerDetails.setFullName(customerDTO.getFullName());
		customerDetails.setCity(addressDTO.getCity());
		customerDetails.setProject(addressDTO.getProject());
		customerDetails.setTower(addressDTO.getTower());
		customerDetails.setFlat(addressDTO.getFlat());

		response.setCustomerDetails(customerDetails);

		List<ScheduleInfoDTO> scheduleDTOList = scheduleDAO.getByCustomerId(customerDTO.getId());
		for (ScheduleInfoDTO scheduleInfoDTO : scheduleDTOList) {
			ScheduledProduct scheduleProductInfo = getScheduleProductInfo(scheduleInfoDTO, locale);
			if (scheduleProductInfo != null) {
				response.getScheduledProductsList().add(scheduleProductInfo);
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse updateScheduledOrder(String authToken, ScheduleProductRequestV2 request, Locale locale)
			throws Exception {
		
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(request));

		GenericResponse response = new GenericResponse();

		ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(request.getScheduleId());
		if (scheduleDTO != null) {
			if (request.getScheduleStatus() == ScheduleStatus.REMOVE.getId()
					&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
				System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
				deleteSchedule(scheduleDTO);
				System.out.println("Delete start Time in Upsert: " + new Timestamp(System.currentTimeMillis()));
			} else if (request.getScheduleStatus() == ScheduleStatus.PAUSENEXT.getId()
					&& (request.getScheduleStatus() != scheduleDTO.getScheduleStatus())) {
				// do nothing
			} else {
				boolean addOrderRequired = false;
				updateSchedule(request, scheduleDTO);
				updateOrders(scheduleDTO, request, addOrderRequired, locale);
			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private void updateOrders(ScheduleInfoDTO scheduleDTO, ScheduleProductRequestV2 request, boolean addOrderRequired, Locale locale) throws Exception {
		boolean cancelled = false;
		ScheduleInfoDTO scheduleInfoDTO = scheduleDAO.getById(scheduleDTO.getId());
		Date scheduledFrom = null;
		if (scheduleInfoDTO.getScheduledUpto() == null) {
			scheduledFrom = scheduleInfoDTO.getStartDate();
		} else {
			scheduledFrom = scheduleDTO.getScheduledUpto();
		}
		addOrdersForSchedule("V2", scheduleDTO.getId(), scheduledFrom, locale);
		if (request.getScheduleStatus() == ScheduleStatus.PAUSE.getId()) {
			cancelled = true;
		}
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleDTO.getId());
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			if (getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
				orderDetailsInfo.setQuantity(request.getQuantity());
				if (cancelled) {
					orderDetailsInfo.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.NA.getName());
					orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				} else {
					orderDetailsInfo.setOrderStatus(OrderStatus.CONFIRMED.getName());
					orderDetailsInfo.setPaymentStatus(PaymentStatus.PENDING.getName());
					orderDetailsInfo.setPaymentModeId(PaymentMode.NA.getId());
				}
				orderDetailsInfo.setCancelled(cancelled);
				orderDetailsInfo.setOrderCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
				orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			}
			if (!cancelled) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
			}
			else{
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
			}



			orderInfoDTO.setCancelled(cancelled);
			orderInfoDTO.setCancellationTime(cancelled ? new Timestamp(System.currentTimeMillis()) : null);
			orderInfoDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
			orderInfoDTO.setTimeSlotId(timeSlotDTO.getId());
			orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
	}

	private void addOrdersForSchedule(String version, Long scheduleId, Date scheduledFrom, Locale locale) throws Exception {

		System.out.println("New Schedule");
		ScheduleInfoDTO scheduleDTO = scheduleDAO.getById(scheduleId);
		if (scheduleDTO != null) {
			Calendar cal = Calendar.getInstance();
			if (scheduleDTO.getScheduledUpto() != null) {
				cal.setTime(scheduledFrom);
			} else {
				cal.setTime(scheduleDTO.getStartDate());
			}
			if (getZeroTimeDate(new Date(cal.getTime().getTime()))
					.before(getZeroTimeDate(new Date(System.currentTimeMillis())))
					|| getZeroTimeDate(new Date(cal.getTime().getTime()))
							.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				cal.setTimeInMillis(System.currentTimeMillis());
				TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
				if (cal.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime())) {
					cal.add(Calendar.DATE, 1);
				}

			}

			Date monthStartDate = new Date(cal.getTimeInMillis());
			System.out.println(monthStartDate);
			long monthEndTime = 0;

			if (newScheduleEndDate == 0 || version == "V1") {
				Calendar calendar = Calendar.getInstance();
				calendar.add(Calendar.MONTH, 1);
				calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
				monthEndTime = calendar.getTimeInMillis();
			} else {
				monthEndTime = newScheduleEndDate;
			}
			Date monthEndDate = new Date(monthEndTime);
			System.out.println(monthStartDate);
			System.out.println(monthEndDate);
			if ((scheduleDTO.getScheduledUpto() != null && ((getZeroTimeDate(scheduleDTO.getScheduledUpto())
					.after(getZeroTimeDate(monthEndDate)))
					|| (getZeroTimeDate(scheduleDTO.getScheduledUpto()).equals(getZeroTimeDate(monthEndDate)))))) {
				return;
			}
			if (getZeroTimeDate(monthStartDate).after(getZeroTimeDate(monthEndDate))) {
				return;
			}
			scheduleDTO.setScheduledUpto(monthEndDate);
			List<Date> dateList = getDaysBetweenDates(monthStartDate, monthEndDate);
			for (Date deliveryDate : dateList) {

				Calendar c = toCalendar(deliveryDate);
				System.out.println(deliveryDate);
				String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
						"Saturday" };
				System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
				if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") && scheduleDTO.isSu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") && scheduleDTO.isMo()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") && scheduleDTO.isTu()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") && scheduleDTO.isWe()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") && scheduleDTO.isTh()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") && scheduleDTO.isFr()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") && scheduleDTO.isSa()) {
					addOrderToOrderInfo(scheduleDTO, deliveryDate, locale);
				}
			}

		}
	}

	private void addScheduledOrderDetailsInfo(long orderId, ScheduleInfoDTO scheduleInfoDTO, Locale locale) throws ValidationException {

		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId(scheduleInfoDTO.getProductId());
		orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
		ProductCityInfoV2DTO productCityInfoDTO = productCityInfoDAO.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoDTO != null) {
			ProductInfoV2DTO productInfoV2DTO = productInfoV2DAO.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
			if (productInfoV2DTO != null && (!productInfoV2DTO.isActive() || !productInfoV2DTO.isStockStatus() || !productCityInfoDTO.isActive() || 
					!productCityInfoDTO.isStockStatus())) {
				String productName = "";
				BrandInfoV2DTO brandInfoV2DTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
				if(brandInfoV2DTO != null && brandInfoV2DTO.getBrand() != null){
					productName = brandInfoV2DTO.getBrand();
				}
				if(productInfoV2DTO.getProductName() != null){
					if(brandInfoV2DTO != null){
						productName = productName.concat(" ");
					}
					productName = productName.concat(productInfoV2DTO.getProductName());
					if(productCityInfoDTO.getQuantityUnit() != null){
						productName = productName.concat(" ").concat(productCityInfoDTO.getQuantityUnit());
					}
				}
				throw new ValidationException(ExceptionResourceBundle
						.getDynamicExceptionCodeProperties(productName, ExceptionCode.PRODUCTDETAILS_NOT_AVAILABLE, locale));
			}
			BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId());
			if(brandInfoDTO!= null){
				orderDetailsDTO.setProductName(brandInfoDTO.getBrand()+ " "+productInfoV2DTO.getProductName());
			}
			//orderDetailsDTO.setProductName(productInfoV2DTO.getProductName());
			orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
			orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
			orderDetailsDTO.setProductSalePrice(productCityInfoDTO.getSalePrice());
			orderDetailsDTO
					.setTotalProductCost((productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
							* scheduleInfoDTO.getQuantity());
			orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() - productCityInfoDTO.getSalePrice());
			orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
		} else {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		
		////////////////////////////////////////////////////////////////////

		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDetailsDTO.setCancelled(false);
		orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);
		

	


	}

	private Long addScheduledOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {

		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(scheduleInfoDTO.getCustomerId());
		// orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(deliveryDate);
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		orderDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDTO.setScheduleId(scheduleInfoDTO.getId());
		orderDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDTO.setCancelled(false);
		CustomerDTO customerDTO = customerDAO.getById(scheduleInfoDTO.getCustomerId());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		orderDTO.setAddressId(addressDTO.getId());
		//orderDTO.setFlatId(addressDTO.getFlatId());
		/*orderDTO.setDeliveryLocation("Flat no. " + addressDTO.getFlat() + ", " + addressDTO.getTower() + ", "
				+ addressDTO.getProject() + ", " + addressDTO.getCity());*/
		// By Pushpinder
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	public static Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	private void addOrderToOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate, Locale locale) throws ValidationException {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleInfoDTO.getId(), deliveryDate);
		if (orderInfoDTO == null) {
			Long orderId = addScheduledOrderInfo(scheduleInfoDTO, deliveryDate);
			addScheduledOrderDetailsInfo(orderId, scheduleInfoDTO, locale);

		}
	}

	public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (getZeroTimeDate(new Date(calendar.getTime().getTime())).before(getZeroTimeDate(enddate))
				|| getZeroTimeDate(new Date(calendar.getTime().getTime())).equals(getZeroTimeDate(enddate))) {
			Date result = new Date(calendar.getTime().getTime());
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	private void updateSchedule(ScheduleProductRequestV2 request, ScheduleInfoDTO scheduleDTO) {
		scheduleDTO.setTimeSlotId(request.getTimeSlotId());
		scheduleDTO.setScheduleStatus(request.getScheduleStatus());
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setQuantity(request.getQuantity());
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		scheduleDAO.updateScheduleInfo(scheduleDTO);
	}

	private void pauseNextDelivery(ScheduleInfoDTO scheduleDTO) {
		scheduleDTO.setPauseDate(new Timestamp(System.currentTimeMillis()));
		Date pauseDate = getPauseDeliveryDate(scheduleDTO);

		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		if (pauseDate != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleDTO.getId(), pauseDate);
			if (orderInfoDTO != null) {
				orderInfoDTO.setCancelled(true);
				orderInfoDTO.setCancellationTime(new Timestamp(System.currentTimeMillis()));
				orderInfoDTO.setOverallOrderStatus(OrderStatus.CANCELLED.getId());
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO
						.getByOrderIdWithNoCancelStatus(orderInfoDTO.getId());
				for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
					orderDetailsDTO.setCancelled(true);
					orderDetailsDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
					orderDetailsDTO.setOrderStatus(OrderStatus.CANCELLED.getName());
					orderDetailsDTO.setPaymentStatus(PaymentStatus.NA.getName());
					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}
			}
		}
	}

	private void deleteSchedule(ScheduleInfoDTO scheduleDTO) {
		Long scheduleId = scheduleDTO.getId();
		scheduleDTO.setPauseDate(null);
		scheduleDTO.setActive(false);
		scheduleDTO.setScheduleStatus(scheduleDTO.getScheduleStatus());
		scheduleDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		scheduleDAO.updateScheduleInfo(scheduleDTO);
		Calendar cal = Calendar.getInstance();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getOrdersByScheduleId(scheduleId);
		for (OrderInfoDTO orderInfoDTO : orderInfoList) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			if (getZeroTimeDate(orderInfoDTO.getDeliveryDate())
					.equals(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
				if (scheduleDTO.getTimeSlotId() == 1) {
					continue;
				} else if ((scheduleDTO.getTimeSlotId() > 1 && cal
						.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
					continue;
				}
			}
			List<Long> orderDetailsInfoIdList = orderDetailsInfoDAO.getIdListByOrderId(orderInfoDTO.getId());
			if (orderDetailsInfoIdList != null && !orderDetailsInfoIdList.isEmpty()
					&& orderDetailsInfoIdList.size() != 0) {
				orderDetailsInfoIdList = wrapWithQuotes(orderDetailsInfoIdList);
				orderDetailsInfoDAO.deleteorderDetailsInfoList(orderDetailsInfoIdList);
			}

			orderInfoDAO.deleteOrderInfo(orderInfoDTO);
		}

	}

	private List<Long> wrapWithQuotes(List<Long> inputList) {
		List<Long> inputListWithQuote = Lists.newArrayList();
		for (Long input : inputList) {
			if (input != null) {
				inputListWithQuote.add(wrapWithQuotes(input));
			}
		}
		return inputListWithQuote;
	}

	private Long wrapWithQuotes(Long input) {
		return input;
	}

	private Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}

	/**
	 * @param scheduleInfoDTO
	 * @return
	 * @throws ValidationException 
	 */
	private ScheduledProduct getScheduleProductInfo(ScheduleInfoDTO scheduleInfoDTO, Locale locale) throws ValidationException {
		ScheduledProduct sInfoV2 = new ScheduledProduct();
		sInfoV2.setScheduleId(scheduleInfoDTO.getId());
		sInfoV2.setProductId(scheduleInfoDTO.getProductId());
		/*ProductDTO productDTO = productDAO.getById(scheduleInfoDTO.getProductId());
		if (productDTO == null) {
			return null;
		}
		sInfoV2.setProductName(productDTO.getProductName());
		sInfoV2.setPrice(productDTO.getPrice());*/
		ProductCityInfoV2DTO productCityInfoV2DTO = productCityInfoDAO
				.getByIdWithoutActiveStatus(scheduleInfoDTO.getProductId());
		if (productCityInfoV2DTO == null) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		ProductInfoV2DTO prInfoV2DTO = productInfoV2DAO
				.getByIdWithoutActiveState(productCityInfoV2DTO.getProductId());
		if (prInfoV2DTO == null) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.PRODUCT_NOT_AVAILABLE, locale));
		}
		sInfoV2.setProductId(productCityInfoV2DTO.getId().intValue());
		BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(prInfoV2DTO.getBrandId());
		if(brandInfoDTO!= null){
			sInfoV2.setProductName(brandInfoDTO.getBrand()+" "+prInfoV2DTO.getProductName());
		}
		else{
		sInfoV2.setProductName(prInfoV2DTO.getProductName());
		}
		sInfoV2.setPrice(productCityInfoV2DTO.getSalePrice()+productCityInfoV2DTO.getDeliveryCharge());
		sInfoV2.setQuantityUnit(productCityInfoV2DTO.getQuantityUnit());
		sInfoV2.setProductStatus(productCityInfoV2DTO.isActive()? 1 : 0);
		sInfoV2.setProductStatus(productCityInfoV2DTO.isActive()? (prInfoV2DTO.isActive()?1:0) : 0);
		sInfoV2.setQuantity(scheduleInfoDTO.getQuantity());
		sInfoV2.setRecurring(scheduleInfoDTO.isRecurring());
		if (scheduleInfoDTO.getPauseDate() != null) {
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
			Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
			if (pauseDate != null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(pauseDate);
				cal.set(Calendar.HOUR_OF_DAY, timeSlotDTO.getEndTime());
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);

				System.out.println("System Time" + new Timestamp(System.currentTimeMillis()));
				System.out.println("Pause Start Time" + new Timestamp(scheduleInfoDTO.getPauseDate().getTime()));
				System.out.println("Pause End Time" + new Timestamp(cal.getTimeInMillis()));
				System.out.println(
						"1st condition" + (System.currentTimeMillis() > scheduleInfoDTO.getPauseDate().getTime()));
				System.out.println("2nd condition" + (System.currentTimeMillis() < cal.getTimeInMillis()));
				if (((System.currentTimeMillis() + 1000) >= scheduleInfoDTO.getPauseDate().getTime())
						&& (System.currentTimeMillis() < cal.getTimeInMillis())) {
					sInfoV2.setScheduleStatus(ScheduleStatus.PAUSE.getId());
					System.out.println("---1---" + scheduleInfoDTO.getId());
				} else {
					sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
					System.out.println("---2---" + scheduleInfoDTO.getId());
				}
			} else {
				sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
				System.out.println("---3---" + scheduleInfoDTO.getId());
			}

		} else {
			sInfoV2.setScheduleStatus(scheduleInfoDTO.getScheduleStatus());
			System.out.println("---4---" + scheduleInfoDTO.getId());
		}

		sInfoV2.setStartDate(scheduleInfoDTO.getStartDate().getTime());
		sInfoV2.setTimeSlotId(scheduleInfoDTO.getTimeSlotId());
		if (scheduleInfoDTO.isSu())
			sInfoV2.getOrderDays().add(0);
		if (scheduleInfoDTO.isMo())
			sInfoV2.getOrderDays().add(1);
		if (scheduleInfoDTO.isTu())
			sInfoV2.getOrderDays().add(2);
		if (scheduleInfoDTO.isWe())
			sInfoV2.getOrderDays().add(3);
		if (scheduleInfoDTO.isTh())
			sInfoV2.getOrderDays().add(4);
		if (scheduleInfoDTO.isFr())
			sInfoV2.getOrderDays().add(5);
		if (scheduleInfoDTO.isSa())
			sInfoV2.getOrderDays().add(6);
		return sInfoV2;
	}

	private Date getPauseDeliveryDate(ScheduleInfoDTO scheduleDTO) {
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };

		long currentTimeStamp = System.currentTimeMillis();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTimeInMillis(currentTimeStamp);
		Calendar pauseTime = Calendar.getInstance();
		if (scheduleDTO.getPauseDate() != null) {
			pauseTime.setTimeInMillis(scheduleDTO.getPauseDate().getTime());
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			List<Long> timeStampList = new ArrayList<>();

			int startDay = 0;
			int endDay = 7;
			if ((pauseTime.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getStartTime()))) {
				startDay = 0;
				if(getZeroTimeDate(scheduleDTO.getStartDate())
						.after(getZeroTimeDate(new Date(scheduleDTO.getPauseDate().getTime())))){
					startDay = 1;
					endDay = 8;
				}

			} else {
				startDay = 1;
				endDay = 8;
			}

			for (; startDay < endDay; startDay++) {

				timeStampList.add(pauseTime.getTimeInMillis() + (startDay * 24 * 60 * 60 * 1000));

			}

			Calendar cal = Calendar.getInstance();
			for (Long timestamp : timeStampList) {
				cal.setTimeInMillis(timestamp);

				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") & scheduleDTO.isSu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") & scheduleDTO.isMo()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") & scheduleDTO.isTu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") & scheduleDTO.isWe()) {
					return new Date(timestamp);

				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") & scheduleDTO.isTh()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") & scheduleDTO.isFr()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") & scheduleDTO.isSa()) {
					return new Date(timestamp);
				}

			}
		}

		return null;
	}

}
