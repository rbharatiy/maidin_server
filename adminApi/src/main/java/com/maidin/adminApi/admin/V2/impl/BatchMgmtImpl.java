package com.maidin.adminApi.admin.V2.impl;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Strings;
import com.maidin.adminApi.admin.V2.BatchMgmt;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.RemarkStatus;
import com.maidin.adminApi.enums.ScheduleStatus;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.model.OrderPaymentInfo;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;

public class BatchMgmtImpl implements BatchMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;

	@Autowired
	public BatchMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CustomerTransactionDAO transactionDAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, ScheduleInfoDAO scheduleInfoDAO, TimeSlotDAO timeSlotDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.scheduleInfoDAO = scheduleInfoDAO;
		this.timeSlotDAO = timeSlotDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(BatchMgmtImpl.class);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addMonthlyOrderInSchedule() throws Exception {
		Properties properties = loadPropertyFile();
		Long batchStartDate = Long.parseLong(properties.getProperty(ResourceProperties.BATCH_START_DATE));
		Long batchEndDate = Long.parseLong(properties.getProperty(ResourceProperties.BATCH_END_DATE));
		Integer batchmonthDuration = Integer.parseInt(properties.getProperty(ResourceProperties.BATCH_MONTH_DURATION));

		System.out.println("I am entered in batch class");
		Calendar originalDate = Calendar.getInstance();
		System.out.println("The original Date is : " + originalDate.getTime());
		Calendar nextMonthDate = (Calendar) originalDate.clone();
		nextMonthDate.add(Calendar.MONTH, batchmonthDuration);
		System.out.println("The Next month date is: " + nextMonthDate.getTime());
		nextMonthDate.set(Calendar.DATE, nextMonthDate.getActualMinimum(Calendar.DAY_OF_MONTH));

		if (batchStartDate == 0 && batchEndDate == 0) {
			batchStartDate = nextMonthDate.getTimeInMillis();
			nextMonthDate.set(Calendar.DATE, nextMonthDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			batchEndDate = nextMonthDate.getTimeInMillis();
		}
		Date monthStartDate = new Date(batchStartDate);
		Date monthEndDate = new Date(batchEndDate);
		System.out.println(monthStartDate);
		System.out.println(monthEndDate);
		List<Date> dateList = getDaysBetweenDates(monthStartDate, monthEndDate);
		for (Date deliveryDate : dateList) {
			Calendar c = toCalendar(deliveryDate);
			System.out.println(deliveryDate);
			String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
					"Saturday" };
			System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
			List<ScheduleInfoDTO> scheduleInfoList = null;
			if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfSunday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfMonday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfTuesday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfWednesday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfThursday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfFriday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfSaturday(ScheduleStatus.RESUME.getId());
			}

			for (ScheduleInfoDTO scheduleInfoDTO : scheduleInfoList) {
				System.out.println("ScheduleId :" + scheduleInfoDTO.getId());
				if (scheduleInfoDTO.getPauseDate() != null) {
					Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
					if (pauseDate != null && !getZeroTimeDate(pauseDate).equals(getZeroTimeDate(deliveryDate))) {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					} else {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					}
					if (getZeroTimeDate(deliveryDate).after(getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()))) {
						scheduleInfoDTO.setScheduledUpto(deliveryDate);
					}

				} else if (scheduleInfoDTO.getScheduledUpto() == null) {
					addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					scheduleInfoDTO.setScheduledUpto(deliveryDate);
				} else {
					if (getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()).before(getZeroTimeDate(deliveryDate))) {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
						if (getZeroTimeDate(deliveryDate).after(getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()))) {
							scheduleInfoDTO.setScheduledUpto(deliveryDate);
						}
					}
				}

			}
		}

		System.out.println("Mothly Batch Completed");
	}

	private Properties loadPropertyFile() throws IOException {
		Properties properties = new Properties();
		try (InputStream inputStream = new FileInputStream(ResourceProperties.COMMON_FILE_LOCATION)) {
			properties.load(inputStream);
		}
		return properties;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse batchPriceDeductionAtSlotStart(List<Integer> timeSlotIdList) {
		GenericResponse response = new GenericResponse();
		System.out.println("Batch Started for price deduction for schedule/checkout at " + new Date(System
				.currentTimeMillis())) /*
										 * " for timeSlotId a:" + timeSlotIdList)
										 */;
		// imeSlotDTO timeSlotDTO = timeSlotDAO.getById(timeSlotId);
		// TimeSlotDTO timeSlotDTO =
		// timeSlotDAO.getByTimeSlot(timeSlot)(timeSlot)
		logger.info(ApiUtils.REQUEST_PAYLOAD,
				"Batch Started for price deduction for schedule and checkout " + java.time.LocalDateTime.now());
		// OrderPaymentInfo opi =
		// orderDetailsInfoDAO.getOrderPaymentInfo(orderId);
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getScheduleOrdersByTimeSlots(timeSlotIdList);
		System.out.println("Total ScheduleCheckoutCount" + orderInfoList.size());
		for (OrderInfoDTO orderInfo : orderInfoList) {
			System.out.println("Customer Name " + customerDAO.getById(orderInfo.getCustomerId()).getFullName()
					+ "OrderInfoId : " + orderInfo.getId() + " Schedule Id: " + orderInfo.getScheduleId());
			List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO
					.getUnSettledOrdersByOrderId(orderInfo.getId());

			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsList) {
				if (orderInfo.getScheduleId() != null) {
					ProductCityInfoV2DTO productCityInfoDTO = productCityInfoV2DAO
							.getByIdWithoutActiveStatus(orderDetailsInfo.getProductId());
					orderDetailsInfo.setProductMRP(productCityInfoDTO.getMRP());
					orderDetailsInfo.setProductSalePrice(productCityInfoDTO.getSalePrice());
					orderDetailsInfo.setTotalProductCost(
							(productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
									* orderDetailsInfo.getQuantity());
					orderDetailsInfo.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
					orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				} else if (orderDetailsInfo.getProductSalePrice() == 0) {
					ProductCityInfoV2DTO productCityInfoDTO = productCityInfoV2DAO
							.getByIdWithoutActiveStatus(orderDetailsInfo.getProductId());
					orderDetailsInfo.setProductMRP(productCityInfoDTO.getMRP());
					orderDetailsInfo.setProductSalePrice(productCityInfoDTO.getSalePrice());
					orderDetailsInfo.setTotalProductCost(
							(productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
									* orderDetailsInfo.getQuantity());
					orderDetailsInfo.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
					orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				} else if (orderDetailsInfo.getTotalProductCost() == 0) {
					ProductCityInfoV2DTO productCityInfoDTO = productCityInfoV2DAO
							.getByIdWithoutActiveStatus(orderDetailsInfo.getProductId());
					orderDetailsInfo.setDeliveryCharges(productCityInfoDTO.getDeliveryCharge());
					orderDetailsInfo.setTotalProductCost(
							(productCityInfoDTO.getSalePrice() + productCityInfoDTO.getDeliveryCharge())
									* orderDetailsInfo.getQuantity());
					orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
				}
				float amountToPay = orderDetailsInfo.getTotalProductCost() - orderDetailsInfo.getAmountPaidByCustomer();
				WalletDTO walletDTO = walletDAO.getByCustomerId(orderInfo.getCustomerId());
				if (walletDTO != null && walletDTO.getMyWalletBalance() > 0 && amountToPay > 0) {
					orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					/*
					 * orderDetailsInfo.setPayBy(","+PaymentMode.ByWallet.getId(
					 * )); orderInfo.setPayBy(String.valueOf(","+PaymentMode.
					 * ByWallet.getId()));
					 */
					if (!Strings.isNullOrEmpty(orderDetailsInfo.getPayBy())) {
						if (orderDetailsInfo.getPayBy().indexOf("," + PaymentMode.ByWallet.getId()) == -1) {
							orderDetailsInfo
									.setPayBy(orderDetailsInfo.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderDetailsInfo.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					if (!Strings.isNullOrEmpty(orderInfo.getPayBy())) {
						if (orderInfo.getPayBy().indexOf("," + PaymentMode.ByWallet.getId()) == -1) {
							orderInfo.setPayBy(orderInfo.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderInfo.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
					transactionRequest.setCustomerId(orderInfo.getCustomerId());
					transactionRequest.setOrderId(orderDetailsInfo.getOrderId());
					transactionRequest.setOrderDetailsId(orderDetailsInfo.getId());
					transactionRequest.setRemarkStatus(0);
					transactionRequest.setTransactionType(WalletDescription.BATCH_DEDUCTIONS.getId());
					if (walletDTO.getMyWalletBalance() >= orderDetailsInfo.getTotalProductCost()) {
						orderDetailsInfo.setAmountPaidByCustomer(orderDetailsInfo.getTotalProductCost());
						orderDetailsInfo.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						transactionRequest.setMoneyOut(orderDetailsInfo.getTotalProductCost());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(orderInfo.getCustomerId(), orderDetailsInfo.getTotalProductCost(),
								WalletDescription.DEDUCTIONS.getName());
						// orderDetailsInfo.setPaymentModeId(PaymentMode.ByWallet.getId());
						orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					} else if (walletDTO.getMyWalletBalance() < orderDetailsInfo.getTotalProductCost()) {
						orderDetailsInfo.setAmountPaidByCustomer(walletDTO.getMyWalletBalance());
						orderDetailsInfo.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						transactionRequest.setMoneyOut(walletDTO.getMyWalletBalance());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest
								.setDueAmount(orderDetailsInfo.getTotalProductCost() - walletDTO.getMyWalletBalance());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(orderInfo.getCustomerId(), walletDTO.getMyWalletBalance(),
								WalletDescription.DEDUCTIONS.getName());

						// orderDetailsInfo.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());

						orderDetailsInfoDAO.updateOrderDetails(orderDetailsInfo);
						orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						orderInfoDAO.updateOrderInfo(orderInfo);
					}
				}
			}

			setOrderPaymentInfo(orderInfo.getId());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		if (response.getStatus()) {
			List<TimeSlotDTO> timeSlotDTOList = timeSlotDAO.getTimeSlotListByIds(timeSlotIdList);
			for (TimeSlotDTO timeSlotDTO : timeSlotDTOList) {
				timeSlotDTO.setBatchupdateTime(new Timestamp(System.currentTimeMillis()));
			}
			// Batch time updated
		}

		logger.info(ApiUtils.REQUEST_PAYLOAD,
				"Batch Ended for price deduction for schedule and checkout " + java.time.LocalDateTime.now());
		return response;
	}

	private void addOrderToOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleInfoDTO.getId(), deliveryDate);
		if (orderInfoDTO == null) {
			Long orderId = addOrderInfo(scheduleInfoDTO, deliveryDate);
			addOrderDetailsInfo(orderId, scheduleInfoDTO);

		}

	}

	private void addOrderDetailsInfo(long orderId, ScheduleInfoDTO scheduleInfoDTO) {
		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId(scheduleInfoDTO.getProductId());
		orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
		/*
		 * ProductCityInfoV2DTO productCityInfoDTO =
		 * productCityInfoV2DAO.getById(scheduleInfoDTO.getProductId(), true);
		 * if (productCityInfoDTO != null) { ProductInfoV2DTO productInfoV2DTO =
		 * productInfoV2DAO.getById(productCityInfoDTO.getProductId(), true);
		 * orderDetailsDTO.setProductName(productInfoV2DTO.getProductName());
		 * orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
		 * orderDetailsDTO.setBarCode(productCityInfoDTO.getBarCode());
		 * orderDetailsDTO.setProductMRP(productCityInfoDTO.getMRP());
		 * orderDetailsDTO.setProductSalePrice(productCityInfoDTO.getSalePrice()
		 * ); orderDetailsDTO
		 * .setTotalProductCost((productCityInfoDTO.getSalePrice() +
		 * productCityInfoDTO.getDeliveryCharge())
		 * scheduleInfoDTO.getQuantity());
		 * orderDetailsDTO.setDiscountAmount(productCityInfoDTO.getMRP() -
		 * productCityInfoDTO.getSalePrice());
		 * orderDetailsDTO.setDeliveryCharges(productCityInfoDTO.
		 * getDeliveryCharge() ); }
		 */
		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDetailsDTO.setCancelled(false);
		// orderDetailsDTO.setPaymentModeId(PaymentMode.NA.getId());
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);

	}

	private Long addOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {
		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(scheduleInfoDTO.getCustomerId());
		orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(deliveryDate);
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		// orderDTO.setPaymentModeId(0);
		orderDTO.setScheduleId(scheduleInfoDTO.getId());
		orderDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDTO.setCancelled(false);
		orderDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		CustomerDTO customerDTO = customerDAO.getById(scheduleInfoDTO.getCustomerId());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		orderDTO.setAddressId(addressDTO.getId());
		// orderDTO.setFlatId(addressDTO.getFlatId());
		/*
		 * orderDTO.setDeliveryLocation("Flat no. " + addressDTO.getFlat() +
		 * ", " + addressDTO.getTower() + ", " + addressDTO.getProject() + ", "
		 * + addressDTO.getCity());
		 */
		orderDTO.setVersion("v2");
		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	private Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}

	boolean checkTimeSlotValidation(ScheduleInfoDTO scheduleDTO, boolean sameDay, Calendar todayCal,
			TimeSlotDTO timeSlotDTO, long timestamp) {

		if ((sameDay) && ((scheduleDTO.getTimeSlotId() == 1))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getEndTime()))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) > (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
			return false;
		}

		return true;

	}

	public static Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (getZeroTimeDate(new Date(calendar.getTime().getTime())).before(getZeroTimeDate(enddate))
				|| getZeroTimeDate(new Date(calendar.getTime().getTime())).equals(getZeroTimeDate(enddate))) {
			Date result = new Date(calendar.getTime().getTime());
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	private Date getPauseDeliveryDate(ScheduleInfoDTO scheduleDTO) {
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };

		long currentTimeStamp = System.currentTimeMillis();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTimeInMillis(currentTimeStamp);
		Calendar pauseTime = Calendar.getInstance();
		if (scheduleDTO.getPauseDate() != null) {
			pauseTime.setTimeInMillis(scheduleDTO.getPauseDate().getTime());
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			List<Long> timeStampList = new ArrayList<>();
			int startDay = 0;
			int endDay = 7;
			if ((pauseTime.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getStartTime()))) {
				startDay = 0;
				if (getZeroTimeDate(scheduleDTO.getStartDate())
						.after(getZeroTimeDate(new Date(scheduleDTO.getPauseDate().getTime())))) {
					startDay = 1;
					endDay = 8;
				}
			} else {
				startDay = 1;
				endDay = 8;
			}
			/*
			 * } else{ if((pauseTime.get(Calendar.HOUR) < (lastLockingPeriod))){
			 * startDay=1; endDay =8; }
			 * 
			 * else{ startDay=2; endDay =9; } }
			 */
			for (; startDay < endDay; startDay++) {
				timeStampList.add(pauseTime.getTimeInMillis() + (startDay * 24 * 60 * 60 * 1000));

			}
			Calendar cal = Calendar.getInstance();
			for (Long timestamp : timeStampList) {
				cal.setTimeInMillis(timestamp);
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") & scheduleDTO.isSu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") & scheduleDTO.isMo()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") & scheduleDTO.isTu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") & scheduleDTO.isWe()) {
					return new Date(timestamp);

				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") & scheduleDTO.isTh()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") & scheduleDTO.isFr()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") & scheduleDTO.isSa()) {
					return new Date(timestamp);
				}

			}
		}

		return null;
	}

	private void setOrderPaymentInfo(Long orderId) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		OrderPaymentInfo opi = orderDetailsInfoDAO.getOrderPaymentInfo(orderId);
		if (opi != null) {
			orderInfoDTO.setOrderSubTotal(opi.getSumTotal());
			if (opi.getDifference() == 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PAYMENTSETTLED.getId());
			} else if (opi.getDifference() == opi.getSumTotal()) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
			} else if (opi.getDifference() > 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PARTIALPAYMENT.getId());
			}
		}

		orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}
	/*
	 * private void setOrderPaymentInfo(Long orderId) { OrderInfoDTO
	 * orderInfoDTO = orderInfoDAO.getById(orderId); //
	 * List<OrderDetailsInfoDTO> orderDetailsList = //
	 * orderDetailsInfoDAO.getByOrderId(orderId); //List<Integer>
	 * paymentModeIdList =
	 * orderDetailsInfoDAO.getByOrderIdAndPaymentModeId(orderId);
	 * OrderPaymentInfo opi = orderDetailsInfoDAO.getOrderPaymentInfo(orderId);
	 * if(opi != null){ orderInfoDTO.setOrderSubTotal(opi.getSumTotal());
	 * if(opi.getDifference() == 0){
	 * orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PAYMENTSETTLED.getId()
	 * ); } else if(opi.getDifference() == opi.getSumTotal()){
	 * orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId()); }
	 * else if(opi.getDifference() < 0){
	 * orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PARTIALPAYMENT.getId()
	 * ); } } if (paymentModeIdList != null && paymentModeIdList.size() > 0) {
	 * // if(orderDetailsList.containsAll(0)) if
	 * (Collections.frequency(paymentModeIdList, paymentModeIdList.get(0)) ==
	 * paymentModeIdList.size()) {
	 * orderInfoDTO.setPaymentModeId(paymentModeIdList.get(0));
	 * 
	 * } else if (!Collections.disjoint(Arrays.asList(5, 6, 7, 8),
	 * paymentModeIdList)){
	 * orderInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
	 * 
	 * } else if (!Collections.disjoint(Arrays.asList(1,2,3,9,10),
	 * paymentModeIdList)) {
	 * orderInfoDTO.setPaymentModeId(PaymentMode.Multiple_Mode.getId()); } }
	 * orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis())); }
	 */

	private void addCustomerTransaction(CustomerTransactionRequest transactionRequest, long adminId) {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(transactionRequest.getCustomerId());
		transactionDTO.setOrderId(transactionRequest.getOrderId());
		transactionDTO.setOrderDetailsId(transactionRequest.getOrderDetailsId());
		transactionDTO.setMoneyIn(transactionRequest.getMoneyIn());
		transactionDTO.setMoneyOut(transactionRequest.getMoneyOut());
		transactionDTO.setPaymentModeId(transactionRequest.getPaymentModeId());
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(transactionRequest.getTransactionType());
		transactionDTO.setDueAmount(transactionRequest.getDueAmount());
		transactionDTO.setPreBalance(checkMaidinBalance(transactionRequest.getCustomerId()));
		transactionDTO.setRemarks(transactionRequest.getRemarkStatus());
		transactionDTO.setUpdatedBy(adminId);
		transactionDAO.addTransaction(transactionDTO);

	}

	private void updateWalletBalance(Long customerId, float totalProductCost, String rechargeType) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			walletDTO.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + totalProductCost);
			}
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.updateBalance(walletDTO);
		} else {
			WalletDTO walletDto = new WalletDTO();
			walletDto.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDto.setMyWalletBalance(-totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDto.setMyWalletBalance(totalProductCost);
			}
			walletDto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.addBalance(walletDto);
		}

	}

	private float checkMaidinBalance(Long customerId) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			return walletDTO.getMyWalletBalance();
		}
		return 0;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Integer> getTimeSlotIdListByStartTime(int hour) {
		List<Integer> timeSlotIdList = timeSlotDAO.getByStartTime(hour);
		return timeSlotIdList;
	}

}
