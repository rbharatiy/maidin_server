package com.maidin.adminApi.admin.V2;

import java.util.Locale;

import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.markdelivery.ChangeDeliveredOrderStateRequest;
import com.maidin.pojo.markdelivery.GetOrderRequest;
import com.maidin.pojo.markdelivery.GetOrderResponse;
import com.maidin.pojo.markdelivery.GetPendingAmount;
import com.maidin.pojo.markdelivery.MarkOrder;
import com.maidin.pojo.markdelivery.MarkOrderWithQtyUpdateRequest;

public interface MarkDeliveryMgmt {

	
	public GenericResponse markDeliveryApiV2(String authToken, MarkOrder request, Locale locale) throws Exception;

	public GenericResponse markOrderDeliveredWithQtyUpdateV2(String authToken, MarkOrderWithQtyUpdateRequest request, Locale locale) throws Exception;

	GenericResponse changeDeliveredOrderStateV2(String authToken, ChangeDeliveredOrderStateRequest request,
			Locale locale) throws Exception;

	public GetOrderResponse getDeliveryList(String authToken, GetOrderRequest request, Locale locale) throws Exception;

	public GetPendingAmount getPendingAmount(String authToken, Locale locale);

	public GenericResponse updatePhotoIdInOtherCities(String authToken, Locale locale);

	

	

}
