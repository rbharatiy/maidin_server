package com.maidin.adminApi.admin.V2.impl;

import java.io.File;
import java.io.FileReader;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.google.common.base.Strings;
import com.maidin.adminApi.admin.V2.MarkDeliveryMgmt;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.RemarkStatus;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
//import com.maidin.adminApi.utils.XlsxBuilder.StyleAttribute;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.persistence.model.OrderPaymentInfo;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.markdelivery.ChangeDeliveredOrderStateRequest;
import com.maidin.pojo.markdelivery.CustomerBalanceInfo;
import com.maidin.pojo.markdelivery.CustomerOrderDetails;
import com.maidin.pojo.markdelivery.CustomerTimeSlot;
import com.maidin.pojo.markdelivery.DeliveryDetails;
import com.maidin.pojo.markdelivery.GetOrderRequest;
import com.maidin.pojo.markdelivery.GetOrderResponse;
import com.maidin.pojo.markdelivery.GetPendingAmount;
import com.maidin.pojo.markdelivery.MarkOrder;
import com.maidin.pojo.markdelivery.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.markdelivery.OrderDetailsInfo;
import com.maidin.pojo.markdelivery.OrderInfoDetails;
import com.maidin.pojo.markdelivery.PaymentMethod;
import com.maidin.pojo.markdelivery.PaymentModeAmount;
import com.maidin.pojo.markdelivery.PaymentSettledModes;

public class MarkDeliveryMgmtImpl implements MarkDeliveryMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final BrandInfoV2DAO brandInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	private final org.json.simple.JSONArray timeSlotJsonArray;
	private org.json.simple.JSONArray cityProjectJsonArray;
	private org.json.simple.JSONArray projectJsonArray;
	private long jsonLoadedOn;

	@Autowired
	public MarkDeliveryMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CustomerTransactionDAO transactionDAO,
			ProductCityInfoV2DAO productCityInfoV2DAO, BrandInfoV2DAO brandInfoV2DAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productInfoV2DAO = productInfoV2DAO;
		this.addressDAO = addressDAO;
		this.transactionDAO = transactionDAO;
		this.productCityInfoV2DAO = productCityInfoV2DAO;
		this.brandInfoV2DAO = brandInfoV2DAO;
		this.timeSlotJsonArray = readTimeSlotJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.cityProjectJsonArray = readCityJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");
		this.projectJsonArray = readProjectJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");
		readPaymentModeJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.jsonLoadedOn = System.currentTimeMillis();
	}

	public static org.json.simple.JSONArray readTimeSlotJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray timeSlotArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			timeSlotArray = (org.json.simple.JSONArray) jsonObject.get("timeSlotList");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return timeSlotArray;
	}

	private org.json.simple.JSONArray readCityJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray cityArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			cityArray = (org.json.simple.JSONArray) jsonObject.get("cities");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cityArray;
	}

	private org.json.simple.JSONArray readProjectJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray projectArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			projectArray = (org.json.simple.JSONArray) jsonObject.get("projects");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return projectArray;
	}

	private org.json.simple.JSONArray readPaymentModeJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray paymentModeArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			paymentModeArray = (org.json.simple.JSONArray) jsonObject.get("paymentModeList");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return paymentModeArray;

	}

	private String paymentMode(org.json.simple.JSONArray paymentModeArray, int timeSlotId) {
		String paymentMode = null;
		for (int i = 0; i < paymentModeArray.size(); i++) {
			org.json.simple.JSONObject timeSlotJSON = (org.json.simple.JSONObject) paymentModeArray.get(i);
			if ((Long) timeSlotJSON.get("id") == (timeSlotId)) {
				return (String) timeSlotJSON.get("paymentMode");

			}
		}
		return paymentMode;

	}

	private org.json.simple.JSONObject readTimeSlotArray(org.json.simple.JSONArray timeSlotArray, int timeSlotId)
			throws JSONException {
		org.json.simple.JSONObject timeslotRequest = new org.json.simple.JSONObject();
		for (int i = 0; i < timeSlotArray.size(); i++) {
			org.json.simple.JSONObject timeSlotJSON = (org.json.simple.JSONObject) timeSlotArray.get(i);
			if ((Long) timeSlotJSON.get("id") == (timeSlotId)) {
				timeslotRequest.put("Id", (Long) timeSlotJSON.get("id"));
				timeslotRequest.put("timeSlot", (String) timeSlotJSON.get("timeSlot"));
				timeslotRequest.put("lockingPeriod", (Long) timeSlotJSON.get("lockingPeriod"));
				timeslotRequest.put("slotStartTime", (Long) timeSlotJSON.get("slotStartTime"));
				timeslotRequest.put("slotEndTime", (Long) timeSlotJSON.get("slotEndTime"));
				break;
			}

		}
		return timeslotRequest;
	}

	private String getCityName(long cityId) {
		for (int i = 0; i < cityProjectJsonArray.size(); i++) {
			org.json.simple.JSONObject cityJSON = (org.json.simple.JSONObject) cityProjectJsonArray.get(i);
			if ((Long) cityJSON.get("id") == (cityId)) {
				return (String) cityJSON.get("city");
			}
		}
		return null;
	}

	private String getTowerName(long projectId, long towerId) {
		for (int i = 0; i < projectJsonArray.size(); i++) {
			org.json.simple.JSONObject projectJSON = (org.json.simple.JSONObject) projectJsonArray.get(i);
			if ((Long) projectJSON.get("id") == (projectId)) {
				org.json.simple.JSONArray towerArray = (org.json.simple.JSONArray) projectJSON.get("towers");
				for (int j = 0; j < towerArray.size(); j++) {
					org.json.simple.JSONObject towerJSON = (org.json.simple.JSONObject) towerArray.get(j);
					if ((Long) towerJSON.get("id") == (towerId)) {
						return (String) towerJSON.get("towerName");
					}
				}
			}
		}
		return null;
	}

	private String getProjectName(long projectId) {
		for (int i = 0; i < projectJsonArray.size(); i++) {
			org.json.simple.JSONObject projectJSON = (org.json.simple.JSONObject) projectJsonArray.get(i);
			if ((Long) projectJSON.get("id") == (projectId)) {
				return (String) projectJSON.get("projectName");
			}
		}
		return null;
	}

	private void checkValidation(String cityName, String projectName, Locale locale) throws ValidationException {
		if (Strings.isNullOrEmpty(cityName) || Strings.isNullOrEmpty(projectName)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CITYPROJECT_JSON_ERROR, locale));
		}
	}

	private long jsonFileLastUpdatedOn(String filepath) {
		return new File(filepath).lastModified();
	}

	private void checkToweValidation(String towerName, Locale locale) throws ValidationException {

		if (Strings.isNullOrEmpty(towerName)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.TOWER_JSON_ERROR, locale));
		}
	}

	/*
	 * private DeliveryDetails setDeliveryDetails(OrderDetailsInfoDTO
	 * orderDetailsInfoDTO, OrderInfoDTO orderInfo, float restMoneyInWallet) {
	 * DeliveryDetails deliveryDetails = new DeliveryDetails(); //
	 * System.out.println("--------------"+orderInfo.getId() + //
	 * orderDetailsInfoDTO.getId()); ProductCityInfoV2DTO productCityInfoV2DTO =
	 * productCityInfoV2DAO
	 * .getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId()); if
	 * (productCityInfoV2DTO == null) { return null; } ProductInfoV2DTO
	 * productInfoV2DTO = productInfoV2DAO
	 * .getByIdWithoutActiveState(productCityInfoV2DTO.getProductId()); if
	 * (productInfoV2DTO == null) { return null; } BrandInfoV2DTO brandInfoDTO =
	 * brandInfoV2DAO.getByBrandId(productInfoV2DTO.getBrandId()); if
	 * (brandInfoDTO != null) {
	 * deliveryDetails.setProductName(brandInfoDTO.getBrand() + " " +
	 * productInfoV2DTO.getProductName()); } else {
	 * deliveryDetails.setProductName(productInfoV2DTO.getProductName()); }
	 * deliveryDetails.setOrderDetailsId(orderDetailsInfoDTO.getId());
	 * deliveryDetails.setQuantity(orderDetailsInfoDTO.getQuantity());
	 * deliveryDetails.setQuantityUnit(productCityInfoV2DTO.getQuantityUnit());
	 * CustomerDTO customerDTO = customerDAO.getById(orderInfo.getCustomerId());
	 * if (customerDTO == null) { return null; } PaymentMethod paymentMethod =
	 * new PaymentMethod();
	 * paymentMethod.setRestMoneyInWallet(restMoneyInWallet); if
	 * (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.DELIVERED.
	 * getName())) {
	 * deliveryDetails.setDeliveryStatus(OrderStatus.DELIVERED.getId());
	 * paymentMethod = setPaymentStatusIfDelivered(paymentMethod, orderInfo,
	 * orderDetailsInfoDTO);
	 * 
	 * } else if
	 * (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.UNDELIVERED.
	 * getName()) ||
	 * (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CANCELLED.
	 * getName()))) {
	 * deliveryDetails.setDeliveryStatus(OrderStatus.UNDELIVERED.getId());
	 * paymentMethod = null; } else if
	 * (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.
	 * getName())) {
	 * deliveryDetails.setDeliveryStatus(OrderStatus.DELIVERYSTATUSPENDING.getId
	 * ()); paymentMethod = setPaymentStatusIfPending(paymentMethod,
	 * customerDTO, productCityInfoV2DTO, orderDetailsInfoDTO,
	 * restMoneyInWallet); }
	 * 
	 * paymentMethod = setPaymentStatus(paymentMethod,orderDetailsInfoDTO
	 * ,productCityInfoV2DTO, restMoneyInWallet);
	 * 
	 * deliveryDetails.setPaymentMethod(paymentMethod);
	 * deliveryDetails.setPrice(orderDetailsInfoDTO.getProductSalePrice() > 0 ?
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * (orderDetailsInfoDTO.getDeliveryCharges())) :
	 * (productCityInfoV2DTO.getSalePrice() +
	 * productCityInfoV2DTO.getDeliveryCharge()));
	 * 
	 * return deliveryDetails;
	 * 
	 * }
	 */

	/*
	 * private PaymentMethod setPaymentStatus(PaymentMethod paymentMethod,
	 * OrderDetailsInfoDTO orderDetailsInfoDTO, ProductCityInfoV2DTO
	 * productCityInfoV2DTO, float restMoneyInWallet) { float walletCollect = 0;
	 * float cashCollect = 0; float paytmCollect = 0; float cardCollect = 0;
	 * List<CustomerTransactionDTO> transactionDTOList = transactionDAO
	 * .getByOrderDetailsId(orderDetailsInfoDTO.getId()); for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * paymentMethod.setCash( paymentMethod.getCash() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
	 * paymentMethod.setWallet( paymentMethod.getWallet() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
	 * paymentMethod.setPaytm( paymentMethod.getPaytm() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCard.getId()) {
	 * paymentMethod.setCard( paymentMethod.getCard() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else {
	 * paymentMethod.setOtherMode( paymentMethod.getOtherMode() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } } if
	 * (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.
	 * getName()) &&
	 * (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.
	 * PARTIALPAYMENT.getName())) ||
	 * (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PENDING.
	 * getName()))) { walletCollect =
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); if
	 * (orderDetailsInfoDTO.getProductSalePrice() == 0) {
	 * orderDetailsInfoDTO.setTotalProductCost(
	 * (productCityInfoV2DTO.getSalePrice() +
	 * productCityInfoV2DTO.getDeliveryCharge())
	 * orderDetailsInfoDTO.getQuantity()); } else {
	 * orderDetailsInfoDTO.setTotalProductCost(
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * orderDetailsInfoDTO.getDeliveryCharges())
	 * orderDetailsInfoDTO.getQuantity()); } cashCollect =
	 * orderDetailsInfoDTO.getTotalProductCost() -
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); if (restMoneyInWallet > 0)
	 * { if (restMoneyInWallet >= cashCollect) { walletCollect = walletCollect +
	 * cashCollect; restMoneyInWallet = restMoneyInWallet - cashCollect;
	 * cashCollect = 0; } else { walletCollect = walletCollect +
	 * restMoneyInWallet; cashCollect = cashCollect - restMoneyInWallet;
	 * restMoneyInWallet = 0; } } } return paymentMethod;
	 * 
	 * }
	 */
	/*
	 * private PaymentMethod setPaymentStatusIfDelivered(PaymentMethod
	 * paymentMethod, OrderInfoDTO orderInfo, OrderDetailsInfoDTO
	 * orderDetailsInfoDTO) {
	 * 
	 * 
	 * if (orderInfo.getPaymentModeId() == PaymentMode.ByCash.getId() ||
	 * orderInfo.getPaymentModeId() ==
	 * PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId() ) {
	 * paymentMethod.setCash(orderDetailsInfoDTO.getAmountPaidByCustomer()); }
	 * else if (orderInfo.getPaymentModeId() == PaymentMode.ByWallet.getId() ||
	 * orderInfo.getPaymentModeId() ==
	 * PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId()) {
	 * paymentMethod.setWallet(orderDetailsInfoDTO.getAmountPaidByCustomer() );
	 * 
	 * } else if (orderInfo.getPaymentModeId() ==
	 * PaymentMode.ByCashByWallet.getId() || orderInfo.getPaymentModeId() ==
	 * PaymentMode.PARTIAL_PAYMENT.getId()) { List<CustomerTransactionDTO>
	 * transactionDTOList = transactionDAO
	 * .getByOrderDetailsId(orderDetailsInfoDTO.getId()); for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * paymentMethod.setCash(paymentMethod.getCash() +
	 * transactionDTO.getMoneyOut());
	 * 
	 * } else if (transactionDTO.getPaymentModeId() ==
	 * PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId()) {
	 * paymentMethod.setCash(paymentMethod.getCash() +
	 * transactionDTO.getMoneyOut());
	 * 
	 * } else if (transactionDTO.getPaymentModeId() ==
	 * PaymentMode.ByWallet.getId()) {
	 * paymentMethod.setWallet(paymentMethod.getWallet() +
	 * transactionDTO.getMoneyOut());
	 * 
	 * } else if (transactionDTO.getPaymentModeId() ==
	 * PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId()) {
	 * paymentMethod.setWallet(paymentMethod.getWallet() +
	 * transactionDTO.getMoneyOut()); } } } else if
	 * (orderInfo.getPaymentModeId() != PaymentMode.PENDING.getId()){
	 * 
	 * List<CustomerTransactionDTO> transactionDTOList = transactionDAO
	 * .getAllTransactionsByOrderDetailsId(orderDetailsInfoDTO.getId()); for
	 * (CustomerTransactionDTO transactionDTO : transactionDTOList) { if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
	 * paymentMethod.setCash( paymentMethod.getCash() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
	 * paymentMethod.setWallet( paymentMethod.getWallet() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
	 * paymentMethod.setPaytm( paymentMethod.getPaytm() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.ByCard.getId()) {
	 * paymentMethod.setCard( paymentMethod.getCard() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else if
	 * (transactionDTO.getPaymentModeId() == PaymentMode.PhonePe.getId()) {
	 * paymentMethod.setCard( paymentMethod.getPhonePe() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } else {
	 * paymentMethod.setOtherMode( paymentMethod.getOtherMode() +
	 * (transactionDTO.getMoneyOut() - transactionDTO.getMoneyIn())); } }
	 * 
	 * } paymentMethod.setDueAmount( orderDetailsInfoDTO.getTotalProductCost() -
	 * orderDetailsInfoDTO.getAmountPaidByCustomer());
	 * 
	 * return paymentMethod; }
	 */

	/*
	 * private PaymentMethod setPaymentStatusIfPending(PaymentMethod
	 * paymentMethod, CustomerDTO customerDTO, ProductCityInfoV2DTO
	 * productCityInfoV2DTO, OrderDetailsInfoDTO orderDetailsInfoDTO, float
	 * restMoneyInWallet) { float walletCollect = 0; float cashCollect = 0;
	 * float paytmCollect = 0; float cardCollect = 0; if
	 * (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.
	 * PAYMENTSETTLED.getName())) { if (orderDetailsInfoDTO.getPaymentModeId()
	 * == PaymentMode.ByWallet.getId()) { walletCollect =
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); }
	 * 
	 * else if(orderDetailsInfoDTO.getPaymentModeId() ==
	 * PaymentMode.ByPaytm.getId()){ paytmCollect =
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); }
	 * 
	 * } else if ((orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.
	 * PARTIALPAYMENT.getName())) ||
	 * (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PENDING.
	 * getName()))) { walletCollect =
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); if
	 * (orderDetailsInfoDTO.getProductSalePrice() == 0) {
	 * orderDetailsInfoDTO.setTotalProductCost(
	 * (productCityInfoV2DTO.getSalePrice() +
	 * productCityInfoV2DTO.getDeliveryCharge())
	 * orderDetailsInfoDTO.getQuantity()); } else {
	 * orderDetailsInfoDTO.setTotalProductCost(
	 * (orderDetailsInfoDTO.getProductSalePrice() +
	 * orderDetailsInfoDTO.getDeliveryCharges())
	 * orderDetailsInfoDTO.getQuantity()); } cashCollect =
	 * orderDetailsInfoDTO.getTotalProductCost() -
	 * orderDetailsInfoDTO.getAmountPaidByCustomer(); if (restMoneyInWallet > 0)
	 * { if (restMoneyInWallet >= cashCollect) { walletCollect = walletCollect +
	 * cashCollect; restMoneyInWallet = restMoneyInWallet - cashCollect;
	 * cashCollect = 0; } else { walletCollect = walletCollect +
	 * restMoneyInWallet; cashCollect = cashCollect - restMoneyInWallet;
	 * restMoneyInWallet = 0; } } } paymentMethod.setPaytm(paytmCollect);
	 * paymentMethod.setCard(cardCollect);
	 * paymentMethod.setWallet(walletCollect);
	 * paymentMethod.setCash(cashCollect);
	 * paymentMethod.setRestMoneyInWallet(restMoneyInWallet);
	 * 
	 * return paymentMethod; }
	 */
	private static final Logger logger = LoggerFactory.getLogger(MarkDeliveryMgmtImpl.class);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse markDeliveryApiV2(String authToken, MarkOrder request, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);

		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		GenericResponse response = new GenericResponse();
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		float balancePaidAtDelivery = request.getAmount();
		int paymentMode = request.getPaymentMode();
		if (paymentMode == 0) {
			paymentMode = PaymentMode.ByCash.getId();
		}
		// add customer transaction entry if cash >0

		if (balancePaidAtDelivery > 0) {

			// recharge rest amount
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(paymentMode);
			transactionRequest.setTransactionType(WalletDescription.AMOUNT_RECEIVED_AT_DELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			if (paymentMode == PaymentMode.ByWallet
					.getId()) {/*
								 * updateWalletBalance(request.getCustomerId(),
								 * balancePaidAtDelivery,
								 * WalletDescription.RECHARGE.getName());
								 */
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.PAYMENTMODE_CANNOT_BE_WALLET, locale));
			}

			List<Long> orderIdList = new ArrayList<>();
			if (request.getDeliveredOrderInfoIdList().size() > 0) {
				orderIdList.addAll(request.getDeliveredOrderInfoIdList());
			}
			if (request.getUnDeliveredOrderDetailsInfoIdList().size() > 0) {
				List<Long> orderIdLists = orderDetailsInfoDAO.getByIds(request.getUnDeliveredOrderDetailsInfoIdList());
				orderIdList.addAll(orderIdLists);
			}
			if (orderIdList.size() > 0) {
				List<OrderInfoDTO> orderInfoList = orderInfoDAO.getByIds(orderIdList);
				orderInfoList.stream().forEach(elt -> {
					elt.setAmountReceivedAtDelivery(elt.getAmountReceivedAtDelivery() == 0 ? request.getAmount()
							: elt.getAmountReceivedAtDelivery());
				});
			}
			/*
			 * if (paymentMode == PaymentMode.ByWallet.getId()) { // true for no
			 * need to update wallet for transactions balancePaidAtDelivery =
			 * reshufflePayment(request.getCustomerId(), balancePaidAtDelivery,
			 * paymentMode, true); } else {
			 */
			balancePaidAtDelivery = reshufflePayment(request.getCustomerId(), balancePaidAtDelivery, paymentMode,
					false);
			// }
		}

		for (Long orderDetailsInfoId : request.getUnDeliveredOrderDetailsInfoIdList()) {
			markOrderUndeliveredV2(orderDetailsInfoId, adminDTO.getId(), request.getCustomerId());
		}

		for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				orderInfoDTO.setRemarks(request.getRemarks());
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();
				int undeliveryStatusCount = 0;
				for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
					// By Pushpinder 5/30/18
					if (request.getUnDeliveredOrderDetailsInfoIdList().contains(orderDetailsInfoDTO.getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTO.getId());
				}

				// setting overall orderStatus
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				// remove all undelivered orderDetailsId from orderInfo list
				if (orderDetailsInfoIdList.size() > 0) {
					orderDetailsInfoIdList.removeAll(request.getUnDeliveredOrderDetailsInfoIdList());
				}
				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(balancePaidAtDelivery);
				for (Long orderDetailsInfoId : orderDetailsInfoIdList) {
					// mark individual OrderDetails as delivered
					pModeAmount = markOrderDeliveredV2(orderDetailsInfoId, adminDTO.getId(), pModeAmount.getAmount(), 0,
							false, paymentMode, locale);
					paymentModeIdList.add(pModeAmount.getPaymentModeId());
				}

				// set overallPaymentModeId
				setOrderPaymentInfo(orderInfoId);
				balancePaidAtDelivery = pModeAmount.getAmount();
			}
		}

		// END - mark Order delivered by OrderDetailsId

		if (balancePaidAtDelivery > 0) {
			// recharge rest amount
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(paymentMode);
			transactionRequest.setTransactionType(WalletDescription.RECHARGE_BY_MARKDELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), balancePaidAtDelivery, WalletDescription.RECHARGE.getName());

		}
		float walletBalanceAfterMarkingDelivery = checkMaidinBalance(request.getCustomerId());
		if (walletBalanceAfterMarkingDelivery > 0) {
			reshufflePayment(request.getCustomerId(), walletBalanceAfterMarkingDelivery, PaymentMode.ByWallet.getId(),
					true);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private void setOrderPaymentInfo(Long orderId) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		List<OrderDetailsInfoDTO> orderDetaiList = orderDetailsInfoDAO.getByOrderId(orderId);
		// orderDetaiList.stream().map(OrderDetailsInfoDTO::getPayBy).
		String payBy = "";
		for (OrderDetailsInfoDTO dto : orderDetaiList) {
			if (!Strings.isNullOrEmpty(dto.getPayBy())) {
				if (!Strings.isNullOrEmpty(payBy)) {
					payBy = payBy.concat("," + dto.getPayBy());
				} else {
					payBy = "," + dto.getPayBy();
				}

			}
		}
		HashSet<String> test = new HashSet<String>(Arrays.asList(payBy.split(",")));
		orderInfoDTO.setPayBy(test.stream().collect(Collectors.joining(",")));

		OrderPaymentInfo opi = orderDetailsInfoDAO.getOrderPaymentInfo(orderId);
		if (opi != null) {
			orderInfoDTO.setOrderSubTotal(opi.getSumTotal());
			if (opi.getDifference() == 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PAYMENTSETTLED.getId());
			} else if (opi.getDifference() == opi.getSumTotal()) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
			} else if (opi.getDifference() > 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PARTIALPAYMENT.getId());
			}
		}
		orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}

	private float reshufflePayment(Long customerId, float balance, int paymentMode, boolean isWalletAlreadyUpdated) {
		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());
				float pendingAmountToPay = orderDetailsDTO.getTotalProductCost()
						- orderDetailsDTO.getAmountPaidByCustomer();
				if (balance > 0 && pendingAmountToPay > 0) {
					if (!Strings.isNullOrEmpty(orderDetailsDTO.getPayBy())) {
						if (orderDetailsDTO.getPayBy().indexOf("," + paymentMode) == -1) {
							orderDetailsDTO.setPayBy(orderDetailsDTO.getPayBy().concat("," + paymentMode));
						}
					} else {
						orderDetailsDTO.setPayBy("," + paymentMode);
					}

					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					if (balance >= pendingAmountToPay) {
						transactionRequest.setMoneyOut(pendingAmountToPay);
						orderDetailsDTO.setAmountPaidByCustomer(
								orderDetailsDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						/*
						 * <<<<<<< HEAD if(pendingAmountToPay ==
						 * orderDetailsDTO.getTotalProductCost()){
						 * orderDetailsDTO.setPaymentModeId(paymentMode); }
						 * else{ List<Integer> paymentModeIdList =
						 * transactionDAO.getPaymentModeByOrderDetailsId(
						 * orderDetailsDTO.getId()); if (paymentModeIdList !=
						 * null && paymentModeIdList.size() > 0) { if
						 * (Collections.frequency(paymentModeIdList,
						 * paymentModeIdList.get(0)) ==
						 * paymentModeIdList.size()) {
						 * orderDetailsDTO.setPaymentModeId(paymentModeIdList.
						 * get(0)); } else if
						 * (!Collections.disjoint(Arrays.asList(1, 2, 3),
						 * paymentModeIdList)) {
						 * orderDetailsDTO.setPaymentModeId(PaymentMode.
						 * ByCashByWallet.getId()); } } } =======
						 */
						/* >>>>>>> refs/heads/PaymentModeAddition */
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, -1);
						if (!isWalletAlreadyUpdated) {
							updateWalletBalance(customerId, pendingAmountToPay, WalletDescription.RECHARGE.getName());
						}
						balance = balance - pendingAmountToPay;
					} else {
						transactionRequest.setMoneyOut(balance);
						orderDetailsDTO.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + balance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(pendingAmountToPay - balance);
						addCustomerTransaction(transactionRequest, -1);
						if (!isWalletAlreadyUpdated) {
							updateWalletBalance(customerId, balance, WalletDescription.RECHARGE.getName());
						}
						balance = 0;
						// orderDetailsDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());

					}
				}
				// TODO:change SetPaymentModeid
				setPaymentModeId(orderDetailsDTO.getOrderId(), paymentMode);
			}
			transactionRequest = null;
		}
		return balance;
	}

	private void addCustomerTransaction(CustomerTransactionRequest transactionRequest, long adminId) {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(transactionRequest.getCustomerId());
		transactionDTO.setOrderId(transactionRequest.getOrderId());
		transactionDTO.setOrderDetailsId(transactionRequest.getOrderDetailsId());
		transactionDTO.setMoneyIn(transactionRequest.getMoneyIn());
		transactionDTO.setMoneyOut(transactionRequest.getMoneyOut());
		transactionDTO.setPaymentModeId(transactionRequest.getPaymentModeId());
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(transactionRequest.getTransactionType());
		transactionDTO.setDueAmount(transactionRequest.getDueAmount());
		transactionDTO.setPreBalance(checkMaidinBalance(transactionRequest.getCustomerId()));
		transactionDTO.setRemarks(transactionRequest.getRemarkStatus());
		transactionDTO.setUpdatedBy(adminId);
		transactionDAO.addTransaction(transactionDTO);

	}

	private float checkMaidinBalance(Long customerId) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			// if (walletDTO.getMyWalletBalance() != null) {
			return walletDTO.getMyWalletBalance();
			// }
			// return 0;
		}
		return 0;
	}

	private void setPaymentModeId(Long orderId, int paymentMode) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
			if (orderInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
				orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + paymentMode));
			}
		} else {
			orderInfoDTO.setPayBy("," + paymentMode);
		}
		orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}

	private PaymentModeAmount markOrderDeliveredV2(Long orderDetailsInfoId, Long adminId, float amount,
			int requestedQuantity, boolean isQuantityChanged, int paymentMode, Locale locale) throws Exception {
		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();
		OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (orderDetailsInfoDTO != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
			if (isQuantityChanged) {
				orderDetailsInfoDTO.setQuantity(requestedQuantity);
			}
			if (orderDetailsInfoDTO.getProductSalePrice() == 0) {
				ProductCityInfoV2DTO productCityInfoDTO = productCityInfoV2DAO
						.getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId());
				orderDetailsInfoDTO.setProductMRP(orderDetailsInfoDTO.getProductMRP() > 0
						? orderDetailsInfoDTO.getProductMRP() : productCityInfoDTO.getMRP());
				orderDetailsInfoDTO.setProductSalePrice(orderDetailsInfoDTO.getProductSalePrice() > 0
						? orderDetailsInfoDTO.getProductSalePrice() : productCityInfoDTO.getSalePrice());
				orderDetailsInfoDTO.setDeliveryCharges(orderDetailsInfoDTO.getDeliveryCharges() > 0
						? orderDetailsInfoDTO.getDeliveryCharges() : productCityInfoDTO.getDeliveryCharge());
				ProductInfoV2DTO pInfoV2DTO = productInfoV2DAO
						.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
				BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(pInfoV2DTO.getBrandId());
				if (brandInfoDTO != null) {
					orderDetailsInfoDTO.setProductName(brandInfoDTO.getBrand() + " " + pInfoV2DTO.getProductName());
				}
			}
			orderDetailsInfoDTO.setTotalProductCost(
					(orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges())
							* orderDetailsInfoDTO.getQuantity());

			// check maidin balance
			float customerMaidinBalance = checkMaidinBalance(orderInfoDTO.getCustomerId());

			// create common customerTransaction object
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(orderInfoDTO.getCustomerId());
			transactionRequest.setOrderId(orderInfoDTO.getId());
			transactionRequest.setOrderDetailsId(orderDetailsInfoId);
			transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
			transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());

			// find out - how much pending amount to pay at time of delivery
			float pendingAmountToPay = orderDetailsInfoDTO.getTotalProductCost()
					- orderDetailsInfoDTO.getAmountPaidByCustomer();
			if (pendingAmountToPay > 0) {
				if (customerMaidinBalance >= pendingAmountToPay) {
					// case 1: Customer has sufficient maidin balance
					orderDetailsInfoDTO.setAmountPaidByCustomer(
							orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
					if (!Strings.isNullOrEmpty(orderDetailsInfoDTO.getPayBy())) {
						if (orderDetailsInfoDTO.getPayBy().indexOf(PaymentMode.ByWallet.getId()) == -1) {
							orderDetailsInfoDTO.setPayBy(
									orderDetailsInfoDTO.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderDetailsInfoDTO.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
						if (orderInfoDTO.getPayBy().indexOf(PaymentMode.ByWallet.getId()) == -1) {
							orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderInfoDTO.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
					transactionRequest.setMoneyOut(pendingAmountToPay);
					transactionRequest.setDueAmount(0);
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay,
							WalletDescription.DEDUCTIONS.getName());
				} else if (customerMaidinBalance > 0 && customerMaidinBalance < pendingAmountToPay) {
					// case 2: Customer hasn't sufficient maidin balance

					transactionRequest.setMoneyOut(customerMaidinBalance);
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setDueAmount(pendingAmountToPay - customerMaidinBalance);
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), customerMaidinBalance,
							WalletDescription.DEDUCTIONS.getName());
					if (!Strings.isNullOrEmpty(orderDetailsInfoDTO.getPayBy())) {
						if (orderDetailsInfoDTO.getPayBy().indexOf(PaymentMode.ByWallet.getId()) == -1) {
							orderDetailsInfoDTO.setPayBy(
									orderDetailsInfoDTO.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderDetailsInfoDTO.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
						if (orderInfoDTO.getPayBy().indexOf(PaymentMode.ByWallet.getId()) == -1) {
							orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + PaymentMode.ByWallet.getId()));
						}
					} else {
						orderInfoDTO.setPayBy("," + PaymentMode.ByWallet.getId());
					}
					orderDetailsInfoDTO.setAmountPaidByCustomer(
							orderDetailsInfoDTO.getAmountPaidByCustomer() + customerMaidinBalance);
					pendingAmountToPay = pendingAmountToPay - customerMaidinBalance;
					if (amount >= pendingAmountToPay) {
						transactionRequest.setMoneyOut(pendingAmountToPay);
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, adminId);
						if (!Strings.isNullOrEmpty(orderDetailsInfoDTO.getPayBy())) {
							if (orderDetailsInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
								orderDetailsInfoDTO.setPayBy(orderDetailsInfoDTO.getPayBy().concat("," + paymentMode));
							}
						} else {
							orderDetailsInfoDTO.setPayBy("," + paymentMode);
						}
						if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
							if (orderInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
								orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + paymentMode));
							}
						} else {
							orderInfoDTO.setPayBy("," + paymentMode);
						}
						orderDetailsInfoDTO.setAmountPaidByCustomer(
								orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						amount = amount - pendingAmountToPay;
					} else {
						if (amount > 0) {
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setPaymentModeId(paymentMode);
							transactionRequest.setDueAmount(pendingAmountToPay - amount);
							addCustomerTransaction(transactionRequest, adminId);
							if (!Strings.isNullOrEmpty(orderDetailsInfoDTO.getPayBy())) {
								if (orderDetailsInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
									orderDetailsInfoDTO
											.setPayBy(orderDetailsInfoDTO.getPayBy().concat("," + paymentMode));
								}
							} else {
								orderDetailsInfoDTO.setPayBy("," + paymentMode);
							}
							if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
								if (orderInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
									orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + paymentMode));
								}
							} else {
								orderInfoDTO.setPayBy("," + paymentMode);
							}
							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							// detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
							amount = 0;
						} else if (amount <= 0) {
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setDueAmount(pendingAmountToPay);
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							// deduct his due amount from wallet
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							// detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
							amount = 0;
						}
						amount = 0;
					}
				} else {
					// no maidin balance
					if (amount > 0) {
						if (!Strings.isNullOrEmpty(orderDetailsInfoDTO.getPayBy())) {
							if (orderDetailsInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
								orderDetailsInfoDTO.setPayBy(orderDetailsInfoDTO.getPayBy().concat("," + paymentMode));
							}
						} else {
							orderDetailsInfoDTO.setPayBy("," + paymentMode);
						}
						if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
							if (orderInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
								orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + paymentMode));
							}
						} else {
							orderInfoDTO.setPayBy("," + paymentMode);
						}
						if (amount >= pendingAmountToPay) {
							// case 3. amount >= pendingAmount of order
							transactionRequest.setMoneyOut(pendingAmountToPay);
							transactionRequest.setDueAmount(0);
							transactionRequest.setPaymentModeId(paymentMode);
							addCustomerTransaction(transactionRequest, adminId);

							orderDetailsInfoDTO.setAmountPaidByCustomer(
									orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
							amount = amount - pendingAmountToPay;
						} else {
							// case 4. amount < pendingAmount of order
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setDueAmount(pendingAmountToPay - amount);
							transactionRequest.setPaymentModeId(paymentMode);
							addCustomerTransaction(transactionRequest, adminId);

							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							// detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId());
							amount = 0;
						}
					} else if (amount <= 0) {
						// due amount deducted from wallet
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmountToPay);
						transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay,
								WalletDescription.DEDUCTIONS.getName());
						if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							// detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());

						} else {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
							// detailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
						}
						amount = 0;
					}
				}
			} else if (pendingAmountToPay < 0) {

				// pendingAmount negative: re-charge wallet with refunded amount
				pendingAmountToPay = pendingAmountToPay * -1;
				transactionRequest.setMoneyIn(pendingAmountToPay);
				orderDetailsInfoDTO.setAmountPaidByCustomer(orderDetailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setTransactionType(WalletDescription.REFUND_ON_QUANTITY_DECREASE.getId());
				addCustomerTransaction(transactionRequest, adminId);
				updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmountToPay,
						WalletDescription.RECHARGE.getName());
				// orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
				// orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());

			}
			// }
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			// set the rest amount in paymentModeAmountTuple
			paymentModeAmountTuple.setAmount(amount);

		}
		return paymentModeAmountTuple;

	}

	private void updateWalletBalance(Long customerId, float totalProductCost, String rechargeType) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			walletDTO.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + totalProductCost);
			}
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.updateBalance(walletDTO);
		} else {
			WalletDTO walletDto = new WalletDTO();
			walletDto.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDto.setMyWalletBalance(-totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDto.setMyWalletBalance(totalProductCost);
			}
			walletDto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.addBalance(walletDto);
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse markOrderDeliveredWithQtyUpdateV2(String authToken, MarkOrderWithQtyUpdateRequest request,
			Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		}
		GenericResponse response = new GenericResponse();
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		float balancePaidAtDelivery = request.getAmount();
		int paymentModeSelected = request.getPaymentMode();
		if (request.getPaymentMode() == 0) {
			paymentModeSelected = PaymentMode.ByCash.getId();
		}

		// add customer transaction entry if cash >0
		// float walletBalanceBeforeMarkingDelivery =
		// checkMaidinBalance(request.getCustomerId());

		if (balancePaidAtDelivery > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(paymentModeSelected);
			transactionRequest.setTransactionType(WalletDescription.AMOUNT_RECEIVED_AT_DELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			if (paymentModeSelected == PaymentMode.ByWallet.getId()) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.PAYMENTMODE_CANNOT_BE_WALLET, locale));
				/*
				 * updateWalletBalance(request.getCustomerId(),
				 * balancePaidAtDelivery, WalletDescription.RECHARGE.getName());
				 */
			}
			transactionRequest = null;

			List<Long> orderIdList = new ArrayList<>();
			if (request.getDeliveredOrderInfoIdList().size() > 0) {
				orderIdList.addAll(request.getDeliveredOrderInfoIdList());
			}
			if (request.getUnDeliveredOrderDetailsInfoIdList().size() > 0) {
				List<Long> orderIdLists = orderDetailsInfoDAO.getByIds(request.getUnDeliveredOrderDetailsInfoIdList());
				orderIdList.addAll(orderIdLists);
			}
			if (orderIdList.size() > 0) {
				List<OrderInfoDTO> orderInfoList = orderInfoDAO.getByIds(orderIdList);
				orderInfoList.stream().forEach(elt -> {
					elt.setAmountReceivedAtDelivery(elt.getAmountReceivedAtDelivery() == 0 ? request.getAmount()
							: elt.getAmountReceivedAtDelivery());
				});
			}

			/*
			 * if (paymentModeSelected == PaymentMode.ByWallet.getId()) { //
			 * true for no need to update wallet for transactions
			 * balancePaidAtDelivery = reshufflePayment(request.getCustomerId(),
			 * balancePaidAtDelivery, paymentModeSelected, true); } else {
			 */
			balancePaidAtDelivery = reshufflePayment(request.getCustomerId(), balancePaidAtDelivery,
					paymentModeSelected, false);
			// }
		}

		// mark undelivered orders
		for (Long orderDetailsInfoId : request.getUnDeliveredOrderDetailsInfoIdList()) {
			markOrderUndeliveredV2(orderDetailsInfoId, adminDTO.getId(), request.getCustomerId());
		}

		// mark delivered orders
		for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				orderInfoDTO.setRemarks(request.getRemarks());
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();

				/* check order status - START */
				int undeliveryStatusCount = 0;

				// prepare all delivered orderDetailInfo Ids list
				for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
					if (request.getUnDeliveredOrderDetailsInfoIdList().contains(orderDetailsInfoDTO.getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTO.getId());
				}
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				/* check order status - END */

				// set updated time
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				// remove un-delivered orderDetailInfo ids
				if (orderDetailsInfoIdList.size() > 0) {
					orderDetailsInfoIdList.removeAll(request.getUnDeliveredOrderDetailsInfoIdList());
				}

				// update payment mode and amount paid
				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(balancePaidAtDelivery);
				for (int i = 0; i < orderDetailsInfoIdList.size(); i++) {
					if (request.getUpdatedOrderDetailInfoIdList().contains(orderDetailsInfoIdList.get(i))) {
						int index = request.getUpdatedOrderDetailInfoIdList().indexOf(orderDetailsInfoIdList.get(i));
						// with quantity update
						/*
						 * pModeAmount =
						 * markDeliveredWithQtyUpdateV2(orderDetailsInfoIdList.
						 * get(i), adminDTO.getId(), pModeAmount.getAmount(),
						 * request.getUpdatedOrderDetailQuantityList().get(index
						 * ), paymentModeSelected, locale);
						 */
						pModeAmount = markOrderDeliveredV2(orderDetailsInfoIdList.get(i), adminDTO.getId(),
								pModeAmount.getAmount(), request.getUpdatedOrderDetailQuantityList().get(index), true,
								paymentModeSelected, locale);
					} else {
						// no quantity update
						pModeAmount = markOrderDeliveredV2(orderDetailsInfoIdList.get(i), adminDTO.getId(),
								pModeAmount.getAmount(), 0, false, paymentModeSelected, locale);
					}
					paymentModeIdList.add(pModeAmount.getPaymentModeId());
				}
				setOrderPaymentInfo(orderInfoId);
				balancePaidAtDelivery = pModeAmount.getAmount();
			}
		}
		if (balancePaidAtDelivery > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setPaymentModeId(paymentModeSelected);
			transactionRequest.setTransactionType(WalletDescription.RECHARGE_BY_MARKDELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), balancePaidAtDelivery, WalletDescription.RECHARGE.getName());

		}
		float walletBalanceAfterMarkingDelivery = checkMaidinBalance(request.getCustomerId());
		if (walletBalanceAfterMarkingDelivery > 0) {
			reshufflePayment(request.getCustomerId(), walletBalanceAfterMarkingDelivery, PaymentMode.ByWallet.getId(),
					true);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private void markOrderUndeliveredV2(Long orderDetailsInfoId, Long adminId, Long customerId) {
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		updateCustomerTransactionRemarks(detailsInfoDTO.getId(), RemarkStatus.UNDELIVERED.getId());
		if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			transactionRequest.setOrderId(detailsInfoDTO.getOrderId());
			transactionRequest.setOrderDetailsId(orderDetailsInfoId);
			transactionRequest.setMoneyIn(detailsInfoDTO.getAmountPaidByCustomer());
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setRemarkStatus(RemarkStatus.UNDELIVERED.getId());
			transactionRequest.setDueAmount(0);
			transactionRequest.setTransactionType(WalletDescription.REFUND_ON_UNDELIVERED.getId());
			addCustomerTransaction(transactionRequest, adminId);
			updateWalletBalance(customerId, detailsInfoDTO.getAmountPaidByCustomer(),
					WalletDescription.RECHARGE.getName());
		}
		detailsInfoDTO.setAmountPaidByCustomer(0);
		detailsInfoDTO.setOrderStatus(OrderStatus.UNDELIVERED.getName());
		detailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
		detailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
		detailsInfoDTO.setCancelled(true);
		detailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
		detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

	}

	private PaymentModeAmount markDeliveredWithQtyUpdateV2(Long orderDetailsInfoId, Long adminId, float amount,
			int quantity, int paymentMode, Locale locale) throws Exception {

		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();
		OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (orderDetailsInfoDTO != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());
			orderDetailsInfoDTO.setQuantity(quantity);

			// set price(ProductMRP , totalProductCost) if not updated earlier
			if (orderDetailsInfoDTO.getProductSalePrice() == 0) {
				ProductCityInfoV2DTO productCityInfoDTO = productCityInfoV2DAO
						.getByIdWithoutActiveStatus(orderDetailsInfoDTO.getProductId());
				orderDetailsInfoDTO.setProductMRP(orderDetailsInfoDTO.getProductMRP() > 0
						? orderDetailsInfoDTO.getProductMRP() : productCityInfoDTO.getMRP());
				orderDetailsInfoDTO.setProductSalePrice(orderDetailsInfoDTO.getProductSalePrice() > 0
						? orderDetailsInfoDTO.getProductSalePrice() : productCityInfoDTO.getSalePrice());
				orderDetailsInfoDTO.setDeliveryCharges(orderDetailsInfoDTO.getDeliveryCharges() > 0
						? orderDetailsInfoDTO.getDeliveryCharges() : productCityInfoDTO.getDeliveryCharge());
				ProductInfoV2DTO pInfoV2DTO = productInfoV2DAO
						.getByIdWithoutActiveState(productCityInfoDTO.getProductId());
				BrandInfoV2DTO brandInfoDTO = brandInfoV2DAO.getByBrandId(pInfoV2DTO.getBrandId());
				if (brandInfoDTO != null) {
					orderDetailsInfoDTO.setProductName(brandInfoDTO.getBrand() + " " + pInfoV2DTO.getProductName());
				}
			}

			// set updated total cost as per change in quantity

			orderDetailsInfoDTO.setTotalProductCost(
					(orderDetailsInfoDTO.getProductSalePrice() + orderDetailsInfoDTO.getDeliveryCharges())
							* orderDetailsInfoDTO.getQuantity());

			// check wallet balance
			float currentMaidinBalance = checkMaidinBalance(orderInfoDTO.getCustomerId());

			// make CustomerTransactionRequest object
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(orderInfoDTO.getCustomerId());
			transactionRequest.setOrderId(orderInfoDTO.getId());
			transactionRequest.setOrderDetailsId(orderDetailsInfoId);
			transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
			transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());

			float pendingAmount = orderDetailsInfoDTO.getTotalProductCost()
					- orderDetailsInfoDTO.getAmountPaidByCustomer();

			if ((pendingAmount > 0)) {

				if (currentMaidinBalance >= pendingAmount) {
					// CASE 1 : Wallet has sufficient amount
					transactionRequest.setDueAmount(0);
					transactionRequest.setMoneyOut(pendingAmount);
					orderDetailsInfoDTO
							.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
					orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());

					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
							WalletDescription.DEDUCTIONS.getName());
					orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());

				} else if (currentMaidinBalance > 0 && currentMaidinBalance < pendingAmount) {
					// case 2: Customer hasn't sufficient maidin balance

					// deduct Balance first from wallet
					transactionRequest.setMoneyOut(currentMaidinBalance);
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setDueAmount(pendingAmount - currentMaidinBalance);
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), currentMaidinBalance,
							WalletDescription.DEDUCTIONS.getName());
					orderDetailsInfoDTO.setAmountPaidByCustomer(
							orderDetailsInfoDTO.getAmountPaidByCustomer() + currentMaidinBalance);
					pendingAmount = pendingAmount - currentMaidinBalance;
					if (amount >= pendingAmount) {
						// deduct amount from amount passed if amount >
						// pendingAmount
						transactionRequest.setMoneyOut(pendingAmount);
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, adminId);
						orderDetailsInfoDTO
								.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
						orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
					} else {
						if (amount > 0) {

							// deduct amount from amount passed if amount > 0
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setPaymentModeId(paymentMode);
							transactionRequest.setDueAmount(pendingAmount - amount);
							addCustomerTransaction(transactionRequest, adminId);
							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
						} else if (amount <= 0) {
							// deduct his due amount from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setDueAmount(pendingAmount);
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
						}
					}
					amount = amount - pendingAmount;
				} else {
					if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
					} else {
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCash.getId());
					}
					if (amount > 0) {
						if (amount >= pendingAmount) {
							// case 3. amount >= pendingAmount of order
							transactionRequest.setMoneyOut(pendingAmount);
							transactionRequest.setDueAmount(0);
							transactionRequest.setPaymentModeId(paymentMode);
							addCustomerTransaction(transactionRequest, adminId);
							orderDetailsInfoDTO.setAmountPaidByCustomer(
									orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						} else {
							// case 4. amount < pendingAmount of order
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setDueAmount(pendingAmount - amount);
							transactionRequest.setPaymentModeId(paymentMode);
							addCustomerTransaction(transactionRequest, adminId);
							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
								orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
							} else {
								orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId());
							}
						}
					} else if (amount <= 0) {
						// due amount deducted from wallet
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmount);
						transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
								WalletDescription.DEDUCTIONS.getName());
						if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
						} else {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
						}
					}
					amount = amount - pendingAmount;
				}
			} else {
				// pendingAmount negative: re-charge wallet with refunded amount
				pendingAmount = pendingAmount * -1;
				transactionRequest.setMoneyIn(pendingAmount);
				orderDetailsInfoDTO.setAmountPaidByCustomer(orderDetailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
				addCustomerTransaction(transactionRequest, adminId);
				updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount, WalletDescription.RECHARGE.getName());
				orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
				orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
			}
			paymentModeAmountTuple.setAmount(amount);
			if (orderDetailsInfoDTO.getPaymentModeId() != null && orderDetailsInfoDTO.getPaymentModeId() > 0) {
				paymentModeAmountTuple.setPaymentModeId(orderDetailsInfoDTO.getPaymentModeId());
			} else if (orderInfoDTO.getPaymentModeId() != null) {
				paymentModeAmountTuple.setPaymentModeId(orderInfoDTO.getPaymentModeId());
			} else if (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
				paymentModeAmountTuple.setPaymentModeId(PaymentMode.ByWallet.getId());
			} else {
				paymentModeAmountTuple.setPaymentModeId(0);
			}

			orderDetailsInfoDTO.setQuantity(quantity);
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
		return paymentModeAmountTuple;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse changeDeliveredOrderStateV2(String authToken, ChangeDeliveredOrderStateRequest request,
			Locale locale) throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		}
		logger.info("AdminId: " + adminDTO.getId(), "authToken :" + authToken);
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		GenericResponse response = new GenericResponse();
		List<Long> undeliverdOrderList = new ArrayList<>();
		List<Long> deliverdOrderList = new ArrayList<>();
		float balancePaidAtDelivery = request.getAmount();
		int paymentMode = request.getPaymentMode();
		if (paymentMode == 0) {
			paymentMode = PaymentMode.ByCash.getId();
		}

		// add customer transaction entry if cash >0
		// float walletBalanceBeforeMarkingDelivery =
		// checkMaidinBalance(request.getCustomerId());
		if (balancePaidAtDelivery > 0) {

			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(paymentMode);
			transactionRequest.setTransactionType(WalletDescription.AMOUNT_RECEIVED_AT_DELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());

			if (paymentMode == PaymentMode.ByWallet.getId()) {
				/*
				 * updateWalletBalance(request.getCustomerId(),
				 * balancePaidAtDelivery, WalletDescription.RECHARGE.getName());
				 * // true means no need to update wallet for transactions
				 * balancePaidAtDelivery =
				 * reshufflePayment(request.getCustomerId(),
				 * balancePaidAtDelivery, paymentMode, true);
				 */
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.PAYMENTMODE_CANNOT_BE_WALLET, locale));

			} else {
				balancePaidAtDelivery = reshufflePayment(request.getCustomerId(), balancePaidAtDelivery, paymentMode,
						false);
			}

			transactionRequest = null;
		}

		for (int i = 0; i < request.getUpdatedOrderDetailInfoIdList().size(); i++) {
			if (request.getUpdatedOrderDetailStatusList().get(i).equals(OrderStatus.UNDELIVERED.getId())) {
				undeliverdOrderList.add(request.getUpdatedOrderDetailInfoIdList().get(i));
			} else {
				deliverdOrderList.add(request.getUpdatedOrderDetailInfoIdList().get(i));
			}
		}

		// restAmount = request.getAmount();
		for (Long orderInfoId : request.getOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				orderInfoDTO.setRemarks(request.getRemarks());
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();
				int undeliveryStatusCount = 0;
				for (int i = 0; i < orderDetailsInfoDTOList.size(); i++) {
					if (undeliverdOrderList.contains(orderDetailsInfoDTOList.get(i).getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTOList.get(i).getId());
				}

				// setting overall orderStatus
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setPayBy(null);
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(balancePaidAtDelivery);
				for (int i = 0; i < orderDetailsInfoIdList.size(); i++) {
					if (request.getUpdatedOrderDetailInfoIdList().contains(orderDetailsInfoIdList.get(i))) {
						int index = request.getUpdatedOrderDetailInfoIdList().indexOf(orderDetailsInfoIdList.get(i));
						if (index != -1) {
							pModeAmount = updateOrderDTOAndTransaction(orderDetailsInfoIdList.get(i), adminDTO.getId(),
									pModeAmount.getAmount(), paymentMode,
									request.getUpdatedOrderDetailStatusList().get(index),
									request.getUpdatedOrderDetailQuantityList().get(index), locale);
							paymentModeIdList.add(pModeAmount.getPaymentModeId());
						}
					}
				}
				setOrderPaymentInfo(orderInfoId);
				// setPaymentModeId(orderInfoId);
				balancePaidAtDelivery = pModeAmount.getAmount();
			}
		}

		if (balancePaidAtDelivery > 0) {
			// recharge rest amount
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(balancePaidAtDelivery);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(paymentMode);
			transactionRequest.setTransactionType(WalletDescription.RECHARGE_BY_MARKDELIVERY.getId());
			transactionRequest.setRemarkStatus(0);
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), balancePaidAtDelivery, WalletDescription.RECHARGE.getName());
			// reshuffle balance

		}
		float walletBalanceAfterMarkingDelivery = checkMaidinBalance(request.getCustomerId());
		if (walletBalanceAfterMarkingDelivery > 0) {
			reshufflePayment(request.getCustomerId(), walletBalanceAfterMarkingDelivery, PaymentMode.ByWallet.getId(),
					true);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		return response;
	}

	private PaymentModeAmount updateOrderDTOAndTransaction(Long orderDetailsInfoId, Long adminId, float amount,
			int paymentMode, int orderStatus, Integer quantity, Locale locale) throws Exception {
		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();
		OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (orderDetailsInfoDTO != null) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());

			// Case 1: product un-delivered, return money if paid
			if (orderStatus == OrderStatus.UNDELIVERED.getId() || quantity == 0) {
				CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
				transactionRequest.setCustomerId(orderInfoDTO.getCustomerId());
				transactionRequest.setOrderId(orderInfoDTO.getId());
				transactionRequest.setOrderDetailsId(orderDetailsInfoDTO.getId());
				transactionRequest.setMoneyIn(orderDetailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setDueAmount(0);
				transactionRequest.setTransactionType(WalletDescription.REFUND_ON_UNDELIVERED.getId());
				addCustomerTransaction(transactionRequest, adminId);
				updateWalletBalance(orderInfoDTO.getCustomerId(), orderDetailsInfoDTO.getTotalProductCost(),
						WalletDescription.RECHARGE.getName());

				// orderInfoDTO.setPayBy(null);
				orderDetailsInfoDTO.setPayBy(null);
				orderDetailsInfoDTO.setOrderStatus(OrderStatus.UNDELIVERED.getName());
				// orderDetailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
				orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
				orderDetailsInfoDTO.setCancelled(true);
				orderDetailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
				orderDetailsInfoDTO.setAmountPaidByCustomer(0);
				updateCustomerTransactionRemarks(orderDetailsInfoDTO.getId(), RemarkStatus.UNDELIVERED.getId());

				// case 2: product delivered
			} else if (orderStatus == OrderStatus.DELIVERED.getId()) {
				float pendingAmount = 0;
				if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
					pendingAmount = orderDetailsInfoDTO.getTotalProductCost();
				}
				orderDetailsInfoDTO.setAmountPaidByCustomer(0);
				orderDetailsInfoDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
				orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
				orderDetailsInfoDTO.setCancelled(false);
				orderDetailsInfoDTO.setOrderCancellationTime(null);

				if (pendingAmount > 0) {
					paymentModeAmountTuple = markOrderDeliveredV2(orderDetailsInfoId, adminId, amount, quantity, true,
							paymentMode, locale);
				}

			} else {
				// not applicable, throw exception
			}
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
		return paymentModeAmountTuple;
	}

	private void updateCustomerTransactionRemarks(Long orderDetailId, int remarkStatus) {
		List<CustomerTransactionDTO> transactionList = transactionDAO.getAllTransactionsByOrderDetailsId(orderDetailId);
		transactionList.stream().forEach(elt -> {
			elt.setRemarks(remarkStatus);
		});

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetOrderResponse getDeliveryList(String authToken, GetOrderRequest request, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		}
		GetOrderResponse response = new GetOrderResponse();

		String jsonFilePath = System.getProperty("config.dir") + "/json/cityProjectTowerList.json";
		long jsonFileLastUpdatedOn = jsonFileLastUpdatedOn(jsonFilePath);

		if (jsonFileLastUpdatedOn > jsonLoadedOn) {
			cityProjectJsonArray = readCityJSON(jsonFilePath);
			projectJsonArray = readProjectJSON(jsonFilePath);
			jsonLoadedOn = System.currentTimeMillis();
		}
		String cityName = getCityName(request.getCityId());
		String projectName = getProjectName(request.getProjectId());
		checkValidation(cityName, projectName, locale);

		String towerName = getTowerName(request.getProjectId(), request.getTowerId());
		if (request.getTowerId() > 0 && Strings.isNullOrEmpty(towerName)) {
			checkToweValidation(towerName, locale);
		}

		Date deliveryDate = new Date(request.getDeliveryDate());

		List<OrderInfoDTO> deliveryList = null;
		List<Long> customerList = null;
		List<String> timeSlotList = null;
		List<Long> addressIdList = null;
		List<AddressDTO> addressDTOList = new ArrayList<>();

		if (!Strings.isNullOrEmpty(towerName)) {
			if (request.getTimeSlot() != null) {
				if (request.getTimeSlot().equals("All")) {
					deliveryList = orderInfoDAO.getCustomersByCPTDeliveryDate(cityName, projectName, towerName,
							deliveryDate);
				} else {
					deliveryList = orderInfoDAO.getCustomersByCPTDeliveryDateAndTimeSlot(cityName, projectName,
							towerName, deliveryDate, request.getTimeSlot());
				}
			}
		} else {
			if (request.getTimeSlot() != null) {
				if (request.getTimeSlot().equals("All")) {
					deliveryList = orderInfoDAO.getCustomersByCPDeliveryDate(cityName, projectName, deliveryDate);
				} else {
					deliveryList = orderInfoDAO.getCustomersByCPDeliveryDateAndTimeSlot(cityName, projectName,
							deliveryDate, request.getTimeSlot());
				}
			}
		}
		if (deliveryList != null) {
			customerList = deliveryList.stream().map(OrderInfoDTO::getCustomerId).distinct()
					.collect(Collectors.toList());
			addressIdList = deliveryList.stream().map(OrderInfoDTO::getAddressId).distinct()
					.collect(Collectors.toList());

			if (addressIdList.size() > 0) {
				addressDTOList = addressDAO.getByIds(addressIdList);
			}

			if (customerList.size() == 0) {
				response.getCustomerOrderList().clear();
			}
			for (Long customerId : customerList) {
				float myTotalBalance = 0;

				WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
				if (walletDTO != null) {
					myTotalBalance = walletDTO.getMyWalletBalance();
				}
				List<Long> customerAddressList = deliveryList.stream().filter(e -> e.getCustomerId().equals(customerId))
						.map(OrderInfoDTO::getAddressId).distinct().collect(Collectors.toList());

				for (Long addressId : customerAddressList) {
					CustomerOrderDetails cDetails = new CustomerOrderDetails();
					cDetails.setCustomerId(customerId);
					CustomerDTO customerDTO = customerDAO.getById(customerId);
					cDetails.setCustomerName(customerDTO.getFullName());
					cDetails.setCustomerMobileNumber(customerDTO.getMobileNumber());
					cDetails.setWalletbalance(myTotalBalance);
					cDetails.setFlatName(addressDTOList.stream().filter(e -> e.getId().equals(addressId))
							.map(AddressDTO::getFlat).distinct().findFirst().orElse(""));
					cDetails.setTowerName(addressDTOList.stream().filter(e -> e.getId().equals(addressId))
							.map(AddressDTO::getTower).distinct().findFirst().orElse(""));

					if (request.getTimeSlot() != null) {
						if (request.getTimeSlot().equals("All")) {
							timeSlotList = deliveryList.stream()
									.filter(e -> e.getCustomerId().equals(customerId)
											&& e.getAddressId().equals(addressId))
									.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
						} else {
							timeSlotList = deliveryList.stream()
									.filter(e -> e.getCustomerId().equals(customerId)
											&& e.getTimeSlot().equals(request.getTimeSlot())
											&& e.getAddressId().equals(addressId))
									.map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());

						}
					}
					float restMoneyInWallet = myTotalBalance;
					for (String timeSlot : timeSlotList) {
						CustomerTimeSlot cTimeSlot = new CustomerTimeSlot();
						PaymentMethod pMethod = new PaymentMethod();
						/*
						 * float cash = 0; float wallet = 0;
						 * 
						 * float card = 0; float paytm = 0; float phonePe = 0;
						 * float otherMode = 0;
						 */float billAmount = 0;
						float dueAmount = 0;
						List<OrderInfoDTO> orderInfoList = deliveryList
								.stream().filter(e -> e.getCustomerId().equals(customerId)
										&& e.getTimeSlot().equals(timeSlot) && e.getAddressId().equals(addressId))
								.collect(Collectors.toList());
						if (orderInfoList.size() == 0) {
							continue;
						}
						for (OrderInfoDTO orderInfo : orderInfoList) {
							pMethod.setAmountReceived(orderInfo.getAmountReceivedAtDelivery());
							cTimeSlot.setTimeSlotId(orderInfo.getTimeSlotId());
							OrderInfoDetails orderInfoDetails = new OrderInfoDetails();
							List<Object[]> list = orderDetailsInfoDAO.getSettledOrdersByOrderId(orderInfo.getId());
							List<OrderDetailsInfo> orderDetailsInfoDTOList = new ArrayList<>();
							list.stream().forEach((record) -> {
								OrderDetailsInfo orderDetailsInfo = new OrderDetailsInfo();
								orderDetailsInfo.setId(Long.valueOf(record[0].toString()));
								orderDetailsInfo.setProductName(String.valueOf(record[2].toString()));
								orderDetailsInfo.setQuantityUnit(String.valueOf(record[3].toString()));
								orderDetailsInfo.setQuantity(Integer.valueOf(record[4].toString()));
								orderDetailsInfo.setOrderMRP(Float.valueOf(record[5].toString()));
								orderDetailsInfo.setOrderSalePrice(Float.valueOf(record[6].toString()));
								orderDetailsInfo.setTotalProductCost(Float.valueOf(record[7].toString()));
								orderDetailsInfo.setAmountPaid(Float.valueOf(record[8].toString()));
								orderDetailsInfo.setPendingAmount(Float.valueOf(record[9].toString()));
								orderDetailsInfo.setOrderDeliveryCharge(Float.valueOf(record[10].toString()));
								orderDetailsInfo.setOrderStatus(Integer.valueOf(record[11].toString()));
								orderDetailsInfo.setPaymentStatus(String.valueOf(record[12].toString()));
								orderDetailsInfo.setProductMRP(Float.valueOf(record[13].toString()));
								orderDetailsInfo.setProductSalePrice(Float.valueOf(record[14].toString()));
								orderDetailsInfo.setProductDeliveryCharge(Float.valueOf(record[15].toString()));
								orderDetailsInfoDTOList.add(orderDetailsInfo);
							});

							if (orderDetailsInfoDTOList.size() > 0) {
								orderInfoDetails.setOrderInfoId(orderInfo.getId());
								if (orderInfo.getRequestedPaymentModeId() != null) {
									orderInfoDetails.setRequestedPaymentModeId(orderInfo.getRequestedPaymentModeId());
								}
								for (OrderDetailsInfo orderDetailsInfo : orderDetailsInfoDTOList) {
									DeliveryDetails deliveryDetails = new DeliveryDetails();
									deliveryDetails.setOrderDetailsId(orderDetailsInfo.getId());
									deliveryDetails.setProductName(orderDetailsInfo.getProductName());
									deliveryDetails.setQuantityUnit(orderDetailsInfo.getQuantityUnit());
									deliveryDetails.setQuantity(orderDetailsInfo.getQuantity());
									deliveryDetails.setPrice(orderDetailsInfo.getProductSalePrice() > 0
											? ((orderDetailsInfo.getOrderSalePrice()
													+ orderDetailsInfo.getOrderDeliveryCharge())
													* orderDetailsInfo.getQuantity())
											: ((orderDetailsInfo.getProductSalePrice()
													+ orderDetailsInfo.getProductDeliveryCharge())
													* orderDetailsInfo.getQuantity()));
									if (orderDetailsInfo.getOrderStatus() == (OrderStatus.DELIVERED.getId())) {
										deliveryDetails.setDeliveryStatus(orderDetailsInfo.getOrderStatus());
										cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId());
									} else if (orderDetailsInfoDTOList.size() > 0 && (orderDetailsInfo
											.getOrderStatus() == (OrderStatus.UNDELIVERED.getId()))) {
										deliveryDetails.setDeliveryStatus(orderDetailsInfo.getOrderStatus());
										cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId());
									} else if (orderDetailsInfo.getOrderStatus() == (OrderStatus.CONFIRMED.getId())) {
										deliveryDetails.setDeliveryStatus(OrderStatus.DELIVERYSTATUSPENDING.getId());
									}
									if (orderDetailsInfo.getOrderStatus() != (OrderStatus.UNDELIVERED.getId())) {
										billAmount = billAmount + (deliveryDetails.getPrice());
										dueAmount = dueAmount
												+ (deliveryDetails.getPrice() - orderDetailsInfo.getAmountPaid());
									}
									orderInfoDetails.getDeliveryDetails().add(deliveryDetails);
								}
							}

							cTimeSlot.getOrderInfoList().add(orderInfoDetails);
						}
						List<Long> orderIdList = orderInfoList.stream().map(OrderInfoDTO::getId)
								.collect(Collectors.toList());
						List<Object[]> paymentModeList = transactionDAO.getPaymentInfoByOrderIdList(orderIdList);
						paymentModeList.stream().forEach((record) -> {
							PaymentSettledModes mode = new PaymentSettledModes();
							mode.setPaymentModeId(Integer.parseInt(record[0].toString()));
							mode.setAmount(Float.parseFloat(record[1].toString()));
							mode.setPaymentModeText(record[2].toString());
							pMethod.getPaymentSettledModeList().add(mode);
						});
						pMethod.setDueAmount(dueAmount);
						if (dueAmount > 0) {
							if (restMoneyInWallet >= dueAmount) {
								pMethod.setAmountNeedToCollect(restMoneyInWallet - dueAmount);
							} else {
								pMethod.setAmountNeedToCollect(dueAmount - restMoneyInWallet);
							}
						}
						cTimeSlot.setPaymentMethod(pMethod);
						cTimeSlot.setTotalBillAmount(billAmount);
						cDetails.getCustomerTimeSlotList().add(cTimeSlot);
					}

					response.getCustomerOrderList().add(cDetails);
				}
			}
		} else {
			response.getCustomerOrderList().clear();
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		return response;

	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public GetOrderResponse
	 * getDeliveryList(String authToken, GetOrderRequest request, Locale locale)
	 * throws Exception { String mobile =
	 * authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken)); //
	 * logger.info(ApiUtils.REQUEST_PAYLOAD, // ApiUtils.toJsonString(request));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if (adminDTO !=
	 * null) { logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(),
	 * ApiUtils.toJsonString(request));
	 * 
	 * } GetOrderResponse response = new GetOrderResponse();
	 * 
	 * String jsonFilePath = System.getProperty("config.dir") +
	 * "/json/cityProjectTowerList.json"; long jsonFileLastUpdatedOn =
	 * jsonFileLastUpdatedOn(jsonFilePath);
	 * 
	 * if (jsonFileLastUpdatedOn > jsonLoadedOn) { cityProjectJsonArray =
	 * readCityJSON(jsonFilePath); projectJsonArray =
	 * readProjectJSON(jsonFilePath); jsonLoadedOn = System.currentTimeMillis();
	 * } String cityName = getCityName(request.getCityId()); String projectName
	 * = getProjectName(request.getProjectId()); checkValidation(cityName,
	 * projectName, locale);
	 * 
	 * String towerName = getTowerName(request.getProjectId(),
	 * request.getTowerId()); if (request.getTowerId() > 0 &&
	 * Strings.isNullOrEmpty(towerName)) { checkToweValidation(towerName,
	 * locale); }
	 * 
	 * Date deliveryDate = new Date(request.getDeliveryDate());
	 * 
	 * List<OrderInfoDTO> deliveryList = null; List<Long> customerList = null;
	 * List<String> timeSlotList = null; List<Long> addressIdList = null;
	 * List<AddressDTO> addressDTOList = new ArrayList<>();
	 * 
	 * if (!Strings.isNullOrEmpty(towerName)) { if (request.getTimeSlot() !=
	 * null) { if (request.getTimeSlot().equals("All")) { deliveryList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDate(cityName, projectName,
	 * towerName, deliveryDate); } else { deliveryList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDateAndTimeSlot(cityName,
	 * projectName, towerName, deliveryDate, request.getTimeSlot()); } } } else
	 * { if (request.getTimeSlot() != null) { if
	 * (request.getTimeSlot().equals("All")) { deliveryList =
	 * orderInfoDAO.getCustomersByCPDeliveryDate(cityName, projectName,
	 * deliveryDate); } else { deliveryList =
	 * orderInfoDAO.getCustomersByCPDeliveryDateAndTimeSlot(cityName,
	 * projectName, deliveryDate, request.getTimeSlot()); } } } customerList =
	 * deliveryList.stream().map(OrderInfoDTO::getCustomerId).distinct().collect
	 * (Collectors.toList()); addressIdList =
	 * deliveryList.stream().map(OrderInfoDTO::getAddressId).distinct().collect(
	 * Collectors.toList());
	 * 
	 * if (addressIdList.size() > 0) { addressDTOList =
	 * addressDAO.getByIds(addressIdList); }
	 * 
	 * if (customerList.size() == 0) { response.getCustomerOrderList().clear();
	 * } for (Long customerId : customerList) { float myTotalBalance = 0;
	 * 
	 * WalletDTO walletDTO = walletDAO.getByCustomerId(customerId); if
	 * (walletDTO != null) { myTotalBalance = walletDTO.getMyWalletBalance(); }
	 * List<Long> customerAddressList = deliveryList.stream().filter(e ->
	 * e.getCustomerId().equals(customerId))
	 * .map(OrderInfoDTO::getAddressId).distinct().collect(Collectors.toList());
	 * ;
	 * 
	 * for (Long addressId : customerAddressList) { CustomerOrderDetails
	 * cDetails = new CustomerOrderDetails();
	 * cDetails.setCustomerId(customerId); CustomerDTO customerDTO =
	 * customerDAO.getById(customerId);
	 * cDetails.setCustomerName(customerDTO.getFullName());
	 * cDetails.setCustomerMobileNumber(customerDTO.getMobileNumber());
	 * cDetails.setWalletbalance(myTotalBalance);
	 * cDetails.setFlatName(addressDTOList.stream().filter(e ->
	 * e.getId().equals(addressId))
	 * .map(AddressDTO::getFlat).distinct().findFirst().orElse(""));
	 * cDetails.setTowerName(addressDTOList.stream().filter(e ->
	 * e.getId().equals(addressId))
	 * .map(AddressDTO::getTower).distinct().findFirst().orElse(""));
	 * 
	 * if (request.getTimeSlot() != null) { if
	 * (request.getTimeSlot().equals("All")) { timeSlotList =
	 * deliveryList.stream() .filter(e -> e.getCustomerId().equals(customerId)
	 * && e.getAddressId().equals(addressId))
	 * .map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
	 * } else { timeSlotList = deliveryList.stream() .filter(e ->
	 * e.getCustomerId().equals(customerId) &&
	 * e.getTimeSlot().equals(request.getTimeSlot()) &&
	 * e.getAddressId().equals(addressId))
	 * .map(OrderInfoDTO::getTimeSlot).distinct().collect(Collectors.toList());
	 * 
	 * } } float restMoneyInWallet = myTotalBalance; for (String timeSlot :
	 * timeSlotList) { CustomerTimeSlot cTimeSlot = new CustomerTimeSlot();
	 * PaymentMethod pMethod = new PaymentMethod(); float cash = 0; float wallet
	 * = 0; float billAmount = 0; float card = 0; float paytm = 0; float phonePe
	 * = 0; float otherMode = 0; float dueAmount = 0; List<OrderInfoDTO>
	 * orderInfoList = deliveryList.stream() .filter(e ->
	 * e.getCustomerId().equals(customerId) &&
	 * e.getDeliveryDate().equals(deliveryDate) &&
	 * e.getTimeSlot().equals(timeSlot) && e.getAddressId().equals(addressId))
	 * .collect(Collectors.toList()); if (orderInfoList.size() == 0) { continue;
	 * } for (OrderInfoDTO orderInfo : orderInfoList) {
	 * pMethod.setAmountReceived(orderInfo.getAmountReceivedAtDelivery());
	 * cTimeSlot.setTimeSlotId(orderInfo.getTimeSlotId()); OrderInfoDetails
	 * orderInfoDetails = new OrderInfoDetails(); List<OrderDetailsInfoDTO>
	 * orderDetailsInfoDTOList = orderDetailsInfoDAO
	 * .getSettledOrderDetailsByOrderId(orderInfo.getId()); if
	 * (orderDetailsInfoDTOList.size() > 0) {
	 * orderInfoDetails.setOrderInfoId(orderInfo.getId()); if
	 * (orderInfo.getRequestedPaymentModeId() != null) {
	 * orderInfoDetails.setRequestedPaymentModeId(orderInfo.
	 * getRequestedPaymentModeId()); } for (OrderDetailsInfoDTO
	 * orderDetailsInfoDTO : orderDetailsInfoDTOList) { DeliveryDetails
	 * deliveryDetails = new DeliveryDetails(); deliveryDetails =
	 * setDeliveryDetails(orderDetailsInfoDTO, orderInfo, restMoneyInWallet); if
	 * (deliveryDetails != null && deliveryDetails.getPaymentMethod() != null) {
	 * restMoneyInWallet =
	 * deliveryDetails.getPaymentMethod().getRestMoneyInWallet(); } if
	 * (deliveryDetails.getDeliveryStatus() == (OrderStatus.DELIVERED.getId()))
	 * { cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } else if
	 * (orderDetailsInfoDTOList.size() > 0 && ((deliveryDetails
	 * .getDeliveryStatus() == (OrderStatus.UNDELIVERED.getId())))) {
	 * cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } if
	 * (deliveryDetails.getDeliveryStatus() !=
	 * (OrderStatus.UNDELIVERED.getId())) { billAmount = billAmount +
	 * (deliveryDetails.getPrice() * deliveryDetails.getQuantity()); }
	 * 
	 * orderInfoDetails.getDeliveryDetails().add(deliveryDetails); if
	 * (deliveryDetails.getPaymentMethod() != null) { cash = cash +
	 * deliveryDetails.getPaymentMethod().getCash(); wallet = wallet +
	 * deliveryDetails.getPaymentMethod().getWallet(); card = card +
	 * deliveryDetails.getPaymentMethod().getCard(); paytm = paytm +
	 * deliveryDetails.getPaymentMethod().getPaytm(); phonePe = phonePe +
	 * deliveryDetails.getPaymentMethod().getPhonePe(); otherMode = otherMode +
	 * deliveryDetails.getPaymentMethod().getOtherMode(); dueAmount = dueAmount
	 * + deliveryDetails.getPaymentMethod().getDueAmount(); } } }
	 * 
	 * cTimeSlot.getOrderInfoList().add(orderInfoDetails); }
	 * 
	 * pMethod.setCash(cash); pMethod.setWallet(wallet); pMethod.setCard(card);
	 * pMethod.setPaytm(paytm); pMethod.setDueAmount(dueAmount);
	 * pMethod.setPhonePe(phonePe);
	 * 
	 * // pMethod.setRestMoneyInWallet(value);
	 * cTimeSlot.setPaymentMethod(pMethod);
	 * cTimeSlot.setTotalBillAmount(billAmount);
	 * cDetails.getCustomerTimeSlotList().add(cTimeSlot); }
	 * 
	 * response.getCustomerOrderList().add(cDetails); } }
	 * 
	 * response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES);
	 * 
	 * return response;
	 * 
	 * }
	 */

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public GetOrderResponse
	 * getDeliveryList(String authToken, GetOrderRequest request, Locale locale)
	 * throws Exception { String mobile =
	 * authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
	 * logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if (adminDTO !=
	 * null) { logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
	 * authToken); } GetOrderResponse response = new GetOrderResponse();
	 * 
	 * String jsonFilePath = System.getProperty("config.dir") +
	 * "/json/cityProjectTowerList.json"; long jsonFileLastUpdatedOn =
	 * jsonFileLastUpdatedOn(jsonFilePath);
	 * 
	 * if (jsonFileLastUpdatedOn > jsonLoadedOn) { cityProjectJsonArray =
	 * readCityJSON(jsonFilePath); projectJsonArray =
	 * readProjectJSON(jsonFilePath); jsonLoadedOn = System.currentTimeMillis();
	 * } String cityName = getCityName(request.getCityId()); String projectName
	 * = getProjectName(request.getProjectId()); checkValidation(cityName,
	 * projectName, locale);
	 * 
	 * String towerName = getTowerName(request.getProjectId(),
	 * request.getTowerId()); if (request.getTowerId() > 0 &&
	 * Strings.isNullOrEmpty(towerName)) { checkToweValidation(towerName,
	 * locale); }
	 * 
	 * Date deliveryDate = new Date(request.getDeliveryDate());
	 * 
	 * List<Long> customerList = null; List<String> timeSlotList = null;
	 * 
	 * if (!Strings.isNullOrEmpty(towerName)) { if (request.getTimeSlot() !=
	 * null) { if (request.getTimeSlot().equals("All")) { customerList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDate(cityName, projectName,
	 * towerName, deliveryDate); } else { customerList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDateAndTimeSlot(cityName,
	 * projectName, towerName, deliveryDate, request.getTimeSlot()); } } } else
	 * { if (request.getTimeSlot() != null) { if
	 * (request.getTimeSlot().equals("All")) { customerList =
	 * orderInfoDAO.getCustomersByCPDeliveryDate(cityName, projectName,
	 * deliveryDate); } else { customerList =
	 * orderInfoDAO.getCustomersByCPDeliveryDateAndTimeSlot(cityName,
	 * projectName, deliveryDate, request.getTimeSlot()); } } } if
	 * (customerList.size() == 0) { response.getCustomerOrderList().clear(); }
	 * for (Long customerId : customerList) { float myTotalBalance = 0;
	 * 
	 * WalletDTO walletDTO = walletDAO.getByCustomerId(customerId); if
	 * (walletDTO != null) { myTotalBalance = walletDTO.getMyWalletBalance(); }
	 * CustomerOrderDetails cDetails = new CustomerOrderDetails();
	 * cDetails.setCustomerId(customerId); CustomerDTO customerDTO =
	 * customerDAO.getById(customerId);
	 * cDetails.setCustomerName(customerDTO.getFullName());
	 * 
	 * AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
	 * cDetails.setFlatName(addressDTO.getFlat());
	 * cDetails.setTowerName(addressDTO.getTower());
	 * 
	 * cDetails.setCustomerMobileNumber(customerDTO.getMobileNumber());
	 * cDetails.setWalletbalance(myTotalBalance);
	 * ///////////////////////////////////////////// // cDet
	 * ///////////////////////////////////////////// if (request.getTimeSlot()
	 * != null) { if (request.getTimeSlot().equals("All")) { timeSlotList =
	 * orderInfoDAO.getTimeSlotStringByCustomerDeliveryDate(customerId,
	 * deliveryDate); } else { timeSlotList =
	 * orderInfoDAO.getTimeSlotStringByCustomerDeliveryDateAndTimeSlot(
	 * customerId, deliveryDate, request.getTimeSlot()); } } float
	 * restMoneyInWallet = myTotalBalance; for (String timeSlot : timeSlotList)
	 * { CustomerTimeSlot cTimeSlot = new CustomerTimeSlot(); PaymentMethod
	 * pMethod = new PaymentMethod(); float cash = 0; float wallet = 0; float
	 * billAmount = 0; float card = 0; float paytm = 0; float phonePe = 0; float
	 * otherMode = 0; float dueAmount = 0; List<OrderInfoDTO> orderInfoList =
	 * orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotAndCityCode(
	 * customerId, deliveryDate, timeSlot, Long.toString(request.getCityId()));
	 * if (orderInfoList.size() == 0) { continue; } for (OrderInfoDTO orderInfo
	 * : orderInfoList) {
	 * pMethod.setAmountReceived(orderInfo.getAmountReceivedAtDelivery());
	 * cTimeSlot.setTimeSlotId(orderInfo.getTimeSlotId()); // AddressDTO
	 * addressDTO = // addressDAO.getByFlatId(orderInfo.getFlatId()); AddressDTO
	 * addressDTO = addressDAO.getById(orderInfo.getAddressId());
	 * cDetails.setFlatName(addressDTO.getFlat());
	 * cDetails.setTowerName(addressDTO.getTower()); OrderInfoDetails
	 * orderInfoDetails = new OrderInfoDetails(); List<OrderDetailsInfoDTO>
	 * orderDetailsInfoDTOList = orderDetailsInfoDAO
	 * .getSettledOrderDetailsByOrderId(orderInfo.getId()); if
	 * (orderDetailsInfoDTOList.size() > 0) {
	 * orderInfoDetails.setOrderInfoId(orderInfo.getId());
	 * 
	 * for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
	 * DeliveryDetails deliveryDetails = new DeliveryDetails(); deliveryDetails
	 * = setDeliveryDetails(orderDetailsInfoDTO, orderInfo, restMoneyInWallet);
	 * if (deliveryDetails != null && deliveryDetails.getPaymentMethod() !=
	 * null) { restMoneyInWallet =
	 * deliveryDetails.getPaymentMethod().getRestMoneyInWallet(); } if
	 * (deliveryDetails.getDeliveryStatus() == (OrderStatus.DELIVERED.getId()))
	 * { cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } else if
	 * (orderDetailsInfoDTOList.size() > 0 &&
	 * ((deliveryDetails.getDeliveryStatus() ==
	 * (OrderStatus.UNDELIVERED.getId())))) {
	 * cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } if
	 * (deliveryDetails.getDeliveryStatus() !=
	 * (OrderStatus.UNDELIVERED.getId())) { billAmount = billAmount +
	 * (deliveryDetails.getPrice() * deliveryDetails.getQuantity()); }
	 * 
	 * orderInfoDetails.getDeliveryDetails().add(deliveryDetails); if
	 * (deliveryDetails.getPaymentMethod() != null) { cash = cash +
	 * deliveryDetails.getPaymentMethod().getCash(); wallet = wallet +
	 * deliveryDetails.getPaymentMethod().getWallet(); card = card +
	 * deliveryDetails.getPaymentMethod().getCard(); paytm = paytm +
	 * deliveryDetails.getPaymentMethod().getPaytm(); phonePe = phonePe +
	 * deliveryDetails.getPaymentMethod().getPhonePe(); otherMode = otherMode +
	 * deliveryDetails.getPaymentMethod().getOtherMode(); dueAmount = dueAmount
	 * + deliveryDetails.getPaymentMethod().getDueAmount(); } } }
	 * 
	 * cTimeSlot.getOrderInfoList().add(orderInfoDetails); }
	 * 
	 * pMethod.setCash(cash); pMethod.setWallet(wallet); pMethod.setCard(card);
	 * pMethod.setPaytm(paytm); pMethod.setDueAmount(dueAmount);
	 * pMethod.setPhonePe(phonePe);
	 * 
	 * // pMethod.setRestMoneyInWallet(value);
	 * cTimeSlot.setPaymentMethod(pMethod);
	 * cTimeSlot.setTotalBillAmount(billAmount);
	 * cDetails.getCustomerTimeSlotList().add(cTimeSlot); }
	 * 
	 * response.getCustomerOrderList().add(cDetails); }
	 * 
	 * response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES);
	 * 
	 * return response;
	 * 
	 * }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetPendingAmount getPendingAmount(String authToken, Locale locale) {
		// TODO Auto-generated method stub
		GetPendingAmount response = new GetPendingAmount();
		List<Long> customerList = orderDetailsInfoDAO.getUnsettledOrderCustomerList();
		// customerList = customerList.stream().filter(e->e.longValue() ==
		// 333).collect(Collectors.toList());
		// customerList.add(333L);
		for (Long customerId : customerList) {
			float amount = 0;
			float wallet = checkMaidinBalance(customerId);
			CustomerBalanceInfo customerBalance = new CustomerBalanceInfo();
			List<OrderDetailsInfoDTO> orderInfoDTOList = orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId);
			for (OrderDetailsInfoDTO orderDetailsInfo : orderInfoDTOList) {
				// System.out.println("Pending amount:"+
				// (orderDetailsInfo.getTotalProductCost() -
				// orderDetailsInfo.getAmountPaidByCustomer()));
				amount = amount - (orderDetailsInfo.getTotalProductCost() - orderDetailsInfo.getAmountPaidByCustomer());
				System.out.println("orderDetailsId: " + orderDetailsInfo.getId() + " Pending amount: "
						+ (orderDetailsInfo.getTotalProductCost() - orderDetailsInfo.getAmountPaidByCustomer())
						+ " amount: " + amount);
			}
			customerBalance.setCustomer(customerId);
			customerBalance.setAmount(amount);
			/*
			 * if(wallet == amount ){ continue; }
			 */

			customerBalance.setWalletBalance(wallet);
			response.getCustomerBalanceInfo().add(customerBalance);
		}

		/////// Start settling

		/*
		 * List<CustomerBalanceInfo> balanceInfos =
		 * response.getCustomerBalanceInfo(); for (CustomerBalanceInfo
		 * balanceInfo : balanceInfos) { if (balanceInfo.getAmount() <= 0 &&
		 * balanceInfo.getAmount() < balanceInfo.getWalletBalance()) { float
		 * amountToSettled = balanceInfo.getWalletBalance() -
		 * balanceInfo.getAmount(); if (amountToSettled > 0) {
		 * System.out.println(balanceInfo.getCustomer());
		 * settledPayment(balanceInfo.getCustomer(), amountToSettled); } } }
		 */

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		return response;

	}

	private void settledPayment(long customerId, float maidinBalance) {
		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				System.out.println("orderDetailsDTO:  " + orderDetailsDTO);
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());
				float pendingAmountToPay = orderDetailsDTO.getTotalProductCost()
						- orderDetailsDTO.getAmountPaidByCustomer();
				if (maidinBalance > 0 && pendingAmountToPay > 0) {
					if (maidinBalance >= pendingAmountToPay) {
						transactionRequest.setMoneyOut(pendingAmountToPay);
						orderDetailsDTO.setAmountPaidByCustomer(
								orderDetailsDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, -1);
						maidinBalance = maidinBalance - pendingAmountToPay;
						orderDetailsDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					} else {
						transactionRequest.setMoneyOut(maidinBalance);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + maidinBalance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(pendingAmountToPay - maidinBalance);
						addCustomerTransaction(transactionRequest, -1);
						maidinBalance = 0;
						orderDetailsDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
					}
				}
				// TODO:change SetPaymentModeid
				setPaymentModeId(orderDetailsDTO.getOrderId(), PaymentMode.ByWallet.getId());
			}
			transactionRequest = null;
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse updatePhotoIdInOtherCities(String authToken, Locale locale) {
		GenericResponse response = new GenericResponse();
		List<ProductCityInfoV2DTO> list = productCityInfoV2DAO.getByCityCode("100001");
		for (ProductCityInfoV2DTO dto : list) {
			List<ProductCityInfoV2DTO> productCityList = productCityInfoV2DAO
					.getByProductIdQuantityUnit(dto.getProductId(), dto.getQuantityUnit());
			productCityList.stream().forEach(e -> e.setPhotoId(dto.getPhotoId()));
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

}
