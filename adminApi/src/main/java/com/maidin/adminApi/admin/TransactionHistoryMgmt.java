package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.pojo.transactions.GetCustomerTransactionResponse;


public interface TransactionHistoryMgmt {

	public GetCustomerTransactionResponse getCustomerTransactionByPageNumber(String authToken, String mobileNumber,
			Integer currentPageNumber, Locale locale) throws Exception;

}
