package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.OrderUpdate;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.UpdateProductRequest;

public interface ReportMgmt {

	public OrderReportResponse getOrderReport(OrderRequest request, String authToken, Locale locale)  throws Exception;

	GenericResponse getNewCustomerRegistrationsReport(String authToken, Locale locale) throws Exception;

	
	

	

	

}
