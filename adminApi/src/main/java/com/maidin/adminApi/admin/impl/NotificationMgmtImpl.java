package com.maidin.adminApi.admin.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.gson.Gson;
import com.maidin.adminApi.admin.NotificationMgmt;
import com.maidin.adminApi.enums.NotificationType;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.NotificationDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.OrderPaymentDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.NotificationDTO;
import com.maidin.persistence.model.InfoNotificationDetails;
import com.maidin.pojo.notifications.AllDeviceNotificationRequest;
import com.maidin.pojo.notifications.InfoNotificationHistoryResponse;
import com.maidin.pojo.notifications.NotificationDetails;
import com.maidin.pojo.notifications.NotificationResponse;

public class NotificationMgmtImpl implements NotificationMgmt {
	private final ProductDAO productDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final CustomerDAO customerDAO;
	//private final OrderPaymentDAO orderPaymentDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final CustomerTransactionDAO customerTransactionDAO;
	private final AddressDAO addressDAO;
	private final AdminDAO adminDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private final NotificationDAO notificationDAO;

	@Autowired
	public NotificationMgmtImpl(ProductDAO productDAO, AuthenticationMgmt authenticationMgmt, CustomerDAO customerDAO,
			/*OrderPaymentDAO orderPaymentDAO,*/ OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			CustomerTransactionDAO customerTransactionDAO, AddressDAO addressDAO, AdminDAO adminDAO,
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO, NotificationDAO notificationDAO) {
		this.productDAO = productDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.customerDAO = customerDAO;
		//this.orderPaymentDAO = orderPaymentDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.customerTransactionDAO = customerTransactionDAO;
		this.addressDAO = addressDAO;
		this.adminDAO = adminDAO;
		this.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
		this.notificationDAO = notificationDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(TransactionHistoryMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String testUserCustomerId = prop.getProperty(ResourceProperties.TEST_USER_ACCOUNT_ID);
	private static final String adminTransactionsRecordsCount = prop
			.getProperty(ResourceProperties.ADMIN_TRANSACTIONS_RECORDS_COUNT);

	@Override
	@Transactional(rollbackFor = Exception.class)
	public NotificationResponse sendMessageToAllDevices(String authToken, AllDeviceNotificationRequest request,
			Locale locale) throws Exception {
		
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		NotificationResponse response = new NotificationResponse();

		List<String> notificationTokensList = customerDeviceDetailsDAO
				.getAllNotificationTokens(Long.parseLong(testUserCustomerId));
		System.out.println(notificationTokensList);

		if (notificationTokensList.size() < 1000) {
			response = makeHTTPConnection(request, notificationTokensList, response);
		} else {
			List<String> notificationTokensListCopy;
			logger.info("Size of notificationTokensList BEFORE sending : " + notificationTokensList.size());
			System.out.println("Size of notificationTokensList BEFORE sending : " + notificationTokensList.size());
			while (notificationTokensList.size() > 0) {
				if (notificationTokensList.size() > 1000) {
					notificationTokensListCopy = notificationTokensList.subList(0, 1000);
				} else {
					notificationTokensListCopy = notificationTokensList;
				}

				response = makeHTTPConnection(request, notificationTokensListCopy, response);
				notificationTokensList.removeAll(notificationTokensListCopy);
			}
			logger.info("Size of notificationTokensList AFTER sending : " + notificationTokensList.size());
			System.out.println("Size of notificationTokensList AFTER sending : " + notificationTokensList.size());
		}

		if (response != null && (response.getSuccess() > 0 || response.getFailure() > 0)) {
			addNotificationInfo(request.getTitle(), request.getBody(), adminDTO.getId(), response.getSuccess(),
					response.getFailure());
		}

		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private NotificationResponse makeHTTPConnection(AllDeviceNotificationRequest request,
			List<String> notificationTokensList, NotificationResponse response) {
		int responseCode = -1;
		String responseBody = null;
		try {
			System.out.println("Sending FCM request");
			JSONObject postData = getPostData(request.getTitle(), request.getBody(), notificationTokensList);

			URL url = new URL(prop.getProperty(ResourceProperties.FCM_URL));
			HttpsURLConnection httpURLConnection = (HttpsURLConnection) url.openConnection();
			httpURLConnection.setConnectTimeout(10000);
			httpURLConnection.setReadTimeout(10000);
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setUseCaches(false);
			httpURLConnection.setRequestMethod("POST");
			httpURLConnection.setRequestProperty("Content-Type", "application/json");

			httpURLConnection.setRequestProperty("Authorization",
					"key=" + prop.getProperty(ResourceProperties.FCM_KEY_CUSTOMER));
			OutputStream out = httpURLConnection.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(out, "utf-8");
			osw.write(postData.toString());
			osw.flush();
			out.close();
			responseCode = httpURLConnection.getResponseCode();

			if (responseCode == HttpStatus.SC_OK) {
				responseBody = convertStreamToString(httpURLConnection.getInputStream());
			} else {
				responseBody = convertStreamToString(httpURLConnection.getErrorStream());
			}

			Gson gson = new Gson();
			NotificationResponse newResponse = gson.fromJson(responseBody, NotificationResponse.class);

			response.setSuccess(response.getSuccess() + newResponse.getSuccess());
			response.setFailure(response.getFailure() + newResponse.getFailure());

			logger.info("FCM message service response: " + responseBody);
			// System.out.println("FCM message service response: " +
			// responseBody);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public JSONObject getPostData(String title, String body, List<String> notificationTokensList)
			throws JSONException, IOException {
		HashMap<String, Object> notificationPayload = new HashMap<>();
		JSONObject payloadObject = new JSONObject();

		notificationPayload.put("title", title);
		notificationPayload.put("body", body);

		JSONObject notification = new JSONObject(notificationPayload);

		payloadObject.put("notification", notification);
		payloadObject.put("priority", "high");
		payloadObject.put("registration_ids", notificationTokensList);

		return payloadObject;
	}

	public static String convertStreamToString(InputStream inStream) throws Exception {
		InputStreamReader inputStream = new InputStreamReader(inStream);
		BufferedReader bReader = new BufferedReader(inputStream);

		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = bReader.readLine()) != null) {
			sb.append(line);
		}

		return sb.toString();
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addNotificationInfo(String title, String message, Long adminId, Long successCount, Long failureCount)
			throws Exception {
		NotificationDTO notificationDTO = new NotificationDTO();
		notificationDTO.setAdminId(adminId);
		notificationDTO.setTitle(title);
		notificationDTO.setMessage(message);
		notificationDTO.setSuccessCount(successCount);
		notificationDTO.setFailureCount(failureCount);
		notificationDTO.setNotificationType(NotificationType.INFO_NOTIFICATION.getId());
		notificationDTO.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		notificationDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		notificationDAO.addNotification(notificationDTO);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public InfoNotificationHistoryResponse getSentInfoNotifications(String authToken, int pageNumber, Locale locale)
			throws Exception {
		
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ADMIN_DOES_NOT_EXIST, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString("pageNumber : " + pageNumber));

		InfoNotificationHistoryResponse response = new InfoNotificationHistoryResponse();

		List<InfoNotificationDetails> notificationDTOList = notificationDAO.getSentInfoNotifications(pageNumber,
				NotificationType.INFO_NOTIFICATION.getId());

		for (InfoNotificationDetails notificationDTO : notificationDTOList) {
			NotificationDetails notificationDetails = new NotificationDetails();
			notificationDetails.setId(notificationDTO.getId());
			notificationDetails.setTitle(notificationDTO.getTitle());
			notificationDetails.setMessage(notificationDTO.getMessage());
			notificationDetails.setAdminName(notificationDTO.getAdminName());
			notificationDetails.setAdminId(notificationDTO.getAdminId());
			notificationDetails.setSentTime(notificationDTO.getCreatedOn().getTime());
			notificationDetails.setSuccessCount(notificationDTO.getSuccessCount());
			notificationDetails.setFailureCount(notificationDTO.getFailureCount());

			response.getNotificationHistoryList().add(notificationDetails);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER,adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
}
