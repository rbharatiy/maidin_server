package com.maidin.adminApi.admin.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.google.common.base.Strings;
import com.maidin.adminApi.admin.AdminMgmt;
import com.maidin.adminApi.enums.ImageCategory;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.RemarkStatus;
import com.maidin.adminApi.enums.ScheduleStatus;
import com.maidin.adminApi.enums.UserCategory;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.CommonApplication;
import com.maidin.adminApi.utils.OperationId;
//import com.maidin.adminApi.utils.XlsxBuilder.StyleAttribute;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.common.exception.MandatoryFieldMissingException;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.common.util.EmailSenderWithAttachment;
import com.maidin.common.util.NotificationUtils;
import com.maidin.common.util.SmsSender;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminAuthenticationDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.AdminDeviceDetailsDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.ImageDAO;
import com.maidin.persistence.dao.MaidinMoneyLedgerDAO;
import com.maidin.persistence.dao.OTPVerificationDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminAuthenticationDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.AdminDeviceDetailsDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.ImageDTO;
import com.maidin.persistence.dto.MaidinMoneyLedgerDTO;
import com.maidin.persistence.dto.OTPVerificationDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.model.OrderPaymentInfo;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.ChangeDeliveredOrderStateRequest;
import com.maidin.pojo.admin.CustomerAccountInfo;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.CustomerOrderDetails;
import com.maidin.pojo.admin.CustomerOrderInfo;
import com.maidin.pojo.admin.CustomerOrderReport;
import com.maidin.pojo.admin.CustomerRecord;
import com.maidin.pojo.admin.CustomerTimeSlot;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.admin.DeliveriesCountByDay;
import com.maidin.pojo.admin.DeliveryDetails;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.admin.OrderFormat;
import com.maidin.pojo.admin.OrderInfoDetails;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.PaymentMethod;
import com.maidin.pojo.admin.PaymentModeAmount;
import com.maidin.pojo.admin.ProductBrands;
import com.maidin.pojo.admin.ProductCategory;
import com.maidin.pojo.admin.ProductDetails;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.SearchedProductDetails;
import com.maidin.pojo.admin.TimeSlotInfo;
import com.maidin.pojo.admin.UpdateProductRequest;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.adminv2.UpsertWalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;

public class AdminMgmtImpl implements AdminMgmt {

	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ProductDAO productDAO;
	private final ProductCategoryDAO categoryDAO;
	private final ProductBrandsDAO brandDAO;
	private final OTPVerificationDAO otpVerificationDAO;
	private final AdminAuthenticationDAO adminAuthenticationDAO;
	private final AdminDeviceDetailsDAO adminDeviceDetailsDAO;
	private final CommonApplication commonApplication;
	private final PaymentModeDAO paymentModeDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final AddressDAO addressDAO;
	private final ImageDAO imageDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final CustomerDeviceDetailsDAO customerDeviceDetailsDAO;
	private final org.json.simple.JSONArray timeSlotJsonArray;
	private org.json.simple.JSONArray cityProjectJsonArray;
	private org.json.simple.JSONArray projectJsonArray;
	private org.json.simple.JSONArray paymentModeJSONArray;
	private long jsonLoadedOn;
	private final MaidinMoneyLedgerDAO maidinMoneyLedgerDAO;

	@Autowired
	public AdminMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
			AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
			ProductDAO productDAO, ProductCategoryDAO categoryDAO, ProductBrandsDAO brandDAO,
			OTPVerificationDAO otpVerificationDAO, AdminAuthenticationDAO adminAuthenticationDAO,
			AdminDeviceDetailsDAO adminDeviceDetailsDAO, CommonApplication commonApplication,
			PaymentModeDAO paymentModeDAO, TimeSlotDAO timeSlotDAO, AddressDAO addressDAO, ImageDAO imageDAO,
			CustomerTransactionDAO transactionDAO, ScheduleInfoDAO scheduleInfoDAO,
			CustomerDeviceDetailsDAO customerDeviceDetailsDAO, MaidinMoneyLedgerDAO maidinMoneyLedgerDAO) {

		this.walletDAO = walletDAO;
		this.customerDAO = customerDAO;
		this.authenticationMgmt = authenticationMgmt;
		this.adminDAO = adminDAO;
		this.orderInfoDAO = orderInfoDAO;
		this.orderDetailsInfoDAO = orderDetailsInfoDAO;
		this.productDAO = productDAO;
		this.categoryDAO = categoryDAO;
		this.brandDAO = brandDAO;
		this.otpVerificationDAO = otpVerificationDAO;
		this.adminAuthenticationDAO = adminAuthenticationDAO;
		this.adminDeviceDetailsDAO = adminDeviceDetailsDAO;
		this.commonApplication = commonApplication;
		this.paymentModeDAO = paymentModeDAO;
		this.timeSlotDAO = timeSlotDAO;
		this.addressDAO = addressDAO;
		this.imageDAO = imageDAO;
		this.transactionDAO = transactionDAO;
		this.scheduleInfoDAO = scheduleInfoDAO;
		this.customerDeviceDetailsDAO = customerDeviceDetailsDAO;
		this.timeSlotJsonArray = readTimeSlotJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.cityProjectJsonArray = readCityJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");
		this.projectJsonArray = readProjectJSON(System.getProperty("config.dir") + "/json/cityProjectTowerList.json");
		this.paymentModeJSONArray = readPaymentModeJSON(System.getProperty("config.dir") + "/json/allLists.json");
		this.jsonLoadedOn = System.currentTimeMillis();
		this.maidinMoneyLedgerDAO = maidinMoneyLedgerDAO;
	}

	private static final Logger logger = LoggerFactory.getLogger(AdminMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}

	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	private static final String fcmKeyCustomer = prop.getProperty(ResourceProperties.FCM_KEY_CUSTOMER);
	private static final int accountLockingHour = Integer
			.parseInt(prop.getProperty(ResourceProperties.ACCOUNT_LOCKING_HOUR));

	private static final int deductionLimit = Integer.parseInt(prop.getProperty(ResourceProperties.DEDUCTION_LIMIT));

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GetWalletResponse upsertWalletInfo(String authToken, UpsertWalletRequest request, Locale locale)
			throws Exception {

		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		GetWalletResponse response = new GetWalletResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		CustomerDTO customerDTO = customerDAO.getByMobileNumberWithoutActiveState(request.getMobile());
		if (customerDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CUSTOMER_DOES_NOT_EXIST, locale));
		}
		customerDTO = customerDAO.getCustomerByMobileNumber(request.getMobile());
		if (customerDTO == null) {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.CUSTOMER_ID_IS_DEACTIVATED, locale));
		}
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerDTO.getId());

		if (request.getAmount() > 0) {
			if (request.getTransactionDesciption() == WalletDescription.DEDUCTIONS.getId()) {
				request.setTransactionDesciption(WalletDescription.DISCHARGE.getId());
			}
			addCustomerTransaction(adminDTO.getId(), customerDTO.getId(), request, locale);
			if (walletDTO == null) {
				addWalletInfo(customerDTO.getId(), request, locale);

			} else {
				updateWalletInfo(walletDTO, request, locale);

			}

		}
		WalletDTO waDto = walletDAO.getByCustomerId(customerDTO.getId());
		float balance = 0;
		if (waDto != null) {
			balance = waDto.getMyWalletBalance();

		}
		response.setWalletBalance(balance);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		if (response.getStatus()) {
			if (request.getTransactionDesciption() == WalletDescription.RECHARGE.getId()) {
				CustomerDeviceDetailsDTO customerDeviceDetailsDTO = customerDeviceDetailsDAO
						.getDeviceDetailsByCustomerId(customerDTO.getId());

				JSONObject payload = commonApplication.getNotificationAndDataPayload(
						customerDeviceDetailsDTO.getDeviceToken(),
						ResourceProperties.RECHARGE_SUCCESS_NOTIFICATION_TITLE,
						ResourceProperties.RECHARGE_SUCCESS_NOTIFICATION_BODY
								+ setNumberUpToTwoDecimal(request.getAmount() + ""),
						ResourceProperties.NOTIFICATION_TYPE_RECHARGE_WALLET);
				NotificationUtils.sendPushNotificationToCustomerOnAndroid(payload, fcmKeyCustomer);
			}
		}

		// reshuffle order Payment
		if (request.getAmount() > 0 && (request.getTransactionDesciption() == (WalletDescription.RECHARGE.getId()))) {
			reshufflePayment(customerDTO.getId(), request.getAmount(), PaymentMode.ByWallet.getId(), true);
		}
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	private String setNumberUpToTwoDecimal(String number) {
		String result = "";
		int decimalIndex = number.indexOf(".");
		if (decimalIndex != -1) {
			if (number.substring(decimalIndex + 1, number.length()).length() == 1) {
				result = number.substring(0, decimalIndex + 2) + "0";
			} else {
				result = number;
			}
		} else {
			result = number + ".00";
		}
		return result;
	}

	private float reshufflePayment(Long customerId, float balance, int paymentMode, boolean isWalletAlreadyUpdated) {
		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId);
		// List<Long> orderInfoIds =
		// orderDetailsList.stream().map(OrderDetailsInfoDTO::getOrderId).distinct().collect(Collectors.toList());
		// List<OrderInfoDTO> orderInfoList =
		// orderInfoDAO.getByIds(orderInfoIds);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				transactionRequest.setRemarkStatus(RemarkStatus.DELIVERED.getId());
				float pendingAmountToPay = orderDetailsDTO.getTotalProductCost()
						- orderDetailsDTO.getAmountPaidByCustomer();
				if (balance > 0 && pendingAmountToPay > 0) {
					if (!Strings.isNullOrEmpty(orderDetailsDTO.getPayBy())) {
						if (orderDetailsDTO.getPayBy().indexOf("," + paymentMode) == -1) {
							orderDetailsDTO.setPayBy(orderDetailsDTO.getPayBy().concat("," + paymentMode));
						}
					} else {
						orderDetailsDTO.setPayBy("," + paymentMode);
					}

					orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
					if (balance >= pendingAmountToPay) {
						transactionRequest.setMoneyOut(pendingAmountToPay);
						orderDetailsDTO.setAmountPaidByCustomer(
								orderDetailsDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, -1);
/*<<<<<<< HEAD
						maidinBalance = maidinBalance - pendingAmountToPay;
						if(pendingAmountToPay == orderDetailsDTO.getTotalProductCost()){
							orderDetailsDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
						}
						else{
						List<Integer> paymentModeIdList = transactionDAO.getPaymentModeByOrderDetailsId(orderDetailsDTO.getId());
						if (paymentModeIdList != null && paymentModeIdList.size() > 0) {
							if (Collections.frequency(paymentModeIdList, paymentModeIdList.get(0)) == paymentModeIdList.size()) {
								orderDetailsDTO.setPaymentModeId(paymentModeIdList.get(0));
							} else if (!Collections.disjoint(Arrays.asList(1, 2, 3), paymentModeIdList)) {
								orderDetailsDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
							}
						}
						}
						//orderDetailsDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
=======*/
						if (!isWalletAlreadyUpdated) {
							updateWalletBalance(customerId, pendingAmountToPay, WalletDescription.RECHARGE.getName());
						}
						balance = balance - pendingAmountToPay;

					} else {
						transactionRequest.setMoneyOut(balance);
						orderDetailsDTO.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + balance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(paymentMode);
						transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
						transactionRequest.setDueAmount(pendingAmountToPay - balance);
						addCustomerTransaction(transactionRequest, -1);
/*<<<<<<< HEAD
						maidinBalance = 0;
						orderDetailsDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
=======*/
						if (!isWalletAlreadyUpdated) {
							updateWalletBalance(customerId, balance, WalletDescription.RECHARGE.getName());
						}
						balance = 0;
						// orderDetailsDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());

/*>>>>>>> refs/heads/PaymentModeAddition*/
					}
				}
				// TODO:change SetPaymentModeid
				setPaymentModeId(orderDetailsDTO.getOrderId(), paymentMode);
			}
			transactionRequest = null;
		}
		return balance;
	}
	/*
	 * private void reshufflePayment(Long customerId, float maidinBalance) {
	 * System.out.println(
	 * "Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged"
	 * ); List<OrderDetailsInfoDTO> orderDetailsList =
	 * orderDetailsInfoDAO.getUnSettledDeliveredOrders(customerId); if
	 * (orderDetailsList.size() > 0) { CustomerTransactionRequest
	 * transactionRequest = new CustomerTransactionRequest();
	 * transactionRequest.setCustomerId(customerId); for (OrderDetailsInfoDTO
	 * orderDetailsDTO : orderDetailsList) {
	 * transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
	 * transactionRequest.setOrderDetailsId(orderDetailsDTO.getId()); float
	 * pendingAmountToPay = orderDetailsDTO.getTotalProductCost() -
	 * orderDetailsDTO.getAmountPaidByCustomer(); if (maidinBalance > 0 &&
	 * pendingAmountToPay > 0) { orderDetailsDTO.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis())); if (maidinBalance >=
	 * pendingAmountToPay) { transactionRequest.setMoneyOut(pendingAmountToPay);
	 * orderDetailsDTO.setAmountPaidByCustomer(
	 * orderDetailsDTO.getAmountPaidByCustomer() + pendingAmountToPay);
	 * orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
	 * transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
	 * transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
	 * transactionRequest.setDueAmount(0);
	 * addCustomerTransaction(transactionRequest, -1); maidinBalance =
	 * maidinBalance - pendingAmountToPay;
	 * orderDetailsDTO.setPaymentModeId(PaymentMode.ByWallet.getId()); } else {
	 * transactionRequest.setMoneyOut(maidinBalance); orderDetailsDTO
	 * .setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() +
	 * maidinBalance);
	 * orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
	 * transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
	 * transactionRequest.setTransactionType(WalletDescription.SETTLED.getId());
	 * transactionRequest.setDueAmount(pendingAmountToPay - maidinBalance);
	 * addCustomerTransaction(transactionRequest, -1); maidinBalance = 0;
	 * orderDetailsDTO.setPaymentModeId(PaymentMode.
	 * PARTIAL_WALLET_PARTIAL_PENDING.getId()); } } // TODO:change
	 * SetPaymentModeid setPaymentModeId(orderDetailsDTO.getOrderId()); }
	 * transactionRequest = null; } }
	 */
	/*
	 * private void updateCustomerTransaction(CustomerTransactionRequest
	 * transactionRequest, int i) { // TODO Auto-generated method stub
	 * 
	 * }
	 */

	private void reshufflePaymentOriginal(Long customerId, float maidinBalance) {
		System.out.println("Make PaymentSettled of AllOrders Whose Payment is pending with Balance Recharged");
		List<OrderDetailsInfoDTO> orderDetailsList = orderDetailsInfoDAO.getUnSettledPaymentOrders(customerId);
		if (orderDetailsList.size() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			for (OrderDetailsInfoDTO orderDetailsDTO : orderDetailsList) {
				transactionRequest.setOrderId(orderDetailsDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsDTO.getId());
				float amountToPay = orderDetailsDTO.getTotalProductCost() - orderDetailsDTO.getAmountPaidByCustomer();
				if (maidinBalance > 0 && amountToPay > 0) {
					if (maidinBalance >= amountToPay) {
						transactionRequest.setMoneyOut(amountToPay);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + amountToPay);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, amountToPay, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = maidinBalance - amountToPay;
						// orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					} else {
						transactionRequest.setMoneyOut(maidinBalance);
						orderDetailsDTO
								.setAmountPaidByCustomer(orderDetailsDTO.getAmountPaidByCustomer() + maidinBalance);
						orderDetailsDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
						addCustomerTransaction(transactionRequest, -1);
						updateWalletBalance(customerId, maidinBalance, WalletDescription.DEDUCTIONS.getName());
						maidinBalance = 0;
						// orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
					}
				}
				// setPaymentModeId(orderDetailsDTO.getOrderId());
			}
			transactionRequest = null;
		}
	}

	/*
	 * private void setPaymentModeId(Long orderId) {OrderInfoDTO orderInfoDTO =
	 * orderInfoDAO.getById(orderId); // List<OrderDetailsInfoDTO>
	 * orderDetailsList = // orderDetailsInfoDAO.getByOrderId(orderId);
	 * List<Integer> paymentModeIdList =
	 * orderDetailsInfoDAO.getByOrderIdAndPaymentModeId(orderId); if
	 * (paymentModeIdList != null && paymentModeIdList.size() > 0) { //
	 * if(orderDetailsList.containsAll(0)) if
	 * (Collections.frequency(paymentModeIdList, paymentModeIdList.get(0)) ==
	 * paymentModeIdList.size()) {
	 * orderInfoDTO.setPaymentModeId(paymentModeIdList.get(0)); } else if
	 * (!Collections.disjoint(Arrays.asList(5, 6, 7, 8, 9), paymentModeIdList))
	 * { orderInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId()); }
	 * else if (!Collections.disjoint(Arrays.asList(1, 2, 3),
	 * paymentModeIdList)) {
	 * orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId()); }
	 * orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis())); } }
	 */

	private void setPaymentModeId(Long orderId, int paymentMode) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderId);
		OrderPaymentInfo opi = orderDetailsInfoDAO.getOrderPaymentInfo(orderId);
		if (opi != null) {
			orderInfoDTO.setOrderSubTotal(opi.getSumTotal());
			if (opi.getDifference() == 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PAYMENTSETTLED.getId());
			} else if (opi.getDifference() == opi.getSumTotal()) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PENDING.getId());
			} else if (opi.getDifference() > 0) {
				orderInfoDTO.setOverallPaymentStatus(PaymentStatus.PARTIALPAYMENT.getId());
			}
		}
		if (!Strings.isNullOrEmpty(orderInfoDTO.getPayBy())) {
			if (orderInfoDTO.getPayBy().indexOf("," + paymentMode) == -1) {
				orderInfoDTO.setPayBy(orderInfoDTO.getPayBy().concat("," + paymentMode));
			}
		} else {
			orderInfoDTO.setPayBy("," + paymentMode);
		}
		orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	}

	private void addCustomerTransaction(Long adminId, Long customerId, UpsertWalletRequest request, Locale locale)
			throws ValidationException {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(customerId);
		if (request.getTransactionDesciption() == WalletDescription.RECHARGE.getId()) {
			transactionDTO.setMoneyIn(request.getAmount());
		} else if ((request.getTransactionDesciption() == WalletDescription.DEDUCTIONS.getId())
				|| (request.getTransactionDesciption() == WalletDescription.DISCHARGE.getId())) {
			transactionDTO.setMoneyOut(request.getAmount());
		}
		transactionDTO.setPreBalance(checkMaidinBalance(customerId));
		transactionDTO.setOrderDetailsId(-1L);
		transactionDTO.setOrderId(-1L);
		transactionDTO.setPaymentModeId(2);
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(request.getTransactionDesciption());
		transactionDTO.setUpdatedBy(adminId);
		transactionDTO.setRemarks(0);
		transactionDAO.addTransaction(transactionDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public OrderReportResponse getOrderReport(OrderRequest request, String authToken, Locale locale) throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		OrderReportResponse response = new OrderReportResponse();
		// String mobile = "7982340415";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		new Thread(() -> {
			try {
				callAsynchronouslyReport(request, locale, adminDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Async
	private void callAsynchronouslyReport(OrderRequest request, Locale locale, AdminDTO adminDTO)
			throws InterruptedException, ExecutionException, JSONException {
		CustomerOrderReport reportResponse = new CustomerOrderReport();
		Calendar cal = Calendar.getInstance();

		if (request.getFromTimestamp() == request.getToTimestamp()) {
			reportResponse = generateOrderReportV2(request, locale);

		} else {
			reportResponse = generateMultipleDayOrderReport(request, locale);
		}
		try {
			if (request.getFromTimestamp() == request.getToTimestamp() && request.getTimeSlot() != 0) {

				convertSingleSlotOrderReportToExcel(request, reportResponse, adminDTO);
			} else {
				convertOrderReportToExcel(request, reportResponse, adminDTO.getEmail());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private CustomerOrderReport generateMultipleDayOrderReport(OrderRequest request, Locale locale)
			throws InterruptedException, ExecutionException {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Date> dateList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			dateList = orderInfoDAO.getCustomersSortedByDeliveryDate(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			dateList = orderInfoDAO.getCustomersInTimeSlotSortedByDeliveryDate(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		// final List<Date> dateList1 = dateList;
		//////////////////////////////////////////////////////////////////////////

		ExecutorService exec = Executors.newFixedThreadPool(5);
		try {
			List<Future<?>> futures = new ArrayList<Future<?>>(dateList.size());
			for (Date date : dateList) {
				logger.info(ApiUtils.dateWithMonthNameFormat(new Timestamp(date.getTime())));
				final Runnable callableTask = new Runnable() {
					public void run() {
						getCustomerRecordsByDate(date, request, response);
					}
				};
				futures.add(exec.submit(callableTask));

			}
			for (Future<?> f : futures) {
				f.get(); // wait for a processor to complete
			}
			logger.info("all items processed");
		} catch (Exception e) {
			exec.shutdown();
		} finally {
			exec.shutdown();
		}

		/////////////////////////////////////////////////////////////////////////////////

		return response;

	}

	private CustomerOrderReport getCustomerRecordsByDate(Date date, OrderRequest request,
			CustomerOrderReport response) {

		// for (Date date : dateList) {
		// logger.info(ApiUtils.dateWithMonthNameFormat(new
		// Timestamp(date.getTime())));

		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getByDeliveryDates(date);

		for (OrderInfoDTO orderInfo : orderInfoList) {
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(orderInfo.getCustomerId());

			CustomerDTO customerDTO = customerDAO.getById(orderInfo.getCustomerId());
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			WalletDTO walletDTO = walletDAO.getByCustomerId(orderInfo.getCustomerId());
			float maidinMoney = 0;
			if (walletDTO != null) {
				maidinMoney = walletDTO.getMyWalletBalance();
			}
			TimeSlotInfo timeSlot = new TimeSlotInfo();

			logger.info("Customer name : " + customerDTO.getFullName() + " ; Id : " + customerDTO.getId());

			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getSettledOrderDetailsByOrderId(orderInfo.getId());

			PaymentModeDTO paymentMode = paymentModeDAO.getById(orderInfo.getPaymentModeId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {

				OrderFormat orderRow = new OrderFormat();
				if (customerDTO != null) {
					orderRow.setName(customerDTO.getFullName());
					orderRow.setProject(addressDTO.getProject());
					orderRow.setTower(addressDTO.getTower());
					orderRow.setFlat(addressDTO.getFlat());
					orderRow.setMobile(customerDTO.getMobileNumber());
					orderRow.setEmail(customerDTO.getEmail());
					orderRow.setDeliverySlot(orderInfo.getTimeSlot());
					ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
					if (productDTO == null)
						continue;
					orderRow.setProduct(productDTO.getProductName());
					orderRow.setUnit(productDTO.getQuantityUnit());
					ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
					orderRow.setBrand(brandDTO.getBrand());
					ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
					if (categoryDTO == null)
						continue;
					orderRow.setCategory(categoryDTO.getCategory());
					orderRow.setQuantity(orderDetailsInfo.getQuantity());
					float billAmount = 0;
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
						billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
					} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						billAmount = (orderDetailsInfo.getProductMRP() > 0 ? orderDetailsInfo.getProductMRP()
								: productDTO.getPrice()) * orderDetailsInfo.getQuantity();
					}
					orderRow.setBillAmount(billAmount);
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						if (maidinMoney > billAmount) {
							maidinMoney = maidinMoney - billAmount;
							orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
						} else if (maidinMoney > 0 && billAmount > maidinMoney) {
							orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
							maidinMoney = 0;
						} else {
							orderRow.setPaymentMode(PaymentMode.ByCash.getName());
						}
					} else {
						if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
							orderRow.setPaymentMode("NA");
						} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {

							if (paymentMode == null) {
								orderRow.setPaymentMode("Payment Not linked To Order");
							} else {
								orderRow.setPaymentMode(paymentMode.getPaymentMode());
							}

						}
					}
					orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
					orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
					orderRow.setOrderedTime(ApiUtils.dateTimeFormat(orderInfo.getOrderedTime()));
					timeSlot.getOrderFormatList().add(orderRow);

				}
			}
			customerOrder.getTimeSlotId().add(timeSlot);
			response.getCustomerOrderInfo().add(customerOrder);
		}
		// }
		return response;
	}

	/*
	 * private CustomerOrderReport getOrderDetailsByDate(Date date, OrderRequest
	 * request, CustomerOrderReport response) { try { // logger.info(
	 * "Customer name: " + customerDAO.getById(customerId).getFullName() +
	 * " , Id: " // + customerDAO.getById(customerId).getId());
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } Long customerId =
	 * WalletDTO walletDTO = walletDAO.getByCustomerId(customerId); float
	 * maidinMoney = 0; if (walletDTO != null) { maidinMoney =
	 * walletDTO.getMyWalletBalance(); } CustomerOrderInfo customerOrder = new
	 * CustomerOrderInfo(); customerOrder.setCustomerId(customerId); CustomerDTO
	 * customerDTO = customerDAO.getById(customerId); if (customerDTO == null) {
	 * return null; } AddressDTO addressDTO =
	 * addressDAO.getById(customerDTO.getAddressId()); if (addressDTO == null) {
	 * return null; } List<OrderInfoDTO> orderInfoDTOList = null; TimeSlotInfo
	 * timeSlot = new TimeSlotInfo(); orderInfoDTOList =
	 * orderInfoDAO.getByCustomerIdDeliveryDate(customerId, new
	 * Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
	 * for (OrderInfoDTO orderInfo : orderInfoDTOList) {
	 * List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
	 * .getSettledOrderDetailsByOrderId(orderInfo.getId());
	 * 
	 * PaymentModeDTO paymentMode =
	 * paymentModeDAO.getById(orderInfo.getPaymentModeId()); for
	 * (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
	 * OrderFormat orderRow = new OrderFormat(); if (customerDTO != null) {
	 * orderRow.setName(customerDTO.getFullName());
	 * orderRow.setProject(addressDTO.getProject());
	 * orderRow.setTower(addressDTO.getTower());
	 * orderRow.setFlat(addressDTO.getFlat());
	 * orderRow.setMobile(customerDTO.getMobileNumber());
	 * orderRow.setEmail(customerDTO.getEmail());
	 * orderRow.setDeliverySlot(orderInfo.getTimeSlot()); ProductDTO productDTO
	 * = productDAO.getById(orderDetailsInfo.getProductId()); if (productDTO ==
	 * null) continue; orderRow.setProduct(productDTO.getProductName());
	 * orderRow.setUnit(productDTO.getQuantityUnit()); ProductBrandsDTO brandDTO
	 * = brandDAO.getById(productDTO.getBrandId());
	 * orderRow.setBrand(brandDTO.getBrand()); ProductCategoryDTO categoryDTO =
	 * categoryDAO.getById(brandDTO.getCategoryId()); if (categoryDTO == null)
	 * continue; orderRow.setCategory(categoryDTO.getCategory());
	 * orderRow.setQuantity(orderDetailsInfo.getQuantity()); float billAmount =
	 * 0; if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName()
	 * )) { billAmount = orderDetailsInfo.getProductMRP() *
	 * orderDetailsInfo.getQuantity(); } else if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName()
	 * )) { billAmount = (orderDetailsInfo.getProductMRP() > 0 ?
	 * orderDetailsInfo.getProductMRP() : productDTO.getPrice()) *
	 * orderDetailsInfo.getQuantity(); } orderRow.setBillAmount(billAmount); if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName()
	 * )) { if (maidinMoney >= billAmount) { maidinMoney = maidinMoney -
	 * billAmount; orderRow.setPaymentMode(PaymentMode.ByWallet.getName()); }
	 * else if (maidinMoney > 0 && billAmount > maidinMoney) {
	 * orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
	 * maidinMoney = 0; } else {
	 * orderRow.setPaymentMode(PaymentMode.ByCash.getName()); } } else { if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName
	 * ())) { orderRow.setPaymentMode("NA"); } else if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName()
	 * )) {
	 * 
	 * if (paymentMode == null) { orderRow.setPaymentMode(
	 * "Payment Not linked To Order"); } else {
	 * orderRow.setPaymentMode(paymentMode.getPaymentMode()); }
	 * 
	 * } } orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
	 * orderRow.setOrderedTime(ApiUtils.dateTimeTwelveHourFormat(orderInfo.
	 * getOrderedTime()));
	 * orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
	 * timeSlot.getOrderFormatList().add(orderRow);
	 * 
	 * } } }
	 * 
	 * customerOrder.getTimeSlotId().add(timeSlot);
	 * response.getCustomerOrderInfo().add(customerOrder); return response; }
	 */

	private CustomerOrderReport getCustomerOrderDetails(Long customerId, OrderRequest request,
			CustomerOrderReport response) {
		try {
			logger.info("Customer name: " + customerDAO.getById(customerId).getFullName() + " , Id: "
					+ customerDAO.getById(customerId).getId());

		} catch (Exception e) {
			e.printStackTrace();
		}
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		float maidinMoney = 0;
		if (walletDTO != null) {
			maidinMoney = walletDTO.getMyWalletBalance();
		}
		CustomerOrderInfo customerOrder = new CustomerOrderInfo();
		customerOrder.setCustomerId(customerId);
		CustomerDTO customerDTO = customerDAO.getById(customerId);
		if (customerDTO == null) {
			return null;
		}
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		if (addressDTO == null) {
			return null;
		}
		List<OrderInfoDTO> orderInfoDTOList = null;
		TimeSlotInfo timeSlot = new TimeSlotInfo();
		orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDate(customerId, new Date(request.getFromTimestamp()),
				new Date(request.getToTimestamp()));
		for (OrderInfoDTO orderInfo : orderInfoDTOList) {
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getSettledOrderDetailsByOrderId(orderInfo.getId());

			PaymentModeDTO paymentMode = paymentModeDAO.getById(orderInfo.getPaymentModeId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
				OrderFormat orderRow = new OrderFormat();
				if (customerDTO != null) {
					orderRow.setName(customerDTO.getFullName());
					orderRow.setProject(addressDTO.getProject());
					orderRow.setTower(addressDTO.getTower());
					orderRow.setFlat(addressDTO.getFlat());
					orderRow.setMobile(customerDTO.getMobileNumber());
					orderRow.setEmail(customerDTO.getEmail());
					orderRow.setDeliverySlot(orderInfo.getTimeSlot());
					ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
					if (productDTO == null)
						continue;
					orderRow.setProduct(productDTO.getProductName());
					orderRow.setUnit(productDTO.getQuantityUnit());
					ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
					orderRow.setBrand(brandDTO.getBrand());
					ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
					if (categoryDTO == null)
						continue;
					orderRow.setCategory(categoryDTO.getCategory());
					orderRow.setQuantity(orderDetailsInfo.getQuantity());
					float billAmount = 0;
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
						billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
					} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						billAmount = (orderDetailsInfo.getProductMRP() > 0 ? orderDetailsInfo.getProductMRP()
								: productDTO.getPrice()) * orderDetailsInfo.getQuantity();
					}
					orderRow.setBillAmount(billAmount);
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						if (maidinMoney >= billAmount) {
							maidinMoney = maidinMoney - billAmount;
							orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
						} else if (maidinMoney > 0 && billAmount > maidinMoney) {
							orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
							maidinMoney = 0;
						} else {
							orderRow.setPaymentMode(PaymentMode.ByCash.getName());
						}
					} else {
						if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
							orderRow.setPaymentMode("NA");
						} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {

							if (paymentMode == null) {
								orderRow.setPaymentMode("Payment Not linked To Order");
							} else {
								orderRow.setPaymentMode(paymentMode.getPaymentMode());
							}

						}
					}
					orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
					orderRow.setOrderedTime(ApiUtils.dateTimeTwelveHourFormat(orderInfo.getOrderedTime()));
					orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
					timeSlot.getOrderFormatList().add(orderRow);

				}
			}
		}

		customerOrder.getTimeSlotId().add(timeSlot);
		response.getCustomerOrderInfo().add(customerOrder);
		return response;
	}

	private CustomerOrderReport generateMultipleDayOrderReport1(OrderRequest request, Locale locale) {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			customerList = orderInfoDAO.getDifferentCustomersByTimestamp(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlotId(
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		for (Long customerId : customerList) {
			WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
			float maidinMoney = 0;
			if (walletDTO != null) {
				maidinMoney = walletDTO.getMyWalletBalance();
			}
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				continue;
			}
			List<OrderInfoDTO> orderInfoDTOList = null;
			TimeSlotInfo timeSlot = new TimeSlotInfo();
			orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDate(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (OrderInfoDTO orderInfo : orderInfoDTOList) {
				List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfo.getId());

				PaymentModeDTO paymentMode = paymentModeDAO.getById(orderInfo.getPaymentModeId());
				for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
					OrderFormat orderRow = new OrderFormat();
					if (customerDTO != null) {
						orderRow.setName(customerDTO.getFullName());
						orderRow.setProject(addressDTO.getProject());
						orderRow.setTower(addressDTO.getTower());
						orderRow.setFlat(addressDTO.getFlat());
						orderRow.setMobile(customerDTO.getMobileNumber());
						orderRow.setEmail(customerDTO.getEmail());
						orderRow.setDeliverySlot(orderInfo.getTimeSlot());
						ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
						if (productDTO == null)
							continue;
						orderRow.setProduct(productDTO.getProductName());
						orderRow.setUnit(productDTO.getQuantityUnit());
						ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
						orderRow.setBrand(brandDTO.getBrand());
						ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
						if (categoryDTO == null)
							continue;
						orderRow.setCategory(categoryDTO.getCategory());
						orderRow.setQuantity(orderDetailsInfo.getQuantity());
						float billAmount = 0;
						if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
							billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
						} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
							billAmount = (orderDetailsInfo.getProductMRP() > 0 ? orderDetailsInfo.getProductMRP()
									: productDTO.getPrice()) * orderDetailsInfo.getQuantity();
						}
						orderRow.setBillAmount(billAmount);
						if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
							if (maidinMoney > billAmount) {
								maidinMoney = maidinMoney - billAmount;
								orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
							} else if (maidinMoney > 0 && billAmount > maidinMoney) {
								orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
								maidinMoney = 0;
							} else {
								orderRow.setPaymentMode(PaymentMode.ByCash.getName());
							}
						} else {
							if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
								orderRow.setPaymentMode("NA");
							} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {

								if (paymentMode == null) {
									orderRow.setPaymentMode("Payment Not linked To Order");
								} else {
									orderRow.setPaymentMode(paymentMode.getPaymentMode());
								}

							}
						}
						orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
						orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
						timeSlot.getOrderFormatList().add(orderRow);

					}
				}
			}

			customerOrder.getTimeSlotId().add(timeSlot);
			response.getCustomerOrderInfo().add(customerOrder);
		}

		return response;

	}

	/*
	 * private OrderReportResponse generateOrderReport(OrderRequest request,
	 * Locale locale) { OrderReportResponse response = new
	 * OrderReportResponse(); List<Long> customerList = null; TimeSlotDTO
	 * timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot()); if
	 * (request.getTimeSlot() == 0) { customerList =
	 * orderInfoDAO.getDifferentCustomersByTimestamp(new
	 * Date(request.getFromTimestamp()), new Date(request.getToTimestamp())); }
	 * else {
	 * 
	 * customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlot(
	 * new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()),
	 * timeSlotDTO.getTimeSlot()); }
	 * 
	 * for (Long customerId : customerList) { WalletDTO walletDTO =
	 * walletDAO.getByCustomerId(customerId); float checkWalletBalance = 0; if
	 * (walletDTO != null) { checkWalletBalance =
	 * walletDTO.getMyWalletBalance(); } CustomerOrderInfo customerOrder = new
	 * CustomerOrderInfo(); int orderCountPerCustomer = 0; // float maidinMoney
	 * = 0.0f; float totalCustomerHadToPaid = 0.0f; // float cashNeedTCollect =
	 * 0.0f;
	 * 
	 * customerOrder.setCustomerId(customerId); List<OrderInfoDTO>
	 * orderInfoDTOList = null; if (request.getTimeSlot() == 0) {
	 * orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDate(customerId,
	 * new Date(request.getFromTimestamp()), new
	 * Date(request.getToTimestamp())); } else { orderInfoDTOList =
	 * orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlot(customerId, new
	 * Date(request.getFromTimestamp()), new Date(request.getToTimestamp()),
	 * timeSlotDTO.getTimeSlot()); } for (OrderInfoDTO orderInfo :
	 * orderInfoDTOList) {
	 * 
	 * List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
	 * .getSettledOrderDetailsByOrderId(orderInfo.getId());
	 * 
	 * orderCountPerCustomer = orderCountPerCustomer +
	 * orderDetailsInfoList.size();
	 * 
	 * for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
	 * OrderFormat orderRow = new OrderFormat(); CustomerDTO customerDTO =
	 * customerDAO.getById(orderInfo.getCustomerId()); if (customerDTO != null)
	 * { orderRow.setName(customerDTO.getFullName()); AddressDTO addressDTO =
	 * addressDAO.getById(customerDTO.getAddressId());
	 * orderRow.setProject(addressDTO.getProject());
	 * orderRow.setTower(addressDTO.getTower());
	 * orderRow.setFlat(addressDTO.getFlat());
	 * orderRow.setMobile(customerDTO.getMobileNumber());
	 * orderRow.setEmail(customerDTO.getEmail());
	 * orderRow.setDeliverySlot(orderInfo.getTimeSlot()); ProductDTO productDTO
	 * = productDAO.getById(orderDetailsInfo.getProductId()); if (productDTO ==
	 * null) continue; orderRow.setProduct(productDTO.getProductName());
	 * orderRow.setUnit(productDTO.getQuantityUnit()); ProductBrandsDTO brandDTO
	 * = brandDAO.getById(productDTO.getBrandId());
	 * orderRow.setBrand(brandDTO.getBrand()); ProductCategoryDTO categoryDTO =
	 * categoryDAO.getById(brandDTO.getCategoryId()); if (categoryDTO == null)
	 * continue; orderRow.setCategory(categoryDTO.getCategory());
	 * orderRow.setQuantity(orderDetailsInfo.getQuantity()); float billAmount =
	 * (orderDetailsInfo.getProductMRP() > 0 ? orderDetailsInfo.getProductMRP()
	 * : productDTO.getPrice()) * orderDetailsInfo.getQuantity(); if
	 * (!orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.
	 * getName())) { orderRow.setBillAmount(billAmount); } PaymentModeDTO
	 * modeDTO = paymentModeDAO.getById(orderInfo.getPaymentModeId()); if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName()
	 * )) { // if (modeDTO == null || modeDTO.getId() == 2) {
	 * 
	 * if(orderDetailsInfo.getOrderStatus().equals(
	 * OrderStatus.UNDELIVERED.getName())){ orderRow.setPaymentMode("NA"); }
	 * else
	 * 
	 * if (checkWalletBalance > billAmount) { checkWalletBalance =
	 * checkWalletBalance - billAmount;
	 * 
	 * orderRow.setPaymentMode("ByWallet");
	 * 
	 * } else if (checkWalletBalance > 0 && billAmount > checkWalletBalance) {
	 * orderRow.setPaymentMode("ByWallet And ByCash");
	 * 
	 * checkWalletBalance = 0; } else { orderRow.setPaymentMode("ByCash");
	 * 
	 * } // } } else { if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName
	 * ())) { orderRow.setPaymentMode("NA"); } else { if (modeDTO != null) {
	 * orderRow.setPaymentMode(modeDTO.getPaymentMode()); } } }
	 * 
	 * orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
	 * orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
	 * orderRow.setOrderedTime(ApiUtils.dateTimeFormat(orderInfo.getOrderedTime(
	 * ))); customerOrder.getOrderFormatList().add(orderRow); if
	 * (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName()
	 * )) { totalCustomerHadToPaid = totalCustomerHadToPaid +
	 * orderRow.getBillAmount(); } } //
	 * response.getCustomerOrderInfo().add(customerOrder) }
	 * 
	 * } customerOrder.setOrderCount(orderCountPerCustomer); // WalletDTO
	 * walletDTO = walletDAO.getByCustomerId(customerId); float walletBalance =
	 * walletDTO != null ? walletDTO.getMyWalletBalance() : 0;
	 * customerOrder.setMaidinMoney(walletBalance);
	 * customerOrder.setPaidBymaidinMoney( walletBalance >=
	 * totalCustomerHadToPaid ? totalCustomerHadToPaid : walletBalance);
	 * customerOrder.setCashNeedTCollect( walletBalance >=
	 * totalCustomerHadToPaid ? 0 : (totalCustomerHadToPaid - walletBalance));
	 * response.getCustomerOrderInfo().add(customerOrder); }
	 * 
	 * return response; // return response; }
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenerateMobileOtpResponse initiateGenerateMobileOTP(GenerateMobileOtp request, Locale locale)
			throws Exception {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		GenerateMobileOtpResponse response = new GenerateMobileOtpResponse();
		// entityCommon.checkMobileNumber(request.getMobileNumber(), locale);
		String code = CodeGenerator.generateCode();

		String message = "Your unique verification code for MAIDIN is " + code;
		OTPVerificationDTO OTPdto = otpVerificationDAO.getByMobile(request.getMobileNumber(),
				UserCategory.ADMIN.getName());

		if (OTPdto == null) {
			OTPVerificationDTO dto = new OTPVerificationDTO();
			dto.setMobileNumber(request.getMobileNumber());
			dto.setOtp(code);
			dto.setGeneratedOn(new Timestamp(System.currentTimeMillis()));
			dto.setUsercategory(UserCategory.ADMIN.getName());
			otpVerificationDAO.saveOTPInfo(dto);
			SmsSender.sendVerifySMS(request.getMobileNumber(), message);
			System.out.println("OTP : " + code);
		} else {
			DateTime codeExpireTime = new DateTime(OTPdto.getGeneratedOn()).plusMinutes(15);
			if (validateRecoveryCodeTime(codeExpireTime, locale)) {
				message = "Your unique verification code for MAIDIN is " + OTPdto.getOtp();
				SmsSender.sendVerifySMS(request.getMobileNumber(), message);
				System.out.println("OTP : " + code);
			} else {
				code = CodeGenerator.generateCode();
				message = "Your unique verification code for MAIDIN is " + code;
				OTPdto.setOtp(code);
				OTPdto.setGeneratedOn(new Timestamp(System.currentTimeMillis()));
				OTPdto.setUsercategory(UserCategory.ADMIN.getName());
				otpVerificationDAO.updateOTPInfo(OTPdto);
				SmsSender.sendVerifySMS(request.getMobileNumber(), message);
				System.out.println("OTP : " + code);
			}

		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public VerifyMobileOtpResponse initiateVerifyMobileOtp(VerifyMobileOtp request, Locale locale) throws Exception {

		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));

		VerifyMobileOtpResponse response = new VerifyMobileOtpResponse();
		// checkConstraint(request, locale);
		OTPVerificationDTO dto = otpVerificationDAO.getByMobile(request.getMobileNumber(),
				UserCategory.ADMIN.getName());
		if (dto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_MOBILE_NUMBER, locale));
		} else {
			OTPVerificationDTO otpdto = otpVerificationDAO.getByMobileAndOtp(request.getMobileNumber(),
					request.getOtpCode(), UserCategory.ADMIN.getName());
			if (otpdto == null) {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_OTP, locale));
			} else {
				String codeInDB = otpdto.getOtp();
				DateTime codeExpireTime = new DateTime(otpdto.getGeneratedOn()).plusMinutes(15);
				validateRecoveryCode(request.getOtpCode(), codeInDB, codeExpireTime, locale);
				String authToken = authenticationMgmt.generateAuthToken(request.getMobileNumber());
				AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(request.getMobileNumber());
				AdminAuthenticationDTO adminAuthDTO = adminAuthenticationDAO
						.getByMobileNumber(request.getMobileNumber());
				if (adminAuthDTO == null) {
					AdminAuthenticationDTO adminAuth = new AdminAuthenticationDTO();
					if (adminDTO != null) {
						if (!adminDTO.isActive()) {
							throw new ValidationException(ExceptionResourceBundle
									.getExceptionCodeProperties(ExceptionCode.ADMIN_ID_IS_DEACTIVATED, locale));
						}
						adminAuth.setAdminId(adminDTO.getId());
					}
					adminAuth.setMobileNumber(request.getMobileNumber());
					adminAuth.setAuthToken(authToken);
					adminAuthenticationDAO.addToken(adminAuth);
				} else {
					authenticationMgmt.removeAuthToken(adminAuthDTO.getAuthToken());
					if (adminDTO != null) {
						adminAuthDTO.setAdminId(adminDTO.getId());
					}
					adminAuthDTO.setMobileNumber(request.getMobileNumber());
					adminAuthDTO.setAuthToken(authToken);
					adminAuthenticationDAO.updateToken(adminAuthDTO);
				}
				response.setAuthToken(authToken);
				otpVerificationDAO.deleteInfo(otpdto);
				AdminDTO admindto = adminDAO.getByMobileNumber(request.getMobileNumber());
				if (admindto != null) {
					response.setAdminId(admindto.getId());
					response.setIsRegistered(true);
					response.setAdminName(admindto.getFullName());
					AdminDeviceDetailsDTO deviceDTO = adminDeviceDetailsDAO.getDeviceDetailsByAdminId(admindto.getId());
					if (deviceDTO != null) {
						if (request.getAppToken() != null) {
							if (deviceDTO.getAppToken() != null
									&& !(request.getAppToken().equals(deviceDTO.getAppToken()))) {
								JSONObject payloadJSON = new JSONObject();
								payloadJSON.put("notificationType", prop.getProperty(ResourceProperties.LOGOUT));
								JSONObject payload = commonApplication.getPayloadDataOnly(deviceDTO.getDeviceToken(),
										payloadJSON);
								commonApplication.sendPushNotificationDataToCustomer(payload, fcmKeyCustomer,
										admindto.getId(), UserCategory.ADMIN.getName());
							}
							deviceDTO.setAppToken(request.getAppToken());
							deviceDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
							adminDeviceDetailsDAO.updateDeviceDetails(deviceDTO);
						}
					} else {
						AdminDeviceDetailsDTO detailsDTO = new AdminDeviceDetailsDTO();
						detailsDTO.setAdminId(admindto.getId());
						if (request.getAppToken() != null) {
							detailsDTO.setAppToken(request.getAppToken());
						}
						detailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
						adminDeviceDetailsDAO.addDeviceDetails(detailsDTO);
					}
				} else {
					response.setIsRegistered(false);
				}

			}
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RegenerateAuthTokenResponse regenAuthToken(RegenerateAuthToken request, Locale locale) throws Exception {

		Long adminId = Long.parseLong(request.getAdminId());
		RegenerateAuthTokenResponse response = new RegenerateAuthTokenResponse();
		AdminDTO tdto = adminDAO.getByIdAndMobileNumber(adminId, request.getMobileNumber());
		if (tdto == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ADMIN_DOES_NOT_EXIST, locale));
		} else {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, tdto.getId(), ApiUtils.toJsonString(request));
			String authToken = authenticationMgmt.generateAuthToken(request.getMobileNumber());
			logger.info("New AuthToken generated for adminId " + adminId + ": ", authToken);
			AdminDTO adminDTO = adminDAO.getByMobileNumber(request.getMobileNumber());
			AdminAuthenticationDTO adminAuthDTO = adminAuthenticationDAO.getByMobileNumber(request.getMobileNumber());
			if (adminAuthDTO == null) {
				addAdminAuthenticationInfo(adminDTO, request.getMobileNumber(), authToken);
			} else {
				logger.info("AuthToken is going to remove for adminId " + adminId + " : ", adminAuthDTO.getAuthToken());
				authenticationMgmt.removeAuthToken(adminAuthDTO.getAuthToken());
				updateAdminAuthenticationInfo(adminDTO, adminAuthDTO, request.getMobileNumber(), authToken);

			}
			response.setAuthToken(authToken);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, tdto.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public DeviceTokenResponse saveDeviceToken(DeviceToken request, String authToken, Locale locale) throws Exception {

		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);

		DeviceTokenResponse response = new DeviceTokenResponse();
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.ADMIN_DOES_NOT_EXIST, locale));
		}
		if (adminDTO.getId() > 0) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
			response.setAdminId(adminDTO.getId());
			AdminDeviceDetailsDTO devicedto = adminDeviceDetailsDAO.getDeviceDetailsByAdminId(adminDTO.getId());
			if (devicedto == null) {
				Long id = addAdminDeviceDetails(request, adminDTO.getId());
				response.setId(id);
			} else {
				updateAdminDeviceDetails(request, devicedto, adminDTO.getId());
				response.setId(devicedto.getId());
			}
			response.setAdminId(adminDTO.getId());
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public AddressInfoResponse getCustomerAddressInfo(String authToken, AddressInfoRequest request, Locale locale)
			throws Exception {
		AddressInfoResponse response = new AddressInfoResponse();

		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));

		if (Strings.isNullOrEmpty(request.getMobile())) {
			AddressDTO addressDTO = addressDAO.getByRegisteredFlatId(request.getFlatId());
			if (addressDTO == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.USER_NOT_REGISTERED_WITH_ADDRESS, locale));
			}
			List<Long> mobileList = customerDAO.getByAddressId(addressDTO.getId());
			for (Long customerId : mobileList) {
				CustomerAccountInfo customerAccountInfo = new CustomerAccountInfo();
				CustomerDTO customerDTO = customerDAO.getById(customerId);
				if (customerDTO != null) {
					customerAccountInfo.setName(customerDTO.getFullName());
					customerAccountInfo.setMobile(customerDTO.getMobileNumber());
					WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
					customerAccountInfo.setWalletBalance(walletDTO == null ? 0.0f : walletDTO.getMyWalletBalance());
					response.getCustomerAccountList().add(customerAccountInfo);
				}

			}

		} else {
			CustomerDTO customerDTO2 = customerDAO.getCustomerByMobileNumber(request.getMobile());
			if (customerDTO2 == null) {
				throw new ValidationException(ExceptionResourceBundle
						.getExceptionCodeProperties(ExceptionCode.USER_NOT_REGISTERED_WITH_MOBILE, locale));
			}
			CustomerAccountInfo customerAccountInfo = new CustomerAccountInfo();
			customerAccountInfo.setName(customerDTO2.getFullName());
			customerAccountInfo.setMobile(customerDTO2.getMobileNumber());
			WalletDTO walletDTO = walletDAO.getByCustomerId(customerDTO2.getId());
			customerAccountInfo.setWalletBalance(walletDTO == null ? 0.0f : walletDTO.getMyWalletBalance());
			response.getCustomerAccountList().add(customerAccountInfo);
			AddressDTO addressDTO = addressDAO.getById(customerDTO2.getAddressId());
			response.setCity(addressDTO.getCity());
			response.setProject(addressDTO.getProject());
			response.setTower(addressDTO.getTower());
			response.setFlat(addressDTO.getFlat());

		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));

		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public CustomerCountInfoResponse getCustomerCountInfo(String authToken, Locale locale) throws Exception {
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();
		// String mobile = "7307345033";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		}
		response.setFlatUserCount(customerDAO.getUsersByFlatCount(true));
		response.setRegisteredUserCount(customerDAO.getRegisteredUserCount(true));
		// response.setTotalMaidinMoneyInUsersWallet(walletDAO.getTotalMadinMoney());
		response.setTotalPositiveMaidinMoney(walletDAO.getPositiveMadinMoney());
		response.setTotalNegativeMaidinMoney(walletDAO.getNegativeMadinMoney());
		MaidinMoneyLedgerDTO mDTO = maidinMoneyLedgerDAO.getByPreviousDay();
		if (mDTO != null) {
			response.setTotalNegativeMaidinMoneyPreviousDay(mDTO.getNegativeBalance());
			response.setTotalPositiveMaidinMoneyPreviousDay(mDTO.getPositiveBalance());
		}
		response.getDeliveriesCountByDay().addAll(getDeliveryOrders(locale));
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	// By Pushpinder
	private List<DeliveriesCountByDay> getDeliveryOrders(Locale locale) {

		List<DeliveriesCountByDay> response = new ArrayList<DeliveriesCountByDay>();

		String[] dates = { Date.valueOf(LocalDate.now().minusDays(1L)).toString(),
				Date.valueOf(LocalDate.now()).toString(), Date.valueOf(LocalDate.now().plusDays(1L)).toString() };

		for (String date : dates) {
			long noOfDeliveries = orderInfoDAO.getTotalCountByDeliveryDate(
					Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			long noOfTotalDeliveriesExcludingUndeliveredOrders = orderInfoDAO
					.getTotalCountByDeliveryDateExcludingUndelivered(
							Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			long noOfMarkedDeliveries = orderInfoDAO.getTotalMarkedDeliveries(
					Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			int noOfCancelledDeliveries = 0;

			List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
					.getOrdersByYTT(Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			double subTotal = 0;
			for (OrderDetailsInfoDTO dto : orderDetailsInfoDTOList) {
				if (dto.getProductMRP() != 0) {
					subTotal += dto.getProductMRP() * dto.getQuantity();
				} else {
					ProductDTO productDTO = productDAO.getById(dto.getProductId());
					subTotal += productDTO.getPrice() * dto.getQuantity();
				}
			}

			long timeStamp = 0;
			try {
				timeStamp = new SimpleDateFormat("yyyy-MM-dd").parse(date).getTime();
			} catch (ParseException e) {
			}
			DeliveriesCountByDay deliveries_Y = new DeliveriesCountByDay();
			deliveries_Y.setNoOfTotalDeliveries(noOfDeliveries);
			deliveries_Y.setNoOfTotalUndeliveredOrders(noOfTotalDeliveriesExcludingUndeliveredOrders - noOfDeliveries);
			deliveries_Y.setNoOfTotalMarkedDeliveries(
					noOfMarkedDeliveries - (noOfTotalDeliveriesExcludingUndeliveredOrders - noOfDeliveries));
			deliveries_Y.setNoOfCancelledDeliveries(noOfCancelledDeliveries);
			deliveries_Y.setTimestamp(timeStamp);
			deliveries_Y.setTotalSale(subTotal);
			response.add(deliveries_Y);
		}
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CustomerCountInfoResponse getCustomerCountByParameters(String authToken, CustomerCountRequest request,
			Locale locale) {
		logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD, ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse changeFlag(String authToken, ItemRequest request, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();

		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		if (request.getCategoryId() > 0) {
			ProductCategoryDTO categoryDTO = categoryDAO.getById(request.getCategoryId());
			if (categoryDTO != null) {
				categoryDTO.setActive(request.getFlag() == 0 ? false : true);
				categoryDAO.updateCategory(categoryDTO);
			}
		} else if (request.getBrandId() > 0) {
			ProductBrandsDTO brandDTO = brandDAO.getById(request.getBrandId());
			if (brandDTO != null) {
				brandDTO.setActive(request.getFlag() == 0 ? false : true);
				brandDAO.updateBrand(brandDTO);
			}
		} else if (request.getProductId() > 0) {
			ProductDTO productDTO = productDAO.getById(request.getProductId());
			if (productDTO != null) {
				productDTO.setActive(request.getFlag() == 0 ? false : true);
				productDAO.updateProduct(productDTO);
			}
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public CategoryListResponse
	 * getCategory(String authToken, Locale locale) throws Exception {
	 * CategoryListResponse response = new CategoryListResponse(); String mobile
	 * = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if (adminDTO !=
	 * null) { logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
	 * authToken); } List<ProductCategoryDTO> categoryList =
	 * categoryDAO.getAllCategoriesWithoutActiveState(); for (ProductCategoryDTO
	 * pCategoryDTO : categoryList) { ProductCategory pcCategory = new
	 * ProductCategory(); pcCategory.setCategoryId(pCategoryDTO.getId());
	 * pcCategory.setProductCategory(pCategoryDTO.getCategory());
	 * pcCategory.setDescription(pCategoryDTO.getDescription());
	 * pcCategory.setActiveStatus(pCategoryDTO.isActive());
	 * pcCategory.setStockStatus(true); pcCategory.setDemandStatus(true);
	 * ImageDTO imageDTO =
	 * imageDAO.getByProductId(ImageCategory.CATEGORY.getName(),
	 * pCategoryDTO.getId().longValue()); if (imageDTO != null &&
	 * imageDTO.getUpdatedOn() != null) {
	 * pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime()); }
	 * response.getCategoryList().add(pcCategory); } response =
	 * ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES); logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response)); return response; }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public CategoryListResponse getCategory(String authToken, Locale locale) throws Exception {
		CategoryListResponse response = new CategoryListResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD, adminDTO.getId());
		}
		List<ProductCategoryDTO> categoryList = categoryDAO.getAllCategoriesWithoutActiveState();
		for (ProductCategoryDTO pCategoryDTO : categoryList) {
			ProductCategory pcCategory = new ProductCategory();
			pcCategory.setCategoryId(pCategoryDTO.getId());
			pcCategory.setProductCategory(pCategoryDTO.getCategory());
			pcCategory.setDescription(pCategoryDTO.getDescription());
			pcCategory.setActiveStatus(pCategoryDTO.isActive());
			pcCategory.setStockStatus(true);
			pcCategory.setDemandStatus(true);
			ImageDTO imageDTO = imageDAO.getByProductId(ImageCategory.CATEGORY.getName(),
					pCategoryDTO.getId().longValue());
			if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
				pcCategory.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
			}
			response.getCategoryList().add(pcCategory);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public BrandListResponse
	 * getBrands(String authToken, GetBrandRequest request, Locale locale)
	 * throws Exception { BrandListResponse response = new BrandListResponse();
	 * logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
	 * String mobile =
	 * authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if(adminDTO !=
	 * null){ logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
	 * authToken); } List<ProductBrandsDTO> brandList = brandDAO
	 * .getAllBrandsByCategoryIDWithoutActiveState(request.getProductCategoryId(
	 * )); for (ProductBrandsDTO pBrandsDTO : brandList) { ProductBrands pBrands
	 * = new ProductBrands(); pBrands.setBrandId(pBrandsDTO.getId());
	 * pBrands.setProductBrands(pBrandsDTO.getBrand());
	 * pBrands.setActiveStatus(pBrandsDTO.isActive());
	 * pBrands.setStockStatus(true); pBrands.setDemandStatus(true); ImageDTO
	 * imageDTO = imageDAO.getByProductId(ImageCategory.BRAND.getName(),
	 * pBrandsDTO.getId().longValue()); if (imageDTO != null &&
	 * imageDTO.getUpdatedOn() != null) {
	 * pBrands.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime()); }
	 * response.getBrandList().add(pBrands); } response =
	 * ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES); logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response)); return response; }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BrandListResponse getBrands(String authToken, GetBrandRequest request, Locale locale) throws Exception {
		BrandListResponse response = new BrandListResponse();
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		List<ProductBrandsDTO> brandList = brandDAO
				.getAllBrandsByCategoryIDWithoutActiveState(request.getProductCategoryId());
		for (ProductBrandsDTO pBrandsDTO : brandList) {
			ProductBrands pBrands = new ProductBrands();
			pBrands.setBrandId(pBrandsDTO.getId());
			pBrands.setProductBrands(pBrandsDTO.getBrand());
			pBrands.setActiveStatus(pBrandsDTO.isActive());
			pBrands.setStockStatus(true);
			pBrands.setDemandStatus(true);
			ImageDTO imageDTO = imageDAO.getByProductId(ImageCategory.BRAND.getName(), pBrandsDTO.getId().longValue());
			if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
				pBrands.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
			}
			response.getBrandList().add(pBrands);
		}
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		return response;
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class, readOnly = true) public
	 * ProductListResponse getProductList(String authToken, GetProductRequest
	 * request, Locale locale) throws Exception { ProductListResponse response =
	 * new ProductListResponse(); logger.info(ApiUtils.REQUEST_PAYLOAD,
	 * ApiUtils.toJsonString(request)); String mobile =
	 * authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if(adminDTO !=
	 * null){ logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
	 * authToken); } List<ProductDTO> pdto = productDAO
	 * .getAllExistingProductsByBrandIdWithoutActiveState(request.
	 * getProductBrandId()); if (pdto == null) { } else { for (ProductDTO dto :
	 * pdto) { ProductDetails pcCat = new ProductDetails();
	 * pcCat.setProductId(dto.getId());
	 * pcCat.setProductName(dto.getProductName());
	 * pcCat.setQuantityUnit(dto.getQuantityUnit());
	 * pcCat.setPrice(dto.getPrice()); pcCat.setActiveStatus(dto.isActive());
	 * 
	 * if(!dto.isActive()){ pcCat.setStockStatus(!dto.isActive()); } else {
	 * pcCat.setStockStatus(dto.isActive()); }
	 * 
	 * pcCat.setStockStatus(true); pcCat.setDemandStatus(true); ImageDTO
	 * imageDTO = imageDAO.getByProductId(ImageCategory.PRODUCT.getName(),
	 * dto.getId().longValue()); if (imageDTO != null && imageDTO.getUpdatedOn()
	 * != null) {
	 * pcCat.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime()); }
	 * response.getProductList().add(pcCat); } } System.out.println("RESPONSE" +
	 * response); response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES); logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response)); return response; }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductListResponse getProductList(String authToken, GetProductRequest request, Locale locale)
			throws Exception {
		ProductListResponse response = new ProductListResponse();
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		List<ProductDTO> pdto = productDAO
				.getAllExistingProductsByBrandIdWithoutActiveState(request.getProductBrandId());
		if (pdto == null) {
		} else {
			for (ProductDTO dto : pdto) {
				ProductDetails pcCat = new ProductDetails();
				pcCat.setProductId(dto.getId());
				pcCat.setProductName(dto.getProductName());
				pcCat.setQuantityUnit(dto.getQuantityUnit());
				pcCat.setPrice(dto.getPrice());
				pcCat.setActiveStatus(dto.isActive());
				/*
				 * if(!dto.isActive()){ pcCat.setStockStatus(!dto.isActive()); }
				 * else { pcCat.setStockStatus(dto.isActive()); }
				 */
				pcCat.setStockStatus(true);
				pcCat.setDemandStatus(true);
				ImageDTO imageDTO = imageDAO.getByProductId(ImageCategory.PRODUCT.getName(), dto.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					pcCat.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
				response.getProductList().add(pcCat);
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse getCustomerRecords(String authToken, Locale locale) throws Exception {
		GenericResponse response = new GenericResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		// logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
		// authToken);
		logger.info(ApiUtils.REQUEST_PAYLOAD, adminDTO.getId());
		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}
		List<CustomerRecord> record = generateCustomerReport(locale);
		convertCustomerRecordsToExcel(record, adminDTO.getEmail());
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse markOrderDelivered(String authToken, MarkOrderRequest request, Locale locale)
			throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		GenericResponse response = new GenericResponse();
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		int paymentModeId = 0;
		// List<Long> filteredDeliverableList =
		// request.getDeliveredOrderInfoIdList().stream().filter(i -> i >=
		// 3).collect(Collectors.toList());

		for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO.getByOrderId(orderInfoId);
			List<Long> orderDetailsInfoIdList = new ArrayList<Long>();
			int undeliveryStatusCount = 0;
			for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
				// By Pushpinder 5/30/18
				if (request.getUnDeliveredOrderDetailsInfoIdList().contains(orderDetailsInfoDTO.getId())) {
					undeliveryStatusCount++;
				}
				orderDetailsInfoIdList.add(orderDetailsInfoDTO.getId());
			}
			if (undeliveryStatusCount == 0) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
			} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
			} else {
				orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
			}

			if (orderDetailsInfoIdList.size() > 0) {
				orderDetailsInfoIdList.removeAll(request.getUnDeliveredOrderDetailsInfoIdList());
			}

			for (Long orderDetailsInfoId : orderDetailsInfoIdList) {
				paymentModeId = markDelivered(orderDetailsInfoId, adminDTO.getId());
				paymentModeIdList.add(paymentModeId);
			}
			if (paymentModeIdList.contains(PaymentMode.ByCashByWallet.getId())) {
				orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
			} else {
				orderInfoDTO.setPaymentModeId(paymentModeId);
			}

		}
		/*
		 * for (OrderDetailsInfoDTO orderDetailsInfoDTO :
		 * orderDetailsInfoDTOList) {
		 * markDelivered(orderDetailsInfoDTO.getId()); }
		 */
		/*
		 * for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
		 * OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
		 * List<OrderDetailsInfoDTO> orderDetailsInfoDTOList =
		 * orderDetailsInfoDAO.getByOrderId(orderInfoId); for
		 * (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
		 * markDelivered(orderDetailsInfoDTO.getId()); } }
		 */

		for (Long orderDetailsInfoId : request.getUnDeliveredOrderDetailsInfoIdList()) {
			markUndelivered(orderDetailsInfoId);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private void markOrderUndeliveredV2(Long orderDetailsInfoId, Long adminId, Long customerId) {
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(customerId);
			transactionRequest.setOrderId(detailsInfoDTO.getOrderId());
			transactionRequest.setOrderDetailsId(orderDetailsInfoId);
			transactionRequest.setMoneyIn(detailsInfoDTO.getAmountPaidByCustomer());
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setDueAmount(0);
			transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
			addCustomerTransaction(transactionRequest, adminId);
			updateWalletBalance(customerId, detailsInfoDTO.getAmountPaidByCustomer(),
					WalletDescription.RECHARGE.getName());

		}
		detailsInfoDTO.setOrderStatus(OrderStatus.UNDELIVERED.getName());
		detailsInfoDTO.setPaymentModeId(0);
		detailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
		detailsInfoDTO.setCancelled(true);
		detailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
		detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		// ---- Added by Pushpinder to deduct the undelivered product money from
		// oderInfo
		// OrderInfoDTO orderInfoDTO =
		// orderInfoDAO.getById(detailsInfoDTO.getOrderId());
		// float subtotal = orderInfoDTO.getOrderSubTotal();
		// if(subtotal != 0) {
		// orderInfoDTO.setOrderSubTotal(subtotal -
		// (detailsInfoDTO.getProductMRP()*detailsInfoDTO.getQuantity()));
		// orderInfoDTO.setUpdatedOn((new
		// Timestamp(System.currentTimeMillis())));
		// }
	}

	private void markUndelivered(Long orderDetailsInfoId) {
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		detailsInfoDTO.setOrderStatus(OrderStatus.UNDELIVERED.getName());
		detailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
		detailsInfoDTO.setCancelled(true);
		detailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
		detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		// ---- Added by Pushpinder to deduct the undelivered product money from
		// oderInfo
		// OrderInfoDTO orderInfoDTO =
		// orderInfoDAO.getById(detailsInfoDTO.getOrderId());
		// float subtotal = orderInfoDTO.getOrderSubTotal();
		// if(subtotal != 0) {
		// orderInfoDTO.setOrderSubTotal(subtotal -
		// (detailsInfoDTO.getProductMRP()*detailsInfoDTO.getQuantity()));
		// orderInfoDTO.setUpdatedOn((new
		// Timestamp(System.currentTimeMillis())));
		// }
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ProductResponse updateProduct(String authToken, UpdateProductRequest request, Locale locale)
			throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		ProductResponse response = new ProductResponse();
		int productId = 0;
		if (request.getProductId() < 1) {
			productId = addProduct(request, adminDTO.getId(), locale);
		} else {
			ProductDTO productDTO = productDAO.getById(request.getProductId());
			productId = updateProduct(productDTO, adminDTO.getId(), request);
		}
		response.setProductId(productId);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}
	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public GetOrderResponse
	 * getOrderList(String authToken, GetOrderRequest request, Locale locale)
	 * throws Exception { String mobile =
	 * authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
	 * logger.info(ApiUtils.REQUEST_PAYLOAD, ApiUtils.toJsonString(request));
	 * AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile); if (adminDTO !=
	 * null) { logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
	 * authToken); } GetOrderResponse response = new GetOrderResponse();
	 * 
	 * String jsonFilePath = System.getProperty("config.dir") +
	 * "/json/cityProjectTowerList.json"; long jsonFileLastUpdatedOn =
	 * jsonFileLastUpdatedOn(jsonFilePath);
	 * 
	 * if (jsonFileLastUpdatedOn > jsonLoadedOn) { cityProjectJsonArray =
	 * readCityJSON(jsonFilePath); projectJsonArray =
	 * readProjectJSON(jsonFilePath); jsonLoadedOn = System.currentTimeMillis();
	 * } String cityName = getCityName(request.getCityId()); String projectName
	 * = getProjectName(request.getProjectId()); checkValidation(cityName,
	 * projectName, locale);
	 * 
	 * String towerName = getTowerName(request.getProjectId(),
	 * request.getTowerId()); if (request.getTowerId() > 0 &&
	 * Strings.isNullOrEmpty(towerName)) { checkToweValidation(towerName,
	 * locale); }
	 * 
	 * Date deliveryDate = new Date(request.getDeliveryDate());
	 * 
	 * org.json.simple.JSONObject jsonObject =
	 * readTimeSlotArray(timeSlotJsonArray, request.getTimeSlotId()); String
	 * timeSlot = (String) jsonObject.get("timeSlot"); List<Long> customerList =
	 * null; List<Integer> timeSlotList = null;
	 * 
	 * if (!Strings.isNullOrEmpty(towerName)) { if (request.getTimeSlotId() > 0)
	 * { customerList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDateAndTimeSlot(cityName,
	 * projectName, towerName, deliveryDate, timeSlot); } else if
	 * (request.getTimeSlotId() == 0) { customerList =
	 * orderInfoDAO.getCustomersByCPTDeliveryDate(cityName, projectName,
	 * towerName, deliveryDate);
	 * 
	 * } } else { if (request.getTimeSlotId() > 0) { customerList =
	 * orderInfoDAO.getCustomersByCPDeliveryDateAndTimeSlotId(cityName,
	 * projectName, deliveryDate, request.getTimeSlotId());
	 * 
	 * } else if (request.getTimeSlotId() == 0) { customerList =
	 * orderInfoDAO.getCustomersByCPDeliveryDate(cityName, projectName,
	 * deliveryDate); } } if (customerList.size() == 0) {
	 * response.getCustomerOrderList().clear(); } for (Long customerId :
	 * customerList) { float myTotalBalance = 0;
	 * 
	 * WalletDTO walletDTO = walletDAO.getByCustomerId(customerId); if
	 * (walletDTO != null) { myTotalBalance = walletDTO.getMyWalletBalance(); }
	 * CustomerOrderDetails cDetails = new CustomerOrderDetails();
	 * cDetails.setCustomerId(customerId); CustomerDTO customerDTO =
	 * customerDAO.getById(customerId);
	 * cDetails.setCustomerName(customerDTO.getFullName()); AddressDTO
	 * addressDTO = addressDAO.getById(customerDTO.getAddressId());
	 * cDetails.setFlatName(addressDTO.getFlat());
	 * cDetails.setTowerName(addressDTO.getTower());
	 * cDetails.setCustomerMobileNumber(customerDTO.getMobileNumber());
	 * cDetails.setWalletbalance(myTotalBalance);
	 * ///////////////////////////////////////////// // cDet
	 * ///////////////////////////////////////////// if (request.getTimeSlotId()
	 * > 0) { timeSlotList =
	 * orderInfoDAO.getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(customerId,
	 * deliveryDate, request.getTimeSlotId());
	 * 
	 * } else if (request.getTimeSlotId() == 0) { timeSlotList =
	 * orderInfoDAO.getTimeSlotByCustomerDeliveryDate(customerId, deliveryDate);
	 * } float restMoneyInWallet = myTotalBalance; for (Integer timeSlotId :
	 * timeSlotList) { CustomerTimeSlot cTimeSlot = new CustomerTimeSlot();
	 * PaymentMethod pMethod = new PaymentMethod(); float cash = 0; float wallet
	 * = 0; float billAmount = 0; cTimeSlot.setTimeSlotId(timeSlotId);
	 * List<OrderInfoDTO> orderInfoList =
	 * orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotId(customerId,
	 * deliveryDate, timeSlotId); if (orderInfoList.size() == 0) { continue; }
	 * for (OrderInfoDTO orderInfo : orderInfoList) { OrderInfoDetails
	 * orderInfoDetails = new OrderInfoDetails(); List<OrderDetailsInfoDTO>
	 * orderDetailsInfoDTOList = orderDetailsInfoDAO
	 * .getSettledOrderDetailsByOrderId(orderInfo.getId()); if
	 * (orderDetailsInfoDTOList.size() > 0) {
	 * orderInfoDetails.setOrderInfoId(orderInfo.getId());
	 * 
	 * for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
	 * DeliveryDetails deliveryDetails = new DeliveryDetails(); deliveryDetails
	 * = setDeliveryDetails(orderDetailsInfoDTO, orderInfo, restMoneyInWallet);
	 * if (deliveryDetails.getPaymentMethod() != null) { restMoneyInWallet =
	 * deliveryDetails.getPaymentMethod().getRestMoneyInWallet(); } if
	 * (deliveryDetails.getDeliveryStatus() == (OrderStatus.DELIVERED.getId()))
	 * { cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } else if
	 * (orderDetailsInfoDTOList.size() > 0 &&
	 * ((deliveryDetails.getDeliveryStatus() ==
	 * (OrderStatus.UNDELIVERED.getId())))) {
	 * cTimeSlot.setFinalOrderStatus(OrderStatus.SETTLED.getId()); } if
	 * (deliveryDetails.getDeliveryStatus() !=
	 * (OrderStatus.UNDELIVERED.getId())) { billAmount = billAmount +
	 * (deliveryDetails.getPrice() * deliveryDetails.getQuantity()); }
	 * 
	 * orderInfoDetails.getDeliveryDetails().add(deliveryDetails); if
	 * (deliveryDetails.getPaymentMethod() != null) { cash = cash +
	 * deliveryDetails.getPaymentMethod().getCash(); wallet = wallet +
	 * deliveryDetails.getPaymentMethod().getWallet(); } } }
	 * 
	 * cTimeSlot.getOrderInfoList().add(orderInfoDetails); }
	 * 
	 * pMethod.setCash(cash);
	 * 
	 * pMethod.setWallet(wallet); // pMethod.setRestMoneyInWallet(value);
	 * cTimeSlot.setPaymentMethod(pMethod);
	 * cTimeSlot.setTotalBillAmount(billAmount);
	 * cDetails.getCustomerTimeSlotList().add(cTimeSlot); }
	 * 
	 * response.getCustomerOrderList().add(cDetails); }
	 * 
	 * response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES);
	 * 
	 * return response;
	 * 
	 * }
	 */

	private void checkToweValidation(String towerName, Locale locale) throws ValidationException {

		if (Strings.isNullOrEmpty(towerName)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.TOWER_JSON_ERROR, locale));

		}

	}

	private long jsonFileLastUpdatedOn(String filepath) {
		return new File(filepath).lastModified();
	}

	private DeliveryDetails setDeliveryDetails(OrderDetailsInfoDTO orderDetailsInfoDTO, OrderInfoDTO orderInfo,
			float restMoneyInWallet) {
		DeliveryDetails deliveryDetails = new DeliveryDetails();
		ProductDTO productDTO = productDAO.getById(orderDetailsInfoDTO.getProductId());
		deliveryDetails.setOrderDetailsId(orderDetailsInfoDTO.getId());
		// deliveryDetails.setTimeSlot(orderInfo.getTimeSlot());
		// deliveryDetails.s
		deliveryDetails.setProductName(productDTO.getProductName());
		deliveryDetails.setQuantity(orderDetailsInfoDTO.getQuantity());
		deliveryDetails.setQuantityUnit(productDTO.getQuantityUnit());
		CustomerDTO customerDTO = customerDAO.getById(orderInfo.getCustomerId());
		if (customerDTO == null) {
			return null;
		}
		// deliveryDetails.setCustomerName(customerDTO.getFullName());
		PaymentMethod paymentMethod = new PaymentMethod();
		paymentMethod.setRestMoneyInWallet(restMoneyInWallet);
		if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.DELIVERED.getName())
		/*
		 * && orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.
		 * PAYMENTSETTLED.getName())
		 */) {
			deliveryDetails.setDeliveryStatus(OrderStatus.DELIVERED.getId());
			paymentMethod = setPaymentStatusIfDelivered(paymentMethod, orderInfo, orderDetailsInfoDTO);

		} else if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())
				|| (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CANCELLED.getName()))) {
			deliveryDetails.setDeliveryStatus(OrderStatus.UNDELIVERED.getId());
			paymentMethod = null;
		} else if (orderDetailsInfoDTO.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
			deliveryDetails.setDeliveryStatus(OrderStatus.DELIVERYSTATUSPENDING.getId());
			paymentMethod = setPaymentStatusIfPending(paymentMethod, customerDTO, productDTO, orderDetailsInfoDTO,
					restMoneyInWallet);
		}
		deliveryDetails.setPaymentMethod(paymentMethod);
		deliveryDetails.setPrice(
				orderDetailsInfoDTO.getProductMRP() > 0 ? orderDetailsInfoDTO.getProductMRP() : productDTO.getPrice());

		return deliveryDetails;

	}

	private PaymentMethod setPaymentStatusIfDelivered(PaymentMethod paymentMethod, OrderInfoDTO orderInfo,
			OrderDetailsInfoDTO orderDetailsInfoDTO) {

		if (orderInfo.getPaymentModeId() == PaymentMode.ByCash.getId()
				|| orderInfo.getPaymentModeId() == PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId()) {
			paymentMethod.setCash(orderDetailsInfoDTO.getAmountPaidByCustomer());
		} else if (orderInfo.getPaymentModeId() == PaymentMode.ByWallet.getId()
				|| orderInfo.getPaymentModeId() == PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId()) {
			paymentMethod.setWallet(orderDetailsInfoDTO.getAmountPaidByCustomer());

		} else if (orderInfo.getPaymentModeId() == PaymentMode.ByCashByWallet.getId()
				|| orderInfo.getPaymentModeId() == PaymentMode.PARTIAL_PAYMENT.getId()) {
			List<CustomerTransactionDTO> transactionDTOList = transactionDAO
					.getByOrderDetailsId(orderDetailsInfoDTO.getId());
			for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
				if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
					paymentMethod.setCash(paymentMethod.getCash() + transactionDTO.getMoneyOut());

				} else if (transactionDTO.getPaymentModeId() == PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId()) {
					paymentMethod.setCash(paymentMethod.getCash() + transactionDTO.getMoneyOut());

				} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
					paymentMethod.setWallet(paymentMethod.getWallet() + transactionDTO.getMoneyOut());

				} else if (transactionDTO.getPaymentModeId() == PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId()) {
					paymentMethod.setWallet(paymentMethod.getWallet() + transactionDTO.getMoneyOut());

				}
			}
		}

		return paymentMethod;
	}

	private PaymentMethod setPaymentStatusIfPending(PaymentMethod paymentMethod, CustomerDTO customerDTO,
			ProductDTO productDTO, OrderDetailsInfoDTO orderDetailsInfoDTO, float restMoneyInWallet) {
		float walletCollect = 0;
		float cashCollect = 0;
		if (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
			walletCollect = orderDetailsInfoDTO.getAmountPaidByCustomer();
		} else if ((orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.getName()))
				|| (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PENDING.getName()))) {
			walletCollect = orderDetailsInfoDTO.getAmountPaidByCustomer();
			if (orderDetailsInfoDTO.getTotalProductCost() == 0) {
				orderDetailsInfoDTO.setTotalProductCost(productDTO.getPrice() * orderDetailsInfoDTO.getQuantity());
			}
			cashCollect = orderDetailsInfoDTO.getTotalProductCost() - orderDetailsInfoDTO.getAmountPaidByCustomer();
			if (restMoneyInWallet > 0) {
				if (restMoneyInWallet >= cashCollect) {
					walletCollect = walletCollect + cashCollect;
					restMoneyInWallet = restMoneyInWallet - cashCollect;
					cashCollect = 0;
				} else {
					walletCollect = walletCollect + restMoneyInWallet;
					cashCollect = cashCollect - restMoneyInWallet;
					restMoneyInWallet = 0;
				}
			}
		}

		paymentMethod.setWallet(walletCollect);
		paymentMethod.setCash(cashCollect);
		paymentMethod.setRestMoneyInWallet(restMoneyInWallet);

		return paymentMethod;
	}

	private String getCityName(long cityId) {
		// String cityName = null;

		for (int i = 0; i < cityProjectJsonArray.size(); i++) {
			org.json.simple.JSONObject cityJSON = (org.json.simple.JSONObject) cityProjectJsonArray.get(i);
			if ((Long) cityJSON.get("id") == (cityId)) {
				return (String) cityJSON.get("city");
			}
		}
		return null;
	}

	/*
	 * private String getCityName(long cityId) { for (int i = 0; i <
	 * cityProjectJsonArray.size(); i++) { org.json.simple.JSONObject cityJSON =
	 * (org.json.simple.JSONObject) cityProjectJsonArray.get(i); if ((Long)
	 * cityJSON.get("id") == (cityId)) { return (String) cityJSON.get("city"); }
	 * } return null; }
	 */

	private String getTowerName(long projectId, long towerId) {
		// String towerName = null;
		for (int i = 0; i < projectJsonArray.size(); i++) {
			org.json.simple.JSONObject projectJSON = (org.json.simple.JSONObject) projectJsonArray.get(i);
			if ((Long) projectJSON.get("id") == (projectId)) {
				org.json.simple.JSONArray towerArray = (org.json.simple.JSONArray) projectJSON.get("towers");
				for (int j = 0; j < towerArray.size(); j++) {
					org.json.simple.JSONObject towerJSON = (org.json.simple.JSONObject) towerArray.get(j);
					if ((Long) towerJSON.get("id") == (towerId)) {
						return (String) towerJSON.get("towerName");
					}
				}
			}
		}
		return null;
	}

	/*
	 * private String getProjectName(long projectId) { for (int i = 0; i <
	 * projectJsonArray.size(); i++) { org.json.simple.JSONObject projectJSON =
	 * (org.json.simple.JSONObject) projectJsonArray.get(i); if ((Long)
	 * projectJSON.get("id") == (projectId)) { return (String)
	 * projectJSON.get("projectName"); } } return null; }
	 */
	private String getProjectName(long projectId) {
		// String projectName = null;
		for (int i = 0; i < projectJsonArray.size(); i++) {
			org.json.simple.JSONObject projectJSON = (org.json.simple.JSONObject) projectJsonArray.get(i);
			if ((Long) projectJSON.get("id") == (projectId)) {
				return (String) projectJSON.get("projectName");
			}
		}
		return null;
	}

	private void checkValidation(String cityName, String projectName, Locale locale) throws ValidationException {
		if (Strings.isNullOrEmpty(cityName) || Strings.isNullOrEmpty(projectName)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CITYPROJECT_JSON_ERROR, locale));

		}
		/*
		 * if(Strings.isNullOrEmpty(projectName)){ throw new
		 * ValidationException(
		 * ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.
		 * CITYPROJECT_JSON_ERROR, locale));
		 * 
		 * }
		 */
	}

	private Integer addProduct(UpdateProductRequest request, Long adminId, Locale locale) throws ValidationException {
		ProductDTO productDTO = new ProductDTO();
		productDTO.setProductName(request.getProductName());
		productDTO.setPrice(request.getPrice());
		productDTO.setQuantityUnit(request.getQuantityUnit());
		ProductBrandsDTO brandDTO = brandDAO.getById(request.getBrandId());
		if (brandDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.NO_BRAND_EXISTS, locale));
		}
		productDTO.setBrandId(request.getBrandId());
		productDTO.setActive(request.isActive());
		productDTO.setStockStatus(true);
		productDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		productDTO.setUpdatedBy(adminId);
		return productDAO.addProduct(productDTO);

	}

	private Integer updateProduct(ProductDTO productDTO, Long adminId, UpdateProductRequest request) {
		productDTO.setProductName(request.getProductName());
		productDTO.setPrice(request.getPrice());
		productDTO.setQuantityUnit(request.getQuantityUnit());
		productDTO.setUpdatedBy(adminId);
		productDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return productDTO.getId();

	}

	private int markDelivered(long orderDetailsId, long adminId) {
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsId);
		if (detailsInfoDTO != null) {
			ProductDTO productDTO = productDAO.getById(detailsInfoDTO.getProductId());
			detailsInfoDTO.setProductMRP(
					detailsInfoDTO.getProductMRP() > 0 ? detailsInfoDTO.getProductMRP() : productDTO.getPrice());
			detailsInfoDTO.setTotalProductCost(
					(detailsInfoDTO.getProductMRP() > 0 ? detailsInfoDTO.getProductMRP() : productDTO.getPrice())
							* detailsInfoDTO.getQuantity());
			detailsInfoDTO.setAmountPaidByCustomer(
					(detailsInfoDTO.getProductMRP() > 0 ? detailsInfoDTO.getProductMRP() : productDTO.getPrice())
							* detailsInfoDTO.getQuantity());
			detailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());
			detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
			detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			OrderInfoDTO orderInfo = orderInfoDAO.getById(detailsInfoDTO.getOrderId());
			float hasMaidinBalance = checkMaidinBalance(orderInfo.getCustomerId());
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(orderInfo.getCustomerId());
			transactionRequest.setOrderId(orderInfo.getId());
			transactionRequest.setOrderDetailsId(orderDetailsId);
			transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
			if (hasMaidinBalance > 0) {
				if (hasMaidinBalance >= detailsInfoDTO.getTotalProductCost()) {
					transactionRequest.setMoneyOut(detailsInfoDTO.getTotalProductCost());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfo.getCustomerId(), detailsInfoDTO.getTotalProductCost(),
							WalletDescription.DEDUCTIONS.getName());
					orderInfo.setPaymentModeId(PaymentMode.ByWallet.getId());
				} else if (hasMaidinBalance < detailsInfoDTO.getTotalProductCost()) {
					transactionRequest.setMoneyOut(hasMaidinBalance);
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfo.getCustomerId(), hasMaidinBalance,
							WalletDescription.DEDUCTIONS.getName());
					transactionRequest.setMoneyOut(detailsInfoDTO.getTotalProductCost() - hasMaidinBalance);
					transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
					addCustomerTransaction(transactionRequest, adminId);
					orderInfo.setPaymentModeId(PaymentMode.ByCashByWallet.getId());

				}
			} else {
				transactionRequest.setMoneyOut(detailsInfoDTO.getTotalProductCost() - hasMaidinBalance);
				transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
				addCustomerTransaction(transactionRequest, adminId);
				orderInfo.setPaymentModeId(PaymentMode.ByCash.getId());
			}
			return orderInfo.getPaymentModeId();
		}
		return 0;

	}

	private void updateWalletBalance(Long customerId, float totalProductCost, String rechargeType) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			walletDTO.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + totalProductCost);
			}
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		} else {
			WalletDTO walletDto = new WalletDTO();
			walletDto.setCustomerId(customerId);
			if (rechargeType.equals(WalletDescription.DEDUCTIONS.getName())) {
				walletDto.setMyWalletBalance(-totalProductCost);
			} else if (rechargeType.equals(WalletDescription.RECHARGE.getName())) {
				walletDto.setMyWalletBalance(totalProductCost);
			}
			walletDto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			walletDAO.addBalance(walletDto);
		}

	}

	private void addCustomerTransaction(CustomerTransactionRequest transactionRequest, long adminId) {
		CustomerTransactionDTO transactionDTO = new CustomerTransactionDTO();
		transactionDTO.setCustomerId(transactionRequest.getCustomerId());
		transactionDTO.setOrderId(transactionRequest.getOrderId());
		transactionDTO.setOrderDetailsId(transactionRequest.getOrderDetailsId());
		transactionDTO.setMoneyIn(transactionRequest.getMoneyIn());
		transactionDTO.setMoneyOut(transactionRequest.getMoneyOut());
		transactionDTO.setPaymentModeId(transactionRequest.getPaymentModeId());
		transactionDTO.setTransactionDate(new Timestamp(System.currentTimeMillis()));
		transactionDTO.setTransactionDescription(transactionRequest.getTransactionType());
		transactionDTO.setDueAmount(transactionRequest.getDueAmount());
		transactionDTO.setPreBalance(checkMaidinBalance(transactionRequest.getCustomerId()));
		transactionDTO.setRemarks(transactionRequest.getRemarkStatus());
		transactionDTO.setUpdatedBy(adminId);
		transactionDAO.addTransaction(transactionDTO);

	}

	private float checkMaidinBalance(Long customerId) {
		WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
		if (walletDTO != null) {
			// if (walletDTO.getMyWalletBalance() != null) {
			return walletDTO.getMyWalletBalance();
			// }
			// return 0;
		}
		return 0;
	}

	private void convertCustomerRecordsToExcel(List<CustomerRecord> recordList, String senderEmail)
			throws FileNotFoundException, IOException, MessagingException {

		// List<OrderFormat> orderList = response.getOrderFormatList();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("CustomerRecords");

		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.LEFT);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);

		int rowCount = 0;
		createCustomerReportheader(sheet, rowCount);

		for (CustomerRecord record : recordList) {

			Row row2 = sheet.createRow(++rowCount);
			Cell serialNoVal = row2.createCell(0);
			serialNoVal.setCellValue(record.getSerialNo());
			serialNoVal.setCellStyle(cellStyle);

			Cell nameVal = row2.createCell(1);
			nameVal.setCellValue(record.getName());
			nameVal.setCellStyle(cellStyle);

			Cell genderVal = row2.createCell(2);
			genderVal.setCellValue(record.getGender());
			genderVal.setCellStyle(cellStyle);

			Cell mobVal = row2.createCell(3);
			mobVal.setCellValue(record.getMobile());
			mobVal.setCellStyle(cellStyle);

			Cell projVal = row2.createCell(4);
			projVal.setCellValue(record.getProject());
			projVal.setCellStyle(cellStyle);

			Cell towerVal = row2.createCell(5);
			towerVal.setCellValue(record.getTower());
			towerVal.setCellStyle(cellStyle);

			Cell flatVal = row2.createCell(6);
			flatVal.setCellValue(record.getFlat());
			flatVal.setCellStyle(cellStyle);

			Cell emailVal = row2.createCell(7);
			emailVal.setCellValue(record.getEmail());
			emailVal.setCellStyle(cellStyle);

			Cell dateVal = row2.createCell(8);
			dateVal.setCellValue(record.getRegisteredOn());
			dateVal.setCellStyle(cellStyle);

			Cell platformVal = row2.createCell(9);
			platformVal.setCellValue(record.getPlatform());
			platformVal.setCellStyle(cellStyle);

			Cell deviceModelVal = row2.createCell(10);
			deviceModelVal.setCellValue(record.getDeviceModel());
			deviceModelVal.setCellStyle(cellStyle);

			Cell appVersionVal = row2.createCell(11);
			appVersionVal.setCellValue(record.getAppVersion());
			appVersionVal.setCellStyle(cellStyle);

			Cell inactiveSinceVal = row2.createCell(12);
			inactiveSinceVal.setCellValue(record.getInactiveSince());
			inactiveSinceVal.setCellStyle(cellStyle);

		}

		long androidUsersCount = getUserCountByClientPlatform("android");
		long iOSUsersCount = getUserCountByClientPlatform("iOS");

		Row row3 = sheet.getRow(3);
		Row row4 = sheet.getRow(4);

		if (androidUsersCount != 0l) {
			Cell androidStatsCell = row3.createCell(13);
			androidStatsCell.setCellValue("Total android users : " + androidUsersCount);
			androidStatsCell.setCellStyle(cellStyle);
		}

		if (iOSUsersCount != 0l) {
			Cell androidStatsCell = row4.createCell(13);
			androidStatsCell.setCellValue("Total iOS users : " + iOSUsersCount);
			androidStatsCell.setCellStyle(cellStyle);
		}

		/*
		 * // limit S. No width to 6 characters sheet.setColumnWidth(0, 256 *
		 * 6);
		 * 
		 * // limit Customer name width to 15 characters sheet.setColumnWidth(1,
		 * 256 * 15);
		 * 
		 * // limit Gender width to 9 characters sheet.setColumnWidth(2, 256 *
		 * 9);
		 * 
		 * // limit Mobile number width to 13 characters sheet.setColumnWidth(3,
		 * 256 * 13);
		 * 
		 * // limit Project name width to 20 characters sheet.setColumnWidth(4,
		 * 256 * 20);
		 * 
		 * // limit Tower name width to 10 characters sheet.setColumnWidth(5,
		 * 256 * 10);
		 * 
		 * // limit Flat name width to 10 characters sheet.setColumnWidth(6, 256
		 * * 10);
		 * 
		 * // limit Email width to 25 characters sheet.setColumnWidth(7, 256 *
		 * 25);
		 * 
		 * // limit Registered on width to 13 characters sheet.setColumnWidth(8,
		 * 256 * 13);
		 * 
		 * // limit Client platform width to 10 characters
		 * sheet.setColumnWidth(9, 256 * 10);
		 * 
		 * // limit Device model width to 25 characters sheet.setColumnWidth(10,
		 * 256 * 25);
		 * 
		 * // limit app version width to 25 characters sheet.setColumnWidth(11,
		 * 256 * 10);
		 * 
		 * // limit user platform count cell width to 30 characters
		 * sheet.setColumnWidth(13, 256 * 30);
		 */

		for (int i = 0; i < 15; i++) {
			sheet.autoSizeColumn(i);
		}

		File file = new File("Customer Records till "
				+ ApiUtils.dateWithMonthNameFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");
		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);
		String currentDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(System.currentTimeMillis()));
		String subject = "Customer Records | Date :  " + currentDate;
		EmailSenderWithAttachment.sendMail(senderEmail, subject, "Attachment", multipart);

	}

	private void createCustomerReportheader(XSSFSheet sheet, int rowCount) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row row = sheet.createRow(rowCount);
		Cell sNoHeader = row.createCell(0);
		sNoHeader.setCellValue("S. No.");
		sNoHeader.setCellStyle(cellStyle);

		Cell nameHeader = row.createCell(1);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		Cell genderHeader = row.createCell(2);
		genderHeader.setCellValue("Gender");
		genderHeader.setCellStyle(cellStyle);

		Cell mobileHeader = row.createCell(3);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		Cell projectHeader = row.createCell(4);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		Cell towerHeader = row.createCell(5);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		Cell flatHeader = row.createCell(6);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		Cell emailHeader = row.createCell(7);
		emailHeader.setCellValue("Email");
		emailHeader.setCellStyle(cellStyle);

		Cell registeredOnHeader = row.createCell(8);
		registeredOnHeader.setCellValue("Registered On");
		registeredOnHeader.setCellStyle(cellStyle);

		Cell platformHeader = row.createCell(9);
		platformHeader.setCellValue("Client platform");
		platformHeader.setCellStyle(cellStyle);

		Cell deviceModelHeader = row.createCell(10);
		deviceModelHeader.setCellValue("Device model");
		deviceModelHeader.setCellStyle(cellStyle);

		Cell appVersionHeader = row.createCell(11);
		appVersionHeader.setCellValue("App version");
		appVersionHeader.setCellStyle(cellStyle);

		Cell inactiveSince = row.createCell(12);
		inactiveSince.setCellValue("Inactive since (in days)");
		inactiveSince.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 1, 0, 1);
	}

	// @SuppressWarnings("null")
	private List<CustomerRecord> generateCustomerReport(Locale locale) {
		List<CustomerRecord> customerRecordList = new ArrayList<CustomerRecord>();
		List<CustomerDTO> customerList = customerDAO.getActiveUsersList(true);

		int i = 1;
		for (CustomerDTO customerDTO : customerList) {
			CustomerRecord record = addCustomerRecord(i, customerDTO);
			customerRecordList.add(record);
			i++;

		}
		return customerRecordList;
	}

	private CustomerRecord addCustomerRecord(int serialNo, CustomerDTO customerDTO) {
		CustomerRecord record = new CustomerRecord();
		record.setSerialNo(serialNo);
		record.setName(customerDTO.getFullName());
		record.setMobile(customerDTO.getMobileNumber());
		record.setEmail(customerDTO.getEmail());
		record.setGender(customerDTO.getGender());
		AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
		if (addressDTO != null) {
			record.setProject(addressDTO.getProject());
			record.setTower(addressDTO.getTower());
			record.setFlat(addressDTO.getFlat());
		}
		CustomerDeviceDetailsDTO customerDeviceDetailsDTO = customerDeviceDetailsDAO
				.getDeviceDetailsByCustomerId(customerDTO.getId());
		if (customerDeviceDetailsDTO != null) {
			record.setPlatform(customerDeviceDetailsDTO.getDevicePlatform());
			record.setDeviceModel(customerDeviceDetailsDTO.getDeviceModel());
			record.setAppVersion(customerDeviceDetailsDTO.getAppVersion());

			if (customerDeviceDetailsDTO.getUpdatedOn() != null) {
				long lastAccessed = customerDeviceDetailsDTO.getUpdatedOn().getTime();
				long currentTime = System.currentTimeMillis();

				long differenceInTime = currentTime - lastAccessed;
				// 86400000 milliseconds are there in a day
				long inactiveSince = differenceInTime / 86400000;

				record.setInactiveSince(inactiveSince);
			} else {
				record.setInactiveSince(-1);
			}
		}

		record.setRegisteredOn(ApiUtils.dateWithMonthNameFormat(customerDTO.getCreatedOn()));
		return record;

	}

	private void updateAdminDeviceDetails(DeviceToken request, AdminDeviceDetailsDTO devicedto, Long adminId) {
		devicedto.setAdminId(adminId);
		devicedto.setDeviceModel(request.getDeviceModel());
		devicedto.setDevicePlatform(request.getDevicePlatform());
		devicedto.setDeviceToken(request.getDeviceToken());
		devicedto.setDevicePlatformVersion(request.getDevicePlatformVersion());
		devicedto.setAppVersion(request.getAppVersion());
		devicedto.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		adminDeviceDetailsDAO.updateDeviceDetails(devicedto);

	}

	private Long addAdminDeviceDetails(DeviceToken request, Long adminId) {
		AdminDeviceDetailsDTO dtoken = new AdminDeviceDetailsDTO();
		dtoken.setAdminId(adminId);
		dtoken.setDeviceModel(request.getDeviceModel());
		dtoken.setDevicePlatform(request.getDevicePlatform());
		dtoken.setDeviceToken(request.getDeviceToken());
		dtoken.setDevicePlatformVersion(request.getDevicePlatformVersion());
		dtoken.setAppVersion(request.getAppVersion());
		dtoken.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		return adminDeviceDetailsDAO.addDeviceDetails(dtoken);
	}

	private void updateAdminAuthenticationInfo(AdminDTO adminDTO, AdminAuthenticationDTO adminAuthDTO,
			String mobileNumber, String authToken) {
		if (adminDTO != null) {
			adminAuthDTO.setAdminId(adminDTO.getId());
		}
		adminAuthDTO.setMobileNumber(mobileNumber);
		adminAuthDTO.setAuthToken(authToken);
		adminAuthenticationDAO.updateToken(adminAuthDTO);

	}

	private void addAdminAuthenticationInfo(AdminDTO driverDTO, String mobileNumber, String authToken) {
		AdminAuthenticationDTO adminAuth = new AdminAuthenticationDTO();
		if (driverDTO != null) {
			adminAuth.setAdminId(driverDTO.getId());
		}
		adminAuth.setMobileNumber(mobileNumber);
		adminAuth.setAuthToken(authToken);
		adminAuthenticationDAO.addToken(adminAuth);

	}

	private boolean validateRecoveryCodeTime(DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (currentTime.isAfter(userCodeExpireTime)) {
			return false;
		}
		return true;
	}

	private void validateRecoveryCode(String recoveryCode, String codeInDB, DateTime userCodeExpireTime, Locale locale)
			throws MandatoryFieldMissingException, ValidationException {
		DateTime currentTime = new DateTime();
		if (codeInDB == null || !codeInDB.equals(recoveryCode) || currentTime.isAfter(userCodeExpireTime)) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_CODE, locale));
		}
	}

	private void updateWalletInfo(WalletDTO walletDTO, UpsertWalletRequest request, Locale locale)
			throws ValidationException {
		if (request.getTransactionDesciption() == WalletDescription.RECHARGE.getId()) {
			walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() + request.getAmount());
			walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

		} else if ((request.getTransactionDesciption() == WalletDescription.DEDUCTIONS.getId())
				|| (request.getTransactionDesciption() == WalletDescription.DISCHARGE.getId())) {
			if ((walletDTO.getMyWalletBalance() - request.getAmount()) >= -deductionLimit) {
				walletDTO.setMyWalletBalance(walletDTO.getMyWalletBalance() - request.getAmount());
				walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CANNOT_DEDUCT, locale));
			}
		}

	}

	private void addWalletInfo(Long customerId, UpsertWalletRequest request, Locale locale) throws ValidationException {
		WalletDTO walletDTO = new WalletDTO();
		walletDTO.setCustomerId(customerId);
		if (request.getTransactionDesciption() == WalletDescription.RECHARGE.getId()) {
			walletDTO.setMyWalletBalance(request.getAmount());
		} else if ((request.getTransactionDesciption() == WalletDescription.DEDUCTIONS.getId())
				|| (request.getTransactionDesciption() == WalletDescription.DISCHARGE.getId())) {
			if (request.getAmount() <= deductionLimit) {
				walletDTO.setMyWalletBalance(-request.getAmount());
				walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.CANNOT_DEDUCT, locale));
			}
		}
		walletDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		walletDAO.addBalance(walletDTO);

	}

	private void createSingleSlotOrderReportHeader(XSSFSheet sheet, int rowCount, OrderRequest request,
			AdminDTO adminDTO) throws JSONException {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray, request.getTimeSlot());

		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue("Delivery report of " + fromDate + " | " + jsonObject.get("timeSlot")
				+ ".  Downloaded by - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())));
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 17));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		int cellSequenc = 0;
		Cell sNoHeader = row.createCell(cellSequenc);
		sNoHeader.setCellValue("S No.");
		sNoHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paymentModeHeader = row.createCell(cellSequenc);
		paymentModeHeader.setCellValue("Payment Mode");
		paymentModeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell maidinMoneyHeader = row.createCell(cellSequenc);
		maidinMoneyHeader.setCellValue("Maidin Money");
		maidinMoneyHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paidByMaidinMoneyHeader = row.createCell(cellSequenc);
		paidByMaidinMoneyHeader.setCellValue("Paid by Maidin Money");
		paidByMaidinMoneyHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell cashCollectHeader = row.createCell(cellSequenc);
		cashCollectHeader.setCellValue("Cash need to Collect ");
		cashCollectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
	}

	private void createOrderReportheader(XSSFSheet sheet, int rowCount, OrderRequest request) {
		logger.info("HEADER createion started");
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

		XSSFFont font = sheet.getWorkbook().createFont();
		// font.setBold(true);
		// font.setFontHeightInPoints((short) 12);
		// font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		// cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		// cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row row = sheet.createRow(rowCount);

		int cellSequenc = 0;
		Cell del_DateHeader = row.createCell(cellSequenc);
		del_DateHeader.setCellValue("Delivery Date");
		del_DateHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell del_SlotHeader = row.createCell(cellSequenc);
		del_SlotHeader.setCellValue("Delivery Slot");
		del_SlotHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		if (request.getFromTimestamp() != request.getToTimestamp()) {
			if (request.getFromTimestamp() == request.getToTimestamp()) {
				cellSequenc = cellSequenc + 1;
				Cell emailHeader = row.createCell(cellSequenc);
				emailHeader.setCellValue("Email");
				emailHeader.setCellStyle(cellStyle);

			}
		}

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell paymentModeHeader = row.createCell(cellSequenc);
		paymentModeHeader.setCellValue("Payment Mode");
		paymentModeHeader.setCellStyle(cellStyle);

		if (request.getFromTimestamp() == request.getToTimestamp()) {
			cellSequenc = cellSequenc + 1;
			Cell maidinMoneyHeader = row.createCell(cellSequenc);
			maidinMoneyHeader.setCellValue("Maidin Money");
			maidinMoneyHeader.setCellStyle(cellStyle);

			cellSequenc = cellSequenc + 1;
			Cell paidByMaidinMoneyHeader = row.createCell(cellSequenc);
			paidByMaidinMoneyHeader.setCellValue("Paid by Maidin Money");
			paidByMaidinMoneyHeader.setCellStyle(cellStyle);

			cellSequenc = cellSequenc + 1;
			Cell cashCollectHeader = row.createCell(cellSequenc);
			cashCollectHeader.setCellValue("Cash need to Collect ");
			cashCollectHeader.setCellStyle(cellStyle);
		}

		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 1, 0, 1);
		logger.info("HEADER createion end");
	}

	public static org.json.simple.JSONArray readTimeSlotJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray timeSlotArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			timeSlotArray = (org.json.simple.JSONArray) jsonObject.get("timeSlotList");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return timeSlotArray;
	}

	private org.json.simple.JSONArray readCityJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray cityArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			cityArray = (org.json.simple.JSONArray) jsonObject.get("cities");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return cityArray;
	}

	private org.json.simple.JSONArray readProjectJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray projectArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			projectArray = (org.json.simple.JSONArray) jsonObject.get("projects");

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return projectArray;
	}

	private org.json.simple.JSONArray readPaymentModeJSON(String filepath) {
		JSONParser parser = new JSONParser();
		org.json.simple.JSONArray paymentModeArray = null;
		try {
			Object obj = parser.parse(new FileReader(filepath));
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
			paymentModeArray = (org.json.simple.JSONArray) jsonObject.get("paymentModeList");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return paymentModeArray;

		/*
		 * org.json.simple.JSONObject paymentMode = new
		 * org.json.simple.JSONObject(); for (int i = 0; i <
		 * paymentModeArray.size(); i++) { org.json.simple.JSONObject
		 * timeSlotJSON = (org.json.simple.JSONObject) paymentModeArray.get(i);
		 * if ((Long) timeSlotJSON.get("id") == (paymentModeId)) {
		 * paymentMode.put("Id", (Long) timeSlotJSON.get("id"));
		 * paymentMode.put("paymentMode", (String)
		 * timeSlotJSON.get("paymentMode"));
		 * 
		 * break; }
		 * 
		 * } return paymentMode;
		 */}

	private String paymentMode(org.json.simple.JSONArray paymentModeArray, int timeSlotId) {
		String paymentMode = null;
		for (int i = 0; i < paymentModeArray.size(); i++) {
			org.json.simple.JSONObject timeSlotJSON = (org.json.simple.JSONObject) paymentModeArray.get(i);
			if ((Long) timeSlotJSON.get("id") == (timeSlotId)) {
				return (String) timeSlotJSON.get("paymentMode");

			}

		}
		return paymentMode;

	}

	private org.json.simple.JSONObject readTimeSlotArray(org.json.simple.JSONArray timeSlotArray, int timeSlotId)
			throws JSONException {
		org.json.simple.JSONObject timeslotRequest = new org.json.simple.JSONObject();
		for (int i = 0; i < timeSlotArray.size(); i++) {
			org.json.simple.JSONObject timeSlotJSON = (org.json.simple.JSONObject) timeSlotArray.get(i);
			if ((Long) timeSlotJSON.get("id") == (timeSlotId)) {
				timeslotRequest.put("Id", (Long) timeSlotJSON.get("id"));
				timeslotRequest.put("timeSlot", (String) timeSlotJSON.get("timeSlot"));
				timeslotRequest.put("lockingPeriod", (Long) timeSlotJSON.get("lockingPeriod"));
				timeslotRequest.put("slotStartTime", (Long) timeSlotJSON.get("slotStartTime"));
				timeslotRequest.put("slotEndTime", (Long) timeSlotJSON.get("slotEndTime"));
				break;
			}

		}
		return timeslotRequest;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addParticularDayInSchedule(Long timeStamp) {
		// GenericResponse response = new GenericResponse();
		System.out.println("I am entered in batch class");
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timeStamp);
		Date deliveryDate = new Date(c.getTimeInMillis());
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };
		// Day_OF_WEEK starts from 1 while array index starts from 0
		System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
		List<ScheduleInfoDTO> scheduleInfoList = null;
		if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfSunday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfMonday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfTuesday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfWednesday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfThursday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfFriday(ScheduleStatus.RESUME.getId());
		} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday")) {
			scheduleInfoList = scheduleInfoDAO.getSchedulesOfSaturday(ScheduleStatus.RESUME.getId());
		}

		for (ScheduleInfoDTO scheduleInfoDTO : scheduleInfoList) {
			if (scheduleInfoDTO.getPauseDate() != null) {
				// need to check its checking date or date and time
				Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
				if (pauseDate != null && (!getZeroTimeDate(pauseDate).equals(getZeroTimeDate(deliveryDate)))) {
					addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
				} else {
					addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
				}
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addOrderInSchedule() throws Exception {
		final Long batchStartDate = Long.parseLong(prop.getProperty(ResourceProperties.BATCH_START_DATE));
		final Long batchEndDate = Long.parseLong(prop.getProperty(ResourceProperties.BATCH_END_DATE));

		// GenericResponse response = new GenericResponse();
		System.out.println("I am entered in batch class");
		Calendar originalDate = Calendar.getInstance();
		System.out.println("The original Date is : " + originalDate.getTime());
		Calendar nextMonthDate = (Calendar) originalDate.clone();
		nextMonthDate.add(Calendar.MONTH, 1);
		System.out.println("The Next month date is: " + nextMonthDate.getTime());
		nextMonthDate.set(Calendar.DATE, nextMonthDate.getActualMinimum(Calendar.DAY_OF_MONTH));
		long batchStartTime = 0;
		long batchEndTime = 0;
		if (batchStartDate == 0 && batchEndDate == 0) {
			batchStartTime = nextMonthDate.getTimeInMillis();
			nextMonthDate.set(Calendar.DATE, nextMonthDate.getActualMaximum(Calendar.DAY_OF_MONTH));
			batchEndTime = nextMonthDate.getTimeInMillis();
		} else {
			batchStartTime = batchStartDate;
			batchEndTime = batchEndDate;
		}
		Date monthStartDate = new Date(batchStartTime);
		Date monthEndDate = new Date(batchEndTime);
		System.out.println(monthStartDate);
		System.out.println(monthEndDate);
		List<Date> dateList = getDaysBetweenDates(monthStartDate, monthEndDate);
		for (Date deliveryDate : dateList) {
			Calendar c = toCalendar(deliveryDate);
			System.out.println(deliveryDate);
			String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
					"Saturday" };
			System.out.println("Next day is : " + strDays[c.get(Calendar.DAY_OF_WEEK) - 1]);
			List<ScheduleInfoDTO> scheduleInfoList = null;
			if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfSunday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfMonday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfTuesday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfWednesday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfThursday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfFriday(ScheduleStatus.RESUME.getId());
			} else if (strDays[c.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday")) {
				scheduleInfoList = scheduleInfoDAO.getSchedulesOfSaturday(ScheduleStatus.RESUME.getId());
			}

			for (ScheduleInfoDTO scheduleInfoDTO : scheduleInfoList) {
				System.out.println("ScheduleId :" + scheduleInfoDTO.getId());
				if (scheduleInfoDTO.getPauseDate() != null) {
					Date pauseDate = getPauseDeliveryDate(scheduleInfoDTO);
					if (pauseDate != null && !getZeroTimeDate(pauseDate).equals(getZeroTimeDate(deliveryDate))) {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					} else {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					}
					if (getZeroTimeDate(deliveryDate).after(getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()))) {
						scheduleInfoDTO.setScheduledUpto(deliveryDate);
					}

				} else if (scheduleInfoDTO.getScheduledUpto() == null) {
					addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
					scheduleInfoDTO.setScheduledUpto(deliveryDate);
				} else {
					if (getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()).before(getZeroTimeDate(deliveryDate))) {
						addOrderToOrderInfo(scheduleInfoDTO, deliveryDate);
						if (getZeroTimeDate(deliveryDate).after(getZeroTimeDate(scheduleInfoDTO.getScheduledUpto()))) {
							scheduleInfoDTO.setScheduledUpto(deliveryDate);
						}
					}
				}

			}
		}

		System.out.println("Batch Completed");
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SearchProductListResponse searchProductList(String authToken, SearchProductRequest request, Locale locale)
			throws Exception {
		SearchProductListResponse response = new SearchProductListResponse();
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		List<ProductDTO> pdto = productDAO.getAllMatchingProducts(request.getProductSubString());
		if (pdto == null) {
		} else {
			for (ProductDTO dto : pdto) {
				SearchedProductDetails searchedProductDetails = new SearchedProductDetails();
				searchedProductDetails.setProductId(dto.getId());
				searchedProductDetails.setProductName(dto.getProductName());
				searchedProductDetails.setQuantityUnit(dto.getQuantityUnit());
				searchedProductDetails.setPrice(dto.getPrice());
				searchedProductDetails.setActiveStatus(dto.isActive());

				// TODO: make these dynamic
				searchedProductDetails.setStockStatus(true);
				searchedProductDetails.setDemandStatus(true);

				if (dto.getBrandId() == 0 || dto.getBrandId() == null) {
					continue;
				}

				ProductBrandsDTO brandsDTO = brandDAO.getById(dto.getBrandId());

				searchedProductDetails.setBrandName(brandsDTO.getBrand());
				searchedProductDetails.setBrandId(brandsDTO.getId());

				ProductCategoryDTO categoryDTO = categoryDAO.getById(brandsDTO.getCategoryId());
				searchedProductDetails.setCategoryName(categoryDTO.getCategory());
				searchedProductDetails.setCategoryId(categoryDTO.getId());

				ImageDTO imageDTO = imageDAO.getByProductId(ImageCategory.PRODUCT.getName(), dto.getId().longValue());
				if (imageDTO != null && imageDTO.getUpdatedOn() != null) {
					searchedProductDetails.setImageLastUpdatedOn(imageDTO.getUpdatedOn().getTime());
				}
				response.getProductList().add(searchedProductDetails);
			}
		}
		System.out.println("RESPONSE" + response);
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}

	private Date getPauseDeliveryDate(ScheduleInfoDTO scheduleDTO) {
		String[] strDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
				"Saturday" };

		long currentTimeStamp = System.currentTimeMillis();
		Calendar todayCal = Calendar.getInstance();
		todayCal.setTimeInMillis(currentTimeStamp);
		Calendar pauseTime = Calendar.getInstance();
		if (scheduleDTO.getPauseDate() != null) {
			pauseTime.setTimeInMillis(scheduleDTO.getPauseDate().getTime());
			TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleDTO.getTimeSlotId());
			List<Long> timeStampList = new ArrayList<>();
			int startDay = 0;
			int endDay = 7;
			if ((pauseTime.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getStartTime()))) {
				startDay = 0;
				if (getZeroTimeDate(scheduleDTO.getStartDate())
						.after(getZeroTimeDate(new Date(scheduleDTO.getPauseDate().getTime())))) {
					startDay = 1;
					endDay = 8;
				}

			} else {
				startDay = 1;
				endDay = 8;
			}
			/*
			 * } else{ if((pauseTime.get(Calendar.HOUR) < (lastLockingPeriod))){
			 * startDay=1; endDay =8; }
			 * 
			 * else{ startDay=2; endDay =9; } }
			 */
			for (; startDay < endDay; startDay++) {
				timeStampList.add(pauseTime.getTimeInMillis() + (startDay * 24 * 60 * 60 * 1000));

			}
			Calendar cal = Calendar.getInstance();
			for (Long timestamp : timeStampList) {
				cal.setTimeInMillis(timestamp);
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Sunday") & scheduleDTO.isSu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Monday") & scheduleDTO.isMo()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Tuesday") & scheduleDTO.isTu()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Wednesday") & scheduleDTO.isWe()) {
					return new Date(timestamp);

				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Thursday") & scheduleDTO.isTh()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Friday") & scheduleDTO.isFr()) {
					return new Date(timestamp);
				}
				if (strDays[cal.get(Calendar.DAY_OF_WEEK) - 1].equals("Saturday") & scheduleDTO.isSa()) {
					return new Date(timestamp);
				}

			}
		}

		return null;
	}

	boolean checkTimeSlotValidation(ScheduleInfoDTO scheduleDTO, boolean sameDay, Calendar todayCal,
			TimeSlotDTO timeSlotDTO, long timestamp) {

		if ((sameDay) && ((scheduleDTO.getTimeSlotId() == 1))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) < (timeSlotDTO.getEndTime()))) {
			return false;
		} else if (sameDay && (scheduleDTO.getTimeSlotId() > 1
				&& todayCal.get(Calendar.HOUR_OF_DAY) > (timeSlotDTO.getStartTime() - timeSlotDTO.getLockingHours()))) {
			return false;
		}

		return true;

	}

	public static Calendar toCalendar(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	public List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (getZeroTimeDate(new Date(calendar.getTime().getTime())).before(getZeroTimeDate(enddate))
				|| getZeroTimeDate(new Date(calendar.getTime().getTime())).equals(getZeroTimeDate(enddate))) {
			Date result = new Date(calendar.getTime().getTime());
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		return dates;
	}

	private void addOrderToOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {
		OrderInfoDTO orderInfoDTO = orderInfoDAO.getByScheduleIdDeliveryDate(scheduleInfoDTO.getId(), deliveryDate);
		if (orderInfoDTO == null) {
			Long orderId = addOrderInfo(scheduleInfoDTO, deliveryDate);
			addOrderDetailsInfo(orderId, scheduleInfoDTO);

		}

	}

	private void addOrderDetailsInfo(long orderId, ScheduleInfoDTO scheduleInfoDTO) {

		OrderDetailsInfoDTO orderDetailsDTO = new OrderDetailsInfoDTO();
		orderDetailsDTO.setOrderId(orderId);
		orderDetailsDTO.setProductId(scheduleInfoDTO.getProductId());
		orderDetailsDTO.setQuantity(scheduleInfoDTO.getQuantity());
		orderDetailsDTO.setOrderStatus(OrderStatus.CONFIRMED.getName());
		orderDetailsDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
		orderDetailsDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		orderDetailsDTO.setCancelled(false);
		orderDetailsDTO.setOrderCancellationTime(null);
		orderDetailsDTO.setPaymentModeId(0);
		orderDetailsDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		orderDetailsInfoDAO.addOrderDetailsInfo(orderDetailsDTO);

	}

	private Long addOrderInfo(ScheduleInfoDTO scheduleInfoDTO, Date deliveryDate) {

		OrderInfoDTO orderDTO = new OrderInfoDTO();
		orderDTO.setCustomerId(scheduleInfoDTO.getCustomerId());
		// orderDTO.setOrderedTime(new Timestamp(System.currentTimeMillis()));
		orderDTO.setDeliveryDate(deliveryDate);
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(scheduleInfoDTO.getTimeSlotId());
		orderDTO.setTimeSlot(timeSlotDTO.getTimeSlot());
		orderDTO.setTimeSlotId(timeSlotDTO.getId());
		orderDTO.setPaymentModeId(0);
		orderDTO.setScheduleId(scheduleInfoDTO.getId());
		orderDTO.setOrderedTime(scheduleInfoDTO.getCreatedOn());
		CustomerDTO customerDTO = customerDAO.getById(scheduleInfoDTO.getCustomerId());
		// AddressDTO addressDTO =
		// addressDAO.getById(customerDTO.getAddressId());
		orderDTO.setAddressId(customerDTO.getId());
		// orderDTO.setFlatId(addressDTO.getFlatId());
		/*
		 * orderDTO.setDeliveryLocation("Flat no. " + addressDTO.getFlat() +
		 * ", " + addressDTO.getTower() + ", " + addressDTO.getProject() + ", "
		 * + addressDTO.getCity());
		 */
		orderDTO.setCancelled(false);
		// By Pushpinder
		orderDTO.setOverallOrderStatus(OrderStatus.CONFIRMED.getId());
		return orderInfoDAO.addOrderInfo(orderDTO);

	}

	private Date getZeroTimeDate(Date dateValue) {
		Date res = dateValue;
		Calendar calendar = Calendar.getInstance();

		calendar.setTime(dateValue);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);

		res = new Date(calendar.getTime().getTime());

		return res;
	}

	private CustomerOrderReport generateOrderReport(OrderRequest request, Locale locale) {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			customerList = orderInfoDAO.getDifferentCustomersByTimestamp(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlotId(
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		for (Long customerId : customerList) {
			WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
			float maidinMoney = 0;
			if (walletDTO != null) {
				maidinMoney = walletDTO.getMyWalletBalance();
			}
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				continue;
			}
			List<OrderInfoDTO> orderInfoDTOList = null;
			List<Integer> timeSlotList = null;
			List<Date> dateList = orderInfoDAO.getByCustomerDifferentDeliveryDates(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (Date deliveryDate : dateList) {
				if (request.getTimeSlot() == 0) {
					timeSlotList = orderInfoDAO.getTimeSlotByCustomerDeliveryDate(customerId, deliveryDate);
				} else if (request.getTimeSlot() > 0) {
					timeSlotList = orderInfoDAO.getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(customerId,
							deliveryDate, timeSlotDTO.getId());
				}
				for (Integer timeSlotId : timeSlotList) {
					float cc = 0;
					float mm = 0;

					TimeSlotInfo timeSlot = new TimeSlotInfo();
					timeSlot.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotId(customerId, deliveryDate,
							timeSlotId);
					for (OrderInfoDTO orderInfo : orderInfoDTOList) {

						List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
								.getSettledOrderDetailsByOrderId(orderInfo.getId());

						orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot
								+ orderDetailsInfoList.size();
						for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
							OrderFormat orderRow = new OrderFormat();
							float tc = 0;
							float tm = 0;
							if (customerDTO != null) {
								orderRow.setName(customerDTO.getFullName());
								orderRow.setProject(addressDTO.getProject());
								orderRow.setTower(addressDTO.getTower());
								orderRow.setFlat(addressDTO.getFlat());
								orderRow.setMobile(customerDTO.getMobileNumber());
								orderRow.setEmail(customerDTO.getEmail());
								orderRow.setDeliverySlot(orderInfo.getTimeSlot());
								ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
								if (productDTO == null)
									continue;
								orderRow.setProduct(productDTO.getProductName());
								orderRow.setUnit(productDTO.getQuantityUnit());
								ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
								orderRow.setBrand(brandDTO.getBrand());
								ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
								if (categoryDTO == null)
									continue;
								orderRow.setCategory(categoryDTO.getCategory());
								orderRow.setQuantity(orderDetailsInfo.getQuantity());
								float billAmount = 0;
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									billAmount = (orderDetailsInfo.getProductMRP() > 0
											? orderDetailsInfo.getProductMRP() : productDTO.getPrice())
											* orderDetailsInfo.getQuantity();
								}
								orderRow.setBillAmount(billAmount);

								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									if (maidinMoney >= billAmount) {
										maidinMoney = maidinMoney - billAmount;
										orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
										orderRow.setCashCollect(0);
										orderRow.setPaidByMaidinMoney(billAmount);
									} else if (maidinMoney > 0 && billAmount > maidinMoney) {
										orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
										orderRow.setCashCollect(billAmount - maidinMoney);
										orderRow.setPaidByMaidinMoney(maidinMoney);
										maidinMoney = 0;
									} else {
										orderRow.setPaymentMode(PaymentMode.ByCash.getName());
										orderRow.setCashCollect(billAmount);
										orderRow.setPaidByMaidinMoney(0);
									}
								} else {
									if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
										orderRow.setPaymentMode("NA");
										List<CustomerTransactionDTO> transactionDTOList = transactionDAO
												.getByOrderDetailsId(orderDetailsInfo.getId());
										if (transactionDTOList == null || transactionDTOList.isEmpty()) {
											orderRow.setCashCollect(0);
											orderRow.setPaidByMaidinMoney(0);
										} else {

											for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
												if ((transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId())
														|| (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
																.getId())) {
													tc = tc + (transactionDTO.getMoneyOut()
															- transactionDTO.getMoneyIn());
												} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
														.getId()) {
													tm = tm + (transactionDTO.getMoneyOut()
															- transactionDTO.getMoneyIn());
												}
											}
											orderRow.setCashCollect(tc);
											orderRow.setPaidByMaidinMoney(tm);
										}
									} else if (orderDetailsInfo.getOrderStatus()
											.equals(OrderStatus.DELIVERED.getName())) {
										List<CustomerTransactionDTO> transactionDTOList = transactionDAO
												.getByOrderDetailsId(orderDetailsInfo.getId());
										if (transactionDTOList == null || transactionDTOList.isEmpty()) {
											if (orderInfo.getPaymentModeId() == PaymentMode.ByCash.getId()) {
												orderRow.setCashCollect(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByCash.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
												orderRow.setPaidByMaidinMoney(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
												orderRow.setCashCollect(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByPaytm.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByCashByWallet
													.getId()) {
												orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
											} else {
												PaymentModeDTO modeDTO = paymentModeDAO
														.getById(orderInfo.getPaymentModeId());
												if (modeDTO == null) {
													orderRow.setPaymentMode("Payment Not linked To Order");

												}
											}
										} else {
											boolean transaction = sameTransaction(transactionDTOList);
											if (!transaction) {
												orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
												for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
													if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash
															.getId()) {
														tc = tc + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
															.getId()) {
														tm = tm + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
															.getId()) {
														tc = tc + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													}
												}
												orderRow.setCashCollect(tc);
												orderRow.setPaidByMaidinMoney(tm);
											} else {
												for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
													if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash
															.getId()) {
														tc = tc + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByCash.getName());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
															.getId()) {
														tm = tm + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
															.getId()) {
														tc = tc + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByPaytm.getName());
													}
												}
												orderRow.setCashCollect(tc);
												orderRow.setPaidByMaidinMoney(tm);
											}

										}
									}
								}

								if (walletDTO != null) {
									orderRow.setMaidinMoney(walletDTO.getMyWalletBalance());
								}
								orderRow.setOrderedTime(
										ApiUtils.dateTimeWithMonthNameFormat(orderInfo.getOrderedTime()));
								orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
								orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
								timeSlot.getOrderFormatList().add(orderRow);

								cc = cc + orderRow.getCashCollect();
								mm = mm + orderRow.getPaidByMaidinMoney();
							}
						}
					}
					timeSlot.setCashCollect(cc);
					timeSlot.setPaidyMaidinMoney(mm);
					customerOrder.getTimeSlotId().add(timeSlot);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}

		return response;

	}

	boolean samePaymentMode(List<OrderDetailsInfoDTO> orderDetailsList) {
		for (int i = 0; i < orderDetailsList.size(); i++) {
			if (orderDetailsList.get(0).getPaymentModeId() != orderDetailsList.get(i).getPaymentModeId()) {
				return false;
			}
		}
		return true;
	}

	boolean sameTransaction(List<CustomerTransactionDTO> transactionDTOList) {
		for (int i = 0; i < transactionDTOList.size(); i++) {
			if (transactionDTOList.get(0).getPaymentModeId() != transactionDTOList.get(i).getPaymentModeId()) {
				return false;
			}
		}
		return true;
	}

	private void convertSingleSlotOrderReportToExcel(OrderRequest request, CustomerOrderReport response,
			AdminDTO adminDTO) throws FileNotFoundException, IOException, MessagingException, JSONException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createSingleSlotOrderReportHeader(sheet, rowCount, request, adminDTO);
		rowCount = 1; // two headers added
		int rf = 2;
		int rt = 0;

		int sNo = 1;

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);
				boolean isByCashAndWallet = false;

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
					cellStyle.setDataFormat(dateFormat);

					int cellSequence = 0;
					Cell sNoVal = row2.createCell(cellSequence);
					sNoVal.setCellValue(sNo);
					sNoVal.setCellStyle(textStyle);

					sNo++;

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell categoryVal = row2.createCell(cellSequence);
					categoryVal.setCellValue(rec.getCategory());
					categoryVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paymentModeVal = row2.createCell(cellSequence);
					paymentModeVal.setCellValue(rec.getPaymentMode());
					paymentModeVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell maidinMoneyVal = row2.createCell(cellSequence);
					maidinMoneyVal.setCellValue(rec.getMaidinMoney());
					maidinMoneyVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paidByMaidinMoneyVal = row2.createCell(cellSequence);
					paidByMaidinMoneyVal.setCellValue(timeSlotInfo.getPaidyMaidinMoney());
					paidByMaidinMoneyVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell cashCollectVal = row2.createCell(cellSequence);
					cashCollectVal.setCellValue(timeSlotInfo.getCashCollect());
					cashCollectVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 182, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(55, 178, 73)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (rt != rf) {
					// merge Customer name
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

					// merge project
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

					// merge tower
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

					// merge flat
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

					// merge mobile number
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

					// merge Maidin Money
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 13, 13));

					// merge Paid by Maidin Money
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 14, 14));

					// merge Cash need to Collect
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 15, 15));

					// merge Ordered Time
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));
					// }

					rf = rt + 1;
					rt = rt + timeSlotInfo.getOrderFormatList().size();
				} else {
					rf++;
				}
			}

			// set serial No width to autoFit
			sheet.autoSizeColumn(0);

			// limit Customer name width to 15 characters
			sheet.setColumnWidth(1, 256 * 15);

			// limit Project name width to 20 characters
			sheet.setColumnWidth(2, 256 * 20);

			// limit Tower name width to 10 characters
			sheet.setColumnWidth(3, 256 * 10);

			// limit Flat name width to 10 characters
			sheet.setColumnWidth(4, 256 * 10);

			// limit Mobile number width to 13 characters
			sheet.setColumnWidth(5, 256 * 13);

			// limit category width to 15 characters
			sheet.setColumnWidth(6, 256 * 15);

			// limit brand width to 15 characters
			sheet.setColumnWidth(7, 256 * 15);

			// limit product name width to 20 characters
			sheet.setColumnWidth(8, 256 * 20);

			// limit unit width to 10 characters
			sheet.setColumnWidth(9, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(10, 256 * 5);

			// limit bill amount width to 9 characters
			sheet.setColumnWidth(11, 256 * 9);

			// set payment mode to auto fit
			sheet.autoSizeColumn(12);

			// limit Maidin money width to 9 characters
			sheet.setColumnWidth(13, 256 * 9);

			// limit paid by maidin money width to 9 characters
			sheet.setColumnWidth(14, 256 * 9);

			// limit Cash need to Collect width to 9 characters
			sheet.setColumnWidth(15, 256 * 9);

			// limit Ordered Time width to 14 characters
			sheet.setColumnWidth(16, 256 * 14);

			// set Order Status to auto fit
			sheet.autoSizeColumn(17);

		}
		org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray, request.getTimeSlot());
		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String finalDateString = fromDate;
		File file = new File("OrderReport " + " - "
				+ (jsonObject.get("timeSlot").toString().equalsIgnoreCase("All") ? "All timeslots"
						: jsonObject.get("timeSlot"))
				+ "    " + finalDateString + " - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : "
				+ jsonObject.get("timeSlot");
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);

	}

	private void convertOrderReportToExcel(OrderRequest request, CustomerOrderReport response, String senderEmail)
			throws FileNotFoundException, IOException, MessagingException, JSONException {
		logger.info("Excel creation started with formatting");
		boolean isOneDayReport = false;
		if (request.getFromTimestamp() == request.getToTimestamp()) {
			isOneDayReport = true;
		} else {
			isOneDayReport = false;
		}
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createOrderReportheader(sheet, rowCount, request);
		int rf = 1;
		int rt = 0;

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);
				boolean isByCashAndWallet = false;

				// check ByCashByWallet > if ByCashByWallet is present then set
				// all payment modes to ByCashByWallet
				// for (int i = 0; i < timeSlotInfo.getOrderFormatList().size();
				// ++i) {
				// if (!isByCashAndWallet &&
				// timeSlotInfo.getOrderFormatList().get(i).getPaymentMode().equalsIgnoreCase("ByCashByWallet"))
				// {
				// isByCashAndWallet = true;
				// for (int k = 0; k < timeSlotInfo.getOrderFormatList().size();
				// ++k) {
				// timeSlotInfo.getOrderFormatList().get(k).setPaymentMode("ByCashByWallet");
				// }
				// break;
				// }
				// }

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
					cellStyle.setDataFormat(dateFormat);
					int cellSequence = 0;
					Cell del_DateVal = row2.createCell(cellSequence);
					del_DateVal.setCellValue(rec.getDeliveryDate());
					del_DateVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell del_SlotVal = row2.createCell(cellSequence);
					del_SlotVal.setCellValue(rec.getDeliverySlot());
					del_SlotVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					// if (!isOneDayReport) {
					cellSequence = cellSequence + 1;
					Cell catVal = row2.createCell(cellSequence);
					catVal.setCellValue(rec.getCategory());
					catVal.setCellStyle(textStyle);
					// }

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell paymentModeVal = row2.createCell(cellSequence);
					paymentModeVal.setCellValue(rec.getPaymentMode());
					paymentModeVal.setCellStyle(cellStyle);

					if (isOneDayReport) {
						cellSequence = cellSequence + 1;
						Cell maidinMoneyVal = row2.createCell(cellSequence);
						maidinMoneyVal.setCellValue(rec.getMaidinMoney());
						maidinMoneyVal.setCellStyle(integerValueStyle);

						cellSequence = cellSequence + 1;
						Cell paidByMaidinMoneyVal = row2.createCell(cellSequence);
						paidByMaidinMoneyVal.setCellValue(timeSlotInfo.getPaidyMaidinMoney());
						paidByMaidinMoneyVal.setCellStyle(integerValueStyle);

						cellSequence = cellSequence + 1;
						Cell cashCollectVal = row2.createCell(cellSequence);
						cashCollectVal.setCellValue(timeSlotInfo.getCashCollect());
						cashCollectVal.setCellStyle(integerValueStyle);
					}

					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 182, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(55, 178, 73)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (isOneDayReport) {
					if (rt != rf) {
						// merge Delivery Date
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 0, 0));

						// merge Delivery slot
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

						// merge Customer name
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

						// merge project
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

						// merge tower
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

						// merge flat
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

						// merge mobile number
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 6, 6));

						// merge Payment Mode
						// sheet.addMergedRegion(new CellRangeAddress(rf, rt,
						// 12, 12));

						// merge Maidin Money
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 14, 14));

						// merge Paid by Maidin Money
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 15, 15));

						// merge Cash need to Collect
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));

						// merge Ordered Time
						sheet.addMergedRegion(new CellRangeAddress(rf, rt, 17, 17));

						rf = rt + 1;
						rt = rt + timeSlotInfo.getOrderFormatList().size();
					} else {
						rf++;
					}
				}
			}

		}

		// limit delivery date width to 12 characters
		sheet.setColumnWidth(0, 256 * 12);

		for (int i = 1; i < 7; i++) {
			sheet.autoSizeColumn(i);
		}
		for (int i = 7; i < 10; i++) {
			sheet.autoSizeColumn(i);
		}
		// limit Customer name width to 15 characters
		sheet.setColumnWidth(2, 256 * 15);

		// limit Project name width to 20 characters
		sheet.setColumnWidth(3, 256 * 20);

		// limit Tower name width to 10 characters
		sheet.setColumnWidth(4, 256 * 10);

		// limit Flat name width to 10 characters
		sheet.setColumnWidth(5, 256 * 10);

		// limit Mobile number width to 13 characters
		sheet.setColumnWidth(6, 256 * 13);

		sheet.autoSizeColumn(13);
		sheet.autoSizeColumn(16);

		if (!isOneDayReport) {
			// limit category width to 18 characters
			sheet.setColumnWidth(7, 256 * 18);

			// limit brand width to 15 characters
			sheet.setColumnWidth(8, 256 * 15);

			// limit product name width to 25 characters
			sheet.setColumnWidth(9, 256 * 25);

			// limit unit width to 10 characters
			sheet.setColumnWidth(10, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(11, 256 * 5);

			// limit bill amount width to 8 characters
			sheet.setColumnWidth(12, 256 * 8);

			// limit payment mode width to 16 characters
			sheet.setColumnWidth(13, 256 * 16);

			sheet.autoSizeColumn(14);
			sheet.autoSizeColumn(15);
		} else {
			// limit category width to 18 characters
			sheet.setColumnWidth(7, 256 * 18);

			// limit brand width to 15 characters
			sheet.setColumnWidth(8, 256 * 15);

			// limit product name width to 25 characters
			sheet.setColumnWidth(9, 256 * 25);

			// limit unit width to 10 characters
			sheet.setColumnWidth(10, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(11, 256 * 5);

			// limit bill amount width to 8 characters
			sheet.setColumnWidth(12, 256 * 8);

			// limit Payment mode width to 16 characters
			sheet.setColumnWidth(13, 256 * 16);

			// limit Cash need to collect width to 8 characters
			sheet.setColumnWidth(16, 256 * 8);

			// limit Ordered time width to 13 characters
			sheet.setColumnWidth(17, 256 * 13);

			// limit Order status width to auto fit
			sheet.autoSizeColumn(18);

		}
		logger.info("Excel creation END with formatting");
		org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray, request.getTimeSlot());
		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String toDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getToTimestamp()));
		String finalDateString = isOneDayReport ? fromDate : fromDate + "  TO  " + toDate;
		File file = new File("OrderReport " + " - " + (jsonObject.get("timeSlot").toString().equalsIgnoreCase("All")
				? "All timeslots" : jsonObject.get("timeSlot")) + "    " + finalDateString + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : "
				+ jsonObject.get("timeSlot");
		EmailSenderWithAttachment.sendMail(senderEmail, subject, "Attachment", multipart);
		logger.info("Order report email SENT");
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public GenericResponse
	 * batchPriceDeductionForScheduledOrders(int timeSlotId) { GenericResponse
	 * response = new GenericResponse(); System.out.println(
	 * "Batch Started for price deduction for schedule/checkout at " + new
	 * Date(System.currentTimeMillis()) + " for timeSlotId a:" + timeSlotId);
	 * List<OrderInfoDTO> orderInfoList =
	 * orderInfoDAO.getScheduleOrdersByTimeSlots(timeSlotId);
	 * System.out.println("Total ScheduleCheckoutCount" + orderInfoList.size());
	 * for (OrderInfoDTO orderInfo : orderInfoList) { System.out.println(
	 * "Customer Name " +
	 * customerDAO.getById(orderInfo.getCustomerId()).getFullName() +
	 * "OrderInfoId : " + orderInfo.getId() + " Schedule Id: " +
	 * orderInfo.getScheduleId()); List<OrderDetailsInfoDTO> orderDetailsList =
	 * orderDetailsInfoDAO .getUnSettledOrdersByOrderId(orderInfo.getId()); for
	 * (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsList) { if
	 * (orderInfo.getScheduleId() != null) { ProductDTO productDTO =
	 * productDAO.getById(orderDetailsInfo.getProductId());
	 * orderDetailsInfo.setProductMRP(productDTO.getPrice());
	 * orderDetailsInfo.setTotalProductCost(productDTO.getPrice() *
	 * orderDetailsInfo.getQuantity()); orderDetailsInfo.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis())); } else if
	 * (orderDetailsInfo.getProductMRP() == 0) { ProductDTO productDTO =
	 * productDAO.getById(orderDetailsInfo.getProductId());
	 * orderDetailsInfo.setProductMRP(productDTO.getPrice());
	 * orderDetailsInfo.setTotalProductCost(productDTO.getPrice() *
	 * orderDetailsInfo.getQuantity()); orderDetailsInfo.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis())); } else if
	 * (orderDetailsInfo.getTotalProductCost() == 0) { orderDetailsInfo
	 * .setTotalProductCost(orderDetailsInfo.getProductMRP() *
	 * orderDetailsInfo.getQuantity()); orderDetailsInfo.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis())); } float amountToPay =
	 * orderDetailsInfo.getTotalProductCost() -
	 * orderDetailsInfo.getAmountPaidByCustomer(); WalletDTO walletDTO =
	 * walletDAO.getByCustomerId(orderInfo.getCustomerId()); if (walletDTO !=
	 * null && walletDTO.getMyWalletBalance() > 0 && amountToPay > 0) {
	 * orderDetailsInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
	 * CustomerTransactionRequest transactionRequest = new
	 * CustomerTransactionRequest();
	 * transactionRequest.setCustomerId(orderInfo.getCustomerId());
	 * transactionRequest.setOrderId(orderDetailsInfo.getOrderId());
	 * transactionRequest.setOrderDetailsId(orderDetailsInfo.getId());
	 * transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId(
	 * )); if (walletDTO.getMyWalletBalance() >=
	 * orderDetailsInfo.getTotalProductCost()) {
	 * orderDetailsInfo.setAmountPaidByCustomer(orderDetailsInfo.
	 * getTotalProductCost());
	 * orderDetailsInfo.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName())
	 * ; orderDetailsInfo.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis()));
	 * transactionRequest.setMoneyOut(orderDetailsInfo.getTotalProductCost());
	 * transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
	 * 
	 * transactionRequest.setDueAmount(0);
	 * addCustomerTransaction(transactionRequest, -1);
	 * updateWalletBalance(orderInfo.getCustomerId(),
	 * orderDetailsInfo.getTotalProductCost(),
	 * WalletDescription.DEDUCTIONS.getName());
	 * orderDetailsInfo.setPaymentModeId(PaymentMode.ByWallet.getId());
	 * orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis())); } else
	 * if (walletDTO.getMyWalletBalance() <
	 * orderDetailsInfo.getTotalProductCost()) {
	 * orderDetailsInfo.setAmountPaidByCustomer(walletDTO.getMyWalletBalance());
	 * orderDetailsInfo.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName())
	 * ; orderDetailsInfo.setUpdatedOn(new
	 * Timestamp(System.currentTimeMillis()));
	 * transactionRequest.setMoneyOut(walletDTO.getMyWalletBalance());
	 * transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
	 * transactionRequest .setDueAmount(orderDetailsInfo.getTotalProductCost() -
	 * walletDTO.getMyWalletBalance());
	 * addCustomerTransaction(transactionRequest, -1);
	 * updateWalletBalance(orderInfo.getCustomerId(),
	 * walletDTO.getMyWalletBalance(), WalletDescription.DEDUCTIONS.getName());
	 * orderDetailsInfo.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
	 * 
	 * orderInfo.setUpdatedOn(new Timestamp(System.currentTimeMillis())); }
	 * 
	 * } } setPaymentModeId(orderInfo.getId()); } System.out.println(
	 * "Batch Ended for price deduction for schedule and checkout for timeSlotId :"
	 * + timeSlotId); response = ApiUtils.setResponseWithOperationId(response,
	 * OperationId.ADMIN_SERVICES); if (response.getStatus()) { TimeSlotDTO
	 * timeSlotDTO = timeSlotDAO.getById(timeSlotId);
	 * timeSlotDTO.setBatchupdateTime(new
	 * Timestamp(System.currentTimeMillis()));
	 * 
	 * } logger.info(ApiUtils.RESPONSE_PAYLOAD,
	 * ApiUtils.toJsonString(response)); return response; }
	 */

	private void setExcelCellBorder(CellStyle cellStyle) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
	}

	private long getUserCountByClientPlatform(String clientPlatform) {
		long result = 0l;
		result = customerDeviceDetailsDAO.getUsersCountByClientPlatform(clientPlatform);
		return result;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public OrderReportResponse getOrderReportV2(OrderRequest request, String authToken, Locale locale)
			throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		OrderReportResponse response = new OrderReportResponse();
		// String mobile = "7982340415";
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO == null) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.PERMISSION_DENIED, locale));
		}
		logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		if (Strings.isNullOrEmpty(adminDTO.getEmail())) {
			throw new ValidationException(
					ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.EMAIL_MISSING, locale));
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		new Thread(() -> {
			try {
				callAsynchronouslyReportV2(request, locale, adminDTO);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		return response;
	}

	private CustomerOrderReport getCustomerRecordsByDateV2(Date date, OrderRequest request,
			CustomerOrderReport response, Map<Long, Float> customerWallet) {
		// = new HashMap<Long, Float>();
		List<OrderInfoDTO> orderInfoList = orderInfoDAO.getByDeliveryDates(date);

		for (OrderInfoDTO orderInfo : orderInfoList) {
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(orderInfo.getCustomerId());
			CustomerDTO customerDTO = customerDAO.getById(orderInfo.getCustomerId());
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			WalletDTO walletDTO = walletDAO.getByCustomerId(orderInfo.getCustomerId());
			float maidinMoney = 0;

			if (!customerWallet.containsKey(orderInfo.getCustomerId())) {
				if (walletDTO != null) {
					maidinMoney = walletDTO.getMyWalletBalance();
				}
				customerWallet.put(orderInfo.getCustomerId(), maidinMoney);
			} else {
				maidinMoney = customerWallet.get(orderInfo.getCustomerId());
			}
			TimeSlotInfo timeSlot = new TimeSlotInfo();
			logger.info("Customer name : " + customerDTO.getFullName() + " ; Id : " + customerDTO.getId() + " Date :"
					+ date);
			List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
					.getSettledOrderDetailsByOrderId(orderInfo.getId());
			PaymentModeDTO paymentMode = paymentModeDAO.getById(orderInfo.getPaymentModeId());
			for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
				OrderFormat orderRow = new OrderFormat();
				if (customerDTO != null) {
					orderRow.setName(customerDTO.getFullName());
					orderRow.setProject(addressDTO.getProject());
					orderRow.setTower(addressDTO.getTower());
					orderRow.setFlat(addressDTO.getFlat());
					orderRow.setMobile(customerDTO.getMobileNumber());
					orderRow.setEmail(customerDTO.getEmail());
					orderRow.setDeliverySlot(orderInfo.getTimeSlot());
					ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
					if (productDTO == null)
						continue;
					orderRow.setProduct(productDTO.getProductName());
					orderRow.setUnit(productDTO.getQuantityUnit());
					ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
					orderRow.setBrand(brandDTO.getBrand());
					ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
					if (categoryDTO == null)
						continue;
					orderRow.setCategory(categoryDTO.getCategory());
					orderRow.setQuantity(orderDetailsInfo.getQuantity());
					float billAmount = 0;
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
						billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
					} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						billAmount = (orderDetailsInfo.getProductMRP() > 0 ? orderDetailsInfo.getProductMRP()
								: productDTO.getPrice()) * orderDetailsInfo.getQuantity();
					}
					orderRow.setBillAmount(billAmount);
					if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
						if (orderDetailsInfo.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
							orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
						} else if (orderDetailsInfo.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.getName())) {
							orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
						} else if (orderDetailsInfo.getPaymentStatus().equals(PaymentStatus.PENDING.getName())) {
							if (paymentMode != null) {
								orderRow.setPaymentMode(paymentMode.getPaymentMode());
							} else {
								if (maidinMoney > billAmount) {
									maidinMoney = maidinMoney - billAmount;
									orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
									customerWallet.put(orderInfo.getCustomerId(), maidinMoney);
								} else if (maidinMoney > 0 && billAmount > maidinMoney) {
									orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
									maidinMoney = 0;
									customerWallet.put(orderInfo.getCustomerId(), maidinMoney);
								} else {
									orderRow.setPaymentMode(PaymentMode.ByCash.getName());
								}

							}
						}
					} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.UNDELIVERED.getName())) {
						orderRow.setPaymentMode("NA");
					} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
						if (paymentMode == null) {
							orderRow.setPaymentMode("Payment Not linked To Order");
						} else {
							if (orderInfo.getPaymentModeId() == PaymentMode.ByCashByWallet.getId()) {
								List<Integer> paymentModeList = transactionDAO
										.getPaymentModeByOrderDetailsId(orderDetailsInfo.getId());
								Set<Integer> paymentModeSet = new HashSet<Integer>(paymentModeList);
								if (paymentModeSet.size() > 1) {
									orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
								} else if (paymentModeSet.size() == 1) {
									orderRow.setPaymentMode(paymentMode.getPaymentMode());
								} else {
									orderRow.setPaymentMode("Payment Not linked To Order");
								}

							} else {
								orderRow.setPaymentMode(paymentMode.getPaymentMode());
							}
						}
					}

					orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
					orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
					orderRow.setOrderedTime(ApiUtils.dateTimeFormat(orderInfo.getOrderedTime()));
					timeSlot.getOrderFormatList().add(orderRow);
				}
			}
			customerOrder.getTimeSlotId().add(timeSlot);
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;
	}

	@Async
	private void callAsynchronouslyReportV2(OrderRequest request, Locale locale, AdminDTO adminDTO)
			throws InterruptedException, ExecutionException {

		CustomerOrderReport reportResponse = new CustomerOrderReport();
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		try {
			if (request.getFromTimestamp() == request.getToTimestamp()) {
				if (request.getTimeSlot() != 0) {

					Calendar calendar = Calendar.getInstance();
					calendar.setTimeInMillis(System.currentTimeMillis());
					if (getZeroTimeDate(new Date(request.getFromTimestamp()))
							.before(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
						reportResponse = generateOrderReportV2(request, locale);
						convertSingleSlotOrderReportToExcel(request, reportResponse, adminDTO);
					} else if (getZeroTimeDate(new Date(request.getFromTimestamp()))
							.after(getZeroTimeDate(new Date(System.currentTimeMillis())))) {
						reportResponse = generateOrderReportWithOutAccountInfo(request, locale);
						convertSingleSlotOrderReportWithoutAccountInfoToExcel(request, reportResponse, adminDTO);
					} else {
						if (calendar.get(Calendar.HOUR_OF_DAY) >= (timeSlotDTO.getStartTime() - accountLockingHour)) {
							reportResponse = generateOrderReportV2(request, locale);
							convertSingleSlotOrderReportToExcel(request, reportResponse, adminDTO);
						} else {
							reportResponse = generateOrderReportWithOutAccountInfo(request, locale);
							convertSingleSlotOrderReportWithoutAccountInfoToExcel(request, reportResponse, adminDTO);
						}
					}
				} else {
					reportResponse = generateOrderReportV2(request, locale);
					convertOrderReportToExcel(request, reportResponse, adminDTO.getEmail());

				}
			} else {
				reportResponse = generateMultipleDayOrderReportV2(request, locale);
				convertOrderReportToExcel(request, reportResponse, adminDTO.getEmail());
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void convertSingleSlotOrderReportWithoutAccountInfoToExcel(OrderRequest request,
			CustomerOrderReport response, AdminDTO adminDTO)
			throws JSONException, MessagingException, FileNotFoundException, IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("OrderReport");
		XSSFCellStyle cellStyle = workbook.createCellStyle();
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		cellStyle.setWrapText(true);

		XSSFCellStyle integerValueStyle = workbook.createCellStyle();
		integerValueStyle.setAlignment(HorizontalAlignment.CENTER);
		integerValueStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle orderStatusStyle = workbook.createCellStyle();
		orderStatusStyle.setAlignment(HorizontalAlignment.CENTER);
		orderStatusStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		XSSFCellStyle textStyle = workbook.createCellStyle();
		textStyle.setAlignment(HorizontalAlignment.LEFT);
		textStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		textStyle.setWrapText(true);

		setExcelCellBorder(cellStyle);
		setExcelCellBorder(integerValueStyle);
		setExcelCellBorder(orderStatusStyle);
		setExcelCellBorder(textStyle);

		XSSFFont font = sheet.getWorkbook().createFont();

		CreationHelper createHelper = workbook.getCreationHelper();
		int rowCount = 0;
		createSingleSlotOrderReportWithoutAccountInfoHeader(sheet, rowCount, request, adminDTO);
		rowCount = 1; // two headers added
		int rf = 2;
		int rt = 0;

		int sNo = 1;

		for (CustomerOrderInfo customerOrderInfo : response.getCustomerOrderInfo()) {
			for (TimeSlotInfo timeSlotInfo : customerOrderInfo.getTimeSlotId()) {
				rt = rf + (timeSlotInfo.getOrderFormatList().size() - 1);
				boolean isByCashAndWallet = false;

				for (int i = 0; i < timeSlotInfo.getOrderFormatList().size(); ++i) {
					OrderFormat rec = timeSlotInfo.getOrderFormatList().get(i);
					Row row2 = sheet.createRow(++rowCount);
					short dateFormat = createHelper.createDataFormat().getFormat("yyyy-dd-MM");
					cellStyle.setDataFormat(dateFormat);

					int cellSequence = 0;
					Cell sNoVal = row2.createCell(cellSequence);
					sNoVal.setCellValue(sNo);
					sNoVal.setCellStyle(textStyle);

					sNo++;

					cellSequence = cellSequence + 1;
					Cell nameVal = row2.createCell(cellSequence);
					nameVal.setCellValue(rec.getName());
					nameVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell projVal = row2.createCell(cellSequence);
					projVal.setCellValue(rec.getProject());
					projVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell towerVal = row2.createCell(cellSequence);
					towerVal.setCellValue(rec.getTower());
					towerVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell flatVal = row2.createCell(cellSequence);
					flatVal.setCellValue(rec.getFlat());
					flatVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell mobVal = row2.createCell(cellSequence);
					mobVal.setCellValue(rec.getMobile());
					mobVal.setCellStyle(cellStyle);

					cellSequence = cellSequence + 1;
					Cell categoryVal = row2.createCell(cellSequence);
					categoryVal.setCellValue(rec.getCategory());
					categoryVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell brandVal = row2.createCell(cellSequence);
					brandVal.setCellValue(rec.getBrand());
					brandVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell productVal = row2.createCell(cellSequence);
					productVal.setCellValue(rec.getProduct());
					productVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell unitVal = row2.createCell(cellSequence);
					unitVal.setCellValue(rec.getUnit());
					unitVal.setCellStyle(textStyle);

					cellSequence = cellSequence + 1;
					Cell quantityVal = row2.createCell(cellSequence);
					quantityVal.setCellValue(rec.getQuantity());
					quantityVal.setCellStyle(integerValueStyle);

					cellSequence = cellSequence + 1;
					Cell billAmountVal = row2.createCell(cellSequence);
					billAmountVal.setCellValue(rec.getBillAmount());
					billAmountVal.setCellStyle(integerValueStyle);
					/*
					 * cellSequence = cellSequence + 1; Cell paymentModeVal =
					 * row2.createCell(cellSequence);
					 * paymentModeVal.setCellValue(rec.getPaymentMode());
					 * paymentModeVal.setCellStyle(cellStyle);
					 */

					/*
					 * cellSequence = cellSequence + 1; Cell maidinMoneyVal =
					 * row2.createCell(cellSequence);
					 * maidinMoneyVal.setCellValue(rec.getMaidinMoney());
					 * maidinMoneyVal.setCellStyle(integerValueStyle);
					 * 
					 * cellSequence = cellSequence + 1; Cell
					 * paidByMaidinMoneyVal = row2.createCell(cellSequence);
					 * paidByMaidinMoneyVal.setCellValue(timeSlotInfo.
					 * getPaidyMaidinMoney());
					 * paidByMaidinMoneyVal.setCellStyle(integerValueStyle);
					 * 
					 * cellSequence = cellSequence + 1; Cell cashCollectVal =
					 * row2.createCell(cellSequence);
					 * cashCollectVal.setCellValue(timeSlotInfo.getCashCollect()
					 * ); cashCollectVal.setCellStyle(integerValueStyle);
					 */

					cellSequence = cellSequence + 1;
					Cell orderedTimeVal = row2.createCell(cellSequence);
					orderedTimeVal.setCellValue(rec.getOrderedTime());
					orderedTimeVal.setCellStyle(cellStyle);

					XSSFRichTextString richTextString = new XSSFRichTextString(rec.getOrderStatus());

					if (rec.getOrderStatus().equalsIgnoreCase("CONFIRMED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 182, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("DELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(55, 178, 73)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else if (rec.getOrderStatus().equalsIgnoreCase("UNDELIVERED")) {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(255, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					} else {
						font.setColor(null);
						font.setColor(new XSSFColor(new java.awt.Color(0, 0, 0)));
						richTextString.applyFont(0, rec.getOrderStatus().length(), font);
					}

					cellSequence = cellSequence + 1;
					Cell orderStatusVal = row2.createCell(cellSequence);
					orderStatusVal.setCellValue(rec.getOrderStatus());
					orderStatusVal.setCellStyle(orderStatusStyle);
					orderStatusVal.setCellValue(richTextString);
				}
				if (rt != rf) {
					// merge Customer name
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 1, 1));

					// merge project
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 2, 2));

					// merge tower
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 3, 3));

					// merge flat
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 4, 4));

					// merge mobile number
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 5, 5));

					/*
					 * // merge Maidin Money sheet.addMergedRegion(new
					 * CellRangeAddress(rf, rt, 13, 13));
					 * 
					 * // merge Paid by Maidin Money sheet.addMergedRegion(new
					 * CellRangeAddress(rf, rt, 14, 14));
					 * 
					 * // merge Cash need to Collect sheet.addMergedRegion(new
					 * CellRangeAddress(rf, rt, 15, 15));
					 */

					// merge Ordered Time
					sheet.addMergedRegion(new CellRangeAddress(rf, rt, 16, 16));

					rf = rt + 1;
					rt = rt + timeSlotInfo.getOrderFormatList().size();
				} else {
					rf++;
				}
			}

			// set serial No width to autoFit
			sheet.autoSizeColumn(0);

			// limit Customer name width to 15 characters
			sheet.setColumnWidth(1, 256 * 15);

			// limit Project name width to 20 characters
			sheet.setColumnWidth(2, 256 * 20);

			// limit Tower name width to 10 characters
			sheet.setColumnWidth(3, 256 * 10);

			// limit Flat name width to 10 characters
			sheet.setColumnWidth(4, 256 * 10);

			// limit Mobile number width to 13 characters
			sheet.setColumnWidth(5, 256 * 13);

			// limit category width to 15 characters
			sheet.setColumnWidth(6, 256 * 15);

			// limit brand width to 15 characters
			sheet.setColumnWidth(7, 256 * 15);

			// limit product name width to 20 characters
			sheet.setColumnWidth(8, 256 * 20);

			// limit unit width to 10 characters
			sheet.setColumnWidth(9, 256 * 10);

			// limit quantity width to 5 characters
			sheet.setColumnWidth(10, 256 * 5);

			// limit bill amount width to 9 characters
			sheet.setColumnWidth(11, 256 * 9);

			// set payment mode to auto fit
			sheet.autoSizeColumn(12);

			// limit Maidin money width to 9 characters
			/*
			 * sheet.setColumnWidth(13, 256 * 9);
			 * 
			 * // limit paid by maidin money width to 9 characters
			 * sheet.setColumnWidth(14, 256 * 9);
			 * 
			 * // limit Cash need to Collect width to 9 characters
			 * sheet.setColumnWidth(15, 256 * 9);
			 */

			// limit Ordered Time width to 14 characters
			sheet.setColumnWidth(16, 256 * 14);

			// set Order Status to auto fit
			sheet.autoSizeColumn(17);

		}
		org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray, request.getTimeSlot());
		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		String finalDateString = fromDate;
		File file = new File("OrderReport " + " - "
				+ (jsonObject.get("timeSlot").toString().equalsIgnoreCase("All") ? "All timeslots"
						: jsonObject.get("timeSlot"))
				+ "    " + finalDateString + " - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())) + ".xlsx");

		try (FileOutputStream outputStream = new FileOutputStream(file)) {
			workbook.write(outputStream);
			outputStream.close();
		}
		MimeBodyPart attachmentBodypart = new MimeBodyPart();
		attachmentBodypart.attachFile(file);
		Multipart multipart = new MimeMultipart();

		multipart.addBodyPart(attachmentBodypart);

		String subject = "Order Summary Report | Date :  " + finalDateString + " | Timeslot : "
				+ jsonObject.get("timeSlot");
		EmailSenderWithAttachment.sendMail(adminDTO.getEmail(), subject, "Attachment", multipart);

	}

	private void createSingleSlotOrderReportWithoutAccountInfoHeader(XSSFSheet sheet, int rowCount,
			OrderRequest request, AdminDTO adminDTO) throws JSONException {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		XSSFFont font = sheet.getWorkbook().createFont();
		font.setBold(true);
		font.setFontHeightInPoints((short) 12);
		font.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

		cellStyle.setFont(font);
		cellStyle.setWrapText(true);
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

		cellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		setExcelCellBorder(cellStyle);

		Row dateTimeSlotRow = sheet.createRow(rowCount);

		String fromDate = ApiUtils.dateWithMonthNameFormat(new Timestamp(request.getFromTimestamp()));
		org.json.simple.JSONObject jsonObject = readTimeSlotArray(timeSlotJsonArray, request.getTimeSlot());

		Cell dateTimeSlot = dateTimeSlotRow.createCell(0);
		dateTimeSlot.setCellValue("Delivery report of " + fromDate + " | " + jsonObject.get("timeSlot")
				+ ".  Downloaded by - " + adminDTO.getFullName() + " at "
				+ ApiUtils.dateTimeTwelveHourFormat(new Timestamp(System.currentTimeMillis())));
		dateTimeSlot.setCellStyle(cellStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 17));

		rowCount++;
		Row row = sheet.createRow(rowCount);
		int cellSequenc = 0;
		Cell sNoHeader = row.createCell(cellSequenc);
		sNoHeader.setCellValue("S No.");
		sNoHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell nameHeader = row.createCell(cellSequenc);
		nameHeader.setCellValue("Name");
		nameHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell projectHeader = row.createCell(cellSequenc);
		projectHeader.setCellValue("Project");
		projectHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell towerHeader = row.createCell(cellSequenc);
		towerHeader.setCellValue("Tower");
		towerHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell flatHeader = row.createCell(cellSequenc);
		flatHeader.setCellValue("Flat");
		flatHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell mobileHeader = row.createCell(cellSequenc);
		mobileHeader.setCellValue("Mobile");
		mobileHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell categoryHeader = row.createCell(cellSequenc);
		categoryHeader.setCellValue("Category");
		categoryHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell brandHeader = row.createCell(cellSequenc);
		brandHeader.setCellValue("Brand");
		brandHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell productHeader = row.createCell(cellSequenc);
		productHeader.setCellValue("Product");
		productHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell unitHeader = row.createCell(cellSequenc);
		unitHeader.setCellValue("Unit");
		unitHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell quantityHeader = row.createCell(cellSequenc);
		quantityHeader.setCellValue("Qty");
		quantityHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell billAmountHeader = row.createCell(cellSequenc);
		billAmountHeader.setCellValue("Bill Amount");
		billAmountHeader.setCellStyle(cellStyle);

		/*
		 * cellSequenc = cellSequenc + 1; Cell paymentModeHeader =
		 * row.createCell(cellSequenc); paymentModeHeader.setCellValue(
		 * "Payment Mode"); paymentModeHeader.setCellStyle(cellStyle);
		 */

		/*
		 * cellSequenc = cellSequenc + 1; Cell maidinMoneyHeader =
		 * row.createCell(cellSequenc); maidinMoneyHeader.setCellValue(
		 * "Maidin Money"); maidinMoneyHeader.setCellStyle(cellStyle);
		 * 
		 * cellSequenc = cellSequenc + 1; Cell paidByMaidinMoneyHeader =
		 * row.createCell(cellSequenc); paidByMaidinMoneyHeader.setCellValue(
		 * "Paid by Maidin Money");
		 * paidByMaidinMoneyHeader.setCellStyle(cellStyle);
		 * 
		 * cellSequenc = cellSequenc + 1; Cell cashCollectHeader =
		 * row.createCell(cellSequenc); cashCollectHeader.setCellValue(
		 * "Cash need to Collect "); cashCollectHeader.setCellStyle(cellStyle);
		 */

		cellSequenc = cellSequenc + 1;
		Cell orderedTimeHeader = row.createCell(cellSequenc);
		orderedTimeHeader.setCellValue("Ordered Time");
		orderedTimeHeader.setCellStyle(cellStyle);

		cellSequenc = cellSequenc + 1;
		Cell orderStatusHeader = row.createCell(cellSequenc);
		orderStatusHeader.setCellValue("Order Status");
		orderStatusHeader.setCellStyle(cellStyle);

		sheet.createFreezePane(0, 2, 0, 2);
	}

	private CustomerOrderReport generateOrderReportWithOutAccountInfo(OrderRequest request, Locale locale) {

		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			customerList = orderInfoDAO.getDifferentCustomersByTimestamp(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlotId(
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		for (Long customerId : customerList) {

			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				continue;
			}
			List<OrderInfoDTO> orderInfoDTOList = null;
			List<Integer> timeSlotList = null;
			List<Date> dateList = orderInfoDAO.getByCustomerDifferentDeliveryDates(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (Date deliveryDate : dateList) {
				if (request.getTimeSlot() == 0) {
					timeSlotList = orderInfoDAO.getTimeSlotByCustomerDeliveryDate(customerId, deliveryDate);
				} else if (request.getTimeSlot() > 0) {
					timeSlotList = orderInfoDAO.getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(customerId,
							deliveryDate, timeSlotDTO.getId());
				}
				for (Integer timeSlotId : timeSlotList) {
					TimeSlotInfo timeSlot = new TimeSlotInfo();
					timeSlot.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotId(customerId, deliveryDate,
							timeSlotId);
					for (OrderInfoDTO orderInfo : orderInfoDTOList) {

						List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
								.getSettledOrderDetailsByOrderId(orderInfo.getId());
						orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot
								+ orderDetailsInfoList.size();
						for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
							OrderFormat orderRow = new OrderFormat();
							if (customerDTO != null) {
								orderRow.setName(customerDTO.getFullName());
								orderRow.setProject(addressDTO.getProject());
								orderRow.setTower(addressDTO.getTower());
								orderRow.setFlat(addressDTO.getFlat());
								orderRow.setMobile(customerDTO.getMobileNumber());
								orderRow.setEmail(customerDTO.getEmail());
								orderRow.setDeliverySlot(orderInfo.getTimeSlot());
								ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
								if (productDTO == null)
									continue;
								orderRow.setProduct(productDTO.getProductName());
								orderRow.setUnit(productDTO.getQuantityUnit());
								ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
								orderRow.setBrand(brandDTO.getBrand());
								ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
								if (categoryDTO == null)
									continue;
								orderRow.setCategory(categoryDTO.getCategory());
								orderRow.setQuantity(orderDetailsInfo.getQuantity());
								float billAmount = 0;
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									billAmount = (orderDetailsInfo.getProductMRP() > 0
											? orderDetailsInfo.getProductMRP() : productDTO.getPrice())
											* orderDetailsInfo.getQuantity();
								}
								orderRow.setBillAmount(billAmount);
								orderRow.setOrderedTime(
										ApiUtils.dateTimeWithMonthNameFormat(orderInfo.getOrderedTime()));
								orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
								orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
								timeSlot.getOrderFormatList().add(orderRow);

							}
						}
					}
					customerOrder.getTimeSlotId().add(timeSlot);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;

	}

	private CustomerOrderReport generateOrderReportV2(OrderRequest request, Locale locale) throws JSONException {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Long> customerList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			customerList = orderInfoDAO.getDifferentCustomersByTimestamp(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			customerList = orderInfoDAO.getDifferentCustomersByTimestampAndTimeSlotId(
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		for (Long customerId : customerList) {
			WalletDTO walletDTO = walletDAO.getByCustomerId(customerId);
			float maidinMoney = 0;
			if (walletDTO != null) {
				maidinMoney = walletDTO.getMyWalletBalance();
			}
			CustomerOrderInfo customerOrder = new CustomerOrderInfo();
			customerOrder.setCustomerId(customerId);
			CustomerDTO customerDTO = customerDAO.getById(customerId);
			if (customerDTO == null) {
				continue;
			}
			AddressDTO addressDTO = addressDAO.getById(customerDTO.getAddressId());
			if (addressDTO == null) {
				continue;
			}
			List<OrderInfoDTO> orderInfoDTOList = null;
			List<Integer> timeSlotList = null;
			List<Date> dateList = orderInfoDAO.getByCustomerDifferentDeliveryDates(customerId,
					new Date(request.getFromTimestamp()), new Date(request.getToTimestamp()));
			for (Date deliveryDate : dateList) {
				if (request.getTimeSlot() == 0) {
					timeSlotList = orderInfoDAO.getTimeSlotByCustomerDeliveryDate(customerId, deliveryDate);
				} else if (request.getTimeSlot() > 0) {
					timeSlotList = orderInfoDAO.getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(customerId,
							deliveryDate, timeSlotDTO.getId());
				}
				for (Integer timeSlotId : timeSlotList) {
					float totalCashOfASlot = 0;
					float totalMMOfSlot = 0;
					TimeSlotInfo timeSlot = new TimeSlotInfo();
					timeSlot.setTimeSlotId(timeSlotId);
					int orderCountPerCustomerPerTimeSlot = 0;
					orderInfoDTOList = orderInfoDAO.getByCustomerIdDeliveryDateAndTimeSlotId(customerId, deliveryDate,
							timeSlotId);
					for (OrderInfoDTO orderInfo : orderInfoDTOList) {

						List<OrderDetailsInfoDTO> orderDetailsInfoList = orderDetailsInfoDAO
								.getSettledOrderDetailsByOrderId(orderInfo.getId());
						orderCountPerCustomerPerTimeSlot = orderCountPerCustomerPerTimeSlot
								+ orderDetailsInfoList.size();
						for (OrderDetailsInfoDTO orderDetailsInfo : orderDetailsInfoList) {
							OrderFormat orderRow = new OrderFormat();
							float cashPerOrder = 0;
							float mmPerOrder = 0;
							if (customerDTO != null) {
								orderRow.setName(customerDTO.getFullName());
								orderRow.setProject(addressDTO.getProject());
								orderRow.setTower(addressDTO.getTower());
								orderRow.setFlat(addressDTO.getFlat());
								orderRow.setMobile(customerDTO.getMobileNumber());
								orderRow.setEmail(customerDTO.getEmail());
								orderRow.setDeliverySlot(orderInfo.getTimeSlot());
								ProductDTO productDTO = productDAO.getById(orderDetailsInfo.getProductId());
								if (productDTO == null)
									continue;
								orderRow.setProduct(productDTO.getProductName());
								orderRow.setUnit(productDTO.getQuantityUnit());
								ProductBrandsDTO brandDTO = brandDAO.getById(productDTO.getBrandId());
								orderRow.setBrand(brandDTO.getBrand());
								ProductCategoryDTO categoryDTO = categoryDAO.getById(brandDTO.getCategoryId());
								if (categoryDTO == null)
									continue;
								orderRow.setCategory(categoryDTO.getCategory());
								orderRow.setQuantity(orderDetailsInfo.getQuantity());
								float billAmount = 0;
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									billAmount = orderDetailsInfo.getProductMRP() * orderDetailsInfo.getQuantity();
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									billAmount = (orderDetailsInfo.getProductMRP() > 0
											? orderDetailsInfo.getProductMRP() : productDTO.getPrice())
											* orderDetailsInfo.getQuantity();
								}
								orderRow.setBillAmount(billAmount);
								if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.CONFIRMED.getName())) {
									if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PAYMENTSETTLED.getName())) {
										orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
										orderRow.setCashCollect(0);
										orderRow.setPaidByMaidinMoney(billAmount);
									} else if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PARTIALPAYMENT.getName())) {
										orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
										float amountToPay = orderDetailsInfo.getTotalProductCost()
												- orderDetailsInfo.getAmountPaidByCustomer();
										if (maidinMoney >= (amountToPay)) {
											orderRow.setPaidByMaidinMoney(
													orderDetailsInfo.getAmountPaidByCustomer() + amountToPay);
											maidinMoney = maidinMoney - amountToPay;
										} else {
											orderRow.setPaidByMaidinMoney(
													orderDetailsInfo.getAmountPaidByCustomer() + maidinMoney);
											maidinMoney = 0;
										}
										orderRow.setCashCollect(orderDetailsInfo.getTotalProductCost()
												- orderDetailsInfo.getAmountPaidByCustomer());
										orderRow.setPaidByMaidinMoney(orderDetailsInfo.getAmountPaidByCustomer());
									} else if (orderDetailsInfo.getPaymentStatus()
											.equals(PaymentStatus.PENDING.getName())) {
										if (maidinMoney >= billAmount) {
											maidinMoney = maidinMoney - billAmount;
											orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
											orderRow.setCashCollect(0);
											orderRow.setPaidByMaidinMoney(billAmount);
										} else if (maidinMoney > 0 && billAmount > maidinMoney) {
											orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
											orderRow.setCashCollect(billAmount - maidinMoney);
											orderRow.setPaidByMaidinMoney(maidinMoney);
											maidinMoney = 0;
										} else {
											orderRow.setPaymentMode(PaymentMode.ByCash.getName());
											if (maidinMoney < 0) {
												orderRow.setCashCollect(billAmount - maidinMoney);
												maidinMoney = 0;
											} else if (maidinMoney == 0) {
												orderRow.setCashCollect(billAmount);
											}
											orderRow.setPaidByMaidinMoney(0);
										}
									}
								} else if (orderDetailsInfo.getOrderStatus()
										.equals(OrderStatus.UNDELIVERED.getName())) {
									orderRow.setPaymentMode("NA");
									orderRow.setCashCollect(0);
									orderRow.setPaidByMaidinMoney(0);
								} else if (orderDetailsInfo.getOrderStatus().equals(OrderStatus.DELIVERED.getName())) {
									if (orderDetailsInfo.getPaymentModeId() != 0) {
										// handling paymentMode from
										// OrderDetailsInfo
										if (orderDetailsInfo.getPaymentModeId() == PaymentMode.ByCash.getId()
												|| orderDetailsInfo
														.getPaymentModeId() == PaymentMode.PARTIAL_CASH_PARTIAL_PENDING
																.getId()) {
											orderRow.setCashCollect(orderDetailsInfo.getAmountPaidByCustomer());
											orderRow.setPaymentMode(paymentMode(paymentModeJSONArray,
													orderDetailsInfo.getPaymentModeId()));
										} else if (orderDetailsInfo.getPaymentModeId() == PaymentMode.ByWallet.getId()
												|| orderDetailsInfo
														.getPaymentModeId() == PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING
																.getId()) {
											orderRow.setPaidByMaidinMoney(orderDetailsInfo.getAmountPaidByCustomer());
											orderRow.setPaymentMode(paymentMode(paymentModeJSONArray,
													orderDetailsInfo.getPaymentModeId()));
										} else if (orderDetailsInfo.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
											orderRow.setCashCollect(orderDetailsInfo.getAmountPaidByCustomer());
											orderRow.setPaymentMode(PaymentMode.ByPaytm.getName());
										} else if (orderDetailsInfo.getPaymentModeId() == PaymentMode.ByCashByWallet
												.getId()
												|| orderDetailsInfo.getPaymentModeId() == PaymentMode.PARTIAL_PAYMENT
														.getId()) {
											orderRow.setPaymentMode(paymentMode(paymentModeJSONArray,
													orderDetailsInfo.getPaymentModeId()));
											List<CustomerTransactionDTO> transactionDTOList = transactionDAO
													.getByOrderDetailsId(orderDetailsInfo.getId());
											for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
												if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash.getId()) {
													cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
															- transactionDTO.getMoneyIn());
												} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
														.getId()) {
													mmPerOrder = mmPerOrder + (transactionDTO.getMoneyOut()
															- transactionDTO.getMoneyIn());
												} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
														.getId()) {
													cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
															- transactionDTO.getMoneyIn());
												}
											}
											orderRow.setCashCollect(cashPerOrder);
											orderRow.setPaidByMaidinMoney(mmPerOrder);

										} else {
											orderRow.setPaymentMode(PaymentMode.PENDING.getName());
										}

									} else {
										// handling paymentMode from OrderInfo
										List<CustomerTransactionDTO> transactionDTOList = transactionDAO
												.getByOrderDetailsId(orderDetailsInfo.getId());
										if (transactionDTOList == null || transactionDTOList.isEmpty()) {
											if (orderInfo.getPaymentModeId() == PaymentMode.ByCash.getId()) {
												orderRow.setCashCollect(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByCash.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByWallet.getId()) {
												orderRow.setPaidByMaidinMoney(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByPaytm.getId()) {
												orderRow.setCashCollect(orderRow.getBillAmount());
												orderRow.setPaymentMode(PaymentMode.ByPaytm.getName());
											} else if (orderInfo.getPaymentModeId() == PaymentMode.ByCashByWallet
													.getId()) {
												orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
											} else {
												PaymentModeDTO modeDTO = paymentModeDAO
														.getById(orderInfo.getPaymentModeId());
												if (modeDTO == null) {
													orderRow.setPaymentMode("Payment Not linked To Order");
												}
											}
										}

										else {
											boolean transaction = sameTransaction(transactionDTOList);
											if (!transaction) {
												orderRow.setPaymentMode(PaymentMode.ByCashByWallet.getName());
												for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
													if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash
															.getId()) {
														cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
															.getId()) {
														mmPerOrder = mmPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
															.getId()) {
														cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
													}
												}
												orderRow.setCashCollect(cashPerOrder);
												orderRow.setPaidByMaidinMoney(mmPerOrder);
											} else {
												for (CustomerTransactionDTO transactionDTO : transactionDTOList) {
													if (transactionDTO.getPaymentModeId() == PaymentMode.ByCash
															.getId()) {
														cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByCash.getName());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByWallet
															.getId()) {
														mmPerOrder = mmPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByWallet.getName());
													} else if (transactionDTO.getPaymentModeId() == PaymentMode.ByPaytm
															.getId()) {
														cashPerOrder = cashPerOrder + (transactionDTO.getMoneyOut()
																- transactionDTO.getMoneyIn());
														orderRow.setPaymentMode(PaymentMode.ByPaytm.getName());
													}
												}
												orderRow.setCashCollect(cashPerOrder);
												orderRow.setPaidByMaidinMoney(mmPerOrder);
											}
										}
									}
								}
								if (walletDTO != null) {
									orderRow.setMaidinMoney(walletDTO.getMyWalletBalance());
								}
								orderRow.setOrderedTime(
										ApiUtils.dateTimeWithMonthNameFormat(orderInfo.getOrderedTime()));
								orderRow.setDeliveryDate(orderInfo.getDeliveryDate());
								orderRow.setOrderStatus(orderDetailsInfo.getOrderStatus());
								timeSlot.getOrderFormatList().add(orderRow);
								totalCashOfASlot = totalCashOfASlot + orderRow.getCashCollect();
								totalMMOfSlot = totalMMOfSlot + orderRow.getPaidByMaidinMoney();
							}
						}
					}
					timeSlot.setCashCollect(totalCashOfASlot);
					timeSlot.setPaidyMaidinMoney(totalMMOfSlot);
					customerOrder.getTimeSlotId().add(timeSlot);
				}
			}
			response.getCustomerOrderInfo().add(customerOrder);
		}
		return response;

	}

	private CustomerOrderReport generateMultipleDayOrderReportV2(OrderRequest request, Locale locale)
			throws InterruptedException, ExecutionException {
		CustomerOrderReport response = new CustomerOrderReport();
		List<Date> dateList = null;
		TimeSlotDTO timeSlotDTO = timeSlotDAO.getById(request.getTimeSlot());
		if (request.getTimeSlot() == 0) {
			dateList = orderInfoDAO.getCustomersSortedByDeliveryDate(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()));
		} else {
			dateList = orderInfoDAO.getCustomersInTimeSlotSortedByDeliveryDate(new Date(request.getFromTimestamp()),
					new Date(request.getToTimestamp()), timeSlotDTO.getId());
		}
		ExecutorService exec = Executors.newFixedThreadPool(5);
		try {
			List<Future<?>> futures = new ArrayList<Future<?>>(dateList.size());
			Map<Long, Float> customerWallet = new HashMap<Long, Float>();
			for (Date date : dateList) {
				logger.info(ApiUtils.dateWithMonthNameFormat(new Timestamp(date.getTime())));
				final Runnable callableTask = new Runnable() {
					public void run() {
						System.out.println(Thread.currentThread().getName());
						getCustomerRecordsByDateV2(date, request, response, customerWallet);
					}
				};
				futures.add(exec.submit(callableTask));
			}
			for (Future<?> f : futures) {
				f.get(); // wait for a processor to complete
			}
			logger.info("all items processed");
		} catch (Exception e) {
			exec.shutdown();
		} finally {
			exec.shutdown();
		}
		return response;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse markDeliveryApiV2(String authToken, MarkOrder request, Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		GenericResponse response = new GenericResponse();
		List<Integer> paymentModeIdList = new ArrayList<Integer>();

		for (Long orderDetailsInfoId : request.getUnDeliveredOrderDetailsInfoIdList()) {
			markOrderUndeliveredV2(orderDetailsInfoId, adminDTO.getId(), request.getCustomerId());
		}

		// mark OrderDelivered for OrderInfo
		float restAmount = request.getAmount();
		for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();
				int undeliveryStatusCount = 0;
				for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
					// By Pushpinder 5/30/18
					if (request.getUnDeliveredOrderDetailsInfoIdList().contains(orderDetailsInfoDTO.getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTO.getId());
				}

				// setting overall orderStatus
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				// remove all undelivered orderDetailsId from orderInfo list
				if (orderDetailsInfoIdList.size() > 0) {
					orderDetailsInfoIdList.removeAll(request.getUnDeliveredOrderDetailsInfoIdList());
				}
				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(restAmount);
				for (Long orderDetailsInfoId : orderDetailsInfoIdList) {
					// mark individual OrderDetails as delivered
					pModeAmount = markOrderDeliveredV2(orderDetailsInfoId, adminDTO.getId(), pModeAmount.getAmount(),
							locale);
					paymentModeIdList.add(pModeAmount.getPaymentModeId());
				}

				// set overallPaymentModeId
				/*
				 * if
				 * (paymentModeIdList.contains(PaymentMode.ByCashByWallet.getId(
				 * ))) {
				 * orderInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.
				 * getId()); } else { orderInfoDTO.setPaymentModeId(
				 * paymentModeIdList.size() > 0 ? paymentModeIdList.get(0) :
				 * orderInfoDTO.getPaymentModeId()); }
				 */
				// setPaymentModeId(orderInfoId);
				restAmount = pModeAmount.getAmount();
			}
		}

		// END - mark Order delivered by OrderDetailsId

		if (restAmount > 0) {
			// recharge rest amount
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(restAmount);
			transactionRequest.setOrderId(-1);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), restAmount, WalletDescription.RECHARGE.getName());
			// reshuffle balance
			// reshufflePayment(request.getCustomerId(), restAmount);

		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private PaymentModeAmount markOrderDeliveredV2(Long orderDetailsInfoId, Long adminId, float amount, Locale locale)
			throws ValidationException {
		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (detailsInfoDTO != null) {
			OrderInfoDTO orderInfo = orderInfoDAO.getById(detailsInfoDTO.getOrderId());

			// set price(ProductMRP , totalProductCost) if not updated earlier
			if (detailsInfoDTO.getProductMRP() == 0) {
				ProductDTO productDTO = productDAO.getById(detailsInfoDTO.getProductId());
				detailsInfoDTO.setProductMRP(
						detailsInfoDTO.getProductMRP() > 0 ? detailsInfoDTO.getProductMRP() : productDTO.getPrice());
			}
			if (detailsInfoDTO.getTotalProductCost() == 0) {
				detailsInfoDTO.setTotalProductCost(detailsInfoDTO.getProductMRP() * detailsInfoDTO.getQuantity());
			}

			if (detailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PARTIALPAYMENT.getName())
					|| detailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PENDING.getName())) {

				// check maidin balance
				float customerMaidinBalance = checkMaidinBalance(orderInfo.getCustomerId());

				// create common customerTransaction object
				CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
				transactionRequest.setCustomerId(orderInfo.getCustomerId());
				transactionRequest.setOrderId(detailsInfoDTO.getOrderId());
				transactionRequest.setOrderDetailsId(orderDetailsInfoId);
				transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());

				// find out - how much pending amount to pay at time of delivery
				float pendingAmountToPay = detailsInfoDTO.getTotalProductCost()
						- detailsInfoDTO.getAmountPaidByCustomer();
				if ((detailsInfoDTO.getTotalProductCost() > detailsInfoDTO.getAmountPaidByCustomer())) {
					if (customerMaidinBalance >= pendingAmountToPay) {
						// case 1: Customer has sufficient maidin balance
						detailsInfoDTO
								.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
						detailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
						detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						transactionRequest.setMoneyOut(pendingAmountToPay);
						transactionRequest.setDueAmount(0);
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfo.getCustomerId(), pendingAmountToPay,
								WalletDescription.DEDUCTIONS.getName());
					} else if (customerMaidinBalance > 0 && customerMaidinBalance < pendingAmountToPay) {
						// case 2: Customer hasn't sufficient maidin balance

						transactionRequest.setMoneyOut(customerMaidinBalance);
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmountToPay - customerMaidinBalance);
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfo.getCustomerId(), customerMaidinBalance,
								WalletDescription.DEDUCTIONS.getName());
						detailsInfoDTO.setAmountPaidByCustomer(
								detailsInfoDTO.getAmountPaidByCustomer() + customerMaidinBalance);
						pendingAmountToPay = pendingAmountToPay - customerMaidinBalance;
						if (amount >= pendingAmountToPay) {
							transactionRequest.setMoneyOut(pendingAmountToPay);
							transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
							transactionRequest.setDueAmount(0);
							addCustomerTransaction(transactionRequest, adminId);
							detailsInfoDTO.setAmountPaidByCustomer(
									detailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
							detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
						} else {
							if (amount > 0) {
								transactionRequest.setMoneyOut(amount);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								transactionRequest.setDueAmount(pendingAmountToPay - amount);
								addCustomerTransaction(transactionRequest, adminId);
								// due amount deducted from wallet
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);
								updateWalletBalance(orderInfo.getCustomerId(), pendingAmountToPay - amount,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO
										.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + amount);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
							} else if (amount <= 0) {
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setDueAmount(pendingAmountToPay);
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);
								// deduct his due amount from wallet
								updateWalletBalance(orderInfo.getCustomerId(), pendingAmountToPay,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
							}
						}
						amount = amount - pendingAmountToPay;
					} else {
						// no maidin balance
						if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
						} else {
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCash.getId());
						}
						if (amount > 0) {
							if (amount >= pendingAmountToPay) {
								// case 3. amount >= pendingAmount of order
								transactionRequest.setMoneyOut(pendingAmountToPay);
								transactionRequest.setDueAmount(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								addCustomerTransaction(transactionRequest, adminId);
								detailsInfoDTO.setAmountPaidByCustomer(
										detailsInfoDTO.getAmountPaidByCustomer() + pendingAmountToPay);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
							} else {
								// case 4. amount < pendingAmount of order
								transactionRequest.setMoneyOut(amount);
								transactionRequest.setDueAmount(pendingAmountToPay - amount);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								addCustomerTransaction(transactionRequest, adminId);
								// due amount deducted from wallet
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);
								updateWalletBalance(orderInfo.getCustomerId(), pendingAmountToPay - amount,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO
										.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + amount);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId());
							}
						} else if (amount <= 0) {
							// due amount deducted from wallet
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setDueAmount(pendingAmountToPay);
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfo.getCustomerId(), pendingAmountToPay,
									WalletDescription.DEDUCTIONS.getName());
							if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
							} else {
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
							}
						}
						amount = amount - pendingAmountToPay;
					}
				}
			} else {
				detailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
			}
			// mark Order Delivered
			detailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());
			detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

			// set the rest amount in paymentModeAmountTuple
			paymentModeAmountTuple.setAmount(amount);
			if (detailsInfoDTO.getPaymentModeId() != null && detailsInfoDTO.getPaymentModeId() > 0) {
				paymentModeAmountTuple.setPaymentModeId(detailsInfoDTO.getPaymentModeId());
			} else if (detailsInfoDTO.getPaymentModeId() != null) {
				paymentModeAmountTuple.setPaymentModeId(orderInfo.getPaymentModeId());
			} else if (detailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
				paymentModeAmountTuple.setPaymentModeId(PaymentMode.ByWallet.getId());
			} else {
				paymentModeAmountTuple.setPaymentModeId(0);
			}

		}
		return paymentModeAmountTuple;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse markOrderDeliveredWithQtyUpdateV2(String authToken, MarkOrderWithQtyUpdateRequest request,
			Locale locale) throws Exception {
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		GenericResponse response = new GenericResponse();
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		int paymentModeId = 0;

		// mark undelivered orders
		for (Long orderDetailsInfoId : request.getUnDeliveredOrderDetailsInfoIdList()) {
			markOrderUndeliveredV2(orderDetailsInfoId, adminDTO.getId(), request.getCustomerId());
		}

		// mark delivered orders
		float restamountToRecharge = request.getAmount();
		for (Long orderInfoId : request.getDeliveredOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();

				/* check order status - START */
				int undeliveryStatusCount = 0;

				// prepare all delivered orderDetailInfo Ids list
				for (OrderDetailsInfoDTO orderDetailsInfoDTO : orderDetailsInfoDTOList) {
					if (request.getUnDeliveredOrderDetailsInfoIdList().contains(orderDetailsInfoDTO.getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTO.getId());
				}
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				/* check order status - END */

				// set updated time
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				// remove un-delivered orderDetailInfo ids
				if (orderDetailsInfoIdList.size() > 0) {
					orderDetailsInfoIdList.removeAll(request.getUnDeliveredOrderDetailsInfoIdList());
				}

				// update payment mode and amount paid
				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(restamountToRecharge);
				for (int i = 0; i < orderDetailsInfoIdList.size(); i++) {
					if (request.getUpdatedOrderDetailInfoIdList().contains(orderDetailsInfoIdList.get(i))) {
						int index = request.getUpdatedOrderDetailInfoIdList().indexOf(orderDetailsInfoIdList.get(i));

						pModeAmount = markDeliveredWithQtyUpdateV2(orderDetailsInfoIdList.get(i), adminDTO.getId(),
								pModeAmount.getAmount(), request.getUpdatedOrderDetailQuantityList().get(index),
								locale);
					} else {
						pModeAmount = markOrderDeliveredV2(orderDetailsInfoIdList.get(i), adminDTO.getId(),
								pModeAmount.getAmount(), locale);
					}
					paymentModeIdList.add(pModeAmount.getPaymentModeId());
				}

				// setPaymentModeId(orderInfoId);
				restamountToRecharge = pModeAmount.getAmount();
			}
		}
		if (restamountToRecharge > 0) {
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(restamountToRecharge);
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), restamountToRecharge, WalletDescription.RECHARGE.getName());

			// reshufflePayment(request.getCustomerId(), restamountToRecharge);
		}

		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		return response;
	}

	private PaymentModeAmount markDeliveredWithQtyUpdateV2(Long orderDetailsInfoId, Long adminId, float amount,
			int quantity, Locale locale) throws ValidationException {

		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();

		// get orderDetailsInfoDTO
		OrderDetailsInfoDTO orderDetailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);

		if (orderDetailsInfoDTO != null) {
			// get orderInfoDTO
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderDetailsInfoDTO.getOrderId());

			// check weather product MRP in orderDetailsInfo table is 0
			if (orderDetailsInfoDTO.getProductMRP() == 0) {
				ProductDTO productDTO = productDAO.getById(orderDetailsInfoDTO.getProductId());
				orderDetailsInfoDTO.setProductMRP(orderDetailsInfoDTO.getProductMRP() > 0
						? orderDetailsInfoDTO.getProductMRP() : productDTO.getPrice());
			}

			// set updated total cost as per change in quantity
			orderDetailsInfoDTO.setTotalProductCost(orderDetailsInfoDTO.getProductMRP() * quantity);

			// check wallet balance
			float currentMaidinBalance = checkMaidinBalance(orderInfoDTO.getCustomerId());

			// make CustomerTransactionRequest object
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(orderInfoDTO.getCustomerId());
			transactionRequest.setOrderId(orderInfoDTO.getId());
			transactionRequest.setOrderDetailsId(orderDetailsInfoId);
			transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());

			float pendingAmount = orderDetailsInfoDTO.getTotalProductCost()
					- orderDetailsInfoDTO.getAmountPaidByCustomer();

			if ((pendingAmount > 0)) {

				if (currentMaidinBalance >= pendingAmount) {
					// CASE 1 : Wallet has sufficient amount
					transactionRequest.setDueAmount(0);
					transactionRequest.setMoneyOut(pendingAmount);
					orderDetailsInfoDTO
							.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
					orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());

					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
							WalletDescription.DEDUCTIONS.getName());
					orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());

				} else if (currentMaidinBalance > 0 && currentMaidinBalance < pendingAmount) {
					// case 2: Customer hasn't sufficient maidin balance

					// deduct Balance first from wallet
					transactionRequest.setMoneyOut(currentMaidinBalance);
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setDueAmount(pendingAmount - currentMaidinBalance);
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), currentMaidinBalance,
							WalletDescription.DEDUCTIONS.getName());
					orderDetailsInfoDTO.setAmountPaidByCustomer(
							orderDetailsInfoDTO.getAmountPaidByCustomer() + currentMaidinBalance);
					pendingAmount = pendingAmount - currentMaidinBalance;
					if (amount >= pendingAmount) {
						// deduct amount from amount passed if amount >
						// pendingAmount
						transactionRequest.setMoneyOut(pendingAmount);
						transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
						transactionRequest.setDueAmount(0);
						addCustomerTransaction(transactionRequest, adminId);
						orderDetailsInfoDTO
								.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
						orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
					} else {
						if (amount > 0) {

							// deduct amount from amount passed if amount > 0
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
							transactionRequest.setDueAmount(pendingAmount - amount);
							addCustomerTransaction(transactionRequest, adminId);

							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
						} else if (amount <= 0) {
							// deduct his due amount from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setDueAmount(pendingAmount);
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
						}
					}
					amount = amount - pendingAmount;
				} else {
					if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
					} else {
						orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByCash.getId());
					}
					if (amount > 0) {
						if (amount >= pendingAmount) {
							// case 3. amount >= pendingAmount of order
							transactionRequest.setMoneyOut(pendingAmount);
							transactionRequest.setDueAmount(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
							addCustomerTransaction(transactionRequest, adminId);
							orderDetailsInfoDTO.setAmountPaidByCustomer(
									orderDetailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						} else {
							// case 4. amount < pendingAmount of order
							transactionRequest.setMoneyOut(amount);
							transactionRequest.setDueAmount(pendingAmount - amount);
							transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
							addCustomerTransaction(transactionRequest, adminId);
							// due amount deducted from wallet
							transactionRequest.setMoneyOut(0);
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
									WalletDescription.DEDUCTIONS.getName());
							orderDetailsInfoDTO
									.setAmountPaidByCustomer(orderDetailsInfoDTO.getAmountPaidByCustomer() + amount);
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
								orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
							} else {
								orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId());
							}
						}
					} else if (amount <= 0) {
						// due amount deducted from wallet
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmount);
						transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
								WalletDescription.DEDUCTIONS.getName());
						if (orderDetailsInfoDTO.getAmountPaidByCustomer() > 0) {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
						} else {
							orderDetailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
							orderDetailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
						}
					}
					amount = amount - pendingAmount;
				}
			} else {
				// pendingAmount negative: re-charge wallet with refunded amount
				pendingAmount = pendingAmount * -1;
				transactionRequest.setMoneyIn(pendingAmount);
				orderDetailsInfoDTO.setAmountPaidByCustomer(orderDetailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
				addCustomerTransaction(transactionRequest, adminId);
				updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount, WalletDescription.RECHARGE.getName());
				orderDetailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
				orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
			}
			paymentModeAmountTuple.setAmount(amount);
			if (orderDetailsInfoDTO.getPaymentModeId() != null && orderDetailsInfoDTO.getPaymentModeId() > 0) {
				paymentModeAmountTuple.setPaymentModeId(orderDetailsInfoDTO.getPaymentModeId());
			} else if (orderInfoDTO.getPaymentModeId() != null) {
				paymentModeAmountTuple.setPaymentModeId(orderInfoDTO.getPaymentModeId());
			} else if (orderDetailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
				paymentModeAmountTuple.setPaymentModeId(PaymentMode.ByWallet.getId());
			} else {
				paymentModeAmountTuple.setPaymentModeId(0);
			}

			orderDetailsInfoDTO.setQuantity(quantity);
			orderDetailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());
			orderDetailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
		}
		return paymentModeAmountTuple;

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public GenericResponse changeDeliveredOrderStateV2(String authToken, ChangeDeliveredOrderStateRequest request,
			Locale locale) throws Exception {
		// logger.info(ApiUtils.REQUEST_PAYLOAD,
		// ApiUtils.toJsonString(request));
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO = adminDAO.getByMobileNumber(mobile);
		if (adminDTO != null) {
			logger.info(ApiUtils.REQUEST_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		}
		// logger.info("AdminId: " + adminDTO.getId(), "authToken :" +
		// authToken);
		List<Integer> paymentModeIdList = new ArrayList<Integer>();
		GenericResponse response = new GenericResponse();

		List<Long> undeliverdOrderList = new ArrayList<>();
		List<Long> deliverdOrderList = new ArrayList<>();
		for (int i = 0; i < request.getUpdatedOrderDetailInfoIdList().size(); i++) {
			if (request.getUpdatedOrderDetailStatusList().get(i).equals(OrderStatus.UNDELIVERED.getId())) {
				undeliverdOrderList.add(request.getUpdatedOrderDetailInfoIdList().get(i));
			} else {
				deliverdOrderList.add(request.getUpdatedOrderDetailInfoIdList().get(i));
			}
		}

		float restAmount = request.getAmount();
		for (Long orderInfoId : request.getOrderInfoIdList()) {
			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(orderInfoId);
			if (orderInfoDTO != null) {
				List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
						.getSettledOrderDetailsByOrderId(orderInfoId);
				List<Long> orderDetailsInfoIdList = new ArrayList<Long>();
				int undeliveryStatusCount = 0;
				for (int i = 0; i < orderDetailsInfoDTOList.size(); i++) {
					if (undeliverdOrderList.contains(orderDetailsInfoDTOList.get(i).getId())) {
						undeliveryStatusCount++;
					}
					orderDetailsInfoIdList.add(orderDetailsInfoDTOList.get(i).getId());
				}

				// setting overall orderStatus
				if (undeliveryStatusCount == 0) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.DELIVERED.getId());
				} else if (undeliveryStatusCount >= orderDetailsInfoDTOList.size()) {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.UNDELIVERED.getId());
				} else {
					orderInfoDTO.setOverallOrderStatus(OrderStatus.PARTIAL_DELIVERED.getId());
				}
				orderInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));

				PaymentModeAmount pModeAmount = new PaymentModeAmount();
				pModeAmount.setAmount(restAmount);
				for (int i = 0; i < orderDetailsInfoIdList.size(); i++) {
					if (request.getUpdatedOrderDetailInfoIdList().contains(orderDetailsInfoIdList.get(i))) {
						int index = request.getUpdatedOrderDetailInfoIdList().indexOf(orderDetailsInfoIdList.get(i));
						if (index != -1) {
							pModeAmount = updateOrderDTOAndTransaction(orderDetailsInfoIdList.get(i), adminDTO.getId(),
									pModeAmount.getAmount(), request.getUpdatedOrderDetailStatusList().get(index));
							paymentModeIdList.add(pModeAmount.getPaymentModeId());
						}
					}
				}

				// setPaymentModeId(orderInfoId);
				restAmount = pModeAmount.getAmount();
			}
		}

		if (restAmount > 0) {
			// recharge rest amount
			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(request.getCustomerId());
			transactionRequest.setMoneyIn(restAmount);
			transactionRequest.setOrderDetailsId(-1);
			transactionRequest.setDueAmount(0);
			transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
			transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
			addCustomerTransaction(transactionRequest, adminDTO.getId());
			updateWalletBalance(request.getCustomerId(), restAmount, WalletDescription.RECHARGE.getName());
			// reshuffle balance
			// reshufflePayment(request.getCustomerId(), restAmount);

		}
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(request));
		response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);

		return response;
	}

	private PaymentModeAmount updateOrderDTOAndTransaction(Long orderDetailsInfoId, Long adminId, float amount,
			int orderStatus) throws ValidationException {
		PaymentModeAmount paymentModeAmountTuple = new PaymentModeAmount();
		OrderDetailsInfoDTO detailsInfoDTO = orderDetailsInfoDAO.getById(orderDetailsInfoId);
		if (detailsInfoDTO != null) {

			OrderInfoDTO orderInfoDTO = orderInfoDAO.getById(detailsInfoDTO.getOrderId());

			// check wallet balance
			float currentMaidinBalance = checkMaidinBalance(orderInfoDTO.getCustomerId());

			// for (int i = 0; i <
			// request.getUpdatedOrderDetailInfoIdList().size(); i++) {

			CustomerTransactionRequest transactionRequest = new CustomerTransactionRequest();
			transactionRequest.setCustomerId(orderInfoDTO.getCustomerId());
			transactionRequest.setOrderDetailsId(detailsInfoDTO.getId());

			// Case 1: product un-delivered, return money if paid
			if (orderStatus == OrderStatus.UNDELIVERED.getId()) {

				transactionRequest.setMoneyIn(detailsInfoDTO.getTotalProductCost());
				transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
				transactionRequest.setDueAmount(0);
				transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
				addCustomerTransaction(transactionRequest, adminId);
				updateWalletBalance(orderInfoDTO.getCustomerId(), detailsInfoDTO.getTotalProductCost(),
						WalletDescription.RECHARGE.getName());

				detailsInfoDTO.setOrderStatus(OrderStatus.UNDELIVERED.getName());
				detailsInfoDTO.setPaymentModeId(PaymentMode.NA.getId());
				detailsInfoDTO.setPaymentStatus(PaymentStatus.NA.getName());
				detailsInfoDTO.setCancelled(true);
				detailsInfoDTO.setOrderCancellationTime(new Timestamp(System.currentTimeMillis()));
				detailsInfoDTO.setAmountPaidByCustomer(0);

				// case 2: product delivered
			} else if (orderStatus == OrderStatus.DELIVERED.getId()) {
				detailsInfoDTO.setOrderStatus(OrderStatus.DELIVERED.getName());

				float pendingAmount = detailsInfoDTO.getTotalProductCost() - detailsInfoDTO.getAmountPaidByCustomer();
				if (pendingAmount > 0) {
					if (currentMaidinBalance >= pendingAmount) {
						detailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
						detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
						detailsInfoDTO.setAmountPaidByCustomer(pendingAmount);

						transactionRequest.setMoneyOut(pendingAmount);
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(0);
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
								WalletDescription.DEDUCTIONS.getName());
						orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					} else if (currentMaidinBalance > 0 && currentMaidinBalance < pendingAmount) {
						// case 2: Customer hasn't sufficient maidin balance

						transactionRequest.setMoneyOut(currentMaidinBalance);
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmount - currentMaidinBalance);
						transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());

						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), currentMaidinBalance,
								WalletDescription.DEDUCTIONS.getName());
						detailsInfoDTO.setAmountPaidByCustomer(
								detailsInfoDTO.getAmountPaidByCustomer() + currentMaidinBalance);

						pendingAmount = pendingAmount - currentMaidinBalance;
						if (amount >= pendingAmount) {
							transactionRequest.setMoneyOut(pendingAmount);
							transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
							transactionRequest.setDueAmount(0);
							transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());

							addCustomerTransaction(transactionRequest, adminId);
							detailsInfoDTO
									.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
							detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
						} else {
							if (amount > 0) {
								transactionRequest.setMoneyOut(amount);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
								transactionRequest.setDueAmount(pendingAmount - amount);
								addCustomerTransaction(transactionRequest, adminId);

								// due amount deducted from wallet
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);

								updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO
										.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + amount);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
							} else if (amount <= 0) {
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setDueAmount(pendingAmount);
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);

								// deduct his due amount from wallet
								updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
							}
						}
						amount = amount - pendingAmount;
					} else if (currentMaidinBalance < 0) {
						detailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
						detailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
						detailsInfoDTO.setAmountPaidByCustomer(0);

						transactionRequest.setMoneyOut(0);
						transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
						transactionRequest.setDueAmount(pendingAmount);
						transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
						addCustomerTransaction(transactionRequest, adminId);
						updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
								WalletDescription.DEDUCTIONS.getName());
						orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					} else {
						// maidin balance zero
						if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCashByWallet.getId());
						} else {
							detailsInfoDTO.setPaymentModeId(PaymentMode.ByCash.getId());
						}
						if (amount > 0) {
							if (amount >= pendingAmount) {
								// case 3. amount >= pendingAmount of order
								transactionRequest.setMoneyOut(pendingAmount);
								transactionRequest.setDueAmount(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());

								addCustomerTransaction(transactionRequest, adminId);
								detailsInfoDTO.setAmountPaidByCustomer(
										detailsInfoDTO.getAmountPaidByCustomer() + pendingAmount);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PAYMENTSETTLED.getName());
							} else {
								// case 4. amount < pendingAmount of order
								transactionRequest.setMoneyOut(amount);
								transactionRequest.setDueAmount(pendingAmount - amount);
								transactionRequest.setPaymentModeId(PaymentMode.ByCash.getId());
								transactionRequest.setTransactionType(WalletDescription.DEDUCTIONS.getId());
								addCustomerTransaction(transactionRequest, adminId);

								// due amount deducted from wallet
								transactionRequest.setMoneyOut(0);
								transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
								transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
								addCustomerTransaction(transactionRequest, adminId);
								updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount - amount,
										WalletDescription.DEDUCTIONS.getName());
								detailsInfoDTO
										.setAmountPaidByCustomer(detailsInfoDTO.getAmountPaidByCustomer() + amount);
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
									detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_PAYMENT.getId());
								} else {
									detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_CASH_PARTIAL_PENDING.getId());
								}
							}
						} else if (amount <= 0) {
							// due amount deducted from wallet
							transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
							transactionRequest.setDueAmount(pendingAmount);
							transactionRequest.setTransactionType(WalletDescription.DEBT.getId());
							addCustomerTransaction(transactionRequest, adminId);
							updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
									WalletDescription.DEDUCTIONS.getName());
							if (detailsInfoDTO.getAmountPaidByCustomer() > 0) {
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PARTIALPAYMENT.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PARTIAL_WALLET_PARTIAL_PENDING.getId());
							} else {
								detailsInfoDTO.setPaymentStatus(PaymentStatus.PENDING.getName());
								detailsInfoDTO.setPaymentModeId(PaymentMode.PENDING.getId());
							}
						}
						amount = amount - pendingAmount;
					}
				} else {
					// pendingAmount negative: re-charge wallet with
					// refunded
					// amount
					pendingAmount = pendingAmount * -1;
					transactionRequest.setMoneyIn(pendingAmount);
					detailsInfoDTO.setAmountPaidByCustomer(detailsInfoDTO.getTotalProductCost());
					transactionRequest.setPaymentModeId(PaymentMode.ByWallet.getId());
					transactionRequest.setTransactionType(WalletDescription.RECHARGE.getId());
					addCustomerTransaction(transactionRequest, adminId);
					updateWalletBalance(orderInfoDTO.getCustomerId(), pendingAmount,
							WalletDescription.RECHARGE.getName());
					detailsInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
					orderInfoDTO.setPaymentModeId(PaymentMode.ByWallet.getId());
				}
				detailsInfoDTO.setCancelled(false);
				detailsInfoDTO.setOrderCancellationTime(null);

				// amount = amount - pendingAmount;
			} else {
				// not applicable, throw exception
			}
			paymentModeAmountTuple.setAmount(amount);
			if (detailsInfoDTO.getPaymentModeId() != null && detailsInfoDTO.getPaymentModeId() > 0) {
				paymentModeAmountTuple.setPaymentModeId(detailsInfoDTO.getPaymentModeId());
			} else if (orderInfoDTO.getPaymentModeId() != null) {
				paymentModeAmountTuple.setPaymentModeId(orderInfoDTO.getPaymentModeId());
			} else if (detailsInfoDTO.getPaymentStatus().equals(PaymentStatus.PAYMENTSETTLED.getName())) {
				paymentModeAmountTuple.setPaymentModeId(PaymentMode.ByWallet.getId());
			} else {
				paymentModeAmountTuple.setPaymentModeId(0);
			}
			detailsInfoDTO.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
			// }
		}
		return paymentModeAmountTuple;
	}

}

final class CodeGenerator {

	public static String generateCode() {
		Random random = new Random();
		int n = 1000 + random.nextInt(9000);
		return Integer.toString(n);
	}

	private CodeGenerator() {
	}
}
