package com.maidin.adminApi.admin.V2.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.adminApi.admin.V2.BatchMgmt;
import com.maidin.adminApi.admin.V2.DashboardMgmt;
import com.maidin.adminApi.admin.impl.AdminMgmtImpl;
import com.maidin.adminApi.enums.OrderStatus;
import com.maidin.adminApi.enums.PaymentMode;
import com.maidin.adminApi.enums.PaymentStatus;
import com.maidin.adminApi.enums.ScheduleStatus;
import com.maidin.adminApi.enums.WalletDescription;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.TimeSlotDTO;
import com.maidin.persistence.dto.WalletDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerTransactionRequest;
import com.maidin.pojo.admin.DeliveriesCountByDay;

public class DashboardMgmtImpl implements DashboardMgmt {	
	
	private final WalletDAO walletDAO;
	private final CustomerDAO customerDAO;
	private final AuthenticationMgmt authenticationMgmt;
	private final AdminDAO adminDAO;
	private final OrderInfoDAO orderInfoDAO;
	private final OrderDetailsInfoDAO orderDetailsInfoDAO;
	private final ScheduleInfoDAO scheduleInfoDAO;
	private final TimeSlotDAO timeSlotDAO;
	private final ProductInfoV2DAO productInfoV2DAO;
	private final AddressDAO addressDAO;
	private final CustomerTransactionDAO transactionDAO;
	private final ProductCityInfoV2DAO productCityInfoV2DAO;
	 @Autowired
		public DashboardMgmtImpl(WalletDAO walletDAO, CustomerDAO customerDAO, AuthenticationMgmt authenticationMgmt,
				AdminDAO adminDAO, OrderInfoDAO orderInfoDAO, OrderDetailsInfoDAO orderDetailsInfoDAO,
				ProductInfoV2DAO productInfoV2DAO, AddressDAO addressDAO, CustomerTransactionDAO transactionDAO,
				ProductCityInfoV2DAO productCityInfoV2DAO, ScheduleInfoDAO scheduleInfoDAO, TimeSlotDAO timeSlotDAO) {
			
			this.walletDAO = walletDAO;
			this.customerDAO = customerDAO;
			this.authenticationMgmt = authenticationMgmt;
			this.adminDAO = adminDAO;
			this.orderInfoDAO = orderInfoDAO;
			this.orderDetailsInfoDAO = orderDetailsInfoDAO;
			this.productInfoV2DAO = productInfoV2DAO;
			this.addressDAO = addressDAO;
			this.transactionDAO = transactionDAO;
			this.productCityInfoV2DAO = productCityInfoV2DAO;
			this.scheduleInfoDAO = scheduleInfoDAO;
			this.timeSlotDAO = timeSlotDAO;
		}

	private static final Logger logger = LoggerFactory.getLogger(DashboardMgmtImpl.class);

	private static Properties loadProperties(String fileName) {
		try {
			return CommonUtils.loadPropertyFile(new File(fileName));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Unable to load the properties file: " + fileName);
		}
	}
	private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
	
	@Override
	@Transactional(rollbackFor =Exception.class, readOnly = true)
	public CustomerCountInfoResponse getDashboardInfo(String authToken, Locale locale) throws Exception {
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();
		String mobile = authenticationMgmt.getAdminMobile(ApiUtils.createAuth(authToken));
		AdminDTO adminDTO  = adminDAO.getByMobileNumberWithoutActiveState(mobile);
		if(adminDTO != null){
		logger.info(ApiUtils.REQUEST_PAYLOAD, adminDTO.getId());
		}
		response.setFlatUserCount(customerDAO.getUsersByFlatCount(true));
		response.setRegisteredUserCount(customerDAO.getRegisteredUserCount(true));
		//response.setTotalMaidinMoneyInUsersWallet(walletDAO.getTotalMadinMoney());
		response.setTotalPositiveMaidinMoney(walletDAO.getPositiveMadinMoney());
		response.setTotalNegativeMaidinMoney(walletDAO.getNegativeMadinMoney());
		response.getDeliveriesCountByDay().addAll(getDeliveryOrders(locale));response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		logger.info(ApiUtils.RESPONSE_PAYLOAD_PARAMETER, adminDTO.getId(), ApiUtils.toJsonString(response));
		return response;
	}
	
	private List<DeliveriesCountByDay> getDeliveryOrders(Locale locale) {

		List<DeliveriesCountByDay> response = new ArrayList<DeliveriesCountByDay>();

		String[] dates = { Date.valueOf(LocalDate.now().minusDays(1L)).toString(),
				Date.valueOf(LocalDate.now()).toString(), Date.valueOf(LocalDate.now().plusDays(1L)).toString() };

		for (String date : dates) {
			long noOfDeliveries = orderInfoDAO.getTotalCountByDeliveryDate(
					Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			long noOfTotalDeliveriesExcludingUndeliveredOrders = orderInfoDAO
					.getTotalCountByDeliveryDateExcludingUndelivered(
							Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			long noOfMarkedDeliveries = orderInfoDAO.getTotalMarkedDeliveries(
					Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			int noOfCancelledDeliveries = 0;

			List<OrderDetailsInfoDTO> orderDetailsInfoDTOList = orderDetailsInfoDAO
					.getOrdersByYTT(Long.parseLong(prop.getProperty(ResourceProperties.TEST_ACCOUNT_ADDRESS_ID)), date);
			double subTotal = 0;
			for (OrderDetailsInfoDTO dto : orderDetailsInfoDTOList) {
				if (dto.getProductSalePrice() != 0) {
					subTotal += (dto.getProductSalePrice()+dto.getDeliveryCharges()) * dto.getQuantity();
				} else {
					ProductCityInfoV2DTO productCityDTO = productCityInfoV2DAO.getByIdWithoutActiveStatus(dto.getProductId());
					if(productCityDTO != null){
					subTotal += (productCityDTO.getSalePrice() +productCityDTO.getDeliveryCharge()) * dto.getQuantity();
					}
				}
			}

			long timeStamp = 0;
			try {
				timeStamp = new SimpleDateFormat("yyyy-MM-dd").parse(date).getTime();
			} catch (ParseException e) {
			}
			DeliveriesCountByDay deliveries_Y = new DeliveriesCountByDay();
			deliveries_Y.setNoOfTotalDeliveries(noOfDeliveries);
			deliveries_Y.setNoOfTotalUndeliveredOrders(noOfTotalDeliveriesExcludingUndeliveredOrders - noOfDeliveries);
			deliveries_Y.setNoOfTotalMarkedDeliveries(
					noOfMarkedDeliveries - (noOfTotalDeliveriesExcludingUndeliveredOrders - noOfDeliveries));
			deliveries_Y.setNoOfCancelledDeliveries(noOfCancelledDeliveries);
			deliveries_Y.setTimestamp(timeStamp);
			deliveries_Y.setTotalSale(subTotal);
			response.add(deliveries_Y);
		}
		return response;
	}

	
	}
