package com.maidin.adminApi.admin;

import java.util.Locale;

import com.maidin.pojo.notifications.AllDeviceNotificationRequest;
import com.maidin.pojo.notifications.InfoNotificationHistoryResponse;
import com.maidin.pojo.notifications.NotificationResponse;

public interface NotificationMgmt {

	NotificationResponse sendMessageToAllDevices(String authToken, AllDeviceNotificationRequest request, Locale locale)
			throws Exception;

	void addNotificationInfo(String title, String message, Long adminId, Long successCount, Long failureCount)
			throws Exception;

	InfoNotificationHistoryResponse getSentInfoNotifications(String authToken, int pageNumber, Locale locale) throws Exception;

}
