package com.maidin.adminApi.enums;

public enum ScheduleStatus {
	
PAUSE(1, "PAUSE"),
	
	RESUME(2, "RESUME"),
	
	REMOVE(3, "REMOVE"),

	PAUSENEXT(5, "PAUSENEXT"),
	
	OUT_FOR_DELIVERY(4, "OUT_FOR_DELIVERY");
	
	private final int id;
	private final String name;
	
	private ScheduleStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static ScheduleStatus fromId(int id) throws Exception {
		for (ScheduleStatus t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Description: " + id);
	}
}
