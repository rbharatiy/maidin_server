package com.maidin.adminApi.enums;

public enum ImageCategory {

    PRODUCT(1, "PRODUCT"),
    
    CATEGORY(2, "CATEGORY"),
    
    BRAND(3, "BRAND"),
    
    PAYMENTOPTION(4, "PAYMENTOPTION"),
    
    PRODUCTDESCRIPTION(5, "PRODUCTDESCRIPTION"),
    
    PRODUCTDETAIL(6, "PRODUCTDETAIL"), SUBCATEGORY(7,"SUBCATEGORY");
	
	private final int id;
	private final String name;
	
	private ImageCategory(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public static ImageCategory fromId(int id) throws Exception {
		for (ImageCategory imageCategory : values()) {
			if (imageCategory.id == id) {
				return imageCategory;
			}
		}
		throw new Exception("Invalid ImageCategory: " + id);
	}
}
