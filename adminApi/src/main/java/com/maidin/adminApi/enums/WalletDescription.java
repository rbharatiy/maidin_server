package com.maidin.adminApi.enums;

public enum WalletDescription {
	
	/*RECHARGE(1, "RECHARGE"),
	
	DEDUCTIONS(2, "DEDUCTIONS"),
	
	DEBT(3, "DEBT"),
	
	SETTLED(4, "SETTLED"),
	
	DISCHARGE(5, "DISCHARGE"),
	
	PAYMENT_AT_DELIVERY(6, "PAYMENT_AT_DELIVERY"),
	
	NA(0, "NA"),
	
	REFUND(7,"REFUND")
	*/
	
	/*RECHARGE(1, "Wallet Recharge"),

	DEDUCTIONS(2, "Order Deductions at Delivery"),

	DEBT(3, "Debt"),

	SETTLED(4, "Settled"),

	DISCHARGE(5, "Wallet Discharge"),
	
	PAYMENT_AT_DELIVERY(6, "Payment done at delivery"),
	
	NA(0, "NA"),
	
	REFUND_ON_UNDELIVERED(7, "Refund on Undelivered "),
	
	REFUND_ON_CANCELLATION(8, "Refund on Cancellation "),
	
	REFUND_ON_QUANTITY_DECREASE(9, "Refund on Quantity Decrease"),
	
	DEDUCTIONS_ON_QUANTITY_INCREASE(10, "Deductions on Quantity Increase"),
	
	RECHARGE_BY_MARKDELIVERY(11, "Extra amount recharge after Marking delivery"),
	
	BATCH_DEDUCTIONS(12, "Batch Deductions")*/
	
	 RECHARGE(1, "Wallet recharge by admin"),

     DEDUCTIONS(2, "Order amount deduction on marking delivery"),

     DEBT(3, "Order amount in debt"),

     SETTLED(4, "Debt amount settlement"),

     DISCHARGE(5, "Wallet discharge by admin"),
     
     AMOUNT_RECEIVED_AT_DELIVERY(6, "Amount received at delivery"),
     
     NA(0, "NA"),
     
     REFUND_ON_UNDELIVERED(7, "Refund on undelivered order"),
     
     REFUND_ON_CANCELLATION(8, "Refund on order cancellation "),
     
     REFUND_ON_QUANTITY_DECREASE(9, "Refund on quantity decrease"),
     
     DEDUCTIONS_ON_QUANTITY_INCREASE(10, "Deductions on quantity increase"),
     
     RECHARGE_BY_MARKDELIVERY(11, "Extra amount recharge after marking delivery"),
     
     BATCH_DEDUCTIONS(12, "Wallet amount deduction by batch")
	/*
	MONEY_IN(12,"Money Added"),
	
	MONEY_OUT(13,"Money Deducted"),*/
	;
	
	private final int id;
	private final String name;
	
	private WalletDescription(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static WalletDescription fromId(int id) throws Exception {
		for (WalletDescription t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Description: " + id);
	}
}
