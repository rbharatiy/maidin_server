package com.maidin.adminApi.enums;

public enum NotificationType {
	INFO_NOTIFICATION(1, "INFO_NOTIFICATION"),

	DATA_NOTIFICATION(2, "DATA_NOTIFICATION");

	private final int id;
	private final String name;

	private NotificationType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public static NotificationType fromId(int id) throws Exception {
		for (NotificationType t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Order Status: " + id);
	}
}
