package com.maidin.adminApi.enums;

public enum DocumentType {
	
	PAN(1, "PAN"),
	
	UAN(2, "UAN"),
	
	ADHAAR(3, "ADHAAR"),
	
	PASSPORT(4, "PASSPORT"),
	
	VOTER(5, "VOTER"), 
	
	PHOTO(6, "PHOTO"),
	
	EDUCATION(7, "EDUCATION"), 
	
	OCCUPATION(8, "OCCUPATION"),
	
	OFFERLETTER(9, "OFFERLETTER"),
	
	SALARYSLIP(10, "SALARYSLIP"),
	
	COMPANYTAG(11, "COMPANYTAG"),
	
	FORM16(12, "FORM16"),
	
	DEGREE(13, "DEGREE"),
	
	MARKSHEET(14, "MARKSHEET"),
	
	COLLEGEID(15, "COLLEGEID");
	
	private final int id;
	private final String name;
	
	private DocumentType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static DocumentType fromId(int id) throws Exception {
		for (DocumentType t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Document Type: " + id);
	}
}
