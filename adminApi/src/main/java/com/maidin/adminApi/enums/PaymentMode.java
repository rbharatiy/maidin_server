package com.maidin.adminApi.enums;

public enum PaymentMode {
	
	ByCash(1, "Cash"),
	
	ByWallet(2, "Maidin Money"),
	
	ByPaytm(3, "PayTm"),
	
	ByCashByWallet(4, "Cash | Maidin Money"),
	
	PARTIAL_WALLET_PARTIAL_PENDING(5, "PARTIAL_WALLET_PARTIAL_PENDING"),
	
	PENDING(6, "Pending"),
	
	PARTIAL_PAYMENT(7, "Partial Payment"),
	
	PARTIAL_CASH_PARTIAL_PENDING(8, "PARTIAL_CASH_PARTIAL_PENDING"),
	
	PhonePe(9, "PhonePe"),
	
	ByCard(10, "Card"),
	
	NA(0, "NA"),
	
	UPI(11, "UPI")
	
	;
	
	private final int id;
	private final String name;
	
	private PaymentMode(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static PaymentMode fromId(int id) throws Exception {
		for (PaymentMode t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Payment Mode: " + id);
	}
}
