package com.maidin.adminApi.enums;

public enum PaymentStatus {
	
	PENDING(1, "PENDING"),
	
	PAYMENTSETTLED(2, "PAYMENTSETTLED"),
	
	NA(3, "NA"),
	
	PARTIALPAYMENT(4, "PARTIALPAYMENT");
	
	private final int id;
	private final String name;
	
	private PaymentStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static PaymentStatus fromId(int id) throws Exception {
		for (PaymentStatus t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Order Status: " + id);
	}
}
