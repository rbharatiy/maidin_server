package com.maidin.adminApi.enums;

public enum OrderStatus {
	
	CONFIRMED(1, "CONFIRMED"),
	
	INPROGESS(2, "INPROGESS"),
	
	DELIVERED(3, "DELIVERED"),
	
	CANCELLED(4, "CANCELLED"),
	
	DELIVERYSTATUSPENDING(5, "DELIVERYSTATUSPENDING"),
	
	UNDELIVERED(6, "UNDELIVERED"),
	
	SETTLED(7, "SETTLED"),
	
	OUT_FOR_DELIVERY(8, "OUT_FOR_DELIVERY"),
	
	PARTIAL_DELIVERED(9, "PARTIAL_DELIVERED")
	;
	
	private final int id;
	private final String name;
	
	private OrderStatus(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public static OrderStatus fromId(int id) throws Exception {
		for (OrderStatus t : values()) {
			if (t.id == id) {
				return t;
			}
		}
		throw new Exception("Invalid Order Status: " + id);
	}
}
