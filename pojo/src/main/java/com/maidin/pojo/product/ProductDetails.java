//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-833 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.01 at 04:50:59 PM IST 
//


package com.maidin.pojo.product;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.maidin.pojo.GenericResponse;


/**
 * <p>Java class for productDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="productDetails">
 *   &lt;complexContent>
 *     &lt;extension base="{http://pojo.maidin.com}generic-response">
 *       &lt;sequence>
 *         &lt;element name="productId" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="quantityUnit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="price" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="activeStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="stockStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="demandStatus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="imageLastUpdatedOn" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "productDetails", propOrder = {
    "productId",
    "productName",
    "productCode",
    "quantityUnit",
    "price",
    "activeStatus",
    "stockStatus",
    "demandStatus",
    "imageLastUpdatedOn"
})
public class ProductDetails
    extends GenericResponse
    implements Serializable
{

    protected long productId;
    @XmlElement(required = true)
    protected String productName;
    @XmlElement(required = true)
    protected String productCode;
    @XmlElement(required = true)
    protected String quantityUnit;
    protected float price;
    protected boolean activeStatus;
    protected boolean stockStatus;
    protected boolean demandStatus;
    protected long imageLastUpdatedOn;

    /**
     * Gets the value of the productId property.
     * 
     */
    public long getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     */
    public void setProductId(long value) {
        this.productId = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the quantityUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityUnit() {
        return quantityUnit;
    }

    /**
     * Sets the value of the quantityUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityUnit(String value) {
        this.quantityUnit = value;
    }

    /**
     * Gets the value of the price property.
     * 
     */
    public float getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     */
    public void setPrice(float value) {
        this.price = value;
    }

    /**
     * Gets the value of the activeStatus property.
     * 
     */
    public boolean isActiveStatus() {
        return activeStatus;
    }

    /**
     * Sets the value of the activeStatus property.
     * 
     */
    public void setActiveStatus(boolean value) {
        this.activeStatus = value;
    }

    /**
     * Gets the value of the stockStatus property.
     * 
     */
    public boolean isStockStatus() {
        return stockStatus;
    }

    /**
     * Sets the value of the stockStatus property.
     * 
     */
    public void setStockStatus(boolean value) {
        this.stockStatus = value;
    }

    /**
     * Gets the value of the demandStatus property.
     * 
     */
    public boolean isDemandStatus() {
        return demandStatus;
    }

    /**
     * Sets the value of the demandStatus property.
     * 
     */
    public void setDemandStatus(boolean value) {
        this.demandStatus = value;
    }

    /**
     * Gets the value of the imageLastUpdatedOn property.
     * 
     */
    public long getImageLastUpdatedOn() {
        return imageLastUpdatedOn;
    }

    /**
     * Sets the value of the imageLastUpdatedOn property.
     * 
     */
    public void setImageLastUpdatedOn(long value) {
        this.imageLastUpdatedOn = value;
    }

}
