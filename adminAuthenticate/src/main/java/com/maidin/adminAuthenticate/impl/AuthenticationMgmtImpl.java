package com.maidin.adminAuthenticate.impl;

import static com.google.common.base.Preconditions.checkNotNull;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;
import org.ehcache.xml.exceptions.XmlConfigurationException;
import org.springframework.beans.factory.annotation.Autowired;


import com.maidin.adminAuthenticate.Auth;
import com.maidin.adminAuthenticate.AuthenticationMgmt;
import com.maidin.adminAuthenticate.exception.AuthenticationException;
import com.maidin.adminAuthenticate.properties.ResourceProperties;
import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerDTO;

/*import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;*/

public class AuthenticationMgmtImpl implements AuthenticationMgmt{
	
    /*private final CacheManager manager;
    private final Cache authTokenCache;*/
	private final Configuration xmlConfig;
	private final CacheManager manager;
	private final Cache<String, String> authTokenCache;
    private final AdminDAO adminDAO;
    
   @Autowired
    public AuthenticationMgmtImpl(AdminDAO adminDAO) throws XmlConfigurationException, MalformedURLException{
    	super();
       /* manager = CacheManager.newInstance(ResourceProperties.EHCACHE_XML_FILE_LOCATION);
        authTokenCache = manager.getCache("authenticationToken");   */
    	xmlConfig = new XmlConfiguration(Paths.get(ResourceProperties.EHCACHE_XML_FILE_LOCATION).toUri().toURL());
		manager = CacheManagerBuilder.newCacheManager(xmlConfig);
		manager.init();
		authTokenCache = manager.getCache("authenticationToken", String.class, String.class);
       this.adminDAO = adminDAO;
    }
   

   
   
   /*  @Override
   public void removeAuthToken(String authToken) {
       checkNotNull(authToken);
       authTokenCache.remove(authToken);        
   }*/
   
   @Override
	public void removeAuthToken(String authToken) {
		checkNotNull(authToken);
		//System.out.println("authToken that is going to remove "+authToken+ "MobileNumber "+ authTokenCache.get(authToken));
		authTokenCache.remove(authToken);
		/*if(authTokenCache.containsKey(authToken)){
			System.out.println("OOPS, authToken does not removed "+ authToken);
		}
		else{
			System.out.println("YESSS, authToken  removed "+ authToken+ " MobileNumber "+ authTokenCache.get(authToken));
		}*/
	}

   
   
  /* public void storeAuthToken(String authToken, String mobileNumber) {
       authTokenCache.put(new Element(authToken, mobileNumber));
	   authTokenCache.put(authToken, mobileNumber);
   }*/
   
   public void storeAuthToken(String authToken, String mobileNumber) {
		authTokenCache.put(authToken, mobileNumber);
		/*System.out.println("Going to store Key in Cache");
		if(authTokenCache.containsKey(authToken)){
			System.out.println("I am stored in cache "+authToken + "Mobile Number"+mobileNumber);
		}
		else{
			System.out.println("Failed to store in cache "+authToken + "Mobile Number"+mobileNumber);
			List<Cache.Entry<String, String>> cacheList = StreamSupport.stream(authTokenCache.spliterator(), false)
					.collect(Collectors.toList());
			System.out.println("Entries list" + cacheList.size());
			for(Cache.Entry<String, String> entry : cacheList){
				System.out.println("AuthToken()  "+entry.getKey() +" || MobileNumber  "+entry.getValue());
			}
			//authTokenCache.
		}*/
   }
   
   @Override
   public void destroy(){
      /* authTokenCache.dispose();
       manager.shutdown();   */
	   System.out.println("Cache is destroyed");
	   authTokenCache.clear();
       manager.close();
   }

	@Override
	public String generateAuthToken(String mobileNumber) {
		 String authToken = UUID.randomUUID().toString()+"A";
		 storeAuthToken(authToken, mobileNumber); 
		return authToken;
		 	
	}

	@Override
	public String checkAccess(Auth auth) throws AuthenticationException {
	   //checkNotNull(auth);
       checkNotNull(auth.getAuthToken());
      
       
     /*  Element cachedResult = authTokenCache.get(auth.getAuthToken());*/
       Object cachedResult = authTokenCache.get(auth.getAuthToken());
       if(cachedResult == null){
    	  // System.out.println("cachedResult == null " + (cachedResult == null) + "earlier authToken of admin is "+ auth.getAuthToken());
           throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");                
       }
       else{
    	   String mobileNumber = (String)cachedResult.toString();
           /*String mobileNumber = ((String)cachedResult.getObjectValue()).toString();*/
           storeAuthToken(auth.getAuthToken(), mobileNumber);
           AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(mobileNumber);
           if(adminDTO != null){
        	   if(adminDTO.isActive()){
        			//response.setDriverId(driverDTO.getId());
        	   } else {
        		   throw new AuthenticationException("ADMIN_ID_IS_DEACTIVATED", "Admin Account is deactivated");  
        	   }
           
           }
           else if(adminDTO == null){
           	throw new AuthenticationException("ADMIN_DOES_NOT_EXIST", "Admin does not exist.");
           }
           return mobileNumber;
       }
	}




	/*@Override
	public String getAdminMobile(Auth auth) throws AuthenticationException {
   	Element cachedResult = authTokenCache.get(auth.getAuthToken());
       if(cachedResult == null){
           throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");                
       }
       else{
           String mobileNumber = ((String)cachedResult.getObjectValue()).toString();
           storeAuthToken(auth.getAuthToken(), mobileNumber);
           return mobileNumber;
       }
   }*/
	
	@Override
	public String getAdminMobile(Auth auth) throws AuthenticationException {
   /*	Element cachedResult = authTokenCache.get(auth.getAuthToken());*/
		 Object cachedResult = authTokenCache.get(auth.getAuthToken());
		
       if(cachedResult == null){
    	  // System.out.println("cachedResult == null " + (cachedResult == null) + "earlier authToken of admin is "+ auth.getAuthToken());
           throw new AuthenticationException("INVALID_AUTH_TOKEN", "Invalid authentication token.");                
       }
       else{
          /* String mobileNumber = ((String)cachedResult.getObjectValue()).toString();*/
    	   String mobileNumber = (String)cachedResult.toString();
           storeAuthToken(auth.getAuthToken(), mobileNumber);
           //response.setMobileNumber(mobileNumber);
           AdminDTO adminDTO = adminDAO.getByMobileNumberWithoutActiveState(mobileNumber);
           if(adminDTO != null){
        	   if(adminDTO.isActive()){
        			//response.setDriverId(driverDTO.getId());
        	   } else {
        		   throw new AuthenticationException("100071", "Admin Account is deactivated");  
        	   }
           
           }
           else if(adminDTO == null){
              	throw new AuthenticationException("100023", "Admin does not exist.");
              }
           return mobileNumber;
       }
   }

}
