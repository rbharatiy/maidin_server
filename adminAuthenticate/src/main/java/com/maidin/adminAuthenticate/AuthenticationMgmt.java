package com.maidin.adminAuthenticate;

import com.maidin.adminAuthenticate.exception.AuthenticationException;

public interface AuthenticationMgmt {

	String generateAuthToken(String mobileNumber);

	String getAdminMobile(Auth auth) throws AuthenticationException;

	void removeAuthToken(String authToken);

	void destroy();

	String checkAccess(Auth auth) throws AuthenticationException;}
