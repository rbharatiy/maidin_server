package com.maidin.adminAuthenticate.properties;

import java.io.File;

/**
 * Class containing the key name in the resource files
 * 
 */
public class ResourceProperties {

	public static final String EHCACHE_XML_FILE_LOCATION = System.getProperty("config.dir") + 
            File.separator + "ehcache-admin.xml";
}
