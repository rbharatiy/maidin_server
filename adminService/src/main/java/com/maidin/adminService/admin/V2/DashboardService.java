package com.maidin.adminService.admin.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.V2.DashboardMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.admin.CustomerCountInfoResponse;

@Controller
public class DashboardService {

	@Autowired
	private DashboardMgmt manager;

	@RequestMapping(value = "v2.0/get/dashboard/info", method = RequestMethod.GET)
	public @ResponseBody CustomerCountInfoResponse getDashboardInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();
		try {
			response = manager.getDashboardInfo(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
}
