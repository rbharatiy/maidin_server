package com.maidin.adminService.admin.V2;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.V2.SearchMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.pojo.search.AdminPopularSearches;
import com.maidin.pojo.search.CustomerPopularSearches;
import com.maidin.pojo.search.PopularSearchesAdminResponse;
import com.maidin.pojo.search.PopularSearchesCustomerResponse;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;

@Controller
public class SearchService {

	@Autowired
	private SearchMgmt manager;

	@RequestMapping(value = "v2.0/search/products", method = RequestMethod.PUT)
	public @ResponseBody SearchProductListResponse searchProducts(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody SearchProductRequest request) {
		SearchProductListResponse response = new SearchProductListResponse();
		try {
			response = manager.searchProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/get/popular/searches", method = RequestMethod.GET)
	public @ResponseBody PopularSearchesAdminResponse getPopularSearches(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		PopularSearchesAdminResponse response = new PopularSearchesAdminResponse();
		try {
			List<PopularSearchesDTO> popularSearchesList = manager.getAllPopularSearchKeywords(authToken,
					ServiceUtils.getLocale(language));
			if (popularSearchesList.size() > 0) {
				for (PopularSearchesDTO dto : popularSearchesList) {
					AdminPopularSearches popularSearches = new AdminPopularSearches();
					popularSearches.setId(dto.getId());
					popularSearches.setKeyword(dto.getKeyword());
					popularSearches.setPriority(dto.getPriority());
					popularSearches.setUpdatedBy(dto.getUpdatedBy());
					popularSearches.setCreatedBy(dto.getCreatedBy());
					popularSearches.setUpdatedOn(dto.getUpdatedOn().getTime());
					popularSearches.setCreatedOn(dto.getCreatedOn().getTime());
					popularSearches.setActive(dto.isActive());

					response.getPopularSearchesList().add(popularSearches);
				}
			} else {
				response.getPopularSearchesList().clear();
			}

			response = ApiUtils.setResponseWithOperationId(response, OperationId.ADMIN_SERVICES);
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

}
