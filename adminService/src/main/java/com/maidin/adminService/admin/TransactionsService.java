package com.maidin.adminService.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.TransactionHistoryMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.transactions.GetCustomerTransactionResponse;
import com.maidin.pojo.transactions.GetCustomerTransactionsRequest;

@Controller
@RequestMapping("/transactions")
public class TransactionsService {

	@Autowired
	private TransactionHistoryMgmt manager;

	@RequestMapping(value = "get/customer/transactions/{currentPageNumber}", method = RequestMethod.PUT)
	public @ResponseBody GetCustomerTransactionResponse getScheduledOrders(@PathVariable Integer currentPageNumber,
			@RequestBody GetCustomerTransactionsRequest customerTransactionsRequest,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetCustomerTransactionResponse response = new GetCustomerTransactionResponse();
		try {
			response = manager.getCustomerTransactionByPageNumber(authToken, customerTransactionsRequest.getMobile(),
					currentPageNumber, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}
