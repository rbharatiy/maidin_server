package com.maidin.adminService.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.AdminMgmt;
import com.maidin.adminApi.admin.ReportMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.OrderUpdate;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.UpdateProductRequest;

@Controller
public class ReportService {

	@Autowired
	private ReportMgmt manager;

	@RequestMapping(value = "admin/get/order/report", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getOrderReportV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody OrderRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getOrderReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "admin/get/new/customer/registration/report", method = RequestMethod.GET)
	public @ResponseBody GenericResponse getCustomerRecords(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.getNewCustomerRegistrationsReport(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
}
