package com.maidin.adminService.admin.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.V2.MarkDeliveryMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.markdelivery.GetOrderRequest;
import com.maidin.pojo.markdelivery.GetOrderResponse;
import com.maidin.pojo.markdelivery.GetPendingAmount;
import com.maidin.pojo.markdelivery.MarkOrder;
import com.maidin.pojo.markdelivery.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.markdelivery.ChangeDeliveredOrderStateRequest;

@Controller
public class MarkDeliveryService {

	@Autowired
	private MarkDeliveryMgmt manager;

	

	@RequestMapping(value = "v2.0/get/deliveryList", method = RequestMethod.PUT)
	public @ResponseBody GetOrderResponse getDeliveryList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetOrderRequest request) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.getDeliveryList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	
	
	
	@RequestMapping(value = "v2.0/mark/delivered", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse markOrderDeliveredV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody MarkOrder request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.markDeliveryApiV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/mark/updated/order", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse markOrderDeliveredWithQuantityUpdateV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody MarkOrderWithQtyUpdateRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.markOrderDeliveredWithQtyUpdateV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "v2.0/change/order/state", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse changeDeliveredOrderState(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody ChangeDeliveredOrderStateRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.changeDeliveredOrderStateV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/get/pending/amount", method = RequestMethod.PUT)
	public @ResponseBody GetPendingAmount getPendingAmount(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language
			/*@RequestBody ChangeDeliveredOrderStateRequest request*/) {
		GetPendingAmount response = new GetPendingAmount();
		try {
			response = manager.getPendingAmount(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	
	@RequestMapping(value = "v2.0/update/photo", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse updatePhotoIdInOtherCities(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language
			/*@RequestBody ChangeDeliveredOrderStateRequest request*/) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.updatePhotoIdInOtherCities(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	
}
