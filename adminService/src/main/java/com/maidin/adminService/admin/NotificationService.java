package com.maidin.adminService.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.NotificationMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.notifications.AllDeviceNotificationRequest;
import com.maidin.pojo.notifications.InfoNotificationHistoryResponse;
import com.maidin.pojo.notifications.NotificationResponse;

@Controller
@RequestMapping("/notification")
public class NotificationService {

	@Autowired
	NotificationMgmt notificationMgmt;

	@RequestMapping(value = "send/to/all", method = RequestMethod.PUT)
	public @ResponseBody NotificationResponse sendNotificationToAllDevices(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody AllDeviceNotificationRequest request) {
		NotificationResponse response = new NotificationResponse();
		try {
			response = notificationMgmt.sendMessageToAllDevices(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/sent/info/notifications/{pageNumber}", method = RequestMethod.PUT)
	public @ResponseBody InfoNotificationHistoryResponse getSentInfoNotifications(
			@RequestHeader("authToken") String authToken, @PathVariable Integer pageNumber,
			@RequestHeader(value = "accept-language", required = false) String language) {
		InfoNotificationHistoryResponse response = new InfoNotificationHistoryResponse();
		try {
			response = notificationMgmt.getSentInfoNotifications(authToken, pageNumber,
					ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

}
