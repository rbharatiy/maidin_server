package com.maidin.adminService.admin.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.V2.ProductUpdateMgmt;
import com.maidin.adminApi.admin.V2.SearchMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.adminv2.ChangeStatusRequest;
import com.maidin.pojo.adminv2.UpdateProductRequest;
import com.maidin.pojo.adminv2.UpdateProductResponse;
import com.maidin.pojo.search.SearchProductListResponse;
import com.maidin.pojo.search.SearchProductRequest;

@Controller
public class ProductUpdateService {

	@Autowired
	private ProductUpdateMgmt manager;

	@RequestMapping(value = "v2.0/upsert/products", method = RequestMethod.PUT)
	public @ResponseBody UpdateProductResponse searchProducts(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody UpdateProductRequest request) {
		UpdateProductResponse response = new UpdateProductResponse();
		try {
			response = manager.upsertProduct(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "v2.0/change/status", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse changeFlag(@RequestHeader("authToken") String authToken,
			@RequestBody ChangeStatusRequest request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.changeStatus(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}
