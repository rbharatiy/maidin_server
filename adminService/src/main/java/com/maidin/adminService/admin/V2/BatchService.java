package com.maidin.adminService.admin.V2;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.AdminMgmt;
import com.maidin.adminApi.admin.V2.BatchMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.common.properties.ResourceProperties;
import com.maidin.common.util.CommonUtils;
import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.OrderUpdate;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.UpdateProductRequest;

@Controller
public class BatchService {

	@Autowired
	private BatchMgmt manager;

	    @Scheduled(cron="0 1 1 28 * *", zone="Asia/Calcutta")
	   @RequestMapping(value = "batch/next/month/scheduledOrder", method = RequestMethod.PUT)
		public void addNextMonthSchedule() {
			try {
				manager.addMonthlyOrderInSchedule();
			} catch (Exception e) {
				 }
		}
	
	   /* @Scheduled(cron="0 0 6 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/1", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlotIst() {
			
			try {
				GenericResponse response = new GenericResponse();
				response = manager.batchPriceDeductionAtSlotStart(1);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
		@Scheduled(cron="0 0 9 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/2", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlot2nd() {
			
			try {
				GenericResponse response = new GenericResponse();
				response = manager.batchPriceDeductionAtSlotStart(2);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
		
		@Scheduled(cron="0 0 12 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/3", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlot3rd() {
			
			try {
				GenericResponse response = new GenericResponse();
				response = manager.batchPriceDeductionAtSlotStart(3);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
		
		@Scheduled(cron="0 0 15 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/4", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlot4th() {
			
			try {
				GenericResponse response = new GenericResponse();
				response = manager.batchPriceDeductionAtSlotStart(4);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
		//Fire at 12:00 PM (noon) every day 0 0 12 * * ?	
		//@Scheduled(cron="0 49 17 ? * *", zone="Asia/Calcutta")
		@Scheduled(cron="0 0 18 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/5", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlot5th() {
			
			try {
				GenericResponse response = new GenericResponse();
				response = manager.batchPriceDeductionAtSlotStart(5);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
		*/
	    
		//@Scheduled(cron="${cronExpression1}", zone="Asia/Calcutta")
		//@Scheduled(cron="#{@getCronValue}", zone="Asia/Calcutta")
		//@Scheduled(cron = "${jobs.mediafiles.imagesPurgeJob.schedule}")
		/*@Scheduled(cron="0 0 18 * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot/6", method = RequestMethod.PUT)
		public void  batchPriceSettingsForScheduledOrdersForTimeSlot6th() {
			
			try {
				GenericResponse response = new GenericResponse();
				//System.out.println("cronExpression1---"+getCronValue());
				response = manager.batchPriceDeductionAtSlotStart(6);
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}*/
		
		/*@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable1;
		@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable2;

		@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable3;

		@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable4;

		@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable5;

		@Value("${jobs.mediafiles.imagesPurgeJob.enable}")
	    private int imagesPurgeJobEnable6;


		@Bean
		public String getCronValue()
		{
			//int timeSlot = 1;
	        if(imagesPurgeJobEnable1 == 1){
	        	return "0 49 17 ? * *";
	        }
	        else if(imagesPurgeJobEnable2 == 2){
	        	return "0 49 17 ? * *";
	        }
	        else if(imagesPurgeJobEnable2 == 2){
	        	return "0 49 17 ? * *";
	        }
	        else if(imagesPurgeJobEnable2 == 2){
	        	return "0 49 17 ? * *";
	        }
	        else if(imagesPurgeJobEnable2 == 2){
	        	return "0 49 17 ? * *";
	        }
	        else if(imagesPurgeJobEnable2 == 2){
	        	return "0 49 17 ? * *";
	        }
	        else{
	        		return "0 49 17 ? * *";
	 	       
	        }
		    //cronRepo.findOne("cron").getCronValue();
		}
		
		private static final Properties prop = loadProperties(ResourceProperties.COMMON_FILE_LOCATION);
		private static Properties loadProperties(String fileName) {
			try {
				return CommonUtils.loadPropertyFile(new File(fileName));
			} catch (Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Unable to load the properties file: " + fileName);
			}
		}
		private static final String cronExpression1 = prop.getProperty(ResourceProperties.CRON_EXPRESSION1);
*/	
		
		
		@Scheduled(cron="0 0 * * * ?", zone="Asia/Calcutta")
		@RequestMapping(value = "batch/price/deduction/slot", method = RequestMethod.PUT)
		public void  batchPriceDeduction() {
			
			try {
				GenericResponse response = new GenericResponse();
				//System.out.println("cronExpression1---"+getCronValue());
				Calendar cal = Calendar.getInstance();
				int hour = cal.get(Calendar.HOUR_OF_DAY);
				System.out.println("hour"+hour);
				List<Integer> timeSlotIdList  = manager.getTimeSlotIdListByStartTime(hour);
				if(timeSlotIdList.size() > 0){
					System.out.println("timeSlotIdList.size"+timeSlotIdList);
				response = manager.batchPriceDeductionAtSlotStart(timeSlotIdList);
				}
			} catch (Exception e) {
				e.getStackTrace();
				return;
			}
			
		}
}
