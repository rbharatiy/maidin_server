package com.maidin.adminService.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.StocksReportMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.reports.PurchaseOrderInfoRequest;

@Controller
public class StocksReportService {

	@Autowired
	private StocksReportMgmt manager;

	@RequestMapping(value = "v2.0/get/purchase/report", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getPurchaseOrderReport(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody PurchaseOrderInfoRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getPurchaseOrderReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@Scheduled(cron = "30 59 23 * * ?", zone = "Asia/Calcutta")
	@RequestMapping(value = "batch/update/wallet/ledger", method = RequestMethod.PUT)
	public void batchUpdateWalletLedger() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchUpdateWalletLedger();
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}

}
