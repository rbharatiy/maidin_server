package com.maidin.adminService.admin.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.V2.ReportMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.adminv2.OrderRequest;
import com.maidin.pojo.reports.BalanceReportRequest;
import com.maidin.pojo.reports.LedgerReportRequest;

@Controller("ReportServiceV2")
public class ReportService {

	@Autowired
	private ReportMgmt manager;

	@RequestMapping(value = "v2.0/get/order/report", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getOrderReportV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody OrderRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getOrderReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/get/balance/report", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getBalanceReport(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody BalanceReportRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getBalanceReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/get/ledger/report", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse getBalanceReport(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody LedgerReportRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.getLedgerReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}
