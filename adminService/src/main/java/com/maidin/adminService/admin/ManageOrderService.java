package com.maidin.adminService.admin;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.ManageCustomerOrderMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.cart.ScheduleProductRequestV2;
import com.maidin.pojo.manageorders.GetScheduleOrdersRequest;
import com.maidin.pojo.manageorders.GetScheduledProductsResponse;

@Controller
@RequestMapping("/manageOrder")
public class ManageOrderService {

	@Autowired
	private ManageCustomerOrderMgmt manager;

	@RequestMapping(value = "v2/get/scheduled/orders", method = RequestMethod.PUT)
	public @ResponseBody GetScheduledProductsResponse getScheduledOrders(
			@RequestBody GetScheduleOrdersRequest getScheduleOrdersRequest,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetScheduledProductsResponse response = new GetScheduledProductsResponse();
		try {
			response = manager.getScheduledOrders(getScheduleOrdersRequest, authToken,
					ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "v2/update/schedule", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse scheduleOrder(
			@RequestHeader("authToken") String authToken,
			@RequestBody ScheduleProductRequestV2 request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.updateScheduledOrder(authToken, request, ServiceUtils.getLocale(language));			
		} catch (Exception e) {
		
			 return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}