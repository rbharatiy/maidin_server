package com.maidin.adminService.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maidin.adminApi.admin.AdminMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.admin.GenerateMobileOtp;
import com.maidin.pojo.admin.GenerateMobileOtpResponse;
import com.maidin.pojo.admin.ItemRequest;
import com.maidin.pojo.admin.MarkOrder;
import com.maidin.pojo.admin.MarkOrderRequest;
import com.maidin.pojo.admin.MarkOrderWithQtyUpdateRequest;
import com.maidin.pojo.admin.OrderReportResponse;
import com.maidin.pojo.admin.OrderRequest;
import com.maidin.pojo.admin.OrderUpdate;
import com.maidin.pojo.admin.VerifyMobileOtp;
import com.maidin.pojo.admin.VerifyMobileOtpResponse;
import com.maidin.pojo.admin.WalletRequest;
import com.maidin.pojo.adminv2.UpsertWalletRequest;
import com.maidin.pojo.cart.GetWalletResponse;
import com.maidin.pojo.admin.BrandListResponse;
import com.maidin.pojo.admin.CategoryListResponse;
import com.maidin.pojo.admin.ChangeDeliveredOrderStateRequest;
import com.maidin.pojo.admin.GetBrandRequest;
import com.maidin.pojo.admin.GetOrderRequest;
import com.maidin.pojo.admin.GetOrderResponse;
import com.maidin.pojo.admin.GetProductRequest;
import com.maidin.pojo.admin.ProductListResponse;
import com.maidin.pojo.admin.ProductResponse;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.admin.AddressInfoRequest;
import com.maidin.pojo.admin.AddressInfoResponse;
import com.maidin.pojo.admin.CustomerCountInfoResponse;
import com.maidin.pojo.admin.CustomerCountRequest;
import com.maidin.pojo.admin.DeviceToken;
import com.maidin.pojo.admin.DeviceTokenResponse;
import com.maidin.pojo.admin.RegenerateAuthToken;
import com.maidin.pojo.admin.RegenerateAuthTokenResponse;
import com.maidin.pojo.admin.SearchProductListResponse;
import com.maidin.pojo.admin.SearchProductRequest;
import com.maidin.pojo.admin.UpdateProductRequest;

@Controller
@RequestMapping("/admin")
public class AdminService {

	@Autowired
	private AdminMgmt manager;

	@RequestMapping(value = "generate/mobile/otp", method = RequestMethod.PUT)
	public @ResponseBody GenerateMobileOtpResponse initiateGenerateMobileOtp(@RequestBody GenerateMobileOtp request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenerateMobileOtpResponse response = new GenerateMobileOtpResponse();
		try {
			response = manager.initiateGenerateMobileOTP(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {

			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "verify/mobile/number/otp", method = RequestMethod.PUT)
	public @ResponseBody VerifyMobileOtpResponse initiateVerifyMobileOtp(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody VerifyMobileOtp request) {
		VerifyMobileOtpResponse response = new VerifyMobileOtpResponse();
		try {
			response = manager.initiateVerifyMobileOtp(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "regenerate/authtoken", method = RequestMethod.PUT)
	public @ResponseBody RegenerateAuthTokenResponse regenAuthToken(
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody RegenerateAuthToken request) {
		RegenerateAuthTokenResponse response = new RegenerateAuthTokenResponse();
		try {
			response = manager.regenAuthToken(request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "upsert/token", method = RequestMethod.PUT)
	public @ResponseBody DeviceTokenResponse saveDeviceToken(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody DeviceToken request) {
		DeviceTokenResponse response = new DeviceTokenResponse();
		try {
			response = manager.saveDeviceToken(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	/*
	 * @RequestMapping(value = "registration", method = RequestMethod.PUT)
	 * public @ResponseBody CustomerRegistrationResponse registration(
	 * 
	 * @RequestHeader("authToken") String authToken,
	 * 
	 * @RequestHeader(value = "accept-language", required = false) String
	 * language,
	 * 
	 * @RequestBody CustomerRegistration request) { CustomerRegistrationResponse
	 * response = new CustomerRegistrationResponse(); try { response =
	 * manager.registration(request,authToken,
	 * ServiceUtils.getLocale(language)); } catch (Exception e) { return
	 * ApiUtils.setErrorResponseWithOperationId(response, e,
	 * OperationId.CUSTOMER_SERVICES); } return response; }
	 * 
	 * @RequestMapping(value = "/get/wallet/info", method = RequestMethod.GET)
	 * public @ResponseBody GetWalletResponse getWalletInfo(
	 * 
	 * @RequestHeader("authToken") String authToken,
	 * 
	 * @RequestHeader(value = "accept-language", required = false) String
	 * language ) { GetWalletResponse response = new GetWalletResponse(); try {
	 * response = manager.getWalletInfo(authToken,
	 * ServiceUtils.getLocale(language)); } catch (Exception e) { return
	 * ApiUtils.setErrorResponseWithOperationId(response, e,
	 * OperationId.CUSTOMER_SERVICES); } return response; }
	 */
	@RequestMapping(value = "get/order/report/V2", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getOrderReport(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody OrderRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getOrderReport(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/customer/report", method = RequestMethod.GET)
	public @ResponseBody GenericResponse getCustomerRecords(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.getCustomerRecords(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/update/wallet/info", method = RequestMethod.PUT)
	public @ResponseBody GetWalletResponse updateWalletInfo(@RequestBody UpsertWalletRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GetWalletResponse response = new GetWalletResponse();
		try {
			response = manager.upsertWalletInfo(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/customer/info", method = RequestMethod.PUT)
	public @ResponseBody AddressInfoResponse updateWalletInfo(@RequestBody AddressInfoRequest request,
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		AddressInfoResponse response = new AddressInfoResponse();
		try {
			response = manager.getCustomerAddressInfo(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/customer/count", method = RequestMethod.GET)
	public @ResponseBody CustomerCountInfoResponse getCustomerCount(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();
		try {
			response = manager.getCustomerCountInfo(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/get/count", method = RequestMethod.PUT)
	public @ResponseBody CustomerCountInfoResponse getCustomerCountByParameters(
			@RequestHeader("authToken") String authToken, @RequestBody CustomerCountRequest request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CustomerCountInfoResponse response = new CustomerCountInfoResponse();
		try {
			response = manager.getCustomerCountByParameters(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "/change/flag", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse changeFlag(@RequestHeader("authToken") String authToken,
			@RequestBody ItemRequest request,
			@RequestHeader(value = "accept-language", required = false) String language) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.changeFlag(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/category", method = RequestMethod.GET)
	public @ResponseBody CategoryListResponse getCategory(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		CategoryListResponse response = new CategoryListResponse();
		try {
			response = manager.getCategory(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/brands", method = RequestMethod.PUT)
	public @ResponseBody BrandListResponse getBrands(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetBrandRequest request) {
		BrandListResponse response = new BrandListResponse();
		try {
			response = manager.getBrands(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "get/products", method = RequestMethod.PUT)
	public @ResponseBody ProductListResponse getProductList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetProductRequest request) {
		ProductListResponse response = new ProductListResponse();
		try {
			response = manager.getProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	/*
	 * @RequestMapping(value = "mark/delivered", method = RequestMethod.PUT)
	 * public @ResponseBody GenericResponse markOrderDelivered(
	 * 
	 * @RequestHeader("authToken") String authToken,
	 * 
	 * @RequestHeader(value = "accept-language", required = false) String
	 * language,
	 * 
	 * @RequestBody OrderUpdate request) { GenericResponse response = new
	 * GenericResponse(); try { response = manager.markOrderDelivered(authToken,
	 * request, ServiceUtils.getLocale(language)); } catch (Exception e) {
	 * return ApiUtils.setErrorResponseWithOperationId(response, e,
	 * OperationId.ADMIN_SERVICES); } return response; }
	 */

	@RequestMapping(value = "mark/delivered", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse markOrderDelivered(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody MarkOrderRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.markOrderDelivered(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	/*@RequestMapping(value = "get/deliveryList", method = RequestMethod.PUT)
	public @ResponseBody GetOrderResponse getOrderList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetOrderRequest request) {
		GetOrderResponse response = new GetOrderResponse();
		try {
			response = manager.getOrderList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}*/

	@RequestMapping(value = "upsert/product", method = RequestMethod.PUT)
	public @ResponseBody ProductResponse updateProduct(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody UpdateProductRequest request) {
		ProductResponse response = new ProductResponse();
		try {
			response = manager.updateProduct(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	// @Scheduled(cron="0 57 11 * * *", zone="Asia/Calcutta")
	/*@RequestMapping(value = "batch/next/month/scheduledOrder", method = RequestMethod.PUT)
	public void addNextMonthSchedule() {
		// GenericResponse response = new GenericResponse();
		try {
			// GenericResponse response = new GenericResponse();
			manager.addOrderInSchedule();
		} catch (Exception e) {
			
			 * return ApiUtils.setErrorResponseWithOperationId(response, e,
			 * OperationId.ADMIN_SERVICES);
			 }
		// return response;
	}
*/
	@RequestMapping(value = "/scheduledOrder/{timeStamp}", method = RequestMethod.PUT)
	public void addOrderInSchedule(@PathVariable(value = "timeStamp") Long timeStamp) {
		// GenericResponse response = new GenericResponse();
		try {
			manager.addParticularDayInSchedule(timeStamp);
		} catch (Exception e) {
			e.printStackTrace();
			/*
			 * return ApiUtils.setErrorResponseWithOperationId(response, e,
			 * OperationId.ADMIN_SERVICES);
			 */}
		// return response;
	}

	@RequestMapping(value = "search/products", method = RequestMethod.PUT)
	public @ResponseBody SearchProductListResponse searchProducts(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody SearchProductRequest request) {
		SearchProductListResponse response = new SearchProductListResponse();
		try {
			response = manager.searchProductList(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	
	//@Scheduled(cron="0 0 6 * * ?", zone="Asia/Calcutta")

	/*@RequestMapping(value = "batch/schedule/price/1", method = RequestMethod.PUT)*/
	/*public void batchPriceSettingsForScheduledOrdersForTimeSlotIst() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchPriceDeductionForScheduledOrders(1);
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}

	//@Scheduled(cron="0 0 9 * * ?", zone="Asia/Calcutta")

	@RequestMapping(value = "batch/schedule/price/2", method = RequestMethod.PUT)
	public void batchPriceSettingsForScheduledOrdersForTimeSlot2nd() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchPriceDeductionForScheduledOrders(2);
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}

	
	//@Scheduled(cron="0 0 12 * * ?", zone="Asia/Calcutta")

	@RequestMapping(value = "batch/schedule/price/3", method = RequestMethod.PUT)
	public void batchPriceSettingsForScheduledOrdersForTimeSlot3rd() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchPriceDeductionForScheduledOrders(3);
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}

	
	//@Scheduled(cron="0 0 15 * * ?", zone="Asia/Calcutta")

	@RequestMapping(value = "batch/schedule/price/4", method = RequestMethod.PUT)
	public void batchPriceSettingsForScheduledOrdersForTimeSlot4th() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchPriceDeductionForScheduledOrders(4);
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}

	//Fire at 12:00 PM (noon) every day 0 0 12 * * ?	
	//@Scheduled(cron="0 49 17 ? * *", zone="Asia/Calcutta")
	//@Scheduled(cron="0 0 18 * * ?", zone="Asia/Calcutta")

	@RequestMapping(value = "batch/schedule/price/5", method = RequestMethod.PUT)
	public void batchPriceSettingsForScheduledOrdersForTimeSlot5th() {

		try {
			GenericResponse response = new GenericResponse();
			response = manager.batchPriceDeductionForScheduledOrders(5);
		} catch (Exception e) {
			e.getStackTrace();
			return;
		}

	}*/

	@RequestMapping(value = "get/order/report/changed", method = RequestMethod.PUT)
	public @ResponseBody OrderReportResponse getOrderReportV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody OrderRequest request) {
		OrderReportResponse response = new OrderReportResponse();
		try {
			response = manager.getOrderReportV2(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "V2/mark/delivered", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse markOrderDeliveredV2(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody MarkOrder request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.markDeliveryApiV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "V2/mark/updated/order", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse markOrderDeliveredWithQuantityUpdateV2(
			@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody MarkOrderWithQtyUpdateRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.markOrderDeliveredWithQtyUpdateV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "V2/change/order/state", method = RequestMethod.PUT)
	public @ResponseBody GenericResponse changeDeliveredOrderState(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody ChangeDeliveredOrderStateRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.changeDeliveredOrderStateV2(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

}
