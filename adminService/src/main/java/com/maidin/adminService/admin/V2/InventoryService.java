package com.maidin.adminService.admin.V2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.maidin.adminApi.admin.V2.InventoryMgmt;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.adminv2.DashboardEssentialsResponse;
import com.maidin.pojo.adminv2.GetCategoryV2Request;
import com.maidin.pojo.inventory.BrandListV2Response;
import com.maidin.pojo.inventory.CategoryListV2Response;
import com.maidin.pojo.inventory.GetProductListV2Request;
import com.maidin.pojo.inventory.GetSubCategoryV2Request;
import com.maidin.pojo.inventory.PaymentOptionResponse;
import com.maidin.pojo.inventory.ProductListV2Response;
import com.maidin.pojo.inventory.SubCategoryListV2Response;
import com.maidin.pojo.inventory.UpsertBrandRequest;

@RestController
public class InventoryService {

	@Autowired
	private InventoryMgmt manager;

	@PutMapping("/v2.0/inventory/get/category")
	public @ResponseBody CategoryListV2Response getV2Category(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetCategoryV2Request request) {
		CategoryListV2Response response = new CategoryListV2Response();
		try {
			response = manager.getCategoryListV2(request, authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@PutMapping("/v2.0/inventory/get/subcategory")
	public @ResponseBody SubCategoryListV2Response getV2Brand(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			/*
			 * @PathVariable Integer pageNum,
			 */@RequestBody GetSubCategoryV2Request request) {
		SubCategoryListV2Response response = new SubCategoryListV2Response();
		try {
			response = manager.getSubCategoryListV2(authToken, request/* ,pageNum */, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@GetMapping("v2.0/get/brand")
	public @ResponseBody BrandListV2Response getV2Brand(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {
		BrandListV2Response response = new BrandListV2Response();
		try {
			response = manager.getBrandListV2(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@PutMapping("/v2.0/inventory/get/product/{pageNum}")
	public @ResponseBody ProductListV2Response getV2Brand(@RequestHeader("authToken") String authToken,
			@PathVariable Integer pageNum, @RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody GetProductListV2Request request) {
		ProductListV2Response response = new ProductListV2Response();
		try {
			response = manager.getProductListV2(authToken, request, pageNum, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@RequestMapping(value = "v2.0/dashboard/lists", method = RequestMethod.GET)
	public @ResponseBody DashboardEssentialsResponse getDashboardList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language) {

		DashboardEssentialsResponse response = new DashboardEssentialsResponse();
		try {
			response = manager.getDashboardList(authToken, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	@PutMapping("/v2.0/inventory/upsert/brand")
	public @ResponseBody GenericResponse upsertBrand(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@RequestBody UpsertBrandRequest request) {
		GenericResponse response = new GenericResponse();
		try {
			response = manager.upsertBrand(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
}
