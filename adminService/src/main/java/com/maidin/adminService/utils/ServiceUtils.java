package com.maidin.adminService.utils;

import java.util.Locale;
import com.google.common.base.Strings;

public class ServiceUtils {


	public static Locale getLocale(String language) {
		if (Strings.isNullOrEmpty(language)) {
			language = "en";
		}
		return new Locale(language);
	}

}
