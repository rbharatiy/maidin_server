package com.maidin.adminService.utils;

import java.io.FileReader;

import org.json.simple.parser.JSONParser;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.util.json.JSONArray;
import com.amazonaws.util.json.JSONObject;
import com.maidin.common.util.EmailSender;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.pojo.common.ContactUsEmailRequest;
import com.maidin.pojo.common.ContactUsEmailResponse;

@RestController
@RequestMapping(value = "/emailer")
public class EmailerService {
	
	@RequestMapping(value = "/contact/us", method = RequestMethod.POST)
	public @ResponseBody ContactUsEmailResponse sendContactUsEmail(
			@RequestBody ContactUsEmailRequest request) {
		ContactUsEmailResponse response = new ContactUsEmailResponse();
		try {
			System.out.println("Test Email Came to service");
			String messageBody = getMessageBody(request);
			EmailSender.sendMail("support@maidin.in", request.getEmail() + "- Maidin Contact Us Form", messageBody);
			ApiUtils.setResponseWithOperationId(response, OperationId.CUSTOMER_SERVICES);
		} catch (Exception e) {
			 return ApiUtils.setErrorResponseWithOperationId(response, e,
					 OperationId.CUSTOMER_SERVICES);
		}
		return response;
	}
	
	public String getMessageBody(ContactUsEmailRequest request)
	{
		String messageBody = "Name : " + toTitleCase(request.getName()) + "<br /><br />"
							+ "Phone No. : " + request.getMobileNumber() + "<br /><br />"
							+ "Email : " + request.getEmail() +"<br /><br />"
							+ "Message : " + request.getMessage();
		return messageBody;
	}
	
    
    
	  
	  public String toTitleCase(String input) {
		    StringBuilder titleCase = new StringBuilder();
		    boolean nextTitleCase = true;

		    for (char c : input.toCharArray()) {
		        if (Character.isSpaceChar(c)) {
		            nextTitleCase = true;
		        } else if (nextTitleCase) {
		            c = Character.toTitleCase(c);
		            nextTitleCase = false;
		        }

		        titleCase.append(c);
		    }

		    return titleCase.toString();
		}
		//Read JSON file
		public JSONArray readJSON(String filepath) {
	        JSONParser parser = new JSONParser();
	        JSONArray json = null;
	        try {
	            FileReader fileReader = new FileReader(filepath);
	            json = (JSONArray) parser.parse(fileReader);
	            
	            /*System.out.println("json: " + json);*/

	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }
	        return json;
	    }
	    
	    
		//getting all list in one json
		@RequestMapping(value = "/getAllListsInJson", method = RequestMethod.GET)
		 public JSONObject getLocationList() {
		  JSONObject response = new JSONObject();
		  try {
		            String current = System.getProperty("config.dir");
		            String filepath = current + "/json/allLists.json";
		    
		   response = readJSONObject(filepath);
		  } catch (Exception e) {
	 }
		  return response;
		 }
		
		
		
		//Read JSON Objectfile
		  private JSONObject readJSONObject(String filepath) {
		         JSONParser parser = new JSONParser();
		         JSONObject json = null;
		         try {
		             FileReader fileReader = new FileReader(filepath);
		             json = (JSONObject) parser.parse(fileReader);
		             
		            /* System.out.println("json: " + json);*/

		         } catch (Exception ex) {
		             ex.printStackTrace();
		         }
		         return json;
		     }
}
