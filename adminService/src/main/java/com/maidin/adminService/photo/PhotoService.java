package com.maidin.adminService.photo;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
import com.maidin.adminApi.enums.DocumentType;
import com.maidin.adminApi.enums.ImageCategory;
import com.maidin.adminApi.exception.ExceptionCode;
import com.maidin.adminApi.exception.ExceptionResourceBundle;
import com.maidin.adminApi.filestore.impl.RetrieveFileResponse;
import com.maidin.adminApi.photo.PhotoMgmt;
import com.maidin.adminApi.photo.impl.PhotoFiles;
import com.maidin.adminApi.utils.ApiUtils;
import com.maidin.adminApi.utils.OperationId;
import com.maidin.adminService.utils.ServiceUtils;
import com.maidin.common.exception.ValidationException;
import com.maidin.common.util.CommonUtils;
import com.maidin.pojo.GenericResponse;
import com.maidin.pojo.filestore.StoreFile;
import com.maidin.pojo.photo.AddPhotoResponse;
import com.maidin.pojo.photo.ChangePrimaryPhoto;
import com.maidin.pojo.photo.ChangePrimaryPhotoResponse;
import com.maidin.pojo.photo.DeletePhotoResponse;
import com.maidin.pojo.photo.GetAllPhotosResponse;

@Controller
/*@RequestMapping("/admin/photo")
*/public class PhotoService {

	@Autowired
	private PhotoMgmt photoMgmt;

	@RequestMapping(value = "v2.0/photo/upload/imageType/{imageType}/image/{imageId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponse addPhoto(@RequestHeader("authToken") String authToken,
			@PathVariable(value = "imageType") int imageType, @PathVariable(value = "imageId") Long imageId,
			@RequestParam(value = "photo", required = true) MultipartFile photo,
			@RequestHeader(value = "accept-language", required = false) String language) {
		AddPhotoResponse response = new AddPhotoResponse();
		try {
			PhotoFiles photoFiles = getPhotoFilesObject(photo, imageType, language);

			response = photoMgmt.addPhoto(authToken, imageType, imageId, photoFiles, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	@RequestMapping(value = "admin/photo/upload/imageType/{imageType}/image/{imageId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponse uploadPhoto(@RequestHeader("authToken") String authToken,
			@PathVariable(value = "imageType") int imageType, @PathVariable(value = "imageId") Long imageId,
			@RequestParam(value = "photo", required = true) MultipartFile photo,
			@RequestHeader(value = "accept-language", required = false) String language) {
		AddPhotoResponse response = new AddPhotoResponse();
		try {
			PhotoFiles photoFiles = getPhotoFilesObject(photo, imageType, language);

			response = photoMgmt.uploadPhoto(authToken, imageType, imageId, photoFiles, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}


	@RequestMapping(value = "/admin/photo/retrieve/{imageType}/{itemId}", method = RequestMethod.GET)
	public @ResponseBody void getPersonalInfoFile(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			HttpServletResponse httpResponse) throws ValidationException {

		long fileStoreId = photoMgmt.getFileStoreId(imageType, itemId, "v1", ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrievePhoto(imageType, itemId, fileStoreId,
					ServiceUtils.getLocale(language));

			ByteSink byteSink = new ByteSink() {

				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}

			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}
	
	/*@RequestMapping(value = "/admin/photo/retrieve/V2/imageType/{imageType}/itemId/{itemId}", method = RequestMethod.GET)
	public @ResponseBody void retrieveV2Image(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			HttpServletResponse httpResponse) throws ValidationException {

		long fileStoreId = photoMgmt.getFileStoreId(imageType, itemId,"v2", ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrievePhotoV2(imageType, itemId, fileStoreId,
					ServiceUtils.getLocale(language));

			ByteSink byteSink = new ByteSink() {

				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}

			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}*/
	
	@RequestMapping(value = "/admin/photo/retrieve/photoId/{photoId}", method = RequestMethod.GET)
	public @ResponseBody void getPersonalInfoFile(
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("photoId") int photoId,
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreIdByPhotoId(photoId, ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrieveIndividualPhoto(photoId, fileStoreId,
					ServiceUtils.getLocale(language));

			ByteSink byteSink = new ByteSink() {

				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}

			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}

	@RequestMapping(value = "/admin/photo/delete/{imageType}/{itemId}", method = RequestMethod.PUT)
	public @ResponseBody DeletePhotoResponse deletePhotoInfo(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId) {
		DeletePhotoResponse response = new DeletePhotoResponse();
		try {
			response = photoMgmt.deletePhoto(authToken, imageType, itemId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponse(response, e);
		}
		return response;
	}
	
	@RequestMapping(value = "/admin/photo/delete/{photoId}", method = RequestMethod.PUT)
	public @ResponseBody DeletePhotoResponse deletePhotoByphotoId(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("photoId") long photoId) {
		DeletePhotoResponse response = new DeletePhotoResponse();
		try {
			response = photoMgmt.deletePhotoByphotoId(authToken, photoId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponse(response, e);
		}
		return response;
	}

	@RequestMapping(value = "/admin/photo/multipleupload/{productDetailId}", method = RequestMethod.POST)
	public @ResponseBody GenericResponse uploadMultipleImages(@RequestHeader("authToken") String authToken,
		    @PathVariable(value = "productDetailId") Long productDetailId,
			@RequestParam(value = "files", required = true) MultipartFile[] files,
			@RequestParam(value = "primaryPhoto", required = false) String primaryPhoto,
			@RequestHeader(value = "accept-language", required = false) String language) {
		AddPhotoResponse response = new AddPhotoResponse();
		try {
			//multiple photo of product Description
			List<PhotoFiles> photoFileList = new ArrayList<>();
			for (MultipartFile photoFile : files) {
				if (photoFile != null) {
					photoFileList.add(getPhotoFilesObject(photoFile, ImageCategory.PRODUCTDESCRIPTION.getId(), language));
				}
			}
			response = photoMgmt.addMultiplePhotos(authToken, productDetailId, photoFileList, primaryPhoto,
					ServiceUtils.getLocale(language));
			//single photo of productDetail
			Long photoId = photoMgmt.getPhotoIdOfproductDetailId(productDetailId);
			if(photoId == null || photoId == 0){
			PhotoFiles photoFile = getPhotoFilesObject(files[0], ImageCategory.PRODUCTDETAIL.getId(), language);
			response = photoMgmt.addPhoto(authToken, ImageCategory.PRODUCTDETAIL.getId(), productDetailId, photoFile, ServiceUtils.getLocale(language));
			}
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}
	
	/*@RequestMapping(value = "/admin/photo/retrieve/productId/{productId}/productDetailsId/{productDetailsId}", method = RequestMethod.GET)
	public @ResponseBody void getImage(
			@RequestHeader(value = "accept-language", required = false) String language,
		    @PathVariable("productId") long productId,
			@PathVariable("productDetailsId") long productDetailsId, 
			HttpServletResponse httpResponse) throws Exception {

		long fileStoreId = photoMgmt.getFileStoreIdOfProduct(ImageCategory.PRODUCT.getId(), productId,productDetailsId, ServiceUtils.getLocale(language));

		try {
			RetrieveFileResponse response = photoMgmt.retrievePhotoV2(ImageCategory.PRODUCT.getId(), productId, fileStoreId,
					ServiceUtils.getLocale(language));

			ByteSink byteSink = new ByteSink() {

				@Override
				public OutputStream openStream() throws IOException {
					return httpResponse.getOutputStream();
				}

			};

			httpResponse.setContentType(response.getContentType());
			httpResponse.setHeader(CommonUtils.CONTENT_DISPOSITION,
					CommonUtils.CONTENT_DISPOSITION_ATTACHMENT + response.getFileName());
			response.getByteSource().copyTo(byteSink);
		} catch (Exception e) {
			e.printStackTrace();
			httpResponse.setHeader("Exception", ExceptionResourceBundle
					.getExceptionMessage(ExceptionCode.FILE_DOWNLOAD_FAILED, ServiceUtils.getLocale(language)));
			httpResponse.setStatus(HttpServletResponse.SC_NO_CONTENT);
		}
	}*/
	
	@GetMapping(value = "/admin/productDescription/{productDetailId}")
	public @ResponseBody GetAllPhotosResponse getImageList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("productDetailId") long productDetailId,
			HttpServletResponse httpResponse) throws ValidationException {
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		try {
			response = photoMgmt.getImageList(authToken, productDetailId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
			return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}	
	
	
	@RequestMapping(value = "v2.0/changePrimaryPhoto/{productDetailId}", method = RequestMethod.POST)
	public @ResponseBody ChangePrimaryPhotoResponse changePrimaryPhotoInfo(@RequestHeader("authToken") String authToken, 
			@RequestHeader(value="accept-language", required=false) String language,
			@PathVariable("productDetailId") long productDetailId,
			@RequestBody ChangePrimaryPhoto request
			){
		ChangePrimaryPhotoResponse response = new ChangePrimaryPhotoResponse();
		try {
			response = photoMgmt.changePrimaryPhoto(authToken, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
//        	return ApiUtils.setErrorResponse(response, e);
        	return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}

	
	
	/*@RequestMapping(value = "/list/imageType/{imageType}/itemId/{itemId}", method = RequestMethod.GET)
	public @ResponseBody GetAllPhotosResponse getImageList(@RequestHeader("authToken") String authToken,
			@RequestHeader(value = "accept-language", required = false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			HttpServletResponse httpResponse) throws ValidationException {
		GetAllPhotosResponse response = new GetAllPhotosResponse();
		try {
			response = photoMgmt.getImageList(authToken, imageType,itemId, ServiceUtils.getLocale(language));
		} catch (Exception e) {
//        	return ApiUtils.setErrorResponse(response, e);
        	return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}	*/

	/*@RequestMapping(value = "/admin/photo/changePrimaryPhoto/imageType/{imageType}/itemId/{itemId}", method = RequestMethod.POST)
	public @ResponseBody ChangePrimaryPhotoResponse changePrimaryPhotoInfo(@RequestHeader("authToken") String authToken, 
			@RequestHeader(value="accept-language", required=false) String language,
			@PathVariable("imageType") int imageType, @PathVariable("itemId") long itemId,
			@RequestBody ChangePrimaryPhoto request
			){
		ChangePrimaryPhotoResponse response = new ChangePrimaryPhotoResponse();
		try {
			response = photoMgmt.changePrimaryPhoto(authToken,  imageType, itemId, request, ServiceUtils.getLocale(language));
		} catch (Exception e) {
//        	return ApiUtils.setErrorResponse(response, e);
        	return ApiUtils.setErrorResponseWithOperationId(response, e, OperationId.ADMIN_SERVICES);
		}
		return response;
	}*/

	public StoreFile toStoreFile(MultipartFile file, long fileSize, Locale locale) throws ValidationException {
		if (file != null) {
			String extension = FilenameUtils.getExtension(file.getOriginalFilename());
			StoreFile storeFile = new StoreFile();
			if (extension.equalsIgnoreCase("gif") || extension.equalsIgnoreCase("png")
					|| extension.equalsIgnoreCase("bmp") || extension.equalsIgnoreCase("jpeg")
					|| extension.equalsIgnoreCase("jpg") || extension.equalsIgnoreCase("webp")) {
				storeFile.setContentType(file.getContentType());
				storeFile.setDocumentType(DocumentType.PHOTO.getName());
				storeFile.setEncrypted(true);
				storeFile.setFileName(file.getOriginalFilename());
				storeFile.setFileSize(fileSize);
				// storeFile.setFileExtension(extension);
				return storeFile;
			} else {
				throw new ValidationException(
						ExceptionResourceBundle.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, locale));
			}

		}
		return null;
	}

	public File convert(MultipartFile file) throws IOException
	{    
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile(); 
	    FileOutputStream fos = new FileOutputStream(convFile); 
	    fos.write(file.getBytes());
	    fos.close(); 
	    return convFile;
	}
	
	private PhotoFiles getPhotoFilesObject(MultipartFile file, int imageType, String locale)
			throws ValidationException {
		// initialize ByteSource and StoreFile to avoid not initialized check
		// out of try catch block
		ByteSource byteSource = new ByteSource() {
			@Override
			public InputStream openStream() throws IOException {
				return null;
			}
		};

		StoreFile storeFile = new StoreFile();
		PhotoFiles photoFiles = new PhotoFiles(storeFile, byteSource);

		long compressesdFileSize = 0;
		try {
			if (file != null) {
				String imageCategory = ImageCategory.fromId(imageType).getName();

				// get byte array from MultipartFile
				byte[] bytes = file.getBytes();
				compressesdFileSize = bytes.length;
				System.out.println("Size before converting"+compressesdFileSize);
				InputStream ins = new ByteArrayInputStream(bytes);
				if(imageType == ImageCategory.PRODUCTDESCRIPTION.getId()  
						|| compressesdFileSize <= 30000 || imageType == ImageCategory.CATEGORY.getId() || imageType == ImageCategory.PAYMENTOPTION.getId()){
					byteSource = new ByteSource() {
						@Override
						public InputStream openStream() throws IOException {
							return ins;
						}
					};
				}
				else{
					BufferedImage oldImage = ImageIO.read(ins);
					// compress BufferedImage
					BufferedImage newImage = photoMgmt.resizeImageWithType(oldImage, imageCategory,compressesdFileSize );
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(newImage, "png", baos);
					InputStream is =  new ByteArrayInputStream(baos.toByteArray());
					/*if(baos.size() > bytes.length){
						byteSource = new ByteSource() {
							@Override
							public InputStream openStream() throws IOException {
								return ins;
							}
						};
					}
					else{
					*/byteSource = new ByteSource() {
						@Override
						public InputStream openStream() throws IOException {
							return is;
						}
					};
					//}
					
					}
								//if()
				/*float threshold = 0;
				if(imageType == ImageCategory.PRODUCTDESCRIPTION.getId() || 
						(imageType == ImageCategory.CATEGORY.getId())){
					threshold = 0.2f;
				}
				else{
					threshold = 0.5f;
				}*/
				//long threshold = 100000;
				//bImageFromConvert = compressImage(bImageFromConvert,threshold);
				// convert compressed BufferedImage to byte
				// ByteArrayOutputStream
				/*ByteArrayOutputStream oldBaos = new ByteArrayOutputStream();
				ByteArrayOutputStream newBaos = new ByteArrayOutputStream();
				 	ImageIO.write( oldImage, "png", oldBaos );
		            ImageIO.write( newImage, "png", newBaos );
		            oldBaos.flush();
		            newBaos.flush();
		            int olddataSize = oldBaos.toByteArray().length;
		            int newdataSize = newBaos.toByteArray().length;
		            byte[] olddata = oldBaos.toByteArray();
		            byte[] newdata = newBaos.toByteArray();
		            compressesdFileSize = newdataSize;
		           if(olddataSize > newdataSize) {
		        	   newdata = olddata;
		        	   compressesdFileSize = olddataSize;
		        	   InputStream is = new ByteArrayInputStream(olddata, 0, oldBaos.size());
						
						System.out.println("Size after converting"+compressesdFileSize);
						
						byteSource = new ByteSource() {
							@Override
							public InputStream openStream() throws IOException {
								return is;
							}
						};
		           }
		           else{
		        	   InputStream is = new ByteArrayInputStream(newdata, 0, newBaos.size());
						
						System.out.println("Size after converting"+compressesdFileSize);
						
						byteSource = new ByteSource() {
							@Override
							public InputStream openStream() throws IOException {
								return is;
							}
						};
		           }*/
				

				// get store file
				storeFile = toStoreFile(file, compressesdFileSize, ServiceUtils.getLocale(locale));

			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (storeFile != null && byteSource != null) {
			photoFiles = new PhotoFiles(storeFile, byteSource);
			return photoFiles;
		} else {
			throw new ValidationException(ExceptionResourceBundle
					.getExceptionCodeProperties(ExceptionCode.INVALID_IMAGE, ServiceUtils.getLocale(locale)));
		}
	}

	/*private BufferedImage compressImage(BufferedImage originalImage, MultipartFile file, long fileSize, long threshold) throws IOException {   
		float quality = 1.0f;
        Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");  
        ImageWriter writer = (ImageWriter)iter.next();  
        ImageWriteParam iwp = writer.getDefaultWriteParam();  
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
		IIOImage image = new IIOImage(originalImage, null, null);  
    float percent = 0.1f;   // 10% of 1  
    while (fileSize > threshold) {  
        if (percent >= quality) {  
            percent = percent * 0.1f;  
        }  
        quality -= percent;  
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos);
		//InputStream is = new ByteArrayInputStream(baos.toByteArray());
		writer.setOutput(ImageIO.createImageOutputStream(baos));
        iwp.setCompressionQuality(quality);  
        writer.write(null, image, iwp);  
        
       ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos1);
		//InputStream is2 = new ByteArrayInputStream(baos1.toByteArray());
		writer.setOutput(ImageIO.createImageOutputStream(baos1));
		writer.write(null, image, iwp); 
		
       int newFileSize = baos.toByteArray().length;  
        if (newFileSize == fileSize) {  
            // cannot reduce more, return  
           // break; 
        	return originalImage;
        } else {  
            fileSize = newFileSize;  
        }  
        System.out.println("quality = " + quality + ", new file size = " + fileSize);  
       // output.close();  
        baos.close();baos.flush();
       // baos1.close();baos1.flush();
    }  

    writer.dispose();
    return originalImage;
}*/
	
	private BufferedImage compressImage(BufferedImage originalImage, float compression) throws IOException {
		Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");
		ImageWriter writer = (ImageWriter) iter.next();

		ImageWriteParam iwp = writer.getDefaultWriteParam();
		iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);

		// reduced quality.
		iwp.setCompressionQuality(compression);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos);
		writer.setOutput(ImageIO.createImageOutputStream(baos));

		IIOImage image = new IIOImage(originalImage, null, null);
		writer.write(null, image, iwp);

		writer.dispose();
		return originalImage;
	}
	
/*	private BufferedImage compressImage(BufferedImage originalImage, long threshold) throws IOException {
		 float quality = 1.0f;  
		 Iterator iter = ImageIO.getImageWritersByFormatName("jpeg");  
	  
	        ImageWriter writer = (ImageWriter)iter.next();  
	  
	        ImageWriteParam iwp = writer.getDefaultWriteParam();  
	  
	        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);  
	  
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ImageIO.write(originalImage, "png", baos);
	        //InputStream ins = new ByteArrayInputStream(baos.toByteArray());
	       baos.flush();
	        //baos.flush();
	     //   IIOImage image = new IIOImage(originalImage, null, null);  
	  
	        float percent = 0.1f;   // 10% of 1  
	        long fileSize = baos.toByteArray().length;
	        if (fileSize <= threshold) {  
	            System.out.println("Image file size is under threshold");  
	            return originalImage;  
	        }
	        
	        while (fileSize > threshold) {  
	            if (percent >= quality) {  
	                percent = percent * 0.1f;  
	            }  
	  
	            quality -= percent;  
	  
	            File fileOut = new File(destImg);  
	            if (fileOut.exists()) {  
	                fileOut.delete();  
	            }  
	            //FileImageOutputStream output = new FileImageOutputStream(fileOut);  
	            baos = new ByteArrayOutputStream();
		        ImageIO.write(originalImage, "png", baos);
	            writer.setOutput(ImageIO.createImageOutputStream(baos));  
	            iwp.setCompressionQuality(quality);  
	            baos.flush();
	            writer.write(null, new IIOImage(originalImage, null, null), iwp);  
	            long newFileSize = baos.toByteArray().length;  
	            if (newFileSize <= fileSize) {  
	                // cannot reduce more, return  
	                break;  
	            } else {  
	                fileSize = newFileSize;  
	            }  
	            System.out.println("quality = " + quality + ", new file size = " + fileSize);  
	            // baos1.close(); // baos2.close(); 
	        }  
	        baos.close();  
	        
			return originalImage;
	}*/
}
