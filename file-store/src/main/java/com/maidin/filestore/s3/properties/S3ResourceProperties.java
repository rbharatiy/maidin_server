package com.maidin.filestore.s3.properties;

import java.io.File;

public class S3ResourceProperties {

	public static final String AWS_PROPERTY_FILE_LOCATION = System.getProperty("config.dir") + 
            File.separator + "s3.properties";
	public static final String AWS_S3_ACCESS_KEY = "accessKey";
	public static final String AWS_S3_SECRET_KEY = "secretKey";
	public static final String AWS_S3_FILESTORE_BUCKET = "bucketName";
	public static final String AWS_S3_FILESTORE_V2BUCKET = "V2bucketName";
	public static final String AWS_S3_BASE_URL = "baseUrl";
}
