package com.maidin.filestore.s3;


import java.io.File;
import java.util.Properties;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.maidin.common.util.CommonUtils;
import com.maidin.filestore.s3.properties.S3ResourceProperties;

public class S3CredentialsProvider implements AWSCredentialsProvider {

	private static final Properties prop = loadProperties();
		
	private static Properties loadProperties() {
		try {
			return CommonUtils.loadPropertyFile(new File(S3ResourceProperties.AWS_PROPERTY_FILE_LOCATION));
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException ("Unable to load the properties file: " + S3ResourceProperties.AWS_PROPERTY_FILE_LOCATION);
		}
	}

	@Override
	public AWSCredentials getCredentials() {
		final String accessKey = prop.getProperty(S3ResourceProperties.AWS_S3_ACCESS_KEY);
		final String secretKey = prop.getProperty(S3ResourceProperties.AWS_S3_SECRET_KEY);
		return new SimpleAWSCredentials(accessKey, secretKey);
	}

	public String getFileStoreBucket() {
		return prop.getProperty(S3ResourceProperties.AWS_S3_FILESTORE_BUCKET);
	}
	
	public String getFileStoreV2Bucket() {
		return prop.getProperty(S3ResourceProperties.AWS_S3_FILESTORE_V2BUCKET);
	}
	
	public String getEndPoint() {
		return prop.getProperty(S3ResourceProperties.AWS_S3_BASE_URL);
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		
	}

}

class SimpleAWSCredentials implements AWSCredentials  {

	private final String accessKey;
	private final String secretKey;

	public SimpleAWSCredentials(String accessKey, String secretKey) {
		this.accessKey = accessKey;
		this.secretKey = secretKey;
	}

	@Override
	public String getAWSAccessKeyId() {
		return accessKey;
	}

	@Override
	public String getAWSSecretKey() {
		return secretKey;
	}

}
