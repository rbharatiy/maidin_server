package com.maidin.filestore.s3;

import com.maidin.filestore.FileStore;
import com.maidin.filestore.FileStoreInstance;

public class S3FileStoreInstance implements FileStoreInstance{

	private final S3FileStore s3FileStore;
	
	public S3FileStoreInstance() {
		S3CredentialsProvider credentialProvider = new S3CredentialsProvider();
		s3FileStore = S3FileStore.builder()
			.withCredentialsProvider(credentialProvider)
			.inBucket(credentialProvider.getFileStoreBucket(), credentialProvider.getFileStoreV2Bucket())
			.withEndpoint(credentialProvider.getEndPoint())
			.build();
	}
	
	@Override
	public FileStore getInstance() {
		return s3FileStore;
	}
}
