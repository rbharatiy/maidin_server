package com.maidin.filestore.s3;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.retry.RetryPolicy;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;

public final class S3FileStoreBuilder {
	
	private AWSCredentialsProvider credentialsProvider;

	private String bucketName;
	
	private String V2bucketName;
	
	private String endpoint;
	
	private ClientConfiguration clientConfig;
	
	private int maxConnections = ClientConfiguration.DEFAULT_MAX_CONNECTIONS;
	
	private RetryPolicy maxRetries = ClientConfiguration.DEFAULT_RETRY_POLICY;
	
	private int socketTimeout = ClientConfiguration.DEFAULT_SOCKET_TIMEOUT;
	
	private int connectionTimeout = -1;
	
	private Protocol protocol = Protocol.HTTPS;
	
	public S3FileStoreBuilder withCredentialsProvider(AWSCredentialsProvider credentialsProvider) {
		this.credentialsProvider = checkNotNull(credentialsProvider);
		return this;
	}

	public S3FileStoreBuilder inBucket(String bucketName, String V2bucketName) {
		checkArgument(!bucketName.isEmpty(), "bucketName cannot be empty");
		this.bucketName = bucketName;
		this.V2bucketName = V2bucketName;
		return this;
	}
	
	public S3FileStoreBuilder withEndpoint(String endpoint) {
		this.endpoint = checkNotNull(endpoint);
		return this;
	}

	public S3FileStore build() {
		checkState(credentialsProvider != null, "No AWS credentials");
		checkState(bucketName != null, "No bucket name");
		ClientConfiguration config = getClientConfig();
		AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider, config);
		s3Client.setS3ClientOptions(new S3ClientOptions().withPathStyleAccess(true));
		s3Client.setEndpoint(endpoint);
		return new S3FileStore(s3Client, bucketName, V2bucketName);
	}
	
	private ClientConfiguration getClientConfig() {
		if (this.clientConfig != null) {
			return new ClientConfiguration(this.clientConfig);
		}
		ClientConfiguration config = new ClientConfiguration();
		config.setProtocol(protocol);
		config.setMaxConnections(maxConnections);
		config.setMaxErrorRetry(maxRetries.getMaxErrorRetry());
		config.setSocketTimeout(socketTimeout);
		if (connectionTimeout >= 0) {
			config.setConnectionTimeout(connectionTimeout);
		}
		return config;
	}
	
}
