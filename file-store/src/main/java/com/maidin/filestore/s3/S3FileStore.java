package com.maidin.filestore.s3;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.google.common.io.ByteSource;
import com.maidin.filestore.FileId;
import com.maidin.filestore.FileIdV2;
import com.maidin.filestore.FileStore;

public class S3FileStore implements FileStore {

	private final String bucketName;
	private final String V2bucketName;
	
	
	private final AmazonS3Client client;
	
	public S3FileStore(AmazonS3Client client, String bucketName, String V2bucketName) {
		this.client = checkNotNull(client);
		this.bucketName = checkNotNull(bucketName);
		this.V2bucketName = checkNotNull(V2bucketName);
	}
	
	@Override
	public void storeFile(FileId fileId, ByteSource input) throws Exception {
		checkNotNull(fileId);
		
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileId.getFileSize());
		String key = toKey(fileId);
		
		try (InputStream stream = input.openBufferedStream()) {
			client.putObject(bucketName, key, stream, metadata);
		}		
	}
	
	@Override
	public void storeFileV2(FileIdV2 fileId, ByteSource input) throws Exception {
		checkNotNull(fileId);
		
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(fileId.getFileSize());
		String key = toKey(fileId);
		
		try (InputStream stream = input.openBufferedStream()) {
			client.putObject(V2bucketName, key, stream, metadata);
		}		
	}

	@Override
	public ByteSource retrieveFile(FileId fileId) throws Exception {
		checkNotNull(fileId);
		String key = toKey(fileId);
		S3Object object = client.getObject(bucketName, key);
		ByteSource source = new ByteSource() {				
			@Override
			public InputStream openStream() throws IOException {
				return object.getObjectContent();
			}
		};
		
		return source;
	}
	
	@Override
	public ByteSource retrieveFileV2FromBucket2(FileIdV2 fileId) throws Exception {
		checkNotNull(fileId);
		String key = toKey(fileId);
		S3Object object = client.getObject(V2bucketName, key);
		ByteSource source = new ByteSource() {				
			@Override
			public InputStream openStream() throws IOException {
				return object.getObjectContent();
			}
		};
		
		return source;
	}
	
	@Override
	public void shutdown() {
		client.shutdown();		
	}
	
	private static String toKey(FileIdV2 fileId) {
		String key = fileId.getFolder() + FileId.separator + fileId.getFileName();
		return key;
	}
	
	private static String toKey(FileId fileId) {
		String key = fileId.getFolder() + FileId.separator + fileId.getFileName();
		return key;
	}
	
	
	public static S3FileStoreBuilder builder() {
		return new S3FileStoreBuilder();
	}

	@Override
	public void deleteFile(List<FileId> fileIds) throws Exception {
		for (FileId fileId: fileIds) {
			String key = toKey(fileId);
			client.deleteObject(bucketName, key);
		}
	}

	@Override
	public void deleteFileV2(List<FileIdV2> fileIds) {
		for (FileIdV2 fileId: fileIds) {
			String key = toKey(fileId);
			client.deleteObject(V2bucketName, key);
		}
	}

	@Override
	public ByteSource retrieveFileV2FromBucket1(FileIdV2 fileId) {
		checkNotNull(fileId);
		String key = toKey(fileId);
		S3Object object = client.getObject(bucketName, key);
		ByteSource source = new ByteSource() {				
			@Override
			public InputStream openStream() throws IOException {
				return object.getObjectContent();
			}
		};
		
		return source;
	}

	@Override
	public void deleteFileV2ForBucket1(List<FileIdV2> fileIds) {

		for (FileIdV2 fileId: fileIds) {
			String key = toKey(fileId);
			client.deleteObject(bucketName, key);
		}
	
		
	}

}
