package com.maidin.filestore;

import static com.google.common.base.Preconditions.checkNotNull;

public class FileId {

	private final int imageType;
	
	private final long imageId;

	private final String folder;

	private final String fileName;

	private final long fileSize;

	public final static String separator = "/";

	private FileId(int imageType, long imageId, String folder, String fileName, long fileSize) {
		this.imageType= imageType;
		this.imageId= imageId;
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = fileSize;
	}

	private FileId(int imageType, long imageId, String folder, String fileName) {
		this.imageType= imageType;
		this.imageId= imageId;
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = 0L;
	}

	public static FileId forUser(int imageType, long imageId, String folder, String fileName) {
		return new FileId(imageType, imageId, folder, fileName);
	}

	public static FileId forUser(int imageType, long imageId, String folder, String fileName, long fileSize) {
		return new FileId(imageType, imageId, folder, fileName, fileSize);
	}

	
	/**
	 * @return the imageType
	 */
	public int getImageType() {
		return imageType;
	}

	/**
	 * @return the imageId
	 */
	public long getImageId() {
		return imageId;
	}

	public String getFolder() {
		return folder;
	}

	public String getFileName() {
		return fileName;
	}

	public long getFileSize() {
		return fileSize;
	}
}
