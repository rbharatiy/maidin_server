package com.maidin.filestore;

import static com.google.common.base.Preconditions.checkNotNull;

public class FileIdV2 {

	
	private final int imageType;
	private final String folder;

	private final String fileName;

	private final long fileSize;

	public final static String separator = "/";

	private FileIdV2(int imageType, String folder, String fileName, long fileSize) {
		this.imageType= imageType;
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = fileSize;
	}

	private FileIdV2(int imageType,String folder, String fileName) {
		this.imageType= imageType;
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = 0L;
	}

	public FileIdV2(String folder, String fileName, long fileSize) {
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = fileSize;
		this.imageType = 0;
	}
	
	public FileIdV2(String folder, String fileName) {
		this.folder = checkNotNull(folder).toLowerCase();
		this.fileName = checkNotNull(fileName).toLowerCase();
		this.fileSize = 0;
		this.imageType = 0;
	}

	public static FileIdV2 forUser(int imageType,String folder, String fileName) {
		return new FileIdV2(imageType, folder, fileName);
	}

	public static FileIdV2 forUser( int imageType,String folder, String fileName, long fileSize) {
		return new FileIdV2(imageType, folder, fileName, fileSize);
	}

	public static FileIdV2 forUser(String folder, String fileName, long fileSize) {
		return new FileIdV2(folder, fileName, fileSize);
	}
	public static FileIdV2 forUser(String folder, String fileName) {
		return new FileIdV2(folder, fileName);
	}
	
	public int getImageType() {
		return imageType;
	}

	public static String getSeparator() {
		return separator;
	}

	public String getFolder() {
		return folder;
	}

	public String getFileName() {
		return fileName;
	}

	public long getFileSize() {
		return fileSize;
	}

	
}
