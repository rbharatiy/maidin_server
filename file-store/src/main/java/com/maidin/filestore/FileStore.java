package com.maidin.filestore;

import java.util.List;

import com.google.common.io.ByteSource;

public interface FileStore {
	
	public void storeFile(FileId fileId, ByteSource input) throws Exception;

	public ByteSource retrieveFile(FileId fileId) throws Exception;
	
	public void deleteFile(List<FileId> fileIds) throws Exception;
	
	public void shutdown();

	void storeFileV2(FileIdV2 fileId, ByteSource input) throws Exception;

	ByteSource retrieveFileV2FromBucket2(FileIdV2 fileId) throws Exception;

	public void deleteFileV2(List<FileIdV2> fileIds);
	public void deleteFileV2ForBucket1(List<FileIdV2> fileIds);

	public ByteSource retrieveFileV2FromBucket1(FileIdV2 fileId);
}
