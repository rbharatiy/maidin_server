package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.PhotoDTO;

public interface PhotoDAO {
	
	public Long addPhoto(PhotoDTO photosDTO);
    
    public void updatePhoto(PhotoDTO photosDTO);
    
   	public PhotoDTO getPhotoByPhotoTypeAndPhotoId(int imageType, long imageId);

	public List<PhotoDTO> getListByPhotoTypePhotoIdAndVersion(int id, Long imageId, String version);

	public PhotoDTO getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto(int id, Long imageId, String version, boolean isPrimaryPhoto);

	public PhotoDTO getById(long photoId);

	public PhotoDTO getByPhotoTypePhotoIdAndVersion(int imageType, long itemId, String string);

	public List<PhotoDTO> getListByPhotoTypePhotoIdVersionAndProductDetailIds(int imageType, long imageId,
			String string);

	public List<PhotoDTO> getByIds(List<Long> photoIds);

	public PhotoDTO getByFileStoreId(long fileStoreId);

	
}
