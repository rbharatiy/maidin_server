package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;

public interface SubCategoryInfoV2DAO {

	public List<SubCategoryInfoV2DTO> getAllSubCategories(List<Integer> subCategoryIdList);

	public SubCategoryInfoV2DTO getById(int subCategoryId);

	public void updateSubCategoryInfo(SubCategoryInfoV2DTO brandDTO);

	public SubCategoryInfoV2DTO getByIdWithoutActiveStatus(int subCategoryId);

	public List<Integer> getByIds(List<Integer> subCategoryIdList);

	public List<Integer> getByIdsWithoutActiveState(List<Integer> subCategoryIdList);

	public List<SubCategoryInfoV2DTO> getAllSubCategoriesWithoutActiveState(List<Integer> subCategoryIdList);

	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active);

	public List<SubCategoryInfoV2DTO> getAll(boolean b);

	public List<SubCategoryInfoV2DTO> getAllSubCategoriesWithoutActiveState();

	//public List<Integer> getByIds(List<Integer> subCategoryIdList, boolean active);
	}
