package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.CustomerDTO;

public interface CustomerDAO {

	public CustomerDTO getByMobileNumberWithoutActiveState(String mobileNumber);

	public CustomerDTO getCustomerByMobileNumber(String mobileNumber);

	public Long addCustomerInfo(CustomerDTO customerDTO);

	public CustomerDTO getCustomerByEmail(String email);

	public CustomerDTO getByIdAndMobileNumber(Long customerId, String mobileNumber);

	//public CustomerDTO getByCityProjectTowerFlatAndMobile(String city, String project, String tower, String flat,String mobile);

	public CustomerDTO getById(Long customerId);

	//public void addCustomerInfo(CustomerDTO2 customerDTO);

	public List<Long> getByAddressId(Long addressId);

	public long getUsersByFlatCount(boolean active);

	public long getRegisteredUserCount(boolean active);

	public List<CustomerDTO> getActiveUsersList(boolean b);

	List<Object[]> getNewCustomerRegistrations(int noOfdaysFromCurrent);

}
