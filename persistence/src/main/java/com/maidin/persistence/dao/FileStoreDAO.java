package com.maidin.persistence.dao;

import com.maidin.persistence.dto.FileStoreDTO;

public interface FileStoreDAO {
	
	public FileStoreDTO getFileById(long id);

	public long addFileStore(FileStoreDTO dto);

	public void updateFileStore(FileStoreDTO dto);
	
	public void deleteFileStore(FileStoreDTO dto);

	public FileStoreDTO getByMD5(String md5);

	public FileStoreDTO getFileByFileName(String fileName);

}
