package com.maidin.persistence.dao.V2;

import java.util.List;
import java.util.Set;

import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;

public interface BrandInfoV2DAO {

	public List<BrandInfoV2DTO> getByBrandIds(List<Integer> brandIdList);

	public BrandInfoV2DTO getByBrandId(Integer brandId);

	public Integer addBrandInfo(BrandInfoV2DTO brandInfoV2DTO);

	public BrandInfoV2DTO getByBrandName(String brandName);

	public void updateBrandInfo(BrandInfoV2DTO brandInfoV2DTO);

	public List<BrandInfoV2DTO> getAll();

	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean b);

	//public List<BrandInfoV2DTO> getByBrandIdsWithoutActiveState(Set<Integer> brandIdList);

	//public void updateBrand(BrandInfoV2DTO brandDTO);

	/*public BrandInfoV2DTO getById(Integer brandId);*/

}
