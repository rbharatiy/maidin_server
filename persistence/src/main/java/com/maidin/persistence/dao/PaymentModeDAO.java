package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.PaymentModeDTO;

public interface PaymentModeDAO {

	public PaymentModeDTO getById(Integer id);

	public PaymentModeDTO getByPaymentMode(String paymentMode);

	//public PaymentModeDTO getByPaymemtId();
	
	public List<PaymentModeDTO> getAll();

	public List<PaymentModeDTO> getAllPaymentMode(boolean displayedInApp, boolean active);

	
	
	    
		
}
