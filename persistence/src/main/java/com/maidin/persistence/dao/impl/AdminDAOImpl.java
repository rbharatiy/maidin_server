package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.AdminDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CartDTO;

public class AdminDAOImpl extends CommonDAO implements AdminDAO {

	@Override
	public AdminDTO getByMobileNumber(String mobileNumber) {
		String namedQuery = "Admin.getByMobileNumber";
        return this.getQuery(namedQuery, AdminDTO.class, new Param("mobileNumber", mobileNumber));
	}

	@Override
	public AdminDTO getByEmail(String email) {
		String namedQuery = "Admin.getByEmail";
        return this.getQuery(namedQuery, AdminDTO.class, new Param("email", email));
	}

	@Override
	public AdminDTO getByIdAndMobileNumber(Long adminId, String mobileNumber) {
		String namedQuery = "Admin.getByIdAndMobileNumber";
        return this.getQuery(namedQuery, AdminDTO.class, new Param("Id", adminId),
        												 new Param("mobileNumber", mobileNumber));
	}

	@Override
	public AdminDTO getByMobileNumberWithoutActiveState(String mobileNumber) {
		String namedQuery = "Admin.getByMobileNumberWithoutActiveState";
        return this.getQuery(namedQuery, AdminDTO.class, new Param("mobileNumber", mobileNumber));
	}

}
