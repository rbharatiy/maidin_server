package com.maidin.persistence.dao.impl.V2;

import java.util.List;
import java.util.Set;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dao.V2.UpdateInfoDAO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.UpdateInfoDTO;

public class UpdateInfoDAOImpl extends CommonDAO implements UpdateInfoDAO{

	@Override
	public UpdateInfoDTO getByText(String text) {
		String namedQuery="UpdateInfo.getByText";
		return this.getQuery(namedQuery, UpdateInfoDTO.class, new Param("parameterText",text));
	}


	/*@Override
	public List<BrandInfoV2DTO> getByBrandIds(Set<Integer> brandIds) {
		String namedQuery="BrandInfoV2.getByBrandIds";
		return this.list(namedQuery, BrandInfoV2DTO.class, new Param("Ids",brandIds));
	}

	@Override
	public BrandInfoV2DTO getByBrandId(Integer brandId) {
		String namedQuery="BrandInfoV2.getById";
		return this.getQuery(namedQuery, BrandInfoV2DTO.class, new Param("Id",brandId));
	}*/
	}
