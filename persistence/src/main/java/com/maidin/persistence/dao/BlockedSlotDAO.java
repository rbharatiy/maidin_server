package com.maidin.persistence.dao;

import java.sql.Date;
import java.util.List;

import com.maidin.persistence.dto.BlockedSlotDTO;;

public interface BlockedSlotDAO {
	public List<BlockedSlotDTO> getByBlockedDate(Date blockedDate);

}
