package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.RecurringTypeDAO;
import com.maidin.persistence.dto.RecurringTypeDTO;

public class RecurringTypeDAOImpl extends CommonDAO implements RecurringTypeDAO {

	public RecurringTypeDTO getById(Long Id){
		String namedQuery = "RecurringType.getById";
        return this.getQuery(namedQuery, RecurringTypeDTO.class, new Param("Id", Id));
    }

	@Override
	public RecurringTypeDTO getByRecurringType(String recurringType) {
		String namedQuery = "RecurringType.getByRecurringType";
		return this.getQuery(namedQuery, RecurringTypeDTO.class, new Param("recurringType", recurringType));
    }

	
	
	    
		
}
