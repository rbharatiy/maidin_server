package com.maidin.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.MaidinMoneyLedgerDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.MaidinMoneyLedgerDTO;
import com.maidin.persistence.dto.OrderInfoDTO;

public class MaidinMoneyLedgerDAOImpl extends CommonDAO implements MaidinMoneyLedgerDAO {

	@Override
	public Long addEntryInLedger(MaidinMoneyLedgerDTO maidinMoneyLedgerDTO) {
		// TODO Auto-generated method stub
		return (Long) this.add(maidinMoneyLedgerDTO);
	}

	@Override
	public void updateBalance(MaidinMoneyLedgerDTO maidinMoneyLedgerDTO) {
		this.update(maidinMoneyLedgerDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public MaidinMoneyLedgerDTO getByDate(Date date) {
		String namedQuery = "MaidinMoneyLedger.getByDate";
		return this.getQuery(namedQuery, MaidinMoneyLedgerDTO.class, new Param("date", date));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public MaidinMoneyLedgerDTO getByPreviousDay() {
		String namedQuery = "MaidinMoneyLedger.getByPreviousDay";
		return this.getQuery(namedQuery, MaidinMoneyLedgerDTO.class);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<MaidinMoneyLedgerDTO> getBetweenDates(Date fromDate, Date toDate) {
		String namedQuery = "MaidinMoneyLedger.getBetweenDates";
		return this.getlist(namedQuery, MaidinMoneyLedgerDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate));
	}

}
