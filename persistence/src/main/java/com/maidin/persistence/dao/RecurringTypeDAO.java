package com.maidin.persistence.dao;

import com.maidin.persistence.dto.RecurringTypeDTO;

public interface RecurringTypeDAO {

	public RecurringTypeDTO getById(Long id);

	public RecurringTypeDTO getByRecurringType(String recurringType);

	
	
	    
		
}
