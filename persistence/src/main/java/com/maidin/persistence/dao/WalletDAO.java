package com.maidin.persistence.dao;


import com.maidin.persistence.dto.WalletDTO;

public interface WalletDAO {

	public Long addBalance(WalletDTO walletDTO);

	public void updateBalance(WalletDTO walletDTO);

	public WalletDTO getByCustomerId(Long customerId);

	double getTotalMadinMoney();
	
	double getPositiveMadinMoney();
	
	double getNegativeMadinMoney();

	
	
	    
		
}
