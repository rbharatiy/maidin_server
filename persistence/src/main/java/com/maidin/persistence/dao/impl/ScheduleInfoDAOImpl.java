package com.maidin.persistence.dao.impl;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.ScheduleInfoDAO;
import com.maidin.persistence.dto.ScheduleInfoDTO;



public class ScheduleInfoDAOImpl extends CommonDAO implements ScheduleInfoDAO {

	@Override
	public Long addScheduleInfo(ScheduleInfoDTO scheduleDTO) {
	return (Long)this.add(scheduleDTO);
	}

	@Override
	public ScheduleInfoDTO getById(long scheduleId) {
		String namedQuery = "ScheduleInfo.getById";
		return this.getQuery(namedQuery,ScheduleInfoDTO.class, new Param("Id",scheduleId));
	}

	@Override
	public void updateScheduleInfo(ScheduleInfoDTO scheduleDTO) {
		this.update(scheduleDTO);
		
	}

	@Override
	public List<ScheduleInfoDTO> getByCustomerId(Long customerId) {
		String namedQuery = "ScheduleInfo.getByCustomerId";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("customerId",customerId));
	}

	@Override
	public void clearAllSchedule(ScheduleInfoDTO scheduleDTO) {
		this.delete(scheduleDTO);
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfSunday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfSunday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Su",true),
															new Param("scheduleStatus", scheduleStatus));
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfMonday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfMonday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Mo",true),
															new Param("scheduleStatus", scheduleStatus));
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfTuesday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfTuesday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Tu",true),
															new Param("scheduleStatus", scheduleStatus));
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfWednesday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfWednesday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("We",true),
															new Param("scheduleStatus", scheduleStatus));
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfThursday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfThursday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Th",true),
															new Param("scheduleStatus", scheduleStatus));
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfFriday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfFriday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Fr",true),
															new Param("scheduleStatus", scheduleStatus));
		
	}

	@Override
	public List<ScheduleInfoDTO> getSchedulesOfSaturday(int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getSchedulesOfSaturday";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("Sa",true),
															new Param("scheduleStatus", scheduleStatus));
		
		
	}

	@Override
	public List<ScheduleInfoDTO> getByCustomerIdAndScheduleStatus(Long customerId, int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getByCustomerIdAndScheduleStatus";
		return this.list(namedQuery, ScheduleInfoDTO.class, new Param("customerId",customerId),
															new Param("scheduleStatus", scheduleStatus));
	}

	@Override
	public List<ScheduleInfoDTO> getByIds(List<Long> Ids) {
		String namedQuery = "ScheduleInfo.getByIds";
		return this.list(namedQuery,ScheduleInfoDTO.class, new Param("Ids", Ids));

	}

	@Override
	public List<ScheduleInfoDTO> getRestSlotsSameDayScheduleOrder(Long customerId,
			int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getTodaysRestSchedule";
		return this.list(namedQuery,ScheduleInfoDTO.class, new Param("customerId", customerId), new Param("scheduleStatus", scheduleStatus));


	}

	@Override
	public List<ScheduleInfoDTO> getNextScheduleDate(Long customerId, int scheduleStatus) {
		String namedQuery = "ScheduleInfo.getNextScheduleDate";
		return this.list(namedQuery,ScheduleInfoDTO.class, new Param("customerId", customerId), new Param("scheduleStatus", scheduleStatus));
	}

	@Override
	public List<Integer> getTimeSlotsByIds(List<Long> scheduleIdList) {
		String namedQuery = "ScheduleInfo.getTimeSlotsByIds";
		return this.getColumnslist(namedQuery,ScheduleInfoDTO.class,-1, new Param("Ids", scheduleIdList));
	}

	@Override
	public List<ScheduleInfoDTO> getByCustomerIdAndScheduleStatusAndTimeSlotId(Long customerId, int scheduleStatus,
			int timeSlotId) {
		String namedQuery = "ScheduleInfo.getByCustomerIdScheduleStatusAndTimeSlotId";
		return this.list(namedQuery,ScheduleInfoDTO.class, new Param("customerId", customerId), new Param("scheduleStatus", scheduleStatus),
				new Param("timeSlotId", timeSlotId));
	}

	@Override
	public ScheduleInfoDTO getByCustomerIdProductIdAndTimeSlot(Long customerId, Integer productId , Integer timeSlotId) {
		String namedQuery = "ScheduleInfo.getByCustomerIdProductIdAndTimeSlotId";
		return this.getQuery(namedQuery,ScheduleInfoDTO.class, new Param("customerId", customerId), new Param("productId", productId),
				new Param("timeSlotId", timeSlotId));
	}

	}
