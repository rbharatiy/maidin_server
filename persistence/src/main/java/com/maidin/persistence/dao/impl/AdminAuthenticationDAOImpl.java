package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.AdminAuthenticationDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CustomerAuthenticationDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminAuthenticationDTO;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;

public class AdminAuthenticationDAOImpl extends CommonDAO implements AdminAuthenticationDAO {

	@Override
	public Long addToken(AdminAuthenticationDTO adminAuthenticationDTO) {
		// TODO Auto-generated method stub
		return (Long)this.add(adminAuthenticationDTO);
	}

	@Override
	public void updateToken(AdminAuthenticationDTO adminAuthenticationDTO) {
		this.update(adminAuthenticationDTO);
		
	}

	@Override
	public AdminAuthenticationDTO getByMobileNumber(String mobileNumber) {
		String namedQuery = "AdminAuthentication.getByMobileNumber";
        return this.getQuery(namedQuery, AdminAuthenticationDTO.class, new Param("mobileNumber", mobileNumber));
	}

}
