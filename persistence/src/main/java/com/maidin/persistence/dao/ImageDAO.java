package com.maidin.persistence.dao;

import com.maidin.persistence.dto.ImageDTO;

public interface ImageDAO {
	
	    public Long addFile(ImageDTO driverDTO);
	    
	    public void updateFile(ImageDTO driverDTO);
	   
	    public void deleteFile(ImageDTO driverAuthenticationDTO);

		public ImageDTO getByProductId(String string, Long productId);
}

	