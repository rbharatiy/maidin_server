package com.maidin.persistence.dao.impl.V2;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.BrandInfoV2DAO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;

public class BrandInfoV2DAOImpl extends CommonDAO implements BrandInfoV2DAO{

	@Override
	public List<BrandInfoV2DTO> getByBrandIds(List<Integer> brandIds) {
		String namedQuery="BrandInfoV2.getByBrandIds";
		return this.list(namedQuery, BrandInfoV2DTO.class, new Param("Ids",brandIds));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public BrandInfoV2DTO getByBrandId(Integer brandId) {
		String namedQuery="BrandInfoV2.getById";
		return this.getQuery(namedQuery, BrandInfoV2DTO.class, new Param("Id",brandId));
	}

	@Override
	public Integer addBrandInfo(BrandInfoV2DTO brandInfoV2DTO) {
		return (Integer)this.add(brandInfoV2DTO);
	}

	@Override
	public BrandInfoV2DTO getByBrandName(String brandName) {
		String namedQuery="BrandInfoV2.getByBrandName";
		return this.getQuery(namedQuery, BrandInfoV2DTO.class, new Param("brand",brandName));
	}

	@Override
	public void updateBrandInfo(BrandInfoV2DTO brandInfoV2DTO) {
		 this.update(brandInfoV2DTO);
		
	}

	@Override
	public List<BrandInfoV2DTO> getAll() {
		String namedQuery = "BrandInfoV2.getAll";
		return this.list(namedQuery, BrandInfoV2DTO.class);
	}

	@Override
	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active) {
		String namedQuery = "BrandInfoV2.getByTextWithActivationStatus";
		return this.getColumnslist(namedQuery, BrandInfoV2DTO.class,-1, new Param("productSubString", "%" + productSubString + "%"),
				new Param("active",active));
	}

	/*@Override
	public BrandInfoV2DTO getById(Integer brandId) {
		String namedQuery = "BrandInfoV2.getById";
		return this.getQuery(namedQuery, BrandInfoV2DTO.class, new Param("Id",brandId));
	}*/
	}
