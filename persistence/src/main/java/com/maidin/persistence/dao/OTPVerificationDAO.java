package com.maidin.persistence.dao;


import com.maidin.persistence.dto.OTPVerificationDTO;

public interface OTPVerificationDAO {
	
	 public void saveOTPInfo(OTPVerificationDTO oTPVerificationDTO);
	    
	 public OTPVerificationDTO getByMobile(String mobileNumber, String usercategory);

	public OTPVerificationDTO getByMobileAndOtp(String mobileNumber, String otpCode, String usercategory);

	public void deleteInfo(OTPVerificationDTO otpdto);

	public void updateOTPInfo(OTPVerificationDTO dto);

}
