package com.maidin.persistence.dao;


import com.maidin.persistence.dto.OrderPaymentDTO;

public interface OrderPaymentDAO {

	public Long addPaymentInfo(OrderPaymentDTO orderPaymentDAO);

	public void deletePaymentInfo(OrderPaymentDTO orderPaymentDAO);

	public OrderPaymentDTO  getById(Long Id);
	
	    
		
}
