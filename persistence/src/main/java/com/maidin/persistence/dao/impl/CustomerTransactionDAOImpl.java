package com.maidin.persistence.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.NamedQuery;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;
 
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CustomerTransactionDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminTransactionsDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;
import com.maidin.persistence.dto.OrderInfoDTO;

public class CustomerTransactionDAOImpl extends CommonDAO implements CustomerTransactionDAO {

	@Override
	public CustomerTransactionDTO getByCustomerId(Long customerId) {
		String namedQuery = "CustomerTransaction.getByCustomerId";
		return this.getQuery(namedQuery, CustomerTransactionDTO.class, new Param("customerId", customerId));
	}

	@Override
	public void addTransaction(CustomerTransactionDTO transactionDTO) {
		this.add(transactionDTO);

	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<CustomerTransactionDTO> getByOrderDetailsId(Long orderDetailsId) {
		String namedQuery = "CustomerTransaction.getByOrderDetailsId";
		return this.getlist(namedQuery, CustomerTransactionDTO.class, -1, new Param("orderDetailsId", orderDetailsId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Integer> getPaymentModeByOrderDetailsId(Long orderDetailsId) {
		String namedQuery = "CustomerTransaction.getPaymentModeByOrderDetailsId";
		return this.getColumnslist(namedQuery, CustomerTransactionDTO.class, -1,
				new Param("orderDetailsId", orderDetailsId));
	}

	@Override
	public List<CustomerTransactionDTO> getByOrderDetailsIdAndPaymentMode(Long orderDetailsId, int paymentMode) {
		String namedQuery = "CustomerTransaction.getByOrderDetailsIdAndPaymentMode";
		return this.getlist(namedQuery, CustomerTransactionDTO.class, -1, new Param("orderDetailsId", orderDetailsId),
				new Param("paymentModeId", paymentMode));
	}

	@Override
	public List<AdminTransactionsDTO> getCustomerTransactions(Long customerId, int firstResult, int maxResult) {
		/*String namedQuery = "SELECT ct.id, "
				+ "ct.TransactionDescription, p.productName, p.quantityUnit, odi.Quantity, odi.productSalePrice+odi.deliveryCharges, "
				+ "odi.totalProductCost, ct.MoneyIn, ct.MoneyOut, odi.OrderStatus, "
				+ "pm.PaymentMode, a.FullName, CAST(1000 * UNIX_TIMESTAMP(ct.TransactionDate) AS UNSIGNED INTEGER), "
				+ "ct.DueAmount FROM CustomerTransaction ct "
				+ "INNER JOIN OrderDetailsInfo odi ON ct.orderDetailsId = odi.Id "
				+ "INNER JOIN Product p ON odi.ProductId = p.Id "
				+ "INNER JOIN PaymentMode pm ON ct.PaymentModeId = pm.Id "
				+ "INNER JOIN Admin a ON ct.UpdatedBy = a.Id WHERE ct.CustomerId=:customerId ORDER BY ct.id desc "
				+ "LIMIT :maxResult ";*/
		String namedQuery = "SELECT ct.id, "
				+ "ct.TransactionDescription, p.productName, pc.quantityUnit, odi.Quantity, odi.productSalePrice+odi.deliveryCharges, "
				+ "odi.totalProductCost, ct.MoneyIn, ct.MoneyOut, odi.OrderStatus, "
				+ "pm.PaymentMode, a.FullName, CAST(1000 * UNIX_TIMESTAMP(ct.TransactionDate) AS UNSIGNED INTEGER), "
				+ "ct.DueAmount,  CAST(1000 * UNIX_TIMESTAMP(oi.OrderedTime) AS UNSIGNED INTEGER), oi.TimeSlotId FROM CustomerTransaction ct "
				+ "INNER JOIN OrderDetailsInfo odi ON ct.orderDetailsId = odi.Id "
				+ "INNER JOIN OrderInfo oi ON odi.OrderId = oi.Id  "
				+ "INNER JOIN ProductCityInfoV2 pc ON odi.ProductId = pc.Id "
				+ "INNER JOIN ProductInfoV2 p ON p.Id = pc.ProductId "
				+ "INNER JOIN PaymentMode pm ON ct.PaymentModeId = pm.Id "
				+ "INNER JOIN Admin a ON ct.UpdatedBy = a.Id WHERE ct.CustomerId=:customerId ORDER BY ct.id desc "
				+ "LIMIT :maxResult ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		// query.addEntity(CustomerTransactionDTO.class);
		query.setParameter("customerId", customerId);
		// query.setParameter("firstResult", firstResult);
		query.setParameter("maxResult", maxResult);
		List<Object[]> list = query.list();
		List<AdminTransactionsDTO> adminTransactionsDTOs = new ArrayList<>();
		list.stream().forEach((record) -> {
			AdminTransactionsDTO transactionDetails = new AdminTransactionsDTO();

			transactionDetails.setCustomerTransactionId(Long.parseLong(record[0].toString()));
			transactionDetails.setTransactionDescription(Integer.valueOf(record[1].toString()));
			transactionDetails.setProductName(record[2] == null ? null:String.valueOf(record[2].toString()));
			transactionDetails.setQuantityUnit(record[3] == null ?null: String.valueOf(record[3].toString()));
			transactionDetails.setQuantity(Integer.parseInt(record[4].toString()));
			transactionDetails.setPrice(Float.parseFloat(record[5].toString()));
			transactionDetails.setTotalCost(Float.parseFloat(record[6].toString()));
			transactionDetails.setMoneyIn(Float.parseFloat(record[7].toString()));
			transactionDetails.setMoneyOut(Float.parseFloat(record[8].toString()));

			if (record[9] != null) {
				transactionDetails.setOrderStatus(String.valueOf(record[9].toString()));
			} else {
				transactionDetails.setOrderStatus(null);
			}

			transactionDetails.setPaymentMode(record[10] == null? "":String.valueOf(record[10].toString()));
			transactionDetails.setAdminName(record[11]== null?"":String.valueOf(record[11].toString()));
			transactionDetails.setTransactionDate(Long.parseLong(record[12].toString()));
			transactionDetails.setDueAmount(Float.parseFloat(record[13].toString()));
			transactionDetails.setOrderedTime(record[14] == null? 0 :Long.parseLong(record[14].toString()));
			transactionDetails.setTimeSlotId(record[15] == null?0:Integer.parseInt(record[15].toString()));
			
			adminTransactionsDTOs.add(transactionDetails);
		});
		System.out.println(adminTransactionsDTOs);
		return adminTransactionsDTOs;
	}

	@Override
	public long getCustomerTransactionCount(Long customerId) {
		String namedQuery = "CustomerTransaction.getCustomerTransactionRecordsCount";
		return (Long) this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
				.setParameter("customerId", customerId).uniqueResult();
	}
	
	@Override
	public long getMoneyCountAllTransactions(Long customerId) {

		String namedQuery="(SELECT ct.Id "
				+ "From CustomerTransaction ct "
				+ "INNER JOIN OrderInfo oi ON oi.id = ct.OrderId "
				+ "INNER JOIN OrderDetailsInfo odi On odi.id = ct.orderDetailsId "
				+ "Where ct.customerId=:customerId and ct.moneyOut > 0 "
				+ "and oi.paymentModeId in (2,4,5,7) "
				+ "and ct.remarks != 1 "
				+ "and odi.Cancelled = false "
				+ "group by ct.orderId "
				+ "having sum(Case when ct.PaymentModeId = 2 then 1 else 0 end ) > 0 )"
				+ "Union all "
				+ "(SELECT cti.Id "
				+ "From CustomerTransaction cti "
				+ "Where cti.customerId=:customerId and cti.moneyIn > 0 "
				+ "and cti.TransactionDescription in (1,11) "
				+ "group by cti.orderId)";
		
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		return (long) (query.list().size());
	}
	
	@Override
	@Transactional(rollbackFor =Exception.class, readOnly = true)
	public List<CustomerTransactionDTO> getAllOrderTransaction() {
		String namedQuery = "CustomerTransaction.getAllOrderTransaction";
		return this.getlist(namedQuery, CustomerTransactionDTO.class, -1);
	}
	
	@Override
	public List<Object[]> getMoneyAllTransactions(Long customerId, int firstResult, int maxResult) {
		System.out.println("----------  getMoneyAllTransactions   -------------");
		String namedQuery="(SELECT ct.Id, "
				+ "ct.orderId, "
				+ "sum(ct.moneyOut) as moneyOut, "
				+ "ct.moneyIn as moneyIn, 1000 * unix_timestamp(ct.TransactionDate) as TransactionDate, "
//				+ "(Case when oi.PaymentModeId = 2 Or ct.PaymentModeId = 4 "
//			    + "then 'true' else 'false' end ) as isFullyPaidByWallet, "
				
				+ "Case when sum(Case when ct.PaymentModeId = 2 "
				+ "then 1 else 0 end ) = count(ct.PaymentModeId) then 'Fully paid' else 'Partially paid' end "
				+ "as isFullyPaidByWallet, "
				
				+ "oi.paymentModeId, pm.ByPaymentMode "
				+ "From CustomerTransaction ct "
				+ "INNER JOIN OrderInfo oi ON oi.id = ct.OrderId "
				+ "INNER JOIN PaymentMode pm ON pm.id = oi.PaymentModeId "
				+ "INNER JOIN OrderDetailsInfo odi On odi.id = ct.orderDetailsId "
				+ "Where ct.customerId=:customerId and ct.moneyOut > 0 "
				+ "and oi.paymentModeId in (2,4,5,7) "
				+ "and ct.remarks != 1 "
				+ "and odi.Cancelled = false "
				+ "group by ct.orderId "
				+ "having sum(Case when ct.PaymentModeId = 2 then 1 else 0 end ) > 0 )"
				+ "Union all "
				+ "(SELECT cti.Id, cti.orderId, cti.moneyOut, cti.moneyIn, "
				+ "1000 * unix_timestamp(cti.TransactionDate) as TransactionDate, 'false', "
//				+ "cti.paymentModeId, "
				+ "1, "
				+ "Case when cti.moneyIn > 0 "
//              + "and cti.OrderId > 0 then concat('Refund | ID : ',cti.OrderId) else pmo.ByPaymentMode end as ByPaymentMode "
              + "and cti.OrderId > 0 then concat('Refund | ID : ',cti.OrderId) else 'By Cash' end as ByPaymentMode "
				+ "From CustomerTransaction cti "
				+ "INNER JOIN PaymentMode pmo ON pmo.id = cti.paymentModeId "
				+ "Where cti.customerId=:customerId and cti.moneyIn > 0 "
				+ "and cti.TransactionDescription in (1,11) "
				+ "and cti.orderId ) "
				+ "order by TransactionDate desc "
				+ "LIMIT :firstResult,:maxResult ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		query.setParameter("firstResult", firstResult);
		query.setParameter("maxResult", maxResult);
		List<Object[]> list = query.list();
		return list;
		
		
	}
	
	@Override
	public List<Object[]> getMoneyOutTransactions(Long customerId, int firstResult, int maxResult) {
		System.out.println("----------  getMoneyOutTransactions   -------------");
		String namedQuery="SELECT ct.Id, "
				+ "ct.orderId, "
				+ "sum(ct.moneyOut) as moneyOut, "
				+ "ct.moneyIn as moneyIn, 1000 * unix_timestamp(ct.TransactionDate) as TransactionDate, "
//				+ "(Case when oi.PaymentModeId = 2 Or ct.PaymentModeId = 4 "
//			    + "then 'true' else 'false' end ) as isFullyPaidByWallet, "

				+ "Case when sum(Case when ct.PaymentModeId = 2 "
				+ "then 1 else 0 end ) = count(ct.PaymentModeId) then 'Fully paid' else 'Partially paid' end "
                + "as isFullyPaidByWallet, "
                
				+ "oi.paymentModeId, pm.ByPaymentMode "
				+ "From CustomerTransaction ct "
				+ "INNER JOIN OrderInfo oi ON oi.id = ct.OrderId "
				+ "INNER JOIN PaymentMode pm ON pm.id = oi.PaymentModeId "
				+ "INNER JOIN OrderDetailsInfo odi On odi.id = ct.orderDetailsId "
				+ "Where ct.customerId=:customerId and ct.moneyOut > 0 "
				+ "and oi.paymentModeId in (2,4,5,7) "
				+ "and ct.remarks != 1 "
				+ "and odi.Cancelled = false "
				+ "group by ct.orderId "
				+ "having sum(Case when ct.PaymentModeId = 2 then 1 else 0 end ) > 0 "
				+ "order by TransactionDate desc "
				+ "LIMIT :firstResult,:maxResult ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		query.setParameter("firstResult", firstResult);
		query.setParameter("maxResult", maxResult);
		List<Object[]> list = query.list();
		return list;
		
	}
	
	@Override
	public List<Object[]> getMoneyInTransactions(Long customerId, int firstResult, int maxResult) {
		System.out.println("----------  getMoneyInTransactions   -------------");
		String namedQuery= "SELECT cti.Id, cti.orderId, cti.moneyOut, cti.moneyIn, "
				+ "1000 * unix_timestamp(cti.TransactionDate) as TransactionDate, 'false', "
//				+ "cti.paymentModeId, "
				+ "1, "
				+ "Case when cti.moneyIn > 0 "
//                + "and cti.OrderId > 0 then concat('Refund | ID : ',cti.OrderId) else pmo.ByPaymentMode end as ByPaymentMode "
                + "and cti.OrderId > 0 then concat('Refund | ID : ',cti.OrderId) else 'By Cash' end as ByPaymentMode "
				+ "From CustomerTransaction cti "
				+ "INNER JOIN PaymentMode pmo ON pmo.id = cti.paymentModeId "
				+ "Where cti.customerId=:customerId and cti.moneyIn > 0 "
				+ "and cti.TransactionDescription in (1,11) "
				+ "order by TransactionDate desc "
				+ "LIMIT :firstResult,:maxResult ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		query.setParameter("firstResult", firstResult);
		query.setParameter("maxResult", maxResult);
		List<Object[]> list = query.list();
		return list;
	}
	

	@Override
	public long getTotalCountMoneyOutTransactions(Long customerId) {
		System.out.println("----------  getTotalCountMoneyOutTransactions   -------------");
		String namedQuery="SELECT "
				+ "ct.orderId "
				+ "From CustomerTransaction ct "
				+ "INNER JOIN OrderInfo oi ON oi.id = ct.OrderId "
				+ "INNER JOIN OrderDetailsInfo odi On odi.id = ct.orderDetailsId "
				+ "Where ct.customerId=:customerId and ct.moneyOut > 0 "
				+ "and oi.paymentModeId in (2,4,5,7) "
				+ "and ct.remarks != 1 "
				+ "and odi.Cancelled = false "
				+ "group by ct.orderId "
				+ "having sum(Case when ct.PaymentModeId = 2 then 1 else 0 end ) > 0 ";
		
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		return (long) (query.list().size());
	}
	
	@Override
	public long getTotalCountMoneyInTransactions(Long customerId) {
		System.out.println("----------  getTotalCountMoneyInTransactions   -------------");

		String namedQuery="SELECT cti.Id "
				+ "From CustomerTransaction cti "
				+ "Where cti.customerId=:customerId and cti.moneyIn > 0 "
				+ "and cti.TransactionDescription in (1,11) "
				+ "Order by cti.TransactionDate Desc";
		
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		return (long) (query.list().size());
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<CustomerTransactionDTO> getAllTransactionsByOrderDetailsId(Long orderDetailId) {
		String namedQuery = "CustomerTransaction.getAllTransactionsByOrderDetailsId";
		return this.getlist(namedQuery, CustomerTransactionDTO.class, -1, new Param("orderDetailsId", orderDetailId));
	
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getByOrderId(Long orderId) {
        String namedQuery= "SELECT t.PaymentModeId, SUM(t.MoneyOut), pm.PaymentModeAbbr from CustomerTransaction t "
        		+ "INNER JOIN PaymentMode pm ON pm.id = t.PaymentModeId "
				+ "where orderId=:orderId "
				+ "and t.moneyOut > 0 and t.remarks != 1 "
				+ "group by t.PaymentModeId "
				+ "order by t.paymentModeId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderId", orderId);
		return query.list();
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public String getByPaymentOptionTextByOrderId(Long orderId) {
        String namedQuery= "SELECT GROUP_CONCAT"
        		+ "(DISTINCT pm.PaymentModeAbbr order by pm.Id SEPARATOR '/' ) "
        		+ "from CustomerTransaction ct "
        		+ "INNER JOIN PaymentMode pm ON pm.id = ct.PaymentModeId "
        		+ "where ct.OrderId=:orderId "	
        		;
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderId", orderId);
		String que = query.list().get(0).toString();
		return que;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<CustomerTransactionDTO> getAllTransactionsByOrderDetailsIdWithNoBebt(Long orderDetailId) {
		String namedQuery = "CustomerTransaction.getAllTransactionsByOrderDetailsIdWithNoBebt";
		return this.getlist(namedQuery, CustomerTransactionDTO.class, -1, new Param("orderDetailsId", orderDetailId));
	
	}


	
	
	@Override
	public List<Object[]> getPaymentInfoByOrderDetailsId(Long orderDetailsId) {
        String namedQuery= "SELECT t.PaymentModeId, SUM(t.MoneyOut), pm.PaymentModeAbbr from CustomerTransaction t "
        		+ "INNER JOIN PaymentMode pm ON pm.id = t.PaymentModeId "
				+ "where orderDetailsId=:orderDetailsId "
				+ "and t.moneyOut > 0 and t.remarks != 1 "
				+ "group by t.PaymentModeId "
				+ "order by t.paymentModeId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderDetailsId", orderDetailsId);
		return query.list();
	}

	@Override
	public List<Object[]> getPaymentInfoByOrderIdList(List<Long> orderIdList) {
        String namedQuery= "SELECT t.PaymentModeId, SUM(t.MoneyOut), pm.PaymentModeAbbr from CustomerTransaction t "
        		+ "INNER JOIN OrderDetailsInfo od ON od.Id = t.orderDetailsId "
        		+ "INNER JOIN PaymentMode pm ON pm.id = t.PaymentModeId "
				+ "where t.orderId in (:orderIdList) "
				+ "and od.cancelled = 0 "
				+ "and t.moneyOut > 0 and t.remarks != 1 "
				+ "group by t.PaymentModeId "
				+ "order by t.paymentModeId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameterList("orderIdList", orderIdList);
		return query.list();
	}



}