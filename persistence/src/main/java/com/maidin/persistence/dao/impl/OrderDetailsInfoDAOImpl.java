package com.maidin.persistence.dao.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
//import com.maidin.persistence.dao.OrderDAO;
import com.maidin.persistence.dao.OrderDetailsInfoDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminTransactionsDTO;
//import com.maidin.persistence.dto.OrderDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.PaymentModeDTO;
import com.maidin.persistence.dto.V2.PreviousOrderDetailsInfoDTO;
import com.maidin.persistence.model.OrderPaymentInfo;

public class OrderDetailsInfoDAOImpl extends CommonDAO implements OrderDetailsInfoDAO {

	@Override
	public OrderDetailsInfoDTO getByOrderIdAndProductId(Long orderId, int productId) {
		String namedQuery = "OrderDetailsInfo.getByOrderIdAndProductId";
		return this.getQuery(namedQuery, OrderDetailsInfoDTO.class, new Param("orderId", orderId),
				new Param("productId", productId));
	}

	@Override
	public Long addOrderDetailsInfo(OrderDetailsInfoDTO orderDTO) {
		return (Long) this.add(orderDTO);
	}

	@Override
	public OrderDetailsInfoDTO getById(long Id) {
		String namedQuery = "OrderDetailsInfo.getById";
		return this.getQuery(namedQuery, OrderDetailsInfoDTO.class, new Param("Id", Id));
	}

	@Override
	public void deleteOrderDetails(OrderDetailsInfoDTO orderInfoDTO) {
		this.delete(orderInfoDTO);

	}

	@Override
	public List<OrderDetailsInfoDTO> getByOrderId(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getByOrderId";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	@Override
	public List<OrderDetailsInfoDTO> getByCustomerIdDeliveryDateAndPaymentStatus(Long customerId, Date deliveryDate,
			String paymentStatus) {
		String namedQuery = "OrderDetailsInfo.getByCustomerIdDeliveryDate";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("paymentStatus", paymentStatus));
	}

	@Override
	public List<Long> getIdListByOrderId(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getIdListByOrderId";
		return this.getColumnslist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	@Override
	public void deleteorderDetailsInfoList(List<Long> orderDetailsInfoIdList) {
		String namedQuery = "OrderDetailsInfo.deleteorderDetailsInfoList";
		this.updateQuery(namedQuery, new Param("orderList", orderDetailsInfoIdList));

	}

	@Override
	public void updateOrderDetailsInfoList(List<Long> orderDetailsInfoIdList, boolean cancelled, int quantity,
			Timestamp updatedOn) {
		String namedQuery = "OrderDetailsInfo.updateOrderDetailsInfoList";
		this.updateQuery(namedQuery, new Param("orderList", orderDetailsInfoIdList), new Param("cancelled", cancelled),
				new Param("quantity", quantity), new Param("updatedOn", updatedOn));

	}

	@Override
	public List<OrderDetailsInfoDTO> getByOrderIdWithNoCancelStatus(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getByOrderIdWithNoCancelStatus";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderDetailsInfoDTO> getSettledOrderDetailsByOrderId(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getSettledOrderDetailsByOrderId";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	// Added By Pushpinder 29/05/18
	// YTT : YESTERDAY, TODAY, TOMORROW
	@Override
	public List<OrderDetailsInfoDTO> getOrdersByYTT(Long excludeAddressId, String date) {
		String namedQuery = "SELECT * FROM OrderDetailsInfo d " + "where d.cancelled = false "
				+ "and (d.orderStatus='DELIVERED' " + "or d.orderStatus='CONFIRMED') "
				+ "and d.orderId in (SELECT t.Id FROM OrderInfo t " + "where t.deliveryDate=:date "
				+ "and t.cancelled = false "
				+ "and t.customerId in (SELECT c.id FROM Customer c where c.addressId !=:addressId))";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(OrderDetailsInfoDTO.class);
		query.setParameter("addressId", excludeAddressId);
		query.setParameter("date", date);
		return query.list();
	}

	@Override
	public List<OrderDetailsInfoDTO> getUnSettledPaymentOrders(Long customerId) {
		String namedQuery = "OrderDetailsInfo.getUnSettledPaymentOrders";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<OrderDetailsInfoDTO> getUnSettledOrdersByOrderId(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getUnSettledOrdersByOrderId";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	@Override
	public Float getTotalPaidByCustomer(List<Long> orderDetailsInfoIdList) {
		String namedQuery = "OrderDetailsInfo.getSumOfAmountPaidByCustomer";
		// Query query =
		// this.sessionFactory.getCurrentSession().createQuery(namedQuery);
		Query query = this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
				.setParameterList("orderDetailsInfoIdList", orderDetailsInfoIdList);
		// query.setEntity(0, orderDetailsInfoIdList);
		Double doubleVal = (Double) query.list().get(0);
		return doubleVal.floatValue();
	}

	@Override
	public List<OrderDetailsInfoDTO> getUnSettledDeliveredOrders(Long customerId) {
		String namedQuery = "OrderDetailsInfo.getUnSettledDeliveredOrders";
		return this.getlist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<Integer> getByOrderIdAndPaymentModeId(Long orderId) {
		String namedQuery = "OrderDetailsInfo.getByOrderIdAndPaymentModeId";
		return this.getColumnslist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderId", orderId));
	}

	@Override
	public List<PreviousOrderDetailsInfoDTO> getOrderDetails(List<Long> orderIdList, String orderStatus,
			boolean isCancelled) {
		String namedQuery = "SELECT distinct od.orderId,od.Id as orderDetailsId, p.Id as productId, od.productId as productDetailsId, "
				+ "p.ProductName, b.brand, pc.QuantityUnit, p.foodType, od.quantity, c.Id as categoryId,pc.subCategoryId, "
				+ "od.totalProductCost,od.amountPaidByCustomer, od.productMRP,od.ProductSalePrice, od.DiscountAmount, od.deliveryCharges, "
				+ "od.OrderStatus, od.orderedTime,od.PaymentModeId, pc.MRP,pc.SalePrice, pc.Discount ,pc.DiscountUnit,pc.DeliveryCharge, "
				+ "pc.Active,pc.StockStatus, pc.StockQuantity,pc.PhotoId "
				+ " FROM maidin.OrderDetailsInfo od, ProductCityInfoV2 pc , ProductInfoV2 p, BrandInfoV2 b, CategoryInfoV2 c "
				+ "where  od.orderId in :orderIds and od.productId = pc.Id and pc.productId = p.Id and p.brandId = b.Id and "
				+ "pc.categoryId=c.Id and od.Cancelled=:cancelled and od.OrderStatus=:orderStatus";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameterList("orderIds", orderIdList);
		query.setParameter("cancelled", isCancelled);
		query.setParameter("orderStatus", orderStatus);
		List<Object[]> list = query.list();
		List<PreviousOrderDetailsInfoDTO> adminTransactionsDTOs = new ArrayList<>();
		list.stream().forEach((record) -> {
			PreviousOrderDetailsInfoDTO transactionDetails = new PreviousOrderDetailsInfoDTO();
			transactionDetails.setOrderId(Long.valueOf(record[0].toString()));
			transactionDetails.setOrderDetailsId(Long.valueOf(record[1].toString()));
			transactionDetails.setProductId(Long.valueOf(record[2].toString()));
			transactionDetails.setProductDetailsId(Long.valueOf(record[3].toString()));
			transactionDetails.setProductName(String.valueOf(record[4].toString()));
			transactionDetails.setBrandName(String.valueOf(record[5].toString()));
			transactionDetails.setQuantityUnit(String.valueOf(record[6].toString()));
			transactionDetails.setFoodType(Integer.valueOf(record[7].toString()));
			transactionDetails.setQuantity(Integer.valueOf(record[8].toString()));
			transactionDetails.setCategoryId(Integer.valueOf(record[9].toString()));
			transactionDetails.setSubCategoryId(Integer.valueOf(record[10].toString()));
			transactionDetails.setTotalPrice(Float.valueOf(record[11].toString()));
			transactionDetails.setAmountPaidByCustomer(Float.valueOf(record[12].toString()));
			transactionDetails.setOrderMRP(Float.valueOf(record[13].toString()));
			transactionDetails.setOrderSalePrice(Float.valueOf(record[14].toString()));
			transactionDetails.setOrderSavedAmount(Float.valueOf(record[15].toString()));
			transactionDetails.setOrderDeliveryCharge(Float.valueOf(record[16].toString()));
			transactionDetails.setOrderStatus(String.valueOf(record[17].toString()));
			// transactionDetails.setOrderedTime(Integer.valueOf(record[18].toString()));
			transactionDetails.setPaymentModeId(record[19] == null ? 0 : Integer.parseInt(record[19].toString()));
			transactionDetails.setProductMRP(Float.valueOf(record[20].toString()));
			transactionDetails.setProductSalePrice(Float.valueOf(record[21].toString()));
			transactionDetails.setProductDiscount(Float.valueOf(record[22].toString()));
			transactionDetails.setProductDiscountUnit(String.valueOf(record[23].toString()));
			// transactionDetails.setProductSavedAmount(Integer.valueOf(record[21].toString()));
			transactionDetails.setProductDeliveryCharge(Float.valueOf(record[24].toString()));
			transactionDetails.setProductActiveStatus(Boolean.valueOf(record[25].toString()));
			transactionDetails.setProductStockStatus(Boolean.valueOf(record[26].toString()));
			transactionDetails.setProductStockQuantity(Integer.valueOf(record[27].toString()));
			transactionDetails.setPhotoId(record[28] == null ? 0 : Long.valueOf(record[28].toString()));
			adminTransactionsDTOs.add(transactionDetails);
		});
		System.out.println(adminTransactionsDTOs);
		return adminTransactionsDTOs;
	}

	@Override
	public void updateOrderDetails(OrderDetailsInfoDTO orderDetailsInfo) {
		this.update(orderDetailsInfo);

	}

	@Override
	public List<Long> getUnsettledOrderCustomerList() {
		String namedQuery = "OrderDetailsInfo.getUnsettledOrderCustomerList";
		return this.getColumnslist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("orderStatus", "DELIVERED"));
	}

	@Override
	public List<Long> getByIds(List<Long> Ids) {
		String namedQuery = "OrderDetailsInfo.getByIds";
		return this.getColumnslist(namedQuery, OrderDetailsInfoDTO.class, -1, new Param("Ids", Ids));
	}

	@Override
	public List<PreviousOrderDetailsInfoDTO> getOrderDetailsWithoutOrderState(List<Long> orderIdList,
			boolean isCancelled) {
		String namedQuery = "SELECT distinct od.orderId,od.Id as orderDetailsId, p.Id as productId, od.productId as productDetailsId, "
				+ "p.ProductName, b.brand, pc.QuantityUnit, p.foodType, od.quantity, c.Id as categoryId,pc.subCategoryId, "
				+ "od.totalProductCost,od.amountPaidByCustomer, od.productMRP,od.ProductSalePrice, od.DiscountAmount, od.deliveryCharges, "
				+ "od.OrderStatus, od.orderedTime,od.PaymentModeId, pc.MRP,pc.SalePrice, pc.Discount ,pc.DiscountUnit,pc.DeliveryCharge, "
				+ "pc.Active,pc.StockStatus, pc.StockQuantity,pc.PhotoId "
				+ " FROM maidin.OrderDetailsInfo od, ProductCityInfoV2 pc , ProductInfoV2 p, BrandInfoV2 b, CategoryInfoV2 c "
				+ "where  od.orderId in :orderIds and od.productId = pc.Id and pc.productId = p.Id and p.brandId = b.Id and "
				+ "pc.categoryId=c.Id and od.Cancelled=:cancelled and od.OrderStatus in ('DELIVERED','CONFIRMED')";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameterList("orderIds", orderIdList);
		query.setParameter("cancelled", isCancelled);
		List<Object[]> list = query.list();
		List<PreviousOrderDetailsInfoDTO> adminTransactionsDTOs = new ArrayList<>();
		list.stream().forEach((record) -> {
			PreviousOrderDetailsInfoDTO transactionDetails = new PreviousOrderDetailsInfoDTO();
			transactionDetails.setOrderId(Long.valueOf(record[0].toString()));
			transactionDetails.setOrderDetailsId(Long.valueOf(record[1].toString()));
			transactionDetails.setProductId(Long.valueOf(record[2].toString()));
			transactionDetails.setProductDetailsId(Long.valueOf(record[3].toString()));
			transactionDetails.setProductName(String.valueOf(record[4].toString()));
			transactionDetails.setBrandName(String.valueOf(record[5].toString()));
			transactionDetails.setQuantityUnit(String.valueOf(record[6].toString()));
			transactionDetails.setFoodType(Integer.valueOf(record[7].toString()));
			transactionDetails.setQuantity(Integer.valueOf(record[8].toString()));
			transactionDetails.setCategoryId(Integer.valueOf(record[9].toString()));
			transactionDetails.setSubCategoryId(Integer.valueOf(record[10].toString()));
			transactionDetails.setTotalPrice(Float.valueOf(record[11].toString()));
			transactionDetails.setAmountPaidByCustomer(Float.valueOf(record[12].toString()));
			transactionDetails.setOrderMRP(Float.valueOf(record[13].toString()));
			transactionDetails.setOrderSalePrice(Float.valueOf(record[14].toString()));
			transactionDetails.setOrderSavedAmount(Float.valueOf(record[15].toString()));
			transactionDetails.setOrderDeliveryCharge(Float.valueOf(record[16].toString()));
			transactionDetails.setOrderStatus(String.valueOf(record[17].toString()));
			// transactionDetails.setOrderedTime(Integer.valueOf(record[18].toString()));
			transactionDetails.setPaymentModeId(record[19] == null ? 0 : Integer.parseInt(record[19].toString()));
			transactionDetails.setProductMRP(Float.valueOf(record[20].toString()));
			transactionDetails.setProductSalePrice(Float.valueOf(record[21].toString()));
			transactionDetails.setProductDiscount(Float.valueOf(record[22].toString()));
			transactionDetails.setProductDiscountUnit(String.valueOf(record[23].toString()));
			// transactionDetails.setProductSavedAmount(Integer.valueOf(record[21].toString()));
			transactionDetails.setProductDeliveryCharge(Float.valueOf(record[24].toString()));
			transactionDetails.setProductActiveStatus(Boolean.valueOf(record[25].toString()));
			transactionDetails.setProductStockStatus(Boolean.valueOf(record[26].toString()));
			transactionDetails.setProductStockQuantity(Integer.valueOf(record[27].toString()));
			transactionDetails.setPhotoId(record[28] == null ? 0 : Long.valueOf(record[28].toString()));
			adminTransactionsDTOs.add(transactionDetails);
		});
		System.out.println(adminTransactionsDTOs);
		return adminTransactionsDTOs;
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderDetailsInfoDTO> getSettledOrderByOrderIdList(List<Long> Ids) {
		String namedQuery = "OrderDetailsInfo.getSettledOrderByOrderIdList";
		return this.list(namedQuery, OrderDetailsInfoDTO.class, new Param("Ids", Ids));

	}

	@Override
	public OrderPaymentInfo getOrderPaymentInfo(Long orderId) {
		String namedQuery = "SELECT sum(totalProductCost), sum(totalProductCost - amountPaidByCustomer) FROM maidin.OrderDetailsInfo od  "
				+ "where  od.orderId=:orderId and od.Cancelled=0";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderId", orderId);
		List<Object[]> queryResult = query.list();
		OrderPaymentInfo orderPaymentInfo = new OrderPaymentInfo();
		queryResult.forEach((record) -> {
			// check for null Pointer Exception
			orderPaymentInfo.setSumTotal(Float.valueOf((record[0]==null?0:record[0]).toString()));
			orderPaymentInfo.setDifference(Float.valueOf((record[1]==null?0:record[1]).toString()));

		});
		return orderPaymentInfo;
	}

	@Override
	public List<Object[]> getSettledOrdersByOrderId(Long orderId) {
		// String namedQuery =
		// "OrderDetailsInfo.getSettledOrderDetailsByOrderId";
		String namedQuery = "SELECT od.Id,od.orderId,concat(COALESCE(b.brand,''),' ',COALESCE(p.ProductName,'')) , pc.quantityUnit, od.quantity, "
				+ " od.productMRP,od.productSalePrice,od.totalProductCost,od.amountPaidByCustomer,(od.totalProductCost-od.amountPaidByCustomer) as pendingAmount, "
				+ " od.deliveryCharges,os.Id as orderStatusId,od.paymentStatus, pc.MRP,pc.salePrice,pc.deliveryCharge from OrderDetailsInfo od, ProductCityInfoV2 pc, ProductInfoV2 p, BrandInfoV2 b "
				+ " , OrderStatus os where od.orderId=:orderId and od.orderStatus != 'CANCELLED' and od.OrderStatus=os.OrderStatus and od.productId = pc.Id and pc.productId = p.Id and p.brandId = b.Id ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderId", orderId);
		return query.list();
	}

}
