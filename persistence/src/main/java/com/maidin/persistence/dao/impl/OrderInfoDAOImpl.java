package com.maidin.persistence.dao.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.OrderInfoDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminTransactionsDTO;
import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.V2.PreviousOrderInfoDTO;
import com.maidin.persistence.dto.V2.UpcomingOrderDTO;

public class OrderInfoDAOImpl extends CommonDAO implements OrderInfoDAO {

	@Override
	public Long addOrderInfo(OrderInfoDTO orderDTO) {
		return (Long) this.add(orderDTO);

	}

	@Override
	public OrderInfoDTO getById(Long Id) {
		String namedQuery = "OrderInfo.getById";
		return this.getQuery(namedQuery, OrderInfoDTO.class, new Param("Id", Id));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDateAndTimeSlot";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("timeSlot", timeSlot));
	}

	@Override
	public List<Date> getDifferentDeliveryDateByCustomerId(Long customerId) {
		String namedQuery = "OrderInfo.getDifferentDeliveryDates";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<OrderInfoDTO> getByCustomerIdAndDeliveryDate(Long customerId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getByCustomerIdAndDeliveryDate";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	public List<Date> getByCustomerIdAndLatestPastOrder(Long customerId, int maxResults, String paymentStatus) {
		String namedQuery = "OrderInfo.getDifferentPastDeliveryDates";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, maxResults, new Param("customerId", customerId),
				new Param("paymentStatus", paymentStatus));
	}

	@Override
	public List<Date> getPastDaysDeliveryByPagination(Long customerId, String paymentStatus, int firstResult,
			int maxResult) {
		String namedQuery = "OrderInfo.getDifferentPastDeliveryDates";
		return this.listByCriteria(namedQuery, OrderInfoDTO.class, firstResult, maxResult,
				new Param("customerId", customerId), new Param("paymentStatus", paymentStatus));
	}

	@Override
	public List<Date> getDifferentDeliveryDatesByTimestamp(Date fromTimestamp, Date toTimestamp) {
		String namedQuery = "OrderInfo.getDifferentDeliveryDatesByTimestamp";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromTimestamp", fromTimestamp),
				new Param("toTimestamp", toTimestamp));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Date> getByCustomerDifferentDeliveryDates(Long customerId, Date fromTimestamp, Date toTimestamp) {

		String namedQuery = "OrderInfo.getByCustomerDifferentDeliveryDates";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("fromTimestamp", fromTimestamp), new Param("toTimestamp", toTimestamp));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByDeliveryDates(Date deliveryDate) {
		String namedQuery = "OrderInfo.getByDeliveryDates";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("deliveryDate", deliveryDate));
	}

	@Override
	public List<OrderInfoDTO> getAllScheduledOrders(Long customerId) {
		String namedQuery = "OrderInfo.getAllScheduledOrders";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public void deleteOrderInfo(OrderInfoDTO orderInfo) {
		this.delete(orderInfo);

	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Long> getDifferentCustomersByTimestamp(Date fromDate, Date toDate) {
		String namedQuery = "OrderInfo.getDifferentCustomersByTimestamp";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByCustomerId(Long customerId) {
		String namedQuery = "OrderInfo.getByCustomerId";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Long> getDifferentCustomersByTimestampAndTimeSlotId(Date fromDate, Date toDate, Integer timeSlotId) {
		String namedQuery = "OrderInfo.getDifferentCustomersByTimestampAndTimeSlotId";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate), new Param("timeSlotId", timeSlotId));
	}

	@Override
	public List<OrderInfoDTO> getByCustomerIdAndTimeSlot(Long customerId, Integer timeSlotId) {
		String namedQuery = "OrderInfo.getByCustomerIdAndTimeSlot";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("timeSlotId", timeSlotId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByCustomerIdDeliveryDate(Long customerId, Date fromDate, Date toDate) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDates";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("fromDate", fromDate), new Param("toDate", toDate));

	}

	@Override
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlot(Long customerId, Date fromDate, Date toDate,
			Integer timeSlotId) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDatesAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("fromDate", fromDate), new Param("toDate", toDate), new Param("timeSlotId", timeSlotId));

	}

	@Override
	public List<Date> getByDeliveryDateAndTimeSlot(Date fromDate, Date toDate, String timeslotId) {
		String namedQuery = "OrderInfo.getByDeliveryDatesAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate), new Param("timeSlot", timeslotId));

	}

	@Override
	public List<Date> getByDeliveryDateAndTimeSlotByCity(String cityName, Date date, Date date2, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Date> getByDeliveryDateAndTimeSlotByCityProject(String cityName, String projectName, Date date,
			Date date2, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Date> getByDeliveryDateAndTimeSlotByCityProjectTower(String cityName, String projectName,
			String towerName, Date date, Date date2, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Date> getByDeliveryDateAndTimeSlotByCityProjectTowerFlat(String cityName, String projectName,
			String towerName, String flatName, Date date, Date date2, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Long> getDifferentCustomersByDeliveryDate(Date deliveryDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Long> getDifferentCustomersByDeliveryDateAndTimeSlot(Date deliveryDate, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	///////////////////////////////////////////////////////////////////////
	@Override
	public List<OrderInfoDTO> getCustomersByCPTDeliveryDateAndTimeSlot(String cityName, String projectName, String towerName,
			Date deliveryDate, String timeSlot) {
		// s
		String namedQuery = "OrderInfo.getCustomersByCPTDeliveryDateAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("city", cityName),
				new Param("project", projectName), new Param("tower", towerName),
				new Param("deliveryDate", deliveryDate), new Param("timeSlot", timeSlot));

	}

	@Override
	public List<OrderInfoDTO> getCustomersByCPTDeliveryDate(String cityName, String projectName, String towerName,
			Date deliveryDate) {
		String namedQuery = "OrderInfo.getCustomersByCPTDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("city", cityName),
				new Param("project", projectName), new Param("tower", towerName),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	public List<OrderInfoDTO> getCustomersByCPDeliveryDateAndTimeSlotId(String cityName, String projectName, Date deliveryDate,
			int timeSlotId) {
		// s
		String namedQuery = "OrderInfo.getCustomersByCPDeliveryDateAndTimeSlotId";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("city", cityName),
				new Param("project", projectName), new Param("deliveryDate", deliveryDate),
				new Param("timeSlotId", timeSlotId));
	}

	@Override
	public List<OrderInfoDTO> getCustomersByCPDeliveryDate(String cityName, String projectName, Date deliveryDate) {
		String namedQuery = "OrderInfo.getCustomersByCPDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("city", cityName),
				new Param("project", projectName), new Param("deliveryDate", deliveryDate));
	}

	/*
	 * @Override public List<Integer>
	 * getTimeSlotsByCustomerCPTDeliveryDateAndTimeSlot(Long customerId, String
	 * cityName, String projectName, String towerName, long deliveryDate, String
	 * timeSlot) { // TODO Auto-generated method stub return null; }
	 * 
	 * @Override public List<Integer> getTimeSlotsByCustomerCPTDeliveryDate(Long
	 * customerId, String cityName, String projectName, String towerName, Date
	 * deliveryDate) { // TODO Auto-generated method stub return null; }
	 * 
	 * @Override public List<Integer>
	 * getTimeSlotsByCustomerCPDeliveryDateAndTimeSlot(Long customerId, String
	 * cityName, String projectName, Date deliveryDate, String timeSlot) { //
	 * TODO Auto-generated method stub return null; }
	 * 
	 * @Override public List<Integer> getTimeSlotsByCustomerCPDeliveryDate(Long
	 * customerId, String cityName, String projectName, Date deliveryDate) { //
	 * TODO Auto-generated method stub return null; }
	 */

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Integer> getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(Long customerId, Date deliveryDate,
			int timeSlotId) {
		String namedQuery = "OrderInfo.getTimeSlotByCustomerDeliveryDateAndTimeSlotId";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("timeSlotId", timeSlotId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Integer> getTimeSlotByCustomerDeliveryDate(Long customerId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getTimeSlotsByCustomerDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlotId(Long customerId, Date deliveryDate,
			Integer timeSlotId) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDateAndTimeSlotId";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("Id", timeSlotId));
	}

	@Override
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateWithVegetableCategory(Long customerId, Date deliveryDate,
			int categoryId) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDateWithVegetableCategory";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("categoryId", categoryId));
	}

	@Override
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateWithOtherCategory(Long customerId, Date deliveryDate,
			int categoryId) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDateWithOtherCategory";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("categoryId", categoryId));
	}

	@Override
	public List<Long> getCheckoutIdsOfUpcomingCurrentDayTimeSlots(long customerId, int lastLockingTime) {
		String namedQuery = "OrderInfo.getCheckoutIdsOfUpcomingCurrentDayTimeSlots";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("lastLockingTime", lastLockingTime));
	}

	@Override
	public List<Integer> getTimeSlotsByCustomerIdAndDeliveryDate(Long customerId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getTimeSlotsByCustomerIdAndDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	public List<Date> getNextOrderDate(Long customerId) {
		String namedQuery = "OrderInfo.getNextOrderDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<Date> getPastOrderDates(Long customerId) {
		String namedQuery = "OrderInfo.getPastOrderDates";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<Integer> getCheckoutTimeSlotsByIds(List<Long> orderIds) {
		String namedQuery = "OrderInfo.getCheckoutTimeSlotsByIds";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("Ids", orderIds));
	}

	/*
	 * @Override public OrderInfoDTO getByScheduleIdAndDeliveryDate(Long id,
	 * Date deliveryDate) { // TODO Auto-generated method stub return null; }
	 */

	@Override
	public List<OrderInfoDTO> getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId(Long customerId, Date deliveryDate,
			Integer timeSlotId) {
		String namedQuery = "OrderInfo.getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("Id", timeSlotId));
	}

	@Override
	public List<Integer> getCheckoutTimeSlotByCustomerDeliveryDate(Long customerId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getCheckoutTimeSlotByCustomerDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	public OrderInfoDTO getByScheduleIdDeliveryDate(Long scheduleId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getByScheduleIdDeliveryDate";
		return this.getQuery(namedQuery, OrderInfoDTO.class, new Param("scheduleId", scheduleId),
				new Param("deliveryDate", deliveryDate));

	}

	@Override
	public List<OrderInfoDTO> getOrdersByScheduleId(Long scheduleId) {
		String namedQuery = "OrderInfo.getOrdersByScheduleId";
		return this.list(namedQuery, OrderInfoDTO.class, new Param("scheduleId", scheduleId));
	}

	@Override
	public List<OrderInfoDTO> getScheduleOrdersByTimeSlots(List<Integer> timeSlotIdList) {
		String namedQuery = "OrderInfo.getScheduleOrdersByTimeSlot";
		return this.list(namedQuery, OrderInfoDTO.class, new Param("timeSlotIdList", timeSlotIdList));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Date> getCustomersSortedByDeliveryDate(Date fromDate, Date toDate) {
		String namedQuery = "OrderInfo.getCustomersSortedByDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Date> getCustomersInTimeSlotSortedByDeliveryDate(Date fromDate, Date toDate, Integer timeslotId) {
		String namedQuery = "OrderInfo.getCustomersInTimeSlotSortedByDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate), new Param("timeslotId", timeslotId));
	}

	// Added By Pushpinder 29/05/18
	@Override
	public List<OrderInfoDTO> getByDeliveryDate(Long excludeAddressId) {
		// String namedQuery = "OrderInfo.getByDeliveryDate";
		// return this.list(namedQuery, OrderInfoDTO.class, new
		// Param("addressId", excludeAddressId));

		String namedQuery = "SELECT * FROM OrderInfo t "
				+ "where t.deliveryDate >= DATE_ADD(CURDATE(), INTERVAL -1 DAY) "
				+ "and t.deliveryDate <= DATE_ADD(CURDATE(), INTERVAL 1 DAY) " + "and t.cancelled = false "
				//+ "and t.customerId in (SELECT c.Id FROM Customer c where c.addressId !=:addressId)";
		+ "and t.addressId != :addressId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(OrderInfoDTO.class);
		query.setParameter("addressId", excludeAddressId);
		return query.list();
	}

	@Override
	public long getTotalCountByDeliveryDate(Long excludeAddressId, String date) {
		String namedQuery = "SELECT * FROM OrderInfo t " + "where t.deliveryDate=:date "
				+ "and t.overallOrderStatus!=4 " + "and t.overallOrderStatus!=6 " + "and t.cancelled = false "
				//+ "and t.customerId in (SELECT c.id FROM Customer c where c.addressId !=:addressId) "
				+ "and t.addressId != :addressId " 
				+ "group by t.timeSlot, t.customerId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(OrderInfoDTO.class);
		query.setParameter("addressId", excludeAddressId);
		query.setParameter("date", date);
		return (long) query.list().size();
	}

	@Override
	public long getTotalCountByDeliveryDateExcludingUndelivered(Long excludeAddressId, String date) {
		String namedQuery = "SELECT * FROM OrderInfo t " + "where t.deliveryDate=:date "
				+ "and t.overallOrderStatus!=4 " + "and t.cancelled = false "
				//+ "and t.customerId in (SELECT c.id FROM Customer c where c.addressId !=:addressId) "
				+ "and t.addressId != :addressId " 
				+ "group by t.timeSlot, t.customerId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(OrderInfoDTO.class);
		query.setParameter("addressId", excludeAddressId);
		query.setParameter("date", date);
		return (long) query.list().size();
	}

	@Override
	public long getTotalMarkedDeliveries(Long excludeAddressId, String date) {
		String namedQuery = "SELECT * FROM OrderInfo t " + "where t.deliveryDate=:date "
				+ "and (t.overallOrderStatus = 3 " + "or t.overallOrderStatus = 6 " + "or t.overallOrderStatus = 9 ) "
				+ "and t.cancelled = false "
			//	+ "and t.customerId in (SELECT c.id FROM Customer c where c.addressId !=:addressId) "
			+ "and t.addressId != :addressId " 
				+ "group by t.timeSlot, t.customerId ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(OrderInfoDTO.class);
		query.setParameter("addressId", excludeAddressId);
		query.setParameter("date", date);
		return (long) query.list().size();
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getCustomerOrdersByDate(Date fromDate, Date toDate) {//test it
		String namedQuery = "SELECT o.deliveryDate,  o.TimeSlot,o.customerId, c.FullName,a.Project,a.Tower, "
				+ " a.Flat,c.MobileNumber,ci.Category,pb.brand,pi.productName, pc.quantityUnit, od.Quantity, "
				+ " od.totalProductCost,pm.paymentMode,od.OrderedTime,od.OrderStatus,od.productMRP, pc.SalePrice, "
				+ " od.Id, t.Id as timeSlotId,od.amountPaidByCustomer,od.paymentStatus, "
				+ " od.payBy, a.cityCode, o.requestedPaymentModeId, group_concat(pm.paymentModeAbbr) as paymentMethod FROM maidin.OrderInfo o,  "
				+ " Customer c,Address a , CategoryInfoV2 ci, BrandInfoV2 pb,ProductCityInfoV2 pc,ProductInfoV2 pi, "
				+ " TimeSlot t , OrderDetailsInfo od "
				+ " left join PaymentMode pm on find_in_set(pm.Id, od.payBy) "
				+ " where o.Id = od.orderId and c.Id = o.customerId and "
				//+ "c.addressId = a.Id and "
				+ " o.AddressId = a.Id and "
				+ " od.productId = pc.Id and pc.productId = pi.Id and pi.brandId = pb.Id and pc.categoryId = ci.Id  "
				//+ " pm.Id = ( IF(od.paymentModeId>0 , od.paymentModeId,  if(o.paymentModeId is null,0,o.paymentModeId) )) "
				+ " and o.cancelled = 0 and od.orderStatus != 'CANCELLED' and t.Id=o.timeSlotId "
				+ " and o.deliveryDate BETWEEN :fromDate AND :toDate group by od.Id order by a.City,a.Project,a.Tower, "
				+ " o.deliveryDate, t.StartTime, c.FullName";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		return query.list();
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getCustomerOrdersByDateAndSlot(Date fromDate, Date toDate, String timeSlot) {//test it
				String namedQuery = "SELECT o.deliveryDate,  o.TimeSlot,o.customerId, c.FullName, a.Project, a.Tower, "
				+ " a.Flat, c.MobileNumber, ci.Category, pb.brand, pi.productName, pc.quantityUnit, od.Quantity, "
				+ " od.totalProductCost, pm.paymentMode, od.OrderedTime, od.OrderStatus, od.productMRP, pc.SalePrice, "
				+ " od.Id, o.timeSlotId,od.amountPaidByCustomer,od.paymentStatus, "
				+ " od.payBy, a.cityCode, o.requestedPaymentModeId, group_concat(pm.paymentModeAbbr) as paymentMethod "
				+ " FROM maidin.OrderInfo o,"
				+ " Customer c, Address a , CategoryInfoV2 ci, BrandInfoV2 pb, ProductCityInfoV2 pc, ProductInfoV2 pi, "
				// + " PaymentMode pm, "
				+ "TimeSlot t  , OrderDetailsInfo od "
				+ " left join PaymentMode pm on find_in_set(pm.Id, od.payBy)  where o.Id = od.orderId and c.Id = o.customerId and "
				//+ "c.addressId = a.Id and "
				+ " o.AddressId = a.Id and t.Id=o.timeSlotId and "
				+ " od.productId = pc.Id and pc.productId = pi.Id and pi.brandId = pb.Id and pc.categoryId = ci.Id  "
			//	+ " and pm.Id = ( IF(od.paymentModeId>0 , od.paymentModeId, if(o.paymentModeId is null,0,o.paymentModeId))) "
				+ " and o.cancelled = 0 and od.orderStatus != 'CANCELLED' "
				+ " and o.deliveryDate BETWEEN :fromDate AND :toDate and o.timeSlot =:timeSlot group by od.Id order by a.City, "
				+ " a.Project,a.Tower, o.deliveryDate, t.StartTime, c.FullName ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		query.setParameter("timeSlot", timeSlot);
		return query.list();
	}

	@Override
	public List<PreviousOrderInfoDTO> getPreviousOrders(Long customerId, String orderStatus, boolean isCancelled,
			int firstResult, int maxResults) {
		String namedQuery = "SELECT distinct "
				+ "oi.Id,oi.TimeSlot, oi.DeliveryDate, oi.ScheduleId, oi.AddressId "
				+ "	as address, oi.orderedTime, oi.OverallOrderStatus FROM maidin.OrderInfo oi,"
				+ "OrderDetailsInfo od," 
				+ " TimeSlot t where oi.Cancelled=:isCancelled and od.OrderStatus=:orderStatus and "
				+ "oi.customerId=:customerId and oi.timeSlotId = t.Id "
			    + "and oi.Id = od.orderId " 
				+ "order By oi.deliveryDate desc,t.StartTime desc,oi.Id desc  ";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		query.setParameter("isCancelled", isCancelled);
		query.setParameter("orderStatus", orderStatus);
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResults);
		List<Object[]> list = query.list();
		List<PreviousOrderInfoDTO> adminTransactionsDTOs = new ArrayList<>();
		list.stream().forEach((record) -> {
			PreviousOrderInfoDTO transactionDetails = new PreviousOrderInfoDTO();
			/*
			 * transactionDetails.setTotalPaid(Float.valueOf(record[0].toString(
			 * )));
			 * transactionDetails.setTotalProductCost(Float.valueOf(record[1].
			 * toString()));
			 * transactionDetails.setNoOfItems(Integer.valueOf(record[2].
			 * toString()));
			 * transactionDetails.setTotalDeliveryCharge(Float.valueOf(record[3]
			 * .toString()));
			 */
			transactionDetails.setOrderId(Long.valueOf(record[0].toString()));
			transactionDetails.setTimeSlot(String.valueOf(record[1].toString()));
			transactionDetails.setDeliveryDate(Date.valueOf(record[2].toString()));
			transactionDetails.setScheduleId(record[3] != null ? Long.valueOf(record[3].toString()) : 0);
			transactionDetails.setAddressId(Long.valueOf(record[4].toString()));
			//transactionDetails.setPaymentOption(String.valueOf(record[5].toString()));
			transactionDetails.setOrderedTime(Timestamp.valueOf(record[5].toString()));
			transactionDetails.setOrderStatus(Integer.valueOf(record[6].toString()));
			adminTransactionsDTOs.add(transactionDetails);
		});
		// System.out.println(adminTransactionsDTOs);
		return adminTransactionsDTOs;

	}
	
	@Override
	public List<PreviousOrderInfoDTO> getOrderById(Long orderId) {
		String namedQuery = "SELECT distinct "
				/*
				 * +
				 * "sum(od.AmountPaidByCustomer), sum(od.TotalProductCost),sum(od.Quantity),sum(od.deliveryCharges), "
				 */
				+ "oi.Id,oi.TimeSlot, oi.DeliveryDate, oi.ScheduleId, oi.AddressId "
				+ "	as address, pm.PaymentOption,oi.orderedTime, oi.OverallOrderStatus FROM maidin.OrderInfo oi,"
				+ "OrderDetailsInfo od," 
				+ " PaymentMode pm where oi.Id=:orderId and "
				+ "oi.paymentModeId = pm.Id "
			    + "and oi.Id = od.orderId " 
				;
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("orderId", orderId);
		List<Object[]> list = query.list();
		List<PreviousOrderInfoDTO> adminTransactionsDTOs = new ArrayList<>();
		list.stream().forEach((record) -> {
			PreviousOrderInfoDTO transactionDetails = new PreviousOrderInfoDTO();
			transactionDetails.setOrderId(Long.valueOf(record[0].toString()));
			transactionDetails.setTimeSlot(String.valueOf(record[1].toString()));
			transactionDetails.setDeliveryDate(Date.valueOf(record[2].toString()));
			transactionDetails.setScheduleId(record[3] != null ? Long.valueOf(record[3].toString()) : 0);
			transactionDetails.setAddressId(Long.valueOf(record[4].toString()));
			transactionDetails.setPaymentOption(String.valueOf(record[5].toString()));
			transactionDetails.setOrderedTime(Timestamp.valueOf(record[6].toString()));
			transactionDetails.setOrderStatus(Integer.valueOf(record[7].toString()));
			adminTransactionsDTOs.add(transactionDetails);
		});
		return adminTransactionsDTOs;

	}

	@Override
	public Long getTotalOrderCount(Long customerId, int orderStausId, boolean cancelled) {
		//String namedQuery = "OrderInfo.getTotalOrderCount";
		return ((Long)this.sessionFactory.getCurrentSession().createQuery("SELECT count(oi) from OrderInfoDTO oi where oi.customerId=:customerId and oi.overallOrderStatus=:overallOrderStatus and oi.cancelled=:cancelled").
				setParameter("customerId", customerId).
				setParameter("overallOrderStatus", orderStausId).
				setParameter("cancelled", cancelled).uniqueResult());
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getOrdersByDateAndCityCode(Date fromDate, Date toDate, String cityCode) {
		String namedQuery = "OrderInfo.getOrdersByDateAndCityCode";
		return this.list(namedQuery, OrderInfoDTO.class, new Param("fromDate", fromDate),
				 new Param("toDate", toDate),
				 new Param("cityCode", cityCode));	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<OrderInfoDTO> getOrdersByDate(Date fromDate, Date toDate) {
		String namedQuery = "OrderInfo.getOrdersByDate";
		return this.list(namedQuery, OrderInfoDTO.class, new Param("fromDate", fromDate)
				, new Param("toDate", toDate));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getCustomerOrdersByDateSlotAndCityCode(Date fromDate, Date toDate, String timeSlot, String cityCode) {//test it
		String namedQuery = "SELECT o.deliveryDate, o.TimeSlot, o.customerId, c.FullName, a.Project, a.Tower, "
				+ "a.Flat, c.MobileNumber, ci.Category, pb.brand, pi.productName, pc.quantityUnit, od.Quantity, "
				+ " od.totalProductCost, pm.paymentMode, od.OrderedTime, od.OrderStatus, od.productMRP, "
				+ " pc.SalePrice, od.Id as orderDetailsId, t.Id as timeSlotId, "
				+ " od.amountPaidByCustomer, od.paymentStatus, od.payBy ,"
				+ " a.cityCode, o.requestedPaymentModeId, group_concat(pm.paymentModeAbbr) as paymentMethod"
				+ " FROM maidin.OrderInfo o, Address a, "
				+ " Customer c, CategoryInfoV2 ci, BrandInfoV2 pb,ProductCityInfoV2 pc, ProductInfoV2 pi, "
				+ " TimeSlot t, OrderDetailsInfo od "
				+ " left join PaymentMode pm on find_in_set(pm.Id, od.payBy) "
				+ " where o.Id = od.orderId and c.Id = o.customerId and "
				//+ " c.addressId = a.Id and  "
				+ " o.AddressId = a.Id and "
				+ " od.productId = pc.Id and "
				+ " pc.productId = pi.Id and pi.brandId = pb.Id and pc.categoryId = ci.Id  "
			//	+ " pm.Id = ( IF(od.paymentModeId>0 , od.paymentModeId,if(o.paymentModeId is null,0,o.paymentModeId) )) "
				+ " and o.cancelled = 0 and od.orderStatus != 'CANCELLED' and t.Id=o.timeSlotId "
				+ " and o.deliveryDate BETWEEN :fromDate AND :toDate and a.cityCode=:cityCode and t.TimeSlot =:timeSlot group by od.Id "
				+ " order by a.City,a.Project,a.Tower, o.deliveryDate, t.StartTime, c.FullName";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
		query.setParameter("timeSlot", timeSlot);
		query.setParameter("cityCode", cityCode);
		return query.list();
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getCustomerOrdersByDateAndCityCode(Date fromDate, Date toDate, String cityCode) {//test it
		
		String namedQuery = "SELECT o.deliveryDate, o.TimeSlot,o.customerId, c.FullName, a.Project, a.Tower, "
		+ " a.Flat, c.MobileNumber, ci.Category, pb.brand, pi.productName, pc.quantityUnit, od.Quantity, "
		+ " od.totalProductCost, pm.paymentMode, od.OrderedTime, od.OrderStatus, od.productMRP, pc.SalePrice, "
		+ " od.Id, o.timeSlotId,od.amountPaidByCustomer,od.paymentStatus, "
		+ " od.payBy, a.cityCode, o.requestedPaymentModeId, group_concat(pm.paymentModeAbbr) as paymentMethod FROM maidin.OrderInfo o, "
		+ " Customer c,Address a , CategoryInfoV2 ci, BrandInfoV2 pb,ProductCityInfoV2 pc,ProductInfoV2 pi, "
		+ " TimeSlot t , OrderDetailsInfo od "
		+ " left join PaymentMode pm on find_in_set(pm.Id, od.payBy) " 
		+ " where o.Id = od.orderId and c.Id = o.customerId and "
		//+ " c.addressId = a.Id and "
		+ " o.AddressId = a.Id and t.Id = o.timeSlotId and "
		+ " od.productId = pc.Id and pc.productId = pi.Id and pi.brandId = pb.Id and pc.categoryId = ci.Id  "
		//+ " pm.Id =( IF(od.paymentModeId>0 , od.paymentModeId, if(o.paymentModeId is null,0,o.paymentModeId)) ) "
		+ " and o.cancelled = 0 and od.orderStatus != 'CANCELLED'  "
		+ " and o.deliveryDate BETWEEN :fromDate AND :toDate and a.cityCode =:cityCode group by od.Id order by a.City,a.Project,"
		+ " a.Tower, o.deliveryDate, t.StartTime,c.FullName ";
SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
query.setParameter("fromDate", fromDate);
query.setParameter("toDate", toDate);
query.setParameter("cityCode", cityCode);
return query.list();
}

	@Override
	public void updateOrderInfo(OrderInfoDTO orderInfo) {
		this.update(orderInfo);
		
	}

	@Override
	public List<OrderInfoDTO> getCustomersByCPDeliveryDateAndTimeSlot(String cityName, String projectName, Date deliveryDate,
			String timeSlot) {
		// s
		String namedQuery = "OrderInfo.getCustomersByCPDeliveryDateAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("city", cityName),
				new Param("project", projectName), new Param("deliveryDate", deliveryDate),
				new Param("timeSlot", timeSlot));
	}

	

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Integer> getTimeSlotsByCustomerDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot) {
		String namedQuery = "OrderInfo.getTimeSlotByCustomerDeliveryDateAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("timeSlot", timeSlot));
	}

	@Override
	public List<Long> getDifferentCustomersByTimestampAndTimeSlot(Date fromDate, Date toDate, String timeSlot) {
		String namedQuery = "OrderInfo.getDifferentCustomersByTimestampAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("fromDate", fromDate),
				new Param("toDate", toDate), new Param("timeSlot", timeSlot));
	}

	@Override
	public List<Object[]> getCustomerOrdersByDateAndTimeSlotId(Date fromDate, Date toDate, int timeSlotId) {String namedQuery = "SELECT o.deliveryDate,  o.TimeSlot,o.customerId, c.FullName,a.Project,a.Tower, "
			+ "a.Flat,c.MobileNumber,ci.Category,pb.brand,pi.productName, pc.quantityUnit,od.Quantity, "
			+ " od.totalProductCost,pm.paymentMode,od.OrderedTime,od.OrderStatus,od.productMRP, "
			+ " pc.SalePrice,od.Id as orderDetailsId,t.Id as timeSlotId, "
			+ " od.amountPaidByCustomer,od.paymentStatus,o.paymentModeId,od.paymentModeId as orderDetails_paymentMode,"
			+ " a.CityCode "
			+ " FROM maidin.OrderInfo o, Address a, "
			+ " OrderDetailsInfo od, Customer c, CategoryInfoV2 ci, BrandInfoV2 pb,ProductCityInfoV2 pc, ProductInfoV2 pi, "
			+ " PaymentMode pm,TimeSlot t where o.Id = od.orderId and c.Id = o.customerId and "
			//+ "c.addressId = a.Id and  "
			+ " o.AddressId = a.Id and  "
			+ " od.productId = pc.Id and "
			+ " pc.productId = pi.Id and pi.brandId = pb.Id and pc.categoryId = ci.Id and "
			+ " pm.Id =( IF(od.paymentModeId>0 , od.paymentModeId,  if(o.paymentModeId is null,0,o.paymentModeId)) ) "
			+ " and o.cancelled = 0 and od.orderStatus != 'CANCELLED' and t.Id=o.timeSlotId "
			+ " and o.deliveryDate BETWEEN :fromDate AND :toDate " + " and t.Id =:timeSlotId "
			+ "order by a.City,a.Project,a.Tower, o.deliveryDate, t.StartTime, c.FullName";
	SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
	query.setParameter("fromDate", fromDate);
	query.setParameter("toDate", toDate);
	query.setParameter("timeSlotId", timeSlotId);
	return query.list();
}

	@Override
	public List<String> getTimeSlotStringByCustomerDeliveryDate(Long customerId, Date deliveryDate) {
		String namedQuery = "OrderInfo.getTimeSlotStringByCustomerDeliveryDate";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate));
	}

	@Override
	public List<String> getTimeSlotStringByCustomerDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot) {
		String namedQuery = "OrderInfo.getTimeSlotStringByCustomerDeliveryDateAndTimeSlot";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("timeSlot", timeSlot));
	}

	@Override
	@Transactional(rollbackFor =Exception.class, readOnly = true)
	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlotAndCityCode(Long customerId, Date deliveryDate,
			String timeSlot, String cityCode) {
		String namedQuery = "OrderInfo.getByCustomerIdDeliveryDateTimeSlotAndCityCode";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId),
				new Param("deliveryDate", deliveryDate), new Param("timeSlot", timeSlot), new Param("cityCode", cityCode));
	}

	@Override
	public List<OrderInfoDTO> getOrders(Long customerId) {
		String namedQuery = "OrderInfo.getCheckoutOrders";
		return this.getlist(namedQuery, OrderInfoDTO.class, -1, new Param("customerId", customerId));
	}

	@Override
	public List<UpcomingOrderDTO> getOrderDetails(Long customerId) {
		String namedQuery = "Select distinct oi.Id, od.Id as orderDetailsId, od.ProductId, b.brand, pi.ProductName, pc.QuantityUnit, pc.CategoryId, "
				+ " pc.SubCategoryId, od.Quantity, od.ProductSalePrice, od.deliveryCharges, od.totalProductCost, pc.StockQuantity,"
				+ " pc.StockStatus,pc.Active,oi.Version,oi.DeliveryDate, oi.TimeSlotId,pc.salePrice,pc.deliveryCharge from OrderInfo oi, OrderDetailsInfo od, BrandInfoV2 b,"
				+ " ProductCityInfoV2 pc, ProductInfoV2 pi where oi.Id=od.orderId and od.productId =pc.Id"
				+ " and pc.productId = pi.Id and pi.brandId =b.Id and oi.customerId=:customerId and od.OrderStatus='CONFIRMED' and oi.ScheduleId is null"
				//+ " and oi.deliveryDate=:deliveryDate and oi.timeSlotId=:timeSlotId "
				+ " order by oi.deliveryDate,oi.TimeSlotId  ";
	SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
	query.setParameter("customerId", customerId);
	//query.setParameter("deliveryDate", deliveryDate);
	//query.setParameter("timeSlotId", timeSlotId);
	List<Object[]> list = query.list();
	List<UpcomingOrderDTO> orderDetailList = new ArrayList<>();
	list.stream().forEach((record) -> {
		UpcomingOrderDTO orderDetails = new UpcomingOrderDTO();
		orderDetails.setOrderId(Long.valueOf(record[0].toString()));
		orderDetails.setOrderDetailsId(Long.valueOf(record[1].toString()));
		orderDetails.setProductDetailsId(Long.valueOf(record[2].toString()));
		orderDetails.setBrandName(String.valueOf(record[3].toString()));
		orderDetails.setProductName(String.valueOf(record[4].toString()));
		orderDetails.setProductUnit(String.valueOf(record[5].toString()));
		orderDetails.setCategoryId(Integer.valueOf(record[6].toString()));
		orderDetails.setSubCategoryId(Integer.valueOf(record[7].toString()));
		orderDetails.setQuantity(Integer.valueOf(record[8].toString()));
		orderDetails.setSalePrice(Float.valueOf(record[9].toString()));
		orderDetails.setDeliveryCharge(Float.valueOf(record[10].toString()));
		orderDetails.setProductCost(Float.valueOf(record[11].toString()));
		orderDetails.setStockQuantity(Integer.valueOf(record[12].toString()));
		orderDetails.setStockStatus(Boolean.valueOf(record[13].toString()));
		orderDetails.setActive(Boolean.valueOf(record[14].toString()));
		//orderDetails.setVersion(String.valueOf(record[15].toString()));
		orderDetails.setDeliveryDate(Date.valueOf(record[16].toString()));
		orderDetails.setTimeSlotId(Integer.valueOf(record[17].toString()));
		orderDetails.setProductSalePrice(Float.valueOf(record[18].toString()));
		orderDetails.setProductDeliveryCharge(Float.valueOf(record[19].toString()));
		orderDetailList.add(orderDetails);
	});
	System.out.println(orderDetailList);
	return orderDetailList;
}

	@Override
	public List<UpcomingOrderDTO> getAllOrderDetails(Long customerId) {
		String namedQuery = "Select distinct oi.Id, od.Id as orderDetailsId, od.ProductId, b.brand, pi.ProductName, pc.QuantityUnit, pc.CategoryId, "
				+ " pc.SubCategoryId, od.Quantity, od.ProductSalePrice, od.deliveryCharges, od.totalProductCost, pc.StockQuantity,"
				+ " pc.StockStatus,pc.Active,oi.Version,oi.DeliveryDate, oi.TimeSlotId,pc.salePrice,pc.deliveryCharge from OrderInfo oi, OrderDetailsInfo od, BrandInfoV2 b,"
				+ " ProductCityInfoV2 pc, ProductInfoV2 pi where oi.Id=od.orderId and od.productId =pc.Id"
				+ " and pc.productId = pi.Id and pi.brandId =b.Id and oi.customerId=:customerId and od.OrderStatus='CONFIRMED' "
				//+ " and oi.deliveryDate=:deliveryDate and oi.timeSlotId=:timeSlotId "
				+ " order by oi.deliveryDate,oi.TimeSlotId  ";
	SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
	query.setParameter("customerId", customerId);
	//query.setParameter("deliveryDate", deliveryDate);
	//query.setParameter("timeSlotId", timeSlotId);
	List<Object[]> list = query.list();
	List<UpcomingOrderDTO> orderDetailList = new ArrayList<>();
	list.stream().forEach((record) -> {
		UpcomingOrderDTO orderDetails = new UpcomingOrderDTO();
		orderDetails.setOrderId(Long.valueOf(record[0].toString()));
		orderDetails.setOrderDetailsId(Long.valueOf(record[1].toString()));
		orderDetails.setProductDetailsId(Long.valueOf(record[2].toString()));
		orderDetails.setBrandName(String.valueOf(record[3].toString()));
		orderDetails.setProductName(String.valueOf(record[4].toString()));
		orderDetails.setProductUnit(String.valueOf(record[5].toString()));
		orderDetails.setCategoryId(Integer.valueOf(record[6].toString()));
		orderDetails.setSubCategoryId(Integer.valueOf(record[7].toString()));
		orderDetails.setQuantity(Integer.valueOf(record[8].toString()));
		orderDetails.setSalePrice(Float.valueOf(record[9].toString()));
		orderDetails.setDeliveryCharge(Float.valueOf(record[10].toString()));
		orderDetails.setProductCost(Float.valueOf(record[11].toString()));
		orderDetails.setStockQuantity(Integer.valueOf(record[12].toString()));
		orderDetails.setStockStatus(Boolean.valueOf(record[13].toString()));
		orderDetails.setActive(Boolean.valueOf(record[14].toString()));
		//orderDetails.setVersion(String.valueOf(record[15].toString()));
		orderDetails.setDeliveryDate(Date.valueOf(record[16].toString()));
		orderDetails.setTimeSlotId(Integer.valueOf(record[17].toString()));
		orderDetails.setProductSalePrice(Float.valueOf(record[18].toString()));
		orderDetails.setProductDeliveryCharge(Float.valueOf(record[19].toString()));
		orderDetailList.add(orderDetails);
	});
	System.out.println(orderDetailList);
	return orderDetailList;
}
	
	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getPurchaseReport(Date fromDate, Date toDate, String joiner_TimeSlots, String categoryIdList) {
		
		/*String namedQuery = "SELECT  pr.Id, pr.productName, pr.quantityUnit, sum(od.Quantity) as quantity, pr.price as 'sellingPrice/Unit', ca.Category " 
				+ " From maidin.OrderInfo oi "
				+ " INNER JOIN maidin.OrderDetailsInfo od ON od.OrderId = oi.Id "
				+ " INNER JOIN maidin.Customer cu ON oi.customerId = cu.Id "
				+ " INNER JOIN maidin.Product pr ON od.productId = pr.Id "
				+ " INNER JOIN maidin.ProductBrands br ON pr.brandId = br.Id "  
				+ " INNER JOIN maidin.ProductCategory ca ON br.categoryId = ca.Id "
				+ " where oi.deliveryDate BETWEEN :fromDate AND :toDate "
				+ " and od.cancelled = false "
				+ " and ca.Id In ("+categoryIdList+") "
				+ " and oi.timeslot In ("+joiner_TimeSlots+") "
				+ " and cu.addressId != 19 "
				+ " group by pr.productName, pr.quantityUnit "
				+ " order by ca.Id, pr.productName " ;*/

		/*String namedQuery = "SELECT  pc.Id, p.productName, pc.quantityUnit, sum(od.Quantity) as quantity, pc.salePrice as 'sellingPrice/Unit', ca.Category " 
				+ " From maidin.OrderInfo oi "
				+ " INNER JOIN maidin.OrderDetailsInfo od ON od.OrderId = oi.Id "
				+ " INNER JOIN maidin.Customer cu ON oi.customerId = cu.Id "
				+ " INNER JOIN maidin.ProductCityInfoV2 pc ON od.productId = pc.Id "
				+ " INNER JOIN maidin.ProductInfoV2 p ON pc.productId = p.Id  "
				+ " INNER JOIN maidin.BrandInfoV2 br ON p.brandId = br.Id "  
				+ " INNER JOIN maidin.CategoryInfoV2 ca ON pc.categoryId = ca.Id "
				+ " where oi.deliveryDate BETWEEN :fromDate AND :toDate "
				+ " and od.cancelled = false "
				+ " and ca.Id In ("+categoryIdList+") "
				+ " and oi.timeslot In ("+joiner_TimeSlots+") "
				+ " and cu.addressId != 19 "
				+ " group by p.productName, pc.quantityUnit "
				+ " order by ca.Id, p.productName " ;*/
		String namedQuery= "SELECT  pc.Id, Concat(br.Brand, ' ', p.productName) as ProductName, pc.quantityUnit, sum(od.Quantity) as quantity, pc.salePrice as 'sellingPrice/Unit', ca.Category " 
				+ " From maidin.OrderInfo oi "
				+ " INNER JOIN maidin.OrderDetailsInfo od ON od.OrderId = oi.Id "
				+ " INNER JOIN maidin.Customer cu ON oi.customerId = cu.Id "
				+ " INNER JOIN maidin.ProductCityInfoV2 pc ON od.productId = pc.Id "
				+ " INNER JOIN maidin.ProductInfoV2 p ON pc.productId = p.Id  "
				+ " INNER JOIN maidin.BrandInfoV2 br ON p.brandId = br.Id "  
				+ " INNER JOIN maidin.CategoryInfoV2 ca ON pc.categoryId = ca.Id "
				+ " where oi.deliveryDate BETWEEN :fromDate AND :toDate "
				+ " and od.cancelled = false "
				+ " and ca.Id In ("+categoryIdList+") "
				+ " and oi.timeslot In ("+joiner_TimeSlots+") "
				+ " and cu.addressId != 19 "
				+ " group by Concat(br.Brand, ' ', p.productName), pc.quantityUnit "
				+ " order by ca.Id, p.productName ";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("fromDate", fromDate);
		query.setParameter("toDate", toDate);
//		query.setParameter("joiner_TimeSlots", joiner_TimeSlots);
//		query.setParameter("categoryIdList", categoryIdList);
		return query.list();
	}

	@Override
	public List<OrderInfoDTO> getByIds(List<Long> orderIdList) {
		String namedQuery = "OrderInfo.getByIds";
		return this.getColumnslist(namedQuery, OrderInfoDTO.class, -1, new Param("Ids",orderIdList));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getBalanceReport(String cityCodeList) {
		String namedQuery= "SELECT cu.FullName, cu.MobileNumber, wa.MyWalletBalance,ad.City, ad.Project, ad.Tower, ad.Flat"
				+ " FROM maidin.Wallet wa "
				+ " Inner Join maidin.Customer cu on cu.id = wa.CustomerId "
				+ " Inner Join maidin.Address ad on ad.id = cu.AddressId "
				+ " where " //cu.addressId != 19 and "
				+ " wa.MyWalletBalance !=0 and "
				+ " ad.CityCode In ("+cityCodeList+") "
				+ " order by wa.MyWalletBalance desc";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		return query.list();
	}
}
