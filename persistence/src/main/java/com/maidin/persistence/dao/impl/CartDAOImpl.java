package com.maidin.persistence.dao.impl;

import java.util.List;

import com.maidin.persistence.dao.CartDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dto.CartDTO;

public class CartDAOImpl extends CommonDAO implements CartDAO {

	@Override
	public Long addCart(CartDTO cartDTO) {
		return (Long)this.add(cartDTO);
	}

	@Override
	public CartDTO getByCustomerIdAndProductId(Long customerId, long productId) {
		String namedQuery = "Cart.getByCustomerIdAndProductId";
        return this.getQuery(namedQuery, CartDTO.class, new Param("customerId", customerId),
        		new Param("productId", productId));
	}

	@Override
	public void removeCart(CartDTO cartDTO) {
		this.delete(cartDTO);
		
	}

	@Override
	public List<CartDTO> getByCustomerId(Long customerId) {
		String namedQuery = "Cart.getByCustomerId";
		return this.list(namedQuery, CartDTO.class, new Param("customerId", customerId));
		
	}

	@Override
	public CartDTO getByCartId(Long Id) {
		String namedQuery = "Cart.getByCartId";
		return this.getQuery(namedQuery, CartDTO.class, new Param("Id", Id));
	}

	

	

	

	
}
