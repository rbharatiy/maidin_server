package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.TimeSlotDTO;

public interface TimeSlotDAO {

	public TimeSlotDTO getById(Integer id);

	public TimeSlotDTO getByTimeSlot(String timeSlot);


	public List<String> getByIds(List<Integer> timeSlotIdList);

	//public List<TimeSlotDTO> getAllTimeSlots(boolean b);

	public List<TimeSlotDTO> getAllAppTimeSlots(boolean b);

	public List<TimeSlotDTO> getAll();

	public List<Integer> getByStartTime(int hour);

	public List<TimeSlotDTO> getTimeSlotListByIds(List<Integer> timeSlotIdList);


	
	
	    
		
}
