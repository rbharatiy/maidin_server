package com.maidin.persistence.dao.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CartDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.WalletDAO;
import com.maidin.persistence.dto.CartDTO;
import com.maidin.persistence.dto.WalletDTO;

public class WalletDAOImpl extends CommonDAO implements WalletDAO {

	@Override
	public Long addBalance(WalletDTO walletDTO) {
		// TODO Auto-generated method stub
		return (Long)this.add(walletDTO);
	}

	@Override
	public void updateBalance(WalletDTO walletDTO) {
		this.update(walletDTO);
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public WalletDTO getByCustomerId(Long customerId) {
		String namedQuery = "Wallet.getByCustomerId";
		return this.getQuery(namedQuery,WalletDTO.class, new Param("customerId", customerId));
	}
	
	@Override
	public double getTotalMadinMoney() {
		String namedQuery = "Wallet.getTotalMadinMoney";
		return (Double)this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery).uniqueResult();
	}

	@Override
	public double getPositiveMadinMoney() {
		String namedQuery = "Wallet.getPositiveMadinMoney";
		return (Double)this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery).uniqueResult();
	}

	@Override
	public double getNegativeMadinMoney() {
		String namedQuery = "Wallet.getNegativeMadinMoney";
		return (Double)this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery).uniqueResult();
	}
	
}
