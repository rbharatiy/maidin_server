package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CustomerAuthenticationDAO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;

public class CustomerAuthenticationDAOImpl extends CommonDAO implements CustomerAuthenticationDAO {

	@Override
	public Long addToken(CustomerAuthenticationDTO customerAuthDTO) {
		return (Long)this.add(customerAuthDTO);
	}

	@Override
	public void updateToken(CustomerAuthenticationDTO customerAuthDTO) {
		this.update(customerAuthDTO);

	}

	@Override
	public CustomerAuthenticationDTO getByMobileNumber(String mobileNumber) {
		String namedQuery = "CustomerAuthentication.getByMobileNumber";
        return this.getQuery(namedQuery, CustomerAuthenticationDTO.class, new Param("mobileNumber", mobileNumber));
	}

}
