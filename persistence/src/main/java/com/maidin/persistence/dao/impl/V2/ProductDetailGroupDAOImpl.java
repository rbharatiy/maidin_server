package com.maidin.persistence.dao.impl.V2;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.V2.ProductDetailGroupDAO;
import com.maidin.persistence.dto.V2.ProductDetailGroupDTO;

public class ProductDetailGroupDAOImpl extends CommonDAO implements ProductDetailGroupDAO{

	/*@Override
	public List<BrandInfoV2DTO> getByBrandIds(Set<Integer> brandIds) {
		String namedQuery="BrandInfoV2.getByBrandIds";
		return this.list(namedQuery, BrandInfoV2DTO.class, new Param("Ids",brandIds));
	}

	@Override
	public BrandInfoV2DTO getByBrandId(Integer brandId) {
		String namedQuery="BrandInfoV2.getById";
		return this.getQuery(namedQuery, BrandInfoV2DTO.class, new Param("Id",brandId));
	}
*/
	@Override
	public ProductDetailGroupDTO getById(long Id) {
		String namedQuery = "ProductDetailGroup.getById";
		return this.getQuery(namedQuery, ProductDetailGroupDTO.class, new Param("Id",Id));
	}

	@Override
	public Long addProductDetailGroupInfo(ProductDetailGroupDTO productDetailGroupDTO) {
		return (Long)this.add(productDetailGroupDTO);
		
	}

	@Override
	public void updateProductDetailGroupInfo(ProductDetailGroupDTO pDetailGroupDTO) {
		this.update(pDetailGroupDTO);
		
	}
	}
