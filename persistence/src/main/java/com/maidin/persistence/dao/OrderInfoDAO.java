package com.maidin.persistence.dao;

import java.sql.Date;
import java.util.List;

import com.maidin.persistence.dto.OrderInfoDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.V2.PreviousOrderInfoDTO;
import com.maidin.persistence.dto.V2.UpcomingOrderDTO;

public interface OrderInfoDAO {

	public Long addOrderInfo(OrderInfoDTO orderDTO);

	public OrderInfoDTO getById(Long orderId);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot);

	public List<Date> getDifferentDeliveryDateByCustomerId(Long customerId);

	public List<OrderInfoDTO> getByCustomerIdAndDeliveryDate(Long customerId, Date expectedDeliveryDate);

	public List<Date> getByCustomerIdAndLatestPastOrder(Long customerId, int maxResults, String paymentStatus);

	public List<Date> getPastDaysDeliveryByPagination(Long id, String name, int firstResult, int elementsPerPage);

	public List<Date> getDifferentDeliveryDatesByTimestamp(Date fromTimestamp, Date toTimestamp);

	public List<Date> getByCustomerDifferentDeliveryDates(Long customerId, Date fromTimestamp, Date toTimestamp);

	public List<OrderInfoDTO> getByDeliveryDates(Date date);

	public List<OrderInfoDTO> getAllScheduledOrders(Long customerId);

	public void deleteOrderInfo(OrderInfoDTO orderInfo);

	public List<Long> getDifferentCustomersByTimestamp(Date fromDate, Date toDate);

	public List<OrderInfoDTO> getByCustomerId(Long customerId);

	public List<Long> getDifferentCustomersByTimestampAndTimeSlotId(Date fromDate, Date toDate, Integer timeslotId);

	public List<OrderInfoDTO> getByCustomerIdAndTimeSlot(Long customerId, Integer timeSlot);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDate(Long customerId, Date fromDate, Date toDate);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlot(Long customerId, Date fromDate, Date toDate,
			Integer timeSlotId);

	public List<Date> getByDeliveryDateAndTimeSlot(Date date, Date date2, String timeslot);

	/*
	 * public List<OrderInfoDTO> getByDeliveryDateAndTimeSlotByFlat(String
	 * flatName, Date date, Date date2, String timeslot);
	 */

	/*
	 * public List<OrderInfoDTO> getByDeliveryDateAndTimeSlotByTowerFlat(String
	 * towerName, String flatName, Date date, Date date2, String timeslot);
	 * 
	 * public List<OrderInfoDTO>
	 * getByDeliveryDateAndTimeSlotByProjectTowerFlat(String projectName, String
	 * towerName, String flatName, Date date, Date date2, String timeslot);
	 * 
	 * public List<OrderInfoDTO>
	 * getByDeliveryDateAndTimeSlotByCityProjectTowerFlat(String cityName,
	 * String projectName, String towerName, String flatName, Date date, Date
	 * date2, String timeslot);
	 */

	public List<Date> getByDeliveryDateAndTimeSlotByCity(String cityName, Date date, Date date2, String string);

	public List<Date> getByDeliveryDateAndTimeSlotByCityProject(String cityName, String projectName, Date date,
			Date date2, String string);

	public List<Date> getByDeliveryDateAndTimeSlotByCityProjectTower(String cityName, String projectName,
			String towerName, Date date, Date date2, String string);

	public List<Date> getByDeliveryDateAndTimeSlotByCityProjectTowerFlat(String cityName, String projectName,
			String towerName, String flatName, Date date, Date date2, String string);

	public List<Long> getDifferentCustomersByDeliveryDate(Date deliveryDate);

	public List<Long> getDifferentCustomersByDeliveryDateAndTimeSlot(Date deliveryDate, String string);
	////////////////////////////////////////////////////////////////////////////////////////

	public List<OrderInfoDTO> getCustomersByCPTDeliveryDateAndTimeSlot(String cityName, String projectName, String towerName,
			Date deliveryDate, String timeSlot);

	public List<OrderInfoDTO> getCustomersByCPTDeliveryDate(String cityName, String projectName, String towerName,
			Date deliveryDate);

	public List<OrderInfoDTO> getCustomersByCPDeliveryDateAndTimeSlotId(String cityName, String projectName, Date deliveryDate,
			int timeSlotId);

	public List<OrderInfoDTO> getCustomersByCPDeliveryDate(String cityName, String projectName, Date deliveryDate);

	/*
	 * public List<Integer>
	 * getTimeSlotsByCustomerCPTDeliveryDateAndTimeSlot(Long customerId, String
	 * cityName, String projectName, String towerName, long deliveryDate, String
	 * timeSlot);
	 * 
	 * public List<Integer> getTimeSlotsByCustomerCPTDeliveryDate(Long
	 * customerId, String cityName, String projectName, String towerName, Date
	 * deliveryDate);
	 * 
	 * public List<Integer> getTimeSlotsByCustomerCPDeliveryDateAndTimeSlot(Long
	 * customerId, String cityName, String projectName, Date deliveryDate,
	 * String timeSlot);
	 * 
	 * public List<Integer> getTimeSlotsByCustomerCPDeliveryDate(Long
	 * customerId, String cityName, String projectName, Date deliveryDate);
	 */
	public List<Integer> getTimeSlotsByCustomerDeliveryDateAndTimeSlotId(Long customerId, Date deliveryDate,
			int timeSlotId);

	public List<Integer> getTimeSlotByCustomerDeliveryDate(Long customerId, Date deliveryDate);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlotId(Long customerId, Date deliveryDate,
			Integer timeSlotId);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateWithVegetableCategory(Long customerId,
			Date expectedDeliveryDate, int i);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateWithOtherCategory(Long customerId, Date expectedDeliveryDate,
			int i);

	// public List<OrderInfoDTO> getNextOrder();

	// public List<OrderInfoDTO> getUpcomingOrder(Long customerId);

	List<Long> getCheckoutIdsOfUpcomingCurrentDayTimeSlots(long customerId, int lastLockingTime);

	public List<Integer> getTimeSlotsByCustomerIdAndDeliveryDate(Long customerId, Date firstOrderDate);

	public List<Date> getNextOrderDate(Long customerId);

	public List<Date> getPastOrderDates(Long customerId);

	public List<Integer> getCheckoutTimeSlotsByIds(List<Long> todaysCheckoutList);

	/*
	 * public OrderInfoDTO getByScheduleIdAndDeliveryDate(Long id, Date
	 * deliveryDate);
	 */

	public List<OrderInfoDTO> getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId(Long customerId, Date dateValue,
			Integer timeSlotId);

	public List<Integer> getCheckoutTimeSlotByCustomerDeliveryDate(Long customerId, Date nextCheckoutDay);

	public OrderInfoDTO getByScheduleIdDeliveryDate(Long scheduleId, Date deliveryDate);

	public List<OrderInfoDTO> getOrdersByScheduleId(Long scheduleId);

	public List<OrderInfoDTO> getScheduleOrdersByTimeSlots(List<Integer> timeSlotIdList);

	List<OrderInfoDTO> getByDeliveryDate(Long excludeAddressId);

	// for multiple day reports sorted by delivery date (by Mudit)

	// all timeslots
	public List<Date> getCustomersSortedByDeliveryDate(Date fromDate, Date toDate);

	// single timeslot
	public List<Date> getCustomersInTimeSlotSortedByDeliveryDate(Date fromDate, Date toDate, Integer timeslotId);

	// By Pushpinder
	long getTotalCountByDeliveryDate(Long excludeAddressId, String date);

	long getTotalMarkedDeliveries(Long excludeAddressId, String date);

	long getTotalCountByDeliveryDateExcludingUndelivered(Long excludeAddressId, String date);

	public List<Object[]> getCustomerOrdersByDate(Date fromDate, Date toDate);

	public List<Object[]> getCustomerOrdersByDateAndSlot(Date fromDate, Date toDate, String timeSlot);

	public List<PreviousOrderInfoDTO> getPreviousOrders(Long customerId,String orderStatus, boolean isCancelled, int firstResult, int maxResult);

	public Long  getTotalOrderCount(Long id, int orderStausId, boolean cancelled);

	public List<OrderInfoDTO> getOrdersByDateAndCityCode(Date date, Date date2, String cityCode);

	public List<OrderInfoDTO> getOrdersByDate(Date date, Date date2);

	public List<Object[]> getCustomerOrdersByDateSlotAndCityCode(Date date, Date date2, String timeSlot, String cityCode);

	public List<Object[]> getCustomerOrdersByDateAndCityCode(Date date, Date date2, String cityCode);

	public void updateOrderInfo(OrderInfoDTO orderInfo);

	public List<OrderInfoDTO> getCustomersByCPDeliveryDateAndTimeSlot(String cityName, String projectName, Date deliveryDate,
			String timeSlot);

	public List<Integer> getTimeSlotsByCustomerDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot);

	public List<Long> getDifferentCustomersByTimestampAndTimeSlot(Date fromDate, Date toDate, String timeSlot);

	public List<Object[]> getCustomerOrdersByDateAndTimeSlotId(Date fromDate, Date toDate, int timeSlot);

	public List<String> getTimeSlotStringByCustomerDeliveryDate(Long customerId, Date deliveryDate);

	public List<String> getTimeSlotStringByCustomerDeliveryDateAndTimeSlot(Long customerId, Date deliveryDate,
			String timeSlot);

	public List<OrderInfoDTO> getByCustomerIdDeliveryDateAndTimeSlotAndCityCode(Long customerId, Date deliveryDate,
			String timeSlot, String cityCode);

	public List<OrderInfoDTO> getOrders(Long customerId);

	//public List<UpcomingOrderDTO> getOrderDetails(Long customerId, Date deliveryDate, Integer timeSlotId);
	public List<UpcomingOrderDTO> getOrderDetails(Long customerId);

	

	//By Pushpinder
	public List<Object[]> getPurchaseReport(Date fromDate, Date toDate, String joiner_TimeSlots, String categoryIdList);

	List<UpcomingOrderDTO> getAllOrderDetails(Long customerId);

	public List<OrderInfoDTO> getByIds(List<Long> orderIdList);

	public List<Object[]> getBalanceReport(String string);

	public List<PreviousOrderInfoDTO> getOrderById(Long orderId);
}
