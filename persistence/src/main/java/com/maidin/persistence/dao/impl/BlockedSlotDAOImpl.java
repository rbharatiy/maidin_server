package com.maidin.persistence.dao.impl;

import java.sql.Date;
import java.util.List;

import com.maidin.persistence.dao.BlockedSlotDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dto.BlockedSlotDTO;

public class BlockedSlotDAOImpl extends CommonDAO implements BlockedSlotDAO {

	@Override
	public List<BlockedSlotDTO> getByBlockedDate(Date blockedDate) {
		String namedQuery ="BlockedSlotInfo.getByBlockedDate";
		return this.list(namedQuery, BlockedSlotDTO.class, new Param("blockedDate",blockedDate));
	}

	
	
	
	
}
