package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.PopularSearchesDTO;
import com.maidin.persistence.dto.V2.SearchQueryDTO;

public interface PopularSearchesDAO {
	Long addKeyWord(PopularSearchesDTO popularSearchesDTO);

	void updateKeyWord(PopularSearchesDTO popularSearchesDTO);

	List<PopularSearchesDTO> getActiveKeywords();

	List<PopularSearchesDTO> getAllKeywords();

}
