package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.ProductInfoV2DAO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;

public class ProductInfoV2DAOImpl extends CommonDAO implements ProductInfoV2DAO {

	@Override
	public List<ProductInfoV2DTO> getByProductIds(List<Integer> productIdList, int firstResult, int elementsPerPage) {
		String namedQuery = "ProductInfoV2.getByProductIds";
		return this.listByCriteria(namedQuery, ProductInfoV2DTO.class, firstResult, elementsPerPage,
				new Param("Ids", productIdList));
	}

	@Override
	public ProductInfoV2DTO getById(Integer productId, boolean active) {
		String namedQuery = "ProductInfoV2.getById";
		return this.getQuery(namedQuery, ProductInfoV2DTO.class, new Param("Id", productId),
				new Param("active", active));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductInfoV2DTO getByIdWithoutActiveState(Integer productId) {
		String namedQuery = "ProductInfoV2.getByIdWithoutActiveStatus";
		return this.getQuery(namedQuery, ProductInfoV2DTO.class, new Param("Id", productId));
	}

	@Override
	public List<ProductInfoV2DTO> getAllMatchingProducts(String productSubString) {
		String namedQuery = "ProductInfoV2.getByProductName";
		return this.list(namedQuery, ProductInfoV2DTO.class,
				new Param("productSubString", "%" + productSubString + "%"));
	}

	@Override
	public Integer addProductInfo(ProductInfoV2DTO productDTO) {
		return (Integer) this.add(productDTO);
	}

	@Override
	public void updateProductInfo(ProductInfoV2DTO productDTO) {
		this.update(productDTO);
	}

	@Override
	public List<ProductInfoV2DTO> getAllMatchingProductsByBrand(String productSubString) {
		String namedQuery = "ProductInfoV2.getByBrandName";
		/*
		 * return this.list(namedQuery, ProductInfoV2DTO.class, new
		 * Param("productSubString", "%" + productSubString + "%"));
		 */
		return this.list(namedQuery, ProductInfoV2DTO.class, new Param("productSubString", productSubString));
	}

	@Override
	public void updateProduct(ProductInfoV2DTO productDTO) {
		this.update(productDTO);

	}

	@Override
	public List<ProductInfoV2DTO> getAllMatchingProductsWithActivationStatus(String productSubString, boolean active) {
		String namedQuery = "ProductInfoV2.getByProductNameWithActivationStatus";
		return this.list(namedQuery, ProductInfoV2DTO.class,
				new Param("productSubString", "%" + productSubString + "%"), new Param("active", active));
		/*
		 * return this.list(namedQuery, ProductInfoV2DTO.class, new
		 * Param("productSubString", productSubString), new Param("active",active));
		 */
	}

	@Override
	public List<ProductInfoV2DTO> getActiveProductsBySubCategoryName(String subString, boolean active,
			String cityCode) {
		int activeValue = active ? 1 : 0;
		String namedQuery = "SELECT DISTINCT p.Id, p.ProductCode, p.ProductName, p.Description, "
				+ "p.BrandId, p.Priority, p.FoodType, p.Active, p.StockStatus, "
				+ "p.CreatedOn, p.CreatedBy, p.UpdatedOn, p.UpdatedBy  FROM ProductInfoV2 p "
				+ "INNER JOIN ProductCityInfoV2 pc ON p.Id = pc.productId "
				+ "INNER JOIN SubCategoryInfoV2 s ON pc.subCategoryId = s.Id " 
				+ "WHERE s.subCategory LIKE :subString "
				+ "AND pc.cityCode = :cityCode " 
				+ "AND pc.active = :activeValue "
				+ "AND pc.stockStatus = :activeValue "
//				+ "ORDER BY p.productName "
				+ "UNION " 
				+ "SELECT pi.Id, pi.ProductCode, pi.ProductName, pi.Description, pi.BrandId, "
				+ "pi.Priority, pi.FoodType, pi.Active, pi.StockStatus, pi.CreatedOn, pi.CreatedBy, "
				+ "pi.UpdatedOn, pi.UpdatedBy FROM ProductInfoV2 pi, BrandInfoV2 b " 
				+ "WHERE pi.brandId = b.Id "
				+ "AND CONCAT(b.brand, ' ', pi.productName) LIKE :subString " 
				+ "AND pi.active=:activeValue ";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductInfoV2DTO.class);
		query.setParameter("subString", "%" + subString + "%");
		query.setParameter("activeValue", activeValue);
		query.setParameter("cityCode", cityCode);
		List<ProductInfoV2DTO> list = query.list();
		return list;
//		return this.list(namedQuery, ProductInfoV2DTO.class, new Param("subString", "%" + subString + "%"),
//				new Param("active", active), new Param("cityCode", cityCode));
	}

	@Override
	public List<ProductInfoV2DTO> getAllMatchingProductsByBrandWithActivationStatus(String productSubString,
			boolean active) {
		String namedQuery = "ProductInfoV2.getByBrandNameWithActivationStatus";
		/*
		 * return this.list(namedQuery, ProductInfoV2DTO.class, new
		 * Param("productSubString", "%" + productSubString + "%"), new
		 * Param("active",active));
		 */
		return this.list(namedQuery, ProductInfoV2DTO.class, new Param("productSubString", productSubString),
				new Param("active", active));
	}

	@Override
	public List<Integer> getByIds(List<Integer> productIdList) {
		String namedQuery = "ProductInfoV2.getByIdsWithActiveStatus";
		return this.getColumnslist(namedQuery, ProductInfoV2DTO.class, -1, new Param("Ids", productIdList));
	}

	@Override
	public List<ProductInfoV2DTO> getByProductIdsWithoutActiveState(List<Integer> productIdList, int firstResult,
			int elementsPerPage) {
		String namedQuery = "ProductInfoV2.getByProductIdsWithoutActiveState";
		return this.listByCriteria(namedQuery, ProductInfoV2DTO.class, firstResult, elementsPerPage,
				new Param("Ids", productIdList));

	}

	@Override
	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active) {
		String namedQuery = "ProductInfoV2.getByTextWithActivationStatus";
		return this.getColumnslist(namedQuery, ProductInfoV2DTO.class, -1,
				new Param("productSubString", "%" + productSubString + "%"), new Param("active", active));
	}

	@Override
	public ProductInfoV2DTO getByProductNameAndBrandId(String productName, int brandId) {
		String namedQuery = "ProductInfoV2.getByProductNameAndBrandId";
		return this.getQuery(namedQuery, ProductInfoV2DTO.class, new Param("productName", productName),
				new Param("brandId", brandId));
	}

	@Override
	public List<ProductInfoV2DTO> getActiveProductsForSuggestions(String subString, boolean active, String cityCode
			, int resultsCount) {
		int activeValue = active ? 1 : 0;
		String namedQuery = "SELECT pi.Id, pi.ProductCode, pi.ProductName, pi.Description, pi.BrandId, "
				+ "pi.Priority, pi.FoodType, pi.Active, pi.StockStatus, pi.CreatedOn, pi.CreatedBy, "
				+ "pi.UpdatedOn, pi.UpdatedBy FROM ProductInfoV2 pi, BrandInfoV2 b " 
				+ "WHERE pi.brandId = b.Id "
				+ "AND pi.Id in (SELECT productId FROM ProductCityInfoV2 p, CategoryInfoV2 c, SubCategoryInfoV2 s "
				+ "WHERE cityCode =:cityCode "
				+ "AND p.subCategoryId = s.Id "
				+ "AND p.categoryId = c.Id "
				+ "AND s.active = true "
				+ "AND c.active = true "
				+ "AND pi.Id = productId AND p.active =:activeValue ) "
				+ "AND CONCAT(b.brand, ' ', pi.productName) LIKE :subString " 
				+ "AND pi.active=:activeValue "
				+ "LIMIT :resultsCount";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductInfoV2DTO.class);
		query.setParameter("subString", "%" + subString + "%");
		query.setParameter("activeValue", activeValue);
		query.setParameter("cityCode", cityCode);
		query.setParameter("resultsCount", resultsCount);
		List<ProductInfoV2DTO> list = query.list();
		return list;
	}
}
