package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.V2.ProductCityInfoV2DAO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;

public class ProductCityInfoV2DAOImpl extends CommonDAO  implements ProductCityInfoV2DAO{

	@Override
	public List<ProductCityInfoV2DTO> getByCityCodeAndSubCategoryId(String cityCode, int subCategoryId, boolean active) {
		String namedQuery="ProductCityInfoV2.getByCityCodeAndSubCategoryId";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("cityCode",cityCode), new Param("subCategoryId", subCategoryId),
				new Param("active",active));
	}

	@Override
	public List<ProductCityInfoV2DTO> getBySubCategoryIdsAndCityCode(List<Integer> subCategoryIds, String cityCode, boolean active) {
		String namedQuery="ProductCityInfoV2.getBySubCategoryIdsAndCityCode";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("Ids",subCategoryIds), new Param("cityCode",cityCode),
				new Param("active",active));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductCityInfoV2DTO getById(long productDetailsId, boolean active) {
		String namedQuery="ProductCityInfoV2.getById";
		return this.getQuery(namedQuery,ProductCityInfoV2DTO.class, new Param("Id",productDetailsId),
				new Param("active",active));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductCityInfoV2DTO getByIdWithoutActiveStatus(long productDetailsId) {
		String namedQuery="ProductCityInfoV2.getByIdWithoutActiveStatus";
		return this.getQuery(namedQuery,ProductCityInfoV2DTO.class, new Param("Id",productDetailsId));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByproductIdsAndCityCode(List<Integer> productIdList, String cityCode) {
		//change namedQuery if needed
		String namedQuery="ProductCityInfoV2.getByproductIdsAndCityCode";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productIds",productIdList), new Param("cityCode", cityCode));
	}
	
	@Override
	public List<ProductCityInfoV2DTO> getByproductIdsAndCityCodeWithActivationStatus(List<Integer> productIdList, String cityCode, boolean active) {
		//change namedQuery if needed
		String namedQuery="ProductCityInfoV2.getByproductIdsAndCityCodeWithActivationStatus";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productIds",productIdList), new Param("cityCode", cityCode)
				, new Param("active", active));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByproductIds(List<Integer> productIdList) {
		String namedQuery="ProductCityInfoV2.getByProductIds";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productIds",productIdList));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByProductDetailGroupId(Long productDetailGroupId) {
	String namedQuery="ProductCityInfoV2.getByProductDetailGroupId";
	return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productDetailGroupId",productDetailGroupId));
	}

	@Override
	public Long addProductInfo(ProductCityInfoV2DTO productDTO) {
		return (Long)this.add(productDTO);
	}

	@Override
	public ProductCityInfoV2DTO getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(int productId, String quantityUnit,
			int categoryId, int subCategoryId, String cityCode) {
		String namedQuery="ProductCityInfoV2.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode";
		return this.getQuery(namedQuery,ProductCityInfoV2DTO.class, new Param("productId",productId),
				new Param("quantityUnit",quantityUnit), new Param("categoryId",categoryId), new Param("subCategoryId",subCategoryId), new Param("cityCode",cityCode));
	}

	@Override
	public void updateProductInfo(ProductCityInfoV2DTO pcCityInfoV2DTO) {
		this.update(pcCityInfoV2DTO);
		
	}

	@Override
	public void updateProductDetailGroup(List<Long> productDetailIds, Long productDetailGroupId) {
		String namedQuery = "ProductCityInfoV2.updateProductGroupId";
		this.updateQuery(namedQuery, new Param("productDetailIds", productDetailIds), new Param("productDetailGroupId", productDetailGroupId));
		
	}

	@Override
	public List<Long> getByProductIdAndCities(int productId, List<String> cities) {
		String namedQuery="ProductCityInfoV2.getByproductIdAndCities";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class, -1, new Param("productId",productId),
				new Param("cities", cities));
	
	}

	@Override
	public void updateStatus(boolean active, boolean stockStatus, List<Long> productDetailIdList) {
		String namedQuery = "ProductCityInfoV2.updateStatus";
		this.updateQuery(namedQuery, new Param("active", active),
				new Param("stockStatus", stockStatus),
				new Param("productDetailIds", productDetailIdList));
		
	}

	@Override
	public List<ProductCityInfoV2DTO> getByProductDetailIds(List<Long> productDetailIds) {
		String namedQuery="ProductCityInfoV2.getByProductDetailIds";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productDetailIds",productDetailIds));
	}

	@Override
	public List<Long> getByProductIdAndCityCode(long productDetailId, int productId, String cityCode) {
		String namedQuery="ProductCityInfoV2.getByProductIdAndCityCode";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class,-1, new Param("Id",productDetailId),
				new Param("productId",productId), new Param("cityCode",cityCode));
	}

	@Override
	public void updateSetByDefault(List<Long> productDetailIdList, boolean setByDefault) {
		String namedQuery = "ProductCityInfoV2.updateSetByDefault";
		this.updateQuery(namedQuery, new Param("productDetailIds", productDetailIdList), new Param("setByDefault", setByDefault));
		
	}

	@Override
	public List<ProductCityInfoV2DTO> getByCategoryIdAndSubCategoryId(int categoryId, Integer subCategoryId) {
		String namedQuery="ProductCityInfoV2.getByCategoryIdAndSubCategoryId";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("categoryId",categoryId),
				new Param("subCategoryId",subCategoryId));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByProductIdGroupByCity(Integer productId) {
		String namedQuery="ProductCityInfoV2.getByProductIdGroupByCity";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productId",productId));
	}

	@Override
	public List<Long> getByProductIdQuantityUnitAndCities(Integer productId, String quantityUnit, List<String> cities) {
		String namedQuery="ProductCityInfoV2.getByProductIdQuantityUnitAndCities";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class, -1, new Param("productId",productId),
				new Param("quantityUnit",quantityUnit),
				new Param("cities", cities));
	}

	@Override
	public List<Long> getIdsBySubCategoryIdAndCities(Integer subCategoryId,
			List<String> cities, boolean active) {
		String namedQuery="ProductCityInfoV2.getIdsBySubCategoryIdAndCities";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class,-1, new Param("subCategoryId",subCategoryId), new Param("cities",cities),
				new Param("active",active));
	}

	@Override
	public List<Long> getIdsByCategoryIdAndCities(int categoryId, List<String> cities, boolean active) {
		String namedQuery="ProductCityInfoV2.getIdsByCategoryIdAndCities";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class,-1, new Param("categoryId",categoryId), new Param("cities",cities),
				new Param("active",active));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByCityCodeAndSubCategoryIdWithoutActiveStatus(String cityCode,
			int subCategoryId) {
		String namedQuery="ProductCityInfoV2.getByCityCodeAndSubCategoryIdWithoutActiveStatus";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("cityCode",cityCode), new Param("subCategoryId", subCategoryId));
	}

	@Override
	public List<ProductCityInfoV2DTO> getBySubCategoryIdsAndCityCodeWithoutActiveState(
			List<Integer> subCategoryIds, String cityCode) {
		String namedQuery="ProductCityInfoV2.getBySubCategoryIdsAndCityCodeWithoutActiveState";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("subCategoryIds",subCategoryIds), new Param("cityCode", cityCode));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByProductIdQuantityUnit(Integer productId, String quantityUnit) {
		String namedQuery="ProductCityInfoV2.getByProductIdQuantityUnit";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productId",productId), new Param("quantityUnit", quantityUnit));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByMatchingIdsAndCityCode(List<Integer> categoryIdList,
			List<Integer> subCategoryIdList, List<Integer> brandIdList, List<Integer> productIdList, String cityCode) {
		String namedQuery="ProductCityInfoV2.getByMatchingIdsAndCityCode";
		/*String namedQuery = "SELECT b.* FROM maidin.ProductCityInfoV2 pc, ProductInfoV2 p where pc.productId = p.Id and "
				+ " pc.categoryId in (1) and pc.SubCategoryId in  (2) and productId in (3) and brandId in (4)"
				+ " cityCode=:cityCode and pc.active=true and p.active=true";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("customerId", customerId);
		//query.setParameter("deliveryDate", deliveryDate);
		//query.setParameter("timeSlotId", timeSlotId);
		return query.list();		*/
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("categoryIdList",categoryIdList), new Param("subCategoryIdList", subCategoryIdList),
				 new Param("brandIdList",brandIdList), new Param("productIdList",productIdList), new Param("cityCode",cityCode));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByPhotoId(Long photoId) {
		String namedQuery="ProductCityInfoV2.getByPhotoId";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("photoId", photoId));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByCityCode(String cityCode) {
		String namedQuery="ProductCityInfoV2.getByCityCodeWithoutActiveState";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("cityCode", cityCode));
	}

	@Override
	public List<ProductCityInfoV2DTO> getByProductIdQuantityUnitAndCityCode(Integer productId, String quantityUnit,
			String cityCode) {
		String namedQuery="ProductCityInfoV2.getByProductIdQuantityUnitAndCityCode";
		return this.list(namedQuery, ProductCityInfoV2DTO.class, new Param("productId", productId) ,new Param("quantityUnit", quantityUnit), new Param("cityCode", cityCode));
	}

	@Override
	public List<Long> getByProductIdQuantityUnitCategoryIdSubCategoryIdAndCities(Integer productId, String quantityUnit,
			Integer categoryId, Integer subCategoryId, List<String> cities) {
		String namedQuery="ProductCityInfoV2.getByProductIdQuantityUnitCategoryIdSubCategoryIdAndCities";
		return this.getColumnslist(namedQuery, ProductCityInfoV2DTO.class,-1, new Param("productId", productId) ,new Param("quantityUnit", quantityUnit), 
				new Param("categoryId",categoryId),
				new Param("subCategoryId",subCategoryId), new Param("cities", cities));
	}

	@Override
	public String getProductName(Integer productDetailId) {
		//String namedQuery = "Select concat(b.brand,' ',p.ProductName,' ',pc.QuantityUnit) from ProductCityInfoV2 pc, "
		String namedQuery = "Select concat(COALESCE(b.brand,''),' ',COALESCE(p.ProductName,''),' ',COALESCE(pc.QuantityUnit,'')) from ProductCityInfoV2 pc, "
				+ " ProductInfoV2 p, BrandInfoV2 b where pc.productId = p.Id and p.brandId = b.Id and pc.Id=:Id";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("Id", productDetailId);
		return (String) query.uniqueResult();
		
	}

	
	}
