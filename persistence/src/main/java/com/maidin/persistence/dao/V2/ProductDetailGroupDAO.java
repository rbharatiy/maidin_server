package com.maidin.persistence.dao.V2;

import java.util.List;
import java.util.Set;

import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductDetailGroupDTO;

public interface ProductDetailGroupDAO {

	     public ProductDetailGroupDTO getById(long Id);

		public Long addProductDetailGroupInfo(ProductDetailGroupDTO productDetailGroupDTO);

		public void updateProductDetailGroupInfo(ProductDetailGroupDTO pDetailGroupDTO);

}
