package com.maidin.persistence.dao.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.TimeSlotDAO;
import com.maidin.persistence.dto.TimeSlotDTO;

public class TimeSlotDAOImpl extends CommonDAO implements TimeSlotDAO {

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public TimeSlotDTO getByTimeSlot(String timeSlot) {
		String namedQuery = "TimeSlot.getByTimeSlot";
		return this.getQuery(namedQuery, TimeSlotDTO.class, new Param("timeSlot", timeSlot));
    }

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public TimeSlotDTO getById(Integer Id) {
		String namedQuery = "TimeSlot.getById";
        return this.getQuery(namedQuery, TimeSlotDTO.class, new Param("Id", Id));
	}

	@Override
	public List<String> getByIds(List<Integer> timeSlotIdList) {
		String namedQuery = "TimeSlot.getByIds";
        return this.getColumnslist(namedQuery, TimeSlotDTO.class,-1, new Param("Ids", timeSlotIdList));
	}

	public List<TimeSlotDTO> getAllAppTimeSlots(boolean displayedInApp) {
		String namedQuery = "TimeSlot.getAllAppTimeSlots";
        return this.list(namedQuery, TimeSlotDTO.class, new Param("displayedInApp", displayedInApp));
	}

	@Override
	public List<TimeSlotDTO> getAll() {
		String namedQuery = "TimeSlot.getAll";
        return this.list(namedQuery, TimeSlotDTO.class);

	}

	@Override
	public List<Integer> getByStartTime(int hour) {
		String namedQuery = "TimeSlot.getByStartTime";
        return this.getColumnslist(namedQuery, TimeSlotDTO.class,-1, new Param("StartTime", hour));
	}

	@Override
	public List<TimeSlotDTO> getTimeSlotListByIds(List<Integer> timeSlotIdList) {
		String namedQuery = "TimeSlot.getTimeSlotListByIds";
        return this.list(namedQuery, TimeSlotDTO.class, new Param("timeSlotIdList", timeSlotIdList));

	}

	
	
	    
		
}
