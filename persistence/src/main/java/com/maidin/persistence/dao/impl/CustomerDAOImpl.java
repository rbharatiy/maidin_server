package com.maidin.persistence.dao.impl;

import java.sql.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CustomerDAO;
import com.maidin.persistence.dto.CustomerDTO;
//import com.maidin.persistence.dto.OrderDTO;


public class CustomerDAOImpl extends CommonDAO implements CustomerDAO {

	@Override
	public CustomerDTO getByMobileNumberWithoutActiveState(String mobileNumber) {
		String namedQuery = "Customer.getByMobileNumberWithoutActiveState";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("mobileNumber", mobileNumber));
	}

	@Override
	public CustomerDTO getCustomerByMobileNumber(String mobileNumber) {
		String namedQuery = "Customer.getByMobileNumber";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("mobileNumber", mobileNumber));
	}

	@Override
	public Long addCustomerInfo(CustomerDTO CustomerDTO) {
		return (Long)this.add(CustomerDTO);
		
	}

	@Override
	public CustomerDTO getCustomerByEmail(String email) {
		String namedQuery = "Customer.getByEmail";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("email", email));
	}

	@Override
	public CustomerDTO getByIdAndMobileNumber(Long customerId, String mobileNumber) {
		String namedQuery = "Customer.getByIdAndMobileNumber";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("Id", customerId),
        		                                            new Param("mobileNumber", mobileNumber));
	}

	/*@Override
	public CustomerDTO getByCityProjectTowerFlatAndMobile(String city, String project, String tower, String flat, String mobile) {
		String namedQuery = "Customer.getByCityProjectTowerFlat";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("city", city),
        		                                            new Param("project", project),
        		                                            new Param("tower", tower),
        		                                            new Param("flat", flat),
        		                                            new Param("mobileNumber", mobile));
	}*/

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public CustomerDTO getById(Long customerId) {
		String namedQuery = "Customer.getById";
        return this.getQuery(namedQuery, CustomerDTO.class, new Param("Id", customerId));
	}

	/*@Override
	public void addCustomerInfo(CustomerDTO CustomerDTO) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public List<Long> getByAddressId(Long addressId) {
		String namedQuery = "Customer.getByAddressId";
        return this.getColumnslist(namedQuery, CustomerDTO.class,-1, new Param("addressId", addressId));
	}

	@Override
	public long getUsersByFlatCount(boolean active) {
		String namedQuery = "Customer.getUsersByFlatCount";
        /*return this.getlist(namedQuery, CustomerDTO.class,-1, new Param("active", active)).size();*/
		return  (Long)this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
	            .setParameter("active", active).uniqueResult();
	}

	@Override
	public long getRegisteredUserCount(boolean active) {
		String namedQuery = "Customer.getRegisteredUserCount";
        return  (Long)this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
	            .setParameter("active", active).uniqueResult();
	}

	@Override
	public List<CustomerDTO> getActiveUsersList(boolean active) {
		String namedQuery = "Customer.getActiveUsersList";
        return this.getlist(namedQuery, CustomerDTO.class,-1, new Param("active", active));
	}
	
	//By Pushpinder
//	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<Object[]> getNewCustomerRegistrations(int noOfdaysFromCurrent) {
		
		String namedQuery = "SELECT cu.FullName, cu.MobileNumber, ad.City, ad.Project, ad.Tower, ad.Flat, cu.Email, date_format(cu.CreatedOn, '%d %b %Y') as date  "
				+ "FROM maidin.Customer cu "
				+ "INNER JOIN maidin.Address ad on ad.Id = cu.AddressId "
				+ "Where cu.CreatedOn > curdate()-:noOfdaysFromCurrent "
				+ "Order by cu.CreatedOn, cu.FullName ";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("noOfdaysFromCurrent", noOfdaysFromCurrent);
		return query.list();
	}

}
