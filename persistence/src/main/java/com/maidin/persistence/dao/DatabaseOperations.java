package com.maidin.persistence.dao;

public enum DatabaseOperations {

    SAVE,
    
    UPDATE,
    
    DELETE,
    
    GET,
    
    EXECUTE_UPDATE, GET_CRITERIA
    
}
