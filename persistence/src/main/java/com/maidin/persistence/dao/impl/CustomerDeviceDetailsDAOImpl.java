package com.maidin.persistence.dao.impl;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CustomerDeviceDetailsDAO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;
import com.maidin.persistence.dto.CustomerDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;

public class CustomerDeviceDetailsDAOImpl extends CommonDAO implements CustomerDeviceDetailsDAO {

	public Long addDeviceDetails(CustomerDeviceDetailsDTO customerDto) {
		return (Long) this.add(customerDto);
	}

	public void updateDeviceDetails(CustomerDeviceDetailsDTO customerDto) {
		this.update(customerDto);
	}

	public CustomerDeviceDetailsDTO getDeviceDetailsByCustomerId(Long customerId) {
		String namedQuery = "CustomerDeviceDetails.getByCustomerId";
		return this.getQuery(namedQuery, CustomerDeviceDetailsDTO.class, new Param("customerId", customerId));
	}

	public CustomerDeviceDetailsDTO getDeviceDetailsById(Long Id) {
		String namedQuery = "CustomerDeviceDetails.getById";
		return this.getQuery(namedQuery, CustomerDeviceDetailsDTO.class, new Param("Id", Id));
	}

	@Override
	public List<CustomerDeviceDetailsDTO> listAllDevicesById() {
		String namedQuery = "CustomerDeviceDetails.listAllDevicesById";
		return this.list(namedQuery, CustomerDeviceDetailsDTO.class);
	}

	@Override
	public long getUsersCountByClientPlatform(String clientPlatform) {
		String namedQuery = "CustomerDeviceDetails.getPlatformUsersCount";
		return (Long) this.sessionFactory.getCurrentSession().getNamedQuery(namedQuery)
				.setParameter("devicePlatform", clientPlatform).uniqueResult();
	}

	@Override
	public List<String> getAllNotificationTokens(Long excludeId) {
		String namedQuery = "CustomerDeviceDetails.getAllNotificationTokens";
		return this.getColumnslist(namedQuery, CustomerDeviceDetailsDTO.class, -1, new Param("customerId", excludeId));
	}

}
