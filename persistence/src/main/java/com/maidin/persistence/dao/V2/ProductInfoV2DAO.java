package com.maidin.persistence.dao.V2;

import java.util.List;
import java.util.Set;

import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;

public interface ProductInfoV2DAO {

	List<ProductInfoV2DTO> getByProductIds(List<Integer> productIdList, int firstResult, int elementsPerPage);

	ProductInfoV2DTO getById(Integer productId, boolean active);

	ProductInfoV2DTO getByIdWithoutActiveState(Integer productId);

	List<ProductInfoV2DTO> getAllMatchingProducts(String productSubString);

	Integer addProductInfo(ProductInfoV2DTO productDTO);

	void updateProductInfo(ProductInfoV2DTO productDTO);

	List<ProductInfoV2DTO> getAllMatchingProductsByBrand(String productSubString);

	void updateProduct(ProductInfoV2DTO productDTO);

	List<ProductInfoV2DTO> getAllMatchingProductsWithActivationStatus(String productSubString, boolean active);

	List<ProductInfoV2DTO> getActiveProductsBySubCategoryName(String subString, boolean active, String cityCode);

	List<ProductInfoV2DTO> getAllMatchingProductsByBrandWithActivationStatus(String productSubString, boolean active);

	List<Integer> getByIds(List<Integer> productIdList);

	List<ProductInfoV2DTO> getByProductIdsWithoutActiveState(List<Integer> productIdList, int firstResult,
			int elementsPerPage);

	List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean b);

	ProductInfoV2DTO getByProductNameAndBrandId(String productName, int brandId);

	List<ProductInfoV2DTO> getActiveProductsForSuggestions(String subString, boolean active, String cityCode,
			int resultsCount);
}
