package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;

public interface CategoryInfoV2DAO {

	List<CategoryInfoV2DTO> getByIds(List<Integer> categoryIdList);

	List<CategoryInfoV2DTO> getAll();

	CategoryInfoV2DTO getById(Integer categoryId);

	//void updateCategory(CategoryInfoV2DTO categoryDTO);

	void updateCategoryInfo(CategoryInfoV2DTO categoryDTO);

	List<CategoryInfoV2DTO> getByIdsWithoutActiveState(List<Integer> categoryIdList);

	List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active);

	List<Integer> getActiveIdsByIds(List<Integer> categoryIdList);

	//List<CategoryInfoV2DTO> getAll(boolean active);

}
