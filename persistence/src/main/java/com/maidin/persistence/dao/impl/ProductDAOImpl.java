package com.maidin.persistence.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.ProductDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.ProductBrandsDTO;
import com.maidin.persistence.dto.ProductDTO;
import com.maidin.persistence.dto.ScheduleInfoDTO;
import com.maidin.persistence.dto.ProductDTO;

public class ProductDAOImpl extends CommonDAO implements ProductDAO {

	/*
	 * @Override public void addProduct(ProductDTO tripDTO) { this.add(tripDTO);
	 * }
	 */

	/*
	 * @Override public ProductDTO getById(Long Id) { String namedQuery =
	 * "Product.getById"; return this.getQuery(namedQuery, ProductDTO.class, new
	 * Param("Id", Id)); }
	 */

	/*
	 * @Override public void deleteInfo(ProductDTO dto) { this.delete(dto); }
	 */

	/*
	 * @Override public void updateProduct(ProductDTO tripDTO) {
	 * this.update(tripDTO); }
	 */

	@Override
	public List<ProductDTO> getAllProductsByBrandId(Integer brandId, boolean active) {
		/*
		 * String namedQuery = "Product.getAllProductsByBrandID"; return
		 * this.list(namedQuery, ProductDTO.class, new Param("brandId",
		 * brandId), new Param("active", active));
		 */
		String namedQuery = "SELECT * from Product p " + "where p.brandId=:brandId"
				+ " and p.active=true order by IF(p.productName LIKE 'Maidin%',1,2), p.productName,p.quantityUnit";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductDTO.class);
		query.setParameter("brandId", brandId);
		// query.setParameter("active", active);
		return query.list();
	}

	@Override
	public ProductDTO getByProductId(Integer productId) {
		String namedQuery = "Product.getByProductId";
		return this.getQuery(namedQuery, ProductDTO.class, new Param("Id", productId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductDTO getById(Integer productId) {
		String namedQuery = "Product.getById";
		return this.getQuery(namedQuery, ProductDTO.class, new Param("Id", productId));
	}

	@Override
	public void updateProduct(ProductDTO productDTO) {
		this.update(productDTO);

	}

	@Override
	public List<ProductDTO> getAllExistingProductsByBrandIdWithoutActiveState(Integer brandId) {
		/*
		 * String namedQuery =
		 * "Product.getAllExistingProductsByBrandIdWithoutActiveState"; return
		 * this.list(namedQuery, ProductDTO.class, new Param("brandId",
		 * brandId));
		 */
		String namedQuery = "SELECT * from Product p "
				+ "where p.brandId=:brandId order by IF(p.productName LIKE 'Maidin%',1,2), p.productName,p.quantityUnit";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductDTO.class);
		query.setParameter("brandId", brandId);
		// query.setParameter("active", active);
		return query.list();
	}

	public Integer addProduct(ProductDTO productDTO) {
		return (Integer) this.add(productDTO);
	}

	@Override
	public List<ProductDTO> getAllMatchingProducts(String productSubString) {
		String namedQuery = "Product.getByProductName";
		return this.list(namedQuery, ProductDTO.class, new Param("productSubString", "%" + productSubString + "%"));
	}

	@Override
	public List<ProductDTO> getAllActiveMatchingProducts(String productSubString) {
		String namedQuery = "Product.getOnlyActiveByProductName";
		return this.list(namedQuery, ProductDTO.class, new Param("productSubString", "%" + productSubString + "%"));
	}
}
