package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;

public interface CityCategoryInfoDAO {

	public CityCategoryInfoDTO getByCityCode(String cityCode);

	public List<CityCategoryInfoDTO> getByCityCodes(List<String> cities);

	public List<String> getByAllCities(boolean b);

	public List<CityCategoryInfoDTO> getAllCityCategories(boolean active);

	List<String> getAllCitiesWithoutActiveState();}
