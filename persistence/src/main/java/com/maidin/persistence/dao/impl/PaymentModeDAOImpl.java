package com.maidin.persistence.dao.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.PaymentModeDAO;
import com.maidin.persistence.dto.PaymentModeDTO;

public class PaymentModeDAOImpl extends CommonDAO implements PaymentModeDAO {

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public PaymentModeDTO getById(Integer Id){
		String namedQuery = "PaymentMode.getById";
        return this.getQuery(namedQuery, PaymentModeDTO.class, new Param("Id", Id));
    }

	@Override
	public PaymentModeDTO getByPaymentMode(String paymentMode) {
		String namedQuery = "PaymentMode.getByTimeSlot";
		return this.getQuery(namedQuery, PaymentModeDTO.class, new Param("paymentMode", paymentMode));
    }

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<PaymentModeDTO> getAll() {
		String namedQuery = "PaymentMode.getAll";
		return this.getlist(namedQuery, PaymentModeDTO.class,-1);
	}

	@Override
	public List<PaymentModeDTO> getAllPaymentMode(boolean displayedInApp, boolean active) {
		String namedQuery = "PaymentMode.getAllPaymentMode";
		return this.getlist(namedQuery, PaymentModeDTO.class,-1, new Param("displayedInApp",displayedInApp),
				 new Param("active",active));
	}

	
	
	    
		
}
