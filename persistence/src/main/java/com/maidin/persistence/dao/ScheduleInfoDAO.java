package com.maidin.persistence.dao;

import java.util.Date;
import java.util.List;

import com.maidin.persistence.dto.ScheduleInfoDTO;

public interface ScheduleInfoDAO {

	Long addScheduleInfo(ScheduleInfoDTO scheduleDTO);

	public ScheduleInfoDTO getById(long scheduleId);

	void updateScheduleInfo(ScheduleInfoDTO scheduleDTO);

	public List<ScheduleInfoDTO> getByCustomerId(Long customerId);

	void clearAllSchedule(ScheduleInfoDTO scheduleDTO);

	List<ScheduleInfoDTO> getSchedulesOfSunday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfMonday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfTuesday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfWednesday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfThursday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfFriday(int scheduleStatus);

	List<ScheduleInfoDTO> getSchedulesOfSaturday(int scheduleStatus);

	List<ScheduleInfoDTO> getByCustomerIdAndScheduleStatus(Long customerId, int scheduleStatus);

	List<ScheduleInfoDTO> getByIds(List<Long> scheduledIdList);

	List<ScheduleInfoDTO> getRestSlotsSameDayScheduleOrder(Long customerId, int scheduleStatus);

	List<ScheduleInfoDTO> getNextScheduleDate(Long customerId, int scheduleStatus);

	List<Integer> getTimeSlotsByIds(List<Long> scheduleIdList);

	List<ScheduleInfoDTO> getByCustomerIdAndScheduleStatusAndTimeSlotId(Long customerId, int id, int timeSlotId);

	ScheduleInfoDTO getByCustomerIdProductIdAndTimeSlot(Long customerId, Integer productId , Integer timeSlotId);
	
}
