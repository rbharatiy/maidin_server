package com.maidin.persistence.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.ProductBrandsDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.ProductBrandsDTO;

public class ProductBrandsDAOImpl extends CommonDAO implements ProductBrandsDAO {

	/*
	 * @Override public void addProductCategory(ProductCategoryDTO tripDTO) {
	 * this.add(tripDTO); }
	 */

	/*
	 * @Override public ProductCategoryDTO getById(Long Id) { String namedQuery
	 * = "ProductCategory.getById"; return this.getQuery(namedQuery,
	 * ProductCategoryDTO.class, new Param("Id", Id)); }
	 */

	/*
	 * @Override public void deleteInfo(ProductCategoryDTO dto) {
	 * this.delete(dto); }
	 */

	/*
	 * @Override public void updateProductCategory(ProductCategoryDTO tripDTO) {
	 * this.update(tripDTO); }
	 */

	@Override
	public List<ProductBrandsDTO> getAllBrandsByCategoryID(Integer categoryId, boolean active) {
		/*
		 * String namedQuery = "ProductBrands.getAllBrandsByCategoryID"; return
		 * this.list(namedQuery, ProductBrandsDTO.class, new Param("categoryId",
		 * categoryId), new Param("active", active));
		 */

		String namedQuery = "SELECT distinct * from ProductBrands b where b.categoryId=:categoryId"
				+ " and b.active=true order by IF(b.brand LIKE 'Maidin%',1,2), b.brand";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductBrandsDTO.class);
		query.setParameter("categoryId", categoryId);
		// query.setParameter("active", active);
		return query.list();
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductBrandsDTO getById(Integer brandId) {
		String namedQuery = "ProductBrands.getById";
		return this.getQuery(namedQuery, ProductBrandsDTO.class, new Param("Id", brandId));
	}

	@Override
	public void updateBrand(ProductBrandsDTO brandDTO) {
		this.update(brandDTO);

	}

	@Override
	public List<ProductBrandsDTO> getAllBrandsByCategoryIDWithoutActiveState(
			Integer categoryId) {/*
									 * String namedQuery =
									 * "ProductBrands.getAllBrandsByCategoryIDWithoutActiveState";
									 * return this.list(namedQuery,
									 * ProductBrandsDTO.class, new
									 * Param("categoryId", categoryId));
									 */
		/*
		 * String namedQuery =
		 * "ProductBrands.getAllBrandsByCategoryIDWithoutActiveState"; return
		 * (List<ProductBrandsDTO>)
		 * this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery).
		 * setParameter("categoryId", categoryId).list();
		 */
		// SQLQuery query = session.createSQLQuery(sql);

		String namedQuery = "SELECT * from ProductBrands b "
				+ " where b.categoryId=:categoryId order by IF(b.brand LIKE 'Maidin%',1,2), b.brand";
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.addEntity(ProductBrandsDTO.class);
		query.setParameter("categoryId", categoryId);
		return query.list();
	}

}
