package com.maidin.persistence.dao;


import java.util.List;

import com.maidin.persistence.dto.CartDTO;

public interface CartDAO {

	public Long addCart(CartDTO orderDTO);

	public void removeCart(CartDTO orderDTO);

	public CartDTO getByCustomerIdAndProductId(Long customerId, long productId);

	public List<CartDTO> getByCustomerId(Long customerId);

	public CartDTO getByCartId(Long cartId);
	
	    
		
}
