package com.maidin.persistence.dao.impl;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dto.PhotoDTO;
import com.maidin.persistence.dao.PhotoDAO;

public class PhotoDAOImpl extends CommonDAO implements PhotoDAO {

	@Override
	public Long addPhoto(PhotoDTO photosDTO) {
		return (Long) this.add(photosDTO);
	}

	@Override
	public void updatePhoto(PhotoDTO photosDTO) {
		this.update(photosDTO);
	}

	@Override
	public PhotoDTO getPhotoByPhotoTypeAndPhotoId(int imageType, long itemId) {
		String namedQuery = "Photo.getPhotoByImageTypeAndImageId";
		return this.getQuery(namedQuery, PhotoDTO.class, new Param("imageType", imageType), new Param("itemId", itemId));
	}

	@Override
	public List<PhotoDTO> getListByPhotoTypePhotoIdAndVersion(int imageType, Long itemId, String version) {
		String namedQuery = "Photo.getByPhotoTypePhotoIdAndVersion";
		return this.list(namedQuery, PhotoDTO.class, new Param("imageType", imageType), new Param("itemId", itemId),
				new Param("version", version));}

	@Override
	public PhotoDTO getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto(int imageType, Long itemId, String version, boolean primaryPhoto) {
		String namedQuery = "Photo.getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto";
		return this.getQuery(namedQuery, PhotoDTO.class, new Param("imageType", imageType), new Param("itemId", itemId),
				 new Param("version", version),
				 new Param("primaryPhoto", primaryPhoto));
	}

	@Override
	public PhotoDTO getById(long photoId) {
		String namedQuery = "Photo.getById";
		return this.getQuery(namedQuery, PhotoDTO.class, new Param("id", photoId));
	}

	@Override
	public PhotoDTO getByPhotoTypePhotoIdAndVersion(int imageType, long itemId, String version) {
		String namedQuery = "Photo.getByPhotoTypePhotoIdAndVersion";
		return this.getQuery(namedQuery, PhotoDTO.class, new Param("imageType", imageType), new Param("itemId", itemId),
				 new Param("version", version));
	}

	@Override
	public List<PhotoDTO> getListByPhotoTypePhotoIdVersionAndProductDetailIds(int imageType, long itemId,
			String version) {
		String namedQuery = "Photo.getByPhotoTypePhotoIdVersionAndProductDetailIds";
		return this.list(namedQuery, PhotoDTO.class, new Param("imageType", imageType), new Param("itemId", itemId),
				 new Param("version", version));
	}

	@Override
	public List<PhotoDTO> getByIds(List<Long> photoIds) {
		String namedQuery = "Photo.getByIds";
		return this.list(namedQuery, PhotoDTO.class, new Param("photoIds", photoIds));
	}

	@Override
	public PhotoDTO getByFileStoreId(long fileStoreId) {
		String namedQuery = "Photo.getByFileStoreId";
		return this.getQuery(namedQuery, PhotoDTO.class, new Param("fileStoreId", fileStoreId));
	}

}
