package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.AddressDTO;

public interface AddressDAO {

	public AddressDTO getByFlatId(long flatId);

	public void addAddressInfo(AddressDTO addressDTO);

	public AddressDTO getById(Long addressId);

	public AddressDTO getByRegisteredFlatId(long flatId);

	public List<AddressDTO> getByIds(List<Long> addressIdList);
	}
