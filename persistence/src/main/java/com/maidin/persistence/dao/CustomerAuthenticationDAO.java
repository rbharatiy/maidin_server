package com.maidin.persistence.dao;

import com.maidin.persistence.dto.CustomerAuthenticationDTO;

public interface CustomerAuthenticationDAO {
	
	    public Long addToken(CustomerAuthenticationDTO travellerDTO);
	    
	    public void updateToken(CustomerAuthenticationDTO travellerDTO);
	   
	    public CustomerAuthenticationDTO getByMobileNumber(String mobileNumber);
}

	