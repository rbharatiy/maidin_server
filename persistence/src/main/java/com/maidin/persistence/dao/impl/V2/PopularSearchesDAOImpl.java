package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.V2.PopularSearchesDAO;
import com.maidin.persistence.dto.V2.PopularSearchesDTO;

public class PopularSearchesDAOImpl extends CommonDAO implements PopularSearchesDAO {

	@Override
	public Long addKeyWord(PopularSearchesDTO popularSearchesDTO) {
		return (Long) this.add(popularSearchesDTO);
	}

	@Override
	public void updateKeyWord(PopularSearchesDTO popularSearchesDTO) {
		this.update(popularSearchesDTO);
		return;
	}

	@Override
	public List<PopularSearchesDTO> getActiveKeywords() {
		String namedQuery = "PopularSearches.getAllActive";
		return this.list(namedQuery, PopularSearchesDTO.class);
	}

	@Override
	public List<PopularSearchesDTO> getAllKeywords() {
		String namedQuery = "PopularSearches.getAll";
		return this.list(namedQuery, PopularSearchesDTO.class);
	}

}
