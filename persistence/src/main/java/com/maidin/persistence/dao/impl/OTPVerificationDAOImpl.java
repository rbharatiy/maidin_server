package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.OTPVerificationDAO;
import com.maidin.persistence.dto.OTPVerificationDTO;

public class OTPVerificationDAOImpl extends CommonDAO implements OTPVerificationDAO {

	@Override
	public void saveOTPInfo(OTPVerificationDTO oTPVerificationDTO) {
		this.add(oTPVerificationDTO);
	}

	@Override
	public OTPVerificationDTO getByMobile(String mobileNumber, String usercategory) {
		String namedQuery = "OTPVerification.getByMobile";
        return this.getQuery(namedQuery, OTPVerificationDTO.class, 
        		new Param("mobileNumber", mobileNumber), 
        		new Param("usercategory", usercategory));
	}

	@Override
	public OTPVerificationDTO getByMobileAndOtp(String mobileNumber, String otp, String usercategory) {
		String namedQuery = "OTPVerification.getByMobileAndOtp";
        return this.getQuery(namedQuery, OTPVerificationDTO.class, 
        		new Param("mobileNumber", mobileNumber), 
        		new Param("otp", otp),
        		new Param("usercategory", usercategory));
	}

	@Override
	public void deleteInfo(OTPVerificationDTO otpdto) {
		this.delete(otpdto);
		
	}

	@Override
	public void updateOTPInfo(OTPVerificationDTO dto) {
		this.update(dto);
		
	}

}
