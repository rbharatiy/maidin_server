package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

//import com.maidin.persistence.V2.dto.CityCategoryInfoV2DTO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.V2.CityCategoryInfoDAO;
import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;

public class CityCategoryInfoDAOImpl extends CommonDAO  implements CityCategoryInfoDAO{

	@Override
	public CityCategoryInfoDTO getByCityCode(String cityCode) {
		String namedQuery = "CityCategoryInfo.getByCC";
		return this.getQuery(namedQuery, CityCategoryInfoDTO.class, new Param("cityCode", cityCode));
		
    }

	@Override
	public List<CityCategoryInfoDTO> getByCityCodes(List<String> cities) {
		String namedQuery = "CityCategoryInfo.getByCityList";
		return this.list(namedQuery, CityCategoryInfoDTO.class, new Param("cities", cities));
	}

	@Override
	public List<String> getByAllCities(boolean active) {
		String namedQuery = "CityCategoryInfo.getByAllCities";
		return this.getColumnslist(namedQuery, CityCategoryInfoDTO.class, -1,new Param("active", active));
	}

	@Override
	public List<CityCategoryInfoDTO> getAllCityCategories(boolean active) {
		String namedQuery = "CityCategoryInfo.getAllCityCategories";
		return this.getColumnslist(namedQuery, CityCategoryInfoDTO.class, -1,new Param("active", active));
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<String> getAllCitiesWithoutActiveState() {
		String namedQuery = "CityCategoryInfo.getAllCitiesWithoutActiveState";
		return this.getColumnslist(namedQuery, CityCategoryInfoDTO.class, -1);
	}
	}
