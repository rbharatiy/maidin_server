package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.NotificationDTO;
import com.maidin.persistence.model.InfoNotificationDetails;

public interface NotificationDAO {

	void addNotification(NotificationDTO notificationDTO);

	List<InfoNotificationDetails> getSentInfoNotifications(int pageNumber, int notificationType);
}
