package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.ProductBrandsDTO;

public interface ProductBrandsDAO {

	public List<ProductBrandsDTO> getAllBrandsByCategoryID(Integer categoryId, boolean active);

	public ProductBrandsDTO getById(Integer brandId);

	public void updateBrand(ProductBrandsDTO brandDTO);

	//public List<ProductBrandsDTO> getAllExistingBrandsByCategoryID(int productCategoryId);

	List<ProductBrandsDTO> getAllBrandsByCategoryIDWithoutActiveState(Integer categoryId);

	

}
