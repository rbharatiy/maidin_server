package com.maidin.persistence.dao.V2;

import com.maidin.persistence.dto.V2.SearchQueryDTO;

public interface SearchQueryDAO {

	Long addSearchQuery(SearchQueryDTO searchQueryDTO);

	void updateQuery(SearchQueryDTO searchQueryDTO);

	SearchQueryDTO getByQuery(String query);
}
