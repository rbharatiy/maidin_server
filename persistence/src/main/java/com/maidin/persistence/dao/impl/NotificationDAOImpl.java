package com.maidin.persistence.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.NotificationDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminTransactionsDTO;
import com.maidin.persistence.dto.CartDTO;
import com.maidin.persistence.dto.NotificationDTO;
import com.maidin.persistence.model.InfoNotificationDetails;

public class NotificationDAOImpl extends CommonDAO implements NotificationDAO {

	@Override
	public void addNotification(NotificationDTO notificationDTO) {
		this.add(notificationDTO);
	}

	@Override
	public List<InfoNotificationDetails> getSentInfoNotifications(int pageNumber, int notificationType) {
		String namedQuery = "SELECT n.Id, n.title, n.message, a.FullName, n.successCount, n.failureCount, "
				+ "n.notificationType, CAST(1000 * UNIX_TIMESTAMP(n.createdOn) AS UNSIGNED INTEGER), "
				+ "n.adminId FROM Notification n INNER JOIN Admin a on n.adminId = a.Id "
				+ "where n.notificationType =:notificationType order by n.Id desc limit 50";

		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(namedQuery);
		query.setParameter("notificationType", notificationType);
		// query.setParameter("pageNumber", pageNumber);
		List<Object[]> list = query.list();
		List<InfoNotificationDetails> notificationDTOList = new ArrayList<>();

		list.stream().forEach((record) -> {
			InfoNotificationDetails notificationDTO = new InfoNotificationDetails();

			notificationDTO.setId(Long.parseLong(record[0].toString()));
			notificationDTO.setTitle(String.valueOf(record[1].toString()));
			notificationDTO.setMessage(String.valueOf(record[2].toString()));
			notificationDTO.setAdminName(String.valueOf(record[3].toString()));
			notificationDTO.setSuccessCount(Long.parseLong(record[4].toString()));
			notificationDTO.setFailureCount(Long.parseLong(record[5].toString()));
			notificationDTO.setNotificationType(Integer.parseInt(record[6].toString()));
			notificationDTO.setCreatedOn((new Timestamp(Long.parseLong(record[7].toString()))));
			notificationDTO.setAdminId(Long.parseLong(record[8].toString()));

			notificationDTOList.add(notificationDTO);
		});

		return notificationDTOList;
	}

}
