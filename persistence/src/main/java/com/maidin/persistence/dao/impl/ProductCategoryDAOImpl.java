package com.maidin.persistence.dao.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.ProductCategoryDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;
import com.maidin.persistence.dto.ProductCategoryDTO;

public class ProductCategoryDAOImpl extends CommonDAO implements ProductCategoryDAO {

	/*@Override
	public void addProductCategory(ProductCategoryDTO tripDTO) {
		this.add(tripDTO);
	}*/

/*	@Override
	public ProductCategoryDTO getById(Long Id) {
		String namedQuery = "ProductCategory.getById";
        return this.getQuery(namedQuery, ProductCategoryDTO.class, 
        		new Param("Id", Id));
	}*/
	
	/*@Override
	public void deleteInfo(ProductCategoryDTO dto) {
		this.delete(dto);
	}*/
	
/*	@Override
	public void updateProductCategory(ProductCategoryDTO tripDTO) {
		this.update(tripDTO);
	}*/

	@Override
	public List<ProductCategoryDTO> getAllProductCategories(boolean active) {
		String namedQuery = "ProductCategory.getAllProductCategories";
		return this.list(namedQuery, ProductCategoryDTO.class,
       		 new Param("active", active));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public ProductCategoryDTO getById(Integer categoryId) {
		String namedQuery = "ProductCategory.getById";
		return this.getQuery(namedQuery, ProductCategoryDTO.class,
       		 new Param("Id", categoryId));
	}

	@Override
	public void updateCategory(ProductCategoryDTO categoryDTO) {
		this.update(categoryDTO);
		
	}

	@Override
	public List<ProductCategoryDTO> getAllCategoriesWithoutActiveState() {
		String namedQuery = "ProductCategory.getAllProductCategoriesWithoutActiveState";
		return this.list(namedQuery, ProductCategoryDTO.class);
	}

	@Override
	public List<ProductCategoryDTO> getByIds(List<Integer> categoryIdList) {
		String namedQuery = "ProductCategory.getByIds";
		return this.list(namedQuery, ProductCategoryDTO.class, new Param("Ids",categoryIdList));
	}

}
