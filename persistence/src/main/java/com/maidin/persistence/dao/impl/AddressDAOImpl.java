package com.maidin.persistence.dao.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.AddressDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AddressDTO;
import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;

public class AddressDAOImpl extends CommonDAO implements AddressDAO {

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public AddressDTO getByFlatId(long flatId) {
		String namedQuery = "Address.getByFlatId";
        return this.getQuery(namedQuery, AddressDTO.class, new Param("flatId", flatId));
	}

	@Override
	public void addAddressInfo(AddressDTO addressDTO) {
		this.add(addressDTO);
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public AddressDTO getById(Long addressId) {
		String namedQuery = "Address.getById";
        return this.getQuery(namedQuery, AddressDTO.class, new Param("Id", addressId));
	}

	@Override
	public AddressDTO getByRegisteredFlatId(long flatId) {
		String namedQuery = "Address.getByRegisteredFlatId";
        return this.getQuery(namedQuery, AddressDTO.class, new Param("flatId", flatId));
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public List<AddressDTO> getByIds(List<Long> addressIdList) {
		String namedQuery="Address.getByIds";
		return this.list(namedQuery, AddressDTO.class, new Param("Ids",addressIdList));
	}
	
}
