package com.maidin.persistence.dao;

import java.sql.Timestamp;
import java.util.List;

import com.maidin.persistence.dto.AdminTransactionsDTO;
import com.maidin.persistence.dto.CustomerTransactionDTO;

public interface CustomerTransactionDAO {

	public CustomerTransactionDTO getByCustomerId(Long customerId);

	public void addTransaction(CustomerTransactionDTO transactionDTO);

	public List<CustomerTransactionDTO> getByOrderDetailsId(Long orderId);

	public List<Integer> getPaymentModeByOrderDetailsId(Long orderId);

	public List<CustomerTransactionDTO> getByOrderDetailsIdAndPaymentMode(Long orderDetailsId, int id);

	List<AdminTransactionsDTO> getCustomerTransactions(Long customerId, int firstResult, int maxResult);

	long getCustomerTransactionCount(Long customerId);

	// public CustomerTransactionDTO getByOrderDetailsIdAndTransType(Long id,
	// String name);

	public List<CustomerTransactionDTO> getAllOrderTransaction();

	// added By Pushpinder
	public List<Object[]> getMoneyOutTransactions(Long customerId, int firstResult, int maxResult);

	// added By Pushpinder
	public long getTotalCountMoneyOutTransactions(Long customerId);

	// added By Pushpinder
	public long getTotalCountMoneyInTransactions(Long customerId);

	// added By Pushpinder
	public List<Object[]> getMoneyInTransactions(Long customerId, int firstResult, int maxResult);

	public List<Object[]> getMoneyAllTransactions(Long customerId, int firstResult, int maxResult);

	long getMoneyCountAllTransactions(Long customerId);

	public List<CustomerTransactionDTO> getAllTransactionsByOrderDetailsId(Long orderDetailId);


	List<Object[]> getByOrderId(Long orderId);

	String getByPaymentOptionTextByOrderId(Long orderId);

	public List<CustomerTransactionDTO> getAllTransactionsByOrderDetailsIdWithNoBebt(Long id);



	List<Object[]> getPaymentInfoByOrderDetailsId(Long orderDetailsId);

	List<Object[]> getPaymentInfoByOrderIdList(List<Long> orderIdList);

}
