package com.maidin.persistence.dao;

import com.maidin.persistence.dto.AdminDTO;

public interface AdminDAO {

	public AdminDTO getByMobileNumber(String mobileNumber);

	public AdminDTO getByEmail(String email);

	public AdminDTO getByIdAndMobileNumber(Long customerId, String mobileNumber);

	public AdminDTO getByMobileNumberWithoutActiveState(String mobileNumber);

}
