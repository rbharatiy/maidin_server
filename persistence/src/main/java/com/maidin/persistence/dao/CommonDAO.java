package com.maidin.persistence.dao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.SessionFactoryUtils;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public abstract class CommonDAO {

	protected SessionFactory sessionFactory;

	private <T> Object executeOperation(final DatabaseOperations operation, final Object obj, final String namedQuery,
			final Map<String, Object> params, int maxResults) {
		Session session = getSession();
		switch (operation) {
		case SAVE:
			return session.save(obj);
		case UPDATE:
			session.update(obj);
			return null;
		case DELETE:
			session.delete(obj);
			return null;
		case GET:
			Query query = session.getNamedQuery(namedQuery);
			if (params != null) {
				query = query.setProperties(params);
			}
			// if(maxResults <= 0){
			query.setMaxResults(maxResults);
			// }
			List queryResult = query.list();
			return queryResult;
		case EXECUTE_UPDATE:
			Query updateQuery = session.getNamedQuery(namedQuery).setProperties(params);
			return updateQuery.executeUpdate();
		default:
			throw new AssertionError();
		}
	}

	private <T> Object executeOperation1(final DatabaseOperations operation, final Object obj, final String namedQuery,
			final Map<String, Object> params, int firstResult, int maxResults) {
		Session session = getSession();
		switch (operation) {
		
		case GET:
			Query query = session.getNamedQuery(namedQuery);
			if (params != null) {
				query = query.setProperties(params);
			}
			query.setFirstResult(firstResult);
			// if(maxResults <= 0){
			query.setMaxResults(maxResults);
			// }
			List queryResult = query.list();
			return queryResult;
		
		default:
			throw new AssertionError();
		}
	}

	protected final Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		// TODO Auto-generated method stub
		return sessionFactory;
	}

	protected final Object add(Object obj) {
		return this.executeOperation(DatabaseOperations.SAVE, obj, null, null, -1);
	}

	protected final void update(Object obj) {
		this.executeOperation(DatabaseOperations.UPDATE, obj, null, null, -1);
	}

	protected final void delete(Object obj) {
		this.executeOperation(DatabaseOperations.DELETE, obj, null, null, -1);
	}

	protected final Object getQuery(String namedQuery, Map<String, Object> params, Object obj, int maxResults) {
		return this.executeOperation(DatabaseOperations.GET, obj, namedQuery, params, maxResults);
	}

	protected final Object getQuery1(String namedQuery, Map<String, Object> params, Object obj, int firstResult,
			int maxResults) {
		return this.executeOperation1(DatabaseOperations.GET, obj, namedQuery, params, firstResult, maxResults);
	}

	protected final Object getQuery(String namedQuery, Map<String, Object> params) {
		return getQuery(namedQuery, params, null, -1);
	}

	protected final <T> T getQuery(String namedQuery, Class<T> objectType, Param... params) {
		Iterable<T> objects = executeObjectListQuery(namedQuery, objectType, params);
		Iterator<T> it = objects.iterator();
		return it.hasNext() ? it.next() : null;
	}

	protected final <T> T getCriteriaQuery(String namedQuery, Class<T> objectType, int firstresult, int maxResult,
			Param... params) {
		Iterable<T> objects = executeObjectListQuery1(namedQuery, objectType, firstresult, maxResult, params);
		Iterator<T> it = objects.iterator();
		return it.hasNext() ? it.next() : null;
	}

	protected final <T> Object updateQuery(String namedQuery, Param... params) {
		Map<String, Object> paramsMap = new HashMap<>();
		for (Param p : params) {
			paramsMap.put(p.name, p.value);
		}
		return this.executeOperation(DatabaseOperations.EXECUTE_UPDATE, null, namedQuery, paramsMap, -1);
	}

	protected final <T> Object getQuery(String namedQuery, Param... params) {
		Map<String, Object> paramsMap = new HashMap<>();
		for (Param p : params) {
			paramsMap.put(p.name, p.value);
		}
		return this.executeOperation(DatabaseOperations.GET, null, namedQuery, paramsMap, -1);
	}

	private <T> Iterable<T> executeObjectListQuery(String namedQuery, Class<T> objectType, Param... params) {
		return executeObjectListQuery(namedQuery, objectType, -1, params);
	}

	private <T> Iterable<T> executeObjectListQuery1(String namedQuery, Class<T> objectType, Param... params) {
		return executeObjectListQuery(namedQuery, objectType, -1, params);
	}

	private  Iterable executeObjectListQuery1(String namedQuery, Class objectType, int firstResult,
			int maxResults, Param... params) {
		Map<String, Object> paramsMap = new HashMap<>();
		for (Param p : params) {
			paramsMap.put(p.name, p.value);
		}
		Object returnObject = this.getQuery1(namedQuery, paramsMap, null, firstResult, maxResults);
		if (returnObject != null) {
			List result = (List) returnObject;
			return result;
		} else {
			return Collections.emptyList();
		}
	}

	private <T> Iterable<T> executeObjectListQuery(String namedQuery, Class<T> objectType, int maxResults,
			Param... params) {
		Map<String, Object> paramsMap = new HashMap<>();
		for (Param p : params) {
			paramsMap.put(p.name, p.value);
		}
		Object returnObject = this.getQuery(namedQuery, paramsMap, null, maxResults);
		if (returnObject != null) {
			List result = (List) returnObject;
			return Iterables.filter(result, objectType);
		} else {
			return Collections.emptyList();
		}
	}
	
	private Iterable executetListQuery(String namedQuery, Class objectType, int maxResults,
			Param... params) {
		Map<String, Object> paramsMap = new HashMap<>();
		for (Param p : params) {
			paramsMap.put(p.name, p.value);
		}
		Object returnObject = this.getQuery(namedQuery, paramsMap, null, maxResults);
		if (returnObject != null) {
			List result = (List) returnObject;
			return result;
		} else {
			return Collections.emptyList();
		}
	}

	protected final <T> List<T> list(String namedQuery, Class<T> objectType, Param... params) {
		return getObjectList(namedQuery, objectType, -1, params);
	}
	protected final  List listByCriteria(String namedQuery, Class objectType,int firstResult,int maxResult, Param... params) {
		return getObjectList2(namedQuery, objectType, firstResult, maxResult, params);
	}
	protected final <T> List<T> getlist(String namedQuery, Class<T> objectType, int maxResults, Param... params) {
		return getObjectList(namedQuery, objectType, maxResults, params);
	}
	//columns only
	protected final  List getColumnslist(String namedQuery, Class objectType, int maxResults, Param... params) {
		return getColumnsList(namedQuery, objectType, maxResults, params);
	}
	private final <T> List<T> getObjectList(String namedQuery, Class<T> objectType, int maxResults, Param... params) {
		Iterable<T> objects = executeObjectListQuery(namedQuery, objectType, maxResults, params);
		return Lists.newArrayList(objects);
	}
	private final  List getObjectList2(String namedQuery, Class objectType,int firstResult, int maxResults, Param... params) {
		Iterable objects = executeObjectListQuery1(namedQuery, objectType, firstResult, maxResults, params);
		return Lists.newArrayList(objects);
	}
	
	private final  List getColumnsList(String namedQuery, Class objectType, int maxResults, Param... params) {
		Iterable objects = executetListQuery(namedQuery, objectType, maxResults, params);
		return Lists.newArrayList(objects);
	}
	public final void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected final Object batchUpdate(Object obj) {
		return this.executeOperation(DatabaseOperations.SAVE, obj, null, null, -1);
	}

	public DataSource getDataSource() {
		return SessionFactoryUtils.getDataSource(sessionFactory);
	}

	public static final class Param {

		public final String name;

		public final Object value;

		public Param(String name, Object value) {
			this.name = name;
			this.value = value;
		}
	}

}
