package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.SubCategoryInfoV2DAO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.SubCategoryInfoV2DTO;

public class SubCategoryInfoV2DAOImpl extends CommonDAO  implements SubCategoryInfoV2DAO{

	@Override
	public List<SubCategoryInfoV2DTO> getAllSubCategories(List<Integer> subCategoryIdList) {
		String namedQuery ="SubCategoryInfoV2.getAllSubCategories";
		return this.list(namedQuery, SubCategoryInfoV2DTO.class,new Param("Ids",subCategoryIdList));
		
	}

	@Override
	public SubCategoryInfoV2DTO getById(int subCategoryId) {
		String namedQuery ="SubCategoryInfoV2.getById";
		return this.getQuery(namedQuery, SubCategoryInfoV2DTO.class,new Param("Id",subCategoryId));
	}

	@Override
	public void updateSubCategoryInfo(SubCategoryInfoV2DTO brandDTO) {
		this.update(brandDTO);
		
	}

	@Override
	public SubCategoryInfoV2DTO getByIdWithoutActiveStatus(int subCategoryId) {
		String namedQuery ="SubCategoryInfoV2.getByIdWithoutActiveStatus";
		return this.getQuery(namedQuery, SubCategoryInfoV2DTO.class,new Param("Id",subCategoryId));
	}

	@Override
	public List<Integer> getByIds(List<Integer> subCategoryIdList) {
		String namedQuery ="SubCategoryInfoV2.getByIdsWithActiveStatus";
		return this.getColumnslist(namedQuery, SubCategoryInfoV2DTO.class, -1, new Param("Ids",subCategoryIdList));
	}

	@Override
	public List<Integer> getByIdsWithoutActiveState(List<Integer> subCategoryIdList) {
		String namedQuery ="SubCategoryInfoV2.getByIdsWithoutActiveStatus";
		return this.getColumnslist(namedQuery, SubCategoryInfoV2DTO.class, -1, new Param("Ids",subCategoryIdList));
	}

	@Override
	public List<SubCategoryInfoV2DTO> getAllSubCategoriesWithoutActiveState(List<Integer> subCategoryIdList) {
		String namedQuery ="SubCategoryInfoV2.getAllSubCategoriesWithoutActiveState";
		return this.list(namedQuery, SubCategoryInfoV2DTO.class,new Param("Ids",subCategoryIdList));
		
	}

	@Override
	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active) {
		String namedQuery ="SubCategoryInfoV2.getAllMatchingTextWithActivationStatus";
		return this.getColumnslist(namedQuery, SubCategoryInfoV2DTO.class, -1, new Param("productSubString", "%" + productSubString + "%"),
				new Param("active",active));
	}

	@Override
	public List<SubCategoryInfoV2DTO> getAll(boolean b) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<SubCategoryInfoV2DTO> getAllSubCategoriesWithoutActiveState() {
		String namedQuery ="SubCategoryInfoV2.getAll";
		return this.list(namedQuery, SubCategoryInfoV2DTO.class);
		
	}
	}
