package com.maidin.persistence.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.maidin.persistence.dto.AdminDeviceDetailsDTO;
import com.maidin.persistence.dto.CustomerAuthenticationDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;

public interface AdminDeviceDetailsDAO {

	public void updateDeviceDetails(AdminDeviceDetailsDTO deviceDTO);

	public Long addDeviceDetails(AdminDeviceDetailsDTO detailsDTO);

	public AdminDeviceDetailsDTO getDeviceDetailsByAdminId(Long id);
	
	   /* public Long addDeviceDetails(CustomerDeviceDetailsDTO travellerDTO);
	    
	    public void updateDeviceDetails(CustomerDeviceDetailsDTO travellerDTO);
	   
	    public SessionFactory getSessionFactory();

		public CustomerDeviceDetailsDTO getDeviceDetailsByCustomerId(Long customerId);
		
		public CustomerDeviceDetailsDTO getDeviceDetailsById(Long Id);

		public List<CustomerDeviceDetailsDTO> listAllDevicesById();*/

		
}
