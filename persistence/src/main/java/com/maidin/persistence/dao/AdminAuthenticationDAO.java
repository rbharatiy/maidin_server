package com.maidin.persistence.dao;

import com.maidin.persistence.dto.AdminAuthenticationDTO;

public interface AdminAuthenticationDAO {
	
	    public Long addToken(AdminAuthenticationDTO travellerDTO);
	    
	    public void updateToken(AdminAuthenticationDTO travellerDTO);
	   
	    public AdminAuthenticationDTO getByMobileNumber(String mobileNumber);
}

	