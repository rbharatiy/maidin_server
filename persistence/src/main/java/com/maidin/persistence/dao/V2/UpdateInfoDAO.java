package com.maidin.persistence.dao.V2;

import java.util.List;
import java.util.Set;

import com.maidin.persistence.dto.V2.BrandInfoV2DTO;
import com.maidin.persistence.dto.V2.UpdateInfoDTO;

public interface UpdateInfoDAO {

	public UpdateInfoDTO getByText(String string);

	/*public List<BrandInfoV2DTO> getByBrandIds(Set<Integer> productIdList);

	public BrandInfoV2DTO getByBrandId(Integer brandId);
*/
}
