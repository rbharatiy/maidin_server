package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.ProductCityInfoV2DTO;

public interface ProductCityInfoV2DAO {

	public List<ProductCityInfoV2DTO> getByCityCodeAndSubCategoryId(String cityCode, int subCategoryId, boolean active);

	public List<ProductCityInfoV2DTO> getBySubCategoryIdsAndCityCode(List<Integer> subCategoryIds, String cityCode, boolean active);

	public ProductCityInfoV2DTO getById(long productDetailsId,boolean active);

	public ProductCityInfoV2DTO getByIdWithoutActiveStatus(long productDetailsId);

	public List<ProductCityInfoV2DTO> getByproductIdsAndCityCode(List<Integer> productIdList, String cityCode);

	public List<ProductCityInfoV2DTO> getByproductIds(List<Integer> productIdList);

	public List<ProductCityInfoV2DTO> getByProductDetailGroupId(Long productDetailGroupId);

	public Long addProductInfo(ProductCityInfoV2DTO productDTO);

	public ProductCityInfoV2DTO getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode(int productId, String quantityUnit,
			int categoryId, int subCategoryId, String cityCode);

	public void updateProductInfo(ProductCityInfoV2DTO pcCityInfoV2DTO);

	public void updateProductDetailGroup(List<Long> productDetailIds, Long productDetailGroupId);

	public List<Long> getByProductIdAndCities(int productId, List<String> cities);

	public void updateStatus(boolean active, boolean stockStatus, List<Long> productDetailIdList);

	List<ProductCityInfoV2DTO> getByproductIdsAndCityCodeWithActivationStatus(List<Integer> productIdList,
			String cityCode, boolean active);

	public List<ProductCityInfoV2DTO> getByProductDetailIds(List<Long> productDetailIds);

	public List<Long> getByProductIdAndCityCode(long productDetailId, int productId, String cityCode);

	public void updateSetByDefault(List<Long> productDetailIdList, boolean setByDefault);

	public List<ProductCityInfoV2DTO> getByCategoryIdAndSubCategoryId(int categoryId, Integer subCategoryId);

	public List<ProductCityInfoV2DTO> getByProductIdGroupByCity(Integer id);

	public List<Long> getByProductIdQuantityUnitAndCities(Integer productId, String quantityUnit, List<String> cities);

	public List<Long> getIdsBySubCategoryIdAndCities(Integer subCategoryId, List<String> cities, boolean active);

	public List<Long> getIdsByCategoryIdAndCities(int categoryId, List<String> cities, boolean b);

	public List<ProductCityInfoV2DTO> getByCityCodeAndSubCategoryIdWithoutActiveStatus(String cityCode,
			int subCategoryId);

	public List<ProductCityInfoV2DTO> getBySubCategoryIdsAndCityCodeWithoutActiveState(
			List<Integer> subCategoryActiveIds, String cityCode);

	public List<ProductCityInfoV2DTO> getByProductIdQuantityUnit(Integer productId, String quantityUnit);

	public List<ProductCityInfoV2DTO> getByMatchingIdsAndCityCode(List<Integer> categoryIdList,
			List<Integer> subCategoryIdList, List<Integer> brandIdList, List<Integer> productIdList, String cityCode);

	public List<ProductCityInfoV2DTO> getByPhotoId(Long photoId);

	public List<ProductCityInfoV2DTO> getByCityCode(String string);

	public List<ProductCityInfoV2DTO> getByProductIdQuantityUnitAndCityCode(Integer productId, String quantityUnit,
			String cityCode);

	public List<Long> getByProductIdQuantityUnitCategoryIdSubCategoryIdAndCities(Integer productId, String quantityUnit,
			Integer categoryId, Integer subCategoryId, List<String> cities);

	public String getProductName(Integer productId);

	/*public List<Long> getIdsBySubCategoryIdAndCityCodes(int subCategoryId, List<String> cities, boolean b);
*/
	/*public List<Long> getBySubCategoryIdsAndCityCodeWithoutActiveStatus(List<Integer> subCategoryIdList,
			String cityCode);*/
}

	
