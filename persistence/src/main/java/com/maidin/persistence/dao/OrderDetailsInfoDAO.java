package com.maidin.persistence.dao;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import com.maidin.persistence.dto.OrderDetailsInfoDTO;
import com.maidin.persistence.dto.V2.PreviousOrderDetailsInfoDTO;
import com.maidin.persistence.model.OrderPaymentInfo;

public interface OrderDetailsInfoDAO {

	OrderDetailsInfoDTO getByOrderIdAndProductId(Long id, int productId);

	public Long addOrderDetailsInfo(OrderDetailsInfoDTO orderDTO);

	public OrderDetailsInfoDTO getById(long orderId);

	void deleteOrderDetails(OrderDetailsInfoDTO orderInfoDTO);

	public List<OrderDetailsInfoDTO> getByOrderId(Long orderId);

	public List<OrderDetailsInfoDTO> getByCustomerIdDeliveryDateAndPaymentStatus(Long customerId, Date paymentDate,
			String paymentStatus);

	List<Long> getIdListByOrderId(Long orderId);

	void deleteorderDetailsInfoList(List<Long> orderDetailsInfoIdList);

	void updateOrderDetailsInfoList(List<Long> orderDetailsInfoIdList, boolean cancelled, int quantity,
			Timestamp updatedOn);

	List<OrderDetailsInfoDTO> getByOrderIdWithNoCancelStatus(Long orderId);

	List<OrderDetailsInfoDTO> getSettledOrderDetailsByOrderId(Long orderId);

	List<OrderDetailsInfoDTO> getOrdersByYTT(Long excludeAddressId, String date);

	List<OrderDetailsInfoDTO> getUnSettledPaymentOrders(Long customerId);

	List<OrderDetailsInfoDTO> getUnSettledOrdersByOrderId(Long id);

	Float getTotalPaidByCustomer(List<Long> orderDetailsInfoIdList);

	List<OrderDetailsInfoDTO> getUnSettledDeliveredOrders(Long customerId);

	List<Integer> getByOrderIdAndPaymentModeId(Long orderId);

	List<PreviousOrderDetailsInfoDTO> getOrderDetails(List<Long> orderId, String orderStatus, boolean isCancelled);

	void updateOrderDetails(OrderDetailsInfoDTO orderDetailsInfo);

	List<Long> getUnsettledOrderCustomerList();

	List<Long> getByIds(List<Long> unDeliveredOrderDetailsInfoIdList);

	List<PreviousOrderDetailsInfoDTO> getOrderDetailsWithoutOrderState(List<Long> orderIdList, boolean isCancelled);

	List<OrderDetailsInfoDTO> getSettledOrderByOrderIdList(List<Long> collect);

	OrderPaymentInfo getOrderPaymentInfo(Long orderId);

	List<Object[]> getSettledOrdersByOrderId(Long orderId);

}
