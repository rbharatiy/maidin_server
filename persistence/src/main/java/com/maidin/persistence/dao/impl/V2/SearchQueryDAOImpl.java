package com.maidin.persistence.dao.impl.V2;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.SearchQueryDAO;
import com.maidin.persistence.dto.V2.SearchQueryDTO;

public class SearchQueryDAOImpl extends CommonDAO implements SearchQueryDAO {

	@Override
	public Long addSearchQuery(SearchQueryDTO searchQueryDTO) {
		return (Long) this.add(searchQueryDTO);
	}

	@Override
	public void updateQuery(SearchQueryDTO searchQueryDTO) {
		this.update(searchQueryDTO);
		return;
	}

	@Override
	public SearchQueryDTO getByQuery(String query) {
		String namedQuery = "SearchQuery.getByQuery";
		return this.getQuery(namedQuery, SearchQueryDTO.class, new Param("query", query));
	}
}
