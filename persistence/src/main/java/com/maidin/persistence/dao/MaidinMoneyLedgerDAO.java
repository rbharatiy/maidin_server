package com.maidin.persistence.dao;

import java.util.Date;
import java.util.List;

import com.maidin.persistence.dto.MaidinMoneyLedgerDTO;

public interface MaidinMoneyLedgerDAO {

	Long addEntryInLedger(MaidinMoneyLedgerDTO maidinMoneyLedgerDTO);

	void updateBalance(MaidinMoneyLedgerDTO maidinMoneyLedgerDTO);

	MaidinMoneyLedgerDTO getByDate(Date date);

	MaidinMoneyLedgerDTO getByPreviousDay();

	List<MaidinMoneyLedgerDTO> getBetweenDates(Date fromDate, Date toDate);

}
