package com.maidin.persistence.dao.V2;

import java.util.List;

import com.maidin.persistence.dto.V2.ErrorInfoDTO;

public interface ErrorInfoDAO {

	public List<ErrorInfoDTO> getAll();

	
}
