package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.AdminDeviceDetailsDAO;
import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dto.AdminDTO;
import com.maidin.persistence.dto.AdminDeviceDetailsDTO;

public class AdminDeviceDetailsDAOImpl extends CommonDAO implements AdminDeviceDetailsDAO {

	@Override
	public void updateDeviceDetails(AdminDeviceDetailsDTO deviceDTO) {
		this.update(deviceDTO);
		
	}

	@Override
	public Long addDeviceDetails(AdminDeviceDetailsDTO detailsDTO) {
	return (Long)this.add(detailsDTO);
		
	}

	@Override
	public AdminDeviceDetailsDTO getDeviceDetailsByAdminId(Long adminId) {
		String namedQuery = "AdminDeviceDetails.getByAdminId";
        return this.getQuery(namedQuery, AdminDeviceDetailsDTO.class, new Param("adminId", adminId));
	}
	
}
