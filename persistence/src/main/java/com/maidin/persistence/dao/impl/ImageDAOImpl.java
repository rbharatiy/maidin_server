package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.ImageDAO;
import com.maidin.persistence.dto.ImageDTO;


public class ImageDAOImpl extends CommonDAO implements ImageDAO{
	
	    @Override
	    public Long addFile(ImageDTO imageDTO){
	    	return (Long)this.add(imageDTO);
	    }
	    
	    public void updateFile(ImageDTO imageDTO){
	    	this.update(imageDTO);
	    }
	   
	    public void deleteFile(ImageDTO imageDTO){
	    	this.delete(imageDTO);
	    }
	    
	    @Override
		public ImageDTO getByProductId(String imageCategory, Long productId){
			String namedQuery = "Images.getByProductId";
			return this.getQuery(namedQuery, ImageDTO.class, new Param("imageCategory",imageCategory),
					                                  new Param("imageCategoryId",productId));
			
		}
}

	