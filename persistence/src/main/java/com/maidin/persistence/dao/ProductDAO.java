package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.ProductDTO;

public interface ProductDAO {

	//List<ProductDTO> getAllProductsByBrandIDAndCategoryId(Long brandId, Long categoryId, boolean active);

	public ProductDTO getByProductId(Integer productId);

	public List<ProductDTO> getAllProductsByBrandId(Integer productBrandId, boolean b);

	public ProductDTO getById(Integer productId);

	public void updateProduct(ProductDTO productDTO);

	//List<ProductDTO> getAllExistingProductsByBrandId(Integer productBrandId);

	//public List<ProductDTO> getAllExistingProductsByBrandIdWithoutActiveState(int productBrandId);

	List<ProductDTO> getAllExistingProductsByBrandIdWithoutActiveState(Integer brandId);

	public Integer addProduct(ProductDTO productDTO);
	
	public List<ProductDTO> getAllMatchingProducts(String productSubString);
	
	public List<ProductDTO> getAllActiveMatchingProducts(String productSubString);

}
