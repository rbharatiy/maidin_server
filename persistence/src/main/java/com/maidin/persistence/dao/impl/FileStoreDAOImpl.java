package com.maidin.persistence.dao.impl;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dto.FileStoreDTO;
import com.maidin.persistence.dao.FileStoreDAO;

public class FileStoreDAOImpl extends CommonDAO implements FileStoreDAO {

	@Override
	public long addFileStore(FileStoreDTO dto) {
		return (Long) this.add(dto);
	}

	@Override
	public void updateFileStore(FileStoreDTO dto) {
		this.update(dto);
	}

	@Override
	public void deleteFileStore(FileStoreDTO dto) {
		//this.delete(dto);
	}

	@Override
	public FileStoreDTO getFileById(long id) {
		String namedQuery = "FileStore.getById";
		return this.getQuery(namedQuery, FileStoreDTO.class, new Param("id", id));
	}

	@Override
	public FileStoreDTO getByMD5(String md5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FileStoreDTO getFileByFileName(String fileName) {
		String namedQuery = "FileStore.getFileByFileName";
		return this.getQuery(namedQuery, FileStoreDTO.class, new Param("fileName", fileName));
	}

}
