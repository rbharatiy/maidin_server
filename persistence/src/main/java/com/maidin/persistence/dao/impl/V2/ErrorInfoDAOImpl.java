package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.ErrorInfoDAO;
import com.maidin.persistence.dto.V2.CityCategoryInfoDTO;
import com.maidin.persistence.dto.V2.ErrorInfoDTO;

public class ErrorInfoDAOImpl extends CommonDAO implements ErrorInfoDAO{

	
	@Override
	public List<ErrorInfoDTO> getAll() {
		String namedQuery = "ErrorInfo.getAll";
		return this.list(namedQuery, ErrorInfoDTO.class);
	}


	}
