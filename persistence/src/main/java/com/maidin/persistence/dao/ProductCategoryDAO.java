package com.maidin.persistence.dao;

import java.util.List;

import com.maidin.persistence.dto.ProductCategoryDTO;

public interface ProductCategoryDAO {

	public List<ProductCategoryDTO> getAllProductCategories(boolean active);

	public ProductCategoryDTO getById(Integer categoryId);

	public void updateCategory(ProductCategoryDTO categoryDTO);

	public List<ProductCategoryDTO> getAllCategoriesWithoutActiveState();

	public List<ProductCategoryDTO> getByIds(List<Integer> categoryIdList);

}
