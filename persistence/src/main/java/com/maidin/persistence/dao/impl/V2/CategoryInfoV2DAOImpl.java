package com.maidin.persistence.dao.impl.V2;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.maidin.persistence.dao.CommonDAO;
import com.maidin.persistence.dao.CommonDAO.Param;
import com.maidin.persistence.dao.V2.CategoryInfoV2DAO;
import com.maidin.persistence.dto.ProductCategoryDTO;
import com.maidin.persistence.dto.V2.CategoryInfoV2DTO;
import com.maidin.persistence.dto.V2.ProductInfoV2DTO;

public class CategoryInfoV2DAOImpl extends CommonDAO implements CategoryInfoV2DAO{

	@Override
	public List<CategoryInfoV2DTO> getByIds(List<Integer> categoryIdList) {
		String namedQuery = "CategoryInfoV2.getByIds";
		return this.list(namedQuery, CategoryInfoV2DTO.class, new Param("Ids",categoryIdList));
	}

	@Override
	public List<CategoryInfoV2DTO> getAll() {
		String namedQuery = "CategoryInfoV2.getAll";
		return this.list(namedQuery, CategoryInfoV2DTO.class);
	}

	@Override
	@Transactional(rollbackFor = Exception.class, readOnly = true)
	public CategoryInfoV2DTO getById(Integer categoryId) {
		String namedQuery = "CategoryInfoV2.getById";
		return this.getQuery(namedQuery, CategoryInfoV2DTO.class, new Param("Id",categoryId));
	}

	@Override
	public void updateCategoryInfo(CategoryInfoV2DTO categoryDTO) {
		this.update(categoryDTO);
		
	}

	@Override
	public List<CategoryInfoV2DTO> getByIdsWithoutActiveState(List<Integer> categoryIdList) {
		String namedQuery = "CategoryInfoV2.getByIdsWithoutActiveState";
		return this.list(namedQuery, CategoryInfoV2DTO.class, new Param("Ids",categoryIdList));
	}

	@Override
	public List<Integer> getAllMatchingTextWithActivationStatus(String productSubString, boolean active) {
		String namedQuery ="CategoryInfoV2.getAllMatchingTextWithActivationStatus";
		return this.getColumnslist(namedQuery, CategoryInfoV2DTO.class, -1, new Param("productSubString", "%" + productSubString + "%"),
				new Param("active",active));
	}

	@Override
	public List<Integer> getActiveIdsByIds(List<Integer> categoryIdList) {
		String namedQuery = "CategoryInfoV2.getActiveIdsByIds";
		return this.getColumnslist(namedQuery, CategoryInfoV2DTO.class, -1, new Param("Ids",categoryIdList));
	}}
