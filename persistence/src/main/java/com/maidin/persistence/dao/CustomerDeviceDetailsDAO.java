package com.maidin.persistence.dao;

import java.util.List;

import org.hibernate.SessionFactory;

import com.maidin.persistence.dto.CustomerAuthenticationDTO;
import com.maidin.persistence.dto.CustomerDeviceDetailsDTO;

public interface CustomerDeviceDetailsDAO {

	public Long addDeviceDetails(CustomerDeviceDetailsDTO travellerDTO);

	public void updateDeviceDetails(CustomerDeviceDetailsDTO travellerDTO);

	public SessionFactory getSessionFactory();

	public CustomerDeviceDetailsDTO getDeviceDetailsByCustomerId(Long customerId);

	public CustomerDeviceDetailsDTO getDeviceDetailsById(Long Id);

	public List<CustomerDeviceDetailsDTO> listAllDevicesById();

	public long getUsersCountByClientPlatform(String clientPlatform);

	List<String> getAllNotificationTokens(Long excludeId);
}
