package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({
		// @NamedQuery(name="ProductInfoV2.getByProductIds", query="Select b
		// from ProductInfoV2DTO b where b.Id in (:Ids) and active=true ")
		@NamedQuery(name = "ProductInfoV2.getByProductIds", query = "Select b from ProductInfoV2DTO b where b.Id in (:Ids) and active=true "
				+ "order by (case when b.productName like 'Maidin%' then 1 else 2 end), b.productName  "),
		@NamedQuery(name = "ProductInfoV2.getById", query = "Select b from ProductInfoV2DTO b where b.Id =:Id and active=:active  "),
		@NamedQuery(name = "ProductInfoV2.getByIdWithoutActiveStatus", query = "Select b from ProductInfoV2DTO b where b.Id =:Id "),
		/*@NamedQuery(name = "ProductInfoV2.getByProductName", query = "SELECT distinct p from ProductCityInfoV2DTO pc, ProductInfoV2DTO p, "
				+ " BrandInfoV2DTO b, SubCategoryInfoV2DTO s where pc.productId = p.Id and pc.subCategoryId = s.Id and p.brandId = b.Id and "
				+ "p.productName LIKE :productSubString or b.brand LIKE"
				+ " :productSubString or s.subCategory LIKE :productSubString") })*/
		@NamedQuery(name = "ProductInfoV2.getByProductName", query = "SELECT distinct p from ProductInfoV2DTO p "
				+ " where p.productName LIKE :productSubString ") ,
		@NamedQuery(name = "ProductInfoV2.getByBrandName", query = /*"SELECT distinct p from ProductInfoV2DTO p, "
				+ " BrandInfoV2DTO b  where   p.brandId = b.Id and "
				+ " b.brand LIKE :productSubString"),*/
				"SELECT p FROM ProductInfoV2DTO p, BrandInfoV2DTO b where p.brandId = b.Id and "
				+ " concat(b.brand, ' ', p.productName) like :productSubString "),
		@NamedQuery(name = "ProductInfoV2.getByProductNameWithActivationStatus", query = "SELECT distinct p from ProductInfoV2DTO p "
				+ " where p.productName LIKE :productSubString and p.active=:active") ,
		@NamedQuery(name = "ProductInfoV2.getByBrandNameWithActivationStatus", query = 
				"SELECT p FROM ProductInfoV2DTO p, BrandInfoV2DTO b where p.brandId = b.Id and "
				+ " concat(b.brand, ' ', p.productName) like :productSubString and p.active=:active"
		/*"SELECT distinct p from ProductInfoV2DTO p, "
				+ " BrandInfoV2DTO b  where p.brandId = b.Id and "
				+ " b.brand LIKE :productSubString and p.active=:active "*/
				),
		@NamedQuery(name = "ProductInfoV2.getByIdsWithActiveStatus", query = "Select b.Id from ProductInfoV2DTO b where b.Id in (:Ids) and b.active=true  "),
		@NamedQuery(name = "ProductInfoV2.getByProductIdsWithoutActiveState", query = "Select b from ProductInfoV2DTO b where b.Id in (:Ids) "
				+ "order by (case when b.productName like 'Maidin%' then 1 else 2 end), b.productName  "),
		@NamedQuery(name = "ProductInfoV2.getByTextWithActivationStatus", query = "SELECT distinct p.Id from ProductInfoV2DTO p "
				+ " where p.productName LIKE :productSubString and p.active=:active"),
		@NamedQuery(name = "ProductInfoV2.getByProductNameAndBrandId", query = "SELECT p from ProductInfoV2DTO p "
				+ " where p.productName=:productName and brandId=:brandId ")
		
})

@Entity
@Table(name = "ProductInfoV2")
public class ProductInfoV2DTO {
	@Id
	@SequenceGenerator(name = "PRODUCTINFOV2_ID_GENERATOR", sequenceName = "PRODUCTINFOV2_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PRODUCTINFOV2_ID_GENERATOR")
	private Integer Id;
	private String productCode;
	private String productName;
	private String description;
	private int foodType;
	private int priority;
	private Integer brandId;
	private boolean active;
	private boolean stockStatus;
	private Timestamp createdOn;
	private Long createdBy;
	private Timestamp updatedOn;
	private Long updatedBy;

	public Integer getId() {
		return Id;
	}

	public int getFoodType() {
		return foodType;
	}

	public void setFoodType(int foodType) {
		this.foodType = foodType;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isStockStatus() {
		return stockStatus;
	}

	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

}
