package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  

@NamedQuery(name = "Wallet.getByCustomerId", 
query = "SELECT t from WalletDTO t "
        + " where t.customerId=:customerId "),

@NamedQuery(name = "Wallet.getTotalMadinMoney", 
query = "SELECT sum(d.myWalletBalance) from WalletDTO d "),

@NamedQuery(name = "Wallet.getPositiveMadinMoney", 
query = "SELECT sum(d.myWalletBalance) from WalletDTO d where d.myWalletBalance >= 0"),

@NamedQuery(name = "Wallet.getNegativeMadinMoney", 
query = "SELECT sum(d.myWalletBalance) from WalletDTO d where d.myWalletBalance < 0 ")
})

@Entity
@Table(name = "Wallet")	
public class WalletDTO {
	@Id
    @SequenceGenerator(name="WALLET_ID_GENERATOR", sequenceName="WALLET_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="WALLET_ID_GENERATOR") 

	private Long Id;
	private Long customerId;
	private float myWalletBalance;
	private Timestamp updatedOn;
	
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public float getMyWalletBalance() {
		return myWalletBalance;
	}
	public void setMyWalletBalance(float myWalletBalance) {
		this.myWalletBalance = myWalletBalance;
	}
	
}
