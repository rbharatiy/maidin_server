package com.maidin.persistence.dto.V2;

import java.sql.Date;

public class UpcomingOrderDTO {
	private long orderId;
	private long orderDetailsId;
	private long productDetailsId;
	private String productName;
	private String brandName;
	private String productUnit;
	private int quantity;
	private int categoryId;
	private int subCategoryId;
	private float salePrice;
	private float deliveryCharge;
	private float productCost;
	private String version;
	private int stockQuantity;
	private boolean active;
	private boolean stockStatus;
	private Date deliveryDate;
	private int timeSlotId;
	private float productSalePrice;
	private float productDeliveryCharge;
	
	public float getProductSalePrice() {
		return productSalePrice;
	}
	public void setProductSalePrice(float productSalePrice) {
		this.productSalePrice = productSalePrice;
	}
	public float getProductDeliveryCharge() {
		return productDeliveryCharge;
	}
	public void setProductDeliveryCharge(float productDeliveryCharge) {
		this.productDeliveryCharge = productDeliveryCharge;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public int getTimeSlotId() {
		return timeSlotId;
	}
	public void setTimeSlotId(int timeSlotId) {
		this.timeSlotId = timeSlotId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getOrderDetailsId() {
		return orderDetailsId;
	}
	public void setOrderDetailsId(long orderDetailsId) {
		this.orderDetailsId = orderDetailsId;
	}
	public long getProductDetailsId() {
		return productDetailsId;
	}
	public void setProductDetailsId(long productDetailsId) {
		this.productDetailsId = productDetailsId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductUnit() {
		return productUnit;
	}
	public void setProductUnit(String productUnit) {
		this.productUnit = productUnit;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public float getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}
	public float getDeliveryCharge() {
		return deliveryCharge;
	}
	public void setDeliveryCharge(float deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}
	public float getProductCost() {
		return productCost;
	}
	public void setProductCost(float productCost) {
		this.productCost = productCost;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public int getStockQuantity() {
		return stockQuantity;
	}
	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}
	
	
	
	

}
