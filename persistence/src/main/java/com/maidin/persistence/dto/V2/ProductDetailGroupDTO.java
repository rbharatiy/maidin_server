package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
	
	@NamedQuery(name = "ProductDetailGroup.getById", 
	query = "SELECT d from ProductDetailGroupDTO d "
			        + "where d.Id=:Id ")
})
@Entity
@Table(name = "ProductDetailGroup")
public class ProductDetailGroupDTO {
	@Id
    @SequenceGenerator(name="PRODUCTDETAILGROUP_ID_GENERATOR", sequenceName="PRODUCTDETAILGROUP_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="PRODUCTDETAILGROUP_ID_GENERATOR") 	
	private Long Id;
	public String getProductDetailIds() {
		return productDetailIds;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public void setProductDetailIds(String productDetailIds) {
		this.productDetailIds = productDetailIds;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	private String productDetailIds;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	
	
	
    
}
