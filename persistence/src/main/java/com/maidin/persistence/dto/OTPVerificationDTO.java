package com.maidin.persistence.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "OTPVerification")
@NamedQueries({  
@NamedQuery(name = "OTPVerification.getByMobileAndOtp", 
query = "SELECT o from OTPVerificationDTO o "
        + "where o.mobileNumber=:mobileNumber "
        + "and o.otp=:otp "
        + "and o.usercategory=:usercategory"),
@NamedQuery(name = "OTPVerification.getByMobile", 
query = "SELECT o from OTPVerificationDTO o "
        + "where o.mobileNumber=:mobileNumber "
        + "and o.usercategory=:usercategory")
})
public class OTPVerificationDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1434636155585561604L;
	@Id
	private String mobileNumber;

	private String otp;
	private Timestamp generatedOn;
	@Id
	private String usercategory;
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public Timestamp getGeneratedOn() {
		return generatedOn;
	}
	public void setGeneratedOn(Timestamp generatedOn) {
		this.generatedOn = generatedOn;
	}
	public String getUsercategory() {
		return usercategory;
	}
	public void setUsercategory(String usercategory) {
		this.usercategory = usercategory;
	}
	
	
}



