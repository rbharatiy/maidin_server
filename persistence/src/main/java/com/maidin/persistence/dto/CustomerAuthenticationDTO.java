package com.maidin.persistence.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({  

@NamedQuery(name = "CustomerAuthentication.getByMobileNumber", 
query = "SELECT da from CustomerAuthenticationDTO da "
        + "where da.mobileNumber=:mobileNumber ")
})

@Entity
@Table(name = "CustomerAuthentication")
public class CustomerAuthenticationDTO {
	@Id
    @SequenceGenerator(name="CUSTOMERAUTHENTICATION_ID_GENERATOR", sequenceName="CUSTOMERAUTHENTICATION_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="CUSTOMERAUTHENTICATION_ID_GENERATOR") 	
	
	private Long Id;
	private Long customerId;
	private String mobileNumber;
	private String authToken;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	
}
