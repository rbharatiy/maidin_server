package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({
	@NamedQuery(name = "CityCategoryInfo.getByCC", query = "SELECT t from CityCategoryInfoDTO t "
					        + " where t.cityCode=:cityCode and t.active=true"),
	@NamedQuery(name = "CityCategoryInfo.getByCityList", query = "SELECT t from CityCategoryInfoDTO t "
	        + " where t.cityCode in (:cities) "),
	@NamedQuery(name = "CityCategoryInfo.getByAllCities", query = "SELECT t.cityCode from CityCategoryInfoDTO t "
	        + " where t.active=:active"),
	@NamedQuery(name = "CityCategoryInfo.getAllCityCategories", query = "SELECT t from CityCategoryInfoDTO t "
	        + " where t.active=:active"),
	@NamedQuery(name = "CityCategoryInfo.getAllCitiesWithoutActiveState", query = "SELECT t.cityCode from CityCategoryInfoDTO t ")
})


@Entity
@Table(name = "CityCategoryInfo")
public class CityCategoryInfoDTO {
	@Id
    @SequenceGenerator(name="CITYCATEGORYINFO_ID_GENERATOR", sequenceName="CITYCATEGORYINFO_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="CITYCATEGORYINFO_ID_GENERATOR") 	
	private Integer Id;
	private String cityCode;
	private String geoLocId;
	private String cityName;
	private String stateName;
	private String country;
	private String categoryIds;
	private String subCategoryIds;
	private boolean active;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getGeoLocId() {
		return geoLocId;
	}
	public void setGeoLocId(String geoLocId) {
		this.geoLocId = geoLocId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCategoryIds() {
		return categoryIds;
	}
	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}
	public String getSubCategoryIds() {
		return subCategoryIds;
	}
	public void setSubCategoryIds(String subCategoryIds) {
		this.subCategoryIds = subCategoryIds;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
