package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({  
/*	@NamedQuery(name = "Product.getAllProductsByBrandID", 
	query = "SELECT p from ProductDTO p "
			        + "where p.brandId=:brandId"
			        + " and p.active=true order by IF(p.productName LIKE 'Maidin%',1,2), p.productName"),*/
	@NamedQuery(name = "Product.getByProductName", 
			query = "SELECT p from ProductDTO p where p.productName LIKE :productSubString"),
	@NamedQuery(name = "Product.getOnlyActiveByProductName", 
	query = "SELECT p from ProductDTO p "
			        + " where p.productName LIKE :productSubString and p.active=true"),
	@NamedQuery(name = "Product.getByProductId", 
	query = "SELECT p from ProductDTO p "
			        + " where p.Id=:Id"
			        + " and p.active=true"),
	@NamedQuery(name = "Product.getById", 
	query = "SELECT p from ProductDTO p "
			        + " where p.Id=:Id")/*,
	@NamedQuery(name = "Product.getAllExistingProductsByBrandIdWithoutActiveState", 
	query = "SELECT p from ProductDTO p "
			        + "where p.brandId=:brandId order by IF(p.productName LIKE 'Maidin%',1,2), p.productName")*/
})

@Entity
@Table(name = "Product")
public class ProductDTO {
	@Id
    @SequenceGenerator(name="PRODUCT_ID_GENERATOR", sequenceName="PRODUCT_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="PRODUCT_ID_GENERATOR") 	
	private Integer Id;
	private String productCode;
	private String productName;
	private	String quantityUnit;
	private Float price;
	private Integer brandId;
	private boolean active;
	private boolean stockStatus;
	private Timestamp updatedOn;
	private Long updatedBy;
	
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	//private Long categoryId;
	
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getQuantityUnit() {
		return quantityUnit;
	}
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}

	
	/*public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}*/
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}
	
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
}
