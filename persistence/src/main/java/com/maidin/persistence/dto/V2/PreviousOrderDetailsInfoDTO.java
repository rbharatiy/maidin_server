package com.maidin.persistence.dto.V2;

public class PreviousOrderDetailsInfoDTO {
	private long orderId;
	private long orderDetailsId;
	private long productId;
	private long productDetailsId;
	private String productName;
	private String brandName;
	private String quantityUnit;
	private int foodType;
	private int quantity;
	private int categoryId;
	private int subCategoryId;
	private float totalPrice;
	private float amountPaidByCustomer;
	private float orderMRP;
	private float orderSalePrice;
	private float orderDiscount;
	private float orderDiscountUnit;
	private float orderSavedAmount;
	private float orderDeliveryCharge;
	private String orderStatus;
	private int paymentModeId;
	/*private String paymentMode;
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}*/
	private String orderedTime;
	public int getPaymentModeId() {
		return paymentModeId;
	}
	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}
	private Long photoId;
	private float productMRP;
	private float productSalePrice;
	private float productDiscount;
	private String productDiscountUnit;
	private float productSavedAmount;
	private float productDeliveryCharge;
	private boolean productActiveStatus;
	private boolean productStockStatus;
	private int productStockQuantity;
	
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getOrderDetailsId() {
		return orderDetailsId;
	}
	public void setOrderDetailsId(long orderDetailsId) {
		this.orderDetailsId = orderDetailsId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getProductDetailsId() {
		return productDetailsId;
	}
	public void setProductDetailsId(long productDetailsId) {
		this.productDetailsId = productDetailsId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getQuantityUnit() {
		return quantityUnit;
	}
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}
	
	public Long getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}
	public int getFoodType() {
		return foodType;
	}
	public void setFoodType(int foodType) {
		this.foodType = foodType;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public float getAmountPaidByCustomer() {
		return amountPaidByCustomer;
	}
	public void setAmountPaidByCustomer(float amountPaidByCustomer) {
		this.amountPaidByCustomer = amountPaidByCustomer;
	}
	public float getOrderMRP() {
		return orderMRP;
	}
	public void setOrderMRP(float orderMRP) {
		this.orderMRP = orderMRP;
	}
	public float getOrderSalePrice() {
		return orderSalePrice;
	}
	public void setOrderSalePrice(float orderSalePrice) {
		this.orderSalePrice = orderSalePrice;
	}
	public float getOrderDiscount() {
		return orderDiscount;
	}
	public void setOrderDiscount(float orderDiscount) {
		this.orderDiscount = orderDiscount;
	}
	public float getOrderDiscountUnit() {
		return orderDiscountUnit;
	}
	public void setOrderDiscountUnit(float orderDiscountUnit) {
		this.orderDiscountUnit = orderDiscountUnit;
	}
	public float getOrderSavedAmount() {
		return orderSavedAmount;
	}
	public void setOrderSavedAmount(float orderSavedAmount) {
		this.orderSavedAmount = orderSavedAmount;
	}
	public float getOrderDeliveryCharge() {
		return orderDeliveryCharge;
	}
	public void setOrderDeliveryCharge(float orderDeliveryCharge) {
		this.orderDeliveryCharge = orderDeliveryCharge;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	/*public int getPaymentModeId() {
		return paymentModeId;
	}
	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}*/
	public String getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(String orderedTime) {
		this.orderedTime = orderedTime;
	}
	public float getProductMRP() {
		return productMRP;
	}
	public void setProductMRP(float productMRP) {
		this.productMRP = productMRP;
	}
	public float getProductSalePrice() {
		return productSalePrice;
	}
	public void setProductSalePrice(float productSalePrice) {
		this.productSalePrice = productSalePrice;
	}
	public float getProductDiscount() {
		return productDiscount;
	}
	public void setProductDiscount(float productDiscount) {
		this.productDiscount = productDiscount;
	}
	
	public String getProductDiscountUnit() {
		return productDiscountUnit;
	}
	public void setProductDiscountUnit(String productDiscountUnit) {
		this.productDiscountUnit = productDiscountUnit;
	}
	public float getProductSavedAmount() {
		return productSavedAmount;
	}
	public void setProductSavedAmount(float productSavedAmount) {
		this.productSavedAmount = productSavedAmount;
	}
	public float getProductDeliveryCharge() {
		return productDeliveryCharge;
	}
	public void setProductDeliveryCharge(float productDeliveryCharge) {
		this.productDeliveryCharge = productDeliveryCharge;
	}
	public boolean isProductActiveStatus() {
		return productActiveStatus;
	}
	public void setProductActiveStatus(boolean productActiveStatus) {
		this.productActiveStatus = productActiveStatus;
	}
	public boolean isProductStockStatus() {
		return productStockStatus;
	}
	public void setProductStockStatus(boolean productStockStatus) {
		this.productStockStatus = productStockStatus;
	}
	public int getProductStockQuantity() {
		return productStockQuantity;
	}
	public void setProductStockQuantity(int productStockQuantity) {
		this.productStockQuantity = productStockQuantity;
	}
	
	
	
	

}
