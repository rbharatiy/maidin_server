package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
@NamedQuery(name = "Cart.getByCustomerIdAndProductId", 
			query = "SELECT t from CartDTO t "
			        + " where t.customerId=:customerId "
			        + " and productId=:productId"),
@NamedQuery(name = "Cart.getByCustomerId", 
query = "SELECT t from CartDTO t "
        + " where t.customerId=:customerId "),
@NamedQuery(name = "Cart.getByCartId", 
query = "SELECT t from CartDTO t "
        + " where t.Id=:Id ")
})

@Entity
@Table(name = "Cart")	
public class CartDTO {
	@Id
    @SequenceGenerator(name="CART_ID_GENERATOR", sequenceName="CART_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="CART_ID_GENERATOR") 

	private Long Id;
	private Long customerId;
	private Integer productId;
	//private String quantityUnit;
	private int quantity;
	//private String productCost;
	//private String totalCost;
	private int timeSlotId;
	//private Timestamp orderedFrom;
	private Timestamp deliveryTime;
	private Timestamp orderedTime;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	
	/*public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}*/
	/*public String getQuantityUnit() {
		return quantityUnit;
	}
	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}*/
	/*public String getProductCost() {
		return productCost;
	}
	public void setProductCost(String productCost) {
		this.productCost = productCost;
	}*/
	
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/*public Timestamp getOrderedTo() {
		return orderedTo;
	}
	public void setOrderedTo(Timestamp orderedTo) {
		this.orderedTo = orderedTo;
	}*/
	public int getTimeSlotId() {
		return timeSlotId;
	}
	public void setTimeSlotId(int timeSlotId) {
		this.timeSlotId = timeSlotId;
	}
	public Timestamp getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(Timestamp deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	/*public Timestamp getOrderedFrom() {
		return orderedFrom;
	}
	public void setOrderedFrom(Timestamp orderedFrom) {
		this.orderedFrom = orderedFrom;
	}
	public Timestamp getOrderTo() {
		return orderedTo;
	}
	public void setOrderTo(Timestamp orderTo) {
		this.orderedTo = orderTo;
	}*/
	public Timestamp getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}
	
	
	
	
}
