package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({

@NamedQuery(name = "Address.getById", 
query = "SELECT da from AddressDTO da "
        + "where da.Id=:Id "),
@NamedQuery(name = "Address.getByFlatId", 
query = "SELECT da from AddressDTO da "
        + " where da.flatId=:flatId "),
@NamedQuery(name = "Address.getByRegisteredFlatId", 
query = "SELECT da from AddressDTO da , CustomerDTO c"
        + " where da.flatId=:flatId and da.Id = c.addressId and c.active=true"),
@NamedQuery(name = "Address.getByIds", 
query = "SELECT da from AddressDTO da "
        + " where da.Id in :Ids "),
})

@Entity
@Table(name = "Address")	
public class AddressDTO {
	@Id
    @SequenceGenerator(name="ADDRESS_ID_GENERATOR", sequenceName="ADDRESS_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ADDRESS_ID_GENERATOR") 	
	private Long Id;
	private Long flatId;
	private String cityCode;
	private String city;
	private String project;
	private String tower;
	private String flat;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long updatedBy;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getFlatId() {
		return flatId;
	}
	public void setFlatId(Long flatId) {
		this.flatId = flatId;
	}
	
	
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getTower() {
		return tower;
	}
	public void setTower(String tower) {
		this.tower = tower;
	}
	public String getFlat() {
		return flat;
	}
	public void setFlat(String flat) {
		this.flat = flat;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	
	
}
