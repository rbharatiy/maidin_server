package com.maidin.persistence.dto;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@NamedQueries({  
@NamedQuery(name = "BlockedSlotInfo.getByBlockedDate", 
query = "SELECT t from BlockedSlotDTO t "
		+ "where t.blockedDate=:blockedDate "
		+ "and t.active=true")
})



@Entity
@Table(name = "BlockedSlotInfo")
public class BlockedSlotDTO {
	@Id
	@SequenceGenerator(name = "BLOCKEDSLOTINFO_ID_GENERATOR", sequenceName = "BLOCKEDSLOTINFO_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "BLOCKEDSLOTINFO_ID_GENERATOR")
	  
	private Long Id;
	private Date blockedDate;
	private String timeSlotIds;
	private String dateFormatter;
	private boolean active;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Date getBlockedDate() {
		return blockedDate;
	}
	public void setBlockedDate(Date blockedDate) {
		this.blockedDate = blockedDate;
	}
	public String getTimeSlotIds() {
		return timeSlotIds;
	}
	public void setTimeSlotIds(String timeSlotIds) {
		this.timeSlotIds = timeSlotIds;
	}
	
	public String getDateFormatter() {
		return dateFormatter;
	}
	public void setDateFormatter(String dateFormatter) {
		this.dateFormatter = dateFormatter;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
		}
