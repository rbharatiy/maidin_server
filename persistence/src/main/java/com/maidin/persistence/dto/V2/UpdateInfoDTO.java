package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({
	@NamedQuery(name="UpdateInfo.getByText", query="Select b from UpdateInfoDTO b where b.parameterText =:parameterText )")
	
})

@Entity
@Table(name = "UpdateInfo")
public class UpdateInfoDTO {
	@Id
    @SequenceGenerator(name="UPDATEINFO_ID_GENERATOR", sequenceName="UPDATEINFO_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="UPDATEINFO_ID_GENERATOR") 	
	private Integer Id;
	private String parameterText;
	private Timestamp updatedOn;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	
	public String getParameterText() {
		return parameterText;
	}
	public void setParameterText(String parameterText) {
		this.parameterText = parameterText;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	
}
