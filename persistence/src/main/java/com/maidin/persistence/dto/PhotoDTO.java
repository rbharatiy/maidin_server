package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Photo")
@NamedQueries({
		@NamedQuery(name = "Photo.getPhotoByImageTypeAndImageId", query = "SELECT p from PhotoDTO p "
				+ "where p.imageType=:imageType "
				+ "and p.itemId=:itemId "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getByPhotoTypeAndPhotoIdVersionAndPrimaryPhoto", query = "SELECT p from PhotoDTO p "
				+ "where p.imageType=:imageType "
				+ "and p.itemId=:itemId "
				+ "and p.version=:version "
				+ "and p.primaryPhoto=:primaryPhoto "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getById", query = "SELECT p from PhotoDTO p "
				+ "where p.id=:id "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getByPhotoTypePhotoIdAndVersion", query = "SELECT p from PhotoDTO p "
				+ "where p.imageType=:imageType "
				+ "and p.itemId=:itemId "
				+ "and p.version=:version "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getByPhotoTypePhotoIdVersionAndProductDetailIds", query = "SELECT p from PhotoDTO p "
				+ "where p.imageType=:imageType "
				+ "and p.itemId=:itemId "
				+ "and p.version=:version "
				+ "and productDetailIds is null "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getByIds", query = "SELECT p from PhotoDTO p "
				+ "where p.id in (:photoIds) "
				+ "and p.version='v2' "
				+ "and p.deleted=false "
				+ "and p.active=true"),
		@NamedQuery(name = "Photo.getByFileStoreId", query = "SELECT p from PhotoDTO p "
				+ "where p.fileStoreId=:fileStoreId")
		})
public class PhotoDTO {

	@Id
	@SequenceGenerator(name = "PHOTOS_ID_GENERATOR", sequenceName = "PHOTOS_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PHOTOS_ID_GENERATOR")
	private Long id;
	
	private int imageType;
	
	// itemId is Id of product/category/brand
	private Long itemId;
	
	private Long fileStoreId;
	
	private boolean primaryPhoto;
	
	//private Long productDetailId;
	private Long itemIdV1;
	
	private String version;
	
	private Timestamp createdOn;
	
	private Long createdBy;
	
	private Timestamp updatedOn;
	
	private Long updatedBy;
	
	private boolean active;

	private boolean deleted;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the imageType
	 */
	public int getImageType() {
		return imageType;
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(int imageType) {
		this.imageType = imageType;
	}

	/**
	 * @return the itemId
	 */
	public Long getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the fileStoreId
	 */
	public Long getFileStoreId() {
		return fileStoreId;
	}

	/**
	 * @param fileStoreId the fileStoreId to set
	 */
	public void setFileStoreId(Long fileStoreId) {
		this.fileStoreId = fileStoreId;
	}

	/**
	 * @return the primaryPhoto
	 */
	public boolean isPrimaryPhoto() {
		return primaryPhoto;
	}

	/**
	 * @param primaryPhoto the primaryPhoto to set
	 */
	public void setPrimaryPhoto(boolean primaryPhoto) {
		this.primaryPhoto = primaryPhoto;
	}

	

	/*public Long getProductDetailId() {
		return productDetailId;
	}

	public void setProductDetailId(Long productDetailId) {
		this.productDetailId = productDetailId;
	}*/

	public Long getItemIdV1() {
		return itemIdV1;
	}

	public void setItemIdV1(Long itemIdV1) {
		this.itemIdV1 = itemIdV1;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the createdBy
	 */
	public Long getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the updatedBy
	 */
	public Long getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted() {
		return deleted;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
}
