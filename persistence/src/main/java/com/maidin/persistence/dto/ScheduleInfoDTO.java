package com.maidin.persistence.dto;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({
		@NamedQuery(name = "ScheduleInfo.getById", query = "SELECT t from ScheduleInfoDTO t " + "where t.Id=:Id and t.active=true"),
		@NamedQuery(name = "ScheduleInfo.getByCustomerId", query = "SELECT t from ScheduleInfoDTO t "
				+ "where t.customerId=:customerId and t.active=true order by t.Id desc"),
		/*@NamedQuery(name = "ScheduleInfo.getSchedulesOfSundayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Su=:Su and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfMondayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Mo=:Mo and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfTuesdayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Tu=:Tu and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfWednesdayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.We=:We and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfThursdayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Th=:Th and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfFridayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Fr=:Fr and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfSaturdayWithPauseDate", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Sa=:Sa and p.pauseDate!=:deliveryDate and p.startDate <=:deliveryDate"),
		*/@NamedQuery(name = "ScheduleInfo.getSchedulesOfSunday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Su=:Su and p.active=true"),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfMonday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Mo=:Mo and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfTuesday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Tu=:Tu and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfWednesday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.We=:We and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfThursday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Th=:Th and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfFriday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Fr=:Fr and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getSchedulesOfSaturday", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.scheduleStatus=:scheduleStatus" + " and p.Sa=:Sa and p.active=true "),
		
		@NamedQuery(name = "ScheduleInfo.getByCustomerIdAndScheduleStatus", query = "SELECT p from ScheduleInfoDTO p "
				+ " where p.customerId=:customerId" + " and p.scheduleStatus=:scheduleStatus and p.active=true"),
		@NamedQuery(name = "ScheduleInfo.getByIds", query = "SELECT p from ScheduleInfoDTO p "
				 + " where p.Id IN (:Ids) and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getTodaysRestSchedule", query = "SELECT p from ScheduleInfoDTO p ,TimeSlotDTO t"
				 + " where p.timeSlotId = t.Id and p.customerId=:customerId and p.scheduleStatus=:scheduleStatus and  t.StartTime > hour(current_time()) and p.active=true"),
		@NamedQuery(name = "ScheduleInfo.getNextScheduleDate", query = "SELECT p from ScheduleInfoDTO p "
				 + " where p.customerId=:customerId and scheduleStatus=:scheduleStatus and p.active=true "),
		@NamedQuery(name = "ScheduleInfo.getTimeSlotsByIds", query = "SELECT p.timeSlotId from ScheduleInfoDTO p "
				 + " where p.Id in (:Ids) and p.active=true"),
		@NamedQuery(name = "ScheduleInfo.getByCustomerIdScheduleStatusAndTimeSlotId", query = "SELECT p from ScheduleInfoDTO p "
				 + " where p.customerId=:customerId and p.timeSlotId=:timeSlotId and p.scheduleStatus=:scheduleStatus and p.active=true"),
		@NamedQuery(name = "ScheduleInfo.getByCustomerIdProductIdAndTimeSlotId", query = "SELECT p from ScheduleInfoDTO p "
				 + " where p.customerId=:customerId and p.timeSlotId=:timeSlotId and p.productId=:productId and p.active=true"),

})

@Entity
@Table(name = "ScheduleInfo")
public class ScheduleInfoDTO {
	@Id
	@SequenceGenerator(name = "SCHEDULEINFO_ID_GENERATOR", sequenceName = "SCHEDULEINFO_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SCHEDULEINFO_ID_GENERATOR")

	private Long Id;
	private Long customerId;
	private Integer timeSlotId;
	private Integer productId;
	private int quantity;
	private Date startDate;
	private Timestamp pauseDate;
	private boolean Mo;
	private boolean Tu;
	private boolean We;
	private boolean Th;
	private boolean Fr;
	private boolean Sa;
	private boolean Su;
	private Integer scheduleStatus;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private boolean recurring;
	private Date scheduledUpto;
	private Date scheduleEndDate;
	private String version;
	private boolean active;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Integer getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(Integer timeSlotId) {
		this.timeSlotId = timeSlotId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public Timestamp getPauseDate() {
		return pauseDate;
	}

	public void setPauseDate(Timestamp pauseDate) {
		this.pauseDate = pauseDate;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public boolean isMo() {
		return Mo;
	}

	public void setMo(boolean mo) {
		Mo = mo;
	}

	public boolean isTu() {
		return Tu;
	}

	public void setTu(boolean tu) {
		Tu = tu;
	}

	public boolean isWe() {
		return We;
	}

	public void setWe(boolean we) {
		We = we;
	}

	public boolean isTh() {
		return Th;
	}

	public void setTh(boolean th) {
		Th = th;
	}

	public boolean isFr() {
		return Fr;
	}

	public void setFr(boolean fr) {
		Fr = fr;
	}

	public boolean isSa() {
		return Sa;
	}

	public void setSa(boolean sa) {
		Sa = sa;
	}

	public boolean isSu() {
		return Su;
	}

	public void setSu(boolean su) {
		Su = su;
	}

	public boolean isRecurring() {
		return recurring;
	}

	public Integer getScheduleStatus() {
		return scheduleStatus;
	}

	public void setScheduleStatus(Integer scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public void setRecurring(boolean recurring) {
		this.recurring = recurring;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Date getScheduledUpto() {
		return scheduledUpto;
	}

	public void setScheduledUpto(Date scheduledUpto) {
		this.scheduledUpto = scheduledUpto;
	}

	public Date getScheduleEndDate() {
		return scheduleEndDate;
	}

	public void setScheduleEndDate(Date scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
}
