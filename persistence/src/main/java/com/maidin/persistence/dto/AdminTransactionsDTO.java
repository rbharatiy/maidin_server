package com.maidin.persistence.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;

public class AdminTransactionsDTO {

	private long customerTransactionId;
	//private String transactionDescription;
	private int transactionDescription;
	private String productName;
	private String quantityUnit;
	private int quantity;
	private float price;
	private float totalCost;
	private float moneyIn;
	private float moneyOut;
	private float dueAmount;
	private String orderStatus;
	private String paymentMode;
	private String adminName;
	private Long transactionDate;
	private Long orderedTime;
	private Integer timeSlotId;

	public long getCustomerTransactionId() {
		return customerTransactionId;
	}

	public void setCustomerTransactionId(long customerTransactionId) {
		this.customerTransactionId = customerTransactionId;
	}

	
	/*public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}*/

	public int getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(int transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}

	public float getMoneyIn() {
		return moneyIn;
	}

	public void setMoneyIn(float moneyIn) {
		this.moneyIn = moneyIn;
	}

	public float getMoneyOut() {
		return moneyOut;
	}

	public void setMoneyOut(float moneyOut) {
		this.moneyOut = moneyOut;
	}

	public float getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(float dueAmount) {
		this.dueAmount = dueAmount;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public Long getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Long transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Long getOrderedTime() {
		return orderedTime;
	}

	public void setOrderedTime(Long orderedTime) {
		this.orderedTime = orderedTime;
	}

	public Integer getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(Integer timeSlotId) {
		this.timeSlotId = timeSlotId;
	}

}
