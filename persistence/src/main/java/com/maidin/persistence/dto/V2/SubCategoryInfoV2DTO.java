package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "SubCategoryInfoV2.getAllSubCategories", query = "SELECT d from SubCategoryInfoV2DTO d "
				+ " where d.Id in (:Ids) and d.active=true order by field(d.Id, :Ids)"),
		@NamedQuery(name = "SubCategoryInfoV2.getAllSubCategoriesWithoutActiveState", query = "SELECT d from SubCategoryInfoV2DTO d "
				+ " where d.Id in (:Ids) order by field(d.Id, :Ids)"),
		@NamedQuery(name = "SubCategoryInfoV2.getById", query = "SELECT d from SubCategoryInfoV2DTO d "
				+ " where d.Id=:Id and d.active=true "),
		@NamedQuery(name = "SubCategoryInfoV2.getByIdWithoutActiveStatus", query = "SELECT d from SubCategoryInfoV2DTO d "
				+ " where d.Id=:Id "),
		@NamedQuery(name = "SubCategoryInfoV2.getByIdsWithActiveStatus", query = "SELECT d.Id from SubCategoryInfoV2DTO d "
				+ " where d.Id in (:Ids) and active = true"),
		@NamedQuery(name = "SubCategoryInfoV2.getByIdsWithoutActiveStatus", query = "SELECT d.Id from SubCategoryInfoV2DTO d "
				+ " where d.Id in (:Ids)"),
		@NamedQuery(name = "SubCategoryInfoV2.getAllMatchingTextWithActivationStatus", query = "SELECT distinct p.Id from SubCategoryInfoV2DTO p "
		+ " where p.subCategory LIKE :productSubString and p.active=:active"),
		@NamedQuery(name = "SubCategoryInfoV2.getAll", query = "SELECT d from SubCategoryInfoV2DTO d "),
		})

@Entity
@Table(name = "SubCategoryInfoV2")
public class SubCategoryInfoV2DTO {
	@Id
	@SequenceGenerator(name = "SUBCATEGORYINFOV2_ID_GENERATOR", sequenceName = "SUBCATEGORYINFOV2_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "SUBCATEGORYINFOV2_ID_GENERATOR")
	private Integer Id;
	private String subCategory;
	private int brandIdV1;
	private Long photoId;
	private boolean active;
	private boolean stockStatus;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;

	public Long getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public int getBrandIdV1() {
		return brandIdV1;
	}

	public void setBrandIdV1(int brandIdV1) {
		this.brandIdV1 = brandIdV1;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isStockStatus() {
		return stockStatus;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

}
