package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
@NamedQuery(name = "PaymentMode.getById", 
			query = "SELECT t from PaymentModeDTO t "
			        + "where t.Id=:Id "),
@NamedQuery(name = "PaymentMode.getByPaymentMode", 
query = "SELECT t from PaymentModeDTO t "
        + "where t.paymentMode=:paymentMode "),
@NamedQuery(name = "PaymentMode.getAll", 
query = "SELECT t from PaymentModeDTO t "),
@NamedQuery(name = "PaymentMode.getAllPaymentMode", 
query = "SELECT t from PaymentModeDTO t"
		+ " where t.displayedInApp=:displayedInApp and "
		+ " t.active=:active order by t.priority ")

})

@Entity
@Table(name = "PaymentMode")	
public class PaymentModeDTO {
	@Id
    @SequenceGenerator(name="PAYMENTMODE_ID_GENERATOR", sequenceName="PAYMENTMODE_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="PAYMENTMODE_ID_GENERATOR") 

	private Integer Id;
	private String paymentMode;
	private String paymentOption;
	private String byPaymentMode;
	private String paymentModeAbbr;
	private int priority;
	private boolean displayedInApp;
	private Long photoId;
	private boolean active;
	private Timestamp createdOn;
	
	
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public boolean isDisplayedInApp() {
		return displayedInApp;
	}
	public void setDisplayedInApp(boolean displayedInApp) {
		this.displayedInApp = displayedInApp;
	}
	public Long getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}
	public boolean isActive() {
		return active;
	}
	public String getByPaymentMode() {
		return byPaymentMode;
	}
	public void setByPaymentMode(String byPaymentMode) {
		this.byPaymentMode = byPaymentMode;
	}
	public String getPaymentModeAbbr() {
		return paymentModeAbbr;
	}
	public void setPaymentModeAbbr(String paymentModeAbbr) {
		this.paymentModeAbbr = paymentModeAbbr;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
	
	
}
