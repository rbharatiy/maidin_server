package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/*@NamedQueries({  
@NamedQuery(name = "DayWiseOrderDetails.getByCustomerId", 
			query = "SELECT t from CustomerDTO t "
			        + "where t.customerId=:customerId ")

})*/

@Entity
@Table(name = "DayWiseOrderDetails")	
public class DayWiseOrderDetailsDTO {
	@Id
    @SequenceGenerator(name="CART_ID_GENERATOR", sequenceName="CART_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="CART_ID_GENERATOR") 

	private Long Id;
	private Long productId;
	private Long workingDayMonFri;
	
	private Long orderedFrom;
	private Long orderTo;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	
	
}
