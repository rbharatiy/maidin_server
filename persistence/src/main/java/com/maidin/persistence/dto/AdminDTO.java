package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({  
@NamedQuery(name = "Admin.getByMobileNumber", 
query = "SELECT t from AdminDTO t "
		+ "where t.mobileNumber=:mobileNumber "
		+ "and t.active=true"),
@NamedQuery(name = "Admin.getByMobileNumberWithoutActiveState", 
query = "SELECT d from AdminDTO d "
        + "where d.mobileNumber=:mobileNumber "),
@NamedQuery(name = "Admin.getByEmail", 
query = "SELECT d from AdminDTO d "
        + "where d.email=:email "),
@NamedQuery(name = "Admin.getByIdAndMobileNumber", 
query = "SELECT d from AdminDTO d "
        + " where d.Id=:Id "
		+ " and d.mobileNumber=:mobileNumber and d.active=true")
})

@Entity
@Table(name = "Admin")	
public class AdminDTO {
	@Id
    @SequenceGenerator(name="ADMIN_ID_GENERATOR", sequenceName="ADMIN_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ADMIN_ID_GENERATOR") 	
	private Long Id;
	private String fullName;
	private String email;
	private boolean emailVerified;
	private String mobileNumber;
	private String gender;
	private String city;
	private String project;
	private boolean active;
	private Timestamp createdOn;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
