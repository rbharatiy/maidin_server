package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="FileStore")
@NamedQueries({
	@NamedQuery(name = "FileStore.getById", 
    query = "SELECT f from FileStoreDTO f "
            + "where f.id=:id "
            /*+ "and f.deleted=false "*/
            + "and f.active=true"),
	@NamedQuery(name = "FileStore.getFileByFileName", 
    query = "SELECT f from FileStoreDTO f "
            + "where f.fileName=:fileName "
            /*+ "and f.deleted=false "*/
            + "and f.active=true")
})
public class FileStoreDTO {
    
    @Id
    @SequenceGenerator(name="FILESTORE_ID_GENERATOR", sequenceName="FILESTORE_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="FILESTORE_ID_GENERATOR")
    private Long id;
    
    private String fileName;
    
    private boolean encrypted;
    
    private long fileSize;
    
    private String md5;
    
    private String contentType;
    
    private Long createdBy;
    
    private Timestamp createdOn;
    
    private Long updatedBy;
    
    private Timestamp updatedOn;
    
    private boolean active;
    
    private boolean deleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isEncrypted() {
		return encrypted;
	}

	public void setEncrypted(boolean encrypted) {
		this.encrypted = encrypted;
	}

	public long getFileSize() {
		return fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
        
}
