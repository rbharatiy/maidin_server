package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({  
	/*@NamedQuery(name = "ProductBrands.getAllBrandsByCategoryID", 
	query = "SELECT distinct b from ProductBrandsDTO b "
			        + " where b.categoryId=:categoryId"
			        + " and b.active=true order by IF(b.brand LIKE 'Maidin%',1,2), b.brand"),*/
	@NamedQuery(name = "ProductBrands.getById", 
	query = "SELECT distinct b from ProductBrandsDTO b "
			        + " where b.Id=:Id")/*,
	@NamedQuery(name = "ProductBrands.getAllBrandsByCategoryIDWithoutActiveState", 
	query = "SELECT * from ProductBrands b "
			        + " where b.categoryId=:categoryId order by IF(b.brand LIKE 'Maidin%',1,2), b.brand")*/
})

@Entity
@Table(name = "ProductBrands")
public class ProductBrandsDTO {
	@Id
    @SequenceGenerator(name="PRODUCTBRANDS_ID_GENERATOR", sequenceName="PRODUCTBRANDS_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="PRODUCTBRANDS_ID_GENERATOR") 	
	private Integer Id;
	private String brand;
	private Integer categoryId;
	private boolean active;
	private boolean stockStatus;
	private Timestamp updatedOn;
	private Long updatedBy;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	
	
	
	
	public String getBrand() {
		return brand;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public boolean isStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
}
