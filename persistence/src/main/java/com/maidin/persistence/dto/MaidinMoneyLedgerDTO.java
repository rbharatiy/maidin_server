package com.maidin.persistence.dto;

import java.sql.Timestamp;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({

		@NamedQuery(name = "MaidinMoneyLedger.getByDate", query = "SELECT t from MaidinMoneyLedgerDTO t "
				+ " where t.date=:date "),

		@NamedQuery(name = "MaidinMoneyLedger.getByPreviousDay", query = "SELECT t from MaidinMoneyLedgerDTO t "
				+ " where t.date=curDate()-1 "),
		
		@NamedQuery(name = "MaidinMoneyLedger.getBetweenDates", query = "SELECT t from MaidinMoneyLedgerDTO t "
				+ " where t.date BETWEEN :fromDate AND :toDate ") })

@Entity
@Table(name = "MaidinMoneyLedger")
public class MaidinMoneyLedgerDTO {
	@Id
	@SequenceGenerator(name = "MAIDINMONEYLEDGER_ID_GENERATOR", sequenceName = "MAIDINMONEYLEDGER_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "MAIDINMONEYLEDGER_ID_GENERATOR")

	private Long Id;
	private double positiveBalance;
	private double negativeBalance;
	private Date date;
	private Timestamp CreatedOn;

	public double getPositiveBalance() {
		return positiveBalance;
	}

	public void setPositiveBalance(double positiveBalance) {
		this.positiveBalance = positiveBalance;
	}

	public double getNegativeBalance() {
		return negativeBalance;
	}

	public void setNegativeBalance(double negativeBalance) {
		this.negativeBalance = negativeBalance;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Timestamp getCreatedOn() {
		return CreatedOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		CreatedOn = createdOn;
	}

}
