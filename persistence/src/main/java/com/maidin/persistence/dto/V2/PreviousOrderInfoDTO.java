package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;
import java.util.Date;

public class PreviousOrderInfoDTO {

	private long orderId;

	//private Long flatId;
	private Long addressId;
	private Date deliveryDate;
	private String timeSlot;
	private String paymentOption;
	private float totalPaid;
	private float totalProductCost;
	private float totalPending;
	private float totalDeliveryCharge;
	private int noOfItems;
	private Long scheduleId;
	private Timestamp orderedTime;
	private int orderStatus;
	
	public int getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}
	public long getOrderId() {
		return orderId;
	}
	public int getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(int noOfItems) {
		this.noOfItems = noOfItems;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	/*public Long getFlatId() {
		return flatId;
	}
	public void setFlatId(Long flatId) {
		this.flatId = flatId;
	}*/
	
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getTimeSlot() {
		return timeSlot;
	}
	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}
	public String getPaymentOption() {
		return paymentOption;
	}
	public void setPaymentOption(String paymentOption) {
		this.paymentOption = paymentOption;
	}
	public float getTotalPaid() {
		return totalPaid;
	}
	public void setTotalPaid(float totalPaid) {
		this.totalPaid = totalPaid;
	}
	public float getTotalProductCost() {
		return totalProductCost;
	}
	public void setTotalProductCost(float totalProductCost) {
		this.totalProductCost = totalProductCost;
	}
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	public float getTotalPending() {
		return totalPending;
	}
	public void setTotalPending(float totalPending) {
		this.totalPending = totalPending;
	}
	public float getTotalDeliveryCharge() {
		return totalDeliveryCharge;
	}
	public void setTotalDeliveryCharge(float totalDeliveryCharge) {
		this.totalDeliveryCharge = totalDeliveryCharge;
	}
	public Long getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}
	public Timestamp getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}
	
}
