package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({
	@NamedQuery(name="BrandInfoV2.getByBrandIds", query="Select b from BrandInfoV2DTO b where b.Id in (:Ids)"),
	@NamedQuery(name="BrandInfoV2.getById", query="Select b from BrandInfoV2DTO b where b.Id=:Id "),
	@NamedQuery(name="BrandInfoV2.getByBrandName", query="Select b from BrandInfoV2DTO b where b.brand=:brand "),
	@NamedQuery(name = "BrandInfoV2.getAll", 
	query = "SELECT d from BrandInfoV2DTO d order by brand"),
	@NamedQuery(name = "BrandInfoV2.getByTextWithActivationStatus", query = "SELECT distinct p.Id from BrandInfoV2DTO p "
			+ " where p.brand LIKE :productSubString and p.active=:active")
})

@Entity
@Table(name = "BrandInfoV2")
public class BrandInfoV2DTO {
	@Id
    @SequenceGenerator(name="BRANDINFOV2_ID_GENERATOR", sequenceName="BRANDINFOV2_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="BRANDINFOV2_ID_GENERATOR") 	
	private Integer Id;
	private String brand;
	private boolean active;
	private boolean stockStatus;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
		
	
	
}
