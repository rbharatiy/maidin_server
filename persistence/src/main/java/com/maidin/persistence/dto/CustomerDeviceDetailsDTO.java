package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({

		@NamedQuery(name = "CustomerDeviceDetails.getById", query = "SELECT t from CustomerDeviceDetailsDTO t "
				+ " where t.Id=:Id "),

		@NamedQuery(name = "CustomerDeviceDetails.getByCustomerId", query = "SELECT t from CustomerDeviceDetailsDTO t "
				+ " where t.customerId=:customerId "),

		@NamedQuery(name = "CustomerDeviceDetails.listAllDevicesById", query = "SELECT t from CustomerDeviceDetailsDTO t "),

		@NamedQuery(name = "CustomerDeviceDetails.getPlatformUsersCount", query = "SELECT COUNT(t.devicePlatform) from CustomerDeviceDetailsDTO t "
				+ " where t.devicePlatform=:devicePlatform"),

		@NamedQuery(name = "CustomerDeviceDetails.getAllNotificationTokens", query = "SELECT t.deviceToken from CustomerDeviceDetailsDTO t "
				+ "where t.deviceToken is not null and t.customerId !=:customerId") })

@Entity
@Table(name = "CustomerDeviceDetails")
public class CustomerDeviceDetailsDTO {

	@Id
	@SequenceGenerator(name = "CUSTOMERDEVICEDETAILS_ID_GENERATOR", sequenceName = "CUSTOMERDEVICEDETAILS_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUSTOMERDEVICEDETAILS_ID_GENERATOR")
	private Long Id;
	private Long customerId;
	private String deviceModel;
	private String appToken;
	private String deviceToken;
	private String devicePlatform;
	private String devicePlatformVersion;
	private String appVersion;
	private Timestamp updatedOn;
	private String networkCarrier;
	private String networkType;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getAppToken() {
		return appToken;
	}

	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getDevicePlatform() {
		return devicePlatform;
	}

	public void setDevicePlatform(String devicePlatform) {
		this.devicePlatform = devicePlatform;
	}

	public String getDevicePlatformVersion() {
		return devicePlatformVersion;
	}

	public void setDevicePlatformVersion(String devicePlatformVersion) {
		this.devicePlatformVersion = devicePlatformVersion;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getNetworkCarrier() {
		return networkCarrier;
	}

	public void setNetworkCarrier(String networkCarrier) {
		this.networkCarrier = networkCarrier;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

}
