package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
@NamedQuery(name = "TimeSlot.getById", 
			query = "SELECT t from TimeSlotDTO t "
			        + "where t.Id=:Id "),
@NamedQuery(name = "TimeSlot.getByTimeSlot", 
query = "SELECT t from TimeSlotDTO t "
        + "where t.timeSlot=:timeSlot "),
@NamedQuery(name = "TimeSlot.getByIds", 
query = "SELECT distinct t.timeSlot from TimeSlotDTO t "
        + "where t.Id in (:Ids) "),
@NamedQuery(name = "TimeSlot.getAllAppTimeSlots", 
query = "SELECT t from TimeSlotDTO t "
        + "where t.displayedInApp=:displayedInApp order by t.priority"),
@NamedQuery(name = "TimeSlot.getAll", 
query = "SELECT t from TimeSlotDTO t "
        + " order by t.priority"),
@NamedQuery(name = "TimeSlot.getByStartTime", 
query = "SELECT t.Id from TimeSlotDTO t "
        + "where t.StartTime=:StartTime"),
@NamedQuery(name = "TimeSlot.getTimeSlotListByIds", 
query = "SELECT t from TimeSlotDTO t "
        + "where t.Id in (:timeSlotIdList)")

})

@Entity
@Table(name = "TimeSlot")	
public class TimeSlotDTO {
	@Id
    @SequenceGenerator(name="TIMESLOT_ID_GENERATOR", sequenceName="TIMESLOT_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="TIMESLOT_ID_GENERATOR") 

	private Integer Id;
	private String timeSlot;
	private Integer StartTime;
    private Integer EndTime;
    private Integer LockingHours;
    private Timestamp batchupdateTime;
    private boolean displayedInApp;
    private int priority;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getTimeSlot() {
		return timeSlot;
	}
	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}
	public Integer getStartTime() {
		return StartTime;
	}
	public void setStartTime(Integer startTime) {
		StartTime = startTime;
	}
	public Integer getEndTime() {
		return EndTime;
	}
	public void setEndTime(Integer endTime) {
		EndTime = endTime;
	}
	public Integer getLockingHours() {
		return LockingHours;
	}
	public void setLockingHours(Integer lockingHours) {
		LockingHours = lockingHours;
	}
	public Timestamp getBatchupdateTime() {
		return batchupdateTime;
	}
	public void setBatchupdateTime(Timestamp batchupdateTime) {
		this.batchupdateTime = batchupdateTime;
	}
	public boolean isDisplayedInApp() {
		return displayedInApp;
	}
	public void setDisplayedInApp(boolean displayedInApp) {
		this.displayedInApp = displayedInApp;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
	
}
