package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "CustomerTransaction.getById", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.customerId=:customerId "),
		@NamedQuery(name = "CustomerTransaction.getByOrderDetailsId", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.orderDetailsId=:orderDetailsId and t.transactionDescription != 3 "),

		@NamedQuery(name = "CustomerTransaction.getByOrderDetailsIdAndPaymentMode", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.orderDetailsId=:orderDetailsId and t.paymentModeId=:paymentModeId"),

		@NamedQuery(name = "CustomerTransaction.getPaymentModeByOrderDetailsId", query = "SELECT t.paymentModeId from CustomerTransactionDTO t "
				+ "where t.orderDetailsId=:orderDetailsId  and t.transactionDescription != 3 and t.moneyOut > 0 and remarks != 1"),

		@NamedQuery(name = "CustomerTransaction.getCustomerTransactionRecordsCount", query = "SELECT count(ct) FROM CustomerTransactionDTO ct WHERE ct.customerId=:customerId"),
		@NamedQuery(name = "CustomerTransaction.getAllOrderTransaction", query = "SELECT ct FROM CustomerTransactionDTO ct WHERE ct.orderDetailsId > 0 and ct.remarks != -1 and ct.moneyOut > 0"), 
		
		@NamedQuery(name = "CustomerTransaction.getAllTransactionsByOrderDetailsId", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.orderDetailsId=:orderDetailsId and (remarks is null or remarks = 0 or remarks = 2)"),

		@NamedQuery(name = "CustomerTransaction.getAllTransactionsByOrderDetailsIdWithNoBebt", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.orderDetailsId=:orderDetailsId and (remarks is null or remarks = 0 or remarks = 2) and t.transactionDescription != 3"),
		@NamedQuery(name = "CustomerTransaction.getByOrderId", query = "SELECT t from CustomerTransactionDTO t "
				+ "where t.orderId=:orderId and t.moneyOut > 0 and t.remarks != 1 "),

})

@Entity
@Table(name = "CustomerTransaction")
public class CustomerTransactionDTO {
	@Id
	@SequenceGenerator(name = "CUSTOMERTRANSACTION_ID_GENERATOR", sequenceName = "CUSTOMERTRANSACTION_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUSTOMERTRANSACTION_ID_GENERATOR")

	private Long Id;
	private Long customerId;
	private int paymentModeId;
	private float moneyIn;
	private float moneyOut;
	private float dueAmount;
	private float preBalance;
	private int transactionDescription;
	//private int descriptionDetail;
	private Timestamp TransactionDate;
	private int remarks;
	private Long orderDetailsId;
	private Long orderId;
	private Long updatedBy;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public int getPaymentModeId() {
		return paymentModeId;
	}

	public void setPaymentModeId(int paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public float getMoneyIn() {
		return moneyIn;
	}

	public void setMoneyIn(float moneyIn) {
		this.moneyIn = moneyIn;
	}

	public float getMoneyOut() {
		return moneyOut;
	}

	public void setMoneyOut(float moneyOut) {
		this.moneyOut = moneyOut;
	}

	public float getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(float dueAmount) {
		this.dueAmount = dueAmount;
	}

	public float getPreBalance() {
		return preBalance;
	}

	public void setPreBalance(float preBalance) {
		this.preBalance = preBalance;
	}

	/*public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}
*/
	public Timestamp getTransactionDate() {
		return TransactionDate;
	}

	public int getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(int transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		TransactionDate = transactionDate;
	}

	public Long getOrderDetailsId() {
		return orderDetailsId;
	}

	public void setOrderDetailsId(Long orderDetailsId) {
		this.orderDetailsId = orderDetailsId;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public int getRemarks() {
		return remarks;
	}

	public void setRemarks(int remarks) {
		this.remarks = remarks;
	}

	
	/*public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
*/
}
