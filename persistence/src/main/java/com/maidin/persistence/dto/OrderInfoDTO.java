package com.maidin.persistence.dto;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({ @NamedQuery(name = "OrderInfo.getById", query = "SELECT t from OrderInfoDTO t where t.Id=:Id "),
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateAndTimeSlot", query = "SELECT t from OrderInfoDTO t  "
				+ "where  t.customerId=:customerId and " + "t.deliveryDate=:deliveryDate and t.timeSlot=:timeSlot "),
		@NamedQuery(name = "OrderInfo.getDifferentDeliveryDates", query = "SELECT t.deliveryDate from OrderInfoDTO t,OrderDetailsInfoDTO od "
				+ "where t.customerId=:customerId and t.deliveryDate >= CURDATE() and od.orderStatus='CONFIRMED' and "
				+ "t.Id = od.orderId  group By t.deliveryDate order by  t.deliveryDate Desc"),
		@NamedQuery(name = "OrderInfo.getByCustomerIdAndDeliveryDate", query = "SELECT distinct t from OrderInfoDTO t,OrderDetailsInfoDTO od "
				+ "where t.Id=od.orderId and t.customerId=:customerId and " + "t.deliveryDate=:deliveryDate"),
		@NamedQuery(name = "OrderInfo.getDifferentPastDeliveryDates", query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi, OrderDetailsInfoDTO odi where "
				+ " oi.customerId=:customerId and oi.Id=odi.orderId  and odi.orderStatus='DELIVERED'"
				+ " and odi.cancelled = false group by oi.deliveryDate order by oi.deliveryDate desc"),
		
		@NamedQuery(name = "OrderInfo.getDifferentDeliveryDatesByTimestamp", query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi ,OrderDetailsInfoDTO od where oi.Id=od.orderId and "
				+ "  oi.deliveryDate BETWEEN :fromTimestamp AND :toTimestamp group by oi.deliveryDate order by oi.deliveryDate desc"),
		
		@NamedQuery(name = "OrderInfo.getByDeliveryDates", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t  where oi.Id =od.orderId "
				+ "and oi.deliveryDate=:deliveryDate and oi.timeSlotId = t.Id order by t.Id, oi.customerId"),
		
		@NamedQuery(name = "OrderInfo.getAllScheduledOrders", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od where oi.Id=od.orderId and oi.customerId=:customerId and deliveryDate>=CURDATE()"),

		@NamedQuery(name = "OrderInfo.getDifferentCustomersByTimestamp", query = "SELECT distinct oi.customerId from OrderInfoDTO oi ,OrderDetailsInfoDTO od,CustomerDTO c, AddressDTO a  where oi.Id=od.orderId and c.addressId = a.Id and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and c.Id=oi.customerId and c.active=true group by oi.customerId order By a.city,a.project,a.flat"),

		@NamedQuery(name = "OrderInfo.getByCustomerId", query = "SELECT oi from OrderInfoDTO oi where oi.customerId=:customerId"),

		@NamedQuery(name = "OrderInfo.getDifferentCustomersByTimestampAndTimeSlotId", query = "SELECT distinct oi.customerId from OrderInfoDTO oi ,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a where oi.Id = od.orderId and c.addressId = a.Id and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and oi.timeSlotId=:timeSlotId and c.Id=oi.customerId and c.active=true group by oi.customerId order By a.city,a.project,a.flat"),

		// for multiple day reports
		@NamedQuery(name = "OrderInfo.getCustomersSortedByDeliveryDate", query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi ,OrderDetailsInfoDTO od,CustomerDTO c where oi.Id=od.orderId and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and c.Id=oi.customerId and c.active=true group by oi.deliveryDate order By oi.deliveryDate"),

		@NamedQuery(name = "OrderInfo.getCustomersInTimeSlotSortedByDeliveryDate", query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi ,OrderDetailsInfoDTO od, CustomerDTO c where oi.Id = od.orderId and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and oi.timeSlotId=:timeSlotId and c.Id=oi.customerId and c.active=true group by oi.customerId order By oi.deliveryDate"),

		
		@NamedQuery(name = "OrderInfo.getByCustomerIdAndTimeSlot", query = "SELECT oi from OrderInfoDTO oi where oi.customerId=:customerId and oi.timeSlotId=:timeSlotId"),
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDates", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t  where oi.Id = od.orderId and oi.customerId=:customerId and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and oi.cancelled=false and oi.timeSlotId=t.Id order by t.Id "),
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDatesAndTimeSlot", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od,TimeSlotDTO t  where oi.Id=od.orderId and oi.customerId=:customerId and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and oi.timeSlotId=:timeSlotId and oi.cancelled=false and oi.timeSlotId=t.Id order by t.Id"),
		@NamedQuery(name = "OrderInfo.getCustomersByCPTDeliveryDateAndTimeSlot", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a  where "
				+ " oi.Id =od.orderId and oi.deliveryDate=:deliveryDate and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.addressId=a.Id"
				+ " and a.city=:city and a.project=:project and a.tower=:tower and c.active=true and oi.cancelled=false  order by c.fullName, oi.Id, od.Id "),
		@NamedQuery(name = "OrderInfo.getCustomersByCPTDeliveryDate", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a ,TimeSlotDTO t where "
				+ " oi.Id= od.orderId and oi.deliveryDate=:deliveryDate and oi.cancelled=false and c.Id=oi.customerId and oi.addressId=a.Id and t.Id = oi.timeSlotId"
				+ " and a.city=:city and a.project=:project and a.tower=:tower and c.active=true  order by c.fullName,t.StartTime,oi.Id,od.Id  "),
		@NamedQuery(name = "OrderInfo.getCustomersByCPDeliveryDateAndTimeSlotId", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a  where "
				+ " oi.Id= od.orderId and oi.deliveryDate=:deliveryDate and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.addressId=a.Id"
				+ " and a.city=:city and a.project=:project and oi.cancelled=false and c.active=true order by c.fullName "),
		@NamedQuery(name = "OrderInfo.getCustomersByCPDeliveryDate", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a,TimeSlotDTO t  where "
				+ " oi.Id=od.orderId and oi.deliveryDate=:deliveryDate and c.Id=oi.customerId and oi.addressId=a.Id and t.Id = oi.timeSlotId"
				+ " and a.city=:city and a.project=:project and c.active=true and  oi.cancelled=false order by c.fullName,t.StartTime,oi.Id,od.Id "),

		@NamedQuery(name = "OrderInfo.getTimeSlotByCustomerDeliveryDateAndTimeSlotId", query = "SELECT distinct t.Id from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id = od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.cancelled=false "
				+ " and t.Id=oi.timeSlotId "),
		//+ " and t.timeSlot=oi.timeSlot "),
		@NamedQuery(name = "OrderInfo.getTimeSlotsByCustomerDeliveryDate", query = "SELECT distinct t.Id from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id =od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and c.Id=oi.customerId"
				//+ "  and t.timeSlot=oi.timeSlot "
				+ "  and t.Id=oi.timeSlotId "
				+ "and oi.cancelled=false  order by  t.Id "),
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateAndTimeSlotId", query = "SELECT distinct oi from OrderInfoDTO oi, OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id=od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id and t.Id=:Id and "
				+ "c.Id=oi.customerId and oi.cancelled=false order by  t.Id "),
		/*@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateWithVegetableCategory", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t , ProductDTO p, ProductBrandsDTO b,"
				+ " ProductCategoryDTO c where oi.Id = od.orderId and od.productId = p.Id and od.orderStatus = 'CONFIRMED' and "
				+ " p.brandId= b.Id and b.categoryId = c.Id and c.Id=:categoryId and "
				+ " oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id  "
				+ " order by  t.Id "),*/
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateWithVegetableCategory", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t , ProductCityInfoV2DTO pc "
				+ "  where oi.Id = od.orderId and od.productId = pc.Id and od.orderStatus = 'CONFIRMED'  "
				+ " and pc.categoryId =:categoryId and "
				+ " oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id  "
				+ " order by  t.Id "),
		/*@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateWithOtherCategory", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t , ProductDTO p, ProductBrandsDTO b,"
				+ " ProductCategoryDTO c where oi.Id = od.orderId and od.productId = p.Id and od.orderStatus = 'CONFIRMED' and "
				+ " p.brandId= b.Id and b.categoryId = c.Id and c.Id!=:categoryId and "
				+ " oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id  "
				+ " order by  t.Id "),*/
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateWithOtherCategory", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t , ProductCityInfoV2DTO pc "
				+ "  where oi.Id = od.orderId and od.productId = pc.Id and od.orderStatus = 'CONFIRMED' and "
				+ " pc.categoryId!=:categoryId and "
				+ " oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id  "
				+ " order by  t.Id "),
		
		@NamedQuery(name = "OrderInfo.getCheckoutIdsOfUpcomingCurrentDayTimeSlots", 
		query = "SELECT distinct oi.Id from OrderInfoDTO oi,OrderDetailsInfoDTO od, TimeSlotDTO t where oi.Id = od.orderId and"
				+ " customerId=:customerId and oi.deliveryDate = curDate() and oi.timeSlotId = t.Id  "

				+ " and od.orderStatus='CONFIRMED' and oi.scheduleId is null "),
				//+ " and hour(current_time()) <:lastLockingTime and od.orderStatus='CONFIRMED' and oi.scheduleId is null "),

		//and hour(current_time()) <  t.EndTime
		@NamedQuery(name = "OrderInfo.getTimeSlotsByCustomerIdAndDeliveryDate", query = "SELECT distinct oi.timeSlot from OrderInfoDTO oi, TimeSlotDTO t where "
				+ " customerId=:customerId and oi.deliveryDate=:deliveryDate and oi.timeSlotId = t.Id  order by t.Id"),
		
		@NamedQuery(name = "OrderInfo.getNextOrderDate", 
		query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi,OrderDetailsInfoDTO od where oi.Id = od.orderId and"
				+ " customerId=:customerId and oi.deliveryDate > curDate() and od.orderStatus='CONFIRMED' and oi.scheduleId is null order by oi.deliveryDate"),
		
		@NamedQuery(name = "OrderInfo.getPastOrderDates", 
		query = "SELECT distinct oi.deliveryDate from OrderInfoDTO oi,OrderDetailsInfoDTO od where oi.Id = od.orderId and"
				+ " customerId=:customerId and oi.deliveryDate < curDate() and " 
				+ " od.orderStatus='CONFIRMED' and oi.scheduleId is null order by oi.deliveryDate "),
		
		@NamedQuery(name = "OrderInfo.getCheckoutTimeSlotsByIds", 
		query = "SELECT distinct t.Id from  OrderInfoDTO oi, OrderDetailsInfoDTO od, TimeSlotDTO t where oi.Id = od.orderId and "
				+ " oi.timeSlotId=t.Id and oi.Id IN (:Ids) and oi.scheduleId is null order by t.Id "),
		
		@NamedQuery(name = "OrderInfo.getCheckoutsByCustomerIdDeliveryDateAndTimeSlotId", query = "SELECT distinct oi from OrderInfoDTO oi, OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id=od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id and t.Id=:Id and "
				+ "c.Id=oi.customerId and od.orderStatus='CONFIRMED' and oi.scheduleId is null order by  t.Id "),
		@NamedQuery(name = "OrderInfo.getCheckoutTimeSlotByCustomerDeliveryDate", query = "SELECT distinct t.Id from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id = od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlotId=t.Id  and "
				+ "c.Id=oi.customerId and od.orderStatus='CONFIRMED' and oi.scheduleId is null order by t.Id "),
		@NamedQuery(name = "OrderInfo.getByScheduleIdDeliveryDate", query = "SELECT oi from OrderInfoDTO oi where "
				+ "oi.scheduleId=:scheduleId and oi.deliveryDate=:deliveryDate "),
		@NamedQuery(name = "OrderInfo.getOrdersByScheduleId", query = "SELECT oi from OrderInfoDTO oi, OrderDetailsInfoDTO od where "
				+ "oi.Id=od.orderId and oi.scheduleId=:scheduleId and oi.deliveryDate >= curDate() "),
		@NamedQuery(name = "OrderInfo.getByCustomerDifferentDeliveryDates", query = "SELECT oi.deliveryDate from OrderInfoDTO oi, OrderDetailsInfoDTO od where "
				+ "oi.Id = od.orderId and oi.customerId=:customerId and oi.deliveryDate BETWEEN :fromTimestamp AND :toTimestamp group by oi.deliveryDate order by oi.deliveryDate desc "),
		@NamedQuery(name = "OrderInfo.getScheduleOrdersByTimeSlot", query = "SELECT oi from OrderInfoDTO oi, OrderDetailsInfoDTO od , TimeSlotDTO t where "
				+ "oi.Id = od.orderId  and oi.timeSlotId=t.Id and oi.deliveryDate= curDate() and t.Id in (:timeSlotIdList) and od.orderStatus = 'CONFIRMED' and od.paymentStatus != 'PAYMENTSETTLED' "
				+ "and oi.cancelled = false and od.cancelled = false "),
		@NamedQuery(name = "OrderInfo.getTotalOrderCount", query = "SELECT count(oi) from OrderInfoDTO oi where oi.customerId=:customerId and oi.overallOrderStatus=:overallOrderStatus and oi.cancelled=:cancelled "),
		@NamedQuery(name = "OrderInfo.getOrdersByDateAndCityCode", query = "SELECT oi from OrderInfoDTO oi,OrderDetailsInfoDTO od ,"
				+ " AddressDTO a, TimeSlotDTO t,CustomerDTO c  where oi.Id = od.orderId and oi.addressId = a.Id and "
				+ " oi.timeSlotId=t.Id and oi.customerId = c.Id "
				+ " and oi.overallOrderStatus !=4 and od.orderStatus != 'CANCELLED' and oi.deliveryDate between :fromDate "
				+ " and :toDate and a.cityCode=:cityCode order by a.city,a.project,a.tower, oi.deliveryDate,t.StartTime  "),
		@NamedQuery(name = "OrderInfo.getOrdersByDate", query = "SELECT oi from OrderInfoDTO oi, OrderDetailsInfoDTO od, "
				+ "AddressDTO a, TimeSlotDTO t, CustomerDTO c where oi.Id=od.orderId and oi.addressId = a.Id "
				+ " and oi.timeSlotId=t.Id and oi.customerId = c.Id "
				+ "and oi.deliveryDate between :fromDate and :toDate and oi.overallOrderStatus != 4 and "
				+ "od.orderStatus != 'CANCELLED' order by a.city,a.project,a.tower, oi.deliveryDate,t.StartTime "),
		@NamedQuery(name = "OrderInfo.getCustomersByCPDeliveryDateAndTimeSlot", query = "SELECT distinct oi from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a  where "
				+ " oi.Id= od.orderId and oi.deliveryDate=:deliveryDate and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.addressId=a.Id  "
				+ " and a.city=:city and a.project=:project and oi.cancelled=false and c.active=true  order by c.fullName,oi.Id,od.Id "),
		@NamedQuery(name = "OrderInfo.getTimeSlotByCustomerDeliveryDateAndTimeSlot", query = "SELECT distinct t.Id from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id = od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.cancelled=false "
				+ " and t.timeSlot=oi.timeSlotId "),
		@NamedQuery(name = "OrderInfo.getDifferentCustomersByTimestampAndTimeSlot", query = "SELECT distinct oi.customerId from OrderInfoDTO oi ,OrderDetailsInfoDTO od, CustomerDTO c, AddressDTO a where oi.Id = od.orderId and c.addressId = a.Id and oi.deliveryDate BETWEEN :fromDate AND"
				+ " :toDate and oi.timeSlot=:timeSlot and c.Id=oi.customerId and c.active=true group by oi.customerId order By a.city,a.project,a.flat"),
		@NamedQuery(name = "OrderInfo.getTimeSlotStringByCustomerDeliveryDateAndTimeSlot", query = "SELECT distinct t.timeSlot from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id = od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and oi.timeSlot=:timeSlot and c.Id=oi.customerId and oi.cancelled=false "
				+ " and t.Id=oi.timeSlotId  "),
		@NamedQuery(name = "OrderInfo.getTimeSlotStringByCustomerDeliveryDate", query = "SELECT distinct t.timeSlot from OrderInfoDTO oi,OrderDetailsInfoDTO od, CustomerDTO c, TimeSlotDTO t  where "
				+ " oi.Id =od.orderId and oi.deliveryDate=:deliveryDate and oi.customerId=:customerId and c.Id=oi.customerId"
				+ "  and t.Id=oi.timeSlotId and oi.cancelled=false  order by  t.Id "),
		@NamedQuery(name = "OrderInfo.getByCustomerIdDeliveryDateTimeSlotAndCityCode", query = "SELECT distinct t from OrderInfoDTO t, AddressDTO a  "
				+ "where t.addressId=a.Id and t.customerId=:customerId and t.deliveryDate=:deliveryDate and t.timeSlot=:timeSlot and a.cityCode=:cityCode"),
		@NamedQuery(name = "OrderInfo.getCheckoutOrders", query = "SELECT distinct o from OrderInfoDTO o, OrderDetailsInfoDTO od  "
				+ " where o.Id = od.orderId and scheduleId is null and customerId=:customerId and od.orderStatus='CONFIRMED' order by deliveryDate "),
		@NamedQuery(name = "OrderInfo.getByIds", query = "SELECT distinct o from OrderInfoDTO o  "
				+ " where o.Id in :Ids")
})

@Entity
@Table(name = "OrderInfo")
public class OrderInfoDTO {
	@Id
	@SequenceGenerator(name = "ORDERINFO_ID_GENERATOR", sequenceName = "ORDERINFO_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "ORDERINFO_ID_GENERATOR")

	private Long Id;
	private Long customerId;
	private String timeSlot;
	private Integer timeSlotId;
	//private Long flatId;
	private Long addressId;
	private Integer paymentModeId;
	private Long invoiceNo;
	private float orderSubTotal;
	private Timestamp orderedTime;
	private Date deliveryDate;
	private String specialInstructions;
	private Long shippingId;
	private boolean cancelled;
	private Timestamp cancellationTime;
	private Integer overallOrderStatus;
	private Integer overallPaymentStatus;
	private Long scheduleId;
	//private String deliveryLocation;
	private String remarks;
	private String version;
	private Timestamp updatedOn;
	private float amountReceivedAtDelivery;
	private Integer requestedPaymentModeId;
	private String payBy;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public Integer getTimeSlotId() {
		return timeSlotId;
	}

	public void setTimeSlotId(Integer timeSlotId) {
		this.timeSlotId = timeSlotId;
	}

	/*public Long getFlatId() {
		return flatId;
	}

	public void setFlatId(Long flatId) {
		this.flatId = flatId;
	}
*/
	
	public Integer getPaymentModeId() {
		return paymentModeId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public void setPaymentModeId(Integer paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public Long getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(Long invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public float getOrderSubTotal() {
		return orderSubTotal;
	}

	public void setOrderSubTotal(float orderSubTotal) {
		this.orderSubTotal = orderSubTotal;
	}

	public Timestamp getOrderedTime() {
		return orderedTime;
	}

	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public Long getShippingId() {
		return shippingId;
	}

	public void setShippingId(Long shippingId) {
		this.shippingId = shippingId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public Timestamp getCancellationTime() {
		return cancellationTime;
	}

	/*public float getCashCollected() {
		return cashCollected;
	}

	public void setCashCollected(float cashCollected) {
		this.cashCollected = cashCollected;
	}*/
	

	public void setCancellationTime(Timestamp cancellationTime) {
		this.cancellationTime = cancellationTime;
	}

	public Integer getOverallOrderStatus() {
		return overallOrderStatus;
	}

	public void setOverallOrderStatus(Integer overallOrderStatus) {
		this.overallOrderStatus = overallOrderStatus;
	}

//	public String getOverallPaymentStatus() {
//		return overallPaymentStatus;
//	}
//
//	public void setOverallPaymentStatus(String overallPaymentStatus) {
//		this.overallPaymentStatus = overallPaymentStatus;
//	}

	
	/*
	 * public Long getOrderPaymentId() { return orderPaymentId; }
	 * 
	 * public void setOrderPaymentId(Long orderPaymentId) { this.orderPaymentId
	 * = orderPaymentId; }
	 */

	public String getVersion() {
		return version;
	}

	public Integer getOverallPaymentStatus() {
		return overallPaymentStatus;
	}

	public void setOverallPaymentStatus(Integer overallPaymentStatus) {
		this.overallPaymentStatus = overallPaymentStatus;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public Long getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Long scheduleId) {
		this.scheduleId = scheduleId;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public float getAmountReceivedAtDelivery() {
		return amountReceivedAtDelivery;
	}

	public void setAmountReceivedAtDelivery(float amountReceivedAtDelivery) {
		this.amountReceivedAtDelivery = amountReceivedAtDelivery;
	}

	
	public Integer getRequestedPaymentModeId() {
		return requestedPaymentModeId;
	}

	public void setRequestedPaymentModeId(Integer requestedPaymentModeId) {
		this.requestedPaymentModeId = requestedPaymentModeId;
	}

	public String getPayBy() {
		return payBy;
	}

	public void setPayBy(String payBy) {
		this.payBy = payBy;
	}

	/*public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}*/

}
