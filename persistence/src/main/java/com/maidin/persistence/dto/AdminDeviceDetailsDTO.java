package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({  

@NamedQuery(name = "AdminDeviceDetails.getById", 
query = "SELECT t from AdminDeviceDetailsDTO t "
		+ " where t.Id=:Id "),

@NamedQuery(name = "AdminDeviceDetails.getByAdminId", 
query = "SELECT t from AdminDeviceDetailsDTO t "
        + " where t.adminId=:adminId ")
})

@Entity
@Table(name = "AdminDeviceDetails")
public class AdminDeviceDetailsDTO {
	
	@Id
    @SequenceGenerator(name="ADMINDEVICEDETAILS_ID_GENERATOR", sequenceName="ADMINDEVICEDETAILS_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ADMINDEVICEDETAILS_ID_GENERATOR")
	private Long Id;
	private Long adminId;
	private String deviceModel;
	private String appToken;
	private String deviceToken;
	private String devicePlatform;
	private String devicePlatformVersion;
	private String appVersion;
	private Timestamp updatedOn;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	
	public Long getAdminId() {
		return adminId;
	}
	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}
	public String getDeviceModel() {
		return deviceModel;
	}
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	public String getAppToken() {
		return appToken;
	}
	public void setAppToken(String appToken) {
		this.appToken = appToken;
	}
	public String getDeviceToken() {
		return deviceToken;
	}
	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
	public String getDevicePlatform() {
		return devicePlatform;
	}
	public void setDevicePlatform(String devicePlatform) {
		this.devicePlatform = devicePlatform;
	}
	public String getDevicePlatformVersion() {
		return devicePlatformVersion;
	}
	public void setDevicePlatformVersion(String devicePlatformVersion) {
		this.devicePlatformVersion = devicePlatformVersion;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	
	
}
