package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "Images")
@NamedQueries({  
@NamedQuery(name = "Images.getByProductId", 
query = "SELECT o from ImageDTO o "
        + " where o.imageCategory=:imageCategory "
        + " and o.imageCategoryId=:imageCategoryId")})

public class ImageDTO {
	@Id
    @SequenceGenerator(name="IMAGES_ID_GENERATOR", sequenceName="IMAGES_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="IMAGES_ID_GENERATOR") 
	    private Long Id;
		private String imageCategory;
	    private Long imageCategoryId;
	    private String fileName;
	    private String contentType;
	    private String fileSize;
	    private byte[] data;
	    private Timestamp updatedOn;
	    private Long updatedBy;
	  
		public Long getId() {
			return Id;
		}
		public void setId(Long id) {
			Id = id;
		}
		
		
		public String getImageCategory() {
			return imageCategory;
		}
		public void setImageCategory(String imageCategory) {
			this.imageCategory = imageCategory;
		}
		public Long getImageCategoryId() {
			return imageCategoryId;
		}
		public void setImageCategoryId(Long imageCategoryId) {
			this.imageCategoryId = imageCategoryId;
		}
		
		public String getFileName() {
			return fileName;
		}
		public byte[] getData() {
			return data;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public String getContentType() {
			return contentType;
		}
		public String getFileSize() {
			return fileSize;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public void setFileSize(String fileSize) {
			this.fileSize = fileSize;
		}
		public void setData(byte[] data) {
			this.data = data;
		}
		public Timestamp getUpdatedOn() {
			return updatedOn;
		}
		public void setUpdatedOn(Timestamp updatedOn) {
			this.updatedOn = updatedOn;
		}
		public Long getUpdatedBy() {
			return updatedBy;
		}
		public void setUpdatedBy(Long updatedBy) {
			this.updatedBy = updatedBy;
		}
}
