package com.maidin.persistence.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@NamedQueries({  

@NamedQuery(name = "AdminAuthentication.getByMobileNumber", 
query = "SELECT da from AdminAuthenticationDTO da "
        + "where da.mobileNumber=:mobileNumber ")
})

@Entity
@Table(name = "AdminAuthentication")
public class AdminAuthenticationDTO {
	@Id
    @SequenceGenerator(name="ADMINAUTHENTICATION_ID_GENERATOR", sequenceName="ADMINAUTHENTICATION_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ADMINAUTHENTICATION_ID_GENERATOR") 	
	
	private Long Id;
	private Long adminId;
	private String mobileNumber;
	private String authToken;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	public Long getAdminId() {
		return adminId;
	}
	public void setAdminId(Long adminId) {
		this.adminId = adminId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getAuthToken() {
		return authToken;
	}
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	
}
