package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "ProductCityInfoV2.getByCityCodeAndSubCategoryId", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.cityCode=:cityCode and b.subCategoryId=:subCategoryId and b.active=:active order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getByCityCode", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.cityCode=:cityCode and b.active=:active order by b.productId, b.priority, b.quantityUnit "),
		@NamedQuery(name = "ProductCityInfoV2.getBySubCategoryIdsAndCityCode", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.cityCode=:cityCode and b.subCategoryId in (:Ids) and b.active=:active order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getById", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.Id=:Id and b.active=:active and b.stockStatus=true"),
		@NamedQuery(name = "ProductCityInfoV2.getByIdWithoutActiveStatus", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.Id=:Id "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIds", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productId in (:productIds) order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getByproductIdsAndCityCode", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productId in (:productIds) and b.cityCode=:cityCode order by b.salePrice"),
		@NamedQuery(name = "ProductCityInfoV2.getByProductDetailGroupId", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productDetailGroupId=:productDetailGroupId"),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdQuantityUnitCategoryIdSubCategoryIdCityCode", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productId=:productId and  b.quantityUnit=:quantityUnit and "
				+ " b.categoryId=:categoryId and  b.subCategoryId=:subCategoryId and b.cityCode=:cityCode ") ,
		@NamedQuery(name = "ProductCityInfoV2.updateProductGroupId", 
		query = "UPDATE ProductCityInfoV2DTO pc SET pc.productDetailGroupId = :productDetailGroupId where "
		        + " pc.Id IN (:productDetailIds) "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdAndCities", 
		query = "Select pc.Id from ProductCityInfoV2DTO pc where pc.productId = :productId and "
		        + " pc.cityCode IN (:cities) "),
		@NamedQuery(name = "ProductCityInfoV2.updateStatus", 
		query = "UPDATE ProductCityInfoV2DTO pc SET pc.active = :active, pc.stockStatus=:stockStatus where "
		        + " pc.Id IN (:productDetailIds) "),
		@NamedQuery(name = "ProductCityInfoV2.getByproductIdsAndCityCodeWithActivationStatus", query = "Select b from ProductCityInfoV2DTO b,"
				+ " CategoryInfoV2DTO c, SubCategoryInfoV2DTO s where b.subCategoryId = s.Id and b.categoryId = c.Id and s.active = true and  "
				+ "c.active = true and "
				+ " b.productId in (:productIds) and b.cityCode=:cityCode and b.active=:active order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductDetailIds", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.Id in (:productDetailIds) "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdAndCityCode", query = "Select b.Id from ProductCityInfoV2DTO b"
				+ " where productId=:productId and cityCode=:cityCode"),
		@NamedQuery(name = "ProductCityInfoV2.updateSetByDefault", 
		query = "UPDATE ProductCityInfoV2DTO pc SET pc.setByDefault=:setByDefault where "
		        + " pc.Id IN (:productDetailIds) "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdGroupByCity", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productId=:productId group by b.cityCode order by b.salePrice"),
		@NamedQuery(name = "ProductCityInfoV2.getByCategoryIdAndSubCategoryId", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.categoryId=:categoryId and b.subCategoryId=:subCategoryId order by b.salePrice"),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdQuantityUnitAndCities", 
		query = "Select pc.Id from ProductCityInfoV2DTO pc where pc.productId=:productId and "
				+ "quantityUnit=:quantityUnit and "
		        + " pc.cityCode IN (:cities) "),
		@NamedQuery(name = "ProductCityInfoV2.getIdsBySubCategoryIdAndCities", query = "Select b.Id from ProductCityInfoV2DTO b"
				+ " where b.cityCode in :cities and b.subCategoryId=:subCategoryId and b.active=:active"),
		@NamedQuery(name = "ProductCityInfoV2.getIdsByCategoryIdAndCities", query = "Select b.Id from ProductCityInfoV2DTO b"
				+ " where b.cityCode in :cities and b.categoryId=:categoryId and b.active=:active"),
		@NamedQuery(name = "ProductCityInfoV2.getByCityCodeAndSubCategoryIdWithoutActiveStatus", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.cityCode=:cityCode and b.subCategoryId=:subCategoryId order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getBySubCategoryIdsAndCityCodeWithoutActiveState", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.cityCode=:cityCode and b.subCategoryId in (:subCategoryIds) order by b.salePrice "),
		@NamedQuery(name = "ProductCityInfoV2.getByProductIdQuantityUnit", query = "Select b from ProductCityInfoV2DTO b"
				+ " where b.productId=:productId and b.quantityUnit=:quantityUnit"),
	    @NamedQuery(name = "ProductCityInfoV2.getByMatchingIdsAndCityCode", query = "Select distinct pc from ProductCityInfoV2DTO pc, ProductInfoV2DTO p "
				+ " where pc.productId = p.Id and pc.categoryId in (:categoryIdList) or pc.subCategoryId in (:subCategoryIdList) "
				+ " or pc.productId in (:productIdList)  or p.brandId in (:brandIdList)  and pc.cityCode=:cityCode and"
				+ " pc.active=true and p.active=true"),
	    @NamedQuery(name = "ProductCityInfoV2.getByPhotoId", query = "Select distinct pc from ProductCityInfoV2DTO pc where"
	    		+ " pc.photoId=:photoId"),
	    @NamedQuery(name = "ProductCityInfoV2.getByCityCodeWithoutActiveState", query = "Select distinct pc from ProductCityInfoV2DTO pc where"
	    		+ " pc.cityCode=:cityCode"),
	    @NamedQuery(name = "ProductCityInfoV2.getByProductIdQuantityUnitAndCityCode", query = "Select pc from ProductCityInfoV2DTO pc where"
	    		+ " pc.productId=:productId and pc.quantityUnit=:quantityUnit and pc.cityCode=:cityCode"),
	    @NamedQuery(name = "ProductCityInfoV2.getByProductIdQuantityUnitCategoryIdSubCategoryIdAndCities", query = "Select pc.Id from ProductCityInfoV2DTO pc where"
	    		+ " pc.productId=:productId and pc.quantityUnit=:quantityUnit and pc.categoryId =:categoryId and pc.subCategoryId =:subCategoryId and pc.cityCode IN (:cities)"),
		
		
})

@Entity
@Table(name = "ProductCityInfoV2")
public class ProductCityInfoV2DTO {
	@Id
	@SequenceGenerator(name = "PRODUCTCITYINFOV2_ID_GENERATOR", sequenceName = "PRODUCTCITYINFOV2_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "PRODUCTCITYINFOV2_ID_GENERATOR")
	private Long Id;
	private String cityCode;
	private String barCode;
	private Integer productId;
	private Integer productIdV1;
	private String quantityUnit;
	private int priority;
	private Integer subCategoryId;
	private Integer categoryId;
	private float MRP;
	private float salePrice;
	private String discountUnit;
	private float savedAmount;
	private float discount;
	private float deliveryCharge;
	private boolean setByDefault;
	private Long productDetailGroupId;
	private String photoGroup;
	private Long photoId;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	private boolean active;
	private boolean stockStatus;
	private int stockQuantity;
	private boolean discountAvailable;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getProductIdV1() {
		return productIdV1;
	}

	public void setProductIdV1(Integer productIdV1) {
		this.productIdV1 = productIdV1;
	}

	public String getQuantityUnit() {
		return quantityUnit;
	}

	public void setQuantityUnit(String quantityUnit) {
		this.quantityUnit = quantityUnit;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public Integer getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public float getMRP() {
		return MRP;
	}

	public void setMRP(float mRP) {
		MRP = mRP;
	}

	public float getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(float salePrice) {
		this.salePrice = salePrice;
	}

	public String getDiscountUnit() {
		return discountUnit;
	}

	public float getSavedAmount() {
		return savedAmount;
	}

	public Long getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}

	public void setSavedAmount(float savedAmount) {
		this.savedAmount = savedAmount;
	}

	public float getDiscount() {
		return discount;
	}

	public void setDiscount(float discount) {
		this.discount = discount;
	}

	public void setDiscountUnit(String discountUnit) {
		this.discountUnit = discountUnit;
	}

	
	public float getDeliveryCharge() {
		return deliveryCharge;
	}

	public boolean isSetByDefault() {
		return setByDefault;
	}

	public void setSetByDefault(boolean setByDefault) {
		this.setByDefault = setByDefault;
	}

	public int getStockQuantity() {
		return stockQuantity;
	}

	public void setStockQuantity(int stockQuantity) {
		this.stockQuantity = stockQuantity;
	}

	public void setDeliveryCharge(float deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public Long getProductDetailGroupId() {
		return productDetailGroupId;
	}

	public void setProductDetailGroupId(Long productDetailGroupId) {
		this.productDetailGroupId = productDetailGroupId;
	}

	public String getPhotoGroup() {
		return photoGroup;
	}

	public void setPhotoGroup(String photoGroup) {
		this.photoGroup = photoGroup;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isStockStatus() {
		return stockStatus;
	}

	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}

	public boolean isDiscountAvailable() {
		return discountAvailable;
	}

	public void setDiscountAvailable(boolean discountAvailable) {
		this.discountAvailable = discountAvailable;
	}

}
