package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({
		@NamedQuery(name = "PopularSearches.getAllActive", query = "Select s from PopularSearchesDTO s where s.active is true)"),
		@NamedQuery(name = "PopularSearches.getAll", query = "Select s from PopularSearchesDTO s)")

})

@Entity
@Table(name = "PopularSearches")
public class PopularSearchesDTO {
	@Id
	@SequenceGenerator(name = "POPULARSEARCHES_ID_GENERATOR", sequenceName = "POPULARSEARCHES_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "POPULARSEARCHES_ID_GENERATOR")
	private Long Id;
	private String keyword;
	private Integer priority;
	private Long createdBy;
	private Long updatedBy;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private boolean active;

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyWord(String keyword) {
		this.keyword = keyword;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Long getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
