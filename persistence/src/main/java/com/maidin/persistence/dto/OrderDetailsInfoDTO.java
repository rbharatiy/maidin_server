package com.maidin.persistence.dto;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({  
@NamedQuery(name = "OrderDetailsInfo.getByOrderIdAndProductId", 
query = "SELECT t from OrderDetailsInfoDTO t "
		+ "where t.orderId=:orderId and "
		+ "t.productId=:productId"),
@NamedQuery(name = "OrderDetailsInfo.getByOrderId", 
query = "SELECT t from OrderDetailsInfoDTO t "
        + "where t.orderId=:orderId and t.cancelled=false"),
@NamedQuery(name = "OrderDetailsInfo.getById", 
query = "SELECT t from OrderDetailsInfoDTO t "
        + "where t.Id=:Id "),
@NamedQuery(name = "OrderDetailsInfo.getByCustomerIdDeliveryDate", 
query = "SELECT odi from OrderDetailsInfoDTO odi, OrderInfoDTO oi "
        + "where oi.customerId=:customerId and oi.deliveryDate=:deliveryDate"
        + " and oi.Id=odi.orderId  and odi.orderStatus='DELIVERED'"
		+ " and odi.cancelled = false"),
@NamedQuery(name = "OrderDetailsInfo.getIdListByOrderId", 
query = "SELECT t.orderId from OrderDetailsInfoDTO t "
        + "where t.orderId=:orderId "),
@NamedQuery(name = "OrderDetailsInfo.deleteorderDetailsInfoList", 
query = "DELETE OrderDetailsInfoDTO o  where "
        + " o.orderId IN (:orderList) "),
@NamedQuery(name = "OrderDetailsInfo.updateOrderDetailsInfoList", 
query = "UPDATE OrderDetailsInfoDTO o  SET o.cancelled=:cancelled,o.quantity=:quantity,o.updatedOn=:updatedOn where "
        + " o.orderId IN (:orderList) "),
@NamedQuery(name = "OrderDetailsInfo.getByOrderIdWithNoCancelStatus", 
query = "SELECT t from OrderDetailsInfoDTO t "
        + "where t.orderId=:orderId "),
@NamedQuery(name = "OrderDetailsInfo.getSettledOrderDetailsByOrderId", 
query = "SELECT t from OrderDetailsInfoDTO t "
        + "where t.orderId=:orderId and orderStatus != 'CANCELLED' "),
@NamedQuery(name = "OrderDetailsInfo.getUnSettledPaymentOrders", 
query = "SELECT od from OrderInfoDTO oi, OrderDetailsInfoDTO od, TimeSlotDTO t "
        + "where oi.Id=od.orderId and oi.customerId=:customerId and t.timeSlot=oi.timeSlot and "
        + "od.orderStatus = 'CONFIRMED' and od.paymentStatus != 'PAYMENTSETTLED' and oi.scheduleId"
        + " is null order by oi.deliveryDate,t.Id,oi.Id,od.Id "),
@NamedQuery(name = "OrderDetailsInfo.getUnSettledOrdersByOrderId", 
query = "SELECT od from OrderDetailsInfoDTO od "
        + "where od.orderId=:orderId and od.orderStatus = 'CONFIRMED' and od.paymentStatus != 'PAYMENTSETTLED' "),
@NamedQuery(name = "OrderDetailsInfo.getSumOfAmountPaidByCustomer", 
query = "Select sum(od.amountPaidByCustomer) FROM OrderDetailsInfoDTO od where od.orderId IN (:orderDetailsInfoIdList)"),
@NamedQuery(name = "OrderDetailsInfo.getUnSettledDeliveredOrders", 
query = "SELECT od from OrderInfoDTO oi, OrderDetailsInfoDTO od, TimeSlotDTO t "
        + "where oi.Id=od.orderId and oi.customerId=:customerId and t.Id=oi.timeSlotId and "
        + "od.orderStatus = 'DELIVERED' and "
        + "od.paymentStatus != 'PAYMENTSETTLED' "
        + "order by oi.deliveryDate,t.StartTime,oi.Id,od.Id "),
@NamedQuery(name = "OrderDetailsInfo.getByOrderIdAndPaymentModeId", 
query = "SELECT od.paymentModeId from OrderInfoDTO oi, OrderDetailsInfoDTO od  "
        + "where od.orderId=:orderId and oi.Id=od.orderId and od.cancelled=0 "),
@NamedQuery(name = "OrderDetailsInfo.getUnsettledOrderCustomerList", 
query = "SELECT distinct oi.customerId from OrderInfoDTO oi, OrderDetailsInfoDTO od "
        + "where oi.Id=od.orderId  and "
        + "od.orderStatus = 'DELIVERED' and od.paymentStatus != 'PAYMENTSETTLED' "),
@NamedQuery(name = "OrderDetailsInfo.getByIds", 
query = "SELECT distinct od.orderId from OrderDetailsInfoDTO od "
        + "where od.Id in :Ids "),
@NamedQuery(name = "OrderDetailsInfo.getSettledOrderByOrderIdList",
query = "SELECT t from OrderDetailsInfoDTO t "
+ "where t.orderId in (:Ids) and orderStatus != 'CANCELLED' ")


})

@Entity
@Table(name = "OrderDetailsInfo")	
public class OrderDetailsInfoDTO {
	@Id
    @SequenceGenerator(name="ORDERDETAILSINFO_ID_GENERATOR", sequenceName="ORDERDETAILSINFO_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="ORDERDETAILSINFO_ID_GENERATOR") 
	
	  private Long Id;
	  private Long orderId;
	  private Integer productId;
	  private int quantity;
	  private Timestamp orderedTime;
	  private boolean cancelled;
	  private Timestamp orderCancellationTime;
	  private String productName;
	  private String barCode;
	  private float productMRP;
	  private float productSalePrice;
	  private float taxAmount;
	  private float deliveryCharges;
	  private float discountAmount;
	  private float totalProductCost;
	  //private float companyBenefitAmount;
	  //private float totalOrderLineCost;
	  private float amountPaidByCustomer;
	  private String paymentStatus;
	  private String orderStatus;
	  private Integer paymentModeId;
	  private Timestamp updatedOn;
	  private String payBy;
	  
	  
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Timestamp getOrderedTime() {
		return orderedTime;
	}
	public void setOrderedTime(Timestamp orderedTime) {
		this.orderedTime = orderedTime;
	}
	public boolean isCancelled() {
		return cancelled;
	}
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	public Timestamp getOrderCancellationTime() {
		return orderCancellationTime;
	}
	public void setOrderCancellationTime(Timestamp orderCancellationTime) {
		this.orderCancellationTime = orderCancellationTime;
	}
	
	public float getProductMRP() {
		return productMRP;
	}
	public void setProductMRP(float productMRP) {
		this.productMRP = productMRP;
	}
	
	public String getBarCode() {
		return barCode;
	}
	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}
	public float getProductSalePrice() {
		return productSalePrice;
	}
	public void setProductSalePrice(float productSalePrice) {
		this.productSalePrice = productSalePrice;
	}
	public float getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(float taxAmount) {
		this.taxAmount = taxAmount;
	}
	public float getDeliveryCharges() {
		return deliveryCharges;
	}
	public void setDeliveryCharges(float deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}
	public float getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(float discountAmount) {
		this.discountAmount = discountAmount;
	}
	public float getTotalProductCost() {
		return totalProductCost;
	}
	public void setTotalProductCost(float totalProductCost) {
		this.totalProductCost = totalProductCost;
	}
	public float getAmountPaidByCustomer() {
		return amountPaidByCustomer;
	}
	public void setAmountPaidByCustomer(float amountPaidByCustomer) {
		this.amountPaidByCustomer = amountPaidByCustomer;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public Integer getPaymentModeId() {
		return paymentModeId;
	}
	public void setPaymentModeId(Integer paymentModeId) {
		this.paymentModeId = paymentModeId;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	/*public String getPaymentModeIds() {*/
	/*	return paymentModeIds;
	}
	public void setPaymentModeIds(String paymentModeIds) {
		this.paymentModeIds = paymentModeIds;
	}*/
	public String getPayBy() {
		return payBy;
	}
	public void setPayBy(String payBy) {
		this.payBy = payBy;
	}
	
	
		}
	
	
	
	

	
