package com.maidin.persistence.dto.V2;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
	
	@NamedQuery(name = "CategoryInfoV2.getByIds", 
	query = "SELECT d from CategoryInfoV2DTO d "
			        + "where d.Id in (:Ids) and d.active=true order by field(d.Id, :Ids) "),
	@NamedQuery(name = "CategoryInfoV2.getAll", 
	query = "SELECT d from CategoryInfoV2DTO d order by d.priority"),
	@NamedQuery(name = "CategoryInfoV2.getById", 
	query = "SELECT d from CategoryInfoV2DTO d where d.Id=:Id"),
	@NamedQuery(name = "CategoryInfoV2.getByIdsWithoutActiveState", 
	query = "SELECT d from CategoryInfoV2DTO d "
			        + "where d.Id in (:Ids) order by field(d.Id, :Ids) "),
	@NamedQuery(name = "CategoryInfoV2.getAllMatchingTextWithActivationStatus", query = "SELECT distinct p.Id from CategoryInfoV2DTO p "
	+ " where p.category LIKE :productSubString and p.active=:active"),
	@NamedQuery(name = "CategoryInfoV2.getActiveIdsByIds", 
	query = "SELECT d.Id from CategoryInfoV2DTO d "
			        + "where d.Id in (:Ids) and d.active=true "),
})
@Entity
@Table(name = "CategoryInfoV2")
public class CategoryInfoV2DTO {
	@Id
    @SequenceGenerator(name="PRODUCTCATEGORY_ID_GENERATOR", sequenceName="PRODUCTCATEGORY_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="PRODUCTCATEGORY_ID_GENERATOR") 	
	private Integer Id;
	private String category;
	private int priority;
	private String description;
	private String timeSlotIds;
	private String todaySlotIds;
	private Long photoId;
	private boolean active;
	private boolean stockStatus;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Long createdBy;
	private Long updatedBy;
	
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getTimeSlotIds() {
		return timeSlotIds;
	}
	public void setTimeSlotIds(String timeSlotIds) {
		this.timeSlotIds = timeSlotIds;
	}
	public String getTodaySlotIds() {
		return todaySlotIds;
	}
	public void setTodaySlotIds(String todaySlotIds) {
		this.todaySlotIds = todaySlotIds;
	}
	public Long getPhotoId() {
		return photoId;
	}
	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}
	/*public boolean isHasAll() {
		return hasAll;
	}
	public void setHasAll(boolean hasAll) {
		this.hasAll = hasAll;
	}*/
	public boolean isActive() {
		return active;
	}
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isStockStatus() {
		return stockStatus;
	}
	public void setStockStatus(boolean stockStatus) {
		this.stockStatus = stockStatus;
	}
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Long getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}
	
    

    
}
