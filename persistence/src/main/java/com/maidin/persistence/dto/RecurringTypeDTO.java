package com.maidin.persistence.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@NamedQueries({  
@NamedQuery(name = "RecurringType.getById", 
			query = "SELECT t from RecurringTypeDTO t "
			        + "where t.Id=:Id ")

})

@Entity
@Table(name = "RecurringType")	
public class RecurringTypeDTO {
	@Id
    @SequenceGenerator(name="RECURRINGTYPE_ID_GENERATOR", sequenceName="RECURRINGTYPE_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="RECURRINGTYPE_ID_GENERATOR") 

	private Long Id;
	private Long recurringType;
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public Long getRecurringType() {
		return recurringType;
	}
	public void setRecurringType(Long recurringType) {
		this.recurringType = recurringType;
	}

	
	
}
