package com.maidin.persistence.dto;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@NamedQueries({  
@NamedQuery(name = "Customer.getByMobileNumber", 
			query = "SELECT t from CustomerDTO t "
			        + "where t.mobileNumber=:mobileNumber "
			        + "and t.active=true"),
@NamedQuery(name = "Customer.getByMobileNumberWithoutActiveState", 
query = "SELECT d from CustomerDTO d "
        + "where d.mobileNumber=:mobileNumber "),
@NamedQuery(name = "Customer.getByEmail", 
query = "SELECT d from CustomerDTO d "
        + "where d.email=:email "),
@NamedQuery(name = "Customer.getByIdAndMobileNumber", 
query = "SELECT d from CustomerDTO d "
        + " where d.Id=:Id "
		+ " and d.mobileNumber=:mobileNumber"),
/*@NamedQuery(name = "Customer.getByCityProjectTowerFlat", 
query = "SELECT d from CustomerDTO d "
        + " where d.city=:city "
		+ " and d.project=:project"
		+ " and d.tower=:tower"
		+ " and d.flat=:flat"),*/
@NamedQuery(name = "Customer.getById", 
query = "SELECT d from CustomerDTO d "
        + " where d.Id=:Id "),
@NamedQuery(name = "Customer.getByAddressId", 
query = "SELECT d.Id from CustomerDTO d "
        + " where d.addressId=:addressId and d.active=true "),
@NamedQuery(name = "Customer.getUsersByFlatCount", 
query = "SELECT  count(distinct a.flatId) from AddressDTO a , "
		+ "CustomerDTO c where c.active=:active and a.Id = c.addressId "),
@NamedQuery(name = "Customer.getRegisteredUserCount", 
query = "SELECT count(d) from CustomerDTO d "
        + " where d.active=:active"),
@NamedQuery(name = "Customer.getActiveUsersList", 
query = "SELECT d from CustomerDTO d "
        + " where d.active=:active and d.addressId != 19")
})

@Entity
@Table(name = "Customer")	
public class CustomerDTO {
	@Id
    @SequenceGenerator(name="CUSTOMER_ID_GENERATOR", sequenceName="CUSTOMER_ID_SEQ", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.AUTO, generator="CUSTOMER_ID_GENERATOR") 	
	private Long Id;
	private String fullName;
	private String email;
	private boolean emailVerified;
	private String mobileNumber;
	private String gender;
	private Long addressId;
	/*private String project;
	private String tower;
	private String flat;*/
	private boolean active;
	private Timestamp createdOn;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
	public Long getAddressId() {
		return addressId;
	}
	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}
	/*public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTower() {
		return tower;
	}
	public void setTower(String tower) {
		this.tower = tower;
	}
	
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getFlat() {
		return flat;
	}
	public void setFlat(String flat) {
		this.flat = flat;
	}*/
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
}
