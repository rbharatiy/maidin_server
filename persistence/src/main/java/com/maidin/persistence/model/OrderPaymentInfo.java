package com.maidin.persistence.model;

import java.sql.Timestamp;

public class OrderPaymentInfo {
	private float sumTotal;
	private float difference;
	public float getSumTotal() {
		return sumTotal;
	}
	public void setSumTotal(float sumTotal) {
		this.sumTotal = sumTotal;
	}
	public float getDifference() {
		return difference;
	}
	public void setDifference(float difference) {
		this.difference = difference;
	}
	}
