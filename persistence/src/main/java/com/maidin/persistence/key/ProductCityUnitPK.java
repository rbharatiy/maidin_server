package com.maidin.persistence.key;

import java.io.Serializable;

public class ProductCityUnitPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5999921189797986093L;
	private Integer productId;
	private String cityCode;
	private String unit;

	public ProductCityUnitPK(Integer productId, String cityCode, String unit) {
		super();
		this.productId = productId;
		this.cityCode = cityCode;
		this.unit = unit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityCode == null) ? 0 : cityCode.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((unit == null) ? 0 : unit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductCityUnitPK other = (ProductCityUnitPK) obj;
		if (cityCode == null) {
			if (other.cityCode != null)
				return false;
		} else if (!cityCode.equals(other.cityCode))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (unit == null) {
			if (other.unit != null)
				return false;
		} else if (!unit.equals(other.unit))
			return false;
		return true;
	}

	
	

}
